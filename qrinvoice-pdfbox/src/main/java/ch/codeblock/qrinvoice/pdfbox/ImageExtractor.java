package ch.codeblock.qrinvoice.pdfbox;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.form.PDFormXObject;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageExtractor {
    private final Logger logger = LoggerFactory.getLogger(ImageExtractor.class);

    public List<BufferedImage> getImagesFromPage(PDPage page, int minWidth, int minHeight) throws IOException {
        return getImagesFromResources(page.getResources(), minWidth, minHeight);
    }

    private List<BufferedImage> getImagesFromResources(PDResources resources, int minWidth, int minHeight)
            throws IOException {

        final List<BufferedImage> images = new ArrayList<>();

        if (resources != null) {
            for (COSName xObjectName : resources.getXObjectNames()) {
                final PDXObject xObject = resources.getXObject(xObjectName);

                if (xObject instanceof PDFormXObject) {
                    // Recursive
                    images.addAll(getImagesFromResources(((PDFormXObject) xObject).getResources(), minWidth, minHeight));
                } else if (xObject instanceof PDImageXObject) {
                    final PDImageXObject imageXObject = (PDImageXObject) xObject;
                    if (imageXObject.getWidth() >= minWidth && imageXObject.getHeight() >= minHeight) {
                        images.add(imageXObject.getImage());
                    } else {
                        logger.debug("ignoring image as it is too small width={} height={} (minWidth={} minHeight={})", imageXObject.getWidth(), imageXObject.getHeight(), minWidth, minHeight);
                    }
                }
            }
        }

        return images;
    }
}
