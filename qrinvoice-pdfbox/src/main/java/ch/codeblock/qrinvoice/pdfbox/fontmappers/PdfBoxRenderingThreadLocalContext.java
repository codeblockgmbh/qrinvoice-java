package ch.codeblock.qrinvoice.pdfbox.fontmappers;

public class PdfBoxRenderingThreadLocalContext {
    public static final ThreadLocal<Boolean> QR_INVOICE_PDFBOX_RENDER_INFLIGHT = new ThreadLocal<>();
    
    public static void activate() {
        QR_INVOICE_PDFBOX_RENDER_INFLIGHT.set(true);
    }
    public static void deactivate() {
        QR_INVOICE_PDFBOX_RENDER_INFLIGHT.remove();
    }
    public static boolean isQrInvoicePdfBoxRenderingInFlight() {
        return QR_INVOICE_PDFBOX_RENDER_INFLIGHT.get() != null && QR_INVOICE_PDFBOX_RENDER_INFLIGHT.get();
    }
}
