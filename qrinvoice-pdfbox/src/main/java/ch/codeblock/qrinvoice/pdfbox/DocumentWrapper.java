package ch.codeblock.qrinvoice.pdfbox;

import ch.codeblock.qrinvoice.pdfbox.fontmappers.PdfBoxRenderingThreadLocalContext;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class DocumentWrapper implements AutoCloseable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentWrapper.class);
    private static final int RENDER_DPI = 72 * 3;

    private final PDDocument document;
    private final PDFRenderer renderer;
    private final int pageCount;


    DocumentWrapper(final InputStream inputStream) throws IOException {
        this(PDDocument.load(inputStream));
    }

    DocumentWrapper(final byte[] document) throws IOException {
        this(PDDocument.load(document));
    }

    DocumentWrapper(final PDDocument document) {
        this(document, new PDFRenderer(document));
    }

    DocumentWrapper(final PDDocument document, final PDFRenderer renderer) {
        this.document = document;
        this.pageCount = document != null ? document.getNumberOfPages() : 0;
        this.renderer = renderer;
    }

    public PDDocument getDocument() {
        return document;
    }

    public int getPageCount() {
        return pageCount;
    }

    public PDFRenderer getRenderer() {
        return renderer;
    }

    public BufferedImage renderPage(final int pageNr) throws IOException {
        final int pageIndex = pageNr - 1;

        try {
            final long start = System.currentTimeMillis();
            PdfBoxRenderingThreadLocalContext.activate();
            final BufferedImage bufferedImage = renderer.renderImageWithDPI(pageIndex, RENDER_DPI, ImageType.GRAY);

            final long end = System.currentTimeMillis();
            LOGGER.debug("took {} ms to render pageNr={} with dpi={}", (end - start), pageNr, RENDER_DPI);
            return bufferedImage;
        } finally {
            PdfBoxRenderingThreadLocalContext.deactivate();
        }
    }

    @Override
    public void close() throws IOException {
        if (document != null) {
            document.close();
        }
    }
}