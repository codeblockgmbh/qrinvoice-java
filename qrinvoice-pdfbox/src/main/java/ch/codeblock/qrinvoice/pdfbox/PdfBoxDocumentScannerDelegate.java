package ch.codeblock.qrinvoice.pdfbox;

import ch.codeblock.qrinvoice.QrInvoiceCodeScanner;
import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.pdfbox.fontmappers.DelegatingFontMapper;
import ch.codeblock.qrinvoice.qrcode.DecodeException;
import ch.codeblock.qrinvoice.qrcode.QrCodeReaderOptions;
import ch.codeblock.qrinvoice.qrcode.SwissQrCode;
import org.apache.pdfbox.pdmodel.font.FontMappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class PdfBoxDocumentScannerDelegate {
    private static final int SCANNING_PDF_MAX_PAGES_DEFAULT = 20;

    static {
        if (System.getProperty(SystemProperties.DISABLE_PDFBOX_CMM_OVERRIDE) == null) {
            PdfBoxCmmUtils.applyKcms();
        }
        if (System.getProperty(SystemProperties.DISABLE_PDFBOX_DELEGATING_FONT_MAPPER) == null) {
            FontMappers.set(new DelegatingFontMapper(FontMappers.instance()));
        }
    }

    private final Logger logger = LoggerFactory.getLogger(PdfBoxDocumentScannerDelegate.class);

    private final QrCodeReaderOptions options;
    
    private final QrInvoiceCodeScanner qrInvoiceCodeScanner;

    private final ImageExtractor imageExtractor = new ImageExtractor();

    public PdfBoxDocumentScannerDelegate(QrCodeReaderOptions options) {
        this.options = options;
        this.qrInvoiceCodeScanner = QrInvoiceCodeScanner.create(options);
    }

    List<QrInvoice> scanDocumentForAllSwissQrCodes(final DocumentWrapper documentWrapper) throws IOException {
        // in case of scanning for all SwissQrCodes, fail fast if max pages limit is exceeded
        assertMaxPages(documentWrapper);

        final ArrayList<QrInvoice> qrInvoices = new ArrayList<>();

        for (int pageNr = 1; pageNr <= documentWrapper.getPageCount(); pageNr++) {
            qrInvoices.addAll(processSinglePage(documentWrapper, pageNr));
        }

        return qrInvoices;
    }

    List<QrInvoice> scanDocumentForAllSwissQrCodes(final DocumentWrapper documentWrapper, final int pageNr) throws IOException {
        assertPageNrInRange(documentWrapper, pageNr);
        return processSinglePage(documentWrapper, pageNr);
    }

    private List<QrInvoice> processSinglePage(final DocumentWrapper documentWrapper, final int pageNr) throws IOException {
        final List<QrInvoice> allFromImages = findAllFromImages(documentWrapper, pageNr);
        final List<QrInvoice> allInPage = new ArrayList<>(allFromImages);
        final BufferedImage bufferedImage = documentWrapper.renderPage(pageNr);
        try {
            for (QrInvoice qrInvoice : qrInvoiceCodeScanner.scanMultiple(bufferedImage)) {
                if (!allInPage.contains(qrInvoice)) {
                    allInPage.add(qrInvoice);
                } else {
                    logger.debug("Duplicate QrInvoice found in rendered page already known from extracted images");
                }
            }
        } catch (DecodeException e) {
            logger.debug("No images found in rendered page");
        } catch (Exception e) {
            logger.warn("Unexpected exception", e);
        }
        return allInPage;
    }


    Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final DocumentWrapper documentWrapper) throws IOException {
        final int pageCount = documentWrapper.getPageCount();
        if (pageCount == 0) {
            return Optional.empty();
        } else if (pageCount == 1) {
            return scanPageForFirstSwissQrCode(documentWrapper, 1);
        }

        // e.g. pageCount=4
        final List<Integer> pageNrs = IntStream.range(1, pageCount + 1).boxed().sorted().collect(Collectors.toList());
        // e.g. pageNrs=1,2,3,4
        // move last to front -> pageNrs=4,1,2,3
        // we assume that most invoice document will have the Swiss QR Code on the last page, or on the first.
        // that is only an assumption to speed up scanning when looking for the first Swiss Qr Code in a document
        pageNrs.add(0, pageNrs.remove(pageNrs.size() - 1));

        boolean maxPageLimitValidated = false;
        for (int i = 0; i < pageNrs.size(); i++) {
            final Integer pageNr = pageNrs.get(i);
            final Optional<QrInvoice> qrInvoiceOptional = scanPageForFirstSwissQrCode(documentWrapper, pageNr);
            if (qrInvoiceOptional.isPresent()) {
                return qrInvoiceOptional;
            }

            // scan first and last page and after that, validate once for max page limit
            if (i > 1 && !maxPageLimitValidated) {
                assertMaxPages(documentWrapper);
                maxPageLimitValidated = true;
            }
        }
        return Optional.empty();
    }

    Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final DocumentWrapper documentWrapper, final int pageNr) throws IOException {
        assertPageNrInRange(documentWrapper, pageNr);
        return scanPageForFirstSwissQrCode(documentWrapper, pageNr);
    }

    private Optional<QrInvoice> scanPageForFirstSwissQrCode(final DocumentWrapper documentWrapper, final int pageNr) throws IOException {
        final Optional<QrInvoice> firstInImages = findFirstInImages(documentWrapper, pageNr);
        if (firstInImages.isPresent()) {
            return firstInImages;
        }

        return findFirstInRenderedPage(documentWrapper, pageNr);
    }

    private Optional<QrInvoice> findFirstInRenderedPage(final DocumentWrapper documentWrapper, final int pageNr) {
        try {
            final BufferedImage bufferedImage = documentWrapper.renderPage(pageNr);
            return Optional.ofNullable(qrInvoiceCodeScanner.scan(bufferedImage));
        } catch (DecodeException e) {
            return Optional.empty();
        } catch (Exception e) {
            logger.warn("Unexpected exception", e);
            return Optional.empty();
        }
    }

    private Optional<QrInvoice> findFirstInImages(final DocumentWrapper documentWrapper, final int pageNr) throws IOException {
        final int minSize = SwissQrCode.QR_CODE_MIN_MODULES;
        final List<BufferedImage> imagesFromPage = imageExtractor.getImagesFromPage(documentWrapper.getDocument().getPage(pageNr - 1), minSize, minSize);
        for (final BufferedImage bufferedImage : imagesFromPage) {
            final Optional<QrInvoice> qrInvoice = findQrInvoiceFromImage(bufferedImage);
            if (qrInvoice.isPresent()) {
                return qrInvoice;
            }
        }
        return Optional.empty();
    }

    private List<QrInvoice> findAllFromImages(final DocumentWrapper documentWrapper, final int pageNr) throws IOException {
        final List<QrInvoice> qrInvoices = new ArrayList<>();
        final int minSize = SwissQrCode.QR_CODE_MIN_MODULES;
        final List<BufferedImage> imagesFromPage = imageExtractor.getImagesFromPage(documentWrapper.getDocument().getPage(pageNr - 1), minSize, minSize);
        for (final BufferedImage bufferedImage : imagesFromPage) {
            final Optional<QrInvoice> qrInvoice = findQrInvoiceFromImage(bufferedImage);
            qrInvoice.ifPresent(qrInvoices::add);
        }
        return qrInvoices;
    }

    private Optional<QrInvoice> findQrInvoiceFromImage(BufferedImage bufferedImage) {
        try {
            final QrInvoice qrInvoice = qrInvoiceCodeScanner.scan(bufferedImage);
            if (qrInvoice != null) {
                return Optional.of(qrInvoice);
            }
        } catch (DecodeException e) {
            logger.debug("Unable to find QR code in given image. Cause={}", e.getMessage());
        } catch (Exception e) {
            logger.warn("Unexpected exception", e);
        }
        return Optional.empty();
    }


    private void assertMaxPages(final DocumentWrapper documentWrapper) {
        final int scanningPdfMaxPages = getScanningPdfMaxPages();
        if (documentWrapper.getPageCount() > scanningPdfMaxPages) {
            throw new ValidationException(String.format("PDF page scanning is limited to %s pages but got %s pages", scanningPdfMaxPages, documentWrapper.getPageCount()));
        }

    }

    private int getScanningPdfMaxPages() {
        try {
            final String scanningPdfMaxPages = System.getProperty(SystemProperties.SCANNING_PDF_MAX_PAGES);
            if (scanningPdfMaxPages != null) {
                return Integer.parseInt(scanningPdfMaxPages);
            }
        } catch (Exception e) {
            logger.warn("");
        }
        return SCANNING_PDF_MAX_PAGES_DEFAULT;
    }

    private void assertPageNrInRange(final DocumentWrapper documentWrapper, final int pageNr) {
        if (pageNr < 1) {
            throw new IllegalArgumentException("requested pageNr must not be 0");
        } else if (pageNr > documentWrapper.getPageCount()) {
            throw new IllegalArgumentException("requested pageNr is greater than the number of available pages (" + documentWrapper.getPageCount() + ")");
        }
    }

}
