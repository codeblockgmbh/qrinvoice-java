package ch.codeblock.qrinvoice.pdfbox;

import org.apache.pdfbox.rendering.PDFRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>Due to https://bugs.openjdk.java.net/browse/JDK-8041125 mentioned on https://pdfbox.apache.org/2.0/getting-started.html (PDFBox and Java 8)</p>
 * <p>Methods copied from org.apache.pdfbox.rendering.PDFRenderer</p>
 *
 * @see PDFRenderer
 */
public class PdfBoxCmmUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfBoxCmmUtils.class);

    public static void applyKcms() {
        // was taken from org.apache.pdfbox.rendering.PDFRenderer#suggestKCMS 
        String cmmProperty = System.getProperty("sun.java2d.cmm");
        if (isMinJdk8() && !"sun.java2d.cmm.kcms.KcmsServiceProvider".equals(cmmProperty)) {
            try {
                // Make sure that class exists
                Class.forName("sun.java2d.cmm.kcms.KcmsServiceProvider");

                String version = System.getProperty("java.version");
                if (version == null ||
                        isGoodVersion(version, "1.8.0_(\\d+)", 191) ||
                        isGoodVersion(version, "9.0.(\\d+)", 4)) {
                    return;
                }

                // codeblock: instead of logging, set the property
                LOGGER.info("Setting system property 'sun.java2d.cmm' to 'sun.java2d.cmm.kcms.KcmsServiceProvider', was '{}' before", cmmProperty);
                System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
            } catch (ClassNotFoundException e) {
                // KCMS not available
            }
        }
    }

    private static boolean isGoodVersion(String version, String regex, int min) {
        Matcher matcher = Pattern.compile(regex).matcher(version);
        if (matcher.matches() && matcher.groupCount() >= 1) {
            try {
                int v = Integer.parseInt(matcher.group(1));
                if (v >= min) {
                    // LCMS no longer bad
                    return true;
                }
            } catch (NumberFormatException ex) {
                return true;
            }
        }
        return false;
    }

    private static boolean isMinJdk8() {
        // strategy from lucene-solr/lucene/core/src/java/org/apache/lucene/util/Constants.java
        String version = System.getProperty("java.specification.version");
        final StringTokenizer st = new StringTokenizer(version, ".");
        try {
            int major = Integer.parseInt(st.nextToken());
            int minor = 0;
            if (st.hasMoreTokens()) {
                minor = Integer.parseInt(st.nextToken());
            }
            return major > 1 || (major == 1 && minor >= 8);
        } catch (NumberFormatException nfe) {
            // maybe some new numbering scheme in the 22nd century
            return true;
        }
    }
}
