package ch.codeblock.qrinvoice.pdfbox.fontmappers;

import org.apache.fontbox.FontBoxFont;
import org.apache.fontbox.ttf.TTFParser;
import org.apache.fontbox.ttf.TrueTypeFont;
import org.apache.pdfbox.pdmodel.font.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DelegatingFontMapper implements FontMapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(DelegatingFontMapper.class);

    private final FontMapper pdfBoxDefaultFontMapper;

    private final TrueTypeFont lastResortFont;

    public DelegatingFontMapper(FontMapper pdfBoxDefaultFontMapper) {
        this.pdfBoxDefaultFontMapper = pdfBoxDefaultFontMapper;
        this.lastResortFont = getLastResortFont();
    }

    private TrueTypeFont getLastResortFont() {
        final TrueTypeFont lastResortFont;
        try {
            String ttfName = "/org/apache/pdfbox/resources/ttf/LiberationSans-Regular.ttf";
            InputStream resourceAsStream = FontMapper.class.getResourceAsStream(ttfName);
            if (resourceAsStream == null) {
                throw new IOException("resource '" + ttfName + "' not found");
            }
            InputStream ttfStream = new BufferedInputStream(resourceAsStream);
            TTFParser ttfParser = new TTFParser();
            lastResortFont = ttfParser.parse(ttfStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lastResortFont;
    }

    @Override
    public FontMapping<TrueTypeFont> getTrueTypeFont(String baseFont, PDFontDescriptor fontDescriptor) {
        if (PdfBoxRenderingThreadLocalContext.isQrInvoicePdfBoxRenderingInFlight()) {
            LOGGER.trace("using fallback font mapping");
            return new FontMapping<>(lastResortFont, true);
        } else {
            LOGGER.trace("delegating to PDFBox default font mapper");
            return pdfBoxDefaultFontMapper.getTrueTypeFont(baseFont, fontDescriptor);
        }
    }

    @Override
    public FontMapping<FontBoxFont> getFontBoxFont(String baseFont, PDFontDescriptor fontDescriptor) {
        if (PdfBoxRenderingThreadLocalContext.isQrInvoicePdfBoxRenderingInFlight()) {
            LOGGER.trace("using fallback font mapping");
            return new FontMapping<>(lastResortFont, true);
        } else {
            LOGGER.trace("delegating to PDFBox default font mapper");
            return pdfBoxDefaultFontMapper.getFontBoxFont(baseFont, fontDescriptor);
        }
    }

    @Override
    public CIDFontMapping getCIDFont(String baseFont, PDFontDescriptor fontDescriptor, PDCIDSystemInfo cidSystemInfo) {
        if (PdfBoxRenderingThreadLocalContext.isQrInvoicePdfBoxRenderingInFlight()) {
            LOGGER.trace("using fallback font mapping");
            return new CIDFontMapping(null, lastResortFont, true);
        } else {
            LOGGER.trace("delegating to PDFBox default font mapper");
            return pdfBoxDefaultFontMapper.getCIDFont(baseFont, fontDescriptor, cidSystemInfo);
        }
    }
}