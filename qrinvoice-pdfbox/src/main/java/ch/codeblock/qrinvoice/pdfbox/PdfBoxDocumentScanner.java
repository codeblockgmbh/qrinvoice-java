package ch.codeblock.qrinvoice.pdfbox;

import ch.codeblock.qrinvoice.infrastructure.IDocumentScanner;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.qrcode.QrCodeReaderOptions;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

public class PdfBoxDocumentScanner implements IDocumentScanner {
    private final PdfBoxDocumentScannerDelegate delegate;

    public PdfBoxDocumentScanner(QrCodeReaderOptions options) {
        this.delegate = new PdfBoxDocumentScannerDelegate(options);
    }

    @Override
    public List<QrInvoice> scanDocumentForAllSwissQrCodes(final byte[] document) throws IOException {
        try (DocumentWrapper documentWrapper = new DocumentWrapper(document)) {
            return delegate.scanDocumentForAllSwissQrCodes(documentWrapper);
        }
    }

    @Override
    public List<QrInvoice> scanDocumentForAllSwissQrCodes(final InputStream inputStream) throws IOException {
        try (DocumentWrapper documentWrapper = new DocumentWrapper(inputStream)) {
            return delegate.scanDocumentForAllSwissQrCodes(documentWrapper);
        }
    }

    @Override
    public List<QrInvoice> scanDocumentForAllSwissQrCodes(final byte[] document, final int pageNr) throws IOException {
        try (DocumentWrapper documentWrapper = new DocumentWrapper(document)) {
            return delegate.scanDocumentForAllSwissQrCodes(documentWrapper, pageNr);
        }
    }

    @Override
    public List<QrInvoice> scanDocumentForAllSwissQrCodes(final InputStream inputStream, final int pageNr) throws IOException {
        try (DocumentWrapper documentWrapper = new DocumentWrapper(inputStream)) {
            return delegate.scanDocumentForAllSwissQrCodes(documentWrapper, pageNr);
        }
    }

    @Override
    public Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final byte[] document) throws IOException {
        try (DocumentWrapper documentWrapper = new DocumentWrapper(document)) {
            return delegate.scanDocumentUntilFirstSwissQrCode(documentWrapper);
        }
    }

    @Override
    public Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final InputStream inputStream) throws IOException {
        try (DocumentWrapper documentWrapper = new DocumentWrapper(inputStream)) {
            return delegate.scanDocumentUntilFirstSwissQrCode(documentWrapper);
        }
    }

    @Override
    public Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final byte[] document, final int pageNr) throws IOException {
        return delegate.scanDocumentUntilFirstSwissQrCode(new DocumentWrapper(document), pageNr);
    }

    @Override
    public Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final InputStream inputStream, final int pageNr) throws IOException {
        return delegate.scanDocumentUntilFirstSwissQrCode(new DocumentWrapper(inputStream), pageNr);
    }

}
