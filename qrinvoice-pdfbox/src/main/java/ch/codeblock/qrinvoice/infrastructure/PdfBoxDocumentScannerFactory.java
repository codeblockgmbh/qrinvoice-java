package ch.codeblock.qrinvoice.infrastructure;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.pdfbox.PdfBoxDocumentScanner;
import ch.codeblock.qrinvoice.qrcode.QrCodeReaderOptions;
import com.github.jaiimageio.jpeg2000.impl.J2KImageReaderSpi;

import javax.imageio.spi.IIORegistry;

public class PdfBoxDocumentScannerFactory implements IDocumentScannerFactory {

    static {
        IIORegistry.getDefaultInstance().registerServiceProvider(new J2KImageReaderSpi());
    }

    @Override
    public IDocumentScanner create(QrCodeReaderOptions options) {
        return new PdfBoxDocumentScanner(options);
    }

    @Override
    public String getShortName() {
        return getClass().getSimpleName().replace("DocumentScannerFactory", "");
    }

    @Override
    public boolean supports(final Object obj) {
        if (obj instanceof MimeType) {
            return MimeType.PDF.equals(obj);
        } else if (obj instanceof String) {
            return MimeType.PDF.getMimeType().equals(obj);
        }
        return false;
    }
}
