#!/bin/bash

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -rv|--releaseversion)
    RELEASEVERSION="$2"
    shift # past argument
    shift # past value
    ;;
    -nv|--newversion)
    NEWVERSION="$2"
    shift # past argument
    shift # past value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ -z "${RELEASEVERSION}" ] || [ -z "${NEWVERSION}" ]; then
    echo "both releaseversion and newversion must be set"
    exit -1
fi

echo RELEASE VERSION  = "${RELEASEVERSION}"
echo NEW VERSION     = "${NEWVERSION}"

while true; do
    read -p "Do you wish to release with this version? y/n: " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

# first of all verify that javadoc generation works
mvn javadoc:javadoc
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

mvn versions:set -DnewVersion="${RELEASEVERSION}"
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

mvn clean install
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

mvn versions:commit
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

git add .
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

git commit -m "release ${RELEASEVERSION}"
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

git tag "${RELEASEVERSION}"
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

git push origin "${RELEASEVERSION}"
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

git push origin HEAD
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

mvn clean deploy -Pdeployment
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

mvn site:site site:deploy
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

mvn versions:set -DnewVersion="${NEWVERSION}"
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

mvn clean install
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

mvn versions:commit
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

git add .
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

git commit -m "prepare ${NEWVERSION}"
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

git push origin HEAD
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; }; fi

echo "done"
