/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.pdf;

import ch.codeblock.qrinvoice.TechnicalException;
import com.lowagie.text.pdf.BadPdfFormatException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;

import java.io.IOException;

public abstract class AbstractPdfMerger {
    public static PdfCopy importPdf(PdfReader pdfReader, PdfCopy copy) throws IOException {
        return importPdf(pdfReader, copy, pdfReader.getNumberOfPages());
    }

    public static PdfCopy importPdf(PdfReader pdfReader, PdfCopy copy, int nrOfPages) throws IOException {
        if (nrOfPages < 1) {
            return copy;
        }

        PdfImportedPage page;

        try {
            // go through pages of PDF to copy
            // all the pages to the  target PDF
            for (int pageNr = 1; pageNr <= nrOfPages; pageNr++) {
                // grab page from input document
                page = copy.getImportedPage(pdfReader, pageNr);
                // add content to target PDF
                copy.addPage(page);
            }
        } catch (BadPdfFormatException e) {
            throw new TechnicalException("Unable to import PDF document", e);
        }
        return copy;
    }
}
