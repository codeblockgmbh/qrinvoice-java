/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.pdf;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.QrInvoicePaymentPartReceiptCreator;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.util.IOUtils;
import com.lowagie.text.pdf.PdfReader;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class QrPDFMergerTest {
    private PaymentPartReceipt paymentPartReceipt;
    private PaymentPartReceipt paymentPartReceiptA4;
    private File outputDirectory;

    @Before
    public void setup() throws Exception {
        // Setup target directory
        String path = QrPDFMergerTest.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        path = URLDecoder.decode(path, "UTF-8");
        File targetClassesDir = new File(path);
        File targetDir = targetClassesDir.getParentFile();

        outputDirectory = new File(targetDir, "test-output-qrpdfmerger");

        if (!outputDirectory.exists() && !outputDirectory.mkdirs()) {
            throw new Exception("Failed to create output directory.");
        }

        // Create payment part receipt
        final QrInvoice qrInvoice = ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder
                .create()
                .creditorIBAN("CH44 3199 9123 0008 8901 2")
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference("210000000003139471430009017")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(Arrays.asList(
                        "eBill/B/john.doe@example.com",
                        "cdbk/some/values"
                ))
                .build();

        paymentPartReceipt = QrInvoicePaymentPartReceiptCreator
                .create()
                .qrInvoice(qrInvoice)
                .outputFormat(OutputFormat.PDF)
                .pageSize(PageSize.DIN_LANG)
                .fontFamily(FontFamily.LIBERATION_SANS) // or HELVETICA, ARIAL
                .locale(Locale.GERMAN)
                .createPaymentPartReceipt();

        paymentPartReceiptA4 = QrInvoicePaymentPartReceiptCreator
                .create()
                .qrInvoice(qrInvoice)
                .outputFormat(OutputFormat.PDF)
                .pageSize(PageSize.A4)
                .fontFamily(FontFamily.LIBERATION_SANS) // or HELVETICA, ARIAL
                .locale(Locale.GERMAN)
                .createPaymentPartReceipt();
    }

    @Test
    public void mergePdfs() throws IOException {
        // the resulting byte array contains the payment part & receipt as PDF
        // arrange
        final byte[] paymentPartReceiptPdf = paymentPartReceipt.getData();
        byte[] inputFile = IOUtils.toByteArray(QrPDFMergerTest.class.getResourceAsStream("/example.pdf"));

        // act
        byte[] mergedPdfs = QrPdfMerger.create().mergePdfs(inputFile, paymentPartReceiptPdf);
        PdfReader pdfReader = new PdfReader(mergedPdfs);

        // assert
        assertEquals(1, pdfReader.getNumberOfPages());

        // Store
        storeInOutputDirectory(mergedPdfs, "mergedPDFs.pdf");
    }

    @Test
    public void mergePdfsMultipage() throws IOException {
        // the resulting byte array contains the payment part & receipt as PDF
        // arrange
        final byte[] paymentPartReceiptPdf = paymentPartReceipt.getData();
        byte[] inputFile = IOUtils.toByteArray(QrPDFMergerTest.class.getResourceAsStream("/example_multipage.pdf"));

        // act
        byte[] mergedPdfs = QrPdfMerger.create().mergePdfs(inputFile, paymentPartReceiptPdf, 2);
        PdfReader pdfReader = new PdfReader(mergedPdfs);

        // assert
        assertEquals(3, pdfReader.getNumberOfPages());

        // Store
        storeInOutputDirectory(mergedPdfs, "mergedPDFs_multipage.pdf");
    }


    @Test
    public void appendPdfs() throws IOException {
        //arrange
        final byte[] paymentPartReceiptPdf = paymentPartReceiptA4.getData();
        byte[] inputFile = IOUtils.toByteArray(QrPDFMergerTest.class.getResourceAsStream("/example.pdf"));
        List<byte[]> data = new ArrayList<>();
        data.add(0, inputFile);
        data.add(1, paymentPartReceiptPdf);

        //act
        byte[] mergedPdfs = QrPdfMerger.create().appendPdfs(data);
        PdfReader pdfReader = new PdfReader(mergedPdfs);

        //assert
        assertEquals(2, pdfReader.getNumberOfPages());

        // Store
        storeInOutputDirectory(mergedPdfs, "appendedPDFs.pdf");
    }

    @Test
    public void appendPdfsMultipage() throws IOException {
        //arrange
        final byte[] paymentPartReceiptPdf = paymentPartReceiptA4.getData();
        byte[] inputFile = IOUtils.toByteArray(QrPDFMergerTest.class.getResourceAsStream("/example_multipage.pdf"));
        List<byte[]> data = new ArrayList<>();
        data.add(0, inputFile);
        data.add(1, paymentPartReceiptPdf);

        //act
        byte[] mergedPdfs = QrPdfMerger.create().appendPdfs(data);
        PdfReader pdfReader = new PdfReader(mergedPdfs);

        //assert
        assertEquals(4, pdfReader.getNumberOfPages());

        // Store
        storeInOutputDirectory(mergedPdfs, "appendedPDFs_multipage.pdf");
    }

    private void storeInOutputDirectory(byte[] data, String fileName) throws IOException {
        try (FileOutputStream stream = new FileOutputStream(new File(outputDirectory, fileName))) {
            stream.write(data);
        }
    }
}
