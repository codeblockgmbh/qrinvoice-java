/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.paymentpart;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.infrastructure.IOutputWriterFactory;
import ch.codeblock.qrinvoice.paymentpartreceipt.BoundaryLines;
import ch.codeblock.qrinvoice.tests.integration.testfiles.TargetTestFileHelper;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import org.apache.commons.io.FileUtils;
import org.junit.Before;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

public abstract class PaymentPartReceiptTestsBase {
    final IOutputWriterFactory factory;
    final OutputFormat outputFormat;
    final FontFamily fontFamily;
    final BoundaryLines boundaryLines;
    final Boolean additionalPrintMargin;
    final Locale locale;
    final String testFile;

    PaymentPartReceiptTestsBase(final IOutputWriterFactory factory, final OutputFormat outputFormat, final FontFamily fontFamily, final Locale locale, final String testFile) {
        this.factory = factory;
        this.outputFormat = outputFormat;
        this.fontFamily = fontFamily;
        this.boundaryLines = BoundaryLines.ENABLED;
        this.additionalPrintMargin = false;
        this.locale = locale;
        this.testFile = testFile;
    }

    PaymentPartReceiptTestsBase(final IOutputWriterFactory factory, final OutputFormat outputFormat, final FontFamily fontFamily, final BoundaryLines boundaryLines, Locale locale, final String testFile) {
        this.factory = factory;
        this.outputFormat = outputFormat;
        this.fontFamily = fontFamily;
        this.boundaryLines = boundaryLines;
        this.additionalPrintMargin = false;
        this.locale = locale;
        this.testFile = testFile;
    }

    PaymentPartReceiptTestsBase(final IOutputWriterFactory factory, final OutputFormat outputFormat, final FontFamily fontFamily, final BoundaryLines boundaryLines, final Boolean additionalPrintMargin, Locale locale, final String testFile) {
        this.factory = factory;
        this.outputFormat = outputFormat;
        this.fontFamily = fontFamily;
        this.boundaryLines = boundaryLines;
        this.additionalPrintMargin = additionalPrintMargin;
        this.locale = locale;
        this.testFile = testFile;
    }

    @Before
    public void init() {
        final String canonicalName = IOutputWriterFactory.class.getCanonicalName();
        System.getProperties().setProperty(canonicalName, factory.getClass().getCanonicalName());

        // verify that we do not have any dependency which prevents execution in headless environment
        System.setProperty("java.awt.headless", "true");
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "true");
    }

    File writeTestResultFile(final byte[] result) throws IOException {
        final String filename = buildTestOutputFileName();
        final File file = testOutputFile(filename);
        FileUtils.writeByteArrayToFile(file, result);
        return file;
    }

    String buildTestOutputFileName() {
        final String postfix = "_" + locale.getLanguage() +
                "_" + fontFamily.name().toLowerCase() +
                "_" + boundaryLines.name().toLowerCase() +
                (additionalPrintMargin ? ("_add_margin") : "") +
                "." + outputFormat.getFileExtension();
        return SpcSamplesRegistry.filename(testFile, postfix);
    }

    File testOutputFile(final String... paths) {
        return TargetTestFileHelper.testOutput(outputFormat, new String[]{"PaymentPartReceipt", getClass().getSimpleName().replace("PaymentPartTests", ""), factory.getShortName(), locale.getLanguage()}, paths);
    }

    // TODO improve, maybe only on CI or install fonts on CI before running tests.
    protected static List<FontFamily> getAllAvailableFontFamilies() {
        if (System.getProperty("BITBUCKET_CI") != null) {
            return singletonList(FontFamily.LIBERATION_SANS);
        } else {
            return asList(FontFamily.values());
        }
    }
}
