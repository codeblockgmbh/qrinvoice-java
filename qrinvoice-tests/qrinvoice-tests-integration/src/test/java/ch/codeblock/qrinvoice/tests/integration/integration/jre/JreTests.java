/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.jre;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.QrInvoicePaymentPartReceiptCreator;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.tests.resources.SpcSample;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import ch.codeblock.qrinvoice.model.mapper.SwissPaymentsCodeToModelMapper;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.tests.integration.testfiles.TargetTestFileHelper;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class JreTests {

    @Rule
    public TestName testName = new TestName();

    @Before
    public void init() {
        // verify that we do not have any dependency which prevents execution in headless environment
        System.setProperty("java.awt.headless", "true");
    }

    @Test
    public void testQrInvoicePdf() throws IOException {
        final SpcSample testSample = SpcSamplesRegistry.defaultSpc();
        final OutputFormat outputFormat = OutputFormat.PDF;

        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(SpcSamplesRegistry.getFileContent(testSample));
        final QrInvoice qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);

        final PaymentPartReceipt result = QrInvoicePaymentPartReceiptCreator.create()
                .qrInvoice(qrInvoice)
                .outputFormat(outputFormat)
                .pageSize(PageSize.A4)
                .fontFamily(FontFamily.LIBERATION_SANS)
                .locale(Locale.GERMAN)
                .createPaymentPartReceipt();

        writeTestResultFile(result.getData(), outputFormat);
    }


    File writeTestResultFile(final byte[] result, final OutputFormat outputFormat) throws IOException {
        final String filename = testName.getMethodName() + "_" + getJavaVersion() + "." + outputFormat.getFileExtension();
        final File file = TargetTestFileHelper.testOutput(outputFormat, filename);
        FileUtils.writeByteArrayToFile(file, result);
        return file;
    }

    String getJavaVersion() {
        String javaVersion = System.getProperty("java.version");
        if (javaVersion.startsWith("1.8")) {
            return "8";
        }
        return javaVersion.substring(0, javaVersion.indexOf('.'));
    }

}
