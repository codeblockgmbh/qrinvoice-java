/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.QrInvoicePaymentPartReceiptCreator;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import ch.codeblock.qrinvoice.model.mapper.SwissPaymentsCodeToModelMapper;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.tests.integration.testfiles.TargetTestFileHelper;
import com.itextpdf.text.DocumentException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

@Deprecated
// remove after proper test integration
public class JavaGraphicsTest {

    private final Locale locale = Locale.GERMAN;
    private final String testFile;

    public JavaGraphicsTest() {
        this.testFile = SpcSamplesRegistry.validDataFilePaths().iterator().next();
    }


    @Test
    public void testQrInvoicePng() throws IOException, DocumentException {
        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(SpcSamplesRegistry.getFileContent(testFile));
        final QrInvoice qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);

        final long start = System.currentTimeMillis();
        final PaymentPartReceipt png = QrInvoicePaymentPartReceiptCreator.create()
                .qrInvoice(qrInvoice)
                .outputFormat(OutputFormat.PNG)
                .pageSize(PageSize.DIN_LANG)
                .locale(locale)
                .createPaymentPartReceipt();
        System.out.println("time: " + (System.currentTimeMillis() - start));

        final String filename = SpcSamplesRegistry.filename(testFile, "_" + locale.getLanguage() + ".png");
        final File file = testOutputFile(OutputFormat.PNG, filename);
        FileUtils.writeByteArrayToFile(file, png.getData());
    }

    @Test
    public void testQrInvoiceGif() throws IOException, DocumentException {
        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(SpcSamplesRegistry.getFileContent(testFile));
        final QrInvoice qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);

        final long start = System.currentTimeMillis();
        final PaymentPartReceipt png = QrInvoicePaymentPartReceiptCreator.create()
                .qrInvoice(qrInvoice)
                .outputFormat(OutputFormat.GIF)
                .pageSize(PageSize.DIN_LANG)
                .locale(locale)
                .createPaymentPartReceipt();
        System.out.println("time: " + (System.currentTimeMillis() - start));

        final String filename = SpcSamplesRegistry.filename(testFile, "_" + locale.getLanguage() + ".gif");
        final File file = testOutputFile(OutputFormat.GIF, filename);
        FileUtils.writeByteArrayToFile(file, png.getData());
    }

    @Test
    public void testQrInvoiceTiff() throws IOException, DocumentException {
        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(SpcSamplesRegistry.getFileContent(testFile));
        final QrInvoice qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);

        final long start = System.currentTimeMillis();
        final PaymentPartReceipt png = QrInvoicePaymentPartReceiptCreator.create()
                .qrInvoice(qrInvoice)
                .outputFormat(OutputFormat.TIFF)
                .pageSize(PageSize.DIN_LANG)
                .locale(locale)
                .createPaymentPartReceipt();
        System.out.println("time: " + (System.currentTimeMillis() - start));

        final String filename = SpcSamplesRegistry.filename(testFile, "_" + locale.getLanguage() + ".tiff");
        final File file = testOutputFile(OutputFormat.TIFF, filename);
        FileUtils.writeByteArrayToFile(file, png.getData());
    }

    private File testOutputFile(final OutputFormat outputFormat, final String... paths) {
        return TargetTestFileHelper.testOutput(outputFormat, new String[]{"PaymentPartReceipt", "DIN_LANG", "Java2d", locale.getLanguage()}, paths);
    }
}
