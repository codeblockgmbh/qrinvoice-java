/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.qrcode;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.infrastructure.IOutputWriterFactory;
import ch.codeblock.qrinvoice.infrastructure.ServiceProvider;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import ch.codeblock.qrinvoice.tests.Triple;
import ch.codeblock.qrinvoice.tests.integration.CombinationHelper;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.List;

import static ch.codeblock.qrinvoice.tests.Combinator.combinationList;
import static java.util.Arrays.asList;

@RunWith(Parameterized.class)
public class QrInvoiceCodeCreatorAllFormatsTests extends QrInvoiceCodeCreatorTestsBase {
    @Parameterized.Parameters(name = "Factory={0} OutputFormat={1} Testfile={2}")
    public static Object[][] data() {
        final List<Triple<IOutputWriterFactory, OutputFormat, String>> triples = combinationList(
                ServiceProvider.getInstance().getAll(IOutputWriterFactory.class),
                asList(OutputFormat.values()),
                asList(SpcSamplesRegistry.defaultSpc().getFilePath())
        );
        return CombinationHelper.filterFactoriesMatchingFormat3(triples);
    }

    public QrInvoiceCodeCreatorAllFormatsTests(final IOutputWriterFactory factory, final OutputFormat outputFormat, final String testFile) {
        super(factory, OutputResolution.LOW_150_DPI, outputFormat, testFile);
    }

}
