/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.paymentpart;

import ch.codeblock.qrinvoice.*;
import ch.codeblock.qrinvoice.infrastructure.JavaGraphicsOutputWriterFactory;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.mapper.SwissPaymentsCodeToModelMapper;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class DinLangPaymentPartReceiptDPITest extends PaymentPartReceiptTestsBase {
    private final OutputResolution outputResolution;

    public DinLangPaymentPartReceiptDPITest(OutputResolution outputResolution) {
        super(new JavaGraphicsOutputWriterFactory(), OutputFormat.JPEG, FontFamily.LIBERATION_SANS, Locale.GERMAN, SpcSamplesRegistry.defaultSpc().getFilePath());
        this.outputResolution = outputResolution;
    }

    @Parameterized.Parameters(name = "OutputResolution={0}")
    public static Object[][] data() {
        return Arrays.stream(OutputResolution.values()).map(e -> new Object[]{e}).toArray(Object[][]::new);
    }

    @Test
    public void testQrInvoice() throws IOException {
        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(SpcSamplesRegistry.getFileContent(testFile));
        final QrInvoice qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);

        final PaymentPartReceipt result = QrInvoicePaymentPartReceiptCreator.create()
                .qrInvoice(qrInvoice)
                .outputFormat(outputFormat)
                .pageSize(PageSize.DIN_LANG)
                .fontFamily(fontFamily)
                .locale(locale)
                .outputResolution(outputResolution)
                .createPaymentPartReceipt();

        switch (outputResolution) {
            case LOW_150_DPI:
                assertEquals(1240, result.getWidth().intValue());
                assertEquals(620, result.getHeight().intValue());
                break;
            case MEDIUM_300_DPI:
                assertEquals(2480, result.getWidth().intValue());
                assertEquals(1240, result.getHeight().intValue());
                break;
            case HIGH_600_DPI:
                assertEquals(4961, result.getWidth().intValue());
                assertEquals(2480, result.getHeight().intValue());
                break;
            default:
                Assert.fail("Unknown OutputResolution");
        }

        writeTestResultFile(result.getData());
    }


    @Override
    String buildTestOutputFileName() {
        final String filename = super.buildTestOutputFileName();
        final int i = filename.lastIndexOf('.');
        return filename.substring(0, i) + "_" + outputResolution + filename.substring(i);
    }
}
