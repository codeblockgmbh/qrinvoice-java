/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.paymentpart;

import ch.codeblock.qrinvoice.*;
import ch.codeblock.qrinvoice.infrastructure.IOutputWriterFactory;
import ch.codeblock.qrinvoice.infrastructure.ServiceProvider;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.mapper.SwissPaymentsCodeToModelMapper;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.paymentpartreceipt.LayoutDefinitions;
import ch.codeblock.qrinvoice.tests.Quintuple;
import ch.codeblock.qrinvoice.tests.integration.CombinationHelper;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static ch.codeblock.qrinvoice.tests.Combinator.combinationList;
import static java.util.Arrays.asList;

@RunWith(Parameterized.class)
public class DinLangPaymentPartReceiptAllSPCTests extends PaymentPartReceiptTestsBase {
    public DinLangPaymentPartReceiptAllSPCTests(final IOutputWriterFactory factory, final OutputFormat outputFormat, final FontFamily fontFamily, final Locale locale, final String testFile) {
        super(factory, outputFormat, fontFamily, locale, testFile);
    }

    @Parameterized.Parameters(name = "Factory={0} OutputFormat={1} FontFamily={2} Locale={3} Testfile={4}")
    public static Object[][] data() {
        final List<Quintuple<IOutputWriterFactory, OutputFormat, FontFamily, Locale, String>> quintuples = combinationList(
                ServiceProvider.getInstance().getAll(IOutputWriterFactory.class),
                asList(OutputFormat.PDF, OutputFormat.JPEG),
                getAllAvailableFontFamilies(),
                LayoutDefinitions.SUPPORTED_LOCALES,
                SpcSamplesRegistry.validDataFilePaths()
        );
        return CombinationHelper.filterFactoriesMatchingFormat5(quintuples);
    }
    @Test
    public void testQrInvoice() throws IOException {
        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(SpcSamplesRegistry.getFileContent(testFile));
        final QrInvoice qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);

        final PaymentPartReceipt result = QrInvoicePaymentPartReceiptCreator.create()
                .qrInvoice(qrInvoice)
                .outputFormat(outputFormat)
                .pageSize(PageSize.DIN_LANG)
                .fontFamily(fontFamily)
                .locale(locale)
                .outputResolution(OutputResolution.LOW_150_DPI)
                .createPaymentPartReceipt();

        writeTestResultFile(result.getData());
    }
}
