/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.documentscanner;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.QrInvoiceDocumentScanner;
import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.transform.QrInvoiceStringTransformer;
import ch.codeblock.qrinvoice.model.validation.InvalidCharacterSequence;
import ch.codeblock.qrinvoice.model.validation.QrInvoiceValidator;
import ch.codeblock.qrinvoice.model.validation.ValidationUtils;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.*;

public class InvalidExampleScanningWithStringTransformationExample {
    @Test
    public void testTransformation() throws IOException {
        final List<QrInvoice> qrInvoices = QrInvoiceDocumentScanner.create(MimeType.PNG)
                .scanDocumentForAllSwissQrCodes(getClass().getResourceAsStream("/samples/qrcode/qrcode-invalid-character.png"));
        assertEquals(1, qrInvoices.size());

        for (final QrInvoice qrInvoice : qrInvoices) {
            System.out.println("Original: " + qrInvoice);
            assertFalse(QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(qrInvoice).isValid());

            QrInvoiceStringTransformer
                    .create(InvalidExampleScanningWithStringTransformationExample::replaceInvalidCharactersWithQuestionMark)
                    .transform(qrInvoice);

            System.out.println("Transformed: " + qrInvoice);
            assertTrue(QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(qrInvoice).isValid());
        }
    }

    private static String replaceInvalidCharactersWithQuestionMark(String str) {
        String result = str;
        for (InvalidCharacterSequence invalidCharacterSequence : ValidationUtils.validateCharacters(result, ImplementationGuidelinesQrBillVersion.V2_2).getInvalidCharacterSequences()) {
            final int len = invalidCharacterSequence.getInvalidCharSequence().length();
            final int start = invalidCharacterSequence.getInvalidCharSequenceStartIndex();
            result = result.substring(0, start)
                    + StringUtils.leftPad("?", len, '?')
                    + result.substring(start + len);
        }
        if (!Objects.equals(str, result)) {
            System.out.printf("Transformed '%s' to '%s'%n", str, result);
        }
        return result;
    }

}
