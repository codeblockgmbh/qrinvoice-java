/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.qrcode;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.QrInvoiceCodeCreator;
import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.output.QrCode;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import ch.codeblock.qrinvoice.tests.integration.testfiles.TargetTestFileHelper;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertNotNull;

public class QrInvoiceCodeCreatorTest {

    @Before
    public void init() {
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "true");
    }

    @Test
    public void testQrCode() throws IOException {
        final QrCode result = QrInvoiceCodeCreator.create()
                .qrInvoice(create())
                .desiredQrCodeSize(500)
                .outputFormat(OutputFormat.PNG)
                .createQrCode();

        assertNotNull(result);
        assertNotNull(result.getData());

        final String filename = SpcSamplesRegistry.filename(QrInvoiceCodeCreatorTest.class.getSimpleName(), ".png");
        final File file = TargetTestFileHelper.testOutput(OutputFormat.PNG, "QrCode", filename);
        FileUtils.writeByteArrayToFile(file, result.getData());
    }

    @Test
    public void testQrCodePDF() throws IOException {
        final QrCode result = QrInvoiceCodeCreator.create()
                .qrInvoice(create())
                .outputFormat(OutputFormat.PDF)
                .createQrCode();

        assertNotNull(result);
        assertNotNull(result.getData());

        final String filename = SpcSamplesRegistry.filename(QrInvoiceCodeCreatorTest.class.getSimpleName(), ".pdf");
        final File file = TargetTestFileHelper.testOutput(OutputFormat.PNG, "QrCode", filename);
        FileUtils.writeByteArrayToFile(file, result.getData());
    }

    @Test
    public void testQrCodePDFLowResolution() throws IOException {
        final QrCode result = QrInvoiceCodeCreator.create()
                .qrInvoice(create())
                .outputResolution(OutputResolution.LOW_150_DPI)
                .outputFormat(OutputFormat.PDF)
                .createQrCode();

        assertNotNull(result);
        assertNotNull(result.getData());

        final String filename = SpcSamplesRegistry.filename(QrInvoiceCodeCreatorTest.class.getSimpleName(), ".pdf");
        final File file = TargetTestFileHelper.testOutput(OutputFormat.PNG, "QrCode", filename);
        FileUtils.writeByteArrayToFile(file, result.getData());
    }

    @Test(expected = ValidationException.class)
    public void testQrCodePDFWithDesiredSize() throws IOException {
        final QrCode result = QrInvoiceCodeCreator.create()
                .qrInvoice(create())
                .desiredQrCodeSize(500)
                .outputFormat(OutputFormat.PDF)
                .createQrCode();
    }


    @Test
    public void testQrCodeSpc() throws IOException {
        final String swissPaymentsCode = QrInvoiceCodeCreator.create()
                .qrInvoice(create())
                .createSwissPaymentsCode();
        assertNotNull(swissPaymentsCode);

        final String filename = SpcSamplesRegistry.filename(QrInvoiceCodeCreatorTest.class.getSimpleName(), ".txt");
        final File file = TargetTestFileHelper.testOutputTxt("SwissPaymentsCode", filename);
        FileUtils.write(file, swissPaymentsCode, StandardCharsets.UTF_8);
    }

    @Test
    public void testQrCodeMaxSize() {
        final QrCode result = QrInvoiceCodeCreator.create()
                .qrInvoice(create())
                .desiredQrCodeSize(10_000)
                .outputFormat(OutputFormat.PNG)
                .createQrCode();
        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test(expected = ValidationException.class)
    public void testQrCodeTooBig() {
        QrInvoiceCodeCreator.create()
                .qrInvoice(create())
                .desiredQrCodeSize(10_000 + 1)
                .outputFormat(OutputFormat.PNG)
                .createQrCode();
    }

    @Test(expected = ValidationException.class)
    public void testQrCodeSizeAndResolution() {
        QrInvoiceCodeCreator.create()
                .qrInvoice(create())
                .desiredQrCodeSize(500)
                .outputResolution(OutputResolution.MEDIUM_300_DPI)
                .outputFormat(OutputFormat.PNG)
                .createQrCode();
    }


    private QrInvoice create() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .structuredAddress()
                        .name("Robert Schneider Services Switzerland AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268/3/1")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .creditorReference("RF18539007547034")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(null)
                .build();
    }
}
