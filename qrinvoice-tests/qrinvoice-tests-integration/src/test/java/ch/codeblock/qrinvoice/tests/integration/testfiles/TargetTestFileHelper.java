/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.testfiles;

import ch.codeblock.qrinvoice.OutputFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class TargetTestFileHelper {
    private static final Logger logger = LoggerFactory.getLogger(TargetTestFileHelper.class);
    private static final Map<OutputFormat, File> TEST_OUTPUT_DIRS = new HashMap<>();
    private static final File testOutputDir;
    private static final File testTxtOutputDir;

    static {
        try {

        String relPath = TargetTestFileHelper.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        relPath = URLDecoder.decode(relPath, "UTF-8");
        testOutputDir = createDir(new File(relPath + "/../../target/tests-output"));
        for (final OutputFormat outputFormat : OutputFormat.values()) {
            TEST_OUTPUT_DIRS.put(outputFormat, createDir(new File(testOutputDir, outputFormat.getFileExtension())));
        }
        testTxtOutputDir = createDir(new File(testOutputDir, "txt"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static File testOutput(final OutputFormat outputFormat, final String[] paths1, final String... paths2) {
        return testOutput(outputFormat, Stream.concat(Arrays.stream(paths1), Arrays.stream(paths2)).toArray(String[]::new));
    }

    public static File testOutput(final OutputFormat outputFormat, final String... paths) {
        return testOutput(TEST_OUTPUT_DIRS.get(outputFormat), paths);
    }

    public static File testOutputTxt(final String[] paths1, final String... paths2) {
        return testOutputTxt(Stream.concat(Arrays.stream(paths1), Arrays.stream(paths2)).toArray(String[]::new));
    }

    public static File testOutputTxt(final String... paths) {
        return testOutput(testTxtOutputDir, paths);
    }

    public static File testOutput(final File parent, final String... paths) {
        final String filename = paths[paths.length - 1];
        File parentDir = parent;
        for (int i = 0; i < paths.length - 1; i++) {
            // all execpt last
            parentDir = createDir(new File(parentDir, paths[i]));
        }

        final File file = new File(parentDir, filename);
        logger.debug(file.getAbsolutePath());
        return file;
    }

    public static File createDir(final File dir) {
        if (!dir.exists() && !dir.mkdirs()) {
            throw new RuntimeException("error creating dir tree");
        }
        logger.trace(dir.getAbsolutePath());
        return dir;
    }

}
