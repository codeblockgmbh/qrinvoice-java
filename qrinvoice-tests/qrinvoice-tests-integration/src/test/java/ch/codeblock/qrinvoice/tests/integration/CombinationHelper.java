/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.infrastructure.IOutputWriterFactory;
import ch.codeblock.qrinvoice.paymentpartreceipt.BoundaryLines;
import ch.codeblock.qrinvoice.paymentpartreceipt.PaymentPartReceiptWriterOptions;
import ch.codeblock.qrinvoice.tests.*;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class CombinationHelper {

    public static Object[][] filterFactoriesMatchingFormat7(final List<Septuple<IOutputWriterFactory, OutputFormat, FontFamily, BoundaryLines, Boolean, Locale, String>> septuples) {
        // need to filter list because not all IOutputWriterFactories are suited for a specific output format
        final List<Septuple<IOutputWriterFactory, OutputFormat, FontFamily, BoundaryLines, Boolean, Locale, String>> filteredList = septuples.stream().filter(q -> {
            final PaymentPartReceiptWriterOptions options = new PaymentPartReceiptWriterOptions();
            options.setOutputFormat(q.getSecond());
            options.setBoundaryLines(q.getFourth());
            return q.getFirst().supports(options);
        }).collect(Collectors.toList());

        return Combinator.septuplesToArray(filteredList);
    }

    
    public static Object[][] filterFactoriesMatchingFormat6A(final List<Sextuple<IOutputWriterFactory, OutputFormat, FontFamily, BoundaryLines, Locale, String>> sextuples) {
        // need to filter list because not all IOutputWriterFactories are suited for a specific output format
        final List<Sextuple<IOutputWriterFactory, OutputFormat, FontFamily, BoundaryLines, Locale, String>> filteredList = sextuples.stream().filter(q -> {
            final PaymentPartReceiptWriterOptions options = new PaymentPartReceiptWriterOptions();
            options.setOutputFormat(q.getSecond());
            options.setBoundaryLines(q.getFourth());
            return q.getFirst().supports(options);
        }).collect(Collectors.toList());

        return Combinator.sextuplesToArray(filteredList);
    }

    
    public static Object[][] filterFactoriesMatchingFormat6B(final List<Sextuple<IOutputWriterFactory, OutputFormat, OutputResolution, FontFamily, Locale, String>> sextuples) {
        // need to filter list because not all IOutputWriterFactories are suited for a specific output format
        final List<Sextuple<IOutputWriterFactory, OutputFormat, OutputResolution, FontFamily, Locale, String>> filteredList = sextuples.stream().filter(q -> {
            final PaymentPartReceiptWriterOptions options = new PaymentPartReceiptWriterOptions();
            options.setOutputFormat(q.getSecond());
            options.setFontFamily(q.getFourth());
            return q.getFirst().supports(options);
        }).collect(Collectors.toList());

        return Combinator.sextuplesToArray(filteredList);
    }

    public static Object[][] filterFactoriesMatchingFormat5(final List<Quintuple<IOutputWriterFactory, OutputFormat, FontFamily, Locale, String>> quintuples) {
        // need to filter list because not all IOutputWriterFactories are suited for a specific output format
        final List<Quintuple<IOutputWriterFactory, OutputFormat, FontFamily, Locale, String>> filteredList = quintuples.stream().filter(q -> {
            final PaymentPartReceiptWriterOptions options = new PaymentPartReceiptWriterOptions();
            options.setOutputFormat(q.getSecond());
            return q.getFirst().supports(options);
        }).collect(Collectors.toList());

        return Combinator.quintuplesToArray(filteredList);
    }

    public static Object[][] filterFactoriesMatchingFormat4(final List<Quadruple<IOutputWriterFactory, OutputFormat, FontFamily, Locale>> quadruples) {
        // need to filter list because not all IOutputWriterFactories are suited for a specific output format
        final List<Quadruple<IOutputWriterFactory, OutputFormat, FontFamily, Locale>> filteredList = quadruples.stream().filter(q -> {
            final PaymentPartReceiptWriterOptions options = new PaymentPartReceiptWriterOptions();
            options.setOutputFormat(q.getSecond());
            return q.getFirst().supports(options);
        }).collect(Collectors.toList());

        return Combinator.quadruplesToArray(filteredList);
    }
    
    public static Object[][] filterFactoriesMatchingFormat3(final List<Triple<IOutputWriterFactory, OutputFormat, String>> triples) {
        // need to filter list because not all IOutputWriterFactories are suited for a specific output format
        final List<Triple<IOutputWriterFactory, OutputFormat, String>> filteredList = triples.stream().filter(q -> {
            final PaymentPartReceiptWriterOptions options = new PaymentPartReceiptWriterOptions();
            options.setOutputFormat(q.getSecond());
            return q.getFirst().supports(options);
        }).collect(Collectors.toList());

        return Combinator.triplesToArray(filteredList);
    }
}
