/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.qrcode;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.QrInvoiceCodeCreator;
import ch.codeblock.qrinvoice.QrInvoiceDocumentScanner;
import ch.codeblock.qrinvoice.infrastructure.IOutputWriterFactory;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.mapper.SwissPaymentsCodeToModelMapper;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.output.QrCode;
import ch.codeblock.qrinvoice.qrcode.QrCodeReaderOptions;
import ch.codeblock.qrinvoice.tests.integration.testfiles.TargetTestFileHelper;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public abstract class QrInvoiceCodeCreatorTestsBase {

    final IOutputWriterFactory factory;
    final OutputFormat outputFormat;
    final String testFile;
    final OutputResolution outputResolution;
    final String spc;
    private final QrInvoice qrInvoice;

    public QrInvoiceCodeCreatorTestsBase(final IOutputWriterFactory factory, final OutputResolution outputResolution, final OutputFormat outputFormat, final String testFile) {
        this.factory = factory;
        this.outputResolution = outputResolution;
        this.outputFormat = outputFormat;
        this.testFile = testFile;
        this.spc = SpcSamplesRegistry.getFileContent(testFile);

        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(spc);
        this.qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);
    }

    @Before
    public void init() {
        final String canonicalName = IOutputWriterFactory.class.getCanonicalName();
        System.getProperties().setProperty(canonicalName, factory.getClass().getCanonicalName());

        // verify that we do not have any dependency which prevents execution in headless environment
        System.setProperty("java.awt.headless", "true");
    }

    @Test
    public void testQrInvoice() throws IOException {
        final QrCode result;
        if (OutputFormat.PDF.equals(outputFormat)) {
            result = QrInvoiceCodeCreator.create()
                    .qrInvoice(qrInvoice)
                    .outputResolution(outputResolution)
                    .outputFormat(outputFormat)
                    .createQrCode();
        } else {
            result = QrInvoiceCodeCreator.create()
                    .qrInvoice(qrInvoice)
                    .desiredQrCodeSize(500)
                    .outputFormat(outputFormat)
                    .createQrCode();
        }

        assertNotNull(result);
        assertNotNull(result.getData());

        final File file = writeTestResultFile(result.getData());

        final QrInvoice scannedQrInvoice = QrInvoiceDocumentScanner.create(outputFormat.getMimeType(), QrCodeReaderOptions.DEFAULT).scanDocumentUntilFirstSwissQrCode(result.getData()).get();
        assertEquals(qrInvoice, scannedQrInvoice);
    }

    File writeTestResultFile(final byte[] result) throws IOException {
        final String filename = buildTestOutputFileName();
        final File file = testOutputFile(filename);
        FileUtils.writeByteArrayToFile(file, result);
        return file;
    }

    String buildTestOutputFileName() {
        return SpcSamplesRegistry.filename(testFile, "." + outputFormat.getFileExtension());
    }

    File testOutputFile(final String... paths) {
        return TargetTestFileHelper.testOutput(outputFormat, new String[]{"QrCode", getClass().getSimpleName(), factory.getShortName()}, paths);
    }
}
