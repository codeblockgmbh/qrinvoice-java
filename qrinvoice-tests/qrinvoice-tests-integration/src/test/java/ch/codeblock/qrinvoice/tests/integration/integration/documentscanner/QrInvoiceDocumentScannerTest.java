/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.documentscanner;

import ch.codeblock.qrinvoice.QrInvoiceDocumentScanner;
import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.validation.QrInvoiceValidator;
import ch.codeblock.qrinvoice.model.validation.ValidationOptions;
import ch.codeblock.qrinvoice.qrcode.DecodeException;
import ch.codeblock.qrinvoice.qrcode.QrCodeLibrary;
import ch.codeblock.qrinvoice.qrcode.QrCodeReaderOptions;
import ch.codeblock.qrinvoice.qrcode.ScanningEffortLevel;
import ch.codeblock.qrinvoice.tests.resources.QrInvoiceSample;
import ch.codeblock.qrinvoice.tests.resources.QrInvoiceSampleMatch;
import ch.codeblock.qrinvoice.tests.resources.QrInvoiceSamplesRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static ch.codeblock.qrinvoice.qrcode.QrCodeLibrary.*;
import static ch.codeblock.qrinvoice.tests.Combinator.combinations;
import static java.util.Arrays.asList;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class QrInvoiceDocumentScannerTest {


    @Parameterized.Parameters(name = "{1} {2} - Testfile: {0}")
    public static Object[][] data() {
        return combinations(QrInvoiceSamplesRegistry.data(), asList(ZXING, BOOFCV, ALL), asList(ScanningEffortLevel.values()));
    }

    private final QrInvoiceSample sample;
    private final QrCodeLibrary qrCodeLibrary;
    private final ScanningEffortLevel scanningEffortLevel;
    private final String mimeType;

    public QrInvoiceDocumentScannerTest(QrInvoiceSample sample, QrCodeLibrary qrCodeLibrary, ScanningEffortLevel scanningEffortLevel) {
        this.qrCodeLibrary = qrCodeLibrary;
        this.scanningEffortLevel = scanningEffortLevel;
        this.sample = sample;
        if (sample.getFilePath().endsWith(".pdf")) {
            this.mimeType = "application/pdf";
        } else if (sample.getFilePath().endsWith(".png")) {
            this.mimeType = "image/png";
        } else if (sample.getFilePath().endsWith(".jpg") || sample.getFilePath().endsWith(".jpeg")) {
            this.mimeType = "image/jpeg";
        } else {
            throw new IllegalArgumentException("Mime type no supported for: " + this.sample.getFilePath());
        }

    }

    private QrInvoiceDocumentScanner getDocumentScanner() {
        return QrInvoiceDocumentScanner.create(mimeType, new QrCodeReaderOptions(scanningEffortLevel, qrCodeLibrary));
    }

    @Test
    public void testScanAll() throws IOException {
        try {
            final List<QrInvoice> qrInvoices = getDocumentScanner().scanDocumentForAllSwissQrCodes(QrInvoiceSamplesRegistry.getFileContent((sample.getFilePath())));
            assertEquals(getExpectedSwissQrCodeCount(), qrInvoices.size());
            for (final QrInvoice qrInvoice : qrInvoices) {
                assertQrInvoice(qrInvoice);
            }
        } catch (DecodeException e) {
            assertEquals("QR Code could not be found in the given image", e.getMessage());
            assertEquals(0, getExpectedSwissQrCodeCount());
        }
    }

    private int getExpectedSwissQrCodeCount() {
        QrInvoiceSampleMatch match;
        if (qrCodeLibrary == BOOFCV) {
            match = sample.getBoofcv();
        } else if (qrCodeLibrary == ZXING) {
            match = sample.getZxing();
        } else if (qrCodeLibrary == ALL) {
            // get max
            match = new QrInvoiceSampleMatch(
                    Math.max(sample.getBoofcv().getExpectedSwissQrCodeCountDefault(), sample.getZxing().getExpectedSwissQrCodeCountDefault()),
                    Math.max(sample.getBoofcv().getExpectedSwissQrCodeCountHarder(), sample.getZxing().getExpectedSwissQrCodeCountHarder())
            );
        } else {
            throw new RuntimeException("Unexpected qrCodeLibrary value");
        }

        if (scanningEffortLevel == ScanningEffortLevel.HARDER) {
            return match.getExpectedSwissQrCodeCountHarder();
        } else {
            return match.getExpectedSwissQrCodeCountDefault();
        }
    }

    @Test
    public void testScanFirst() throws IOException {
        final Optional<QrInvoice> qrInvoiceOptional = getDocumentScanner().scanDocumentUntilFirstSwissQrCode(QrInvoiceSamplesRegistry.getFileContent((sample.getFilePath())));
        if (getExpectedSwissQrCodeCount() >= 1) {
            assertTrue("expected to find an Swiss QR Code", qrInvoiceOptional.isPresent());
        } else {
            assertFalse("expected to not find any Swiss QR Code", qrInvoiceOptional.isPresent());
        }
    }

    private void assertQrInvoice(final QrInvoice qrInvoice) {
        final ValidationOptions validationOptions = new ValidationOptions();
        validationOptions.setSkipBillInformationObject(true);
        final boolean valid = QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(qrInvoice, validationOptions).isValid();

        if (!valid && sample.isValid()) {
            fail("Expected sample to be valid");
        } else if (valid && !sample.isValid()) {
            fail("Expected sample to be invalid");
        }
    }

}
