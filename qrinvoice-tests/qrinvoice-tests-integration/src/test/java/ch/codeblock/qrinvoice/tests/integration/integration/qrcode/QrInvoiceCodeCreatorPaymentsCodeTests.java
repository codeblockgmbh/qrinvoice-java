/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.qrcode;

import ch.codeblock.qrinvoice.QrInvoiceCodeCreator;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import ch.codeblock.qrinvoice.model.mapper.SwissPaymentsCodeToModelMapper;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.tests.integration.testfiles.TargetTestFileHelper;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class QrInvoiceCodeCreatorPaymentsCodeTests {
    @Parameterized.Parameters(name = "Testfile: {0}")
    public static Collection<String> testFile() {
        return SpcSamplesRegistry.validDataFilePaths();
    }

    private final String testFile;
    final String spc;
    private final QrInvoice qrInvoice;

    public QrInvoiceCodeCreatorPaymentsCodeTests(final String testFile) {
        this.testFile = testFile;
        this.spc = SpcSamplesRegistry.getFileContent(testFile);

        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(spc);
        this.qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);
    }

    @Test
    public void testSwissPaymentsCode() throws IOException {
        final String swissPaymentsCode = QrInvoiceCodeCreator.create()
                .qrInvoice(qrInvoice)
                .createSwissPaymentsCode();
        assertNotNull(swissPaymentsCode);

        final String filename = SpcSamplesRegistry.filename(testFile, ".txt");
        final File file = TargetTestFileHelper.testOutputTxt("SwissPaymentsCode", filename);
        FileUtils.write(file, swissPaymentsCode, StandardCharsets.UTF_8);

        assertTrue(StringUtils.isNotEmpty(swissPaymentsCode));
        final int lineCount = countLines(swissPaymentsCode);
        assertTrue(lineCount >= 31 && lineCount <= 34);
    }

    private static int countLines(String str) {
        final Matcher matcher = Pattern.compile("\r\n|\r|\n").matcher(str);
        int cnt = 1; // one line exists without line feed
        while (matcher.find()) {
            cnt++;
        }
        return cnt;
    }

}
