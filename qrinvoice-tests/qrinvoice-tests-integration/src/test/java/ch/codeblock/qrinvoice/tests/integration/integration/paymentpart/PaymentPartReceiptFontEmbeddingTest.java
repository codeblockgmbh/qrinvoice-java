/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.integration.paymentpart;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.QrInvoicePaymentPartReceiptCreator;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontLike;
import org.junit.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PaymentPartReceiptFontEmbeddingTest {
    @Test
    public void testEmbeddedLiberationSansFont() throws IOException {
        // arrange
        final QrInvoice qrInvoice = getQrInvoice();

        // act
        final PaymentPartReceipt ppr = createPpr(qrInvoice, FontFamily.LIBERATION_SANS, true);

        // assert
        final Set<PDFont> allFonts = extractAllFonts(ppr.getData());
        assertEquals(2, allFonts.size());
        assertTrue(allFonts.stream().allMatch(PDFontLike::isEmbedded));
    }

    @Test
    public void testUnEmbeddedLiberationSansFont() throws IOException {
        // arrange
        final QrInvoice qrInvoice = getQrInvoice();

        // act
        final PaymentPartReceipt ppr = createPpr(qrInvoice, FontFamily.LIBERATION_SANS, false);

        // assert
        final Set<PDFont> allFonts = extractAllFonts(ppr.getData());
        assertEquals(2, allFonts.size());
        assertTrue(allFonts.stream().noneMatch(PDFontLike::isEmbedded));
    }

    @Test
    public void testEmbeddedHelveticaFont() throws IOException {
        // TODO improve, maybe only on CI or install fonts on CI before running tests.
        if (System.getProperty("BITBUCKET_CI") != null) {
            return;
        }
        // arrange
        final QrInvoice qrInvoice = getQrInvoice();

        // act
        final PaymentPartReceipt ppr = createPpr(qrInvoice, FontFamily.HELVETICA, true);

        // assert
        final Set<PDFont> allFonts = extractAllFonts(ppr.getData());
        assertEquals(2, allFonts.size());
        assertTrue(allFonts.stream().allMatch(PDFontLike::isEmbedded));
    }

    @Test
    public void testUnEmbeddedHelveticaFont() throws IOException {
        // arrange
        final QrInvoice qrInvoice = getQrInvoice();

        // act
        final PaymentPartReceipt ppr = createPpr(qrInvoice, FontFamily.HELVETICA, false);

        // assert
        final Set<PDFont> allFonts = extractAllFonts(ppr.getData());
        assertEquals(2, allFonts.size());
        assertTrue(allFonts.stream().noneMatch(PDFontLike::isEmbedded));
    }

    public Set<PDFont> extractAllFonts(final byte[] data) throws IOException {
        final Set<PDFont> allFonts = new HashSet<>();
        final PDDocument pdDocument = PDDocument.load(data);
        assertEquals(1, pdDocument.getNumberOfPages());
        final PDPage page = pdDocument.getPage(0);
        final PDResources resources = page.getResources();
        final Iterable<COSName> ite = resources.getFontNames();
        for (final COSName name : ite) {
            allFonts.add(resources.getFont(name));
        }
        return allFonts;
    }

    public PaymentPartReceipt createPpr(final QrInvoice qrInvoice, final FontFamily sans, final boolean embeddFont) {
        return QrInvoicePaymentPartReceiptCreator.create()
                .qrInvoice(qrInvoice)
                .outputFormat(OutputFormat.PDF)
                .fontsEmbedded(embeddFont)
                .fontFamily(sans)
                .createPaymentPartReceipt();
    }

    public QrInvoice getQrInvoice() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("tuvwxyz{}~£´ÀÁÂÄÇÈÉÊËÌÍÎÏÑÒÓÔÖÙÚÛÜßàáâäçèéêëìíîïñòóôö÷ùúûüý")
                )
                .build();
    }
}
