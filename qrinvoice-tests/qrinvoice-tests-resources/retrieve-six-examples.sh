#!/bin/bash

rm -rf src/main/resources/samples/qrinvoice/six/
rm -rf src/main/resources/samples/spc/six/

rm -rf six-examples
mkdir -p six-examples/downloads

start_dir=$(pwd)

cd six-examples/downloads || exit

wget https://www.paymentstandards.ch/dam/downloads/muster-qr-zahlteile-de.zip
wget https://www.paymentstandards.ch/dam/downloads/muster-qr-zahlteile-fr.zip
wget https://www.paymentstandards.ch/dam/downloads/muster-qr-zahlteile-it.zip
wget https://www.paymentstandards.ch/dam/downloads/muster-qr-zahlteile-en.zip

wget https://www.paymentstandards.ch/dam/downloads/datenschema-zu-mustern-txt-dateien.zip
wget https://www.paymentstandards.ch/dam/downloads/datenschema-zu-mustern-txt-dateien-fr.zip
wget https://www.paymentstandards.ch/dam/downloads/datenschema-zu-mustern-txt-dateien-it.zip
wget https://www.paymentstandards.ch/dam/downloads/datenschema-zu-mustern-txt-dateien-en.zip

cd $start_dir || exit

ditto -xk six-examples/downloads/muster-qr-zahlteile-de.zip src/main/resources/samples/qrinvoice/six/
ditto -xk six-examples/downloads/muster-qr-zahlteile-fr.zip src/main/resources/samples/qrinvoice/six/
ditto -xk six-examples/downloads/muster-qr-zahlteile-it.zip src/main/resources/samples/qrinvoice/six/
ditto -xk six-examples/downloads/muster-qr-zahlteile-en.zip src/main/resources/samples/qrinvoice/six/

ditto -xk six-examples/downloads/datenschema-zu-mustern-txt-dateien.zip src/main/resources/samples/spc/six/
ditto -xk six-examples/downloads/datenschema-zu-mustern-txt-dateien-fr.zip src/main/resources/samples/spc/six/
ditto -xk six-examples/downloads/datenschema-zu-mustern-txt-dateien-it.zip src/main/resources/samples/spc/six/
ditto -xk six-examples/downloads/datenschema-zu-mustern-txt-dateien-en.zip src/main/resources/samples/spc/six/

find src/main/resources/samples/**/six -depth -name '* *' \
  -execdir bash -c 'mv -- "$1" "${1//î/oe}"' bash {} \;

find src/main/resources/samples/**/six -depth -name '* *' \
  -execdir bash -c 'mv -- "$1" "${1// /_}"' bash {} \;

find src/main/resources/samples/**/six -depth -name '*.*' \
  -execdir bash -c 'mv -- "$1" "${1//__/_}"' bash {} \;

find src/main/resources/samples/qrinvoice/six -depth -name '*.*' \
  -execdir bash -c 'mv -- "$1" "${1//Nr./SIX_example}"' bash {} \;
  
find src/main/resources/samples/spc/six -depth -name '*.*' \
  -execdir bash -c 'mv -- "$1" "${1//Nr./spc_SIX_example}"' bash {} \;

mv src/main/resources/samples/qrinvoice/six/muster*/* src/main/resources/samples/qrinvoice/six

mv src/main/resources/samples/spc/six/datenschema*/* src/main/resources/samples/spc/six
mv src/main/resources/samples/spc/six/* src/main/resources/samples/spc

rm -rf src/main/resources/samples/spc/datenschema*
rm -rf src/main/resources/samples/spc/six/
rm -rf src/main/resources/samples/qrinvoice/six/muster-qr-zahlteile*
