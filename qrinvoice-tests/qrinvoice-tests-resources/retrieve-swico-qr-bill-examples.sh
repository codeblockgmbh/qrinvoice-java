#!/bin/bash

rm -rf swico-qr-bill

git clone https://github.com/swico/qr-bill.git swico-qr-bill
rm -rf swico-qr-bill/.git

mkdir -p src/main/resources/samples/qrinvoice/swico
cp -r swico-qr-bill/invoices/* src/main/resources/samples/qrinvoice/swico

ls -al src/main/resources/samples/qrinvoice/swico