/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Combinator {
    public static <T1, T2> List<Tuple<T1, T2>> combinationList(final Collection<T1> firstCollection, final Collection<T2> secondCollection) {
        final List<Tuple<T1, T2>> result = new ArrayList<>(firstCollection.size() * secondCollection.size());
        for (final T1 t1 : firstCollection) {
            for (final T2 t2 : secondCollection) {
                result.add(new Tuple<>(t1, t2));
            }
        }

        return result;
    }

    public static <T1, T2, T3> List<Triple<T1, T2, T3>> combinationList(final Collection<T1> firstCollection, final Collection<T2> secondCollection, final Collection<T3> thirdCollection) {
        final List<Triple<T1, T2, T3>> result = new ArrayList<>(firstCollection.size() * secondCollection.size() * thirdCollection.size());
        for (final T1 t1 : firstCollection) {
            for (final T2 t2 : secondCollection) {
                for (final T3 t3 : thirdCollection) {
                    result.add(new Triple<>(t1, t2, t3));
                }
            }
        }

        return result;
    }

    public static <T1, T2, T3, T4> List<Quadruple<T1, T2, T3, T4>> combinationList(final Collection<T1> firstCollection, final Collection<T2> secondCollection, final Collection<T3> thirdCollection, final Collection<T4> fourthCollection) {
        final List<Quadruple<T1, T2, T3, T4>> result = new ArrayList<>(firstCollection.size() * secondCollection.size() * thirdCollection.size() * fourthCollection.size());
        for (final T1 t1 : firstCollection) {
            for (final T2 t2 : secondCollection) {
                for (final T3 t3 : thirdCollection) {
                    for (final T4 t4 : fourthCollection) {
                        result.add(new Quadruple<>(t1, t2, t3, t4));
                    }
                }
            }
        }

        return result;
    }

    public static <T1, T2, T3, T4, T5> List<Quintuple<T1, T2, T3, T4, T5>> combinationList(final Collection<T1> firstCollection, final Collection<T2> secondCollection, final Collection<T3> thirdCollection, final Collection<T4> fourthCollection, final Collection<T5> fifthCollection) {
        final List<Quintuple<T1, T2, T3, T4, T5>> result = new ArrayList<>(firstCollection.size() * secondCollection.size() * thirdCollection.size() * fourthCollection.size() * firstCollection.size());
        for (final T1 t1 : firstCollection) {
            for (final T2 t2 : secondCollection) {
                for (final T3 t3 : thirdCollection) {
                    for (final T4 t4 : fourthCollection) {
                        for (final T5 t5 : fifthCollection) {
                            result.add(new Quintuple<>(t1, t2, t3, t4, t5));
                        }
                    }
                }
            }
        }

        return result;
    }

    public static <T1, T2, T3, T4, T5, T6> List<Sextuple<T1, T2, T3, T4, T5, T6>> combinationList(final Collection<T1> firstCollection, final Collection<T2> secondCollection, final Collection<T3> thirdCollection, final Collection<T4> fourthCollection, final Collection<T5> fifthCollection, final Collection<T6> sixthCollection) {
        final List<Sextuple<T1, T2, T3, T4, T5, T6>> result = new ArrayList<>(firstCollection.size() * secondCollection.size() * thirdCollection.size() * fourthCollection.size() * firstCollection.size() * sixthCollection.size());
        for (final T1 t1 : firstCollection) {
            for (final T2 t2 : secondCollection) {
                for (final T3 t3 : thirdCollection) {
                    for (final T4 t4 : fourthCollection) {
                        for (final T5 t5 : fifthCollection) {
                            for (final T6 t6 : sixthCollection) {
                                result.add(new Sextuple<>(t1, t2, t3, t4, t5, t6));
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    public static <T1, T2, T3, T4, T5, T6, T7> List<Septuple<T1, T2, T3, T4, T5, T6, T7>> combinationList(final Collection<T1> firstCollection, final Collection<T2> secondCollection, final Collection<T3> thirdCollection, final Collection<T4> fourthCollection, final Collection<T5> fifthCollection, final Collection<T6> sixthCollection, final Collection<T7> seventhCollection) {
        final List<Septuple<T1, T2, T3, T4, T5, T6, T7>> result = new ArrayList<>(firstCollection.size() * secondCollection.size() * thirdCollection.size() * fourthCollection.size() * firstCollection.size() * sixthCollection.size() * seventhCollection.size());
        for (final T1 t1 : firstCollection) {
            for (final T2 t2 : secondCollection) {
                for (final T3 t3 : thirdCollection) {
                    for (final T4 t4 : fourthCollection) {
                        for (final T5 t5 : fifthCollection) {
                            for (final T6 t6 : sixthCollection) {
                                for (final T7 t7 : seventhCollection) {
                                    result.add(new Septuple<>(t1, t2, t3, t4, t5, t6, t7));
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    public static <T1, T2> Object[][] combinations(final Collection<T1> firstCollection, final Collection<T2> secondCollection) {
        return tuplesToArray(combinationList(firstCollection, secondCollection));
    }

    public static <T1, T2, T3> Object[][] combinations(final Collection<T1> firstCollection, final Collection<T2> secondCollection, final Collection<T3> thirdCollection) {
        return triplesToArray(combinationList(firstCollection, secondCollection, thirdCollection));
    }

    public static <T1, T2> Object[][] tuplesToArray(final List<Tuple<T1, T2>> tuples) {
        return tuples.stream().map(tuple -> new Object[]{tuple.getFirst(), tuple.getSecond()}).toArray(Object[][]::new);
    }

    public static <T1, T2, T3> Object[][] triplesToArray(final List<Triple<T1, T2, T3>> triples) {
        return triples.stream().map(triple -> new Object[]{triple.getFirst(), triple.getSecond(), triple.getThird()}).toArray(Object[][]::new);
    }

    public static <T1, T2, T3, T4> Object[][] combinations(final Collection<T1> firstCollection, final Collection<T2> secondCollection, final Collection<T3> thirdCollection, final Collection<T4> fourthCollection) {
        return quadruplesToArray(combinationList(firstCollection, secondCollection, thirdCollection, fourthCollection));
    }

    public static <T1, T2, T3, T4> Object[][] quadruplesToArray(final List<Quadruple<T1, T2, T3, T4>> quadruples) {
        return quadruples.stream().map(quadruple -> new Object[]{quadruple.getFirst(), quadruple.getSecond(), quadruple.getThird(), quadruple.getFourth()}).toArray(Object[][]::new);
    }

    public static <T1, T2, T3, T4, T5> Object[][] quintuplesToArray(final List<Quintuple<T1, T2, T3, T4, T5>> quintuples) {
        return quintuples.stream().map(quintuple -> new Object[]{quintuple.getFirst(), quintuple.getSecond(), quintuple.getThird(), quintuple.getFourth(), quintuple.getFifth()}).toArray(Object[][]::new);
    }

    public static <T1, T2, T3, T4, T5, T6> Object[][] sextuplesToArray(final List<Sextuple<T1, T2, T3, T4, T5, T6>> sextuples) {
        return sextuples.stream().map(sextuple -> new Object[]{sextuple.getFirst(), sextuple.getSecond(), sextuple.getThird(), sextuple.getFourth(), sextuple.getFifth(), sextuple.getSixth()}).toArray(Object[][]::new);
    }

    public static <T1, T2, T3, T4, T5, T6, T7> Object[][] septuplesToArray(final List<Septuple<T1, T2, T3, T4, T5, T6, T7>> septuples) {
        return septuples.stream().map(septuple -> new Object[]{septuple.getFirst(), septuple.getSecond(), septuple.getThird(), septuple.getFourth(), septuple.getFifth(), septuple.getSixth(), septuple.getSeventh()}).toArray(Object[][]::new);
    }

    private static <T1, T2, T3> Triple[] asArray(List<Triple<T1, T2, T3>> col) {
        return col.toArray(new Triple[col.size()]);
    }
}
