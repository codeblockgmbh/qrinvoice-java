/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.resources;

public class SpcSample {
    private String filePath;
    private boolean valid;
    private boolean validToParse;

    public SpcSample(final String filePath) {
        this(filePath, true, true);
    }

    public SpcSample(final String filePath, final boolean valid) {
        this(filePath, valid, true);
    }

    public SpcSample(final String filePath, final boolean valid, final boolean validToParse) {
        this.filePath = filePath;
        this.valid = valid;
        this.validToParse = validToParse;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(final String filePath) {
        this.filePath = filePath;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(final boolean valid) {
        this.valid = valid;
    }

    public boolean isValidToParse() {
        return validToParse;
    }

    public void setValidToParse(boolean validToParse) {
        this.validToParse = validToParse;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SpcSample{");
        sb.append("filePath='").append(filePath).append('\'');
        sb.append(", valid=").append(valid);
        sb.append(", validToParse=").append(validToParse);
        sb.append('}');
        return sb.toString();
    }
}
