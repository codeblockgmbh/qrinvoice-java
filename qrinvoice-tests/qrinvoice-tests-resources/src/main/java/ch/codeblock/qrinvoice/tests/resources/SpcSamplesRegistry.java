/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.resources;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class SpcSamplesRegistry {
    private static final String TESTDATEN_FOLDER = "/samples/spc/";

    public static SpcSample defaultSpc() {
        return data().iterator().next();
    }

    public static Collection<SpcSample> data() {
        // Note: Samples can be validToParse, but contain missing values which make an invalid QrInvoice object (valid=false)
        return Arrays.asList(
                new SpcSample(TESTDATEN_FOLDER + "spc_IG2.0_default.txt"), // default, see #defaultSpc
                new SpcSample(TESTDATEN_FOLDER + "esr+.txt"),
                new SpcSample(TESTDATEN_FOLDER + "Example.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_IG1.0_page32.txt", false, false),
                new SpcSample(TESTDATEN_FOLDER + "spc_IG2.0_page39_orig_qrr_qriban_avs_billinformation.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_IG2.0_page39_variation_qrr_avs_billinfo_plus_ultimatecreditor.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_IG2.0_page41_orig_non_nodebtor_unstructuredmessage.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_IG2.0_page41_variation_non_unstructuredmessage_plus_debtor.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_IG2.0_page41_variation_scor_unstructuredmessage_plus_reference.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_IG2.0_page43_orig_scor.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_IG2.1_page43_variation_scor_round_amount.txt"), // test with a round amount
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_additional_linebreak.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_additional_longtail.txt", false, false),
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_additional_tail.txt", false, false),
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_empty.txt", false, false),// test with synthetic data and without debtor in order to get the free text field which might consume more space
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_header_only.txt", false, false),
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_header_trailer_only.txt", false),
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_missing_currency.txt", false),
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_missing_reference_type.txt", false),
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_qrr_syntheticdata_max_nodebtor.txt"),// test with synthetic data and without debtor in order to get the free text field which might consume more space
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_syntheticdata_max_overall.txt"), // test with synthetic data in order to see edge cases regarding text wrapping
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_syntheticdata_max_per_element_without_ultimate_creditor.txt"),// test with synthetic data and max per element in order to see edge cases regarding text wrapping
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_syntheticdata_max_per_element_combined_without_ultimate_creditor.txt"),// test with synthetic data and max per element in order to see edge cases regarding text wrapping
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_syntheticdata_max_per_element.txt"),// test with synthetic data and max per element in order to see edge cases regarding text wrapping
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_syntheticdata_max_per_element_combined.txt"),// test with synthetic data and max per element in order to see edge cases regarding text wrapping
                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_nodebtor_msg_billinfo.txt"), // encountered edge case when debtor is not set but both billinfo and unstructured message, needed minor fix in itext4writer

                new SpcSample(TESTDATEN_FOLDER + "spc_QRIN_rappen_ohne_fuehrende_null.txt"),// https://github.com/manuelbl/SwissQRBill/issues/16

                // https://www.paymentstandards.ch/de/shared/communication-grid/script.html - Einführungsdrehbücher zur QR-Rechnung - Muster Zahlteile
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_1_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_1_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_1_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_21_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_21_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_21_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_22_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_22_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_22_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_29_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_29_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_29_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_2_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_2_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_2_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_30_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_30_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_30_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_33_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_33_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_33_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_34_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_34_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_34_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_37_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_37_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_37_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_38_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_38_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_38_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_45_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_45_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_45_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_46_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_46_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_46_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_5_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_5_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_5_Datenschema_italienisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_6_Datenschema_englisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_6_Datenschema_franzoesisch.txt"),
                new SpcSample(TESTDATEN_FOLDER + "spc_SIX_example_6_Datenschema_italienisch.txt")
        );
    }

    public static Collection<String> validDataFilePaths() {
        return data().stream().filter(SpcSample::isValid).map(SpcSample::getFilePath).collect(Collectors.toList());
    }

    public static String plainFilename(final String file) {
        return file.replace(SpcSamplesRegistry.TESTDATEN_FOLDER, "").replace(".txt", "");
    }

    public static String filename(final String file, final String postfix) {
        return plainFilename(file) + postfix;
    }

    public static String getFileContent(final SpcSample sample) {
        return getFileContent(sample.getFilePath());
    }

    public static String getFileContent(final String file) {
        try {
            // source encoding is UTF-8
            return IOUtils.toString(SpcSamplesRegistry.class.getResourceAsStream(file), StandardCharsets.UTF_8)
                    .replace("\uFEFF", ""); // fixes some SIX example files
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
