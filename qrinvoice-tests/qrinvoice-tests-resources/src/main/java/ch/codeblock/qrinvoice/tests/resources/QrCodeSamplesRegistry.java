/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.resources;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

public class QrCodeSamplesRegistry {
    private static final String TESTDATEN_FOLDER = "/samples/qrcode/";

    public static Collection<QrCodeSample> data() {
        return Arrays.asList(
                new QrCodeSample(TESTDATEN_FOLDER + "qrcode-none.png", 0),
                new QrCodeSample(TESTDATEN_FOLDER + "qrcode-swisscross-noborders.png", 1),
                new QrCodeSample(TESTDATEN_FOLDER + "qrcode-swisscross-whiteborder.png", 1),
                new QrCodeSample(TESTDATEN_FOLDER + "qrcode-swisscross-wholepaymentpart.png", 1),
                new QrCodeSample(TESTDATEN_FOLDER + "two-qrcode-swisscross.png", 2),
                new QrCodeSample(TESTDATEN_FOLDER + "two-identical-qrcode-swisscross-one-other-qr-code.png", 1)
        );
    }

    public static byte[] getFileContent(final String file) {
        try {
            return IOUtils.toByteArray(QrCodeSamplesRegistry.class.getResourceAsStream(file));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


