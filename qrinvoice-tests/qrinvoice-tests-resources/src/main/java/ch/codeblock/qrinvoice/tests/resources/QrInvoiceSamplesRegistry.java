/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.resources;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static ch.codeblock.qrinvoice.tests.resources.QrInvoiceSampleMatch.all;
import static ch.codeblock.qrinvoice.tests.resources.QrInvoiceSampleMatch.match;

public class QrInvoiceSamplesRegistry {
    private static final String TESTDATEN_FOLDER = "/samples/qrinvoice/";
    private static final String CODEBLOCK = TESTDATEN_FOLDER + "codeblock/";
    private static final String SWICO = TESTDATEN_FOLDER + "swico/";
    private static final String SIX = TESTDATEN_FOLDER + "six/";
    private static final String BLURRED = TESTDATEN_FOLDER + "blurred/";
    private static final String DARK = TESTDATEN_FOLDER + "dark/";
    private static final String ANGLED = TESTDATEN_FOLDER + "angled/";
    private static final String TOPDOWN = TESTDATEN_FOLDER + "topdown/";

    public static Collection<QrInvoiceSample> data() {
        return Arrays.asList(
                // codeblock
                new QrInvoiceSample(CODEBLOCK + "digital-origin-ppr-single-page-pdfexpert-raster-graphic.pdf", all(1)),
                new QrInvoiceSample(CODEBLOCK + "digital-origin-ppr-single-page-word-raster-graphic.pdf", all(1)),
                new QrInvoiceSample(CODEBLOCK + "digital-origin-ppr-single-page.pdf", all(1)),
                new QrInvoiceSample(CODEBLOCK + "digital-origin-ppr-three-pages-two-spcs.pdf", all(2)),
                new QrInvoiceSample(CODEBLOCK + "digital-origin-swissqrcode-single-dark.png", all(1)),
                new QrInvoiceSample(CODEBLOCK + "digital-origin-swissqrcode-single.png", all(1)),
                new QrInvoiceSample(CODEBLOCK + "scanned-ppr-single-page.pdf", all(1)),
//
                // swico
                new QrInvoiceSample(SWICO + "sample-0002-swico-synthetic.png", all(1)),
                new QrInvoiceSample(SWICO + "sample-0003-run-my-accounts.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0004-cresus.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0005-cresus.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0006-bcge-vierge.jpg", all(1), false),
                new QrInvoiceSample(SWICO + "sample-0007-bcge-complete.jpg", match(0, 1), match(0, 1), false),
                new QrInvoiceSample(SWICO + "sample-0008-sage200.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0009-abacus-immobilien.pdf", all(12)),
                new QrInvoiceSample(SWICO + "sample-0010-proffix.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0011-abraxas-juris.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0012-messerli-informatik.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0013-messerli-informatik.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0014-sap-de.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0015-sap-fr.pdf", all(1)),
                new QrInvoiceSample(SWICO + "sample-0016-sap-en.pdf", all(1)),
//
                // six https://www.paymentstandards.ch/de/shared/communication-grid/script.html - muster qr zahlteile
                new QrInvoiceSample(SIX + "SIX_example_13_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_13_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_13_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_13_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_14_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_14_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_14_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_14_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_17_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_17_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_17_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_17_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_18_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_18_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_18_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_18_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_1_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_1_englisch.jpg", all(1)),
                // TODO unfortunately was 0-byte length from SIX - new QrInvoiceSample(SIX + "SIX_example_1_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_1_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_21_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_21_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_21_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_21_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_22_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_22_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_22_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_22_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_29_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_29_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_29_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_29_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_2_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_2_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_2_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_2_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_30_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_30_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_30_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_30_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_33_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_33_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_33_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_33_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_34_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_34_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_34_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_34_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_37_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_37_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_37_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_37_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_38_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_38_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_38_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_38_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_45_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_45_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_45_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_45_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_46_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_46_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_46_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_46_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_5_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_5_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_5_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_5_italienisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_6_deutsch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_6_englisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_6_franzoesisch.jpg", all(1)),
                new QrInvoiceSample(SIX + "SIX_example_6_italienisch.jpg", all(1)),
//
                // blurred
                new QrInvoiceSample(BLURRED + "sample-0001-strong-blur.jpg", all(1)),
                new QrInvoiceSample(BLURRED + "sample-0002-blur.jpg", all(1)),
//
                // dark
                new QrInvoiceSample(DARK + "sample-0001-dark.jpg", all(1)),
                new QrInvoiceSample(DARK + "sample-0002-very-dark.jpg", all(1)),
                new QrInvoiceSample(DARK + "sample-0003-dark.jpg", match(0, 1), match(1, 1)),
                new QrInvoiceSample(DARK + "sample-0003-dark.jpg", match(0, 1), match(1, 1)),
//
                // angled
                new QrInvoiceSample(ANGLED + "sample-0001-angled.jpg", all(1)),
                new QrInvoiceSample(ANGLED + "sample-0002-angled.jpg", all(1)),
//
                // topdown
                new QrInvoiceSample(TOPDOWN + "sample-0001-topdown.jpg", all(1)),
                new QrInvoiceSample(TOPDOWN + "sample-0001-topdown.pdf", all(1)),
                new QrInvoiceSample(TOPDOWN + "sample-0002-topdown.jpg", all(1)),
                new QrInvoiceSample(TOPDOWN + "sample-0002-topdown.pdf", all(1)),
                new QrInvoiceSample(TOPDOWN + "150dpi-color.jpg", all(1)),
                new QrInvoiceSample(TOPDOWN + "150dpi-color.pdf", all(1)),
                new QrInvoiceSample(TOPDOWN + "150dpi-truegray.pdf", all(1)),
                new QrInvoiceSample(TOPDOWN + "300dpi-color.jpg", all(1)),
                new QrInvoiceSample(TOPDOWN + "300dpi-color.pdf", all(1)),
                new QrInvoiceSample(TOPDOWN + "300dpi-truegray.pdf", all(1)),
                new QrInvoiceSample(TOPDOWN + "600dpi-color.pdf", all(1)),
                new QrInvoiceSample(TOPDOWN + "qr-rechnung-swiftscan.pdf", all(1))

        );
    }

    public static byte[] getFileContent(final String file) {
        try {
            return IOUtils.toByteArray(QrInvoiceSamplesRegistry.class.getResourceAsStream(file));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


