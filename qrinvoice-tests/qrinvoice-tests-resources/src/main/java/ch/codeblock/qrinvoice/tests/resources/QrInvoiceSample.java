/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.resources;

public class QrInvoiceSample {
    private String filePath;
    private QrInvoiceSampleMatch zxing;
    private QrInvoiceSampleMatch boofcv;
    private boolean valid;

    public QrInvoiceSample(final String filePath, final QrInvoiceSampleMatch all) {
        this(filePath, all, all);
    }

    public QrInvoiceSample(final String filePath, final QrInvoiceSampleMatch zxing, final QrInvoiceSampleMatch boofcv) {
        this(filePath, zxing, boofcv, true);
    }

    public QrInvoiceSample(final String filePath, final QrInvoiceSampleMatch all, final boolean valid) {
        this(filePath, all, all, valid);
    }

    public QrInvoiceSample(final String filePath, final QrInvoiceSampleMatch zxing, QrInvoiceSampleMatch boofcv, final boolean valid) {
        this.filePath = filePath;
        this.zxing = zxing;
        this.boofcv = boofcv;
        this.valid = valid;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(final String filePath) {
        this.filePath = filePath;
    }

    public QrInvoiceSampleMatch getZxing() {
        return zxing;
    }

    public void setZxing(QrInvoiceSampleMatch zxing) {
        this.zxing = zxing;
    }

    public QrInvoiceSampleMatch getBoofcv() {
        return boofcv;
    }

    public void setBoofcv(QrInvoiceSampleMatch boofcv) {
        this.boofcv = boofcv;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(final boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("QrInvoiceSample{");
        sb.append("filePath='").append(filePath).append('\'');
        sb.append(", zxing=").append(zxing);
        sb.append(", boofcv=").append(boofcv);
        sb.append(", valid=").append(valid);
        sb.append('}');
        return sb.toString();
    }
}
