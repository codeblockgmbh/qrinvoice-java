/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests;

public class Septuple<T1, T2, T3, T4, T5, T6, T7> {

    private T1 first;
    private T2 second;
    private T3 third;
    private T4 fourth;
    private T5 fifth;
    private T6 sixth;
    private T7 seventh;

    public Septuple() {
    }

    public Septuple(final T1 first, final T2 second, final T3 third, final T4 fourth, final T5 fifth, final T6 sixth, final T7 seventh) {
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
        this.fifth = fifth;
        this.sixth = sixth;
        this.seventh = seventh;
    }

    public T1 getFirst() {
        return first;
    }

    public void setFirst(final T1 first) {
        this.first = first;
    }

    public T2 getSecond() {
        return second;
    }

    public void setSecond(final T2 second) {
        this.second = second;
    }

    public T3 getThird() {
        return third;
    }

    public void setThird(final T3 third) {
        this.third = third;
    }

    public T4 getFourth() {
        return fourth;
    }

    public void setFourth(T4 fourth) {
        this.fourth = fourth;
    }

    public T5 getFifth() {
        return fifth;
    }

    public void setFifth(T5 fifth) {
        this.fifth = fifth;
    }

    public T6 getSixth() {
        return sixth;
    }

    public void setSixth(T6 sixth) {
        this.sixth = sixth;
    }

    public T7 getSeventh() {
        return seventh;
    }

    public void setSeventh(final T7 seventh) {
        this.seventh = seventh;
    }
}
