java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=PDF -DpageSize=A4 -Doutput=PaymentPartReceipt ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt

java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=PDF -DpageSize=DIN_LANG -Doutput=PaymentPartReceipt ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=PNG -DpageSize=DIN_LANG -Doutput=PaymentPartReceipt ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=JPEG -DpageSize=DIN_LANG -Doutput=PaymentPartReceipt ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=BMP -DpageSize=DIN_LANG -Doutput=PaymentPartReceipt ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=GIF -DpageSize=DIN_LANG -Doutput=PaymentPartReceipt ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=TIFF -DpageSize=DIN_LANG -Doutput=PaymentPartReceipt ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt

java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=PDF -Doutput=QrCode ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=PNG -Doutput=QrCode ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=GIF -Doutput=QrCode ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
java -cp "qrinvoice-tests-performance.jar:*" -DoutputFormat=TIFF -Doutput=QrCode ch.codeblock.qrinvoice.tests.performance.benchmark.Main  >> performance.txt
