/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tests.integration.performance.benchmark;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.QrInvoiceCodeCreator;
import ch.codeblock.qrinvoice.QrInvoicePaymentPartReceiptCreator;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.mapper.SwissPaymentsCodeToModelMapper;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.output.QrCode;
import org.apache.commons.io.IOUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

public class Main {
    public static void main(String[] args) throws IOException {
        final int warmUp = Integer.valueOf(getProperty("warmUpRuns", "300"));
        final int runs = Integer.valueOf(getProperty("runs", "1000"));

        final OutputFormat outputFormat = OutputFormat.valueOf(getProperty("outputFormat", "PDF"));
        final PageSize pageSize = PageSize.valueOf(getProperty("pageSize", "A4"));
        final Locale locale = new Locale(getProperty("locale", "de"));

        final String output = getProperty("output", "PaymentPartReceipt");

        final String formattedString = String.format("warmUpRuns=%s runs=%s output=%s outputFormat=%s pageSize=%s locale=%s", warmUp, runs, output, outputFormat, pageSize, locale);
        System.out.println(formattedString);

        System.out.println();
        final int totalRuns = runs + warmUp;
        final DescriptiveStatistics stats = new DescriptiveStatistics();
        for (int i = 0; i < totalRuns; i++) {
            final long start = System.currentTimeMillis();
            generate(output, outputFormat, pageSize, locale);
            final long end = System.currentTimeMillis();
            final long ms = (end - start);
            if (i >= warmUp) {
                stats.addValue(ms);
                System.out.print(ms + "ms ");
            }
        }

        System.out.println();
        System.out.println(formattedString);
        System.out.println("Statistics");
        System.out.println("- Min: " + Math.round(stats.getMin()));
        System.out.println("- Max: " + Math.round(stats.getMax()));
        System.out.println("- Mean / Avg: " + Math.round(stats.getMean()));
        System.out.println("- 50th percentile (Median): " + Math.round(stats.getPercentile(50.0D)));
        System.out.println("- 75th percentile: " + Math.round(stats.getPercentile(75.0D)));
        System.out.println("- 90th percentile: " + Math.round(stats.getPercentile(90.0D)));
    }

    public static void generate(final String output, final OutputFormat outputFormat, final PageSize pageSize, final Locale locale) throws IOException {
        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(IOUtils.toString(Main.class.getResourceAsStream("/spc_IG2.0_page39_orig_qrr_qriban_avs_billinformation.txt"), StandardCharsets.UTF_8.toString()));
        final QrInvoice qrInvoice = SwissPaymentsCodeToModelMapper.create().map(original);

        if ("PaymentPartReceipt".equals(output)) {
            final PaymentPartReceipt paymentPartReceipt = QrInvoicePaymentPartReceiptCreator.create()
                    .qrInvoice(qrInvoice)
                    .outputFormat(outputFormat)
                    .pageSize(pageSize)
                    .locale(locale)
                    .createPaymentPartReceipt();
        } else if("QrCode".equals(output)) {
            final QrCode qrCode = QrInvoiceCodeCreator.create()
                    .qrInvoice(qrInvoice)
                    .outputFormat(outputFormat)
                    .createQrCode();
        } else {
            throw new RuntimeException("Unsupported output: Valid are QrCode / PaymentPartReceipt");
        }
    }

    private static String getProperty(final String propertyName, final String defaultValue) {
        final String propertyValue = System.getProperty(propertyName);
        if (propertyValue != null) {
            return propertyValue;
        } else {
            return defaultValue;
        }
    }
}
