package ch.codeblock.qrinvoice.bulk.model;

import org.junit.Test;

import static ch.codeblock.qrinvoice.config.SystemProperties.BULK_DEFAULTS_PREFIX;
import static org.junit.Assert.assertEquals;

public class DefaultValuesTest {

    @Test
    public void assertDefaults() {
        assertEquals("LIBERATION_SANS", DefaultValues.buildNewBulkDefaultsMap().get(ColumnKeys.FONT));
        assertEquals("TRUE", DefaultValues.buildNewBulkDefaultsMap().get(ColumnKeys.BOUNDARY_LINES));
        assertEquals("GERMAN", DefaultValues.buildNewBulkDefaultsMap().get(ColumnKeys.LANGUAGE));
    }

    @Test
    public void assertModifiedDefaults() {
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.FONT, "Arial");
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.BOUNDARY_LINES, "false");
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.LANGUAGE, "IT");
        assertEquals("Arial", DefaultValues.buildNewBulkDefaultsMap().get(ColumnKeys.FONT));
        assertEquals("false", DefaultValues.buildNewBulkDefaultsMap().get(ColumnKeys.BOUNDARY_LINES));
        assertEquals("IT", DefaultValues.buildNewBulkDefaultsMap().get(ColumnKeys.LANGUAGE));
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.FONT);
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.BOUNDARY_LINES);
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.LANGUAGE);
    }

}