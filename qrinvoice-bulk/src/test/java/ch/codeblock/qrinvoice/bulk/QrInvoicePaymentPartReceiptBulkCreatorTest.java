package ch.codeblock.qrinvoice.bulk;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.bulk.model.InvoiceEntry;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.output.BulkOutput;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class QrInvoicePaymentPartReceiptBulkCreatorTest {
    @Test
    public void createFromCsvAsPdf() throws Exception {
        final BulkOutput output = QrInvoicePaymentPartReceiptBulkCreator.create()
                .inputMimeType(MimeType.CSV)
                .inputStream(getClass().getResourceAsStream("/ch/codeblock/qrinvoice/bulk/csv/QR_basic.csv"))
                .invoiceEntryPreProcessor(this::setLiberationSans)
                .createAsMimeType(MimeType.PDF);
        assertEquals(OutputFormat.PDF, output.getOutputFormat());
        assertNotNull(output.getData());
        assertEquals(13, output.getCount());
    }


    @Test
    public void createFromCsvAsZip() throws Exception {
        final BulkOutput output = QrInvoicePaymentPartReceiptBulkCreator.create()
                .inputMimeType(MimeType.CSV)
                .inputStream(getClass().getResourceAsStream("/ch/codeblock/qrinvoice/bulk/csv/QR_basic.csv"))
                .invoiceEntryPreProcessor(this::setLiberationSans)
                .createAsMimeType(MimeType.ZIP);
        assertEquals(OutputFormat.ZIP, output.getOutputFormat());
        assertNotNull(output.getData());
        assertEquals(13, output.getCount());
    }

    @Test
    public void createFromXlsxAsPdf() throws Exception {
        final BulkOutput output = QrInvoicePaymentPartReceiptBulkCreator.create()
                .inputMimeType(MimeType.XLSX)
                .inputStream(getClass().getResourceAsStream("/ch/codeblock/qrinvoice/bulk/excel/QR_basic.xlsx"))
                .invoiceEntryPreProcessor(this::setLiberationSans)
                .createAsMimeType(MimeType.PDF);
        assertEquals(OutputFormat.PDF, output.getOutputFormat());
        assertNotNull(output.getData());
        assertEquals(13, output.getCount());
    }

    @Test
    public void createFromXlsxAsZip() throws Exception {
        final BulkOutput output = QrInvoicePaymentPartReceiptBulkCreator.create()
                .inputMimeType(MimeType.XLSX)
                .inputStream(getClass().getResourceAsStream("/ch/codeblock/qrinvoice/bulk/excel/QR_basic.xlsx"))
                .invoiceEntryPreProcessor(this::setLiberationSans)
                .createAsMimeType(MimeType.ZIP);
        assertEquals(OutputFormat.ZIP, output.getOutputFormat());
        assertNotNull(output.getData());
        assertEquals(13, output.getCount());
    }

    @Test(expected = ValidationException.class)
    public void createFromCsvLimitExceeded() throws Exception {
        final BulkOutput output = QrInvoicePaymentPartReceiptBulkCreator.create()
                .inputMimeType(MimeType.CSV)
                .inputStream(getClass().getResourceAsStream("/ch/codeblock/qrinvoice/bulk/csv/QR_basic.csv"))
                .invoiceEntryPreProcessor(this::setLiberationSans)
                .limit(5)
                .createAsMimeType(MimeType.ZIP);
    }

    // TODO improve, maybe only on CI or install fonts on CI before running tests.
    private void setLiberationSans(InvoiceEntry invoiceEntry) {
        if(System.getProperty("BITBUCKET_CI") != null) {
            if (invoiceEntry.getFontFamily() == FontFamily.FRUTIGER
                    || invoiceEntry.getFontFamily() == FontFamily.HELVETICA
                    || invoiceEntry.getFontFamily() == FontFamily.ARIAL
            ) {
                invoiceEntry.setFontFamily(FontFamily.LIBERATION_SANS);
            }
        }
    }
}
