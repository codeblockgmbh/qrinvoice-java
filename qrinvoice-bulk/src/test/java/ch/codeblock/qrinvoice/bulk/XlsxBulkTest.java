package ch.codeblock.qrinvoice.bulk;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.bulk.excel.Xlsx;
import ch.codeblock.qrinvoice.bulk.excel.model.XlsxInvoiceEntry;
import ch.codeblock.qrinvoice.bulk.model.ColumnKeys;
import ch.codeblock.qrinvoice.bulk.model.DefaultValues;
import ch.codeblock.qrinvoice.bulk.model.InvoiceEntryMapper;
import ch.codeblock.qrinvoice.model.QrInvoice;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static ch.codeblock.qrinvoice.config.SystemProperties.BULK_DEFAULTS_PREFIX;
import static org.junit.Assert.*;

public class XlsxBulkTest {

    @Test
    public void readXlsxFile() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException {
        assertEquals(1, create(Xlsx.readXlsx(asStream("/ch/codeblock/qrinvoice/bulk/excel/QR_minimal.xlsx"), XlsxInvoiceEntry.class)).size());
        assertEquals(13, create(Xlsx.readXlsx(asStream("/ch/codeblock/qrinvoice/bulk/excel/QR_basic.xlsx"), XlsxInvoiceEntry.class)).size());
        assertEquals(1, create(Xlsx.readXlsx(asStream("/ch/codeblock/qrinvoice/bulk/excel/QR_minimal_additional_columns.xlsx"), XlsxInvoiceEntry.class)).size());
        assertEquals(1024, create(Xlsx.readXlsx(asStream("/ch/codeblock/qrinvoice/bulk/excel/QR_PDF_1024.xlsx"), XlsxInvoiceEntry.class)).size());
        assertEquals(588, create(Xlsx.readXlsx(asStream("/ch/codeblock/qrinvoice/bulk/excel/QR_variations.xlsx"), XlsxInvoiceEntry.class)).size());
    }

    @Test
    public void testDefaultValues() throws IOException, InvocationTargetException, NoSuchMethodException, IllegalAccessException, InstantiationException {
        // run 1 - defaults
        final String file = "/ch/codeblock/qrinvoice/bulk/excel/QR_minimal.xlsx";
        List<XlsxInvoiceEntry> qrInvoices = Xlsx.readXlsx(asStream(file), XlsxInvoiceEntry.class);
        assertEquals(1, qrInvoices.size());

        XlsxInvoiceEntry invoice = qrInvoices.get(0);
        assertEquals(Locale.GERMAN, invoice.getLanguage());
        assertEquals(true, invoice.getBoundaryLines());
        assertEquals(FontFamily.LIBERATION_SANS, invoice.getFontFamily());
        assertNull(invoice.getAmount());

        // run 2 overwrite defaults
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.FONT, "Arial");
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.BOUNDARY_LINES, "false");
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.LANGUAGE, "IT");
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.AMOUNT, "42");
        DefaultValues.reloadDefaultsMap();

        qrInvoices = Xlsx.readXlsx(asStream(file), XlsxInvoiceEntry.class);
        assertEquals(1, qrInvoices.size());

        invoice = qrInvoices.get(0);
        assertEquals(Locale.ITALIAN, invoice.getLanguage());
        assertEquals(false, invoice.getBoundaryLines());
        assertEquals(FontFamily.ARIAL, invoice.getFontFamily());
        assertEquals(BigDecimal.valueOf(42), invoice.getAmount());

        // reset defaults
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.FONT);
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.BOUNDARY_LINES);
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.LANGUAGE);
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.AMOUNT);
        DefaultValues.reloadDefaultsMap();
    }

    private Collection<QrInvoice> create(final List<XlsxInvoiceEntry> invoices) {
        return invoices.stream().map(InvoiceEntryMapper.create()::map).collect(Collectors.toList());
    }

    @Test
    public void readXlsxFileHeaderRow2() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException {
        try {
            Xlsx.readXlsx(asStream("/ch/codeblock/qrinvoice/bulk/excel/QR_invalid_header_row2.xlsx"), XlsxInvoiceEntry.class);
            fail("Expected exception");
        } catch (RuntimeException e) {
            assertEquals(BulkTestConstants.MISSING_HEADERS, e.getMessage());
        }
    }

    @Test
    public void readXlsxFileMissingHeader() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException {
        try {
            Xlsx.readXlsx(asStream("/ch/codeblock/qrinvoice/bulk/excel/QR_invalid_missing_header.xlsx"), XlsxInvoiceEntry.class);
            fail("Expected exception");
        } catch (RuntimeException e) {
            assertEquals(BulkTestConstants.MISSING_HEADERS, e.getMessage());
        }
    }

    private InputStream asStream(final String s) {
        return getClass().getResourceAsStream(s);
    }
}