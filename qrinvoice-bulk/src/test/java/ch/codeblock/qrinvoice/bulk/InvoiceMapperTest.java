package ch.codeblock.qrinvoice.bulk;

import ch.codeblock.qrinvoice.bulk.excel.model.XlsxInvoiceEntry;
import ch.codeblock.qrinvoice.bulk.model.AddressType;
import ch.codeblock.qrinvoice.bulk.model.InvoiceEntryMapper;
import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.ReferenceType;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.Assert.*;

public class InvoiceMapperTest {

    @Test
    public void createQrInvoiceStructured() {
        final XlsxInvoiceEntry invoice = createStructured();

        final QrInvoice qrInvoice = InvoiceEntryMapper.create().map(invoice);
        assertEquals("CH3709000000304442225", qrInvoice.getCreditorInformation().getIban());

        assertEquals("CreditorName", qrInvoice.getCreditorInformation().getCreditor().getName());
        assertEquals("Creditor Street", qrInvoice.getCreditorInformation().getCreditor().getStreetName());
        assertEquals("1", qrInvoice.getCreditorInformation().getCreditor().getHouseNumber());
        assertEquals("3000", qrInvoice.getCreditorInformation().getCreditor().getPostalCode());
        assertEquals("Creditor City", qrInvoice.getCreditorInformation().getCreditor().getCity());
        assertEquals("CH", qrInvoice.getCreditorInformation().getCreditor().getCountry());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getAddressLine1());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getAddressLine2());

        assertEquals("Debtor Name", qrInvoice.getUltimateDebtor().getName());
        assertEquals("Debtor Street", qrInvoice.getUltimateDebtor().getStreetName());
        assertEquals("2", qrInvoice.getUltimateDebtor().getHouseNumber());
        assertEquals("3014", qrInvoice.getUltimateDebtor().getPostalCode());
        assertEquals("Debtor City", qrInvoice.getUltimateDebtor().getCity());
        assertEquals("CH", qrInvoice.getUltimateDebtor().getCountry());
        assertEquals(null, qrInvoice.getUltimateDebtor().getAddressLine1());
        assertEquals(null, qrInvoice.getUltimateDebtor().getAddressLine2());

        assertEquals(ReferenceType.WITHOUT_REFERENCE, qrInvoice.getPaymentReference().getReferenceType());
        assertEquals(null, qrInvoice.getPaymentReference().getReference());
        assertEquals("Unstructured Message", qrInvoice.getPaymentReference().getAdditionalInformation().getUnstructuredMessage());
        assertEquals("//XY/", qrInvoice.getPaymentReference().getAdditionalInformation().getBillInformation());

        assertEquals(0, qrInvoice.getPaymentAmountInformation().getAmount().compareTo(BigDecimal.valueOf(42)));
        assertEquals(SwissPaymentsCode.CHF, qrInvoice.getPaymentAmountInformation().getCurrency());

        assertEquals("eBill/B/john.doe@example.com", qrInvoice.getAlternativeSchemes().getAlternativeSchemeParameters().get(0));
        assertEquals("cdbk/some/values", qrInvoice.getAlternativeSchemes().getAlternativeSchemeParameters().get(1));
    }


    @Test
    public void createQrInvoiceStructuredWithoutStreetnameAndNumber() {
        final XlsxInvoiceEntry invoice = createStructured();
        invoice.setCreditorStreetnameAddressLine1(null);
        invoice.setCreditorStreetnumberAddressLine2(null);
        invoice.setDebtorStreetnameAddressLine1(null);
        invoice.setDebtorStreetnumberAddressLine2(null);

        final QrInvoice qrInvoice = InvoiceEntryMapper.create().map(invoice);
        assertEquals("CH3709000000304442225", qrInvoice.getCreditorInformation().getIban());

        assertEquals("CreditorName", qrInvoice.getCreditorInformation().getCreditor().getName());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getStreetName());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getHouseNumber());
        assertEquals("3000", qrInvoice.getCreditorInformation().getCreditor().getPostalCode());
        assertEquals("Creditor City", qrInvoice.getCreditorInformation().getCreditor().getCity());
        assertEquals("CH", qrInvoice.getCreditorInformation().getCreditor().getCountry());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getAddressLine1());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getAddressLine2());

        assertEquals("Debtor Name", qrInvoice.getUltimateDebtor().getName());
        assertEquals(null, qrInvoice.getUltimateDebtor().getStreetName());
        assertEquals(null, qrInvoice.getUltimateDebtor().getHouseNumber());
        assertEquals("3014", qrInvoice.getUltimateDebtor().getPostalCode());
        assertEquals("Debtor City", qrInvoice.getUltimateDebtor().getCity());
        assertEquals("CH", qrInvoice.getUltimateDebtor().getCountry());
        assertEquals(null, qrInvoice.getUltimateDebtor().getAddressLine1());
        assertEquals(null, qrInvoice.getUltimateDebtor().getAddressLine2());
    }


    @Test
    public void createQrInvoiceCombinedQrReference() {
        final XlsxInvoiceEntry invoice = createCombined();

        final QrInvoice qrInvoice = InvoiceEntryMapper.create().map(invoice);
        assertEquals("CH4431999123000889012", qrInvoice.getCreditorInformation().getIban());

        assertEquals("CreditorName", qrInvoice.getCreditorInformation().getCreditor().getName());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getStreetName());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getHouseNumber());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getPostalCode());
        assertEquals(null, qrInvoice.getCreditorInformation().getCreditor().getCity());
        assertEquals("CH", qrInvoice.getCreditorInformation().getCreditor().getCountry());
        assertEquals("Creditor Street 1", qrInvoice.getCreditorInformation().getCreditor().getAddressLine1());
        assertEquals("3000 Creditor City", qrInvoice.getCreditorInformation().getCreditor().getAddressLine2());

        assertEquals("Debtor Name", qrInvoice.getUltimateDebtor().getName());
        assertEquals(null, qrInvoice.getUltimateDebtor().getStreetName());
        assertEquals(null, qrInvoice.getUltimateDebtor().getHouseNumber());
        assertEquals(null, qrInvoice.getUltimateDebtor().getPostalCode());
        assertEquals(null, qrInvoice.getUltimateDebtor().getCity());
        assertEquals("LI", qrInvoice.getUltimateDebtor().getCountry());
        assertEquals("Debtor Street 2", qrInvoice.getUltimateDebtor().getAddressLine1());
        assertEquals("3014 Debtor City", qrInvoice.getUltimateDebtor().getAddressLine2());

        assertEquals(ReferenceType.QR_REFERENCE, qrInvoice.getPaymentReference().getReferenceType());
        assertEquals("210000000003139471430009017", qrInvoice.getPaymentReference().getReference());
        assertEquals(null, qrInvoice.getPaymentReference().getAdditionalInformation().getUnstructuredMessage());
        assertEquals(null, qrInvoice.getPaymentReference().getAdditionalInformation().getBillInformation());

        assertEquals(0, qrInvoice.getPaymentAmountInformation().getAmount().compareTo(BigDecimal.valueOf(42.20)));
        assertEquals(SwissPaymentsCode.EUR, qrInvoice.getPaymentAmountInformation().getCurrency());

        assertTrue(qrInvoice.getAlternativeSchemes() == null || CollectionUtils.isEmpty(qrInvoice.getAlternativeSchemes().getAlternativeSchemeParameters()));
    }

    @Test
    public void createQrInvoiceStructuredWithoutCreditorStreetname() {
        final XlsxInvoiceEntry invoice = createStructured();
        invoice.setCreditorStreetnameAddressLine1(null);
        streetForcedValidationTest(invoice);
    }

    @Test
    public void createQrInvoiceStructuredWithoutCreditorStreetNr() {
        final XlsxInvoiceEntry invoice = createStructured();
        invoice.setCreditorStreetnumberAddressLine2(null);
        streetForcedValidationTest(invoice);
    }

    @Test
    public void createQrInvoiceStructuredWithoutDebtorStreetname() {
        final XlsxInvoiceEntry invoice = createStructured();
        invoice.setDebtorStreetnameAddressLine1(null);
        streetForcedValidationTest(invoice);
    }

    @Test
    public void createQrInvoiceStructuredWithoutDebtorStreetNr() {
        final XlsxInvoiceEntry invoice = createStructured();
        invoice.setDebtorStreetnumberAddressLine2(null);
        streetForcedValidationTest(invoice);
    }

    @Test
    public void createQrInvoiceCombinedWithoutCreditorAddressLine1() {
        final XlsxInvoiceEntry invoice = createCombined();
        invoice.setCreditorStreetnameAddressLine1(null);
        streetForcedValidationTest(invoice);
    }

    @Test
    public void createQrInvoiceCombinedWithoutDebtorAddressLine1() {
        final XlsxInvoiceEntry invoice = createCombined();
        invoice.setDebtorStreetnameAddressLine1(null);
        streetForcedValidationTest(invoice);
    }

    private void streetForcedValidationTest(XlsxInvoiceEntry invoice) {
        System.setProperty(SystemProperties.BULK_FORCE_STREETS, "true");
        try {
            InvoiceEntryMapper.create().map(invoice);
            fail("Expected validation error");
        } catch (ValidationException e) {
            assertTrue(e.getValidationResult().getErrors().stream()
                    .map(ve -> ve.getErrorMessageKeys().contains("validation.error.address.streetNameHouseNumberForce"))
                    .findAny().orElse(false));
        }
        System.clearProperty(SystemProperties.BULK_FORCE_STREETS);
    }

    private XlsxInvoiceEntry createStructured() {
        final XlsxInvoiceEntry invoice = new XlsxInvoiceEntry();
        invoice.setIban("CH3709000000304442225");
        invoice.setCreditorAddressType(AddressType.STRUCTURED);
        invoice.setCreditorName("CreditorName");
        invoice.setCreditorStreetnameAddressLine1("Creditor Street");
        invoice.setCreditorStreetnumberAddressLine2("1");
        invoice.setCreditorPostalCode("3000");
        invoice.setCreditorCity("Creditor City");
        invoice.setCreditorCountry("CH");
        invoice.setAmount(new BigDecimal(42));
        invoice.setCurrency(Currency.getInstance("CHF"));
        invoice.setDebtorAddressType(AddressType.STRUCTURED);
        invoice.setDebtorName("Debtor Name");
        invoice.setDebtorStreetnameAddressLine1("Debtor Street");
        invoice.setDebtorStreetnumberAddressLine2("2");
        invoice.setDebtorPostalCode("3014");
        invoice.setDebtorCity("Debtor City");
        invoice.setDebtorCountry("CH");
        invoice.setReferenceType(ReferenceType.WITHOUT_REFERENCE);
        invoice.setReferenceNumber(null);
        invoice.setUnstructuredMessage("Unstructured Message");
        invoice.setBillInformation("//XY/");
        invoice.setAltPmt1("eBill/B/john.doe@example.com");
        invoice.setAltPmt2("cdbk/some/values");
        return invoice;
    }

    private XlsxInvoiceEntry createCombined() {
        final XlsxInvoiceEntry invoice = new XlsxInvoiceEntry();
        invoice.setIban("CH4431999123000889012");
        invoice.setCreditorAddressType(AddressType.COMBINED);
        invoice.setCreditorName("CreditorName");
        invoice.setCreditorStreetnameAddressLine1("Creditor Street 1");
        invoice.setCreditorStreetnumberAddressLine2("3000 Creditor City");
        invoice.setCreditorPostalCode("");
        invoice.setCreditorCity("");
        invoice.setCreditorCountry("CH");
        invoice.setAmount(new BigDecimal("42.20"));
        invoice.setCurrency(Currency.getInstance("EUR"));
        invoice.setDebtorAddressType(AddressType.COMBINED);
        invoice.setDebtorName("Debtor Name");
        invoice.setDebtorStreetnameAddressLine1("Debtor Street 2");
        invoice.setDebtorStreetnumberAddressLine2("3014 Debtor City");
        invoice.setDebtorPostalCode(null);
        invoice.setDebtorCity(null);
        invoice.setDebtorCountry("LI");
        invoice.setReferenceType(ReferenceType.QR_REFERENCE);
        invoice.setReferenceNumber("210000000003139471430009017");
        invoice.setUnstructuredMessage(null);
        invoice.setBillInformation(null);
        invoice.setAltPmt1(null);
        invoice.setAltPmt2(null);
        return invoice;
    }
}