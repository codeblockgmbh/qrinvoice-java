package ch.codeblock.qrinvoice.bulk.utils;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ZipBuilderTest {

    @Test
    public void zipBuilderTest() {
        try (final ZipBuilder zipBuilder = ZipBuilder.create()) {
            zipBuilder.addFile("file1", "foo bar baz".getBytes(StandardCharsets.UTF_8));
            zipBuilder.addFile("file2", "lorem ipsum".getBytes(StandardCharsets.UTF_8));

            final byte[] zip = zipBuilder.getZip();

            final ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(zip));
            final ZipEntry firstEntry = zipInputStream.getNextEntry();
            assertEquals("file1", firstEntry.getName());

            final ZipEntry secondEntry = zipInputStream.getNextEntry();
            assertEquals("file2", secondEntry.getName());
        } catch (Exception e) {
            fail("no exception expected");
        }
    }
}