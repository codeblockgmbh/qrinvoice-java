package ch.codeblock.qrinvoice.bulk;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.bulk.csv.Csv;
import ch.codeblock.qrinvoice.bulk.csv.model.CsvInvoiceEntry;
import ch.codeblock.qrinvoice.bulk.model.ColumnKeys;
import ch.codeblock.qrinvoice.bulk.model.DefaultValues;
import ch.codeblock.qrinvoice.bulk.model.InvoiceEntryMapper;
import ch.codeblock.qrinvoice.bulk.model.validation.InvalidHeaderException;
import ch.codeblock.qrinvoice.model.QrInvoice;
import org.junit.Test;

import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static ch.codeblock.qrinvoice.bulk.BulkTestConstants.MISSING_HEADERS;
import static ch.codeblock.qrinvoice.config.SystemProperties.BULK_DEFAULTS_PREFIX;
import static org.junit.Assert.*;

public class CsvBulkTest {
    @Test(expected = InvalidHeaderException.class)
    public void readEmptyFile() {
        Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_empty.csv"), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class);
    }

    @Test
    public void readNoLinesFile() {
        assertTrue(create(Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_no_lines.csv"), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class)).isEmpty());
    }

    @Test
    public void readCsvFile() {
        assertEquals(1, create(Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_minimal.csv"), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class)).size());
        assertEquals(13, create(Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_basic.csv"), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class)).size());
        assertEquals(13, create(Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_basic_colons.csv"), StandardCharsets.UTF_8, ',', CsvInvoiceEntry.class)).size());
        assertEquals(1, create(Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_minimal_additional_columns.csv"), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class)).size());
        assertEquals(588, create(Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_variations.csv"), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class)).size());
    }

    @Test
    public void testDefaultValues() {
        // run 1 - defaults
        final String file = "/ch/codeblock/qrinvoice/bulk/csv/QR_minimal.csv";
        List<CsvInvoiceEntry> qrInvoices = Csv.readCsv(asStream(file), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class);
        assertEquals(1, qrInvoices.size());

        CsvInvoiceEntry invoice = qrInvoices.get(0);
        assertEquals(Locale.GERMAN, invoice.getLanguage());
        assertEquals(true, invoice.getBoundaryLines());
        assertEquals(FontFamily.LIBERATION_SANS, invoice.getFontFamily());
        assertNull(invoice.getAmount());

        // run 2 overwrite defaults
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.FONT, "Arial");
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.BOUNDARY_LINES, "false");
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.LANGUAGE, "IT");
        System.setProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.AMOUNT, "42");
        DefaultValues.reloadDefaultsMap();

        qrInvoices = Csv.readCsv(asStream(file), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class);
        assertEquals(1, qrInvoices.size());

        invoice = qrInvoices.get(0);
        assertEquals(Locale.ITALIAN, invoice.getLanguage());
        assertEquals(false, invoice.getBoundaryLines());
        assertEquals(FontFamily.ARIAL, invoice.getFontFamily());
        assertEquals(BigDecimal.valueOf(42), invoice.getAmount());

        // reset defaults
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.FONT);
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.BOUNDARY_LINES);
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.LANGUAGE);
        System.clearProperty(BULK_DEFAULTS_PREFIX + ColumnKeys.AMOUNT);
        DefaultValues.reloadDefaultsMap();
    }

    private Collection<QrInvoice> create(final List<CsvInvoiceEntry> invoices) {
        return invoices.stream().map(InvoiceEntryMapper.create()::map).collect(Collectors.toList());
    }

    @Test
    public void readCsvFileHeaderRow2() {
        try {
            Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_invalid_header_row2.csv"), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class);
            fail("Expected exception");
        } catch (RuntimeException e) {
            assertEquals(MISSING_HEADERS, e.getMessage());
        }
    }

    @Test
    public void readCsvFileMissingHeader() {
        try {
            Csv.readCsv(asStream("/ch/codeblock/qrinvoice/bulk/csv/QR_invalid_missing_header.csv"), StandardCharsets.UTF_8, ';', CsvInvoiceEntry.class);
            fail("Expected exception");
        } catch (RuntimeException e) {
            assertEquals(MISSING_HEADERS, e.getMessage());
        }
    }

    private InputStream asStream(final String s) {
        return getClass().getResourceAsStream(s);
    }

}