package ch.codeblock.qrinvoice.bulk;

public class BulkTestConstants {
    public static final String MISSING_HEADERS = "The following required columns are missing: \n" +
            " - CR_ADDRESS_TYPE\n" +
            " - CR_COUNTRY\n" +
            " - CR_NAME\n" +
            " - CURRENCY\n" +
            " - IBAN\n" +
            " - REFERENCE_TYPE";
}
