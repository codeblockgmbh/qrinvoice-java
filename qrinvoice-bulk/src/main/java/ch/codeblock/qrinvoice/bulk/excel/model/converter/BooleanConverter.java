package ch.codeblock.qrinvoice.bulk.excel.model.converter;


import ch.codeblock.qrinvoice.util.StringUtils;

public class BooleanConverter implements ValueConverter<Boolean, Object> {
    @Override
    public Boolean convert(final Object value) {
        if (value instanceof Boolean) {
            return (Boolean) value;
        } else if (value instanceof String) {
            final String trimmedValue = StringUtils.trimToNull((String) value);
            if ("WAHR".equalsIgnoreCase(trimmedValue)) {
                return true;
            } else if ("VRAI".equalsIgnoreCase(trimmedValue)) {
                return true;
            } else if ("VERO".equalsIgnoreCase(trimmedValue)) {
                return true;
            } else if (trimmedValue != null) {
                return Boolean.parseBoolean(trimmedValue);
            } else {
                return false;
            }
        }
        return null;
    }
}
