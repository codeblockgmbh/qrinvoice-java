package ch.codeblock.qrinvoice.bulk.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipBuilder implements AutoCloseable {

    public static ZipBuilder create() {
        return new ZipBuilder();
    }

    private final ByteArrayOutputStream baos;
    private final ZipOutputStream zipOut;

    private ZipBuilder() {
        baos = new ByteArrayOutputStream();
        zipOut = new ZipOutputStream(baos);
    }

    public void addFile(String filename, byte[] output) throws IOException {
        final ZipEntry zipEntry = new ZipEntry(filename);
        zipOut.putNextEntry(zipEntry);
        zipOut.write(output);
    }

    public byte[] getZip() throws IOException {
        zipOut.close();

        return baos.toByteArray();
    }

    @Override
    public void close() throws Exception {
        zipOut.close();
    }
}
