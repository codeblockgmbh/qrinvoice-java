package ch.codeblock.qrinvoice.bulk.excel.model.converter;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.util.StringUtils;

public class FontFamilyConverter implements ValueConverter<FontFamily, Object> {
    @Override
    public FontFamily convert(final Object value) {
        if (value instanceof FontFamily) {
            return (FontFamily) value;
        } else if (value instanceof String) {
            final String normalizedValue = StringUtils.trimToEmpty((String) value).toUpperCase().replace('-', '_').replace(' ', '_');
            if (!normalizedValue.isEmpty()) {
                return FontFamily.valueOf(normalizedValue);
            }
        }
        return null;
    }
}
