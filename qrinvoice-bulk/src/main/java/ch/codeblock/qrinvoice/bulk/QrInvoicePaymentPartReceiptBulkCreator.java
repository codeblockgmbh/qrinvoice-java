package ch.codeblock.qrinvoice.bulk;

import ch.codeblock.qrinvoice.*;
import ch.codeblock.qrinvoice.bulk.csv.Csv;
import ch.codeblock.qrinvoice.bulk.csv.model.CsvInvoiceEntry;
import ch.codeblock.qrinvoice.bulk.excel.Xlsx;
import ch.codeblock.qrinvoice.bulk.excel.model.XlsxInvoiceEntry;
import ch.codeblock.qrinvoice.bulk.model.InvoiceEntry;
import ch.codeblock.qrinvoice.bulk.model.InvoiceEntryMapper;
import ch.codeblock.qrinvoice.bulk.model.validation.BulkValidationResult;
import ch.codeblock.qrinvoice.bulk.utils.ZipBuilder;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.output.BulkOutput;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.paymentpartreceipt.BoundaryLines;
import ch.codeblock.qrinvoice.pdf.PdfMerger;
import ch.codeblock.qrinvoice.util.StringUtils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * This class is stateful an not intended to be reused
 */
public class QrInvoicePaymentPartReceiptBulkCreator {
    private static final char DEFAULT_CSV_SEPARATOR = ';';
    private char csvSeparator = DEFAULT_CSV_SEPARATOR;
    private MimeType inputMimeType;
    private InputStream inputStream;
    private List<QrInvoice> qrInvoices;
    private Consumer<InvoiceEntry> invoiceEntryPreProcessor;
    private Consumer<QrInvoice> qrInvoiceConsumer;
    private Function<QrInvoicePaymentPartReceiptCreator, QrInvoicePaymentPartReceiptCreator> creatorOptionsCallback;
    private Integer limit;

    public static QrInvoicePaymentPartReceiptBulkCreator create() {
        return new QrInvoicePaymentPartReceiptBulkCreator();
    }


    public QrInvoicePaymentPartReceiptBulkCreator csvSeparator(String csvSeparator) {
        if (csvSeparator == null) {
            return csvSeparator(DEFAULT_CSV_SEPARATOR);
        } else if (csvSeparator.length() == 1) {
            return csvSeparator(csvSeparator.charAt(0));
        } else {
            throw new ValidationException("csv separator must have a length of 1 but was " + csvSeparator.length());
        }
    }

    public QrInvoicePaymentPartReceiptBulkCreator csvSeparator(char csvSeparator) {
        this.csvSeparator = csvSeparator;
        return this;
    }

    public QrInvoicePaymentPartReceiptBulkCreator qrInvoiceConsumer(Consumer<QrInvoice> qrInvoiceConsumer) {
        this.qrInvoiceConsumer = qrInvoiceConsumer;
        return this;
    }

    public QrInvoicePaymentPartReceiptBulkCreator csvInput(char csvSeparator) {
        return csvSeparator(';').inputMimeType(MimeType.CSV);
    }

    public QrInvoicePaymentPartReceiptBulkCreator xlsxInput() {
        return inputMimeType(MimeType.XLSX);
    }

    public QrInvoicePaymentPartReceiptBulkCreator inputMimeType(final MimeType inputMimeType) {
        this.inputMimeType = inputMimeType;
        return this;
    }

    public QrInvoicePaymentPartReceiptBulkCreator inputStream(final InputStream inputStream) {
        if (this.qrInvoices != null) {
            throw new ValidationException("Either provide input in supported format as InputStream or with a list of QrInvoices - not both");
        }
        this.inputStream = inputStream;
        return this;
    }

    public QrInvoicePaymentPartReceiptBulkCreator qrInvoices(final List<QrInvoice> qrInvoices, final Function<QrInvoicePaymentPartReceiptCreator, QrInvoicePaymentPartReceiptCreator> creatorOptionsCallback) {
        if (this.inputStream != null) {
            throw new ValidationException("Either provide input in supported format as InputStream or with a list of QrInvoices - not both");
        }
        this.qrInvoices = qrInvoices;
        this.creatorOptionsCallback = creatorOptionsCallback;
        return this;
    }

    public QrInvoicePaymentPartReceiptBulkCreator limit(final int limit) {
        this.limit = limit;
        return this;
    }

    public QrInvoicePaymentPartReceiptBulkCreator invoiceEntryPreProcessor(final Consumer<InvoiceEntry> invoiceEntryPreProcessor) {
        this.invoiceEntryPreProcessor = invoiceEntryPreProcessor;
        return this;
    }

    public BulkOutput createAsZip() throws Exception {
        return createAsMimeType(MimeType.ZIP);
    }


    public BulkOutput createAsPdf() throws Exception {
        return createAsMimeType(MimeType.PDF);
    }

    public BulkOutput createAsMimeType(final MimeType outputMimeType) throws Exception {
        if (this.inputStream == null && this.qrInvoices == null) {
            throw new ValidationException("Missing input data");
        }
        if (this.inputStream != null) {
            return createFromInputStream(outputMimeType);
        } else {
            return createFromInvoicesList(outputMimeType);
        }
    }

    private BulkOutput createFromInputStream(final MimeType outputMimeType) throws Exception {
        final List<? extends InvoiceEntry> invoices = readInputStream();
        validateLimit(invoices);
        preProcessEntries(invoices);
        final Map<InvoiceEntry, QrInvoice> qrInvoices = mapInvoices(invoices);

        if (outputMimeType == MimeType.PDF) {
            // override output format - set PDF because we want to concatenate PDF output

            try (final PdfMerger pdfMerger = PdfMerger.create()) {
                for (final Map.Entry<InvoiceEntry, QrInvoice> entry : qrInvoices.entrySet()) {
                    final QrInvoice qrInvoice = entry.getValue();
                    final InvoiceEntry invoice = entry.getKey();

                    // override output format - set PDF because we want to concatenate PDF output
                    final PaymentPartReceipt paymentPartReceipt = QrInvoicePaymentPartReceiptCreator
                            .create()
                            .qrInvoice(qrInvoice)
                            .outputFormat(OutputFormat.PDF)
                            .outputResolution(invoice.getResolution())
                            .pageSize(invoice.getPageSize() != PageSize.DIN_LANG_CROPPED ? invoice.getPageSize() : PageSize.DIN_LANG)
                            .fontFamily(invoice.getFontFamily())
                            .locale(invoice.getLanguage())
                            .boundaryLines(toBoundaryLines(invoice))
                            .boundaryLineScissors(invoice.getBoundaryLineScissors())
                            .boundaryLineSeparationText(invoice.getBoundaryLineSeparationText())
                            .createPaymentPartReceipt();

                    pdfMerger.addPdf(paymentPartReceipt.getData());

                    acceptQrInvoiceConsumer(qrInvoice);
                }
                return new BulkOutput(OutputFormat.PDF, pdfMerger.getPdf(), qrInvoices.size());
            }
        } else if (outputMimeType == MimeType.ZIP) {
            try (final ZipBuilder zipBuilder = ZipBuilder.create()) {
                int nr = 1;
                for (final Map.Entry<InvoiceEntry, QrInvoice> entry : qrInvoices.entrySet()) {
                    final QrInvoice qrInvoice = entry.getValue();
                    final InvoiceEntry invoice = entry.getKey();

                    final PaymentPartReceipt paymentPartReceipt = QrInvoicePaymentPartReceiptCreator
                            .create()
                            .qrInvoice(qrInvoice)
                            .outputFormat(invoice.getOutputFormat())
                            .outputResolution(invoice.getResolution())
                            .pageSize(invoice.getPageSize())
                            .fontFamily(invoice.getFontFamily())
                            .locale(invoice.getLanguage())
                            .boundaryLines(toBoundaryLines(invoice))
                            .boundaryLineScissors(invoice.getBoundaryLineScissors())
                            .boundaryLineSeparationText(invoice.getBoundaryLineSeparationText())
                            .createPaymentPartReceipt();

                    final String filename;
                    if (StringUtils.isNotBlank(invoice.getOutputFilename())) {
                        filename = StringUtils.trim(invoice.getOutputFilename());
                    } else {
                        filename = String.format("QR-Invoice-%d.%s", nr++, paymentPartReceipt.getOutputFormat().getFileExtension());
                    }
                    zipBuilder.addFile(filename, paymentPartReceipt.getData());

                    acceptQrInvoiceConsumer(qrInvoice);
                }
                return new BulkOutput(OutputFormat.ZIP, zipBuilder.getZip(), qrInvoices.size());
            }
        } else {
            throw new NotYetImplementedException(String.format("Bulk output for mimeType '%s' is not yet supported", outputMimeType));
        }
    }


    private Map<InvoiceEntry, QrInvoice> mapInvoices(final List<? extends InvoiceEntry> invoices) {
        final BulkValidationResult bulkValidationResult = new BulkValidationResult();
        final Map<InvoiceEntry, QrInvoice> qrInvoices = new LinkedHashMap<>();
        for (final InvoiceEntry invoice : invoices) {
            try {
                qrInvoices.put(invoice, InvoiceEntryMapper.create().enableStringNormalization().map(invoice));
            } catch (ValidationException e) {
                bulkValidationResult.addValidationResult(invoice.getRowId(), e.getValidationResult());
            } catch (Exception e) {
                throw new RuntimeException("Unexpected error occurred processing invoice entry with rowId=" + invoice.getRowId(), e);
            }
        }
        bulkValidationResult.throwExceptionOnErrors();
        return qrInvoices;
    }

    private List<? extends InvoiceEntry> readInputStream() throws java.io.IOException, NoSuchMethodException, IllegalAccessException, java.lang.reflect.InvocationTargetException, InstantiationException {
        final List<? extends InvoiceEntry> invoices;
        switch (inputMimeType) {
            case CSV:
                invoices = Csv.readCsv(new InputStreamReader(inputStream, StandardCharsets.UTF_8), csvSeparator, CsvInvoiceEntry.class);
                break;
            case XLSX:
                invoices = Xlsx.readXlsx(inputStream, XlsxInvoiceEntry.class);
                break;
            default:
                throw new RuntimeException("Unsupported input mimeType=" + inputMimeType);
        }
        return invoices;
    }


    public BulkOutput createFromInvoicesList(final MimeType outputMimeType) throws Exception {
        validateLimit(qrInvoices);
        if (outputMimeType == MimeType.PDF) {
            try (final PdfMerger pdfMerger = PdfMerger.create()) {
                for (final QrInvoice invoice : qrInvoices) {
                    final PaymentPartReceipt paymentPartReceipt = creatorOptionsCallback.apply(QrInvoicePaymentPartReceiptCreator.create())
                            .qrInvoice(invoice)
                            .createPaymentPartReceipt();
                    pdfMerger.addPdf(paymentPartReceipt.getData());

                    acceptQrInvoiceConsumer(invoice);
                }
                return new BulkOutput(OutputFormat.PDF, pdfMerger.getPdf(), qrInvoices.size());
            }
        } else if (outputMimeType == MimeType.ZIP) {
            try (final ZipBuilder zipBuilder = ZipBuilder.create()) {
                int nr = 1;
                for (final QrInvoice invoice : qrInvoices) {
                    final PaymentPartReceipt paymentPartReceipt = creatorOptionsCallback.apply(QrInvoicePaymentPartReceiptCreator.create())
                            .qrInvoice(invoice)
                            .createPaymentPartReceipt();

                    zipBuilder.addFile(String.format("QR-Invoice-%d.%s", nr++, paymentPartReceipt.getOutputFormat().getFileExtension()), paymentPartReceipt.getData());

                    acceptQrInvoiceConsumer(invoice);
                }
                return new BulkOutput(OutputFormat.ZIP, zipBuilder.getZip(), qrInvoices.size());
            }
        } else {
            throw new NotYetImplementedException(String.format("Bulk output for mimeType '%s' is not yet supported", outputMimeType));
        }
    }

    private void acceptQrInvoiceConsumer(QrInvoice invoice) {
        if (qrInvoiceConsumer != null) {
            qrInvoiceConsumer.accept(invoice);
        }
    }

    public boolean isSupported(final MimeType mimeType) {
        return mimeType == MimeType.ZIP || mimeType == MimeType.PDF;
    }

    private BoundaryLines toBoundaryLines(final InvoiceEntry invoice) {
        if (invoice.getBoundaryLines() == null) {
            return BoundaryLines.NONE;
        }

        if (invoice.getBoundaryLines() && invoice.getBoundaryLineMargin()) {
            return BoundaryLines.ENABLED_WITH_MARGINS;
        } else if (invoice.getBoundaryLines()) {
            return BoundaryLines.ENABLED;
        } else {
            return BoundaryLines.NONE;
        }
    }

    private void validateLimit(final Collection<?> entries) {
        if (limit != null && entries != null && entries.size() > limit) {
            throw new ValidationException("A bulk number limit of " + limit + " was set but encountered " + entries.size() + " entries");
        }
    }

    private void preProcessEntries(final Collection<? extends InvoiceEntry> entries) {
        for (InvoiceEntry entry : entries) {
            if (invoiceEntryPreProcessor != null) {
                invoiceEntryPreProcessor.accept(entry);
            }
        }
    }
}
