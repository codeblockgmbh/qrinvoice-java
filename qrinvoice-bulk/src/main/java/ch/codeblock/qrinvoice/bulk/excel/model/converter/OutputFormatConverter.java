package ch.codeblock.qrinvoice.bulk.excel.model.converter;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.util.StringUtils;

public class OutputFormatConverter implements ValueConverter<OutputFormat, Object> {
    @Override
    public OutputFormat convert(final Object value) {
        if (value instanceof OutputFormat) {
            return (OutputFormat) value;
        } else if (value instanceof String) {
            final String normalizedValue = StringUtils.trimToEmpty((String) value).toUpperCase();
            if (!normalizedValue.isEmpty()) {
                if ("JPG".equalsIgnoreCase(normalizedValue)) {
                    return OutputFormat.JPEG;
                }
                try {
                    return OutputFormat.valueOf(normalizedValue);
                } catch (IllegalArgumentException e) {
                    throw new RuntimeException("Unsupported Output Format: " + value, e);
                }
            }
        }
        return null;
    }
}
