package ch.codeblock.qrinvoice.bulk.csv.model;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.bulk.csv.model.converter.*;
import ch.codeblock.qrinvoice.bulk.model.AddressType;
import ch.codeblock.qrinvoice.bulk.model.DefaultValues;
import ch.codeblock.qrinvoice.bulk.model.InvoiceEntry;
import ch.codeblock.qrinvoice.model.ReferenceType;
import ch.codeblock.qrinvoice.util.StringUtils;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import com.opencsv.bean.FieldMap;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.opencsv.exceptions.CsvValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

import static ch.codeblock.qrinvoice.bulk.model.ColumnKeys.*;

/**
 * Represents all information required for a single bill
 */
public class CsvInvoiceEntry implements InvoiceEntry {
    private static final Logger LOGGER = LoggerFactory.getLogger(CsvInvoiceEntry.class);

    private Integer rowId;

    @CsvBindByName(column = OUTPUT_FILENAME)
    private String outputFilename;

    // Options
    @CsvCustomBindByName(column = LANGUAGE, converter = LocaleConverter.class)
    private Locale language = Locale.GERMAN;

    @CsvCustomBindByName(column = FONT, converter = FontFamilyConverter.class)
    private FontFamily fontFamily = FontFamily.LIBERATION_SANS;

    @CsvCustomBindByName(column = FORMAT, converter = OutputFormatConverter.class)
    private OutputFormat outputFormat = OutputFormat.PDF;

    @CsvCustomBindByName(column = RESOLUTION, converter = OutputResolutionConverter.class)
    private OutputResolution resolution = OutputResolution.MEDIUM_300_DPI;

    @CsvCustomBindByName(column = PAGE_SIZE, converter = PageSizeConverter.class)
    private PageSize pageSize = PageSize.A4;

    @CsvCustomBindByName(column = BOUNDARY_LINES, converter = BooleanConverter.class)
    private Boolean boundaryLines = Boolean.TRUE;

    @CsvCustomBindByName(column = BOUNDARY_LINE_SCISSORS, converter = BooleanConverter.class)
    private Boolean boundaryLineScissors = Boolean.FALSE;

    @CsvCustomBindByName(column = SEPARATION_TEXT, converter = BooleanConverter.class)
    private Boolean boundaryLineSeparationText = Boolean.FALSE;

    @CsvCustomBindByName(column = BOUNDARY_LINE_MARGINS, converter = BooleanConverter.class)
    private Boolean boundaryLineMargin = Boolean.FALSE;

    // Creditor Information
    @CsvBindByName(column = IBAN, required = true)
    private String iban;

    // Creditor address
    @CsvCustomBindByName(column = CR_ADDRESS_TYPE, required = true, converter = AddressTypeConverter.class)
    private AddressType creditorAddressType;

    @CsvBindByName(column = CR_NAME, required = true)
    private String creditorName;

    @CsvBindByName(column = CR_STREET_OR_ADDRESS_LINE1)
    private String creditorStreetnameAddressLine1;

    @CsvBindByName(column = CR_BUILDING_NR_OR_ADDRESS_LINE2)
    private String creditorStreetnumberAddressLine2;

    @CsvBindByName(column = CR_POSTAL_CODE)
    private String creditorPostalCode;

    @CsvBindByName(column = CR_CITY)
    private String creditorCity;

    @CsvBindByName(column = CR_COUNTRY, required = true)
    private String creditorCountry;

    // Payment Amount Information
    @CsvBindByName(column = AMOUNT)
    private BigDecimal amount;

    @CsvCustomBindByName(column = CURRENCY, required = true, converter = CurrencyConverter.class)
    private Currency currency;

    // Debtor for the QR payment part
    @CsvCustomBindByName(column = UD_ADDRESS_TYPE, converter = AddressTypeConverter.class)
    private AddressType debtorAddressType;

    @CsvBindByName(column = UD_NAME)
    private String debtorName;

    @CsvBindByName(column = UD_STREET_OR_ADDRESS_LINE1)
    private String debtorStreetnameAddressLine1;

    @CsvBindByName(column = UD_BUILDING_NR_OR_ADDRESS_LINE2)
    private String debtorStreetnumberAddressLine2;

    @CsvBindByName(column = UD_POSTAL_CODE)
    private String debtorPostalCode;

    @CsvBindByName(column = UD_CITY)
    private String debtorCity;

    @CsvBindByName(column = UD_COUNTRY)
    private String debtorCountry;

    // Payment Reference
    @CsvCustomBindByName(column = REFERENCE_TYPE, required = true, converter = ReferenceTypeConverter.class)
    private ReferenceType referenceType;

    @CsvBindByName(column = REFERENCE)
    private String referenceNumber;

    @CsvBindByName(column = UNSTRUCTURED_MESSAGE)
    private String unstructuredMessage;

    @CsvBindByName(column = BILLING_INFORMATION)
    private String billInformation;

    @CsvBindByName(column = AP1)
    private String altPmt1;

    @CsvBindByName(column = AP2)
    private String altPmt2;

    public static void applyDefaults(Object o, FieldMap<String, String, ?, ?> fieldMap) {
        ALL_COLUMN_KEYS.forEach(columnKey -> applyDefault(o, fieldMap, columnKey));
    }

    private static void applyDefault(Object o, FieldMap<String, String, ?, ?> fieldMap, String columnKey) {
        final String defaultValue = DefaultValues.getBulkDefault(columnKey);
        if (StringUtils.isNotBlank(defaultValue)) {
            try {
                fieldMap.get(columnKey).setFieldValue(o, defaultValue, columnKey);
            } catch (CsvDataTypeMismatchException |
                     CsvRequiredFieldEmptyException |
                     CsvConstraintViolationException | CsvValidationException e) {
                LOGGER.info("Unexpected exception trying to apply default values for column={}", columnKey, e);
            }
        }
    }

    private static final Set<String> REQUIRED_HEADERS = new HashSet<>();
    private static final Map<String, Field> FIELD_MAP = new HashMap<>();

    static {
        final Field[] fields = CsvInvoiceEntry.class.getDeclaredFields();
        for (final Field field : fields) {
            final CsvCustomBindByName csvCustomBindByName = field.getAnnotation(CsvCustomBindByName.class);
            if (csvCustomBindByName != null) {
                registerColumn(csvCustomBindByName.column(), field, csvCustomBindByName.required());
            }

            final CsvBindByName csvBindByName = field.getAnnotation(CsvBindByName.class);
            if (csvBindByName != null) {
                registerColumn(csvBindByName.column(), field, csvBindByName.required());
            }
        }
    }

    private static void registerColumn(String csvBindByName, Field field, boolean csvBindByName1) {
        if (FIELD_MAP.containsKey(csvBindByName)) {
            throw new RuntimeException("Duplicate column definition: " + csvBindByName);
        }

        FIELD_MAP.put(csvBindByName, field);

        if (csvBindByName1) {
            REQUIRED_HEADERS.add(csvBindByName);
        }
    }

    @Override
    public Integer getRowId() {
        return rowId;
    }

    public void setRowId(final Integer rowId) {
        this.rowId = rowId;
    }

    @Override
    public String getOutputFilename() {
        return outputFilename;
    }

    public void setOutputFilename(String outputFilename) {
        this.outputFilename = outputFilename;
    }

    @Override
    public Locale getLanguage() {
        return language;
    }

    public void setLanguage(final Locale language) {
        this.language = language;
    }

    @Override
    public FontFamily getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(final FontFamily fontFamily) {
        this.fontFamily = fontFamily;
    }

    @Override
    public OutputFormat getOutputFormat() {
        return outputFormat;
    }

    public void setOutputFormat(final OutputFormat outputFormat) {
        this.outputFormat = outputFormat;
    }

    @Override
    public OutputResolution getResolution() {
        return resolution;
    }

    public void setResolution(final OutputResolution resolution) {
        this.resolution = resolution;
    }

    @Override
    public PageSize getPageSize() {
        return pageSize;
    }

    public void setPageSize(final PageSize pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public Boolean getBoundaryLines() {
        return boundaryLines;
    }

    public void setBoundaryLines(final Boolean boundaryLines) {
        this.boundaryLines = boundaryLines;
    }

    @Override
    public Boolean getBoundaryLineScissors() {
        return boundaryLineScissors;
    }

    public void setBoundaryLineScissors(final Boolean boundaryLineScissors) {
        this.boundaryLineScissors = boundaryLineScissors;
    }

    @Override
    public Boolean getBoundaryLineSeparationText() {
        return boundaryLineSeparationText;
    }

    public void setBoundaryLineSeparationText(final Boolean boundaryLineSeparationText) {
        this.boundaryLineSeparationText = boundaryLineSeparationText;
    }

    @Override
    public Boolean getBoundaryLineMargin() {
        return boundaryLineMargin;
    }

    public void setBoundaryLineMargin(final Boolean boundaryLineMargin) {
        this.boundaryLineMargin = boundaryLineMargin;
    }

    @Override
    public String getIban() {
        return iban;
    }

    public void setIban(final String iban) {
        this.iban = iban;
    }

    @Override
    public AddressType getCreditorAddressType() {
        return creditorAddressType;
    }

    public void setCreditorAddressType(final AddressType creditorAddressType) {
        this.creditorAddressType = creditorAddressType;
    }

    @Override
    public String getCreditorName() {
        return creditorName;
    }

    public void setCreditorName(final String creditorName) {
        this.creditorName = creditorName;
    }

    @Override
    public String getCreditorStreetnameAddressLine1() {
        return creditorStreetnameAddressLine1;
    }

    public void setCreditorStreetnameAddressLine1(final String creditorStreetnameAddressLine1) {
        this.creditorStreetnameAddressLine1 = creditorStreetnameAddressLine1;
    }

    @Override
    public String getCreditorStreetnumberAddressLine2() {
        return creditorStreetnumberAddressLine2;
    }

    public void setCreditorStreetnumberAddressLine2(final String creditorStreetnumberAddressLine2) {
        this.creditorStreetnumberAddressLine2 = creditorStreetnumberAddressLine2;
    }

    @Override
    public String getCreditorPostalCode() {
        return creditorPostalCode;
    }

    public void setCreditorPostalCode(final String creditorPostalCode) {
        this.creditorPostalCode = creditorPostalCode;
    }

    @Override
    public String getCreditorCity() {
        return creditorCity;
    }

    public void setCreditorCity(final String creditorCity) {
        this.creditorCity = creditorCity;
    }

    @Override
    public String getCreditorCountry() {
        return creditorCountry;
    }

    public void setCreditorCountry(final String creditorCountry) {
        this.creditorCountry = creditorCountry;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    @Override
    public AddressType getDebtorAddressType() {
        return debtorAddressType;
    }

    public void setDebtorAddressType(final AddressType debtorAddressType) {
        this.debtorAddressType = debtorAddressType;
    }

    @Override
    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(final String debtorName) {
        this.debtorName = debtorName;
    }

    @Override
    public String getDebtorStreetnameAddressLine1() {
        return debtorStreetnameAddressLine1;
    }

    public void setDebtorStreetnameAddressLine1(final String debtorStreetnameAddressLine1) {
        this.debtorStreetnameAddressLine1 = debtorStreetnameAddressLine1;
    }

    @Override
    public String getDebtorStreetnumberAddressLine2() {
        return debtorStreetnumberAddressLine2;
    }

    public void setDebtorStreetnumberAddressLine2(final String debtorStreetnumberAddressLine2) {
        this.debtorStreetnumberAddressLine2 = debtorStreetnumberAddressLine2;
    }

    @Override
    public String getDebtorPostalCode() {
        return debtorPostalCode;
    }

    public void setDebtorPostalCode(final String debtorPostalCode) {
        this.debtorPostalCode = debtorPostalCode;
    }

    @Override
    public String getDebtorCity() {
        return debtorCity;
    }

    public void setDebtorCity(final String debtorCity) {
        this.debtorCity = debtorCity;
    }

    @Override
    public String getDebtorCountry() {
        return debtorCountry;
    }

    public void setDebtorCountry(final String debtorCountry) {
        this.debtorCountry = debtorCountry;
    }

    @Override
    public ReferenceType getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(final ReferenceType referenceType) {
        this.referenceType = referenceType;
    }

    @Override
    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    @Override
    public String getUnstructuredMessage() {
        return unstructuredMessage;
    }

    public void setUnstructuredMessage(final String unstructuredMessage) {
        this.unstructuredMessage = unstructuredMessage;
    }

    @Override
    public String getBillInformation() {
        return billInformation;
    }

    public void setBillInformation(final String billInformation) {
        this.billInformation = billInformation;
    }

    @Override
    public String getAltPmt1() {
        return altPmt1;
    }

    public void setAltPmt1(final String altPmt1) {
        this.altPmt1 = altPmt1;
    }

    @Override
    public String getAltPmt2() {
        return altPmt2;
    }

    public void setAltPmt2(final String altPmt2) {
        this.altPmt2 = altPmt2;
    }

    @Override
    public Set<String> getRequiredHeaders() {
        return REQUIRED_HEADERS;
    }

    @Override
    public void setFieldValue(final String column, final String value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setFieldValue(final String column, final Boolean value) {
        throw new UnsupportedOperationException();
    }

}
