package ch.codeblock.qrinvoice.bulk.csv.model.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class BooleanConverter<T, I> extends AbstractBeanField<T, I> {
    public BooleanConverter() {
    }

    protected Object convert(String value) throws CsvDataTypeMismatchException {
        return new ch.codeblock.qrinvoice.bulk.excel.model.converter.BooleanConverter().convert(value);
    }
}
