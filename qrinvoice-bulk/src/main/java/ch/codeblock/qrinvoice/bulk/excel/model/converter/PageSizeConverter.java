package ch.codeblock.qrinvoice.bulk.excel.model.converter;

import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.util.StringUtils;

public class PageSizeConverter implements ValueConverter<PageSize, Object> {
    @Override
    public PageSize convert(final Object value) {
        if (value instanceof PageSize) {
            return (PageSize) value;
        } else if (value instanceof String) {
            final String normalizedValue = StringUtils.trimToEmpty((String) value).toUpperCase().replace('-', '_').replace(' ', '_');
            if (!normalizedValue.isEmpty()) {
                return PageSize.valueOf(normalizedValue);
            }
        }
        return null;
    }
}
