package ch.codeblock.qrinvoice.bulk.csv;

import ch.codeblock.qrinvoice.bulk.csv.bean.CsvInvoiceEntryMappingStrategy;
import ch.codeblock.qrinvoice.bulk.excel.Xlsx;
import ch.codeblock.qrinvoice.bulk.model.validation.InvalidHeaderException;
import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.util.IOUtils;
import ch.codeblock.qrinvoice.util.StringUtils;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Csv {
    private static final Logger LOGGER = LoggerFactory.getLogger(Csv.class);

    private static final Pattern MISSING_HEADER_FIELD_PATTERN = Pattern.compile("^Header is missing required fields \\[([^]]+)]");

    /**
     * @param inputStream
     * @param charset
     * @param separator
     * @param modelClass
     * @param <T>
     * @return
     */
    public static <T> List<T> readCsv(InputStream inputStream, final Charset charset, final char separator, final Class<T> modelClass) {
        return readCsv(new InputStreamReader(inputStream, charset), separator, modelClass);
    }

    /**
     * Parses a CSV file into Invoice objects
     *
     * @param inputStreamReader
     * @param separator
     * @param modelClass
     * @return List of Invoice objects
     */
    public static <T> List<T> readCsv(InputStreamReader inputStreamReader, final char separator, final Class<T> modelClass) {
        try {
            return new CsvToBeanBuilder<T>(inputStreamReader)
                    .withType(modelClass)
                    .withSeparator(separator)
                    .withIgnoreEmptyLine(true)
                    .withMappingStrategy(new CsvInvoiceEntryMappingStrategy<>())
                    .build()
                    .parse();
        } catch (RuntimeException e) {
            if (e.getCause() instanceof CsvRequiredFieldEmptyException) {
                final Matcher matcher = MISSING_HEADER_FIELD_PATTERN.matcher(e.getCause().getMessage());
                if (matcher.find()) {
                    throw new InvalidHeaderException(matcher.group(1).split(","));
                } else {
                    throw new InvalidHeaderException(Collections.singleton("Unable to determine missing fields"));
                }
            } else {
                throw e;
            }
        }
    }

    public static byte[] getExample() throws IOException {
        final String externalFilePath = System.getProperty(SystemProperties.BULK_CSV_EXAMPLE_FILE_PATH);
        if (StringUtils.isNotBlank(externalFilePath)) {
            LOGGER.info("Custom external bulk CSV example file configured: {}", externalFilePath);
            return Files.readAllBytes(Paths.get(externalFilePath));
        } else {
            return IOUtils.toByteArray(Xlsx.class.getResourceAsStream("/ch/codeblock/qrinvoice/bulk/csv/example.csv"));
        }
    }
}
