package ch.codeblock.qrinvoice.bulk.excel.model.converter;

public class NoOpConverter implements ValueConverter<Object, Object> {
    @Override
    public Object convert(final Object value) {
        return value;
    }
}
