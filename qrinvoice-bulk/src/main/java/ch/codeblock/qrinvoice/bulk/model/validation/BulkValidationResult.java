/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.bulk.model.validation;

import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import ch.codeblock.qrinvoice.util.CollectionUtils;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class BulkValidationResult implements Serializable {

    private static final long serialVersionUID = 4241612126355208249L;

    private LinkedHashMap<Integer, ValidationResult> validationResults;


    public void addValidationResult(final Integer rowId, final ValidationResult error) {
        if (error.isValid()) {
            return;
        }

        if (validationResults == null) {
            validationResults = new LinkedHashMap<>();
        }
        // do not accept duplicates
        if (!validationResults.containsValue(error)) {
            validationResults.put(rowId, error);
        }
    }


    public boolean isEmpty() {
        return CollectionUtils.isEmpty(validationResults);
    }

    public boolean isValid() {
        return isEmpty();
    }

    public boolean hasErrors() {
        return !isValid();
    }

    public Map<Integer, ValidationResult> getValidationResults() {
        return validationResults;
    }

    /**
     * if there are {@link #getValidationResults()}, throw a {@link BulkValidationException}, otherwise it is a silent operation
     *
     * @throws BulkValidationException in case there are validation errors present
     */
    public void throwExceptionOnErrors() throws BulkValidationException {
        if (!isEmpty()) {
            throw new BulkValidationException(this);
        }
    }

    public String getValidationErrorSummary() {
        if (hasErrors()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Bulk QR Invoices have validation errors:");
            sb.append(System.lineSeparator());

            int errorNr = 1;
            for (final Map.Entry<Integer, ValidationResult> entry : validationResults.entrySet()) {
                if (errorNr++ > 1) {
                    sb.append(System.lineSeparator());
                }
                final Integer rowId = entry.getKey();
                final ValidationResult validationResult = entry.getValue();
                sb.append("Row ").append(rowId).append(":").append(System.lineSeparator());

                validationResult.appendValidationErrors(sb);
            }
            return sb.toString();
        } else {
            return "No validation errors";
        }
    }

}
