package ch.codeblock.qrinvoice.bulk.model.validation;

import ch.codeblock.qrinvoice.BaseException;

public class BulkValidationException extends BaseException {
    private static final long serialVersionUID = -5041306089668923762L;
    private final BulkValidationResult bulkValidationResult;

    public BulkValidationException(final BulkValidationResult bulkValidationResult) {
        super(bulkValidationResult.getValidationErrorSummary());
        this.bulkValidationResult = bulkValidationResult;
    }

    public BulkValidationResult getBulkValidationResult() {
        return bulkValidationResult;
    }
}
