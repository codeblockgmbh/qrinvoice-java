package ch.codeblock.qrinvoice.bulk.csv.bean;

import ch.codeblock.qrinvoice.bulk.csv.model.CsvInvoiceEntry;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.exceptions.CsvBeanIntrospectionException;

import java.util.Map;

public class CsvInvoiceEntryMappingStrategy<T> extends HeaderColumnNameMappingStrategy<T> {
    public CsvInvoiceEntryMappingStrategy() {
        super();
        setType((Class<? extends T>) CsvInvoiceEntry.class);
    }
    
    @Override
    protected Map<Class<?>, Object> createBean() throws CsvBeanIntrospectionException, IllegalStateException {
        final Map<Class<?>, Object> bean = super.createBean();
        final Object o = bean.get(CsvInvoiceEntry.class);
        if(o instanceof CsvInvoiceEntry) {
            CsvInvoiceEntry.applyDefaults(o, getFieldMap());
        }
        return bean;
    }
}
