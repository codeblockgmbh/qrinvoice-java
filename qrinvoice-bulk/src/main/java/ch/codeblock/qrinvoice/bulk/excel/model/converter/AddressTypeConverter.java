package ch.codeblock.qrinvoice.bulk.excel.model.converter;

import ch.codeblock.qrinvoice.bulk.model.AddressType;

public class AddressTypeConverter implements ValueConverter<AddressType, String> {

    public AddressType convert(String value) {
        final AddressType type = AddressType.valueOfLabel(value);

        if (type == null) {
            throw new RuntimeException("Unknown address type: " + value);
        }

        return type;
    }
}
