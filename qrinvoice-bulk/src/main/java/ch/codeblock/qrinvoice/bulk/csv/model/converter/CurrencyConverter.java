package ch.codeblock.qrinvoice.bulk.csv.model.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class CurrencyConverter<T, I> extends AbstractBeanField<T, I> {
    public CurrencyConverter() {
    }

    protected Object convert(String value) throws CsvDataTypeMismatchException {
        return new ch.codeblock.qrinvoice.bulk.excel.model.converter.CurrencyConverter().convert(value);
    }
}
