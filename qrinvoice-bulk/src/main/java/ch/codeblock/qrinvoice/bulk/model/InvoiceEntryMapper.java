package ch.codeblock.qrinvoice.bulk.model;

import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.model.util.StringNormalizer;
import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static ch.codeblock.qrinvoice.bulk.model.ColumnKeys.*;

public class InvoiceEntryMapper {
    private final Logger logger = LoggerFactory.getLogger(InvoiceEntryMapper.class);
    private final boolean forceStreetNames;

    public static InvoiceEntryMapper create() {
        return new InvoiceEntryMapper();
    }

    private InvoiceEntryMapper() {
        this.forceStreetNames = Boolean.parseBoolean(System.getProperty(SystemProperties.BULK_FORCE_STREETS));
    }

    private final StringNormalizer stringNormalizer = StringNormalizer.create();

    public QrInvoice map(final InvoiceEntry invoice) {
        final ValidationResult validationResult = new ValidationResult();

        final QrInvoiceBuilder qrInvoiceBuilder = QrInvoiceBuilder
                .create()
                .creditorIBAN(stringNormalizer.normalize(invoice.getIban()))
                .paymentAmountInformation(p -> p
                        .currency(invoice.getCurrency())
                        .amount(invoice.getAmount())
                );

        if (invoice.getCreditorAddressType() != null) {
            switch (invoice.getCreditorAddressType()) {
                case COMBINED:
                    logCombinedAddressUsage();
                    validateCombinedCreditor(invoice, validationResult);
                    mapCombinedCreditor(invoice, qrInvoiceBuilder);
                    break;
                case STRUCTURED:
                    validateStructuredCreditor(invoice, validationResult);
                    mapStructuredCreditor(invoice, qrInvoiceBuilder);
                    break;
            }
        } else {
            validationResult.addError(CR_ADDRESS_TYPE, null, null, "{{validation.error.address.addressType}}");
        }

        mapPaymentReference(invoice, qrInvoiceBuilder);

        if (invoice.getDebtorAddressType() != null) {
            switch (invoice.getDebtorAddressType()) {
                case COMBINED:
                    logCombinedAddressUsage();
                    validateCombinedUltimateDebtor(invoice, validationResult);
                    mapCombinedUltimateDebtor(invoice, qrInvoiceBuilder);
                    break;
                case STRUCTURED:
                    validateStructuredUltimateDebtor(invoice, validationResult);
                    mapStructuredUltimateDebtor(invoice, qrInvoiceBuilder);
                    break;
            }
        } else if (StringUtils.isNotBlank(invoice.getDebtorName()) ||
                StringUtils.isNotBlank(invoice.getDebtorStreetnameAddressLine1()) ||
                StringUtils.isNotBlank(invoice.getDebtorStreetnumberAddressLine2()) ||
                StringUtils.isNotBlank(invoice.getDebtorPostalCode()) ||
                StringUtils.isNotBlank(invoice.getDebtorCity()) ||
                StringUtils.isNotBlank(invoice.getDebtorCountry())
        ) {
            validationResult.addError(UD_ADDRESS_TYPE, null, null, "{{validation.error.address.addressType}}");
        }

        mapAdditionalInformation(invoice, qrInvoiceBuilder);

        mapAlternativeSchemeParameter(invoice, qrInvoiceBuilder);

        validationResult.throwExceptionOnErrors();
        return qrInvoiceBuilder.build();
    }

    public void mapPaymentReference(final InvoiceEntry invoice, final QrInvoiceBuilder qrInvoiceBuilder) {
        qrInvoiceBuilder.paymentReference(r -> r
                .referenceType(invoice.getReferenceType())
                .reference(stringNormalizer.normalize(invoice.getReferenceNumber()))
        );
    }

    public void validateCombinedUltimateDebtor(final InvoiceEntry invoice, final ValidationResult validationResult) {
        if (StringUtils.isNotBlank(invoice.getDebtorPostalCode())) {
            validationResult.addError(UD_POSTAL_CODE, null, invoice.getDebtorPostalCode(), "{{validation.error.address.combined.postalCode}}");
        }
        if (StringUtils.isNotBlank(invoice.getDebtorCity())) {
            validationResult.addError(UD_CITY, null, invoice.getDebtorCity(), "{{validation.error.address.combined.city}}");
        }
        if (forceStreetNames) {
            if (StringUtils.isBlank(invoice.getDebtorStreetnameAddressLine1())) {
                validationResult.addError(UD_STREET_OR_ADDRESS_LINE1, null, invoice.getDebtorStreetnameAddressLine1(), "{{validation.error.address.streetNameHouseNumberForce}}");
            }
        }
    }

    public void validateStructuredUltimateDebtor(final InvoiceEntry invoice, final ValidationResult validationResult) {
        if (forceStreetNames) {
            if (StringUtils.isBlank(invoice.getDebtorStreetnameAddressLine1())) {
                validationResult.addError(UD_STREET_OR_ADDRESS_LINE1, null, invoice.getDebtorStreetnameAddressLine1(), "{{validation.error.address.streetNameHouseNumberForce}}");
            }
            if (StringUtils.isBlank(invoice.getDebtorStreetnumberAddressLine2())) {
                validationResult.addError(UD_BUILDING_NR_OR_ADDRESS_LINE2, null, invoice.getDebtorStreetnumberAddressLine2(), "{{validation.error.address.streetNameHouseNumberForce}}");
            }
        }
    }

    public void validateCombinedCreditor(final InvoiceEntry invoice, final ValidationResult validationResult) {
        if (StringUtils.isNotBlank(invoice.getCreditorPostalCode())) {
            validationResult.addError(CR_POSTAL_CODE, null, invoice.getCreditorPostalCode(), "{{validation.error.address.combined.postalCode}}");
        }
        if (StringUtils.isNotBlank(invoice.getCreditorCity())) {
            validationResult.addError(CR_CITY, null, invoice.getCreditorCity(), "{{validation.error.address.combined.city}}");
        }
        if (forceStreetNames) {
            if (StringUtils.isBlank(invoice.getCreditorStreetnameAddressLine1())) {
                validationResult.addError(CR_STREET_OR_ADDRESS_LINE1, null, invoice.getCreditorStreetnameAddressLine1(), "{{validation.error.address.streetNameHouseNumberForce}}");
            }
        }
    }

    public void validateStructuredCreditor(final InvoiceEntry invoice, final ValidationResult validationResult) {
        if (forceStreetNames) {
            if (StringUtils.isBlank(invoice.getCreditorStreetnameAddressLine1())) {
                validationResult.addError(CR_STREET_OR_ADDRESS_LINE1, null, invoice.getCreditorStreetnameAddressLine1(), "{{validation.error.address.streetNameHouseNumberForce}}");
            }
            if (StringUtils.isBlank(invoice.getCreditorStreetnumberAddressLine2())) {
                validationResult.addError(CR_BUILDING_NR_OR_ADDRESS_LINE2, null, invoice.getCreditorStreetnumberAddressLine2(), "{{validation.error.address.streetNameHouseNumberForce}}");
            }
        }
    }

    /**
     * @deprecated see {@link ch.codeblock.qrinvoice.model.AddressType#COMBINED}
     */
    @Deprecated
    public void mapCombinedCreditor(final InvoiceEntry invoice, final QrInvoiceBuilder qrInvoiceBuilder) {
        qrInvoiceBuilder.creditor(c -> c
                .combinedAddress()
                .name(stringNormalizer.normalize(invoice.getCreditorName()))
                .addressLine1(stringNormalizer.normalize(invoice.getCreditorStreetnameAddressLine1()))
                .addressLine2(stringNormalizer.normalize(invoice.getCreditorStreetnumberAddressLine2()))
                .country(stringNormalizer.normalize(invoice.getCreditorCountry())));
    }

    public void mapStructuredCreditor(final InvoiceEntry invoice, final QrInvoiceBuilder qrInvoiceBuilder) {
        qrInvoiceBuilder.creditor(c -> c
                .structuredAddress()
                .name(stringNormalizer.normalize(invoice.getCreditorName()))
                .streetName(stringNormalizer.normalize(invoice.getCreditorStreetnameAddressLine1()))
                .houseNumber(stringNormalizer.normalize(invoice.getCreditorStreetnumberAddressLine2()))
                .postalCode(stringNormalizer.normalize((invoice.getCreditorPostalCode())))
                .city(stringNormalizer.normalize(invoice.getCreditorCity()))
                .country(stringNormalizer.normalize(invoice.getCreditorCountry())));
    }

    /**
     * @deprecated see {@link ch.codeblock.qrinvoice.model.AddressType#COMBINED}
     */
    @Deprecated
    public void mapCombinedUltimateDebtor(final InvoiceEntry invoice, final QrInvoiceBuilder qrInvoiceBuilder) {
        qrInvoiceBuilder.ultimateDebtor(d -> d
                .combinedAddress()
                .name(stringNormalizer.normalize(invoice.getDebtorName()))
                .addressLine1(stringNormalizer.normalize(invoice.getDebtorStreetnameAddressLine1()))
                .addressLine2(stringNormalizer.normalize(invoice.getDebtorStreetnumberAddressLine2()))
                .country(stringNormalizer.normalize((invoice.getDebtorCountry()))));
    }

    public void mapStructuredUltimateDebtor(final InvoiceEntry invoice, final QrInvoiceBuilder qrInvoiceBuilder) {
        qrInvoiceBuilder.ultimateDebtor(d -> d
                .structuredAddress()
                .name(stringNormalizer.normalize((invoice.getDebtorName())))
                .streetName(stringNormalizer.normalize(invoice.getDebtorStreetnameAddressLine1()))
                .houseNumber(stringNormalizer.normalize(invoice.getDebtorStreetnumberAddressLine2()))
                .postalCode(stringNormalizer.normalize(invoice.getDebtorPostalCode()))
                .city(stringNormalizer.normalize(invoice.getDebtorCity()))
                .country(stringNormalizer.normalize(invoice.getDebtorCountry())));
    }

    public void mapAdditionalInformation(final InvoiceEntry invoice, final QrInvoiceBuilder qrInvoiceBuilder) {
        qrInvoiceBuilder.additionalInformation(a -> {
            a.unstructuredMessage(stringNormalizer.normalize(invoice.getUnstructuredMessage()))
                    .billInformation(stringNormalizer.normalize(invoice.getBillInformation()));
        });
    }

    public void mapAlternativeSchemeParameter(final InvoiceEntry invoice, final QrInvoiceBuilder qrInvoiceBuilder) {
        if (StringUtils.isNotBlank(invoice.getAltPmt1()) || StringUtils.isNotBlank(invoice.getAltPmt2())) {
            final List<String> alternativeSchemeParameters = new ArrayList<>();

            if (StringUtils.isNotBlank(invoice.getAltPmt1())) {
                alternativeSchemeParameters.add(stringNormalizer.normalize(invoice.getAltPmt1()));
            }
            if (StringUtils.isNotBlank(invoice.getAltPmt2())) {
                alternativeSchemeParameters.add(stringNormalizer.normalize(invoice.getAltPmt2()));

            }
            qrInvoiceBuilder.alternativeSchemeParameters(alternativeSchemeParameters);
        }
    }

    public StringNormalizer getStringNormalizer() {
        return stringNormalizer;
    }

    public InvoiceEntryMapper enableStringNormalization() {
        stringNormalizer.enableAll();
        return this;
    }

    private void logCombinedAddressUsage() {
        logger.warn("Combined address is used (phase out november 2025");
    }
}
