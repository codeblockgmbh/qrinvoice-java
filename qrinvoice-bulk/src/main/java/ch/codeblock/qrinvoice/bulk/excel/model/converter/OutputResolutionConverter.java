package ch.codeblock.qrinvoice.bulk.excel.model.converter;

import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.util.StringUtils;

public class OutputResolutionConverter implements ValueConverter<OutputResolution, Object> {
    @Override
    public OutputResolution convert(final Object value) {
        if (value instanceof OutputResolution) {
            return (OutputResolution) value;
        } else if (value instanceof Number) {
            return fromNumber((Number) value);
        } else if (value instanceof String) {
            final String normalizedValue = StringUtils.trimToEmpty((String) value).toUpperCase().replace('-', '_').replace(' ', '_');
            if (!normalizedValue.isEmpty()) {
                try {
                    fromNumber(Integer.valueOf(normalizedValue));
                } catch (NumberFormatException nfe) {
                    // ignore
                }
                for (final OutputResolution outputResolution : OutputResolution.values()) {
                    if (outputResolution.name().startsWith(normalizedValue)) {
                        return outputResolution;
                    }
                }
                return OutputResolution.valueOf(normalizedValue);
            }
        }
        return null;
    }

    private OutputResolution fromNumber(final Number value) {
        if (value == null) {
            return null;
        }

        for (final OutputResolution outputResolution : OutputResolution.values()) {
            if (outputResolution.getDpi() == value.intValue()) {
                return outputResolution;
            }
        }
        return null;
    }
}
