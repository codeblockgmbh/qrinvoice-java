package ch.codeblock.qrinvoice.bulk.csv.model.converter;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class ReferenceTypeConverter<T, I> extends AbstractBeanField<T, I> {
    public ReferenceTypeConverter() {
    }

    protected Object convert(String value) throws CsvDataTypeMismatchException {
        return new ch.codeblock.qrinvoice.bulk.excel.model.converter.ReferenceTypeConverter().convert(value);
    }
}
