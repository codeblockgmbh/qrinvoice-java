package ch.codeblock.qrinvoice.bulk.model.validation;

import ch.codeblock.qrinvoice.BaseException;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class InvalidHeaderException extends BaseException {
    private static final long serialVersionUID = -896765680123562963L;

    public InvalidHeaderException(final Collection<String> missingHeaders) {
        super(toMessage(missingHeaders));
    }

    public InvalidHeaderException(String... missingHeaders) {
        this(Arrays.asList(missingHeaders));
    }

    private static String toMessage(Collection<String> missingHeaders) {
        final Set<String> normalizedMissingHeaders = missingHeaders.stream().map(String::trim).collect(Collectors.toSet());
        return "The following required columns are missing: \n - " + String.join("\n - ", new TreeSet<>(normalizedMissingHeaders));
    }

}
