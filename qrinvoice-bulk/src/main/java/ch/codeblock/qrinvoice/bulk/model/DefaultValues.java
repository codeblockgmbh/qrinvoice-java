package ch.codeblock.qrinvoice.bulk.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static ch.codeblock.qrinvoice.bulk.model.ColumnKeys.*;
import static ch.codeblock.qrinvoice.config.SystemProperties.BULK_DEFAULTS_PREFIX;

/**
 * Provides default values to each non-present values in bulk input (CSV, XLSX)
 */
public class DefaultValues {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultValues.class);
    private static Map<String, String> bulkDefaults;

    static {
        reloadDefaultsMap();
    }

    public static void reloadDefaultsMap() {
        bulkDefaults = buildNewBulkDefaultsMap();
    }

    public static String getBulkDefault(String columnKey) {
        return bulkDefaults.get(columnKey);
    }

    public static Map<String, String> getBulkDefaults() {
        return Collections.unmodifiableMap(bulkDefaults);
    }

    public static Map<String, String> buildNewBulkDefaultsMap() {
        final Map<String, String> map = new HashMap<>();
        getSystemPropertyOrDefault(map, LANGUAGE, "GERMAN");
        getSystemPropertyOrDefault(map, FONT, "LIBERATION_SANS");
        getSystemPropertyOrDefault(map, FORMAT, "PDF");
        getSystemPropertyOrDefault(map, RESOLUTION, "MEDIUM_300_DPI");
        getSystemPropertyOrDefault(map, PAGE_SIZE, "A4");
        getSystemPropertyOrDefault(map, BOUNDARY_LINES, "TRUE");
        getSystemPropertyOrDefault(map, BOUNDARY_LINE_SCISSORS, "FALSE");
        getSystemPropertyOrDefault(map, SEPARATION_TEXT, "FALSE");
        getSystemPropertyOrDefault(map, BOUNDARY_LINE_MARGINS, "FALSE");

        // for the remaining read system property value, else use null
        ALL_COLUMN_KEYS.forEach(columnKey -> {
            if(!map.containsKey(columnKey)) {
                getSystemPropertyOrDefault(map, columnKey, null);
            }
        });
        return map;
    }

    public static void getSystemPropertyOrDefault(Map<String, String> map, String key, String defaultValue) {
        final String propertyValue = System.getProperty(BULK_DEFAULTS_PREFIX + key);
        if (propertyValue != null) {
            LOGGER.info("Bulk default value for column '{}' is set to '{}' overwriting default '{}'", key, propertyValue, defaultValue);
        }
        map.put(key, Optional.ofNullable(propertyValue).orElse(defaultValue));
    }
}
