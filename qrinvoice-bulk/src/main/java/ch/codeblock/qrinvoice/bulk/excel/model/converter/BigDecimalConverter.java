package ch.codeblock.qrinvoice.bulk.excel.model.converter;


import ch.codeblock.qrinvoice.util.StringUtils;

import java.math.BigDecimal;

public class BigDecimalConverter implements ValueConverter<BigDecimal, Object> {
    @Override
    public BigDecimal convert(final Object value) {
        if (value instanceof BigDecimal) {
            return (BigDecimal) value;
        } else if (value instanceof String) {
            final String trimmedValue = StringUtils.trimToNull((String) value);
            if (trimmedValue != null) {
                return new BigDecimal(trimmedValue);
            }
        }
        return null;
    }
}
