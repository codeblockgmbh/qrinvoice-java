package ch.codeblock.qrinvoice.bulk.excel.model.converter;


import ch.codeblock.qrinvoice.util.StringUtils;

import java.util.Currency;

public class CurrencyConverter implements ValueConverter<Currency, Object> {
    @Override
    public Currency convert(final Object value) {
        if (value instanceof Currency) {
            return (Currency) value;
        } else if (value instanceof String) {
            final String normalizedValue = StringUtils.trimToEmpty((String) value).toUpperCase();
            if (!normalizedValue.isEmpty()) {
                return Currency.getInstance(normalizedValue);
            }
        }
        return null;
    }
}
