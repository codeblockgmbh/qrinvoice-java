package ch.codeblock.qrinvoice.bulk.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum AddressType {
    NONE(""),
    STRUCTURED("S", "STRUCTURED", "STRUKTURIERT"),
    /**
     * @deprecated
     * The combined addresses will be discontinued with the specification version v2.3
     * {@link AddressType#STRUCTURED} should be used instead
     */
    @Deprecated
    COMBINED("K", "C", "COMBINED", "KOMBINIERT");

    private final Set<String> types;

    AddressType(String... types) {
        this.types = new HashSet<>(Arrays.asList(types));
    }

    public static AddressType valueOfLabel(String label) {
        if (label == null) {
            return null;
        }
        for (AddressType e : values()) {
            if (e.types.contains(label.toUpperCase())) {
                return e;
            }
        }
        return null;
    }
}
