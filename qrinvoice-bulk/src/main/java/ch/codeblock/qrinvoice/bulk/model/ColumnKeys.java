package ch.codeblock.qrinvoice.bulk.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class ColumnKeys {
    private ColumnKeys() {
    }

    public static final String OUTPUT_FILENAME = "OUTPUT_FILENAME";
    public static final String LANGUAGE = "LANGUAGE";
    public static final String FONT = "FONT";
    public static final String FORMAT = "FORMAT";
    public static final String RESOLUTION = "RESOLUTION";
    public static final String PAGE_SIZE = "PAGE_SIZE";
    public static final String BOUNDARY_LINES = "BOUNDARY_LINES";
    public static final String BOUNDARY_LINE_SCISSORS = "BOUNDARY_LINE_SCISSORS";
    public static final String SEPARATION_TEXT = "SEPARATION_TEXT";
    public static final String BOUNDARY_LINE_MARGINS = "BOUNDARY_LINE_MARGINS";
    public static final String IBAN = "IBAN";
    public static final String CR_ADDRESS_TYPE = "CR_ADDRESS_TYPE";
    public static final String CR_NAME = "CR_NAME";
    public static final String CR_STREET_OR_ADDRESS_LINE1 = "CR_STREET_OR_ADDRESS_LINE1";
    public static final String CR_BUILDING_NR_OR_ADDRESS_LINE2 = "CR_BUILDING_NR_OR_ADDRESS_LINE2";
    public static final String CR_POSTAL_CODE = "CR_POSTAL_CODE";
    public static final String CR_CITY = "CR_CITY";
    public static final String CR_COUNTRY = "CR_COUNTRY";
    public static final String AMOUNT = "AMOUNT";
    public static final String CURRENCY = "CURRENCY";
    public static final String UD_ADDRESS_TYPE = "UD_ADDRESS_TYPE";
    public static final String UD_NAME = "UD_NAME";
    public static final String UD_STREET_OR_ADDRESS_LINE1 = "UD_STREET_OR_ADDRESS_LINE1";
    public static final String UD_BUILDING_NR_OR_ADDRESS_LINE2 = "UD_BUILDING_NR_OR_ADDRESS_LINE2";
    public static final String UD_POSTAL_CODE = "UD_POSTAL_CODE";
    public static final String UD_CITY = "UD_CITY";
    public static final String UD_COUNTRY = "UD_COUNTRY";
    public static final String REFERENCE_TYPE = "REFERENCE_TYPE";
    public static final String REFERENCE = "REFERENCE";
    public static final String UNSTRUCTURED_MESSAGE = "UNSTRUCTURED_MESSAGE";
    public static final String BILLING_INFORMATION = "BILLING_INFORMATION";
    public static final String AP1 = "AP1";
    public static final String AP2 = "AP2";

    public static final Set<String> ALL_COLUMN_KEYS;

    static {
        final HashSet<String> set = new HashSet<>();
        set.add(LANGUAGE);
        set.add(FONT);
        set.add(FORMAT);
        set.add(RESOLUTION);
        set.add(PAGE_SIZE);
        set.add(BOUNDARY_LINES);
        set.add(BOUNDARY_LINE_SCISSORS);
        set.add(SEPARATION_TEXT);
        set.add(BOUNDARY_LINE_MARGINS);
        set.add(IBAN);
        set.add(CR_ADDRESS_TYPE);
        set.add(CR_NAME);
        set.add(CR_STREET_OR_ADDRESS_LINE1);
        set.add(CR_BUILDING_NR_OR_ADDRESS_LINE2);
        set.add(CR_POSTAL_CODE);
        set.add(CR_CITY);
        set.add(CR_COUNTRY);
        set.add(AMOUNT);
        set.add(CURRENCY);
        set.add(UD_ADDRESS_TYPE);
        set.add(UD_NAME);
        set.add(UD_STREET_OR_ADDRESS_LINE1);
        set.add(UD_BUILDING_NR_OR_ADDRESS_LINE2);
        set.add(UD_POSTAL_CODE);
        set.add(UD_CITY);
        set.add(UD_COUNTRY);
        set.add(REFERENCE_TYPE);
        set.add(REFERENCE);
        set.add(UNSTRUCTURED_MESSAGE);
        set.add(BILLING_INFORMATION);
        set.add(AP1);
        set.add(AP2);
        ALL_COLUMN_KEYS = Collections.unmodifiableSet(set);
    }
}
