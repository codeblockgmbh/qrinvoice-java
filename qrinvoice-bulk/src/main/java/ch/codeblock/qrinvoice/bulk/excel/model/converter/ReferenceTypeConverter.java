package ch.codeblock.qrinvoice.bulk.excel.model.converter;

import ch.codeblock.qrinvoice.model.ReferenceType;
import ch.codeblock.qrinvoice.util.StringUtils;

public class ReferenceTypeConverter implements ValueConverter<ReferenceType, Object> {
    @Override
    public ReferenceType convert(final Object value) {
        if (value instanceof ReferenceType) {
            return (ReferenceType) value;
        } else if (value instanceof String) {
            final String normalizedValue = StringUtils.trimToEmpty((String) value).toUpperCase();
            if (!normalizedValue.isEmpty()) {
                return ReferenceType.parse(normalizedValue);
            }
        }
        return null;
    }
}
