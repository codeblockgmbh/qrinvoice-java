package ch.codeblock.qrinvoice.bulk.model;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.model.ReferenceType;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Set;

public interface InvoiceEntry {

    Integer getRowId();

    String getOutputFilename();

    void setRowId(Integer rowId);

    void setFieldValue(String column, String value);

    void setFieldValue(String column, Boolean value);

    Set<String> getRequiredHeaders();

    Locale getLanguage();

    FontFamily getFontFamily();

    void setFontFamily(FontFamily fontFamily);

    OutputFormat getOutputFormat();

    void setOutputFormat(OutputFormat outputFormat);

    OutputResolution getResolution();

    PageSize getPageSize();

    void setPageSize(PageSize pageSize);

    Boolean getBoundaryLines();

    Boolean getBoundaryLineScissors();

    Boolean getBoundaryLineSeparationText();

    Boolean getBoundaryLineMargin();

    String getIban();

    AddressType getCreditorAddressType();

    String getCreditorName();

    String getCreditorStreetnameAddressLine1();

    String getCreditorStreetnumberAddressLine2();

    String getCreditorPostalCode();

    String getCreditorCity();

    String getCreditorCountry();

    BigDecimal getAmount();

    Currency getCurrency();

    AddressType getDebtorAddressType();

    String getDebtorName();

    String getDebtorStreetnameAddressLine1();

    String getDebtorStreetnumberAddressLine2();

    String getDebtorPostalCode();

    String getDebtorCity();

    String getDebtorCountry();

    ReferenceType getReferenceType();

    String getReferenceNumber();

    String getUnstructuredMessage();

    String getBillInformation();

    String getAltPmt1();

    String getAltPmt2();
}
