package ch.codeblock.qrinvoice.bulk.excel.model;

import ch.codeblock.qrinvoice.bulk.excel.model.converter.NoOpConverter;
import ch.codeblock.qrinvoice.bulk.excel.model.converter.ValueConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface XlsxColumn {
    String column();

    boolean required() default false;

    Class<? extends ValueConverter> converter() default NoOpConverter.class;
}
