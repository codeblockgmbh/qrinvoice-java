package ch.codeblock.qrinvoice.bulk.excel.model.converter;

public interface ValueConverter<T, U> {
    T convert(U value);
}
