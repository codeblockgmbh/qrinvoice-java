package ch.codeblock.qrinvoice.bulk.excel;

import ch.codeblock.qrinvoice.TechnicalException;
import ch.codeblock.qrinvoice.bulk.model.InvoiceEntry;
import ch.codeblock.qrinvoice.bulk.model.validation.InvalidHeaderException;
import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.util.IOUtils;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class Xlsx {
    private static final Logger LOGGER = LoggerFactory.getLogger(Xlsx.class);

    public static <T extends InvoiceEntry> List<T> readXlsx(InputStream inputStream, final Class<T> modelClass) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final Map<Integer, Map<Integer, Object>> data = readXlsxToMap(inputStream);
        final Map<Integer, String> headerRow = extractHeaderRow(data);
        validateHeader(headerRow, modelClass);
        return mapToInvoiceList(modelClass, headerRow, data);
    }

    private static Map<Integer, String> extractHeaderRow(final Map<Integer, Map<Integer, Object>> data) {
        final Map<Integer, String> headerRow = new HashMap<>();
        for (final Map.Entry<Integer, Map<Integer, Object>> rowEntry : data.entrySet()) {
            final Integer rowIndex = rowEntry.getKey();
            final Map<Integer, Object> row = rowEntry.getValue();
            if (rowIndex == 0) {
                for (final Map.Entry<Integer, Object> headerRowColumnEntry : row.entrySet()) {
                    // normalize columns header values
                    headerRow.put(headerRowColumnEntry.getKey(), StringUtils.trimToEmpty((String) headerRowColumnEntry.getValue()).toUpperCase());
                }
            }
        }
        return headerRow;
    }

    private static <T extends InvoiceEntry> void validateHeader(final Map<Integer, String> headerRow, final Class<T> modelClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        final Set<String> requiredHeaders = modelClass.getDeclaredConstructor().newInstance().getRequiredHeaders();
        final Collection<String> presentHeaders = headerRow.values();
        if (!CollectionUtils.containsAll(presentHeaders, requiredHeaders)) {
            final Collection<String> missingHeaders = CollectionUtils.subtract(requiredHeaders, presentHeaders);
            throw new InvalidHeaderException(missingHeaders);
        }
    }

    private static <T extends InvoiceEntry> List<T> mapToInvoiceList(final Class<T> modelClass, final Map<Integer, String> headerRow, final Map<Integer, Map<Integer, Object>> data) throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        final List<T> rows = new LinkedList<>();
        for (final Map.Entry<Integer, Map<Integer, Object>> rowEntry : data.entrySet()) {
            final Integer rowIndex = rowEntry.getKey();
            final Map<Integer, Object> row = rowEntry.getValue();
            if (rowIndex > 0) { // skip first row (header)
                final T e = modelClass.getDeclaredConstructor().newInstance();
                e.setRowId(rowIndex + 1);
                rows.add(e);

                for (final Map.Entry<Integer, Object> columnEntry : row.entrySet()) {
                    final Integer columnIndex = columnEntry.getKey();
                    final String columnName = headerRow.get(columnIndex);

                    if (columnEntry.getValue() instanceof Boolean) {
                        e.setFieldValue(columnName, (Boolean) columnEntry.getValue());
                    } else if (columnEntry.getValue() instanceof String) {
                        e.setFieldValue(columnName, (String) columnEntry.getValue());
                    } else if (columnEntry.getValue() != null) {
                        throw new TechnicalException("Unsupported type of value encountered during read of XLSX");
                    }
                }
            }
        }
        return rows;
    }


    private static Map<Integer, Map<Integer, Object>> readXlsxToMap(final InputStream inputStream) throws IOException {
        final Workbook workbook = new XSSFWorkbook(inputStream);

        final Sheet sheet = workbook.getSheetAt(0);

        final NumberFormat nf = DecimalFormat.getInstance();
        nf.setGroupingUsed(false);
        nf.setMinimumFractionDigits(0);
        final Map<Integer, Map<Integer, Object>> data = new HashMap<>();
        for (Row row : sheet) {
            data.put(row.getRowNum(), new LinkedHashMap<>());
            for (Cell cell : row) {
                Object cellValue = null;
                switch (cell.getCellType()) {
                    case STRING:
                        cellValue = cell.getRichStringCellValue().getString();
                        break;
                    case NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            cellValue = cell.getDateCellValue() + "";
                        } else {
                            final double numericCellValue = cell.getNumericCellValue();
                            cellValue = nf.format(numericCellValue);
                        }
                        break;
                    case BOOLEAN:
                        cellValue = cell.getBooleanCellValue();
                        break;
                    case FORMULA:
                        throw new TechnicalException(String.format("Formulas are not supported - encountered on rowNr=%s columnNr=%s", cell.getRowIndex() + 1, cell.getColumnIndex() + 1));
                    case ERROR:
                        throw new TechnicalException(String.format("Encountered Formula Error - Formulas are not supported - encountered on rowNr=%s columnNr=%s", cell.getRowIndex() + 1, cell.getColumnIndex() + 1));
                    case BLANK:
                        break;
                    default:
                        throw new TechnicalException(String.format("Encountered unsupported cell type '%s' on rowNr=%s columnNr=%s", cell.getCellType(), cell.getRowIndex() + 1, cell.getColumnIndex() + 1));
                }
                data.get(cell.getRowIndex()).put(cell.getColumnIndex(), cellValue);
            }
        }
        if (data.isEmpty()) {
            throw new ValidationException("Input file is empty");
        }
        return data;
    }

    public static byte[] getExample() throws IOException {
        final String externalFilePath = System.getProperty(SystemProperties.BULK_XLSX_EXAMPLE_FILE_PATH);
        if (StringUtils.isNotBlank(externalFilePath)) {
            LOGGER.info("Custom external bulk XLSX example file configured: {}", externalFilePath);
            return Files.readAllBytes(Paths.get(externalFilePath));
        } else {
            return IOUtils.toByteArray(Xlsx.class.getResourceAsStream("/ch/codeblock/qrinvoice/bulk/excel/example.xlsx"));
        }
    }
}
