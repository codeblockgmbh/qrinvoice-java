package ch.codeblock.qrinvoice.bulk.excel.model.converter;

import ch.codeblock.qrinvoice.paymentpartreceipt.LayoutDefinitions;
import ch.codeblock.qrinvoice.util.StringUtils;

import java.util.Locale;

public class LocaleConverter implements ValueConverter<Locale, Object> {
    @Override
    public Locale convert(final Object value) {
        if (value instanceof Locale) {
            return (Locale) value;
        } else if (value instanceof String) {
            final String normalizedValue = StringUtils.trimToEmpty((String) value).toLowerCase();
            if (StringUtils.isNotBlank(normalizedValue)) {
                if (normalizedValue.equals("deutsch") || normalizedValue.equals("german")) {
                    return Locale.GERMAN;
                } else if (normalizedValue.equals("franzoesisch") || normalizedValue.equals("französisch") || normalizedValue.equals("french")) {
                    return Locale.FRENCH;
                } else if (normalizedValue.equals("italienisch") || normalizedValue.equals("italian")) {
                    return Locale.ITALIAN;
                } else if (normalizedValue.equals("englisch") || normalizedValue.equals("english")) {
                    return Locale.ENGLISH;
                } else {
                    final Locale locale = new Locale(normalizedValue.toLowerCase());
                    for (final Locale supportedLocale : LayoutDefinitions.SUPPORTED_LOCALES) {
                        if (supportedLocale.getLanguage().equalsIgnoreCase(locale.getLanguage())) {
                            return locale;
                        }
                    }
                    throw new RuntimeException("Ungültige Sprache: " + value);
                }
            }
        }
        return null;
    }
}
