# Development Information

## Release

### Prepare
Make sure documentation is up to date (qrinvoice-documentation)

* Release Notes
* API Documentation Links etc
* Latest version

### Execute

In order to release a new version use the release.sh script

    ./release.sh -rv 1.20 -nv 1.21-SNAPSHOT

Here "rv" means "release version" and "nv" means "new version".

### Publish Documentation

After a successful release of the qrinvoice library artifacts, make sure to also publish the documentation

## settings.xml

the following must be added to the settings.xml in order to deploy artifacts

    <settings>
        <servers>
            <server>
                <id>qr-invoice-mvnrepo</id>
                <username>mvnrepo@qr-invoice.ch</username>
                <password>...</password>
            </server>
            <server>
                <id>qr-invoice-site</id>
                <username>docs@qr-invoice.ch</username>
                <password>...</password>
            </server>
            <server>
              <id>ossrh</id>
              <!-- access information via oss nexus portal -->
              <username>....</username>
              <password>....</password>
            </server>
        </servers>
        <profiles>
            <profile>
                <id>inject-ftp-info</id>
                <properties>
                    <ftp-host>ftp.qr-invoice.ch</ftp-host>
                    <ftp-username>docs@qr-invoice.ch</ftp-username>
                    <ftp-password>...</ftp-password>
                </properties>
            </profile>
        </profiles>
    
        <activeProfiles>
            <activeProfile>inject-ftp-info</activeProfile>
        </activeProfiles>
    </settings>

## Maven Central - PGP Signing

https://blog.sonatype.com/2010/01/how-to-generate-pgp-signatures-with-maven/

gpg --list-keys

gpg --gen-key

gpg --keyserver hkp://pgp.mit.edu --send-keys <key-id>
