curl 'https://cdn.jsdelivr.net/gh/stefangabos/world_countries/data/de/world.json' -o target/iso-3166-1-alpha2_de.json
curl 'https://cdn.jsdelivr.net/gh/stefangabos/world_countries/data/fr/world.json' -o target/iso-3166-1-alpha2_fr.json
curl 'https://cdn.jsdelivr.net/gh/stefangabos/world_countries/data/it/world.json' -o target/iso-3166-1-alpha2_it.json
curl 'https://cdn.jsdelivr.net/gh/stefangabos/world_countries/data/en/world.json' -o target/iso-3166-1-alpha2_en.json

targetdir=./src/main/resources/ch/codeblock/qrinvoice/standards

toProperties () {
    input=$1
    output=$2
    echo "# suppress inspection \"UnusedProperty\" for whole file" > $output
    echo "# ISO 3166-1 alpha-2 - data origin=https://cdn.jsdelivr.net/gh/stefangabos/world_countries - last updated $(date)" >> $output
    cat $input | jq -r '.[] | "\(.alpha2)" + "=" + "\(.name)"' | awk '{$1=toupper(substr($1,0,2))substr($1,3)}1' | uni2ascii -q -aL | sort >> $output
} 

toProperties "target/iso-3166-1-alpha2_de.json" "$targetdir/iso3166-1_de.properties"
toProperties "target/iso-3166-1-alpha2_fr.json" "$targetdir/iso3166-1_fr.properties"
toProperties "target/iso-3166-1-alpha2_it.json" "$targetdir/iso3166-1_it.properties"
toProperties "target/iso-3166-1-alpha2_en.json" "$targetdir/iso3166-1.properties"

touch "$targetdir/iso3166-1_en.properties"
