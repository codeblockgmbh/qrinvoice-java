
   ___   ___    ___                 _          
  / _ \ | _ \  |_ _| _ _ __ __ ___ (_) __  ___ 
 | (_) ||   /   | | | ' \\ V // _ \| |/ _|/ -_)
  \__\_\|_|_\  |___||_||_|\_/ \___/|_|\__|\___|
                                               
  https://www.qr-invoice.ch © by Codeblock GmbH

  Make sure you are using this solution with an appropriate
  commercial license, or you fully comply with the AGPLv3 terms
