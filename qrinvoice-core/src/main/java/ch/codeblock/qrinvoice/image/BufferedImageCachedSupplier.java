/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.image;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Class is not thread safe
 */
public class BufferedImageCachedSupplier implements Supplier<Optional<BufferedImage>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BufferedImageCachedSupplier.class);

    private final Supplier<BufferedImage> supplier;
    private BufferedImage cached;
    private boolean previouslyCached = false;

    public BufferedImageCachedSupplier(Supplier<BufferedImage> supplier) {
        this.supplier = supplier;
    }

    @Override
    public Optional<BufferedImage> get() {
        if (cached == null) {
            if (previouslyCached) {
                LOGGER.warn("Result was no longer in cache but was previously. Potential performance flaw!");
            }
            cached = supplier.get();
            previouslyCached = cached != null;
        }
        return Optional.ofNullable(cached);
    }

    public void destroyCache() {
        this.cached = null;
    }

}
