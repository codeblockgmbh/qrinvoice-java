/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.image;

import ch.codeblock.qrinvoice.qrcode.QrCodeReaderOptions;
import ch.codeblock.qrinvoice.qrcode.ScanningEffortLevel;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static ch.codeblock.qrinvoice.image.ImageUtils.blur;
import static ch.codeblock.qrinvoice.image.ImageUtils.scaleIfNeeded;

public class QrReaderBufferedImageContainer {
    private static final int MAX_SHORT_SIDE = (int) ((72.0 * 3.0) * 210.0 / 25.4);
    private static final int MAX_LONG_SIDE = (int) ((72.0 * 3.0) * 297.0 / 25.4);
    private static final int MARGIN = 50;

    private BufferedImageCachedSupplier imageSupplier;
    private BufferedImageCachedSupplier scaledImageSupplier;
    private BufferedImageCachedSupplier blurredImageSupplier;

    public QrReaderBufferedImageContainer(BufferedImage image, QrCodeReaderOptions options) {
        setImage(image);
        if (options.getScanningEffortLevel() == ScanningEffortLevel.HARDER) {
            setScaledImageSupplier(new BufferedImageCachedSupplier(() -> scaleIfNeeded(image, MAX_LONG_SIDE, MAX_SHORT_SIDE, MARGIN)));
            // blur scaled image if present, unscaled only if no scaled version is given
            setBlurredImageSupplier(new BufferedImageCachedSupplier(() -> {
                final BufferedImage srcToBlur = getScaledImageSupplier().get().orElse(image);
                getScaledImageSupplier().destroyCache(); // released scaled image if any is present -> releases memory 
                return blur(srcToBlur);
            }));
        }
    }

    public void setImage(BufferedImage image) {
        this.imageSupplier = new BufferedImageCachedSupplier(() -> image);
    }

    public BufferedImageCachedSupplier getImageSupplier() {
        return imageSupplier;
    }

    public void setImageSupplier(BufferedImageCachedSupplier imageSupplier) {
        this.imageSupplier = imageSupplier;
    }

    public BufferedImageCachedSupplier getScaledImageSupplier() {
        return scaledImageSupplier;
    }

    public void setScaledImageSupplier(BufferedImageCachedSupplier scaledImageSupplier) {
        this.scaledImageSupplier = scaledImageSupplier;
    }

    public BufferedImageCachedSupplier getBlurredImageSupplier() {
        return blurredImageSupplier;
    }

    public void setBlurredImageSupplier(BufferedImageCachedSupplier blurredImageSupplier) {
        this.blurredImageSupplier = blurredImageSupplier;
    }

    public List<BufferedImageCachedSupplier> getImageVariationSuppliers() {
        final ArrayList<BufferedImageCachedSupplier> suppliers = new ArrayList<>();
        if (imageSupplier != null) {
            suppliers.add(imageSupplier);
        }
        if (scaledImageSupplier != null) {
            suppliers.add(scaledImageSupplier);
        }
        if (blurredImageSupplier != null) {
            suppliers.add(blurredImageSupplier);
        }
        return suppliers;
    }

    public void destroyCaches() {
        getImageVariationSuppliers().forEach(BufferedImageCachedSupplier::destroyCache);
    }
}
