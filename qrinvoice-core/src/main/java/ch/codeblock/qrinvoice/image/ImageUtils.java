/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.image;

import ch.codeblock.qrinvoice.layout.Dimension;
import com.jhlabs.image.GaussianFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

public class ImageUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageUtils.class);
    private static final double MIN_RATIO = 0.95;

    public static BufferedImage blur(BufferedImage src) {
        double radius = Math.round(Math.min(3.0, Math.max(1.0, (((double) src.getWidth()) / 1000.0) * 1.5)));
        double radiusRounded = new BigDecimal(radius).setScale(1, RoundingMode.HALF_UP).doubleValue();
        final long start = System.currentTimeMillis();
        final BufferedImage blurredImage = new GaussianFilter(radiusRounded).filter(src, null);
        final long end = System.currentTimeMillis();
        LOGGER.debug("took {} ms to blur image w={} h={} r={}", (end - start), src.getWidth(), src.getHeight(), radiusRounded);
        return blurredImage;
    }

    public static BufferedImage scaleImage(BufferedImage src, int targetWidth, int targetHeight) {
        final long start = System.currentTimeMillis();
        Graphics2D g = null;
        try {
            final BufferedImage result = new BufferedImage(targetWidth, targetHeight, (src.getTransparency() == Transparency.OPAQUE ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB));
            g = result.createGraphics();
            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g.drawImage(src, 0, 0, targetWidth, targetHeight, null);
            g.dispose();
            final long end = System.currentTimeMillis();
            LOGGER.debug("took {} ms to scale image from w={} h={} to w={} h={}", (end - start), src.getWidth(), src.getHeight(), targetWidth, targetHeight);
            return result;
        } finally {
            if (g != null) {
                g.dispose();
            }
        }
    }

    public static BufferedImage scaleIfNeeded(BufferedImage image, int maxLongSide, int maxShortSide, int margin) {
        return calculateTargetDimension(maxLongSide, maxShortSide, margin, image.getWidth(), image.getHeight()).map(
                targetDimension -> scaleImage(image, targetDimension.getWidth(), targetDimension.getHeight())
        ).orElse(null);
    }

    static Optional<Dimension<Integer>> calculateTargetDimension(int maxLongSide, int maxShortSide, int margin, int width, int height) {
        final boolean portrait = height > width;
        int targetWidth = -1;
        int targetHeight = -1;
        if (portrait) {
            final double widthRatio = ((double) maxShortSide) / ((double) width);
            final double heightRatio = ((double) maxLongSide) / ((double) height);
            if (widthRatio > MIN_RATIO && heightRatio > MIN_RATIO) {
                LOGGER.trace("Do not resize as scale ratio is too big, widthRatio={} heightRatio={}", widthRatio, heightRatio);
                return Optional.empty();
            }

            if (widthRatio > heightRatio) {
                // Portrait - width limited
                targetHeight = (int) (widthRatio * (double) height);
                targetWidth = maxShortSide;
            } else {
                // Portrait - height limited
                targetWidth = (int) (heightRatio * (double) width);
                targetHeight = maxLongSide;
            }
        } else {
            final double widthRatio = ((double) maxLongSide) / ((double) width);
            final double heightRatio = ((double) maxShortSide) / ((double) height);
            if (widthRatio > MIN_RATIO && heightRatio > MIN_RATIO) {
                LOGGER.trace("Do not resize as scale ratio is too big, widthRatio={} heightRatio={}", widthRatio, heightRatio);
                return Optional.empty();
            }

            if (widthRatio > heightRatio) {
                // Landscape - width limited
                targetHeight = (int) (widthRatio * (double) height);
                targetWidth = maxLongSide;
            } else {
                // Landscape - height limited
                targetWidth = (int) (heightRatio * (double) width);
                targetHeight = maxShortSide;
            }
        }
        if (targetWidth != -1 || targetHeight != -1) {
            return Optional.of(new Dimension<>(targetWidth, targetHeight));
        } else {
            return Optional.empty();
        }
    }
}
