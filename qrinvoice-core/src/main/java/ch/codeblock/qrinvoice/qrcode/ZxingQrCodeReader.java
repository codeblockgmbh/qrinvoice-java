/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.qrcode;

import ch.codeblock.qrinvoice.BaseException;
import ch.codeblock.qrinvoice.TechnicalException;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.qrcode.QRCodeMultiReader;
import com.google.zxing.qrcode.QRCodeReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Optional.empty;

public class ZxingQrCodeReader implements IQrCodeReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZxingQrCodeReader.class);
    private static final Map<DecodeHintType, Object> hintMap;

    static {
        Map<DecodeHintType, Object> map = new EnumMap<>(DecodeHintType.class);
        map.put(DecodeHintType.POSSIBLE_FORMATS, Collections.singleton(BarcodeFormat.QR_CODE));
        map.put(DecodeHintType.CHARACTER_SET, SwissPaymentsCode.ENCODING.name());
        map.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hintMap = Collections.unmodifiableMap(map);
    }

    public Optional<String> readSingleSwissPaymentsCode(final BufferedImage image) {
        final long start = System.currentTimeMillis();
        Optional<String> result = empty();
        try {
            result = Optional.ofNullable(new QRCodeReader()
                    .decode(getBinaryBitmap(image), hintMap)
                    .getText());

            if (result.isPresent() && !SwissQrCodeFilter.filter(result.get())) {
                LOGGER.info("found 1 qr codes which is not an SPC but we were looking for single result, now we re-read all qr codes");
                result = readAllSwissPaymentCodes(image).stream().findFirst();
            }

            return result;
        } catch (BaseException e) {
            throw e;
        } catch (NotFoundException e) {
            return empty();
        } catch (Exception e) {
            throw new TechnicalException(UNEXPECTED_EXCEPTION_MSG, e);
        } finally {
            final long end = System.currentTimeMillis();
            LOGGER.debug("found {} qr codes which took {} ms to readAll from image w={} h={}", result.map((o) -> 1).orElse(0), (end - start), image.getWidth(), image.getHeight());
        }
    }


    public List<String> readAllSwissPaymentCodes(final BufferedImage image) {
        final long start = System.currentTimeMillis();
        int resultSize = 0;
        try {
            final Result[] qrCodeResults = new QRCodeMultiReader().decodeMultiple(getBinaryBitmap(image), hintMap);
            final List<String> detections = Arrays.stream(qrCodeResults)
                    .map(Result::getText)
                    .filter(SwissQrCodeFilter::filter)
                    .distinct()
                    .collect(Collectors.toList());
            resultSize = detections.size();
            return detections;
        } catch (BaseException e) {
            throw e;
        } catch (NotFoundException e) {
            return Collections.emptyList();
        } catch (Exception e) {
            throw new TechnicalException(UNEXPECTED_EXCEPTION_MSG, e);
        } finally {
            final long end = System.currentTimeMillis();
            LOGGER.debug("found {} qr codes which took {} ms to readAll from image w={} h={}", resultSize, (end - start), image.getWidth(), image.getHeight());
        }
    }


    private BinaryBitmap getBinaryBitmap(final BufferedImage image) {
        return new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));
    }

}
