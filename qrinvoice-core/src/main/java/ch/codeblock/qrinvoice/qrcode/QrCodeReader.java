/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.qrcode;

import ch.codeblock.qrinvoice.image.BufferedImageCachedSupplier;
import ch.codeblock.qrinvoice.image.ImageSupport;
import ch.codeblock.qrinvoice.image.QrReaderBufferedImageContainer;
import ch.codeblock.qrinvoice.infrastructure.IQrCodeReaderFactory;
import ch.codeblock.qrinvoice.infrastructure.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class QrCodeReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(QrCodeReader.class);

    public static final String QR_CODE_NOT_FOUND_MSG = "QR Code could not be found in the given image";
    private final QrCodeReaderOptions options;

    private final List<IQrCodeReader> qrCodeReaderDelegates;

    public static QrCodeReader create() {
        return create(QrCodeReaderOptions.DEFAULT);
    }

    public static QrCodeReader create(QrCodeReaderOptions options) {
        return new QrCodeReader(options);
    }

    public QrCodeReader() {
        this(QrCodeReaderOptions.DEFAULT);
    }

    public QrCodeReader(QrCodeReaderOptions options) {
        this.options = options;
        this.qrCodeReaderDelegates = ServiceProvider.getInstance().getAll(IQrCodeReaderFactory.class, Arrays.stream(options.getQrCodeLibraries()).distinct().collect(Collectors.toList())).stream().map(IQrCodeReaderFactory::create).collect(Collectors.toList());
    }

    public String readQRCode(final BufferedImage image) {
        return readQRCode(new QrReaderBufferedImageContainer(image, options));
    }

    private String readQRCode(QrReaderBufferedImageContainer imageContainer) {
        for (BufferedImageCachedSupplier imageSupplier : imageContainer.getImageVariationSuppliers()) {
            for (IQrCodeReader s : qrCodeReaderDelegates) {
                Optional<String> opt = imageSupplier.get().flatMap(s::readSingleSwissPaymentsCode);
                if (opt.isPresent()) {
                    return opt.get();
                }
            }
            // release references
            imageSupplier.destroyCache();
        }

        throw new DecodeException(QR_CODE_NOT_FOUND_MSG);
    }

    public String readQRCode(final byte[] qrImageInput) {
        return readQRCode(new ByteArrayInputStream(qrImageInput));
    }

    public String readQRCode(final InputStream qrImageInput) {
        return readQRCode(new ImageSupport().read(qrImageInput));
    }

    public List<String> readQRCodes(final BufferedImage image) {
        return readQRCodes(new QrReaderBufferedImageContainer(image, options));
    }

    public List<String> readQRCodes(QrReaderBufferedImageContainer imageContainer) {
        final Set<String> strs = new HashSet<>();
        outer:
        for (BufferedImageCachedSupplier imageSupplier : imageContainer.getImageVariationSuppliers()) {
            for (IQrCodeReader reader : qrCodeReaderDelegates) {
                try {
                    imageSupplier.get()
                            .map(reader::readAllSwissPaymentCodes)
                            .ifPresent(strs::addAll);
                } catch (Exception e) {
                    LOGGER.warn("Experienced error during scan ", e);
                }
                if (!strs.isEmpty()) {
                    break outer;
                }
            }
        }

        // release references as soon as possible
        imageContainer.destroyCaches();

        if (strs.isEmpty()) {
            throw new DecodeException(QR_CODE_NOT_FOUND_MSG);
        } else {
            return new ArrayList<>(strs);
        }
    }

    public List<String> readQRCodes(final byte[] qrImageInput) {
        return readQRCodes(new ByteArrayInputStream(qrImageInput));
    }

    public List<String> readQRCodes(final InputStream qrImageInput) {
        return readQRCodes(new ImageSupport().read(qrImageInput));
    }


}
