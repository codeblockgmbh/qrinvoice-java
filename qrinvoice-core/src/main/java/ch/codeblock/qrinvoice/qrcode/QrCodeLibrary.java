/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.qrcode;

import java.util.*;
import java.util.stream.Collectors;

// TODO name
public enum QrCodeLibrary {
    ZXING,
    BOOFCV,
    DEFAULT(ZXING),
    ALL(ZXING, BOOFCV);

    private final List<QrCodeLibrary> libraries;

    QrCodeLibrary() {
        this.libraries = Collections.singletonList(this);
    }

    QrCodeLibrary(QrCodeLibrary... libraries) {
        this.libraries = Arrays.stream(libraries).distinct().collect(Collectors.toList());
    }

    public List<QrCodeLibrary> getLibraries() {
        return libraries;
    }

    public boolean supports(QrCodeLibrary library) {
        if (library == null) {
            return false;
        }
        return containsAll(libraries, library.getLibraries());
    }

    public boolean supportedBy(QrCodeLibrary library) {
        if (library == null) {
            return false;
        }
        return containsAll(library.libraries, this.libraries);
    }

    public boolean supportedBy(Collection<QrCodeLibrary> libraries) {
        if (libraries == null) {
            return false;
        }
        return libraries.stream().anyMatch(l -> containsAll(l.libraries, this.libraries));
    }

    private boolean containsAll(List<QrCodeLibrary> libraries, List<QrCodeLibrary> library) {
        return new HashSet<>(libraries).containsAll(library);
    }
}
