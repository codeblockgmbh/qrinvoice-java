/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.document;

import ch.codeblock.qrinvoice.QrInvoiceCodeScanner;
import ch.codeblock.qrinvoice.infrastructure.IDocumentScanner;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.qrcode.DecodeException;
import ch.codeblock.qrinvoice.qrcode.QrCodeReaderOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;

public class JavaGraphicsDocumentScanner implements IDocumentScanner {
    private final Logger logger = LoggerFactory.getLogger(JavaGraphicsDocumentScanner.class);
    private final QrCodeReaderOptions options;

    public JavaGraphicsDocumentScanner(QrCodeReaderOptions options) {
        this.options = options;
    }

    private QrInvoiceCodeScanner createQrInvoiceCodeScanner() {
        return QrInvoiceCodeScanner.create(options);
    }

    @Override
    public List<QrInvoice> scanDocumentForAllSwissQrCodes(final byte[] document) {
        return createQrInvoiceCodeScanner().scanMultiple(document);
    }

    @Override
    public List<QrInvoice> scanDocumentForAllSwissQrCodes(final InputStream inputStream) {
        return createQrInvoiceCodeScanner().scanMultiple(inputStream);
    }

    @Override
    public List<QrInvoice> scanDocumentForAllSwissQrCodes(final byte[] document, final int pageNr) {
        throw unsupportedException();
    }

    @Override
    public List<QrInvoice> scanDocumentForAllSwissQrCodes(final InputStream inputStream, final int pageNr) {
        throw unsupportedException();
    }

    @Override
    public Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final byte[] document) {
        try {
            return Optional.ofNullable(createQrInvoiceCodeScanner().scan(document));
        } catch (DecodeException e) {
            return Optional.empty();
        } catch (Exception e) {
            logger.warn("Unexpected error during scanDocumentUntilFirstSwissQrCode", e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final InputStream inputStream) {
        try {
            return Optional.ofNullable(createQrInvoiceCodeScanner().scan(inputStream));
        } catch (DecodeException e) {
            return Optional.empty();
        } catch (Exception e) {
            logger.warn("Unexpected error during scanDocumentUntilFirstSwissQrCode", e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final byte[] document, final int pageNr) {
        throw unsupportedException();
    }

    @Override
    public Optional<QrInvoice> scanDocumentUntilFirstSwissQrCode(final InputStream inputStream, final int pageNr) {
        throw unsupportedException();
    }

    private UnsupportedOperationException unsupportedException() {
        return new UnsupportedOperationException("No multi page rasterized graphic formats supported.");
    }
}
