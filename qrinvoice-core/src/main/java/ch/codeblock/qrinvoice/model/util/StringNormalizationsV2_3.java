/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.util;

public abstract class StringNormalizationsV2_3{
    private StringNormalizationsV2_3() {
    }

    public static char[] singleCharSearch = {};
    public static char[] singleCharReplacement = {};
    public static char[] multipleCharSearch = {};
    public static char[][] multipleCharReplacement = {};

    static {
        basicLatin();
        latin1ExtendedB();
        spacingModifierLetters();
        combiningDiacriticalMarks();
        greekAndCoptic();
        cyrillic();
        latinExtendedAdditional();
        greekExtended();
        generalPunctuation();
        superscriptsAndSubscripts();
    }

    private static void addSearchReplacement(char searchChr, char replaceChr) {
        char[] newSearch = new char[singleCharSearch.length + 1];
        System.arraycopy(singleCharSearch, 0, newSearch, 0, singleCharSearch.length);
        newSearch[newSearch.length -1] = searchChr;
        singleCharSearch = newSearch;

        char[] newReplacement = new char[singleCharReplacement.length + 1];
        System.arraycopy(singleCharReplacement, 0, newReplacement, 0, singleCharReplacement.length);
        newReplacement[newReplacement.length - 1] = replaceChr;
        singleCharReplacement = newReplacement;
    }

    private static void addSearchReplacement(char searchChr, char[] replaceChrArr) {
        char[] newSearch = new char[multipleCharSearch.length + 1];
        System.arraycopy(multipleCharSearch, 0, newSearch, 0, multipleCharSearch.length);
        newSearch[newSearch.length -1] = searchChr;
        multipleCharSearch = newSearch;

        char[][] newReplacement = new char[multipleCharReplacement.length + 1][];
        System.arraycopy(multipleCharReplacement, 0, newReplacement, 0, multipleCharReplacement.length);
        newReplacement[newReplacement.length - 1] = replaceChrArr;
        multipleCharReplacement = newReplacement;
    }

    /**
     * 0000—007F
     * 0000—007F
     */
    private static void basicLatin() {

    }

    /**
     * 0180—024F
     *
     * Exceptions: 0218,0219,021A,021B,20AC
     */
    private static void latin1ExtendedB() {
        addSearchReplacement('\u01CD', 'A');
        addSearchReplacement('\u01CE', 'a');

        addSearchReplacement('\u01CF', 'I');
        addSearchReplacement('\u01D0', 'i');

        addSearchReplacement('\u01D1', 'O');
        addSearchReplacement('\u01D2', 'o');

        addSearchReplacement('\u01D3', 'U');
        addSearchReplacement('\u01D4', 'u');
        addSearchReplacement('\u01D5', 'U');
        addSearchReplacement('\u01D6', 'u');
        addSearchReplacement('\u01D7', 'U');
        addSearchReplacement('\u01D8', 'u');
        addSearchReplacement('\u01D9', 'U');
        addSearchReplacement('\u01DA', 'u');
        addSearchReplacement('\u01DB', 'U');
        addSearchReplacement('\u01DC', 'u');

        addSearchReplacement('\u01E6', 'G');
        addSearchReplacement('\u01E7', 'g');

        addSearchReplacement('\u01E8', 'K');
        addSearchReplacement('\u01E9', 'k');

        addSearchReplacement('\u01F4', 'G');
        addSearchReplacement('\u01F5', 'g');

        addSearchReplacement('\u01F8', 'N');
        addSearchReplacement('\u01F9', 'n');


        addSearchReplacement('\u0200', 'A');
        addSearchReplacement('\u0201', 'a');
        addSearchReplacement('\u0202', 'A');
        addSearchReplacement('\u0203', 'a');

        addSearchReplacement('\u0204', 'E');
        addSearchReplacement('\u0205', 'e');
        addSearchReplacement('\u0206', 'E');
        addSearchReplacement('\u0207', 'e');

        addSearchReplacement('\u0208', 'I');
        addSearchReplacement('\u0209', 'i');
        addSearchReplacement('\u020A', 'I');
        addSearchReplacement('\u020B', 'i');

        addSearchReplacement('\u020C', 'O');
        addSearchReplacement('\u020D', 'o');
        addSearchReplacement('\u020E', 'O');
        addSearchReplacement('\u020F', 'o');

        addSearchReplacement('\u0210', 'R');
        addSearchReplacement('\u0211', 'r');
        addSearchReplacement('\u0212', 'R');
        addSearchReplacement('\u0213', 'r');

        addSearchReplacement('\u0214', 'U');
        addSearchReplacement('\u0215', 'u');
        addSearchReplacement('\u0216', 'U');
        addSearchReplacement('\u0217', 'u');

        addSearchReplacement('\u021E', 'H');
        addSearchReplacement('\u021F', 'h');

        addSearchReplacement('\u0226', 'A');
        addSearchReplacement('\u0227', 'a');

        addSearchReplacement('\u0228', 'E');
        addSearchReplacement('\u0229', 'e');

        addSearchReplacement('\u022E', 'O');
        addSearchReplacement('\u022F', 'o');
    }

    /**
     * 02B0—02FF
     */
    private static void spacingModifierLetters() {
        // replace all single-quotes and "single-quotes-looking" chars with simple single quote
        // replace all double-quotes and "double-quotes-looking" chars with simple double quote
        addSearchReplacement('\u02B9', '\'');
        addSearchReplacement('\u02BA', '"');
        addSearchReplacement('\u02BB', '\'');
        addSearchReplacement('\u02BC', '\'');
        addSearchReplacement('\u02BD', '\'');
        addSearchReplacement('\u02BE', '\'');
        addSearchReplacement('\u02CA', '\'');
        addSearchReplacement('\u02CB', '\'');
        addSearchReplacement('\u02F4', '\'');
        addSearchReplacement('\u02F5', '"');
        addSearchReplacement('\u02F6', '"');
    }

    /**
     * 0300—036F
     */
    private static void combiningDiacriticalMarks() {
        addSearchReplacement('\u0300', '\'');
        addSearchReplacement('\u0301', '\'');
        addSearchReplacement('\u030B', '"');
        addSearchReplacement('\u030D', '\'');
        addSearchReplacement('\u030E', '"');
        addSearchReplacement('\u030F', '"');
        addSearchReplacement('\u0312', '\'');
        addSearchReplacement('\u0313', '\'');
        addSearchReplacement('\u0314', '\'');
        addSearchReplacement('\u0315', '\'');
    }

    /**
     * 0370—03FF
     */
    private static void greekAndCoptic() {

    }

    /**
     * 0400—04FF
     */
    private static void cyrillic() {
        addSearchReplacement('\u0400', 'E');
        addSearchReplacement('\u0401', 'E');
        addSearchReplacement('\u0405', 'S');
        addSearchReplacement('\u0406', 'I');
        addSearchReplacement('\u0407', 'I');
        addSearchReplacement('\u0408', 'J');
        addSearchReplacement('\u0450', 'e');
        addSearchReplacement('\u0451', 'e');
        addSearchReplacement('\u0455', 's');
        addSearchReplacement('\u0456', 'i');
        addSearchReplacement('\u0457', 'i');
        addSearchReplacement('\u0458', 'j');
    }

    /**
     * 0500—052F
     */
    private static void cyrillicSupplement() {

    }

    /**
     * 1E00—1EFF
     */
    private static void latinExtendedAdditional() {

    }

    /**
     * 1F00—1FFF
     */
    private static void greekExtended() {

    }

    /**
     * 2000—206F
     */
    private static void generalPunctuation() {
        // replace all dash variants and the middle dot '·'
        addSearchReplacement('\u2010', '-');
        addSearchReplacement('\u2011', '-');
        addSearchReplacement('\u2012', '-');
        addSearchReplacement('\u2013', '-');
        addSearchReplacement('\u2014', '-');
        addSearchReplacement('\u2015', '-');

        // replace all single-quotes and "single-quotes-looking" chars with simple single quote
        addSearchReplacement('\u2018', '\'');// ‘
        addSearchReplacement('\u2019', '\'');// ’

        // replace all double-quotes and "double-quotes-looking" chars with simple double quote
        addSearchReplacement('\u201C', '"');// “
        addSearchReplacement('\u201D', '"');// ”
        addSearchReplacement( '\u201E', '"');// „
        addSearchReplacement('\u201F', '"');// ‟
    }

    /**
     * 2070—209F
     */
    private static void superscriptsAndSubscripts() {

    }

}
