/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.parser;

import ch.codeblock.qrinvoice.model.ParseException;
import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameterTypes;
import ch.codeblock.qrinvoice.util.CollectionUtils;
import ch.codeblock.qrinvoice.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class AlternativeSchemeParameterParser {
    public static AlternativeSchemeParameterParser create() {
        return new AlternativeSchemeParameterParser();
    }

    public AlternativeSchemeParameter parseAlternativeSchemeParameter(String alternativeSchemeParameter) {
        if (StringUtils.isEmpty(alternativeSchemeParameter)) {
            return null;
        }

        final AlternativeSchemeParameterTypes[] values = AlternativeSchemeParameterTypes.values();
        for (final AlternativeSchemeParameterTypes value : values) {
            if (value.getAlternativeSchemeParameterType().supports(alternativeSchemeParameter)) {
                return value.getAlternativeSchemeParameterType().parse(alternativeSchemeParameter);
            }
        }

        throw new ParseException("No matching AlternativeSchemeParameter parser found");
    }

    public List<AlternativeSchemeParameter> parseAlternativeSchemeParameters(List<String> alternativeSchemeParameterStrings) {
        final List<AlternativeSchemeParameter> alternativeSchemeParameters = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(alternativeSchemeParameterStrings)) {
            for (final String alternativeSchemeParameter : alternativeSchemeParameterStrings) {
                if (StringUtils.isNotBlank(alternativeSchemeParameter)) {
                    alternativeSchemeParameters.add(parseAlternativeSchemeParameter(alternativeSchemeParameter));
                }
            }
        }
        return alternativeSchemeParameters;
    }
}
