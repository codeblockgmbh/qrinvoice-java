/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.parser.AlternativeSchemeParameterParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

import static ch.codeblock.qrinvoice.model.validation.ValidationUtils.validateOptionalLength;
import static ch.codeblock.qrinvoice.model.validation.ValidationUtils.validateString;

public class AlternativeSchemeParameterValidator {
    private final Logger logger = LoggerFactory.getLogger(AlternativeSchemeParameterValidator.class);

    private final ImplementationGuidelinesQrBillVersion qrBillVersion;

    private AlternativeSchemeParameterValidator(ImplementationGuidelinesQrBillVersion qrBillVersion) {
        this.qrBillVersion = qrBillVersion;
    }

    /**
     * @return new instance
     * @deprecated This method is to be replaced with {@link #create(ImplementationGuidelinesQrBillVersion)}
     * this method will always use the version {@link ImplementationGuidelinesQrBillVersion#V2_2}
     */
    @Deprecated
    public static AlternativeSchemeParameterValidator create() {
        return new AlternativeSchemeParameterValidator(ImplementationGuidelinesQrBillVersion.V2_2);
    }

    /**
     * Creates a new instance that uses the specific {@link ImplementationGuidelinesQrBillVersion}
     *
     * @param qrBillVersion The desired {@link ImplementationGuidelinesQrBillVersion}
     * @return new instance
     */
    public static AlternativeSchemeParameterValidator create(ImplementationGuidelinesQrBillVersion qrBillVersion) {
        return new AlternativeSchemeParameterValidator(qrBillVersion);
    }

    public ValidationResult validate(final QrInvoice qrInvoice, final String alternativeSchemeParameter) {
        final ValidationResult result = new ValidationResult();

        validateRawString(alternativeSchemeParameter, result);

        if (alternativeSchemeParameterValidationEnabled()) {
            try {
                final Optional<AlternativeSchemeParameter> alternativeSchemeParameterObjectOptional = parseAlternativeSchemeParameters(alternativeSchemeParameter);
                alternativeSchemeParameterObjectOptional.ifPresent(alternativeSchemeParameterObject -> alternativeSchemeParameterObject.validate(result, qrInvoice));
            } catch (Exception e) {
                logger.debug("Unable to parse AltPmt. Reason={}", e.getMessage());
            }
        } else {
            logDisableAltPmtFlag();
        }
        return result;
    }

    public void validateRawString(final String alternativeSchemeParameter, final ValidationResult result) {
        validateOptionalLength(alternativeSchemeParameter, 0, 140, (value) -> result.addError("paymentReference.additionalInformation", "alternativeSchemeParameter", value, "{{validation.error.paymentReference.additionalInformation.alternativeSchemeParameter}}"));
        validateString(alternativeSchemeParameter, qrBillVersion, (value, msgs) -> result.addError("paymentReference.additionalInformation", "alternativeSchemeParameter", value, msgs));
    }

    public ValidationResult validate(final String alternativeSchemeParameter) {
        return validate(null, alternativeSchemeParameter);
    }

    public ValidationResult validate(final QrInvoice qrInvoice, final AlternativeSchemeParameter alternativeSchemeParameter) {
        return alternativeSchemeParameter.validate(qrInvoice);
    }

    public ValidationResult validate(final AlternativeSchemeParameter alternativeSchemeParameter) {
        if (alternativeSchemeParameter != null && alternativeSchemeParameterValidationEnabled()) {
            return alternativeSchemeParameter.validate();
        } else {
            logDisableAltPmtFlag();
            return new ValidationResult();
        }
    }

    private boolean alternativeSchemeParameterValidationEnabled() {
        return System.getProperty(SystemProperties.DISABLE_ALT_PMT_VALIDATION) == null;
    }

    private Optional<AlternativeSchemeParameter> parseAlternativeSchemeParameters(final String alternativeSchemeParameter) {
        return Optional.ofNullable(AlternativeSchemeParameterParser.create().parseAlternativeSchemeParameter(alternativeSchemeParameter));
    }

    private void logDisableAltPmtFlag() {
        if (logger.isTraceEnabled()) {
            logger.trace("DISABLE_ALT_PMT_VALIDATION={}", System.getProperty(SystemProperties.DISABLE_ALT_PMT_VALIDATION));
        }
    }


}
