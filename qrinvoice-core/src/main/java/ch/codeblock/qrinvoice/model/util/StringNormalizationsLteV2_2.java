/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.util;

public abstract class StringNormalizationsLteV2_2 {
    private StringNormalizationsLteV2_2() {
    }

    public static char[] singleCharSearch = {};
    public static char[] singleCharReplacement = {};
    public static char[] multipleCharSearch = {};
    public static char[][] multipleCharReplacement = {};

    static {
        basicLatin();
        latin1Supplement();
        latin1ExtendedA();
        latin1ExtendedB();
        spacingModifierLetters();
        combiningDiacriticalMarks();
        greekAndCoptic();
        cyrillic();
        latinExtendedAdditional();
        greekExtended();
        generalPunctuation();
        superscriptsAndSubscripts();
    }

    private static void addSearchReplacement(char searchChr, char replaceChr) {
        char[] newSearch = new char[singleCharSearch.length + 1];
        System.arraycopy(singleCharSearch, 0, newSearch, 0, singleCharSearch.length);
        newSearch[newSearch.length -1] = searchChr;
        singleCharSearch = newSearch;

        char[] newReplacement = new char[singleCharReplacement.length + 1];
        System.arraycopy(singleCharReplacement, 0, newReplacement, 0, singleCharReplacement.length);
        newReplacement[newReplacement.length - 1] = replaceChr;
        singleCharReplacement = newReplacement;
    }

    private static void addSearchReplacement(char searchChr, char[] replaceChrArr) {
        char[] newSearch = new char[multipleCharSearch.length + 1];
        System.arraycopy(multipleCharSearch, 0, newSearch, 0, multipleCharSearch.length);
        newSearch[newSearch.length -1] = searchChr;
        multipleCharSearch = newSearch;

        char[][] newReplacement = new char[multipleCharReplacement.length + 1][];
        System.arraycopy(multipleCharReplacement, 0, newReplacement, 0, multipleCharReplacement.length);
        newReplacement[newReplacement.length - 1] = replaceChrArr;
        multipleCharReplacement = newReplacement;
    }

    /**
     * 0000—007F
     */
    private static void basicLatin() {

    }

    /**
     * 0080—00FF
     */
    private static void latin1Supplement() {
        addSearchReplacement('\u00A0', ' ');//  
        addSearchReplacement('\u00A1', '!');// ¡
        addSearchReplacement('\u00A2', 'c');// ¢
        addSearchReplacement('\u00AB', '"');// «
        addSearchReplacement('\u00AD', '-');// ­
        addSearchReplacement('\u00B0', '.');// °

        addSearchReplacement('\u00B2', '2');// ²
        addSearchReplacement('\u00B3', '3');// ³
        addSearchReplacement('\u00B7', '-');// ·
        addSearchReplacement('\u00B9', '1');// ¹
        addSearchReplacement('\u00BB', '"');// »

        addSearchReplacement('\u00C3', 'A');// Ã
        addSearchReplacement('\u00C5', 'A');// Å
        addSearchReplacement('\u00D0', 'D');// Ð
        addSearchReplacement('\u00D5', 'O');// Õ
        addSearchReplacement('\u00D7', 'x');// ×
        addSearchReplacement('\u00D8', 'Ö');// Ø
        addSearchReplacement('\u00DD', 'Y');// Ý

        addSearchReplacement('\u00E3', 'a');// ã
        addSearchReplacement('\u00E5', 'a');// å
        addSearchReplacement('\u00F0', 'd');// ð
        addSearchReplacement('\u00F5', 'o');// õ
        addSearchReplacement('\u00F8', 'ö');// ø
        addSearchReplacement('\u00FF', 'y');// ÿ

        addSearchReplacement('\u00C6', new char[]{'A', 'e'});// Æ
        addSearchReplacement('\u00E6', new char[]{'a', 'e'});// æ
    }

    /**
     * 0100—017F
     */
    private static void latin1ExtendedA() {
        addSearchReplacement('\u0100', 'A');
        addSearchReplacement('\u0101', 'a');
        addSearchReplacement('\u0102', 'A');
        addSearchReplacement('\u0103', 'a');
        addSearchReplacement('\u0104', 'A');
        addSearchReplacement('\u0105', 'a');

        addSearchReplacement('\u0106', 'C');
        addSearchReplacement('\u0107', 'c');
        addSearchReplacement('\u0108', 'C');
        addSearchReplacement('\u0109', 'c');
        addSearchReplacement('\u010A', 'C');
        addSearchReplacement('\u010B', 'c');
        addSearchReplacement('\u010C', 'C');
        addSearchReplacement('\u010D', 'c');

        addSearchReplacement('\u010E', 'D');
        addSearchReplacement('\u010F', 'd');
        addSearchReplacement('\u0110', 'D');
        addSearchReplacement('\u0111', 'd');

        addSearchReplacement('\u0112', 'E');
        addSearchReplacement('\u0113', 'e');
        addSearchReplacement('\u0114', 'E');
        addSearchReplacement('\u0115', 'e');
        addSearchReplacement('\u0116', 'E');
        addSearchReplacement('\u0117', 'e');
        addSearchReplacement('\u0118', 'E');
        addSearchReplacement('\u0119', 'e');
        addSearchReplacement('\u011A', 'E');
        addSearchReplacement('\u011B', 'e');

        addSearchReplacement('\u011C', 'G');
        addSearchReplacement('\u011D', 'g');
        addSearchReplacement('\u011E', 'G');
        addSearchReplacement('\u011F', 'g');
        addSearchReplacement('\u0120', 'G');
        addSearchReplacement('\u0121', 'g');
        addSearchReplacement('\u0122', 'G');
        addSearchReplacement('\u0123', 'g');

        addSearchReplacement('\u0124', 'H');
        addSearchReplacement('\u0125', 'h');
        addSearchReplacement('\u0126', 'H');
        addSearchReplacement('\u0127', 'h');

        addSearchReplacement('\u0128', 'I');
        addSearchReplacement('\u0129', 'i');
        addSearchReplacement('\u012A', 'I');
        addSearchReplacement('\u012B', 'i');
        addSearchReplacement('\u012C', 'I');
        addSearchReplacement('\u012D', 'i');
        addSearchReplacement('\u012E', 'I');
        addSearchReplacement('\u012F', 'i');
        addSearchReplacement('\u0130', 'I');
        addSearchReplacement('\u0131', 'i');

        addSearchReplacement('\u0134', 'J');
        addSearchReplacement('\u0135', 'j');
        addSearchReplacement('\u0136', 'K');
        addSearchReplacement('\u0137', 'k');
        addSearchReplacement('\u0138', 'k');

        addSearchReplacement('\u0139', 'L');
        addSearchReplacement('\u013A', 'l');
        addSearchReplacement('\u013B', 'L');
        addSearchReplacement('\u013C', 'l');
        addSearchReplacement('\u013D', 'L');
        addSearchReplacement('\u013E', 'l');
        addSearchReplacement('\u013F', 'L');
        addSearchReplacement('\u0140', 'l');
        addSearchReplacement('\u0141', 'L');
        addSearchReplacement('\u0142', 'l');

        addSearchReplacement('\u0143', 'N');
        addSearchReplacement('\u0144', 'n');
        addSearchReplacement('\u0145', 'N');
        addSearchReplacement('\u0146', 'n');
        addSearchReplacement('\u0147', 'N');
        addSearchReplacement('\u0148', 'n');
        addSearchReplacement('\u014A', 'N');
        addSearchReplacement('\u014B', 'n');

        addSearchReplacement('\u014C', 'O');
        addSearchReplacement('\u014D', 'o');
        addSearchReplacement('\u014E', 'O');
        addSearchReplacement('\u014F', 'o');
        addSearchReplacement('\u0150', 'O');
        addSearchReplacement('\u0151', 'o');

        addSearchReplacement('\u0152', new char[]{'O', 'e'});// Œ
        addSearchReplacement('\u0153', new char[]{'o', 'e'});// œ

        addSearchReplacement('\u0154', 'R');
        addSearchReplacement('\u0155', 'r');
        addSearchReplacement('\u0156', 'R');
        addSearchReplacement('\u0157', 'r');
        addSearchReplacement('\u0158', 'R');
        addSearchReplacement('\u0159', 'r');

        addSearchReplacement('\u015A', 'S');
        addSearchReplacement('\u015B', 's');
        addSearchReplacement('\u015C', 'S');
        addSearchReplacement('\u015D', 's');
        addSearchReplacement('\u015E', 'S');
        addSearchReplacement('\u015F', 's');
        addSearchReplacement('\u0160', 'S');
        addSearchReplacement('\u0161', 's');

        addSearchReplacement('\u0162', 'T');
        addSearchReplacement('\u0163', 't');
        addSearchReplacement('\u0164', 'T');
        addSearchReplacement('\u0165', 't');
        addSearchReplacement('\u0166', 'T');
        addSearchReplacement('\u0167', 't');

        addSearchReplacement('\u0168', 'U');
        addSearchReplacement('\u0169', 'u');
        addSearchReplacement('\u016A', 'U');
        addSearchReplacement('\u016B', 'u');
        addSearchReplacement('\u016C', 'U');
        addSearchReplacement('\u016D', 'u');
        addSearchReplacement('\u016E', 'U');
        addSearchReplacement('\u016F', 'u');
        addSearchReplacement('\u0170', 'U');
        addSearchReplacement('\u0171', 'u');
        addSearchReplacement('\u0172', 'U');
        addSearchReplacement('\u0173', 'u');

        addSearchReplacement('\u0174', 'W');
        addSearchReplacement('\u0175', 'w');


        addSearchReplacement('\u0176', 'Y');
        addSearchReplacement('\u0177', 'y');
        addSearchReplacement('\u0178', 'Y');

        addSearchReplacement('\u0179', 'Z');
        addSearchReplacement('\u017A', 'z');
        addSearchReplacement('\u017B', 'Z');
        addSearchReplacement('\u017C', 'z');
        addSearchReplacement('\u017D', 'Z');
        addSearchReplacement('\u017E', 'z');
    }

    /**
     * 0180—024F
     */
    private static void latin1ExtendedB() {
        addSearchReplacement('\u01CD', 'A');
        addSearchReplacement('\u01CE', 'a');

        addSearchReplacement('\u01CF', 'I');
        addSearchReplacement('\u01D0', 'i');

        addSearchReplacement('\u01D1', 'O');
        addSearchReplacement('\u01D2', 'o');

        addSearchReplacement('\u01D3', 'U');
        addSearchReplacement('\u01D4', 'u');
        addSearchReplacement('\u01D5', 'U');
        addSearchReplacement('\u01D6', 'u');
        addSearchReplacement('\u01D7', 'U');
        addSearchReplacement('\u01D8', 'u');
        addSearchReplacement('\u01D9', 'U');
        addSearchReplacement('\u01DA', 'u');
        addSearchReplacement('\u01DB', 'U');
        addSearchReplacement('\u01DC', 'u');

        addSearchReplacement('\u01E6', 'G');
        addSearchReplacement('\u01E7', 'g');

        addSearchReplacement('\u01E8', 'K');
        addSearchReplacement('\u01E9', 'k');

        addSearchReplacement('\u01F4', 'G');
        addSearchReplacement('\u01F5', 'g');

        addSearchReplacement('\u01F8', 'N');
        addSearchReplacement('\u01F9', 'n');


        addSearchReplacement('\u0200', 'A');
        addSearchReplacement('\u0201', 'a');
        addSearchReplacement('\u0202', 'A');
        addSearchReplacement('\u0203', 'a');

        addSearchReplacement('\u0204', 'E');
        addSearchReplacement('\u0205', 'e');
        addSearchReplacement('\u0206', 'E');
        addSearchReplacement('\u0207', 'e');

        addSearchReplacement('\u0208', 'I');
        addSearchReplacement('\u0209', 'i');
        addSearchReplacement('\u020A', 'I');
        addSearchReplacement('\u020B', 'i');

        addSearchReplacement('\u020C', 'O');
        addSearchReplacement('\u020D', 'o');
        addSearchReplacement('\u020E', 'O');
        addSearchReplacement('\u020F', 'o');

        addSearchReplacement('\u0210', 'R');
        addSearchReplacement('\u0211', 'r');
        addSearchReplacement('\u0212', 'R');
        addSearchReplacement('\u0213', 'r');

        addSearchReplacement('\u0214', 'U');
        addSearchReplacement('\u0215', 'u');
        addSearchReplacement('\u0216', 'U');
        addSearchReplacement('\u0217', 'u');

        addSearchReplacement('\u0218', 'S');
        addSearchReplacement('\u0219', 's');

        addSearchReplacement('\u021A', 'T');
        addSearchReplacement('\u021B', 't');

        addSearchReplacement('\u021E', 'H');
        addSearchReplacement('\u021F', 'h');

        addSearchReplacement('\u0226', 'A');
        addSearchReplacement('\u0227', 'a');

        addSearchReplacement('\u0228', 'E');
        addSearchReplacement('\u0229', 'e');

        addSearchReplacement('\u022E', 'O');
        addSearchReplacement('\u022F', 'o');


        // euro sign is not supported, but might not be that unusual, therefore replace it with "EUR" if present
        addSearchReplacement('\u20AC', new char[]{'E', 'U', 'R'});// €
    }

    /**
     * 02B0—02FF
     */
    private static void spacingModifierLetters() {
        // replace all single-quotes and "single-quotes-looking" chars with simple single quote
        // replace all double-quotes and "double-quotes-looking" chars with simple double quote
        addSearchReplacement('\u02B9', '\'');
        addSearchReplacement('\u02BA', '"');
        addSearchReplacement('\u02BB', '\'');
        addSearchReplacement('\u02BC', '\'');
        addSearchReplacement('\u02BD', '\'');
        addSearchReplacement('\u02BE', '\'');
        addSearchReplacement('\u02CA', '\'');
        addSearchReplacement('\u02CB', '\'');
        addSearchReplacement('\u02F4', '\'');
        addSearchReplacement('\u02F5', '"');
        addSearchReplacement('\u02F6', '"');
    }

    /**
     * 0300—036F
     */
    private static void combiningDiacriticalMarks() {
        addSearchReplacement('\u0300', '\'');
        addSearchReplacement('\u0301', '\'');
        addSearchReplacement('\u030B', '"');
        addSearchReplacement('\u030D', '\'');
        addSearchReplacement('\u030E', '"');
        addSearchReplacement('\u030F', '"');
        addSearchReplacement('\u0312', '\'');
        addSearchReplacement('\u0313', '\'');
        addSearchReplacement('\u0314', '\'');
        addSearchReplacement('\u0315', '\'');
    }

    /**
     * 0370—03FF
     */
    private static void greekAndCoptic() {

    }

    /**
     * 0400—04FF
     */
    private static void cyrillic() {
        addSearchReplacement('\u0400', 'E');
        addSearchReplacement('\u0401', 'E');
        addSearchReplacement('\u0405', 'S');
        addSearchReplacement('\u0406', 'I');
        addSearchReplacement('\u0407', 'I');
        addSearchReplacement('\u0408', 'J');
        addSearchReplacement('\u0450', 'e');
        addSearchReplacement('\u0451', 'e');
        addSearchReplacement('\u0455', 's');
        addSearchReplacement('\u0456', 'i');
        addSearchReplacement('\u0457', 'i');
        addSearchReplacement('\u0458', 'j');
    }

    /**
     * 0500—052F
     */
    private static void cyrillicSupplement() {

    }

    /**
     * 1E00—1EFF
     */
    private static void latinExtendedAdditional() {

    }

    /**
     * 1F00—1FFF
     */
    private static void greekExtended() {

    }

    /**
     * 2000—206F
     */
    private static void generalPunctuation() {
        // replace all dash variants and the middle dot '·'
        addSearchReplacement('\u2010', '-');
        addSearchReplacement('\u2011', '-');
        addSearchReplacement('\u2012', '-');
        addSearchReplacement('\u2013', '-');
        addSearchReplacement('\u2014', '-');
        addSearchReplacement('\u2015', '-');

        // replace all single-quotes and "single-quotes-looking" chars with simple single quote
        addSearchReplacement('\u2018', '\'');// ‘
        addSearchReplacement('\u2019', '\'');// ’

        // replace all double-quotes and "double-quotes-looking" chars with simple double quote
        addSearchReplacement('\u201C', '"');// “
        addSearchReplacement('\u201D', '"');// ”
        addSearchReplacement( '\u201E', '"');// „
        addSearchReplacement('\u201F', '"');// ‟
    }

    /**
     * 2070—209F
     */
    private static void superscriptsAndSubscripts() {

    }

}
