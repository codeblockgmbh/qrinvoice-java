/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.alternativeschemes.ebill;

import ch.codeblock.qrinvoice.model.ParseException;
import ch.codeblock.qrinvoice.model.annotation.Description;

public enum Type {
    /**
     * <p>From the specification "Specification on the Use of the Alternative Scheme eBill in the Swiss QR Code" v1.1</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th></tr>
     * <tr><td>EN</td><td>Invoices and rolled over invoices</td></tr>
     * <tr><td>DE</td><td>Rechnungen und überrollende Rechnungen</tr>
     * <tr><td>FR</td><td>TODO</tr>
     * <tr><td>IT</td><td>TODO</tr>
     * </table>
     */
    @Description("Invoices and rolled over invoices")
    BILL("B"),
    /**
     * <p>From the specification "Specification on the Use of the Alternative Scheme eBill in the Swiss QR Code" v1.1</p>
     * <table border="1" summary="Excerpt from the specification">
     * <tr><th>Language</th><th>General Definition</th></tr>
     * <tr><td>EN</td><td>Reminders</td></tr>
     * <tr><td>DE</td><td>Mahnungen</tr>
     * <tr><td>FR</td><td>TODO</tr>
     * <tr><td>IT</td><td>TODO</tr>
     * </table>
     */
    @Description("Reminders")
    REMINDER("R");

    private final String type;

    Type(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static Type parse(String type) {
        for (final Type t : values()) {
            if (t.getType().equalsIgnoreCase(type) || t.name().equalsIgnoreCase(type)) {
                return t;
            }
        }
        throw new ParseException("Invalid EBill type '" + type + "' given");
    }
}
