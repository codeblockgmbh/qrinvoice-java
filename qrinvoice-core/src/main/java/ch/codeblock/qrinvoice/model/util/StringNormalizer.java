/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.util;

import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringNormalizer {
    private static final Logger logger = LoggerFactory.getLogger(StringNormalizer.class);
    private final ImplementationGuidelinesQrBillVersion qrBillVersion;

    private boolean trim;
    private boolean replaceLineBreaks;
    private boolean replaceTabs;
    private boolean replaceCharactersSameLength;
    private boolean replaceCharactersWithMultipleChars;
    private boolean removeUnsupportedCharacters;

    private StringNormalizer(ImplementationGuidelinesQrBillVersion qrBillVersion) {
        this.qrBillVersion = qrBillVersion;
    }

    /**
     * @deprecated This method is to be replaced with {@link #create(ImplementationGuidelinesQrBillVersion)}
     * this method will always use the version {@link ImplementationGuidelinesQrBillVersion#V2_2}
     *
     * @return new instance
     */
    @Deprecated
    public static StringNormalizer create() {
        return new StringNormalizer(ImplementationGuidelinesQrBillVersion.V2_2);
    }

    public static StringNormalizer create(ImplementationGuidelinesQrBillVersion qrBillVersion) {
        return new StringNormalizer(qrBillVersion);
    }

    public StringNormalizer enableTrim() {
        return enableTrim(true);
    }

    public StringNormalizer enableTrim(boolean enabled) {
        this.trim = enabled;
        return this;
    }

    public StringNormalizer enableReplaceLineBreaks() {
        return enableReplaceLineBreaks(true);
    }

    public StringNormalizer enableReplaceLineBreaks(boolean enabled) {
        this.replaceLineBreaks = enabled;
        return this;
    }

    public StringNormalizer enableReplaceTabs() {
        return enableReplaceTabs(true);
    }

    public StringNormalizer enableReplaceTabs(boolean enabled) {
        this.replaceTabs = enabled;
        return this;
    }

    /**
     * Enables both {@link #enableReplaceCharactersSameLength()} and {@link #enableReplaceCharactersWithMultipleChars()}
     *
     * @return self
     */
    public StringNormalizer enableReplaceCharacters() {
        enableReplaceCharactersSameLength();
        enableReplaceCharactersWithMultipleChars();
        return this;
    }

    /**
     * Sets both {@link #enableReplaceCharactersSameLength(boolean)} and {@link #enableReplaceCharactersWithMultipleChars(boolean)}
     *
     * @return self
     */
    public StringNormalizer enableReplaceCharacters(boolean enabled) {
        enableReplaceCharactersSameLength(enabled);
        enableReplaceCharactersSameLength(enabled);
        return this;
    }

    public StringNormalizer enableReplaceCharactersSameLength() {
        return enableReplaceCharactersSameLength(true);
    }

    public StringNormalizer enableReplaceCharactersSameLength(boolean enabled) {
        this.replaceCharactersSameLength = enabled;
        return this;
    }

    public StringNormalizer enableReplaceCharactersWithMultipleChars() {
        return enableReplaceCharactersWithMultipleChars(true);
    }

    public StringNormalizer enableReplaceCharactersWithMultipleChars(boolean enabled) {
        this.replaceCharactersWithMultipleChars = enabled;
        return this;
    }

    public StringNormalizer enableRemoveUnsupportedCharacters() {
        return enableRemoveUnsupportedCharacters(true);
    }

    public StringNormalizer enableRemoveUnsupportedCharacters(boolean enabled) {
        this.removeUnsupportedCharacters = enabled;
        return this;
    }

    public String normalize(String input) {
        if (input == null) {
            return null;
        }

        final boolean isV2_2 = qrBillVersion.equals(ImplementationGuidelinesQrBillVersion.V2_2);

        final StringBuilder sb = new StringBuilder(input);
        for (int i = 0; i < sb.length(); i++) {
            final boolean hasMoreChars = i < sb.length() - 1;
            final char chr = sb.charAt(i);

            if (replaceLineBreaks) {
                // first replace \r\n by \r in order to get rid of duplicate spaces
                if (chr == '\r' && hasMoreChars && sb.charAt(i + 1) == '\n') {
                    sb.deleteCharAt(i + 1);
                }

                if (chr == '\r' || chr == '\n') {
                    sb.setCharAt(i, ' ');
                }
            }

            if (replaceTabs && chr == '\t') {
                sb.setCharAt(i, ' ');
            }

            if (replaceCharactersSameLength) {
                final char[] singleCharSearch = isV2_2 ? StringNormalizationsLteV2_2.singleCharSearch : StringNormalizationsV2_3.singleCharSearch;
                final char[] singleCharReplacement = isV2_2 ? StringNormalizationsLteV2_2.singleCharReplacement : StringNormalizationsV2_3.singleCharReplacement;
                for (int searchReplaceIdx = 0; searchReplaceIdx < singleCharSearch.length; searchReplaceIdx++) {
                    if (singleCharSearch[searchReplaceIdx] == chr) {
                        sb.setCharAt(i,  singleCharReplacement[searchReplaceIdx]);
                    }
                }
            }

            if (replaceCharactersWithMultipleChars) {
                final char[] multipleCharSearch = isV2_2 ? StringNormalizationsLteV2_2.multipleCharSearch : StringNormalizationsV2_3.multipleCharSearch;
                final char[][] multipleCharReplacement = isV2_2 ? StringNormalizationsLteV2_2.multipleCharReplacement : StringNormalizationsV2_3.multipleCharReplacement;
                for (int searchReplaceIdx = 0; searchReplaceIdx < multipleCharSearch.length; searchReplaceIdx++) {
                    if (multipleCharSearch[searchReplaceIdx] == chr) {
                        final char[] replacementChars = multipleCharReplacement[searchReplaceIdx];
                        for (int j = 0; j < replacementChars.length; j++) {
                            if (j == 0) {
                                sb.setCharAt(i, replacementChars[j]);
                            } else {
                                sb.insert(++i, replacementChars[j]);
                            }
                        }
                    }
                }
            }
        }

        if (removeUnsupportedCharacters) {
            for (int i = 0; i < sb.length(); i++) {
                final String chr = sb.substring(i, i+1);

                final String validChars = isV2_2 ? SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2 : SwissPaymentsCode.VALID_CHARACTERS_GTE_2_3;

                if (!validChars.contains(chr)) {
                    sb.deleteCharAt(i);
                }
            }
        }

        String result = sb.toString();
        if (trim) {
            result = result.trim();
        }

        if (logger.isDebugEnabled() && !input.equals(result)) {
            logger.debug("String normalized - input={} result={}", input, sb);
        }

        return result;
    }

    public StringNormalizer enableAll() {
        return enableTrim()
                .enableReplaceLineBreaks()
                .enableReplaceTabs()
                .enableReplaceCharactersSameLength()
                .enableReplaceCharactersWithMultipleChars()
                .enableRemoveUnsupportedCharacters();
    }
}
