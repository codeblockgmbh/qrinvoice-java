/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.alternativeschemes;

import java.util.EnumSet;
import java.util.Set;

public class RawAlternativeSchemeParameterType implements AlternativeSchemeParameterType {
    private static final long serialVersionUID = 1633403560028570919L;
    private static Set<AlternativeSchemeParameterTypes> nonGeneralTypes;

    private static final RawAlternativeSchemeParameterType INSTANCE = new RawAlternativeSchemeParameterType();

    private RawAlternativeSchemeParameterType() {
    }

    public static RawAlternativeSchemeParameterType getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean supports(final String alternativeSchemeParameter) {
        return getNonGeneralTypes().stream().noneMatch(type -> type.getAlternativeSchemeParameterType().supports(alternativeSchemeParameter));
    }

    private Set<AlternativeSchemeParameterTypes> getNonGeneralTypes() {
        if (nonGeneralTypes == null) {
            nonGeneralTypes = EnumSet.complementOf(EnumSet.of(AlternativeSchemeParameterTypes.GENERAL));
        }
        return nonGeneralTypes;
    }

    @Override
    public RawAlternativeSchemeParameter parse(final String alternativeSchemeParameter) {
        return new RawAlternativeSchemeParameter(alternativeSchemeParameter);
    }
}
