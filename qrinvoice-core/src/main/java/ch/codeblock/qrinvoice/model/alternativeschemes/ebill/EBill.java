/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.alternativeschemes.ebill;

import ch.codeblock.qrinvoice.model.ParseException;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.annotation.Description;
import ch.codeblock.qrinvoice.model.annotation.Example;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import ch.codeblock.qrinvoice.util.StringUtils;

import java.util.stream.Stream;

import static ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBillType.*;

public class EBill implements AlternativeSchemeParameter {

    private Type type;

    /**
     * {@link EBillType#EMAIL_PATTERN}
     */
    private String recipientEmailAddress;
    /**
     * {@link EBillType#BILL_RECIPIENT_ID_PATTERN}
     */
    private String recipientId;
    /**
     * {@link EBillType#NORMALIZED_UID_PATTERN}
     */
    private String recipientEnterpriseIdentificationNumber;
    /**
     * (invoice number of the referenced business case): This sub-element is optional. It
     * only has to be filled for reminders and rolled over invoices in order to identify the original invoice.
     */
    private String referencedBill;


    @Example("BILL")
    @Description("The identifier of the business case type")
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Description("E-mail address of the invoice recipient")
    @Example("john.doe@example.com")
    public String getRecipientEmailAddress() {
        return recipientEmailAddress;
    }

    public void setRecipientEmailAddress(String recipientEmailAddress) {
        this.recipientEmailAddress = StringUtils.trimToNull(recipientEmailAddress);
    }

    @Description("Unique identification of the invoice recipient (e.g. PID)")
    @Example("41010560425610173")
    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = StringUtils.trimToNull(recipientId);
    }

    public void setBillRecipientId(Integer billRecipientId) {
        setRecipientId(String.valueOf(billRecipientId));
    }

    public void setRecipientIdentification(String recipientIdentificaton) {
        final String id = StringUtils.trimToNull(recipientIdentificaton);
        if (id == null) {
            return;
        }
        if (NORMALIZED_UID_PATTERN.matcher(id).matches()) {
            setRecipientEnterpriseIdentificationNumber(id);
        } else if (EMAIL_PATTERN.matcher(id).matches()) {
            setRecipientEmailAddress(id);
        } else if (BILL_RECIPIENT_ID_PATTERN.matcher(id).matches()) {
            setRecipientId(id);
        } else {
            throw new ParseException("no matching recipient identification type");
        }
    }

    @Description("Enterprise identification number (UID) for companies as invoice recipients (in the eBill for Business context).")
    @Example("CHE123456789")
    public String getRecipientEnterpriseIdentificationNumber() {
        return recipientEnterpriseIdentificationNumber;
    }

    public void setRecipientEnterpriseIdentificationNumber(String recipientEnterpriseIdentificationNumber) {
        this.recipientEnterpriseIdentificationNumber = StringUtils.trimToNull(recipientEnterpriseIdentificationNumber);
    }

    @Example("2018-123456-22")
    @Description("invoice number of the referenced business case: This sub-element is optional. It only has to be filled for reminders and rolled over invoices in order to identify the original invoice.")
    public String getReferencedBill() {
        return referencedBill;
    }

    public void setReferencedBill(String referencedBill) {
        this.referencedBill = StringUtils.trimToNull(referencedBill);
    }

    public String getRecipientIdentification() {
        return Stream.of(recipientId, recipientEmailAddress, recipientEnterpriseIdentificationNumber)
                .filter(StringUtils::isNotBlank)
                .findFirst()
                .orElse(null);
    }

    @Override
    public String getAlternativeSchemeParameterType() {
        return PREFIX;
    }

    @Override
    public ValidationResult validate() {
        final ValidationResult validationResult = new ValidationResult();
        validate(validationResult, null);
        return validationResult;
    }

    @Override
    public ValidationResult validate(QrInvoice qrInvoice) {
        final ValidationResult validationResult = new ValidationResult();
        validate(validationResult, qrInvoice);
        return validationResult;
    }

    @Override
    public void validate(ValidationResult result, QrInvoice qrInvoice) {
        validateType(result);
        validateEnterpriseIdentificationNumber(result);
        validateEmailAddress(result);
        validateBillRecipientId(result);
        validateCrossDependentElements(result);
    }


    @Override
    public void validate(ValidationResult result) {
        validate(result, null);
    }

    private void validateType(final ValidationResult result) {
        if (type == null) {
            result.addError("altPmtInf.altPmt.ebill", "type", type, "{{validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.type}}");
        }
    }

    private void validateEnterpriseIdentificationNumber(final ValidationResult result) {
        if (StringUtils.isNotEmpty(recipientEnterpriseIdentificationNumber) && !NORMALIZED_UID_PATTERN.matcher(recipientEnterpriseIdentificationNumber).matches()) {
            result.addError("altPmtInf.altPmt.ebill", "enterpriseIdentificationNumber", recipientEnterpriseIdentificationNumber, "{{validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.enterpriseIdentificationNumber}}");
        }
    }

    private void validateEmailAddress(ValidationResult result) {
        if (StringUtils.isNotEmpty(recipientEmailAddress) && !EMAIL_PATTERN.matcher(recipientEmailAddress).matches()) {
            result.addError("altPmtInf.altPmt.ebill", "emailAddress", recipientEmailAddress, "{{validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.emailAddress}}");
        }
    }

    private void validateBillRecipientId(ValidationResult result) {
        if (StringUtils.isNotEmpty(recipientId) && !BILL_RECIPIENT_ID_PATTERN.matcher(recipientId).matches()) {
            result.addError("altPmtInf.altPmt.ebill", "billRecipientId", recipientId, "{{validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.billRecipientId}}");
        }
    }

    private void validateCrossDependentElements(ValidationResult result) {
        if (Stream.of(recipientId, recipientEmailAddress, recipientEnterpriseIdentificationNumber)
                .filter(StringUtils::isNotBlank)
                .count() != 1) {
            result.addError("altPmtInf.altPmt.ebill", "recipientIdentification", null, "{{validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.recipientIdentification}}");
        }
    }

    public String toAlternativeSchemeParameterString() {
        if (type == null) {
            throw new ValidationException("EBill: invalid or no type was provided");
        }
        
        final StringBuilder sb = new StringBuilder();
        // only safety guards to make sure no unexpected whitespaces are present
        assertNoWhitespaces(type.getType(), "AltPmtInf/AltPmt/eBill/type");

        assertNoWhitespaces(recipientEmailAddress, "AltPmtInf/AltPmt/eBill/emailAddress");
        assertNoWhitespaces(recipientId, "AltPmtInf/AltPmt/eBill/billRecipientId");
        assertNoWhitespaces(recipientEnterpriseIdentificationNumber, "AltPmtInf/AltPmt/eBill/enterpriseIdentificationNumber");

        assertNoWhitespaces(referencedBill, "AltPmtInf/AltPmt/eBill/referencedBill");

        appendField(sb, PREFIX, true);
        appendField(sb, type.getType(), true);
        appendField(sb, getRecipientIdentification(), StringUtils.isNotBlank(referencedBill));
        appendField(sb, referencedBill, false);

        return sb.toString();
    }

    public void assertNoWhitespaces(final String str, final String field) {
        if (StringUtils.containsWhitespace(str)) {
            throw new ValidationException("EBill: " + field + " did unexpectedly contain whitespace(s)");
        }
    }

    private void appendField(final StringBuilder sb, final String value, final boolean appendElementSeparator) {
        if (value != null) {
            sb.append(value);
            if (appendElementSeparator) {
                sb.append(ELEMENT_SEPARATOR);
            }
        }
    }

}
