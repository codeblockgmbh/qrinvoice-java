/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model;

import java.time.LocalDate;

/**
 * <p>Implementation Guidelines QR-bill Version</p>
 * <table border="1" summary="Implementation Guidelines QR-bill Version History">
 * <tr><th>Version</th><th>Valid from</th><th>Valid to</th><th>Remarks</th></tr>
 * <tr><td>2.0 - 2.2</td><td>30.06.2020</td><td>(20.11.2025)</td><td>Version 2.2 is still valid, but the {@link AddressType#COMBINED} address type is discontinued.</td></tr>
 * <tr><td>2.3</td><td>21.11.2025</td><td></td><td>Address type {@link AddressType#COMBINED} is no longer supported. Character Set enhancement.</td></tr>
 * </table>
 */
public enum ImplementationGuidelinesQrBillVersion {
    /**
     * Version 2.2
     */
    V2_2,
    /**
     * Version 2.3
     */
    V2_3;
    private static final LocalDate RELEASE_V2_3 = LocalDate.of(2025, 11, 21);

    public static ImplementationGuidelinesQrBillVersion latestActive() {
        return forDate(LocalDate.now());
    }

    public static ImplementationGuidelinesQrBillVersion forDate(LocalDate date) {
        if (date.isBefore(RELEASE_V2_3)) {
            return V2_2;
        }
        return V2_3;
    }
}
