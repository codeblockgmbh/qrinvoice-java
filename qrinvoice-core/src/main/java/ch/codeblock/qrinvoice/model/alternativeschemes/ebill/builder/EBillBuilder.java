/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.alternativeschemes.ebill.builder;

import ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill;
import ch.codeblock.qrinvoice.model.alternativeschemes.ebill.Type;

public final class EBillBuilder {
    private Type type;
    private String emailAddress;
    private String billRecipientId;
    private String enterpriseIdentificationNumber;
    private String referencedBill;

    private EBillBuilder() {
    }

    public static EBillBuilder create() {
        return new EBillBuilder();
    }

    public EBillBuilder reminder() {
        return type(Type.REMINDER);
    }

    public EBillBuilder bill() {
        return type(Type.BILL);
    }

    public EBillBuilder type(Type type) {
        this.type = type;
        return this;
    }

    public EBillBuilder emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public EBillBuilder billRecipientId(String billRecipientId) {
        this.billRecipientId = billRecipientId;
        return this;
    }

    public EBillBuilder billRecipientId(int billRecipientId) {
        return billRecipientId(String.valueOf(billRecipientId));
    }

    public EBillBuilder enterpriseIdentificationNumber(String enterpriseIdentificationNumber) {
        this.enterpriseIdentificationNumber = enterpriseIdentificationNumber;
        return this;
    }

    public EBillBuilder referencedBill(String referencedBill) {
        this.referencedBill = referencedBill;
        return this;
    }

    public EBill build() {
        EBill eBill = new EBill();
        eBill.setType(type);
        eBill.setRecipientEmailAddress(emailAddress);
        eBill.setRecipientId(billRecipientId);
        eBill.setRecipientEnterpriseIdentificationNumber(enterpriseIdentificationNumber);
        eBill.setReferencedBill(referencedBill);
        return eBill;
    }

}
