/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.alternativeschemes.ebill;

import ch.codeblock.qrinvoice.model.ParseException;
import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameterType;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;
import java.util.regex.Pattern;

import static ch.codeblock.qrinvoice.util.StringUtils.emptyStringAsNull;


public class EBillType implements AlternativeSchemeParameterType {
    private final Logger logger = LoggerFactory.getLogger(EBillType.class);

    private static final long serialVersionUID = -750753109819742398L;
    private static final EBillType INSTANCE = new EBillType();

    public static final String PREFIX = "eBill";
    public static final String ELEMENT_SEPARATOR = "/";
    public static final Pattern EMAIL_PATTERN = Pattern.compile("^(.+)@(.+)\\.(.+)$");
    public static final Pattern BILL_RECIPIENT_ID_PATTERN = Pattern.compile("^\\d{1,17}$");
    public static final Pattern NORMALIZED_UID_PATTERN = Pattern.compile("^CHE\\d{9}$");

    private EBillType() {
    }

    public static EBillType getInstance() {
        return INSTANCE;
    }

    @Override
    public boolean supports(String alternativeSchemeParameter) {
        return alternativeSchemeParameter != null && alternativeSchemeParameter.startsWith(PREFIX);
    }

    @Override
    public EBill parse(final String alternativeSchemeParameter) {
        try {
            return internalParse(alternativeSchemeParameter);
        } catch (RuntimeException e) {
            throw new ParseException("Error while parsing alternativeSchemeParameter", e);
        }
    }

    private EBill internalParse(String alternativeSchemeParameter) {
        alternativeSchemeParameter = StringUtils.trimToNull(alternativeSchemeParameter);
        if (alternativeSchemeParameter.startsWith(PREFIX)) {
            try (final Scanner scanner = new Scanner(alternativeSchemeParameter).useDelimiter(ELEMENT_SEPARATOR)) {
                final EBill eBill = new EBill();
                read(scanner); // prefix
                try {
                    final Type parse = Type.parse(read(scanner));
                    eBill.setType(parse);
                } catch (Exception e) {
                    logger.debug("Unable to parse EBill Type. Reason={}", e.getMessage());
                }
                eBill.setRecipientIdentification(read(scanner));
                eBill.setReferencedBill(read(scanner));
                return eBill;
            }
        }
        throw new ParseException("Error while trying to parse EBill '" + alternativeSchemeParameter + "' given");
    }


    private String read(final Scanner scanner) {
        if (scanner.hasNext()) {
            return emptyStringAsNull(scanner.next());
        } else {
            return null;
        }
    }
}
