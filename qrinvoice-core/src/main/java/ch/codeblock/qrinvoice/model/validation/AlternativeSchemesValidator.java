/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import ch.codeblock.qrinvoice.model.AlternativeSchemes;
import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameter;
import ch.codeblock.qrinvoice.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static ch.codeblock.qrinvoice.model.SwissPaymentsCode.MAX_ALT_PMT;
import static ch.codeblock.qrinvoice.model.validation.ValidationUtils.validateOptionalLength;

public class AlternativeSchemesValidator {
    private final Logger logger = LoggerFactory.getLogger(AlternativeSchemesValidator.class);

    private final ImplementationGuidelinesQrBillVersion qrBillVersion;

    private AlternativeSchemesValidator(ImplementationGuidelinesQrBillVersion qrBillVersion) {
        this.qrBillVersion = qrBillVersion;
    }

    /**
     * Creates a new instance that uses the specific {@link ImplementationGuidelinesQrBillVersion}
     *
     * @param qrBillVersion The desired {@link ImplementationGuidelinesQrBillVersion}
     * @return new instance
     */
    public static AlternativeSchemesValidator create(ImplementationGuidelinesQrBillVersion qrBillVersion) {
        return new AlternativeSchemesValidator(qrBillVersion);
    }

    /**
     * @return new instance
     * @deprecated This method is to be replaced with {@link #create(ImplementationGuidelinesQrBillVersion)}
     * this method will always use the version {@link ImplementationGuidelinesQrBillVersion#V2_2}
     */
    @Deprecated
    public static AlternativeSchemesValidator create() {
        return create(ImplementationGuidelinesQrBillVersion.V2_2);
    }

    public void validate(final ValidationOptions validationOptions, final AlternativeSchemes alternativeSchemes, final ValidationResult result) {
        if (alternativeSchemes != null) {
            final List<String> alternativeSchemeParameters = alternativeSchemes.getAlternativeSchemeParameters();
            final int size = CollectionUtils.size(alternativeSchemeParameters);
            if (size > MAX_ALT_PMT) {
                result.addError("alternativeSchemes", "alternativeSchemeParameters", size, "{{validation.error.alternativeSchemes.alternativeSchemeParameters.size}}");
            }
            if (size > 0) {
                for (final String alternativeSchemeParameter : alternativeSchemeParameters) {
                    validateSingleScheme(validationOptions, alternativeSchemeParameter, result);
                }
            }

            if (validationOptions.isSkipAlternativeSchemeParameterObject()) {
                try {
                    if (alternativeSchemes.getAlternativeSchemeParameterObjects() != null) {
                        for (final AlternativeSchemeParameter alternativeSchemeParameterObject : alternativeSchemes.getAlternativeSchemeParameterObjects()) {
                            alternativeSchemeParameterObject.validate(result, null /* not needed here */);
                        }
                    }
                } catch (Exception e) {
                    logger.debug("Unable to validate Alternative Scheme Parameter. Reason={}", e.getMessage());
                }
            }
        }
    }

    private void validateSingleScheme(ValidationOptions options, String param, ValidationResult result) {
        validateOptionalLength(param, 0, 100, (value) -> result.addError("alternativeSchemes", "alternativeSchemeParameters", value, "{{validation.error.alternativeSchemes.alternativeSchemeParameter.length}}"));
        ValidationUtils.validateString(param, qrBillVersion, (value, msgs) -> result.addError("alternativeSchemes", "alternativeSchemeParameters", value, msgs));
    }

}
