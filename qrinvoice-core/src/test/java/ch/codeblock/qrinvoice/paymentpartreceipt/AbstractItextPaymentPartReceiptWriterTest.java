/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.paymentpartreceipt;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AbstractItextPaymentPartReceiptWriterTest {
    private AbstractItextPaymentPartReceiptWriter abstractItextPaymentPartReceiptWriter;
    private float informationSectionWidth;
    private Function<String, Float> widthFunction;

    @Before
    public void init() {
        final PaymentPartReceiptWriterOptions options = new PaymentPartReceiptWriterOptions();
        options.setLocale(Locale.GERMAN);
        abstractItextPaymentPartReceiptWriter = new AbstractItextPaymentPartReceiptWriter(options) {
            @Override
            public PaymentPartReceipt write(final QrInvoice qrInvoice, final BufferedImage qrCodeImage) {
                return new PaymentPartReceipt(PageSize.DIN_LANG, OutputFormat.PDF, new byte[0],0,0);
            }
        };
        informationSectionWidth = 210;
        // calculate as if one char would take a width of 3, that means for 210 total width, 70 chars could be place one one line
        widthFunction = (str) -> ((float) str.length() * 3);
    }

    @Test
    public void testOneLiner() throws Exception {
        final AtomicBoolean aggressiveSplitting = new AtomicBoolean(false);
        final String text = "Robert Schneider Services Switzerland AG Robert Schneider Services"; // 66 chars
        // calculate as if one char would take a width of 3, that means for 210 total width, 70 chars could be place one one line
        abstractItextPaymentPartReceiptWriter.applyOptimalLineSplitting(text, informationSectionWidth, widthFunction, () -> aggressiveSplitting.set(true));
        assertFalse(aggressiveSplitting.get());
    }

    @Test
    public void testNaturalWrappingApplies() throws Exception {
        final AtomicBoolean aggressiveSplitting = new AtomicBoolean(false);
        // line 1: Robert Schneider Services Switzerland AG Robert Schneider Services 
        // line 2: Switzerland AG Robert Schneider Services Switzerland AG Robert
        final String text = "Robert Schneider Services Switzerland AG Robert Schneider Services Switzerland AG Robert Schneider Services Switzerland AG Robert"; // 129
        // calculate as if one char would take a width of 3, that means for 210 total width, 70 chars could be place one one line
        abstractItextPaymentPartReceiptWriter.applyOptimalLineSplitting(text, informationSectionWidth, widthFunction, () -> aggressiveSplitting.set(true));
        assertFalse(aggressiveSplitting.get());
    }

    @Test
    public void testAggressiveSplittingAppliesTwoLines() throws Exception {
        final AtomicBoolean aggressiveSplitting = new AtomicBoolean(false);
        final String text = "Robert Schneider Services Switzerland AG RobertSchneiderServicesSwitzerlandAGRobertSchneiderServicesSwitzerlandAGRobert"; // 119
        // calculate as if one char would take a width of 3, that means for 210 total width, 70 chars could be place one one line
        abstractItextPaymentPartReceiptWriter.applyOptimalLineSplitting(text, informationSectionWidth, widthFunction, () -> aggressiveSplitting.set(true));
        assertTrue(aggressiveSplitting.get());
    }

    @Test
    public void testAggressiveSplittingAppliesMoreThanTwoLines() throws Exception {
        final AtomicBoolean aggressiveSplitting = new AtomicBoolean(false);
        final String text = "Robert Schneider Services Switzerland AG Robert Schneider Services Switzerland AG Robert Schneider Services Switzerland AG Robert Schneider Services Switzerland AG"; // 163

        abstractItextPaymentPartReceiptWriter.applyOptimalLineSplitting(text, informationSectionWidth, widthFunction, () -> aggressiveSplitting.set(true));
        assertTrue(aggressiveSplitting.get());
    }

}
