/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice;

import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.ParseException;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.validation.QrInvoiceValidator;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class QrInvoiceCodeParserTest {

    @Test(expected = ParseException.class)
    public void parseWithNull() {
        QrInvoiceCodeParser.create().parse((String) null);
    }

    @Test(expected = ParseException.class)
    public void parseWithTypeOnly() {
        QrInvoiceCodeParser.create().parse("SPC");
    }

    @Test(expected = ParseException.class)
    public void parseWithTypeAndVersion() {
        QrInvoiceCodeParser.create().parse("SPC\n" +
                "0200");
    }

    @Test
    public void parseWithTypeAndVersionCodingTypeLf() {
        QrInvoiceCodeParser.create().parse("SPC\n" +
                "0200\n" +
                "1\n" + generatedEPDafterHeader(3, "\n"));
    }

    @Test
    public void parseWithTypeAndVersionCodingTypeCr() {
        QrInvoiceCodeParser.create().parse("SPC\r" +
                "0200\r" +
                "1\n" + generatedEPDafterHeader(3, "\n"));
    }

    @Test
    public void parseWithTypeAndVersionCodingTypeCrLf() {
        QrInvoiceCodeParser.create().parse("SPC\r\n" +
                "0200\r\n" +
                "1\n" + generatedEPDafterHeader(3, "\n"));
    }

    @Test
    public void parseAndManuallyValidateWithTypeAndVersionCodingType() {
        final ValidationResult validationResult = QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(QrInvoiceCodeParser.create().parse("SPC\n" +
                "0200\n" +
                "1\n" + generatedEPDafterHeader(3, "\n")));

        System.out.println(validationResult.getValidationErrorSummary());
    }

    @Test(expected = ValidationException.class)
    public void parseAndValidateWithTypeAndVersionCodingType() {
        QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(QrInvoiceCodeParser.create().parseAndValidate("SPC\n" +
                "0200\n" +
                "1\n" + generatedEPDafterHeader(3, "\n")));
    }

    @Test
    public void testSPC1() {
        try {
            QrInvoiceCodeParser.create().parse(SpcSamplesRegistry.getFileContent("/samples/spc/spc_IG1.0_page32.txt"));
            fail("excpected parsing exception");
        } catch (ParseException e) {
            assertTrue(e.getMessage().contains("Version '0200'"));
            assertTrue(e.getMessage().contains("but '0100' encountered"));
        }
    }

    @Test
    public void testEPDTrailerMissing() {
        try {
            final String spc = SpcSamplesRegistry.getFileContent("/samples/spc/spc_IG2.0_page39_orig_qrr_qriban_avs_billinformation.txt");
            final String spcWithoutEpd = spc.replace("EPD", "");
            QrInvoiceCodeParser.create().parse(spcWithoutEpd);
            fail("excpected parsing exception");
        } catch (ParseException e) {
            assertTrue(e.getMessage().contains("Trailer 'EPD' expected, but 'null' encountered"));
        }
    }

    private String generatedEPDafterHeader(final int givenRowsBefore, final String separator) {
        // just generated some blank lines and then append the EPD header
        final int trailerLine = 31;
        return IntStream.range(0, trailerLine - givenRowsBefore).mapToObj(i -> "").collect(Collectors.joining(separator)) + SwissPaymentsCode.END_PAYMENT_DATA_TRAILER;
    }

}
