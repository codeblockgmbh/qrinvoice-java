/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice;

import ch.codeblock.qrinvoice.image.ImageSupport;
import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.validation.QrInvoiceValidator;
import ch.codeblock.qrinvoice.qrcode.QrCodeReader;
import ch.codeblock.qrinvoice.tests.resources.QrCodeSample;
import ch.codeblock.qrinvoice.tests.resources.QrCodeSamplesRegistry;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class QrInvoiceCodeScannerParameterizedTest {


    @Parameterized.Parameters(name = "Testfile: {0}")
    public static Collection<QrCodeSample> data() {
        return QrCodeSamplesRegistry.data().stream().filter(sample -> sample.getExpectedSwissQrCodeCount() > 0).collect(Collectors.toList());
    }

    private final QrCodeSample sample;

    public QrInvoiceCodeScannerParameterizedTest(final QrCodeSample sample) {
        this.sample = sample;
    }

    @Test
    public void testParseStringString() throws IOException {
        final InputStream resourceAsStream = getClass().getResourceAsStream(sample.getFilePath());
        final List<String> spcsFromStream = QrCodeReader.create().readQRCodes(resourceAsStream);
        assertNotNull(spcsFromStream);
        spcsFromStream.forEach(System.out::println);

        final BufferedImage bufferedImage = new ImageSupport().read(getClass().getResourceAsStream(sample.getFilePath()));
        final List<String> spcsFromBufferedImage = QrCodeReader.create().readQRCodes(bufferedImage);
        assertEquals(spcsFromStream, spcsFromBufferedImage);

        final byte[] byteArray = IOUtils.toByteArray(getClass().getResourceAsStream(sample.getFilePath()));
        final List<String> spcFromByteArray = QrCodeReader.create().readQRCodes(byteArray);
        assertEquals(spcsFromStream, spcFromByteArray);

        assertEquals(sample.getExpectedSwissQrCodeCount(), spcsFromStream.stream().filter(s -> s.startsWith("SPC")).count());
    }

    @Test
    public void testParseQrInvoice() {
        final InputStream resourceAsStream = getClass().getResourceAsStream(sample.getFilePath());
        final List<QrInvoice> qrInvoices = QrInvoiceCodeScanner.create().scanMultiple(resourceAsStream);
        assertEquals(sample.getExpectedSwissQrCodeCount(), qrInvoices.size());
        for (final QrInvoice qrInvoice : qrInvoices) {
            QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(qrInvoice).throwExceptionOnErrors();
        }
    }

}
