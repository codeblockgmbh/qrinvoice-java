/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class CreditorReferenceUtilsTest {

    @Test
    public void testIsValid() {
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF45 1234 5123 45"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF451234512345"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF411274"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF941290"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF551410"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF68AB2G5"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF68ab2g5"));

        assertFalse("wrong prefix", CreditorReferenceUtils.isValidCreditorReference("AB451234512345"));
        assertFalse("wrong check digits", CreditorReferenceUtils.isValidCreditorReference("RF461234512345"));
        assertFalse("too short", CreditorReferenceUtils.isValidCreditorReference("RF4"));
        assertFalse("empty string", CreditorReferenceUtils.isValidCreditorReference(""));
        assertFalse("null", CreditorReferenceUtils.isValidCreditorReference(null));
        assertFalse("too long", CreditorReferenceUtils.isValidCreditorReference("RF451234351234523423423423"));
        assertFalse("wrong check digits", CreditorReferenceUtils.isValidCreditorReference("RF35C4"));
        assertFalse("wrong check digits", CreditorReferenceUtils.isValidCreditorReference("RF214377"));
        assertFalse("invalid character", CreditorReferenceUtils.isValidCreditorReference("ä"));

        // some random creditor references generated using an online service
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF96DPM2A4WAW0SLLQA6"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF06GZAXTCA"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF23GW3I60LF6RFYPF"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF14QYI17IRZPRZNMC6EJL5"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF65ANEFDWKCMPYFO3I2"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF380FHSFUG34KQUX4"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF4756COLMQZ17E8YMZP6FW"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF9048PR104Z"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF94T"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF815JZCU5S97KAJJMIAC6X"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF10YQ0Q91C91V"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF61PGQRC15X9WXEHLN"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF746C2A0WC"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF17R2UMJ8FAZV8CBFAUC"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF62BJ4OQYRWYTT0NMH4BQDG"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF54C14EFPF0R1DCNRT"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF71KR5F5NSAU10"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF05H3F45OOQ2T8"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF54XUB56UZZN5FBAYBUDCJ4"));
        assertTrue(CreditorReferenceUtils.isValidCreditorReference("RF396TWEJ64E3"));

    }

    @Test
    public void testNormalize() {
        assertEquals("RF451234512345", CreditorReferenceUtils.normalizeCreditorReference("RF45 1234 5123 45"));
        assertEquals("RF451234512345", CreditorReferenceUtils.normalizeCreditorReference("RF451234512345"));
        assertEquals("RF411274", CreditorReferenceUtils.normalizeCreditorReference("RF411274"));
        assertEquals("RF941290", CreditorReferenceUtils.normalizeCreditorReference("RF941290"));
        assertEquals("RF68AB2G5", CreditorReferenceUtils.normalizeCreditorReference("RF68 AB2G 5"));
        assertEquals("RF68AB2G5", CreditorReferenceUtils.normalizeCreditorReference("RF68 ab2g 5"));
        assertEquals("", CreditorReferenceUtils.normalizeCreditorReference(""));
        assertEquals("", CreditorReferenceUtils.normalizeCreditorReference(null));
    }

    @Test
    public void testFormat() {
        assertEquals("RF45 1234 5123 45", CreditorReferenceUtils.formatCreditorReference("RF45 1234 5123 45"));
        assertEquals("RF45 1234 5123 45", CreditorReferenceUtils.formatCreditorReference("RF451234512345"));
        assertEquals("RF41 1274", CreditorReferenceUtils.formatCreditorReference("RF411274"));
        assertEquals("RF94 1290", CreditorReferenceUtils.formatCreditorReference("RF941290"));
        assertEquals("RF68 AB2G 5", CreditorReferenceUtils.formatCreditorReference("RF68AB2G5"));
        assertEquals("RF68 AB2G 5", CreditorReferenceUtils.formatCreditorReference("RF68ab2g5"));
        assertEquals("", CreditorReferenceUtils.formatCreditorReference(""));
        assertEquals("", CreditorReferenceUtils.formatCreditorReference(null));
    }

    @Test
    public void testCreateCreditorReference() {
        assertEquals("RF741", CreditorReferenceUtils.createCreditorReference("1"));
        assertEquals("RF7401", CreditorReferenceUtils.createCreditorReference("01"));
        assertEquals("RF74001", CreditorReferenceUtils.createCreditorReference("001"));
        assertEquals("RF635", CreditorReferenceUtils.createCreditorReference("5"));
        assertEquals("RF6812", CreditorReferenceUtils.createCreditorReference("12"));
        assertEquals("RF78123", CreditorReferenceUtils.createCreditorReference("123"));
        assertEquals("RF96187", CreditorReferenceUtils.createCreditorReference("187"));
        assertEquals("RF462348", CreditorReferenceUtils.createCreditorReference("2348"));
        assertEquals("RF25A", CreditorReferenceUtils.createCreditorReference("a"));
        assertEquals("RF25A", CreditorReferenceUtils.createCreditorReference("A"));
        assertEquals("RF87B2D", CreditorReferenceUtils.createCreditorReference("B2D"));
        assertEquals("RF25243DF", CreditorReferenceUtils.createCreditorReference("243DF"));
        assertEquals("RF374238791", CreditorReferenceUtils.createCreditorReference("4238791"));
        assertEquals("RF02O9JLZKZBX0WKCRVID3K9P", CreditorReferenceUtils.createCreditorReference("O9JLZKZBX0WKCRVID3K9P"));

        assertEquals("RF374238791", CreditorReferenceUtils.createCreditorReference("RF374238791"));
        assertEquals("RF451234512345", CreditorReferenceUtils.createCreditorReference("RF45 1234 5123 45"));
        assertEquals(CreditorReferenceUtils.createCreditorReference("rf374238791"), CreditorReferenceUtils.createCreditorReference("RF374238791"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditorReferenceTooLong() {
        CreditorReferenceUtils.createCreditorReference("CT8VI6S9U34ZDIM2Q9L3ZA");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditorReferenceEmpty() {
        CreditorReferenceUtils.createCreditorReference("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCreditorReferenceNull() {
        CreditorReferenceUtils.createCreditorReference(null);
    }

    @Test
    public void testNormalizeFormatRoundtrip() {
        assertEquals("RF45 1234 5123 45", CreditorReferenceUtils.formatCreditorReference(CreditorReferenceUtils.normalizeCreditorReference("RF45 1234 5123 45")));
        assertEquals("RF451234512345", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF451234512345")));

        // some random creditor references generated using an online service
        assertEquals("RF96DPM2A4WAW0SLLQA6", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF96DPM2A4WAW0SLLQA6")));
        assertEquals("RF06GZAXTCA", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF06GZAXTCA")));
        assertEquals("RF23GW3I60LF6RFYPF", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF23GW3I60LF6RFYPF")));
        assertEquals("RF14QYI17IRZPRZNMC6EJL5", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF14QYI17IRZPRZNMC6EJL5")));
        assertEquals("RF65ANEFDWKCMPYFO3I2", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF65ANEFDWKCMPYFO3I2")));
        assertEquals("RF380FHSFUG34KQUX4", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF380FHSFUG34KQUX4")));
        assertEquals("RF4756COLMQZ17E8YMZP6FW", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF4756COLMQZ17E8YMZP6FW")));
        assertEquals("RF9048PR104Z", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF9048PR104Z")));
        assertEquals("RF94T", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF94T")));
        assertEquals("RF815JZCU5S97KAJJMIAC6X", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF815JZCU5S97KAJJMIAC6X")));
        assertEquals("RF10YQ0Q91C91V", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF10YQ0Q91C91V")));
        assertEquals("RF61PGQRC15X9WXEHLN", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF61PGQRC15X9WXEHLN")));
        assertEquals("RF746C2A0WC", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF746C2A0WC")));
        assertEquals("RF17R2UMJ8FAZV8CBFAUC", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF17R2UMJ8FAZV8CBFAUC")));
        assertEquals("RF62BJ4OQYRWYTT0NMH4BQDG", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF62BJ4OQYRWYTT0NMH4BQDG")));
        assertEquals("RF54C14EFPF0R1DCNRT", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF54C14EFPF0R1DCNRT")));
        assertEquals("RF71KR5F5NSAU10", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF71KR5F5NSAU10")));
        assertEquals("RF05H3F45OOQ2T8", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF05H3F45OOQ2T8")));
        assertEquals("RF54XUB56UZZN5FBAYBUDCJ4", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF54XUB56UZZN5FBAYBUDCJ4")));
        assertEquals("RF396TWEJ64E3", CreditorReferenceUtils.normalizeCreditorReference(CreditorReferenceUtils.formatCreditorReference("RF396TWEJ64E3")));
    }

    @Test
    public void testConvertToNumericRepresentation() {
        assertEquals("10", CreditorReferenceUtils.convertToNumericRepresentation("a"));
        assertEquals("10", CreditorReferenceUtils.convertToNumericRepresentation("A"));
        assertEquals("11", CreditorReferenceUtils.convertToNumericRepresentation("b"));
        assertEquals("11", CreditorReferenceUtils.convertToNumericRepresentation("B"));
        assertEquals("12", CreditorReferenceUtils.convertToNumericRepresentation("c"));
        assertEquals("12", CreditorReferenceUtils.convertToNumericRepresentation("C"));
        assertEquals("13", CreditorReferenceUtils.convertToNumericRepresentation("d"));
        assertEquals("13", CreditorReferenceUtils.convertToNumericRepresentation("D"));
        assertEquals("14", CreditorReferenceUtils.convertToNumericRepresentation("e"));
        assertEquals("14", CreditorReferenceUtils.convertToNumericRepresentation("E"));
        assertEquals("15", CreditorReferenceUtils.convertToNumericRepresentation("f"));
        assertEquals("15", CreditorReferenceUtils.convertToNumericRepresentation("F"));
        assertEquals("16", CreditorReferenceUtils.convertToNumericRepresentation("g"));
        assertEquals("16", CreditorReferenceUtils.convertToNumericRepresentation("G"));
        assertEquals("17", CreditorReferenceUtils.convertToNumericRepresentation("h"));
        assertEquals("17", CreditorReferenceUtils.convertToNumericRepresentation("H"));
        assertEquals("18", CreditorReferenceUtils.convertToNumericRepresentation("i"));
        assertEquals("18", CreditorReferenceUtils.convertToNumericRepresentation("I"));
        assertEquals("19", CreditorReferenceUtils.convertToNumericRepresentation("j"));
        assertEquals("19", CreditorReferenceUtils.convertToNumericRepresentation("J"));
        assertEquals("20", CreditorReferenceUtils.convertToNumericRepresentation("k"));
        assertEquals("20", CreditorReferenceUtils.convertToNumericRepresentation("K"));
        assertEquals("21", CreditorReferenceUtils.convertToNumericRepresentation("l"));
        assertEquals("21", CreditorReferenceUtils.convertToNumericRepresentation("L"));
        assertEquals("22", CreditorReferenceUtils.convertToNumericRepresentation("m"));
        assertEquals("22", CreditorReferenceUtils.convertToNumericRepresentation("M"));
        assertEquals("23", CreditorReferenceUtils.convertToNumericRepresentation("n"));
        assertEquals("23", CreditorReferenceUtils.convertToNumericRepresentation("N"));
        assertEquals("24", CreditorReferenceUtils.convertToNumericRepresentation("o"));
        assertEquals("24", CreditorReferenceUtils.convertToNumericRepresentation("O"));
        assertEquals("25", CreditorReferenceUtils.convertToNumericRepresentation("p"));
        assertEquals("25", CreditorReferenceUtils.convertToNumericRepresentation("P"));
        assertEquals("26", CreditorReferenceUtils.convertToNumericRepresentation("q"));
        assertEquals("26", CreditorReferenceUtils.convertToNumericRepresentation("Q"));
        assertEquals("27", CreditorReferenceUtils.convertToNumericRepresentation("r"));
        assertEquals("27", CreditorReferenceUtils.convertToNumericRepresentation("R"));
        assertEquals("28", CreditorReferenceUtils.convertToNumericRepresentation("s"));
        assertEquals("28", CreditorReferenceUtils.convertToNumericRepresentation("S"));
        assertEquals("29", CreditorReferenceUtils.convertToNumericRepresentation("t"));
        assertEquals("29", CreditorReferenceUtils.convertToNumericRepresentation("T"));
        assertEquals("30", CreditorReferenceUtils.convertToNumericRepresentation("u"));
        assertEquals("30", CreditorReferenceUtils.convertToNumericRepresentation("U"));
        assertEquals("31", CreditorReferenceUtils.convertToNumericRepresentation("v"));
        assertEquals("31", CreditorReferenceUtils.convertToNumericRepresentation("V"));
        assertEquals("32", CreditorReferenceUtils.convertToNumericRepresentation("w"));
        assertEquals("32", CreditorReferenceUtils.convertToNumericRepresentation("W"));
        assertEquals("33", CreditorReferenceUtils.convertToNumericRepresentation("x"));
        assertEquals("33", CreditorReferenceUtils.convertToNumericRepresentation("X"));
        assertEquals("34", CreditorReferenceUtils.convertToNumericRepresentation("y"));
        assertEquals("34", CreditorReferenceUtils.convertToNumericRepresentation("Y"));
        assertEquals("35", CreditorReferenceUtils.convertToNumericRepresentation("z"));
        assertEquals("35", CreditorReferenceUtils.convertToNumericRepresentation("Z"));
        assertEquals("0", CreditorReferenceUtils.convertToNumericRepresentation("0"));
        assertEquals("1", CreditorReferenceUtils.convertToNumericRepresentation("1"));
        assertEquals("2", CreditorReferenceUtils.convertToNumericRepresentation("2"));
        assertEquals("3", CreditorReferenceUtils.convertToNumericRepresentation("3"));
        assertEquals("4", CreditorReferenceUtils.convertToNumericRepresentation("4"));
        assertEquals("5", CreditorReferenceUtils.convertToNumericRepresentation("5"));
        assertEquals("6", CreditorReferenceUtils.convertToNumericRepresentation("6"));
        assertEquals("7", CreditorReferenceUtils.convertToNumericRepresentation("7"));
        assertEquals("8", CreditorReferenceUtils.convertToNumericRepresentation("8"));
        assertEquals("9", CreditorReferenceUtils.convertToNumericRepresentation("9"));
        assertEquals("2715", CreditorReferenceUtils.convertToNumericRepresentation("rf"));
        assertEquals("2715", CreditorReferenceUtils.convertToNumericRepresentation("RF"));
        assertEquals("24919213520351133032201227311813320925271502", CreditorReferenceUtils.convertToNumericRepresentation("O9JLZKZBX0WKCRVID3K9PRF02"));
    }
    
    @Test
    public void testMod97Remainder() {
        assertEquals(1, CreditorReferenceUtils.getMod97Remainder("1672303027271545"));
        assertEquals(1, CreditorReferenceUtils.getMod97Remainder("18205271565"));
        assertEquals(0, CreditorReferenceUtils.getMod97Remainder("18205271564"));
        assertEquals(82, CreditorReferenceUtils.getMod97Remainder("124271535"));
        assertEquals(51, CreditorReferenceUtils.getMod97Remainder("4377271521"));
    }

    @Test
    public void testCalculateCheckDigitsForReferenceNumber() {
        assertEquals("45", CreditorReferenceUtils.calculateCheckDigitsForReferenceNumber("1672303027"));
        assertEquals("65", CreditorReferenceUtils.calculateCheckDigitsForReferenceNumber("18205"));
        assertEquals("51", CreditorReferenceUtils.calculateCheckDigitsForReferenceNumber("124"));
        assertEquals("68", CreditorReferenceUtils.calculateCheckDigitsForReferenceNumber("4377"));
        assertEquals("02", CreditorReferenceUtils.calculateCheckDigitsForReferenceNumber("O9JLZKZBX0WKCRVID3K9P"));
    }
    @Test
    public void testCalculateCheckDigits() {
        assertEquals("45", CreditorReferenceUtils.calculateCheckDigits("1672303027271500"));
        assertEquals("65", CreditorReferenceUtils.calculateCheckDigits("18205271500"));
        assertEquals("51", CreditorReferenceUtils.calculateCheckDigits("124271500"));
        assertEquals("68", CreditorReferenceUtils.calculateCheckDigits("4377271500"));
        assertEquals("02", CreditorReferenceUtils.calculateCheckDigits("24919213520351133032201227311813320925271500"));
    }


    @Test
    public void testExtractReferenceNumberFromQrReference() {
        assertNull( CreditorReferenceUtils.extractReferenceNumberFromCreditorReference(null, true));
        
        assertEquals("1234512345", CreditorReferenceUtils.extractReferenceNumberFromCreditorReference("RF451234512345", true));
        assertEquals("1", CreditorReferenceUtils.extractReferenceNumberFromCreditorReference("RF74001", true));
        
        assertEquals("1234512345", CreditorReferenceUtils.extractReferenceNumberFromCreditorReference("RF451234512345", false));
        assertEquals("001", CreditorReferenceUtils.extractReferenceNumberFromCreditorReference("RF74001", false));
    }
}
