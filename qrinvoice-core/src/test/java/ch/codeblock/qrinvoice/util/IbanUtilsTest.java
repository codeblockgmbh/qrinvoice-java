/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import ch.codeblock.qrinvoice.tests.data.TestIbans;
import org.junit.Test;

import static org.junit.Assert.*;

public class IbanUtilsTest {

    @Test
    public void testNormalizeFormatIbanRoundtrip() {
        assertEquals("CH3908704016075473007", IbanUtils.normalizeIBAN(IbanUtils.formatIban("CH3908704016075473007")));
        assertEquals("CH39 0870 4016 0754 7300 7", IbanUtils.formatIban(IbanUtils.normalizeIBAN("CH39 0870 4016 0754 7300 7")));
    }

    @Test
    public void testFormatIban() {
        assertEquals("CH39 0870 4016 0754 7300 7", IbanUtils.formatIban("CH3908704016075473007"));
    }

    @Test
    public void testNormalizeIban() {
        assertEquals("CH3908704016075473007", IbanUtils.normalizeIBAN("CH39 0870 4016 0754 7300 7"));
        
        assertEquals("", IbanUtils.normalizeIBAN(""));
        assertEquals("", IbanUtils.normalizeIBAN(null));
    }

    @Test
    public void testFormatIbanEmptyString() {
        assertEquals("", IbanUtils.formatIban(""));
    }

    @Test
    public void testFormatIbanBlankString() {
        assertEquals("", IbanUtils.formatIban("    "));
    }

    @Test
    public void testFormatIbanNull() {
        assertEquals("", IbanUtils.formatIban(null));
    }

    @Test
    public void testValidChLiIban() {
        assertTrue(IbanUtils.isValidIBAN("CH3908704016075473007", true));
        assertTrue(IbanUtils.isValidIBAN("CH39 0870 4016 0754 7300 7", true));
        assertTrue(IbanUtils.isValidIBAN("LI21 0881 0000 2324 013A A", true));
    }

    @Test
    public void testValidForeignIbans() {
        assertTrue(IbanUtils.isValidIBAN("BE68844010370034", false));
        assertTrue(IbanUtils.isValidIBAN("MT98MMEB44093000000009027293051", false));
    }

    @Test
    public void testInvalidIban() {
        assertFalse(IbanUtils.isValidIBAN("CH39", false));
        assertFalse(IbanUtils.isValidIBAN("CH3908704016075473001", false));
        assertFalse(IbanUtils.isValidIBAN("AR3908704016075473007", false));
        assertFalse(IbanUtils.isValidIBAN("CH390870401607547300ä", false));
    }

    @Test
    public void testNull() {
        assertFalse(IbanUtils.isValidIBAN(null, false));
    }

    @Test
    public void testEmptyString() {
        assertFalse(IbanUtils.isValidIBAN("", false));
    }

    @Test
    public void testLengths() {
        assertFalse(IbanUtils.isValidIBAN("CH390870401607", false)); // 14 < 15
        assertFalse(IbanUtils.isValidIBAN("CH390870401607547300723432423234234", false)); // 35 > 34
    }

    @Test
    public void testValidGermanIBAN() {
        assertTrue(IbanUtils.isValidIBAN("DE89 3704 0044 0532 0130 00", false));
    }

    @Test
    public void testValidGermanIBANButInvalidDueToCountryCode() {
        assertFalse(IbanUtils.isValidIBAN("DE89 3704 0044 0532 0130 00", true));
    }

    @Test
    public void testIsQrIBAN() throws Exception {
        assertTrue(IbanUtils.isQrIBAN("CH44 3199 9123 0008 8901 2"));
        assertTrue(IbanUtils.isQrIBAN("CH4431999123000889012"));
        assertFalse(IbanUtils.isQrIBAN("CH31 8123 9000 0012 4568 9"));


        // valid IBAN, but not a valid QR-IBAN, as this only exists for switzerland / liechtenstein
        assertTrue(IbanUtils.isValidIBAN("DE46 31990044 0532 0130 2", false));
        assertFalse("Only CH and LI do have QR-IBANs", IbanUtils.isQrIBAN("DE46 3199 9044 0532 0130 2"));
    }

    @Test
    public void testIbanTestData() throws Exception {
        assertFalse(IbanUtils.isQrIBAN(TestIbans.IBAN_CH3709000000304442225));
        assertFalse(IbanUtils.isQrIBAN(TestIbans.IBAN_CH3908704016075473007));
        assertTrue(IbanUtils.isQrIBAN(TestIbans.QR_IBAN_CH4431999123000889012));

        assertTrue(IbanUtils.isValidIBAN(TestIbans.IBAN_CH3709000000304442225, true));
        assertTrue(IbanUtils.isValidIBAN(TestIbans.IBAN_CH3908704016075473007, true));
        assertTrue(IbanUtils.isValidIBAN(TestIbans.QR_IBAN_CH4431999123000889012, true));
    }

        
    @Test
    public void testSwissIban() throws Exception {
        assertTrue(IbanUtils.isValidIBAN(TestIbans.IBAN_CH3908704016075473007, true));
        assertFalse(IbanUtils.isValidIBAN(TestIbans.IBAN_CH3908704016075473007 + 1, true));
    }
}
