/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import org.junit.Test;

import java.nio.CharBuffer;

import static org.junit.Assert.*;

public class StringUtilsTest {

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(StringUtils.isEmpty(null));
        assertTrue(StringUtils.isEmpty(""));
        assertFalse(StringUtils.isEmpty(" "));
        assertFalse(StringUtils.isEmpty("a"));
    }

    @Test
    public void testIsNotEmpty() throws Exception {
        assertFalse(StringUtils.isNotEmpty(null));
        assertFalse(StringUtils.isNotEmpty(""));
        assertTrue(StringUtils.isNotEmpty(" "));
        assertTrue(StringUtils.isNotEmpty("a"));
    }

    @Test
    public void testIsBlank() throws Exception {
        assertTrue(StringUtils.isBlank(null));
        assertTrue(StringUtils.isBlank(""));
        assertTrue(StringUtils.isBlank(" "));
        assertFalse(StringUtils.isBlank("a"));
    }

    @Test
    public void testIsNotBlank() throws Exception {
        assertFalse(StringUtils.isNotBlank(null));
        assertFalse(StringUtils.isNotBlank(""));
        assertFalse(StringUtils.isNotBlank(" "));
        assertTrue(StringUtils.isNotBlank("a"));
    }

    @Test
    public void testEmptyStringAsNull() throws Exception {
        assertNull(StringUtils.emptyStringAsNull(null));
        assertNull(StringUtils.emptyStringAsNull(""));
        assertEquals(" ", StringUtils.emptyStringAsNull(" "));
        assertEquals("a", StringUtils.emptyStringAsNull("a"));
    }

    @Test
    public void testIsNumeric() throws Exception {
        assertTrue(StringUtils.isNumeric("-1456"));
        assertTrue(StringUtils.isNumeric("-1"));
        assertTrue(StringUtils.isNumeric("0"));
        assertTrue(StringUtils.isNumeric("1"));
        assertTrue(StringUtils.isNumeric("+1"));
        assertTrue(StringUtils.isNumeric("1234"));
        assertFalse(StringUtils.isNumeric("1234d"));
        assertFalse(StringUtils.isNumeric("a"));
        assertFalse(StringUtils.isNumeric("ASDF234"));
        assertFalse(StringUtils.isNumeric(null));
    }

    @Test
    public void testIsUnsignedIntegerNumber() throws Exception {
        assertFalse(StringUtils.isUnsignedIntegerNumber("-1456"));
        assertFalse(StringUtils.isUnsignedIntegerNumber("-1"));
        assertTrue(StringUtils.isUnsignedIntegerNumber("0"));
        assertTrue(StringUtils.isUnsignedIntegerNumber("1"));
        assertTrue(StringUtils.isUnsignedIntegerNumber("9999"));
        assertFalse(StringUtils.isUnsignedIntegerNumber("0.0"));
        assertFalse(StringUtils.isUnsignedIntegerNumber("0."));
        assertFalse(StringUtils.isUnsignedIntegerNumber("a"));
    }


    @Test
    public void testLengthString() {
        assertEquals(0, StringUtils.length((String) null));
        assertEquals(0, StringUtils.length(""));
        assertEquals(1, StringUtils.length("A"));
        assertEquals(1, StringUtils.length(" "));
        assertEquals(8, StringUtils.length("ABCDEFGH"));
    }

    @Test
    public void testLengthStringBuffer() {
        assertEquals(0, StringUtils.length(new StringBuffer("")));
        assertEquals(1, StringUtils.length(new StringBuffer("A")));
        assertEquals(1, StringUtils.length(new StringBuffer(" ")));
        assertEquals(8, StringUtils.length(new StringBuffer("ABCDEFGH")));
    }

    @Test
    public void testLengthStringBuilder() {
        assertEquals(0, StringUtils.length(new StringBuilder("")));
        assertEquals(1, StringUtils.length(new StringBuilder("A")));
        assertEquals(1, StringUtils.length(new StringBuilder(" ")));
        assertEquals(8, StringUtils.length(new StringBuilder("ABCDEFGH")));
    }

    @Test
    public void testLength_CharBuffer() {
        assertEquals(0, StringUtils.length(CharBuffer.wrap("")));
        assertEquals(1, StringUtils.length(CharBuffer.wrap("A")));
        assertEquals(1, StringUtils.length(CharBuffer.wrap(" ")));
        assertEquals(8, StringUtils.length(CharBuffer.wrap("ABCDEFGH")));
    }

    @Test
    public void testLengthStrings() {
        assertEquals(0, StringUtils.length(null, null));
        assertEquals(0, StringUtils.length("", ""));
        assertEquals(1, StringUtils.length("A", null));
        assertEquals(1, StringUtils.length("A", ""));
        assertEquals(1, StringUtils.length(null, "B"));
        assertEquals(2, StringUtils.length("A", "B"));
        assertEquals(8, StringUtils.length("AB", "C", "DEFGH"));
    }

    @Test
    public void testJoin() {
        assertEquals("", StringUtils.join("", null, null));
        assertEquals("", StringUtils.join("", "", null));
        assertEquals("", StringUtils.join("", "", ""));
        assertEquals("A", StringUtils.join("", "A", null));
        assertEquals("AB", StringUtils.join("", "A", "B"));
        assertEquals("A B", StringUtils.join(" ", "A", "B"));
        assertEquals("ABC DEF GH", StringUtils.join(" ", "ABC", "DEF", "GH"));
    }

    @Test
    public void testIsTrimmable() {
        assertFalse(StringUtils.isTrimmable(null));
        assertFalse(StringUtils.isTrimmable(""));
        assertTrue(StringUtils.isTrimmable(" "));
        assertTrue(StringUtils.isTrimmable(" a"));
        assertTrue(StringUtils.isTrimmable(" a "));
        assertTrue(StringUtils.isTrimmable("a "));
        assertFalse(StringUtils.isTrimmable("a"));
    }

    @Test
    public void testLeftPad() {
        assertEquals("00000000000000000000000123", StringUtils.leftPad("123", 26, '0'));
        assertEquals("0000000000000000000000000x", StringUtils.leftPad("x", 26, '0'));
        assertEquals("0000000000000000000000123x", StringUtils.leftPad("123x", 26, '0'));
        assertEquals("000000 ", StringUtils.leftPad(" ", 7, '0'));
        assertEquals("0000000", StringUtils.leftPad("", 7, '0'));
        assertEquals("001 1 1", StringUtils.leftPad("1 1 1", 7, '0'));
        assertEquals("123", StringUtils.leftPad("123", 0, '0'));
        assertEquals("123", StringUtils.leftPad("123", 1, '0'));
        assertEquals("0123", StringUtils.leftPad("123", 4, '0'));
        assertEquals("123", StringUtils.leftPad("123", -1, '0'));
        assertEquals("0000000", StringUtils.leftPad(null, 7, '0'));
        assertEquals("     42", StringUtils.leftPad("42", 7, ' '));
    }

    @Test(expected = NullPointerException.class)
    public void testStartsWithNull() {
        StringUtils.startsWith(null, null);
    }

    @Test
    public void testStartsWith() {
        assertFalse(StringUtils.startsWith(null, ""));
        assertFalse(StringUtils.startsWith(null, "whatever"));
        assertTrue(StringUtils.startsWith("SPC", "SPC"));
        assertTrue(StringUtils.startsWith("SPC\n0100", "SPC"));
        assertTrue(StringUtils.startsWith("SPC\n0100", ""));
        assertFalse(StringUtils.startsWith("SPC\n0100", "spc"));
    }

    @Test
    public void testRemoveWhitespaces() {
        assertEquals("asdf123abc", StringUtils.removeWhitespaces("asdf 123  abc"));
        assertEquals("abc", StringUtils.removeWhitespaces("   abc"));
        assertEquals("abc", StringUtils.removeWhitespaces("abc   "));
        assertEquals("abc", StringUtils.removeWhitespaces("   abc   "));

        assertEquals("", StringUtils.removeWhitespaces(""));
        assertEquals("", StringUtils.removeWhitespaces(" "));
        assertEquals("", StringUtils.removeWhitespaces("   "));
        assertEquals("", StringUtils.removeWhitespaces(null));

        assertEquals("abc123", StringUtils.removeWhitespaces("abc\t123"));
        assertEquals("abc123", StringUtils.removeWhitespaces("abc\n123"));
        assertEquals("abc123", StringUtils.removeWhitespaces("abc\t123\n"));
    }

    @Test
    public void testContainsWhitespace() {
        assertFalse(StringUtils.containsWhitespace(null));
        assertFalse(StringUtils.containsWhitespace(""));
        assertFalse(StringUtils.containsWhitespace("a"));
        assertFalse(StringUtils.containsWhitespace("abcdefg"));
        assertTrue(StringUtils.containsWhitespace("abcd efg"));
        assertTrue(StringUtils.containsWhitespace(" "));
        assertTrue(StringUtils.containsWhitespace("   "));
        assertTrue(StringUtils.containsWhitespace("\n"));
        assertTrue(StringUtils.containsWhitespace("\t"));
    }

    @Test
    public void escapeControlCharacters() {
        assertNull(StringUtils.escapeControlCharacters(null));
        assertEquals("", StringUtils.escapeControlCharacters(""));
        assertEquals("abc", StringUtils.escapeControlCharacters("abc"));
        assertEquals("abc\\ndef", StringUtils.escapeControlCharacters("abc\ndef"));
        assertEquals("abc\\ndef\\tghi", StringUtils.escapeControlCharacters("abc\ndef\tghi"));
        assertEquals("abc\\ndef\\nghi", StringUtils.escapeControlCharacters("abc\ndef\nghi"));
        assertEquals("abc\\n\\rdef\\n\\rghi", StringUtils.escapeControlCharacters("abc\n\rdef\n\rghi"));
    }

    @Test
    public void trimToEmpty() {
        assertEquals("", StringUtils.trimToEmpty(null));
        assertEquals("", StringUtils.trimToEmpty(""));
        assertEquals("", StringUtils.trimToEmpty(" "));
        assertEquals("", StringUtils.trimToEmpty("          "));
        assertEquals("a", StringUtils.trimToEmpty("    a      "));
        assertEquals("b", StringUtils.trimToEmpty("b"));
    }


    @Test
    public void trimToNull() {
        assertNull(StringUtils.trimToNull(null));
        assertNull(StringUtils.trimToNull(""));
        assertNull(StringUtils.trimToNull(" "));
        assertNull(StringUtils.trimToNull("          "));
        assertEquals("a", StringUtils.trimToNull("    a      "));
        assertEquals("b", StringUtils.trimToNull("b"));
    }


    private static final String FOO = "foo";
    private static final String BAZ = "baz";
    private static final String SENTENCE = "foo bar baz";

    @Test
    public void testSubstring2() {
        assertNull(StringUtils.substring(null, 0));
        assertEquals("", StringUtils.substring("", 0));
        assertEquals("", StringUtils.substring("", 2));

        assertEquals("", StringUtils.substring(SENTENCE, 80));
        assertEquals(BAZ, StringUtils.substring(SENTENCE, 8));
        assertEquals(BAZ, StringUtils.substring(SENTENCE, -3));
        assertEquals(SENTENCE, StringUtils.substring(SENTENCE, 0));
        assertEquals("abc", StringUtils.substring("abc", -4));
        assertEquals("abc", StringUtils.substring("abc", -3));
        assertEquals("bc", StringUtils.substring("abc", -2));
        assertEquals("c", StringUtils.substring("abc", -1));
        assertEquals("abc", StringUtils.substring("abc", 0));
        assertEquals("bc", StringUtils.substring("abc", 1));
        assertEquals("c", StringUtils.substring("abc", 2));
        assertEquals("", StringUtils.substring("abc", 3));
        assertEquals("", StringUtils.substring("abc", 4));
    }

    @Test
    public void testSubstring3() {
        assertNull(StringUtils.substring(null, 0, 0));
        assertNull(StringUtils.substring(null, 1, 2));
        assertEquals("", StringUtils.substring("", 0, 0));
        assertEquals("", StringUtils.substring("", 1, 2));
        assertEquals("", StringUtils.substring("", -2, -1));

        assertEquals("", StringUtils.substring(SENTENCE, 8, 6));
        assertEquals(FOO, StringUtils.substring(SENTENCE, 0, 3));
        assertEquals("o", StringUtils.substring(SENTENCE, -9, 3));
        assertEquals(FOO, StringUtils.substring(SENTENCE, 0, -8));
        assertEquals("o", StringUtils.substring(SENTENCE, -9, -8));
        assertEquals(SENTENCE, StringUtils.substring(SENTENCE, 0, 80));
        assertEquals("", StringUtils.substring(SENTENCE, 2, 2));
        assertEquals("b", StringUtils.substring("abc", -2, -1));
    }

    @Test
    public void testStripStart() {
        assertNull(StringUtils.stripStart(null, null));
        assertNull(StringUtils.stripStart(null, ""));
        assertEquals(StringUtils.stripStart("", null), "");
        assertEquals(StringUtils.stripStart("", ""), "");
        assertEquals(StringUtils.stripStart("abc", ""), "abc");
        assertEquals(StringUtils.stripStart("abc", null), "abc");
        assertEquals(StringUtils.stripStart("  abc", null), "abc");
        assertEquals(StringUtils.stripStart("abc  ", null), "abc  ");
        assertEquals(StringUtils.stripStart(" abc ", null), "abc ");
        assertEquals(StringUtils.stripStart("yxabc  ", "xyz"), "abc  ");
        assertEquals(StringUtils.stripStart("000042", "0"), "42");
        assertEquals(StringUtils.stripStart("0000420", "0"), "420");
    }
}
