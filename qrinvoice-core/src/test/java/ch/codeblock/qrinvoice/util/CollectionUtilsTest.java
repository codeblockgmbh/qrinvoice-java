/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class CollectionUtilsTest {
    @Test
    public void testIsEmptyWithEmptyCollection() {
        final Collection<Object> coll = new ArrayList<>();
        assertTrue(CollectionUtils.isEmpty(coll));
    }

    @Test
    public void testIsEmptyWithNonEmptyCollection() {
        final Collection<String> coll = new ArrayList<>();
        coll.add("item");
        assertFalse(CollectionUtils.isEmpty(coll));
    }

    @Test
    public void testIsEmptyWithNull() {
        final Collection<?> coll = null;
        assertTrue(CollectionUtils.isEmpty(coll));
    }

    @Test
    public void testIsNotEmptyWithEmptyCollection() {
        final Collection<Object> coll = new ArrayList<>();
        assertFalse(CollectionUtils.isNotEmpty(coll));
    }

    @Test
    public void testIsNotEmptyWithNonEmptyCollection() {
        final Collection<String> coll = new ArrayList<>();
        coll.add("item");
        assertTrue(CollectionUtils.isNotEmpty(coll));
    }

    @Test
    public void testIsNotEmptyWithNull() {
        final Collection<?> coll = null;
        assertFalse(CollectionUtils.isNotEmpty(coll));
    }

    @Test
    public void testSize_List() {
        List<String> list = null;
        assertEquals(0, CollectionUtils.size(list));
        list = new ArrayList<>();
        assertEquals(0, CollectionUtils.size(list));
        list.add("a");
        assertEquals(1, CollectionUtils.size(list));
        list.add("b");
        assertEquals(2, CollectionUtils.size(list));
    }

    @Test
    public void testSize_Map() {
        Map<String, String> map = null;
        assertEquals(0, CollectionUtils.size(map));
        map = new HashMap<>();
        assertEquals(0, CollectionUtils.size(map));
        map.put("1", "a");
        assertEquals(1, CollectionUtils.size(map));
        map.put("2", "b");
        assertEquals(2, CollectionUtils.size(map));
    }

    @Test
    public void testIsEmptyMap() {
        Map<String, String> map = null;
        assertTrue(CollectionUtils.isEmpty(map));
        assertFalse(CollectionUtils.isNotEmpty(map));

        map = new HashMap<>();
        assertTrue(CollectionUtils.isEmpty(map));
        assertFalse(CollectionUtils.isNotEmpty(map));
        
        map.put("1", "a");
        assertFalse(CollectionUtils.isEmpty(map));
        assertTrue(CollectionUtils.isNotEmpty(map));
    }

}
