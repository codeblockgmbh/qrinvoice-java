/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class QRReferenceUtilsTest {

    @Test
    public void testIsValid() {
        assertTrue(QRReferenceUtils.isValidQrReference("00 00000 00000 00000 00000 05989"));
        assertTrue(QRReferenceUtils.isValidQrReference("11 27098 76540 00000 55555 22228"));
        assertTrue(QRReferenceUtils.isValidQrReference("11 00012 34560 00000 00008 13457"));
        assertTrue(QRReferenceUtils.isValidQrReference("110001234560000000000813457"));

        assertFalse("wrong checksum digit", QRReferenceUtils.isValidQrReference("11 00012 34560 00000 00008 13456"));
        assertFalse("wrong checksum digit", QRReferenceUtils.isValidQrReference("11 27098 76540 00000 55555 22229"));
        assertFalse("too short - 27 digits expected", QRReferenceUtils.isValidQrReference("1"));
        assertFalse("too long - 27 digits expected", QRReferenceUtils.isValidQrReference("1234567890123456789012345678"));
    }

    @Test
    public void testNormalize() {
        assertEquals("000000000000000000000005989", QRReferenceUtils.normalizeQrReference("00 00000 00000 00000 00000 05989"));
        assertEquals("110001234560000000000813457", QRReferenceUtils.normalizeQrReference("110001234560000000000813457"));
        assertEquals("", QRReferenceUtils.normalizeQrReference(""));
        assertEquals("", QRReferenceUtils.normalizeQrReference(null));
    }

    @Test
    public void testFormat() {
        assertEquals("00 00000 00000 00000 00000 05989", QRReferenceUtils.formatQrReference("000000000000000000000005989"));
        assertEquals("11 00012 34560 00000 00008 13457", QRReferenceUtils.formatQrReference("110001234560000000000813457"));
        assertEquals("", QRReferenceUtils.formatQrReference(""));
        assertEquals("", QRReferenceUtils.formatQrReference(null));
    }

    @Test
    public void testNormalizeFormatRoundtrip() {
        assertEquals("00 00000 00000 00000 00000 05989", QRReferenceUtils.formatQrReference(QRReferenceUtils.normalizeQrReference("00 00000 00000 00000 00000 05989")));
        assertEquals("110001234560000000000813457", QRReferenceUtils.normalizeQrReference(QRReferenceUtils.formatQrReference("110001234560000000000813457")));
    }

    @Test
    public void testModulo10Recursive() {
        assertEquals(0, QRReferenceUtils.modulo10Recursive("0"));
        assertEquals(9, QRReferenceUtils.modulo10Recursive("00000000000000000000000598"));
        assertEquals(7, QRReferenceUtils.modulo10Recursive("11000123456000000000081345"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalCharacter() {
        QRReferenceUtils.modulo10Recursive("a");
    }

    @Test
    public void testCreateQrReference() {
        assertEquals("000000000000000000000005989", QRReferenceUtils.createQrReference("00 00000 00000 00000 00000 05989"));
        assertEquals("000000000000000000000005989", QRReferenceUtils.createQrReference("000000000000000000000005989"));

        assertEquals("000000000000000000000000011", QRReferenceUtils.createQrReference("1"));
        assertEquals("000000000000000000000000420", QRReferenceUtils.createQrReference("42"));
        assertEquals("000000000000000000000001273", QRReferenceUtils.createQrReference("127"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateQrReferenceEmpty() {
        QRReferenceUtils.createQrReference("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateQrReferenceNull() {
        QRReferenceUtils.createQrReference(null);
    }

    @Test
    public void testCreateQrReferenceWithCustomerId() {
        assertEquals("123000000000000000000000017", QRReferenceUtils.createQrReference("123", "1"));
        assertEquals("123456789012300000000000423", QRReferenceUtils.createQrReference("123456 7890123", "42"));
        assertEquals("123000000000000000000001279", QRReferenceUtils.createQrReference("123", "127"));
        assertEquals("123456123456789012345678904", QRReferenceUtils.createQrReference("123 456", "1234567890 1234567890"));
    }

    @Test
    public void testCreateQrReferenceWithEmptyCustomerId() {
        assertEquals("000000000000000000000005989", QRReferenceUtils.createQrReference("", "00 00000 00000 00000 00000 05989"));
        assertEquals("000000000000000000000005989", QRReferenceUtils.createQrReference("", "000000000000000000000005989"));

        assertEquals("000000000000000000000000011", QRReferenceUtils.createQrReference("", "1"));
        assertEquals("000000000000000000000000420", QRReferenceUtils.createQrReference("", "42"));
        assertEquals("000000000000000000000001273", QRReferenceUtils.createQrReference("", "127"));

        assertEquals("000000000000000000000005989", QRReferenceUtils.createQrReference(null, "00 00000 00000 00000 00000 05989"));
        assertEquals("000000000000000000000005989", QRReferenceUtils.createQrReference(null, "000000000000000000000005989"));

        assertEquals("000000000000000000000000011", QRReferenceUtils.createQrReference(null, "1"));
        assertEquals("000000000000000000000000420", QRReferenceUtils.createQrReference(null, "42"));
        assertEquals("000000000000000000000001273", QRReferenceUtils.createQrReference(null, "127"));
    }

 @Test
    public void testExtractReferenceNumberFromQrReference() {
        assertNull(QRReferenceUtils.extractReferenceNumberFromQrReference(null, true));
        
        assertEquals("598", QRReferenceUtils.extractReferenceNumberFromQrReference("000000000000000000000005989", true));
        assertEquals("1", QRReferenceUtils.extractReferenceNumberFromQrReference("000000000000000000000000011", true));
        assertEquals("42", QRReferenceUtils.extractReferenceNumberFromQrReference("000000000000000000000000420", true));
        assertEquals("598", QRReferenceUtils.extractReferenceNumberFromQrReference("00 00000 00000 00000 00000 05989", true));
        assertEquals("127", QRReferenceUtils.extractReferenceNumberFromQrReference("000000000000000000000001273", true));

        assertEquals("00000000000000000000000598", QRReferenceUtils.extractReferenceNumberFromQrReference("000000000000000000000005989", false));
        assertEquals("00000000000000000000000001", QRReferenceUtils.extractReferenceNumberFromQrReference("000000000000000000000000011", false));
        assertEquals("00000000000000000000000042", QRReferenceUtils.extractReferenceNumberFromQrReference("000000000000000000000000420", false));
        assertEquals("00000000000000000000000598", QRReferenceUtils.extractReferenceNumberFromQrReference("00 00000 00000 00000 00000 05989", false));
        assertEquals("00000000000000000000000127", QRReferenceUtils.extractReferenceNumberFromQrReference("000000000000000000000001273", false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateQrReferenceWithCustomerIdTooLong() {
        QRReferenceUtils.createQrReference("123", "000000000000000000000005989");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateQrReferenceWithCustomerIdTooLong2() {
        QRReferenceUtils.createQrReference("123456789012345678901234567890", "1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateQrReferenceWithCustomerIdInputEmpty() {
        QRReferenceUtils.createQrReference("123", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateQrReferenceWithCustomerIdInputNull() {
        QRReferenceUtils.createQrReference("123", null);
    }
}
