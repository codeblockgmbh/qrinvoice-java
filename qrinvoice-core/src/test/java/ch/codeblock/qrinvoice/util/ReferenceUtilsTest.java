/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import ch.codeblock.qrinvoice.model.ReferenceType;
import ch.codeblock.qrinvoice.tests.data.TestReferenceNumbers;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ReferenceUtilsTest {

    @Test
    public void format() {
        assertEquals("11 00012 34560 00000 00008 13457", ReferenceUtils.format(ReferenceType.QR_REFERENCE, TestReferenceNumbers.QRR_110001234560000000000813457));
        assertEquals("RF18 5390 0754 7034", ReferenceUtils.format(ReferenceType.CREDITOR_REFERENCE,TestReferenceNumbers.SCOR_RF18539007547034));
        assertEquals("", ReferenceUtils.format(ReferenceType.WITHOUT_REFERENCE,""));
        assertEquals("", ReferenceUtils.format(ReferenceType.WITHOUT_REFERENCE,null));
    }

    @Test
    public void normalize() {
        assertEquals("110001234560000000000813457", ReferenceUtils.normalize(ReferenceType.QR_REFERENCE, "11 00012 34560 00000 00008 13457"));
        assertEquals("RF18539007547034", ReferenceUtils.normalize(ReferenceType.CREDITOR_REFERENCE, "RF18 5390 0754 7034"));
        assertEquals("", ReferenceUtils.normalize(ReferenceType.WITHOUT_REFERENCE,""));
        assertEquals("abc", ReferenceUtils.normalize(ReferenceType.WITHOUT_REFERENCE,"abc"));
        assertNull( ReferenceUtils.normalize(ReferenceType.WITHOUT_REFERENCE,null));
    }
}
