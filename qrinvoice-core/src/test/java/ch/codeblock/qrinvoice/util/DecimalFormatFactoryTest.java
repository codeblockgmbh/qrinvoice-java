/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import org.junit.Test;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DecimalFormatFactoryTest {

    @Test
    public void testCreateQrCodeAmountFormat() throws ParseException {
        Map<String, BigDecimal> testValues = new HashMap<>();
        testValues.put("200.00", new BigDecimal("200.00"));
        testValues.put("200.00", new BigDecimal("200"));
        testValues.put("200.00", new BigDecimal("200.0"));
        testValues.put("200.05", new BigDecimal("200.05"));
        testValues.put("1200.00", new BigDecimal("1200"));
        testValues.put("1200.05", new BigDecimal("1200.05"));

        DecimalFormat format = DecimalFormatFactory.createSwissPaymentsCodeAmountFormat();
        format.setParseBigDecimal(true);

        testValues.entrySet().stream().forEach(testValue ->
                {
                    assertTrue(testValue.getValue().compareTo((BigDecimal) format.parseObject(testValue.getKey(), new ParsePosition(0))) == 0);
                    assertEquals(testValue.getKey(), format.format(testValue.getValue()));
                }
        );
    }

    @Test
    public void testCreatePrintAmountFormat() {
        Map<String, BigDecimal> testValues = new HashMap<>();
        testValues.put("200.00", new BigDecimal("200.00"));
        testValues.put("200.00", new BigDecimal("200"));
        testValues.put("200.00", new BigDecimal("200.0"));
        testValues.put("200.05", new BigDecimal("200.05"));
        testValues.put("1 200.00", new BigDecimal("1200"));
        testValues.put("1 200.05", new BigDecimal("1200.05"));

        DecimalFormat format = DecimalFormatFactory.createPrintAmountFormat();

        testValues.entrySet().stream().forEach(testValue ->
                {
                    assertEquals(testValue.getKey(), format.format(testValue.getValue()));
                }
        );
    }


}
