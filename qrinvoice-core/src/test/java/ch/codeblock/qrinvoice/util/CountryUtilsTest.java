/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

public class CountryUtilsTest {

    @Test
    public void testIsValidIsoCode() throws Exception {
        assertTrue(CountryUtils.isValidIsoCode("CH"));
        assertTrue(CountryUtils.isValidIsoCode("LI"));
        assertTrue(CountryUtils.isValidIsoCode("DE"));


        assertFalse("Codes must be in upper case", CountryUtils.isValidIsoCode("ch"));
    }

    @Test
    public void testLanguage() throws Exception {
        assertEquals("Switzerland", CountryUtils.getCountries(Locale.ENGLISH).getString("CH"));
        assertEquals("Schweiz", CountryUtils.getCountries(Locale.GERMAN).getString("CH"));
        assertEquals("Suisse", CountryUtils.getCountries(Locale.FRENCH).getString("CH"));
        assertEquals("Svizzera", CountryUtils.getCountries(Locale.ITALIAN).getString("CH"));
        
        assertEquals("Côte d'Ivoire", CountryUtils.getCountries(Locale.FRENCH).getString("CI"));
    }
}
