/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.util;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class NumberUtilsTest {
    @Test
    public void subtract() {
        assertEquals(Short.valueOf((short) -1), NumberUtils.subtract((short) 6, (short) 7));
        assertEquals(Integer.valueOf(4), NumberUtils.subtract(12, 8));
        assertEquals(Long.valueOf(5), NumberUtils.subtract(7L, 2L));
        assertEquals(Float.valueOf(4.13f), NumberUtils.subtract(12.23f, 8.1f));
        assertEquals(Double.valueOf(3.812), NumberUtils.subtract(12.012, 8.2));
        assertEquals(BigDecimal.valueOf(1295.2863), NumberUtils.subtract(BigDecimal.valueOf(1337.42), BigDecimal.valueOf(42.1337)));
    }

    @Test
    public void add() {
        assertEquals(Short.valueOf((short) 13), NumberUtils.add((short) 6, (short) 7));
        assertEquals(Integer.valueOf(20), NumberUtils.add(12, 8));
        assertEquals(Long.valueOf(9), NumberUtils.add(7L, 2L));
        assertEquals(Float.valueOf(20.33f), NumberUtils.add(12.23f, 8.1f));
        assertEquals(Double.valueOf(20.212), NumberUtils.add(12.012, 8.2));
        assertEquals(BigDecimal.valueOf(1379.5537), NumberUtils.add(BigDecimal.valueOf(1337.42), BigDecimal.valueOf(42.1337)));
    }

    @Test
    public void isZero() {
        assertTrue(NumberUtils.isZero((short) 0));
        assertFalse(NumberUtils.isZero((short) 1));
        assertFalse(NumberUtils.isZero((short) -1));

        assertTrue(NumberUtils.isZero(0));
        assertFalse(NumberUtils.isZero(1));
        assertFalse(NumberUtils.isZero(-1));

        assertTrue(NumberUtils.isZero(0L));
        assertFalse(NumberUtils.isZero(1L));
        assertFalse(NumberUtils.isZero(-1L));

        assertTrue(NumberUtils.isZero(0f));
        assertFalse(NumberUtils.isZero(1.0));
        assertFalse(NumberUtils.isZero(-1.0f));

        assertTrue(NumberUtils.isZero(0.0));
        assertFalse(NumberUtils.isZero(1.0));
        assertFalse(NumberUtils.isZero(-1.0));

        assertTrue(NumberUtils.isZero(BigDecimal.ZERO));
        assertFalse(NumberUtils.isZero(BigDecimal.valueOf(1)));
        assertFalse(NumberUtils.isZero(BigDecimal.valueOf(-1)));

        BigDecimal zero = new BigDecimal("0");
        assertTrue(NumberUtils.isZero(zero));

        BigDecimal zeroOne = new BigDecimal("0.0");
        assertTrue(NumberUtils.isZero(zeroOne));

        BigDecimal zeroTwo = new BigDecimal("0.00");
        assertTrue(NumberUtils.isZero(zeroTwo));

        BigDecimal zeroThree = new BigDecimal("0.000");
        assertTrue(NumberUtils.isZero(zeroThree));
    }

    @Test
    public void isNegative() {
        assertFalse(NumberUtils.isNegative((short) 0));
        assertFalse(NumberUtils.isNegative((short) 1));
        assertTrue(NumberUtils.isNegative((short) -1));

        assertFalse(NumberUtils.isNegative(0));
        assertFalse(NumberUtils.isNegative(1));
        assertTrue(NumberUtils.isNegative(-1));

        assertFalse(NumberUtils.isNegative(0L));
        assertFalse(NumberUtils.isNegative(1L));
        assertTrue(NumberUtils.isNegative(-1L));

        assertFalse(NumberUtils.isNegative(0f));
        assertFalse(NumberUtils.isNegative(1.0));
        assertTrue(NumberUtils.isNegative(-0.001f));
        assertTrue(NumberUtils.isNegative(-1.0f));

        assertFalse(NumberUtils.isNegative(0.0));
        assertFalse(NumberUtils.isNegative(1.0));
        assertTrue(NumberUtils.isNegative(-0.01));
        assertTrue(NumberUtils.isNegative(-1.0));

        assertFalse(NumberUtils.isNegative(BigDecimal.ZERO));
        assertFalse(NumberUtils.isNegative(BigDecimal.valueOf(1)));
        assertTrue(NumberUtils.isNegative(BigDecimal.valueOf(-1)));

        BigDecimal zero = new BigDecimal("0");
        assertFalse(NumberUtils.isNegative(zero));

        BigDecimal zeroOne = new BigDecimal("0.0");
        assertFalse(NumberUtils.isNegative(zeroOne));

        BigDecimal zeroTwo = new BigDecimal("0.00");
        assertFalse(NumberUtils.isNegative(zeroTwo));

        BigDecimal zeroThree = new BigDecimal("0.000");
        assertFalse(NumberUtils.isNegative(zeroThree));
    }

    @Test
    public void isPositive() {
        assertTrue(NumberUtils.isPositive((short) 0));
        assertTrue(NumberUtils.isPositive((short) 1));
        assertFalse(NumberUtils.isPositive((short) -1));

        assertTrue(NumberUtils.isPositive(0));
        assertTrue(NumberUtils.isPositive(1));
        assertFalse(NumberUtils.isPositive(-1));

        assertTrue(NumberUtils.isPositive(0L));
        assertTrue(NumberUtils.isPositive(1L));
        assertFalse(NumberUtils.isPositive(-1L));

        assertTrue(NumberUtils.isPositive(0f));
        assertTrue(NumberUtils.isPositive(1.0));
        assertFalse(NumberUtils.isPositive(-1.0f));

        assertTrue(NumberUtils.isPositive(0.0));
        assertTrue(NumberUtils.isPositive(1.0));
        assertFalse(NumberUtils.isPositive(-1.0));

        assertTrue(NumberUtils.isPositive(BigDecimal.ZERO));
        assertTrue(NumberUtils.isPositive(BigDecimal.valueOf(1)));
        assertFalse(NumberUtils.isPositive(BigDecimal.valueOf(-1)));

        BigDecimal zero = new BigDecimal("0");
        assertTrue(NumberUtils.isPositive(zero));

        BigDecimal zeroOne = new BigDecimal("0.0");
        assertTrue(NumberUtils.isPositive(zeroOne));

        BigDecimal zeroTwo = new BigDecimal("0.00");
        assertTrue(NumberUtils.isPositive(zeroTwo));

        BigDecimal zeroThree = new BigDecimal("0.000");
        assertTrue(NumberUtils.isPositive(zeroThree));
    }
}
