/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.fonts;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.TechnicalException;
import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.util.OperatingSystemUtils;
import org.junit.Assume;
import org.junit.Test;

import java.awt.*;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class FontManagerTest {

    @Test
    public void testGetPathOnMacOS() {
        Assume.assumeTrue("Test only runs on Mac OS", System.getProperty("os.name").toLowerCase().startsWith("mac"));
        String arialPath = FontManager.getFontPath(FontFamily.ARIAL, FontStyle.REGULAR);
        String arialBoldPath = FontManager.getFontPath(FontFamily.ARIAL, FontStyle.BOLD);
        assertTrue(arialPath.startsWith("/Library/Fonts/") || arialPath.startsWith("/System/Library/Fonts/"));
        assertTrue(arialPath.endsWith("Arial.ttf"));
        assertTrue(arialBoldPath.startsWith("/Library/Fonts/") || arialBoldPath.startsWith("/System/Library/Fonts/"));
        assertTrue(arialBoldPath.endsWith("Arial Bold.ttf"));
    }

    @Test
    public void testLoadSystemFont() {
        Assume.assumeTrue("Test only runs on Mac OS", System.getProperty("os.name").toLowerCase().startsWith("mac"));
        System.out.println(System.getProperty("os.name"));
        System.out.println(OperatingSystemUtils.detectOperatingSystem());
        assertEquals("Arial", FontManager.getFont(FontFamily.ARIAL, FontStyle.REGULAR).getName());
        assertEquals("Arial", FontManager.getFont(FontFamily.ARIAL, FontStyle.BOLD).getName());
        assertEquals(Font.BOLD, FontManager.getFont(FontFamily.ARIAL, FontStyle.BOLD).getStyle());

        // helvetica not installed on CI
        // assertEquals("Helvetica", FontManager.getFont(FontFamily.HELVETICA, FontStyle.REGULAR).getName());
        // assertEquals("Helvetica", FontManager.getFont(FontFamily.HELVETICA, FontStyle.BOLD).getName());
        // assertEquals(Font.BOLD, FontManager.getFont(FontFamily.HELVETICA, FontStyle.BOLD).getStyle());
    }

    @Test(expected = TechnicalException.class)
    public void testLoadFallbackFontNotAvailable() {
        try {
            System.setProperty(SystemProperties.IGNORE_SYSTEM_FONTS, "true");
            FontManager.reset();
            assertEquals("Arial", FontManager.getFont(FontFamily.ARIAL, FontStyle.REGULAR).getName());
        } finally {
            System.setProperty(SystemProperties.IGNORE_SYSTEM_FONTS, "");
            FontManager.reset();
        }
    }

    @Test
    public void testEmbeddedFont() {
        assertEquals("Liberation Sans", FontManager.getFont(FontFamily.LIBERATION_SANS, FontStyle.REGULAR).getName());
        assertEquals("Liberation Sans", FontManager.getFont(FontFamily.LIBERATION_SANS, FontStyle.BOLD).getName());
        assertEquals(Font.BOLD, FontManager.getFont(FontFamily.LIBERATION_SANS, FontStyle.BOLD).getStyle());
    }

    @Test
    public void testGetLiberationSans() {
        assertTrue(FontManager.isEmbedded(FontFamily.LIBERATION_SANS));
        assertNotNull(FontManager.getEmbeddedTtf(FontFamily.LIBERATION_SANS, FontStyle.REGULAR));
        assertEquals("LiberationSans-Regular.ttf", FontManager.getEmbeddedTtfFileName(FontFamily.LIBERATION_SANS, FontStyle.REGULAR));
    }

    @Test
    public void testGetFrutiger() throws URISyntaxException {
        Assume.assumeTrue("Test only runs on Mac OS with installed Frutiger", System.getProperty("os.name").toLowerCase().startsWith("mac"));
        assertTrue(FontManager.getFont(FontFamily.FRUTIGER, FontStyle.REGULAR).getName().startsWith("Frutiger"));
        assertTrue(FontManager.getFont(FontFamily.FRUTIGER, FontStyle.BOLD).getName().startsWith("Frutiger"));
        assertEquals(Font.BOLD, FontManager.getFont(FontFamily.FRUTIGER, FontStyle.BOLD).getStyle());
    }

    @Test
    public void testSetFontPathManually() throws URISyntaxException {
        String fakePathRegular = Paths.get(FontManagerTest.class.getResource("/ch/codeblock/qrinvoice/fonts/liberation_sans/LiberationSans-Regular.ttf").toURI()).toFile().getAbsolutePath();
        String fakePathBold = Paths.get(FontManagerTest.class.getResource("/ch/codeblock/qrinvoice/fonts/liberation_sans/LiberationSans-Bold.ttf").toURI()).toFile().getAbsolutePath();

        try {
            FontManager.setFontPath(FontFamily.FRUTIGER, fakePathRegular, fakePathBold);

            assertFalse(FontManager.isEmbedded(FontFamily.FRUTIGER));
            assertEquals(fakePathRegular, FontManager.getFontPath(FontFamily.FRUTIGER, FontStyle.REGULAR));
            assertEquals(fakePathBold, FontManager.getFontPath(FontFamily.FRUTIGER, FontStyle.BOLD));

        } finally {
            FontManager.reset();
        }
    }
}
