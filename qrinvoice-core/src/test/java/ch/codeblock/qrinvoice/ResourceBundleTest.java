/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ResourceBundleTest {
    private static final Map<Locale, String> LOCALE_TITLES;
    static {
        LOCALE_TITLES = new HashMap<>();
        LOCALE_TITLES.put(Locale.ENGLISH, "Payment part");
        LOCALE_TITLES.put(Locale.FRENCH, "Section paiement");
        LOCALE_TITLES.put(Locale.ITALIAN, "Sezione pagamento");
        LOCALE_TITLES.put(Locale.GERMAN, "Zahlteil");
    }


    @Parameterized.Parameters(name = "Locale: {0}")
    public static Collection<Locale> data() {
        return LOCALE_TITLES.keySet();
    }
    
    private Locale locale;
    public ResourceBundleTest(final Locale locale) {
        this.locale = locale;
    }

    @Test
    public void testResourceBundleLocales() {
        for (final Locale l : LOCALE_TITLES.keySet()) {
            // just alter the default locale to check that all variants are resolving correctly
            Locale.setDefault(l);
            final ResourceBundle bundle = ResourceBundle.getBundle("qrinvoice", locale);
            assertEquals(LOCALE_TITLES.get(locale), bundle.getString("TitlePaymentPart"));
            assertEquals(locale.getLanguage(), bundle.getLocale().getLanguage());
        }
    }
    
    @Test
    public void testResourceUnknownDefaultLocale() {
        Locale.setDefault(Locale.JAPAN);
        final ResourceBundle bundle = ResourceBundle.getBundle("qrinvoice", locale);
        assertEquals(LOCALE_TITLES.get(locale), bundle.getString("TitlePaymentPart"));
        assertEquals(locale.getLanguage(), bundle.getLocale().getLanguage());
    }
}
