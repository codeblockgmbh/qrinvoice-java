/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class MimeTypeTest {

    @Test
    public void getByMimeType() {
        assertEquals(MimeType.PDF, MimeType.getByMimeType("application/pdf").orElse(null));
        assertEquals(MimeType.PDF, MimeType.getByMimeType("APPLICATION/PDF").orElse(null));
        assertEquals(MimeType.PNG, MimeType.getByMimeType("image/png").orElse(null));
        assertEquals(MimeType.JPEG, MimeType.getByMimeType("image/jpeg").orElse(null));
        assertEquals(MimeType.XLSX, MimeType.getByMimeType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet").orElse(null));
        assertFalse(MimeType.getByMimeType("n/a").isPresent());
    }

    @Test
    public void getByExtension() {
        assertEquals(MimeType.PDF, MimeType.getByExtension("pdf").orElse(null));
        assertEquals(MimeType.PDF, MimeType.getByExtension(".pdf").orElse(null));
        assertEquals(MimeType.PDF, MimeType.getByExtension(".PDF").orElse(null));
        assertEquals(MimeType.PDF, MimeType.getByExtension("pDf").orElse(null));

        assertEquals(MimeType.JPEG, MimeType.getByExtension("jpg").orElse(null));
        assertEquals(MimeType.JPEG, MimeType.getByExtension("jpeg").orElse(null));

        assertEquals(MimeType.CSV, MimeType.getByExtension("csv").orElse(null));
        assertEquals(MimeType.CSV, MimeType.getByExtension(".csv").orElse(null));
        assertEquals(MimeType.CSV, MimeType.getByExtension(".CSV").orElse(null));

        assertEquals(MimeType.XLSX, MimeType.getByExtension("xlsx").orElse(null));
        assertEquals(MimeType.XLSX, MimeType.getByExtension(".xlsx").orElse(null));
        assertEquals(MimeType.XLSX, MimeType.getByExtension(".XLSX").orElse(null));

        assertFalse(MimeType.getByExtension("mpeg").isPresent());
    }

    @Test
    public void getByFilename() {
        assertEquals(MimeType.PDF, MimeType.getByFilename(".pdf").orElse(null));
        assertEquals(MimeType.PDF, MimeType.getByFilename(".PDF").orElse(null));
        assertEquals(MimeType.PDF, MimeType.getByFilename("test.pdf").orElse(null));
        assertEquals(MimeType.PDF, MimeType.getByFilename("test.PdF").orElse(null));

        assertEquals(MimeType.JPEG, MimeType.getByFilename("logo.jpg").orElse(null));
        assertEquals(MimeType.JPEG, MimeType.getByFilename("logo.JPEG").orElse(null));
        assertEquals(MimeType.PNG, MimeType.getByFilename("logo.png").orElse(null));

        assertFalse(MimeType.getByFilename("logo").isPresent());
    }
}
