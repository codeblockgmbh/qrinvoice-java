/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.layout;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DimensionUnitUtilsTest {
    @Test
    public void millimetersToPoints() throws Exception {
        assertEquals(39.685f, DimensionUnitUtils.millimetersToPoints(14f), 0.001f);
    }
    @Test
    public void millimetersToPointsRounded() throws Exception {
        assertEquals(40, DimensionUnitUtils.millimetersToPointsRounded(14f, 72));
    }
    @Test
    public void dimensionMillimetersToPoints() throws Exception {
        assertEquals(new Dimension<>(40, 20), DimensionUnitUtils.millimetersToPointsRounded(new Dimension<Float>(14f,7f), 72));
    }

    @Test
    public void millimetersToPointsDpi() throws Exception {
        assertEquals(39.685f, DimensionUnitUtils.millimetersToPoints(14f, 72), 0.001f);
        assertEquals(165.354f, DimensionUnitUtils.millimetersToPoints(14f, 300), 0.001f);
    }

    @Test
    public void millimetersToInches() throws Exception {
        assertEquals(10f, DimensionUnitUtils.millimetersToInches(254f), 0f);
    }

    @Test
    public void inchesToPoints() throws Exception {
        assertEquals(720f, DimensionUnitUtils.inchesToPoints(10f), 0f);
    }

    @Test
    public void inchesToPointsDpi() throws Exception {
        assertEquals(720f, DimensionUnitUtils.inchesToPoints(10f, 72), 0f);
        assertEquals(3000f, DimensionUnitUtils.inchesToPoints(10f, 300), 0f);
    }

}
