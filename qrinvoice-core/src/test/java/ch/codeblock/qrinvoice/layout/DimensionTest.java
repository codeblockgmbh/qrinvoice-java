/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.layout;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DimensionTest {
    private static final Integer WIDTH = 42;
    private static final Integer HEIGHT = 21;

    private Dimension<Integer> dim;

    public DimensionTest() {
        this.dim = new Dimension<>(WIDTH, HEIGHT);
    }

    public void testCreation() {
        assertEquals(WIDTH, dim.getWidth());
        assertEquals(HEIGHT, dim.getHeight());
    }

    @Test
    public void toRectangleZeroCoordinates() throws Exception {
        final int x = 0;
        final int y = 0;
        toRectangleWithAssert(x, y);
    }

    @Test
    public void toRectangleNonZeroCoordinates() throws Exception {
        final int x = 1;
        final int y = 2;
        toRectangleWithAssert(x, y);
    }

    private void toRectangleWithAssert(final int x, final int y) {
        final Rect<Integer> rect = dim.toRectangle(x, y);
        assertEquals(x, rect.getLowerLeftX().intValue());
        assertEquals(x + WIDTH, rect.getUpperRightX().intValue());
        assertEquals(y, rect.getLowerLeftY().intValue());
        assertEquals(y + HEIGHT, rect.getUpperRightY().intValue());
    }
    
    @Test
    public void transformTest() {
        final Dimension<Integer> transformed = dim.transform(a -> a * 2);
        assertEquals(WIDTH * 2, transformed.getWidth().intValue());
        assertEquals(HEIGHT * 2, transformed.getHeight().intValue());
    }
}
