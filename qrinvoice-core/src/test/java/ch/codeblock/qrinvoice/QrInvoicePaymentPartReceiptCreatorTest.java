/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice;

import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.paymentpartreceipt.BoundaryLines;
import org.junit.Test;

public class QrInvoicePaymentPartReceiptCreatorTest {

    @Test
    public void testWithBoundaryLines() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.DIN_LANG).withBoundaryLines().applyDefaults().validateOptions();
    }

    @Test
    public void testScissorWithBoundaryLines() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.DIN_LANG).withBoundaryLines().withScissors().applyDefaults().validateOptions();
    }

    @Test
    public void testWithBoundaryLinesWithMargins() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.DIN_LANG).withBoundaryLinesWithMargins().applyDefaults().validateOptions();
    }

    @Test
    public void testSeparationTextWithBoundaryLines() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).withBoundaryLines().withSeparationText().applyDefaults().validateOptions();
    }

    @Test
    public void testSeparationTextWithBoundaryLinesWithMargins() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).withBoundaryLinesWithMargins().withSeparationText().applyDefaults().validateOptions();
    }

    @Test
    public void testSeparationTextWithoutBoundaryLinesWithMargins() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).withoutBoundaryLines().withSeparationText().applyDefaults().validateOptions();
    }

    @Test
    public void testBoundaryLinesEnum() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).boundaryLines(BoundaryLines.NONE).applyDefaults().validateOptions();
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).boundaryLines(BoundaryLines.ENABLED).applyDefaults().validateOptions();
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).boundaryLines(BoundaryLines.ENABLED_WITH_MARGINS).applyDefaults().validateOptions();
    }

    @Test
    public void testBoundaryLinesEnumWithScissors() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).boundaryLines(BoundaryLines.ENABLED).withScissors().applyDefaults().validateOptions();
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).boundaryLines(BoundaryLines.ENABLED_WITH_MARGINS).withScissors().applyDefaults().validateOptions();
    }

    @Test(expected = ValidationException.class)
    public void testScissorWithoutBoundaryLines() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.DIN_LANG).withoutBoundaryLines().withScissors().applyDefaults().validateOptions();
    }

    @Test(expected = ValidationException.class)
    public void testSeparationTextWithDinLang() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.DIN_LANG).withoutBoundaryLines().withScissors().applyDefaults().validateOptions();
    }

    @Test(expected = ValidationException.class)
    public void testScissorsWithoutBoundaryLinesEnabled() {
        QrInvoicePaymentPartReceiptCreator.create().pageSize(PageSize.A4).boundaryLines(BoundaryLines.NONE).withScissors().applyDefaults().validateOptions();
    }
}
