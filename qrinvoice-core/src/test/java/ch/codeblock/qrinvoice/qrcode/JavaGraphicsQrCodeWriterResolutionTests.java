/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.qrcode;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.output.QrCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)

public class JavaGraphicsQrCodeWriterResolutionTests {
    @Parameterized.Parameters(name = "resolution={0}")
    public static Object[] data() {
        return new Object[]{OutputResolution.LOW_150_DPI, OutputResolution.MEDIUM_300_DPI, OutputResolution.HIGH_600_DPI};
    }

    final OutputResolution resolution;

    public JavaGraphicsQrCodeWriterResolutionTests(final OutputResolution resolution) {
        this.resolution = resolution;
    }

    @Test
    public void writeLowDpi() throws Exception {
        Map<OutputResolution, Integer> sizeMap = new HashMap<>();
        sizeMap.put(OutputResolution.LOW_150_DPI, 250);
        sizeMap.put(OutputResolution.MEDIUM_300_DPI, 525);
        sizeMap.put(OutputResolution.HIGH_600_DPI, 1075);
        final Integer expectedSize = sizeMap.get(resolution);

        final String qrCodeString = "test string to encode";
        final QrCodeWriterOptions options = new QrCodeWriterOptions(OutputFormat.JPEG, resolution);
        final QrCode qrCode = JavaGraphicsQrCodeWriter.create(options).write(qrCodeString);

        final byte[] img = qrCode.getData();
        assertNotNull(img);
        assertTrue(qrCode.getSize() > 0);

        assertEquals(expectedSize, qrCode.getWidth());
        assertEquals(expectedSize, qrCode.getHeight());
    }

}
