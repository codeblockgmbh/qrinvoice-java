/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.qrcode;

import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.output.QrCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class SwissQrCodeForthBackWriteReadTests {
    private static final String SPC_QR_REFERENCE = "SPC\n" +
            "0200\n" +
            "1\n" +
            "CH4431999123000889012\n" +
            "S\n" +
            "Robert Schneider AG\n" +
            "Rue du Lac\n" +
            "1268\n" +
            "2501\n" +
            "Biel\n" +
            "CH\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "1949.75\n" +
            "CHF\n" +
            "S\n" +
            "Pia-Maria Rutschmann-Schnyder\n" +
            "Grosse Marktgasse\n" +
            "28\n" +
            "9400\n" +
            "Rorschach\n" +
            "CH\n" +
            "QRR\n" +
            "210000000003139471430009017\n" +
            "Instruction of 03.04.2019\n" +
            "EPD\n" +
            "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30\n" +
            "eBill/B/john.doe@example.com\n" +
            "cdbk/some/values";

    @Parameterized.Parameters(name = "Format={0}")
    public static Object[] data() {
        return new Object[]{OutputFormat.PNG, OutputFormat.GIF, OutputFormat.TIFF, OutputFormat.BMP, OutputFormat.JPEG};
    }

    final OutputFormat format;

    public SwissQrCodeForthBackWriteReadTests(final OutputFormat format) {
        this.format = format;
    }


    @Test
    public void write() throws Exception {
        final String qrCodeString = SPC_QR_REFERENCE;
        final QrCodeWriterOptions options = new QrCodeWriterOptions(format, 500);
        final QrCode qrCode = JavaGraphicsQrCodeWriter.create(options).write(qrCodeString);
        assertNotNull(qrCode);
        assertNotNull(qrCode.getData());
        final String resultQrCodeString = QrCodeReader.create().readQRCode(qrCode.getData());
        assertEquals(qrCodeString, resultQrCodeString);
    }

}
