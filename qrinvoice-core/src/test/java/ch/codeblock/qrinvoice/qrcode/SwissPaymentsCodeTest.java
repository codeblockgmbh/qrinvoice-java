/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.qrcode;

import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class SwissPaymentsCodeTest {
    private static final Pattern ELEMENT_SEPARATOR_PATTERN = Pattern.compile(SwissPaymentsCode.ELEMENT_SEPARATOR_REGEX_PATTERN);
    private static final Pattern VALID_CHARACTERS_PATTERN = Pattern.compile(String.format("([%s]*)", Pattern.quote(SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2)));
    private static final Pattern BILL_INFORMATION_PATTERN = Pattern.compile(SwissPaymentsCode.BILL_INFORMATION_REGEX_PATTERN);

    @Test
    public void testElementSeparator() {
        assertTrue(ELEMENT_SEPARATOR_PATTERN.matcher(SwissPaymentsCode.ELEMENT_SEPARATOR).matches());
        assertTrue(ELEMENT_SEPARATOR_PATTERN.matcher("\r").matches());
        assertTrue(ELEMENT_SEPARATOR_PATTERN.matcher("\n").matches());
        assertTrue(ELEMENT_SEPARATOR_PATTERN.matcher("\r\n").matches());
        assertFalse(ELEMENT_SEPARATOR_PATTERN.matcher("x").matches());
        assertFalse(ELEMENT_SEPARATOR_PATTERN.matcher(" ").matches());

        // CR, LF and CR + LF are permitted element separators, thus check pattern does correctly match
        // two sequences of line separator result in 3 elements
        assertEquals(3, ("a\r\n\r\nb".split(SwissPaymentsCode.ELEMENT_SEPARATOR_REGEX_PATTERN).length));
        assertEquals(3, ("a\r\rb".split(SwissPaymentsCode.ELEMENT_SEPARATOR_REGEX_PATTERN).length));
        assertEquals(3, ("a\n\nb".split(SwissPaymentsCode.ELEMENT_SEPARATOR_REGEX_PATTERN).length));
    }

    @Test
    public void testValidCharactersPattern() {
        assertTrue(VALID_CHARACTERS_PATTERN.matcher(SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2).matches());
    }

    @Test
    public void testValidBillInformation() {
        assertTrue(BILL_INFORMATION_PATTERN.matcher("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30").matches());
        assertTrue(BILL_INFORMATION_PATTERN.matcher("//BD/").matches());
        assertTrue(BILL_INFORMATION_PATTERN.matcher("//XY").matches());
        assertFalse(BILL_INFORMATION_PATTERN.matcher("/S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30").matches());
        assertFalse(BILL_INFORMATION_PATTERN.matcher("S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30").matches());
        assertFalse(BILL_INFORMATION_PATTERN.matcher("//XYZ").matches());
    }
}
