/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.qrcode;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static ch.codeblock.qrinvoice.qrcode.QrCodeLibrary.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class QrCodeLibraryTest {

    @Test
    public void supports() {
        assertFalse(ALL.supports(null));

        assertTrue(ALL.supports(ZXING));
        assertTrue(ALL.supports(BOOFCV));
        assertTrue(ALL.supports(DEFAULT));
        assertTrue(ALL.supports(ALL));

        assertTrue(DEFAULT.supports(ZXING));
        assertFalse(DEFAULT.supports(BOOFCV));
        assertTrue(DEFAULT.supports(DEFAULT));
        assertFalse(DEFAULT.supports(ALL));

        assertTrue(ZXING.supports(ZXING));
        assertFalse(ZXING.supports(BOOFCV));
        assertTrue(ZXING.supports(DEFAULT));
        assertFalse(ZXING.supports(ALL));

        assertFalse(BOOFCV.supports(ZXING));
        assertTrue(BOOFCV.supports(BOOFCV));
        assertFalse(BOOFCV.supports(DEFAULT));
        assertFalse(BOOFCV.supports(ALL));
    }

    @Test
    public void supportedBy() {
        assertFalse(ALL.supportedBy((QrCodeLibrary) null));

        assertTrue(ZXING.supportedBy(ZXING));
        assertFalse(ZXING.supportedBy(BOOFCV));
        assertTrue(ZXING.supportedBy(DEFAULT));
        assertTrue(ZXING.supportedBy(ALL));

        assertFalse(BOOFCV.supportedBy(ZXING));
        assertTrue(BOOFCV.supportedBy(BOOFCV));
        assertFalse(BOOFCV.supportedBy(DEFAULT));
        assertTrue(BOOFCV.supportedBy(ALL));

        assertTrue(DEFAULT.supportedBy(ZXING));
        assertFalse(DEFAULT.supportedBy(BOOFCV));
        assertTrue(DEFAULT.supportedBy(DEFAULT));
        assertTrue(DEFAULT.supportedBy(ALL));

        assertFalse(ALL.supportedBy(ZXING));
        assertFalse(ALL.supportedBy(BOOFCV));
        assertFalse(ALL.supportedBy(DEFAULT));
        assertTrue(ALL.supportedBy(ALL));
    }

    @Test
    public void supportedBySet() {
        assertFalse(ALL.supportedBy((List<QrCodeLibrary>) null));

        assertTrue(ZXING.supportedBy(set(ZXING)));
        assertTrue(ZXING.supportedBy(set(ZXING, BOOFCV)));
        assertTrue(ZXING.supportedBy(set(ZXING, BOOFCV, DEFAULT)));
        assertTrue(ZXING.supportedBy(set(ZXING, BOOFCV, DEFAULT, ALL)));
        assertTrue(ZXING.supportedBy(set(BOOFCV, DEFAULT, ALL)));
        assertTrue(ZXING.supportedBy(set(DEFAULT, ALL)));
        assertTrue(ZXING.supportedBy(set(ALL)));
        assertTrue(ZXING.supportedBy(set(DEFAULT)));

        assertFalse(ZXING.supportedBy(set(BOOFCV)));

        assertTrue(BOOFCV.supportedBy(set(BOOFCV)));
        assertTrue(BOOFCV.supportedBy(set(ZXING, BOOFCV)));
        assertTrue(BOOFCV.supportedBy(set(ZXING, BOOFCV, DEFAULT)));
        assertTrue(BOOFCV.supportedBy(set(ZXING, BOOFCV, DEFAULT, ALL)));
        assertTrue(BOOFCV.supportedBy(set(BOOFCV, DEFAULT, ALL)));
        assertTrue(BOOFCV.supportedBy(set(DEFAULT, ALL)));
        assertTrue(BOOFCV.supportedBy(set(ALL)));
        assertFalse(BOOFCV.supportedBy(set(DEFAULT)));
    }

    private static List<QrCodeLibrary> set(QrCodeLibrary... libraries) {
        return Arrays.stream(libraries).collect(Collectors.toList());
    }
}
