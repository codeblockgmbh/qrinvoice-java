/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.image;

import ch.codeblock.qrinvoice.layout.Dimension;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class ImageUtilsTest {

    @Test
    public void calculateTargetDimensionWidthLimited() {
        Optional<Dimension<Integer>> dimension = ImageUtils.calculateTargetDimension(1000, 800, 50, 1000, 800);
        assertFalse(dimension.isPresent());

        dimension = ImageUtils.calculateTargetDimension(1000, 800, 50, 1051, 851);
        assertTrue(dimension.isPresent());

        dimension = ImageUtils.calculateTargetDimension(1000, 800, 50, 1100, 900);
        assertTrue(dimension.isPresent());
        // width limited
        assertEquals(Integer.valueOf(818), dimension.get().getHeight());
        assertEquals(Integer.valueOf(1000), dimension.get().getWidth());
        
        dimension = ImageUtils.calculateTargetDimension(900, 800, 50, 1100, 900);
        assertTrue(dimension.isPresent());
        // height limited
        assertEquals(Integer.valueOf(800), dimension.get().getHeight());
        assertEquals(Integer.valueOf(977), dimension.get().getWidth());
    }

    @Test
    public void calculateTargetDimensionHeightLimited() {
        Optional<Dimension<Integer>> dimension = ImageUtils.calculateTargetDimension(1000, 800, 50, 800, 1000);
        assertFalse(dimension.isPresent());

        dimension = ImageUtils.calculateTargetDimension(1000, 800, 50, 851, 1051);
        assertTrue(dimension.isPresent());

        dimension = ImageUtils.calculateTargetDimension(1000, 800, 50, 900, 1100);
        assertTrue(dimension.isPresent());
        // width limited
        assertEquals(Integer.valueOf(1000), dimension.get().getHeight());
        assertEquals(Integer.valueOf(818), dimension.get().getWidth());

        dimension = ImageUtils.calculateTargetDimension(900, 800, 50, 900, 1100);
        assertTrue(dimension.isPresent());
        // height limited
        assertEquals(Integer.valueOf(977), dimension.get().getHeight());
        assertEquals(Integer.valueOf(800), dimension.get().getWidth());
    }
}
