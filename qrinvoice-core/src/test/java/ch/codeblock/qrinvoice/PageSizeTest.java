/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice;

import org.junit.Test;

import static ch.codeblock.qrinvoice.PageSize.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PageSizeTest {

    @Test
    public void greaterThan() {
        assertTrue(A4.greaterThan(A5));
        assertTrue(A4.greaterThan(DIN_LANG));
        assertTrue(A5.greaterThan(DIN_LANG));

        assertFalse(A4.greaterThan(A4));
        assertFalse(A5.greaterThan(A5));
        assertFalse(DIN_LANG.greaterThan(DIN_LANG));
        assertFalse(DIN_LANG.greaterThan(A5));
        assertFalse(DIN_LANG.greaterThan(A4));
    }

    @Test
    public void smallerThan() {
        assertFalse(A4.smallerThan(A4));
        assertFalse(A4.smallerThan(A5));
        assertFalse(A4.smallerThan(DIN_LANG));
        assertFalse(A4.smallerThan(DIN_LANG_CROPPED));
        assertFalse(A5.smallerThan(DIN_LANG));
        assertFalse(DIN_LANG.smallerThan(DIN_LANG_CROPPED));
        assertFalse(A5.smallerThan(DIN_LANG_CROPPED));

        assertFalse(A4.smallerThan(DIN_LANG));
        assertFalse(A4.smallerThan(DIN_LANG_CROPPED));
        assertFalse(A5.smallerThan(A5));
        assertFalse(DIN_LANG.smallerThan(DIN_LANG));
        assertTrue(DIN_LANG.smallerThan(A5));
        assertTrue(DIN_LANG.smallerThan(A4));
        assertTrue(DIN_LANG_CROPPED.smallerThan(DIN_LANG));
        assertTrue(DIN_LANG_CROPPED.smallerThan(A4));
    }

    @Test
    public void sameAs() {
        assertTrue(A4.sameAs(A4));
        assertTrue(A5.sameAs(A5));
        assertTrue(DIN_LANG.sameAs(DIN_LANG));

        assertFalse(A4.sameAs(A5));
        assertFalse(A4.sameAs(DIN_LANG));
        assertFalse(A5.sameAs(DIN_LANG));
        assertFalse(DIN_LANG.sameAs(A5));
    }

    @Test
    public void differentFrom() {
        assertFalse(A4.differentFrom(A4));
        assertFalse(A5.differentFrom(A5));
        assertFalse(DIN_LANG.differentFrom(DIN_LANG));

        assertTrue(A4.differentFrom(A5));
        assertTrue(A4.differentFrom(DIN_LANG));
        assertTrue(A5.differentFrom(DIN_LANG));
        assertTrue(DIN_LANG.differentFrom(A5));
    }
}
