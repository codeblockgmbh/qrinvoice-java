/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.output;

import ch.codeblock.qrinvoice.OutputFormat;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class OutputTest {

    @Test
    public void testMappings() {
        final OutputFormat format = OutputFormat.PDF;
        final byte[] data = new byte[]{1, 2, 3, 4, 5, 6};
        final Output output = new Output(format, data, 10, 20) {

        };

        assertNotNull(output.getData());
        assertEquals(format, output.getOutputFormat());
        assertEquals(data.length, output.getSize());
        assertEquals(output.getData().length, output.getSize());
        assertEquals(Integer.valueOf(10), output.getWidth());
        assertEquals(Integer.valueOf(20), output.getHeight());
    }
}
