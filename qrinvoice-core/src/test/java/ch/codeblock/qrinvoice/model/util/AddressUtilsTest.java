/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.util;

import ch.codeblock.qrinvoice.model.Address;
import ch.codeblock.qrinvoice.model.AddressType;
import ch.codeblock.qrinvoice.model.Creditor;
import ch.codeblock.qrinvoice.model.UltimateCreditor;
import org.junit.Test;

import java.util.List;

import static ch.codeblock.qrinvoice.model.util.AddressUtils.toSingleLineAddress;
import static org.junit.Assert.*;

public class AddressUtilsTest {
    @Test
    public void isEmptyStructured() {
        final Creditor creditor = new Creditor();
        assertTrue(AddressUtils.isEmpty(creditor));
        creditor.setAddressType(AddressType.STRUCTURED);

        creditor.setName("Firma");
        creditor.setStreetName("Hauptstrasse");
        creditor.setHouseNumber("7");
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setStreetName("Hauptstrasse");
        creditor.setHouseNumber("7");
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setStreetName(null);
        creditor.setHouseNumber("7");
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setStreetName(null);
        creditor.setHouseNumber(null);
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setStreetName(null);
        creditor.setHouseNumber(null);
        creditor.setPostalCode(null);
        creditor.setCity("Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setStreetName(null);
        creditor.setHouseNumber(null);
        creditor.setPostalCode(null);
        creditor.setCity(null);
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setAddressType(null);
        creditor.setName(null);
        creditor.setStreetName(null);
        creditor.setHouseNumber(null);
        creditor.setPostalCode(null);
        creditor.setCity(null);
        creditor.setCountry(null);
        assertTrue(AddressUtils.isEmpty(creditor));
    }

    @Test
    public void isEmptyCombined() {
        final Creditor creditor = new Creditor();
        assertTrue(AddressUtils.isEmpty(creditor));

        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Firma");
        creditor.setAddressLine1("Hauptstrasse 7");
        creditor.setAddressLine2("3000 Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setAddressLine1("Hauptstrasse 7");
        creditor.setAddressLine2("3000 Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setAddressLine1(null);
        creditor.setAddressLine2("3000 Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setAddressLine1(null);
        creditor.setAddressLine2("3000 Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setAddressLine1(null);
        creditor.setAddressLine2("Bern");
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setName(null);
        creditor.setAddressLine1(null);
        creditor.setAddressLine2(null);
        creditor.setCountry("CH");
        assertFalse(AddressUtils.isEmpty(creditor));

        creditor.setAddressType(null);
        creditor.setName(null);
        creditor.setAddressLine1(null);
        creditor.setAddressLine2(null);
        creditor.setCountry(null);
        assertTrue(AddressUtils.isEmpty(creditor));
    }

    @Test
    public void toAddressFullStructured() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Firma");
        creditor.setStreetName("Hauptstrasse");
        creditor.setHouseNumber("7");
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");
        creditor.setCountry("CH");

        final String addressImage = "Firma" + "\n" +
                "Hauptstrasse 7" + "\n" +
                "3000 Bern";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toAddressFullStructuredGermany() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Firma");
        creditor.setStreetName("Torstrasse");
        creditor.setHouseNumber("42");
        creditor.setPostalCode("10115");
        creditor.setCity("Berlin");
        creditor.setCountry("DE");

        final String addressImage = "Firma" + "\n" +
                "Torstrasse 42" + "\n" +
                "DE - 10115 Berlin";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toAddressFullStructuredLiechtenstein() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Firma");
        creditor.setStreetName("Torstrasse");
        creditor.setHouseNumber("42");
        creditor.setPostalCode("9490");
        creditor.setCity("Vaduz");
        creditor.setCountry("LI");

        final String addressImage = "Firma" + "\n" +
                "Torstrasse 42" + "\n" +
                "LI - 9490 Vaduz";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toAddressFullStructuredOmitStreetnameHousenumber() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Firma");
        creditor.setStreetName("Hauptstrasse");
        creditor.setHouseNumber("7");
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");
        creditor.setCountry("CH");

        final String addressImage = "Firma" + "\n" +
                "3000 Bern";
        assertEquals(addressImage, toString(creditor, true));
    }

    @Test
    public void toAddressWithoutStreetHousenumber() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Firma");
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");
        creditor.setCountry("CH");

        creditor.setHouseNumber(null);
        final String addressImage = "Firma" + "\n" +
                "3000 Bern";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toAddressWithoutHousenumber() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Firma");
        creditor.setStreetName("Hauptstrasse");
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");
        creditor.setCountry("CH");

        creditor.setHouseNumber(null);
        final String addressImage = "Firma" + "\n" +
                "Hauptstrasse" + "\n" +
                "3000 Bern";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toAddressWithoutCity() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Firma");
        creditor.setStreetName("Hauptstrasse");
        creditor.setHouseNumber("7");

        final String addressImage = "Firma" + "\n" +
                "Hauptstrasse 7";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toAddressWithoutCountryCodeStructured() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Firma");
        creditor.setStreetName("Hauptstrasse");
        creditor.setHouseNumber("7");
        creditor.setPostalCode("3000");
        creditor.setCity("Bern");

        final String addressImage = "Firma" + "\n" +
                "Hauptstrasse 7" + "\n" +
                "3000 Bern";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toAddressFullCombined() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Firma");
        creditor.setAddressLine1("Hauptstrasse 7");
        creditor.setAddressLine2("3000 Bern");
        creditor.setCountry("CH");

        final String addressImage = "Firma" + "\n" +
                "Hauptstrasse 7" + "\n" +
                "3000 Bern";
        assertEquals(addressImage, toString(creditor));
    }
    @Test
    public void toAddressFullCombinedGermany() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Firma");
        creditor.setAddressLine1("Torstrasse 42");
        creditor.setAddressLine2("10115 Berlin");
        creditor.setCountry("DE");

        final String addressImage = "Firma" + "\n" +
                "Torstrasse 42" + "\n" +
                "DE - 10115 Berlin";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toAddressFullCombinedOmitAddressLine1() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Firma");
        creditor.setAddressLine2("3000 Bern");
        creditor.setCountry("CH");

        final String addressImage = "Firma" + "\n" +
                "3000 Bern";
        assertEquals(addressImage, toString(creditor));
    }


    @Test
    public void toAddressWithoutAddressLine2() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Firma");
        creditor.setAddressLine1("Hauptstrasse 7");

        final String addressImage = "Firma" + "\n" +
                "Hauptstrasse 7";
        assertEquals(addressImage, toString(creditor));
    }


    @Test
    public void toAddressWithoutCountryCodeCombined() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Firma");
        creditor.setAddressLine1("Hauptstrasse 7");
        creditor.setAddressLine2("3000 Bern");

        final String addressImage = "Firma" + "\n" +
                "Hauptstrasse 7" + "\n" +
                "3000 Bern";
        assertEquals(addressImage, toString(creditor));
    }


    @Test
    public void toAddressEmpty() {
        final Creditor creditor = new Creditor();
        final String addressImage = "";
        assertEquals(addressImage, toString(creditor));
    }

    @Test
    public void toSingleLineAddressEmpty() {
        final UltimateCreditor ultimateCreditor = new UltimateCreditor();
        final String addressImage = "";
        assertEquals(addressImage, toSingleLineAddress(ultimateCreditor));
    }

    @Test
    public void toSingleLineAddressWithoutCountryCodeCombined() {
        final UltimateCreditor cultimateCreditoreditor = new UltimateCreditor();
        cultimateCreditoreditor.setAddressType(AddressType.COMBINED);
        cultimateCreditoreditor.setName("Firma");
        cultimateCreditoreditor.setAddressLine1("Hauptstrasse 7");
        cultimateCreditoreditor.setAddressLine2("3000 Bern");

        final String addressImage = "Firma, Hauptstrasse 7, 3000 Bern";
        assertEquals(addressImage, toSingleLineAddress(cultimateCreditoreditor));
    }

    private String toString(final Address creditor) {
        return toString(creditor, false);
    }

    private String toString(final Address address, final boolean omitStreetNameHouseNumberOrAddressLine1) {
        final List<String> list = AddressUtils.toAddressLines(address, omitStreetNameHouseNumberOrAddressLine1);
        if (list == null) {
            return "";
        }
        return String.join("\n", list);
    }

}
