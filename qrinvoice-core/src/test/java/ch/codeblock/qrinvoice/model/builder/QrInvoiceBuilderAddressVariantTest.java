/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.builder;

import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import ch.codeblock.qrinvoice.tests.data.TestReferenceNumbers;
import org.junit.Before;
import org.junit.Test;

public class QrInvoiceBuilderAddressVariantTest {
    @Before
    public void init() {
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "true");
    }

    @Test
    public void createStructuredSemiFluent() {
        final QrInvoiceBuilder qrInvoiceBuilder = QrInvoiceBuilder.create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225);

        qrInvoiceBuilder.paymentAmountInformation()
                .chf(1949.75);

        qrInvoiceBuilder.creditor()
                .structuredAddress()
                .name("Robert Schneider AG")
                .streetName("Rue du Lac")
                .houseNumber("1268")
                .postalCode("2501")
                .city("Biel")
                .country("CH");
        qrInvoiceBuilder.ultimateCreditor()
                .structuredAddress()
                .name("Robert Schneider Services Switzerland AG")
                .streetName("Rue du Lac")
                .houseNumber("1268/3/1")
                .postalCode("2501")
                .city("Biel")
                .country("CH");
        qrInvoiceBuilder.ultimateDebtor()
                .structuredAddress()
                .name("Pia-Maria Rutschmann-Schnyder")
                .streetName("Grosse Marktgasse")
                .houseNumber("28")
                .postalCode("9400")
                .city("Rorschach")
                .country("CH");

        qrInvoiceBuilder.paymentReference().creditorReference(TestReferenceNumbers.SCOR_RF18539007547034);

        final QrInvoice qrInvoice = qrInvoiceBuilder.build();
    }


    @Test
    public void createStructuredFluent() {
        QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .structuredAddress()
                        .name("Robert Schneider Services Switzerland AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268/3/1")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .creditorReference(TestReferenceNumbers.SCOR_RF18539007547034)
                )
                .build();
    }


    @Test
    public void createCombinedSemiFluent() {
        final QrInvoiceBuilder qrInvoiceBuilder = QrInvoiceBuilder.create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225);

        qrInvoiceBuilder.paymentAmountInformation()
                .chf(1949.75);

        qrInvoiceBuilder.creditor()
                .combinedAddress()
                .name("Robert Schneider AG")
                .addressLine1("Rue du Lac 1268")
                .addressLine2("2501 Biel")
                .country("CH");
        qrInvoiceBuilder.ultimateCreditor()
                .combinedAddress()
                .name("Robert Schneider Services Switzerland AG")
                .addressLine1("Rue du Lac 1268/3/1")
                .addressLine2("2501 Biel")
                .country("CH");
        qrInvoiceBuilder.ultimateDebtor()
                .combinedAddress()
                .name("Pia-Maria Rutschmann-Schnyder")
                .addressLine1("Grosse Marktgasse 28")
                .addressLine2("9400 Rorschach")
                .country("CH");

        
        qrInvoiceBuilder.paymentReference()
                .creditorReference(TestReferenceNumbers.SCOR_RF18539007547034);

        final QrInvoice qrInvoice = qrInvoiceBuilder.build();
    }

    @Test
    public void createCombinedFluent() {
        QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .combinedAddress()
                        .name("Robert Schneider AG")
                        .addressLine1("Rue du Lac 1268")
                        .addressLine2("2501 Biel")
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .combinedAddress()
                        .name("Robert Schneider Services Switzerland AG")
                        .addressLine1("Rue du Lac 1268/3/1")
                        .addressLine2("2501 Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .combinedAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .addressLine1("Grosse Marktgasse 28")
                        .addressLine2("9400 Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .creditorReference(TestReferenceNumbers.SCOR_RF18539007547034)
                )
                .build();
    }

}
