/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.builder;

import ch.codeblock.qrinvoice.model.PaymentReference;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class QrInvoiceBuilderPaymentReferenceTest {

    @Test
    public void createValidateFluent() {
        validate(createFluentWithReferenceBuild1(), "000000000000000000000001273", "QRR");
        validate(createFluentWithReferenceBuild2(), "000000000000000000000001273", "QRR");
        validate(createFluentWithReferenceBuild3(), "123000000000000000000001279", "QRR");

        validate(createFluentWithCreditorReferenceBuild(), "RF67127", "SCOR");
    }

    @Test
    public void createValidateSemiFluent() {
        validate(createSemiFluentWithReferenceBuild1(), "000000000000000000000001273", "QRR");
        validate(createSemiFluentWithReferenceBuild2(), "000000000000000000000001273", "QRR");
        validate(createSemiFluentWithReferenceBuild3(), "123000000000000000000001279", "QRR");

        validate(createSemiFluentWithCreditorReferenceBuild(), "RF67127", "SCOR");
    }

    private void validate(QrInvoice qrInvoice, String expectedReference, String expectedReferenceType) {
        final PaymentReference paymentReference = qrInvoice.getPaymentReference();
        assertEquals(expectedReferenceType, paymentReference.getReferenceType().getReferenceTypeCode());
        assertEquals(expectedReference, paymentReference.getReference());
        assertNull(paymentReference.getAdditionalInformation().getUnstructuredMessage());
        assertEquals("EPD", paymentReference.getAdditionalInformation().getTrailer());
        assertNull(paymentReference.getAdditionalInformation().getBillInformation());
    }

    private QrInvoice createFluentWithReferenceBuild1() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012)
                .paymentAmountInformation(PaymentAmountInformationBuilder::eur
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Salvation Army Foundation Switzerland")
                        .postalCode("3000")
                        .city("Bern")
                        .country("CH")
                )
                .paymentReference(p -> p.buildQrReference("127"))
                .build();
    }

    private QrInvoice createFluentWithReferenceBuild2() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012)
                .paymentAmountInformation(PaymentAmountInformationBuilder::eur
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Salvation Army Foundation Switzerland")
                        .postalCode("3000")
                        .city("Bern")
                        .country("CH")
                )
                .paymentReference(p -> p.buildQrReference("", "127"))
                .build();
    }

    private QrInvoice createFluentWithReferenceBuild3() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012)
                .paymentAmountInformation(PaymentAmountInformationBuilder::eur
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Salvation Army Foundation Switzerland")
                        .postalCode("3000")
                        .city("Bern")
                        .country("CH")
                )
                .paymentReference(p -> p.buildQrReference("123", "127"))
                .build();
    }

    private QrInvoice createFluentWithCreditorReferenceBuild() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(PaymentAmountInformationBuilder::eur
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Salvation Army Foundation Switzerland")
                        .postalCode("3000")
                        .city("Bern")
                        .country("CH")
                )
                .paymentReference(p -> p.buildCreditorReference("127"))
                .build();
    }

    private QrInvoice createSemiFluentWithReferenceBuild1() {
        final QrInvoiceBuilder qrInvoiceBuilder = QrInvoiceBuilder.create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012);

        qrInvoiceBuilder.creditor()
                .structuredAddress()
                .name("Robert Schneider AG")
                .streetName("Rue du Lac")
                .houseNumber("1268")
                .postalCode("2501")
                .city("Biel")
                .country("CH");

        qrInvoiceBuilder.paymentReference()
                .buildQrReference("127");

        qrInvoiceBuilder.paymentAmountInformation(PaymentAmountInformationBuilder::eur
        );

        qrInvoiceBuilder.alternativeSchemeParameters(null);

        return qrInvoiceBuilder.build();
    }

    private QrInvoice createSemiFluentWithReferenceBuild2() {
        final QrInvoiceBuilder qrInvoiceBuilder = QrInvoiceBuilder.create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012);

        qrInvoiceBuilder.creditor()
                .structuredAddress()
                .name("Robert Schneider AG")
                .streetName("Rue du Lac")
                .houseNumber("1268")
                .postalCode("2501")
                .city("Biel")
                .country("CH");

        qrInvoiceBuilder.paymentReference()
                .buildQrReference("", "127");

        qrInvoiceBuilder.paymentAmountInformation(PaymentAmountInformationBuilder::eur
        );

        qrInvoiceBuilder.alternativeSchemeParameters(null);

        return qrInvoiceBuilder.build();
    }

    private QrInvoice createSemiFluentWithReferenceBuild3() {
        final QrInvoiceBuilder qrInvoiceBuilder = QrInvoiceBuilder.create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012);

        qrInvoiceBuilder.creditor()
                .structuredAddress()
                .name("Robert Schneider AG")
                .streetName("Rue du Lac")
                .houseNumber("1268")
                .postalCode("2501")
                .city("Biel")
                .country("CH");

        qrInvoiceBuilder.paymentReference()
                .buildQrReference("123", "127");

        qrInvoiceBuilder.paymentAmountInformation(PaymentAmountInformationBuilder::eur
        );

        qrInvoiceBuilder.alternativeSchemeParameters(null);

        return qrInvoiceBuilder.build();
    }

    private QrInvoice createSemiFluentWithCreditorReferenceBuild() {
        final QrInvoiceBuilder qrInvoiceBuilder = QrInvoiceBuilder.create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225);

        qrInvoiceBuilder.creditor()
                .structuredAddress()
                .name("Robert Schneider AG")
                .streetName("Rue du Lac")
                .houseNumber("1268")
                .postalCode("2501")
                .city("Biel")
                .country("CH");

        qrInvoiceBuilder.paymentReference()
                .buildCreditorReference("127");

        qrInvoiceBuilder.paymentAmountInformation(PaymentAmountInformationBuilder::eur
        );

        qrInvoiceBuilder.alternativeSchemeParameters(null);

        return qrInvoiceBuilder.build();
    }
}
