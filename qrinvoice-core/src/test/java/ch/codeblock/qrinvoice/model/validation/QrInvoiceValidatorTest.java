/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import ch.codeblock.qrinvoice.tests.data.TestReferenceNumbers;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Currency;
import java.util.List;

import static org.junit.Assert.*;

public class QrInvoiceValidatorTest {
    private final Logger logger = LoggerFactory.getLogger(QrInvoiceValidatorTest.class);

    private QrInvoiceValidator qrInvoiceValidator;

    @Before
    public void setUp() throws Exception {
        qrInvoiceValidator = QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2);
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "true");
    }

    // ------------------------------------------------------
    // CREDITOR_REFERENCE - SCOR
    // ------------------------------------------------------

    @Test
    public void testValidCreditorReference() throws Exception {
        final QrInvoice qrInvoice = createValidCreditorReference();
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.isValid());
    }

    @Test
    public void testInvalidCreditorName() throws Exception {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = createValidCreditorReference();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        qrInvoice.getCreditorInformation().getCreditor().setName("this is invalid as it is just a too long string with more than 70 chars");
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("creditorinformation.creditor.name"));
        assertEquals(1, validationResult.getErrors().size());
    }

    @Test
    public void testInvalidIban() throws Exception {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = createValidCreditorReference();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        qrInvoice.getCreditorInformation().setIban("non-valid-iban");
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("creditorInformation.iban"));
        assertEquals(1, validationResult.getErrors().size());
    }

    @Test
    public void testMissingCurrency() throws Exception {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = createValidCreditorReference();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        qrInvoice.getPaymentAmountInformation().setCurrency(null);
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("paymentAmountInformation.currency"));
        assertEquals(1, validationResult.getErrors().size());
    }

    @Test
    public void testUnsupportedCurrency() throws Exception {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = createValidCreditorReference();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        qrInvoice.getPaymentAmountInformation().setCurrency(Currency.getInstance("JPY"));
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("paymentAmountInformation.currency"));
        assertEquals(1, validationResult.getErrors().size());
    }

    @Test
    public void testCreditorReferenceWithWrongReferenceNumber() throws Exception {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = createValidCreditorReference();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        qrInvoice.getPaymentReference().setReference("1234567");
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());

        // ReferenceNr is a valid Creditor-Reference, but not a valid Qr-Reference
        assertTrue(validationResult.hasErrors("paymentReference.reference"));
        assertEquals(1, validationResult.getErrors().size());
    }

    private QrInvoice createValidCreditorReference() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .structuredAddress()
                        .name("Robert Schneider Services Switzerland AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268/3/1")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .creditorReference(TestReferenceNumbers.SCOR_RF18539007547034)
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(null)
                .build();
    }

    // ------------------------------------------------------
    // QR_REFERENCE - QRR
    // ------------------------------------------------------

    @Test
    public void testValidQrReference() throws Exception {
        final QrInvoice qrInvoice = createValidQrReference();
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.isValid());
    }

    @Test
    public void testQrReferenceWithWrongReferenceNumber() throws Exception {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = createValidQrReference();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        // when setting a creditor-reference reference number for type QRR, a validation error must occur
        qrInvoice.getPaymentReference().setReference(TestReferenceNumbers.SCOR_RF18539007547034);
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("paymentReference.reference"));
        assertEquals(1, validationResult.getErrors().size());
    }

    private QrInvoice createValidQrReference() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .structuredAddress()
                        .name("Robert Schneider Services Switzerland AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268/3/1")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference("110001234560000000000813457")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(null)
                .build();
    }

    // ------------------------------------------------------
    // WITHOUT_REFERENCE - NON
    // ------------------------------------------------------

    @Test
    public void testValidWithoutReference() throws Exception {
        final QrInvoice qrInvoice = createValidWithoutReference();
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.isValid());
    }

    @Test
    public void testWithoutReferenceWithReferenceNumber() throws Exception {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = createValidWithoutReference();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        // when setting reference number for type NON (without reference), a validation error must occur
        qrInvoice.getPaymentReference().setReference("110001234560000000000813457");
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("paymentReference.reference"));
        assertEquals(1, validationResult.getErrors().size());
    }


    private QrInvoice createValidWithoutReference() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .structuredAddress()
                        .name("Robert Schneider Services Switzerland AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268/3/1")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .withoutReference()
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(null)
                .build();
    }

    @Test
    public void testValidateStringOneInvalidSequence() {
        final String input = "Price is 20€";
        final CharacterValidationResult result = qrInvoiceValidator.validateString(input);
        assertTrue(result.hasInvalidCharacters());
        assertEquals(input, result.getInput());

        final List<InvalidCharacterSequence> charSequences = result.getInvalidCharacterSequences();
        assertEquals(1, charSequences.size());

        final InvalidCharacterSequence invalidCharSequence = charSequences.get(0);
        assertEquals("Invalid character(s) '€' at index 11 with a length of 1 (context: 's 20<€>')", invalidCharSequence.getErrorSummary());
        assertEquals("€", invalidCharSequence.getInvalidCharSequence());
        assertEquals(11, invalidCharSequence.getInvalidCharSequenceStartIndex());
    }


    @Test
    public void testValidateStringThreeInvalidSequences() {
        final String input = "This is € first, second ФѰ and third Œ";
        final CharacterValidationResult result = qrInvoiceValidator.validateString(input);
        assertTrue(result.hasInvalidCharacters());
        assertEquals(input, result.getInput());

        final List<InvalidCharacterSequence> charSequences = result.getInvalidCharacterSequences();
        assertEquals(3, charSequences.size());

        assertEquals("€", charSequences.get(0).getInvalidCharSequence());
        assertEquals("ФѰ", charSequences.get(1).getInvalidCharSequence());
        assertEquals("Œ", charSequences.get(2).getInvalidCharSequence());
    }

    @Test
    public void testValidString() {
        assertTrue(qrInvoiceValidator.isStringValid("Price is 20 CHF"));
        assertFalse(qrInvoiceValidator.isStringValid("Price is 20€"));

        assertTrue(qrInvoiceValidator.isStringValid(""));
        assertTrue(qrInvoiceValidator.isStringValid(null));
    }
}
