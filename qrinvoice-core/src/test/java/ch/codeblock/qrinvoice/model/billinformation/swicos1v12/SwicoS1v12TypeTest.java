/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.billinformation.swicos1v12;

import ch.codeblock.qrinvoice.model.ParseException;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class SwicoS1v12TypeTest {

    @Test
    public void parse() {
        final String input = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertNotNull(s1);
        assertEquals("10201409", s1.getInvoiceReference());
        assertEquals(LocalDate.of(2019, 5, 12), s1.getInvoiceDate());
        assertEquals("1400.000-53", s1.getCustomerReference());
        assertEquals("106017086", s1.getUidNumber());

        assertEquals(1, s1.getVatDetails().size());
        final VatDetails firstTaxDetails = s1.getVatDetails().get(0);
        assertEquals(BigDecimal.valueOf(7.7), firstTaxDetails.getTaxPercentage());
        assertFalse(firstTaxDetails.hasNetAmount());
        assertNull(firstTaxDetails.getTaxedNetAmount());

        assertEquals(LocalDate.of(2018, 5, 8), s1.getVatDateStart());
        assertNull(s1.getVatDateEnd());

        //assertEquals("106017086", SwicoS1v12Type.get());
        assertEquals(2, s1.getPaymentConditions().size());
        assertEquals(10, s1.getPaymentConditions().get(0).getEligiblePaymentPeriodDays());
        assertEquals(BigDecimal.valueOf(2), s1.getPaymentConditions().get(0).getCashDiscountPercentage());

        assertEquals(30, s1.getPaymentConditions().get(1).getEligiblePaymentPeriodDays());
        assertEquals(BigDecimal.valueOf(0), s1.getPaymentConditions().get(1).getCashDiscountPercentage());

        assertEquals(30, s1.getDefaultPaymentPeriodDays().intValue());
        assertEquals(LocalDate.of(2019, 6, 11), s1.getDefaultPaymentPeriod());
        assertEquals(30, s1.getInvoiceDate().until(s1.getDefaultPaymentPeriod()).get(ChronoUnit.DAYS));

        assertTrue(s1.validate().isValid());
    }

    @Test
    public void parseWithDecimalDiscounts() {
        final String input = "//S1/10/100000/11/200114/40/5.00:10;2.00:20;0.0:30";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertNotNull(s1);

        // Test discounts
        assertEquals(30, s1.getDefaultPaymentPeriodDays().intValue());

        assertEquals(3, s1.getPaymentConditions().size());
        assertEquals(10, s1.getPaymentConditions().get(0).getEligiblePaymentPeriodDays());
        assertTrue(s1.getPaymentConditions().get(0).getCashDiscountPercentage().compareTo(new BigDecimal("5")) == 0);

        assertEquals(20, s1.getPaymentConditions().get(1).getEligiblePaymentPeriodDays());
        assertTrue(s1.getPaymentConditions().get(1).getCashDiscountPercentage().compareTo(new BigDecimal("2")) == 0);

        assertEquals(30, s1.getPaymentConditions().get(2).getEligiblePaymentPeriodDays());
        assertTrue(s1.getPaymentConditions().get(2).getCashDiscountPercentage().compareTo(new BigDecimal("0")) == 0);

        assertTrue(s1.validate().isValid());
    }

    @Test
    public void parseWithPaymentConditionsNeverZeroPercent() {
        final String input = "//S1/10/100000/11/200114/40/5.00:10;2.00:20;1.50:30";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertNotNull(s1);

        // Test discounts
        assertNull(s1.getDefaultPaymentPeriodDays());

        assertEquals(3, s1.getPaymentConditions().size());
        assertEquals(10, s1.getPaymentConditions().get(0).getEligiblePaymentPeriodDays());
        assertTrue(s1.getPaymentConditions().get(0).getCashDiscountPercentage().compareTo(new BigDecimal("5")) == 0);

        assertEquals(20, s1.getPaymentConditions().get(1).getEligiblePaymentPeriodDays());
        assertTrue(s1.getPaymentConditions().get(1).getCashDiscountPercentage().compareTo(new BigDecimal("2")) == 0);

        assertEquals(30, s1.getPaymentConditions().get(2).getEligiblePaymentPeriodDays());
        assertTrue(s1.getPaymentConditions().get(2).getCashDiscountPercentage().compareTo(new BigDecimal("1.5")) == 0);

        assertTrue(s1.validate().isValid());
    }

    @Test
    public void parseForthAndBack1() {
        final String input = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals(input, s1.toBillInformationString());
    }

    @Test
    public void parseForthAndBack2() {
        final String input = "//S1/10/10104/11/180228/30/395856455/31/180226180227/32/3.7:400.19;7.7:553.39;0:14/40/0:30";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals(input, s1.toBillInformationString());
    }

    @Test
    public void parseForthAndBack3() {
        final String input = "//S1/10/4031202511/11/180107/20/61257233.4/30/105493567/32/8:49.82/33/2.5:14.85/40/0:30";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals(input, s1.toBillInformationString());
    }

    @Test
    public void parseForthAndBack4() {
        final String input = "//S1/10/X.66711\\/8824/11/200712/20/MW-2020-04/30/107978798/32/2.5:117.22/40/3:5;1.5:20;1:40;0:60";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals(input, s1.toBillInformationString());
    }

    @Test
    public void parseForthAndBackMinimal() {
        final String input = "//S1";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals(input, s1.toBillInformationString());
    }

    @Test
    public void parseForthAndBackPaymentConditions() {
        final String input = "//S1/40/3:5;1.5:20;1:40;0:60";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals(input, s1.toBillInformationString());
    }

    @Test
    public void parseForthAndBackTagOrdering() {
        final String input = "//S1/40/3:5;1.5:20;1:40;0:60/10/4031202511";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals("//S1/10/4031202511/40/3:5;1.5:20;1:40;0:60", s1.toBillInformationString());
    }

    @Test
    public void parseForthAndBackEscaping() {
        final String input = "//S1/10/\\/invoicenumber/20/kunden-\\\\referenz";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals(input, s1.toBillInformationString());
    }

    @Test
    public void parseEmptyTag() {
        final String input = "//S1/10//11/200712";

        final SwicoS1v12 s1 = SwicoS1v12Type.getInstance().parse(input);
        assertEquals("//S1/11/200712", s1.toBillInformationString());
    }

    @Test(expected = ParseException.class)
    public void parseUnknownTag() {
        SwicoS1v12Type.getInstance().parse("//S1/97/200712");
    }

    @Test(expected = ParseException.class)
    public void parseDuplicateTag() {
        SwicoS1v12Type.getInstance().parse("//S1/11/200712/11/200719");
    }

    @Test
    public void testEscape() {
        assertNull(SwicoS1v12Type.escapeValue(null));
        assertEquals("", SwicoS1v12Type.escapeValue(""));
        assertEquals("a\\/b", SwicoS1v12Type.escapeValue("a/b"));
        assertEquals("a\\\\b", SwicoS1v12Type.escapeValue("a\\b"));
        assertEquals("\\/a\\\\b\\/c\\\\d\\/e\\\\", SwicoS1v12Type.escapeValue("/a\\b/c\\d/e\\"));
    }

    @Test
    public void testUnescape() {
        assertNull(SwicoS1v12Type.unescapeValue(null));
        assertEquals("", SwicoS1v12Type.unescapeValue(""));
        assertEquals("a/b", SwicoS1v12Type.unescapeValue("a\\/b"));
        assertEquals("a\\b", SwicoS1v12Type.unescapeValue("a\\\\b"));
        assertEquals("/a\\b/c\\d/e\\", SwicoS1v12Type.unescapeValue("\\/a\\\\b\\/c\\\\d\\/e\\\\"));
    }

    @Test
    public void testEscapeUnescape() {
        assertEquals("a/b", SwicoS1v12Type.unescapeValue(SwicoS1v12Type.escapeValue("a/b")));
        assertEquals("a\\b", SwicoS1v12Type.unescapeValue(SwicoS1v12Type.escapeValue("a\\b")));
        assertEquals("/a\\b/c\\d/e\\", SwicoS1v12Type.unescapeValue(SwicoS1v12Type.escapeValue("/a\\b/c\\d/e\\")));
    }

    @Test
    public void testExtractValueList() {
        assertTrue(SwicoS1v12Type.extractValueList(null).isEmpty());
        assertTrue(SwicoS1v12Type.extractValueList("").isEmpty());

        final List<String> a = SwicoS1v12Type.extractValueList("a");
        assertEquals(1, a.size());
        assertEquals("a", a.get(0));

        final List<String> b = SwicoS1v12Type.extractValueList("abc;def;g");
        assertEquals(3, b.size());
        assertEquals("abc", b.get(0));
        assertEquals("def", b.get(1));
        assertEquals("g", b.get(2));
    }

    @Test
    public void testExtractValuePair() {
        assertFalse(SwicoS1v12Type.extractValuePair(null).isPresent());
        assertFalse(SwicoS1v12Type.extractValuePair("").isPresent());

        final Optional<String[]> pair = SwicoS1v12Type.extractValuePair("8:1000");
        assertTrue(pair.isPresent());
        assertEquals(2, pair.get().length);
        assertEquals("8", pair.get()[0]);
        assertEquals("1000", pair.get()[1]);

        final Optional<String[]> singleValue = SwicoS1v12Type.extractValuePair("7.7");
        assertTrue(singleValue.isPresent());
        assertEquals(1, singleValue.get().length);
        assertEquals("7.7", singleValue.get()[0]);
    }


    @Test
    public void testExtractValuePairList() {
        final List<Optional<String[]>> collect = SwicoS1v12Type.extractValueList("3:15;0.5:45;0:90").stream().map(SwicoS1v12Type::extractValuePair).collect(Collectors.toList());
        final String[] first = collect.get(0).orElseThrow(() -> new RuntimeException("value not present"));
        assertEquals("3", first[0]);
        assertEquals("15", first[1]);

        final String[] second = collect.get(1).orElseThrow(() -> new RuntimeException("value not present"));
        assertEquals("0.5", second[0]);
        assertEquals("45", second[1]);

        final String[] third = collect.get(2).orElseThrow(() -> new RuntimeException("value not present"));
        assertEquals("0", third[0]);
        assertEquals("90", third[1]);
    }
}
