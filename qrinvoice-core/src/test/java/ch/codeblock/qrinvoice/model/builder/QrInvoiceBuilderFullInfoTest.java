/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.builder;

import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.*;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import ch.codeblock.qrinvoice.tests.data.TestReferenceNumbers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class QrInvoiceBuilderFullInfoTest {
    @Before
    public void init() {
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "true");
    }
    @After
    public void after() {
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "false");
    }

    @Test
    public void createValidateSemiFluent() {
        validate(createSemiFluent());
    }


    @Test
    public void createValidateFluent() {
        validate(createFluent());
    }

    @Test
    public void verifyDifferentConstructionPathEquals() {
        final QrInvoice fluent = createFluent();
        final QrInvoice semiFluent = createSemiFluent();
        assertEquals(fluent, semiFluent);
    }

    private void validate(QrInvoice qrInvoice) {
        final Header header = qrInvoice.getHeader();
        assertEquals("SPC", header.getQrType());
        assertEquals(200, header.getVersion());
        assertEquals(1, header.getCodingType());

        final PaymentAmountInformation paymentAmountInformation = qrInvoice.getPaymentAmountInformation();
        assertEquals(BigDecimal.valueOf(1949.75), paymentAmountInformation.getAmount());
        assertEquals("CHF", paymentAmountInformation.getCurrency().getCurrencyCode());

        final CreditorInformation creditorInformation = qrInvoice.getCreditorInformation();
        assertEquals("CH4431999123000889012", creditorInformation.getIban());

        final Creditor creditor = creditorInformation.getCreditor();
        assertEquals(AddressType.STRUCTURED, creditor.getAddressType());
        assertEquals("Robert Schneider AG", creditor.getName());
        assertEquals("Rue du Lac", creditor.getStreetName());
        assertEquals("1268", creditor.getHouseNumber());
        assertEquals("2501", creditor.getPostalCode());
        assertEquals("Biel", creditor.getCity());
        assertEquals("CH", creditor.getCountry());


        final UltimateCreditor ultimateCreditor = qrInvoice.getUltimateCreditor();
        assertEquals(AddressType.STRUCTURED, ultimateCreditor.getAddressType());
        assertEquals("Robert Schneider Services Switzerland AG", ultimateCreditor.getName());
        assertEquals("Rue du Lac", ultimateCreditor.getStreetName());
        assertEquals("1268/3/1", ultimateCreditor.getHouseNumber());
        assertEquals("2501", ultimateCreditor.getPostalCode());
        assertEquals("Biel", ultimateCreditor.getCity());
        assertEquals("CH", ultimateCreditor.getCountry());


        final UltimateDebtor ultimateDebtor = qrInvoice.getUltimateDebtor();
        assertEquals(AddressType.STRUCTURED, ultimateDebtor.getAddressType());
        assertEquals("Pia-Maria Rutschmann-Schnyder", ultimateDebtor.getName());
        assertEquals("Grosse Marktgasse", ultimateDebtor.getStreetName());
        assertEquals("28", ultimateDebtor.getHouseNumber());
        assertEquals("9400", ultimateDebtor.getPostalCode());
        assertEquals("Rorschach", ultimateDebtor.getCity());
        assertEquals("CH", ultimateDebtor.getCountry());

        final PaymentReference paymentReference = qrInvoice.getPaymentReference();
        assertEquals("QRR", paymentReference.getReferenceType().getReferenceTypeCode());
        assertEquals("110001234560000000000813457", paymentReference.getReference());

        final AdditionalInformation additionalInformation = paymentReference.getAdditionalInformation();
        assertEquals("Instruction of 03.04.2019", additionalInformation.getUnstructuredMessage());
        assertEquals("EPD", additionalInformation.getTrailer());
        assertEquals("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30", additionalInformation.getBillInformation());

        assertNull(qrInvoice.getAlternativeSchemes());

    }

    private QrInvoice createSemiFluent() {
        final QrInvoiceBuilder qrInvoiceBuilder = QrInvoiceBuilder.create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012);

        qrInvoiceBuilder.paymentAmountInformation()
                .chf(1949.75);

        qrInvoiceBuilder.creditor()
                .structuredAddress()
                .name("Robert Schneider AG")
                .streetName("Rue du Lac")
                .houseNumber("1268")
                .postalCode("2501")
                .city("Biel")
                .country("CH");
        qrInvoiceBuilder.ultimateCreditor()
                .structuredAddress()
                .name("Robert Schneider Services Switzerland AG")
                .streetName("Rue du Lac")
                .houseNumber("1268/3/1")
                .postalCode("2501")
                .city("Biel")
                .country("CH");
        qrInvoiceBuilder.ultimateDebtor()
                .structuredAddress()
                .name("Pia-Maria Rutschmann-Schnyder")
                .streetName("Grosse Marktgasse")
                .houseNumber("28")
                .postalCode("9400")
                .city("Rorschach")
                .country("CH");

        qrInvoiceBuilder.paymentReference()
                .qrReference(TestReferenceNumbers.QRR_110001234560000000000813457);
        
        qrInvoiceBuilder.additionalInformation()
                .unstructuredMessage("Instruction of 03.04.2019")
                .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");

        qrInvoiceBuilder.alternativeSchemeParameters(null);

        return qrInvoiceBuilder.build();
    }

    private QrInvoice createFluent() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .structuredAddress()
                        .name("Robert Schneider Services Switzerland AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268/3/1")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference(TestReferenceNumbers.QRR_110001234560000000000813457)
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(null)
                .build();
    }
}
