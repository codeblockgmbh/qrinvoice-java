/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import ch.codeblock.qrinvoice.model.AddressType;
import ch.codeblock.qrinvoice.model.Creditor;
import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class AddressValidatorTest {
    private final Logger logger = LoggerFactory.getLogger(AddressValidatorTest.class);

    private AddressValidator validator;
    private ValidationResult validationResult;

    @Before
    public void setup() {
        validator = AddressValidator.create();
        validationResult = new ValidationResult();
    }

    @Test
    public void testValidateStructured() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Robert Schneider AG");
        creditor.setStreetName("Rue du Lac");
        creditor.setStreetName("1268");
        creditor.setPostalCode("2501");
        creditor.setCity("Biel");
        creditor.setCountry("CH");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.isEmpty());
    }

    @Test
    public void testValidateStructuredMinimal() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Robert Schneider AG");
        creditor.setPostalCode("2501");
        creditor.setCity("Biel");
        creditor.setCountry("CH");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.isEmpty());
    }

    @Test
    public void testValidateStructuredInvalidAddressLine1() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Robert Schneider AG");
        creditor.setPostalCode("2501");
        creditor.setCity("Biel");
        creditor.setAddressLine1("Invalid");
        creditor.setCountry("CH");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertEquals(1, validationResult.getErrors().size());
        assertEquals("creditorinformation.creditor.addressLine1", validationResult.getErrors().get(0).getDataPath());
        assertEquals("validation.error.address.structured.addressLines", validationResult.getErrors().get(0).getErrorMessageKeys().get(0));
    }

    @Test
    public void testValidateStructuredInvalidAddressLine2() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.STRUCTURED);
        creditor.setName("Robert Schneider AG");
        creditor.setPostalCode("2501");
        creditor.setCity("Biel");
        creditor.setAddressLine2("Invalid");
        creditor.setCountry("CH");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertEquals(1, validationResult.getErrors().size());
        assertEquals("creditorinformation.creditor.addressLine2", validationResult.getErrors().get(0).getDataPath());
        assertEquals("validation.error.address.structured.addressLines", validationResult.getErrors().get(0).getErrorMessageKeys().get(0));
    }

    @Test
    public void testValidateCombined() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Robert Schneider AG");
        creditor.setAddressLine1("Rue du Lac 1268");
        creditor.setAddressLine2("2501 Biel");
        creditor.setCountry("CH");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.isEmpty());
    }

    @Test
    public void testValidateCombinedMinimal() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Robert Schneider AG");
        creditor.setAddressLine2("2501 Biel");
        creditor.setCountry("CH");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.isEmpty());
    }

    @Test
    public void testValidateCombinedMissingAddressLine2() {
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        creditor.setName("Robert Schneider AG");
        creditor.setCountry("CH");
        validator.validate(creditor, validationResult);
        assertEquals(1, validationResult.getErrors().size());
        assertEquals("creditorinformation.creditor.addressLine2", validationResult.getErrors().get(0).getDataPath());
        assertTrue(validationResult.getErrors().get(0).getErrorMessageKeys().contains("validation.error.address.addressLine2"));
        assertTrue(validationResult.getErrors().get(0).getErrorMessageKeys().contains("validation.error.address.combined.addressLine2"));
    }

    @Test
    public void testInvalidNull() {
        final Creditor creditor = new Creditor();
        creditor.setName(null);
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertFalse(validationResult.isEmpty());
    }

    @Test
    public void testInvalidEmpty() {
        final Creditor creditor = new Creditor();
        creditor.setName("");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertFalse(validationResult.isEmpty());
    }

    @Test
    public void testInvalidTooLong() {
        final Creditor creditor = new Creditor();
        creditor.setName("This is a way too long company name that exceeds 70 characters by a 5 chars");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertFalse(validationResult.isEmpty());
    }

    @Test
    public void testInvalidCharacter() {
        final Creditor creditor = new Creditor();
        creditor.setName("The following char is unsupported: ж rigth? yes it is, even sequences жжж");
        validator.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertFalse(validationResult.isEmpty());
    }

    @Test
    public void testCombinedAddressInvalidGuidelinesV2_3() {
        AddressValidator validatorV2_3 = AddressValidator.create(ImplementationGuidelinesQrBillVersion.V2_3);
        final Creditor creditor = new Creditor();
        creditor.setAddressType(AddressType.COMBINED);
        validatorV2_3.validate(creditor, validationResult);
        logger.info(validationResult.getValidationErrorSummary());
        assertFalse(validationResult.isEmpty());
    }
}
