/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PaymentReferenceTest {

    @Test
    public void testValidRereferenceTypeCodes() {
        assertEquals(ReferenceType.QR_REFERENCE, ReferenceType.parse("QRR"));
        assertEquals(ReferenceType.CREDITOR_REFERENCE, ReferenceType.parse("SCOR"));
        assertEquals(ReferenceType.WITHOUT_REFERENCE, ReferenceType.parse("NON"));
    }

    @Test(expected = Exception.class)
    public void testInvalidRereferenceTypeCodeCase() {
        assertEquals(ReferenceType.QR_REFERENCE, ReferenceType.parse("qrr"));
    }

    @Test(expected = Exception.class)
    public void testEmptyString() {
        assertEquals(ReferenceType.QR_REFERENCE, ReferenceType.parse(""));
    }

    @Test(expected = Exception.class)
    public void testNull() {
        assertEquals(ReferenceType.QR_REFERENCE, ReferenceType.parse(null));
    }
}
