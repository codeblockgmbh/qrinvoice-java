/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.parser;

import ch.codeblock.qrinvoice.model.billinformation.BillInformation;
import ch.codeblock.qrinvoice.model.billinformation.RawBillInformation;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12;
import org.junit.Test;

import static org.junit.Assert.*;

public class BillInformationParserTest {

    @Test
    public void parseEmpty() {
        assertNull(BillInformationParser.create().parseBillInformation(null));
        assertNull(BillInformationParser.create().parseBillInformation(""));
    }

    @Test
    public void parseS1() {
        final String input = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30";

        final BillInformation billInformation = BillInformationParser.create().parseBillInformation(input);
        assertTrue(billInformation instanceof SwicoS1v12);
        assertEquals(input, billInformation.toBillInformationString());
    }

    @Test
    public void parseRawStructured() {
        final String input = "//XY/z";
        final BillInformation billInformation = BillInformationParser.create().parseBillInformation(input);
        assertTrue(billInformation instanceof RawBillInformation);
        assertEquals(input, billInformation.toBillInformationString());
    }

    @Test
    public void parseRawUnstructured() {
        final String input = "foo";
        final BillInformation billInformation = BillInformationParser.create().parseBillInformation(input);
        assertTrue(billInformation instanceof RawBillInformation);
        assertEquals(input, billInformation.toBillInformationString());
    }
}
