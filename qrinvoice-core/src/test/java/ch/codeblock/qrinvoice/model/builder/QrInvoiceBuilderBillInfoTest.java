/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.builder;

import ch.codeblock.qrinvoice.model.AdditionalInformation;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QrInvoiceBuilderBillInfoTest {

    @Test
    public void testRawBillInformation() {
        final QrInvoice qrInvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(PaymentAmountInformationBuilder::chf)
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .additionalInformation(a -> a.billInformation("//XY/raw string 42"))
                .build();
        final AdditionalInformation additionalInformation = qrInvoice.getPaymentReference().getAdditionalInformation();
        assertEquals("//XY/raw string 42", additionalInformation.getBillInformation());
    }


}
