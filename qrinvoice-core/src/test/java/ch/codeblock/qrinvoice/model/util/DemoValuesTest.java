/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.util;

import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.builder.PaymentAmountInformationBuilder;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import ch.codeblock.qrinvoice.tests.data.TestReferenceNumbers;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class DemoValuesTest {

    @Test
    public void checkDemoValuesEmptyObject() {
        // just check there are no exceptions on partial objects
        DemoValues.apply(new QrInvoice());
    }

    @Test
    public void checkDemoValuesPartialObject() {
        final QrInvoice qrInvoice = QrInvoiceBuilder.create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(PaymentAmountInformationBuilder::chf)
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .build();
        DemoValues.apply(qrInvoice);
        assertThat(qrInvoice.getPaymentAmountInformation().getAmount(), is(nullValue()));
    }

    @Test
    public void checkDemoValuesFullExample() {
        final QrInvoice qrInvoice = createFluent();
        DemoValues.apply(qrInvoice);
        assertThat(qrInvoice.getPaymentAmountInformation().getAmount(), is(BigDecimal.valueOf(1.25)));
        assertThat(qrInvoice.getPaymentReference().getAdditionalInformation().getUnstructuredMessage().contains("DEMO"), is(true));
    }

    private QrInvoice createFluent() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference(TestReferenceNumbers.QRR_110001234560000000000813457)
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(null)
                .build();
    }

}
