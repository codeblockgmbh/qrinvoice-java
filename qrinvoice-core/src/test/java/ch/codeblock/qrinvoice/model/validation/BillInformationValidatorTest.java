/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import ch.codeblock.qrinvoice.model.PaymentAmountInformation;
import ch.codeblock.qrinvoice.model.QrInvoice;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BillInformationValidatorTest {

    @Test
    public void testValidEmptyS1() {
        final ValidationResult validationResult = BillInformationValidator.create().validate("//S1");
        assertTrue(validationResult.isValid());
    }

    @Test
    public void testValidS1() {
        final ValidationResult validationResult = BillInformationValidator.create().validate("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30");
        assertTrue(validationResult.isValid());
    }

    @Test
    public void testCrossQrInvoiceValidation() {
        final QrInvoice qrInvoice = new QrInvoice();
        qrInvoice.setPaymentAmountInformation(new PaymentAmountInformation());
        qrInvoice.getPaymentAmountInformation().setAmount(BigDecimal.valueOf(118.5)); // 100 + 100*7.7% + 10 + 10 * 8%
        final ValidationResult validationResult = BillInformationValidator.create()
                .validate(qrInvoice, "//S1/32/7.7:100;8:10");
        assertTrue(validationResult.isValid());


        qrInvoice.getPaymentAmountInformation().setAmount(BigDecimal.valueOf(119.1));
        final ValidationResult validationResult2 = BillInformationValidator.create()
                .validate(qrInvoice, "//S1/32/7.7:100;8:10");
        assertFalse(validationResult2.isValid());
    }

    @Test
    public void testValidBeginning() {
        assertTrue(BillInformationValidator.create().validate("//XY").isValid());
        assertTrue(BillInformationValidator.create().validate("//BD").isValid());
    }

    @Test
    public void testInvalidBeginning() {
        assertFalse(BillInformationValidator.create().validate("//XYZ").isValid());
        assertFalse(BillInformationValidator.create().validate("///S1").isValid());
        assertFalse(BillInformationValidator.create().validate("/S1").isValid());
        assertFalse(BillInformationValidator.create().validate("S1").isValid());
    }

}
