/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.parser;

import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.tests.resources.SpcSample;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class SwissPaymentsCodeValidParserTest {

    @Parameterized.Parameters(name = "Testfile: {0}")
    public static Collection<SpcSample> data() {
        return SpcSamplesRegistry.data().stream().filter(SpcSample::isValid).collect(Collectors.toList());
    }

    private final SpcSample sample;

    public SwissPaymentsCodeValidParserTest(final SpcSample sample) {
        this.sample = sample;
    }

    @Test
    public void testParse() {
        final String fileContent = SpcSamplesRegistry.getFileContent(sample);
        final SwissPaymentsCode parsed = SwissPaymentsCodeParser.create().parse(fileContent);
        assertEquals("SPC", parsed.getQrType());
        assertTrue(SwissPaymentsCode.isVersionSupported( parsed.getVersion()));
        assertEquals("EPD", parsed.getTrailer());
    }
}
