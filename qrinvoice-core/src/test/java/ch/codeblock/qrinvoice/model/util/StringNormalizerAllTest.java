/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.util;

import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class StringNormalizerAllTest {

    private StringNormalizer normalizerV2_2;
    private StringNormalizer normalizerV2_3;

    @Before
    public void init() {
        this.normalizerV2_2 = StringNormalizer.create(ImplementationGuidelinesQrBillVersion.V2_2).enableAll();
        this.normalizerV2_3 = StringNormalizer.create(ImplementationGuidelinesQrBillVersion.V2_3).enableAll();
    }

    @Test
    public void testNull() {
        assertNull(normalizerV2_2.normalize(null));
    }

    @Test
    public void testEmpty() {
        assertEquals("", normalizerV2_2.normalize(""));
        assertEquals("", normalizerV2_2.normalize(" "));
        assertEquals("", normalizerV2_2.normalize("  "));
    }

    @Test
    public void testSimpleMixed() {
        assertEquals("a b \"c\" d e f", normalizerV2_2.normalize(" a b\t«c»\rd\r\ne\nf "));
    }

    @Test
    public void testMultipleCharacterReplacements() {
        assertEquals("a b \"c\" d \"e\" f", normalizerV2_2.normalize(" a b «c» d «e» f "));
        assertEquals("a b c d e f", normalizerV2_2.normalize(" a\tb\tc\td\te\tf"));
        assertEquals("a b c d e f", normalizerV2_2.normalize(" a\nb\nc\nd\ne\r\nf"));
    }

    @Test
    public void testTrim() {
        assertEquals("", normalizerV2_2.normalize(" "));
        assertEquals("a b c d e f", normalizerV2_2.normalize(" a\tb\tc\td\te\tf\t"));
        assertEquals("a b c d e f", normalizerV2_2.normalize(" a\tb\tc\td\te\tf\n"));
        assertEquals("a b c d e f", normalizerV2_2.normalize(" a\tb\tc\td\te\tf\n "));
        assertEquals("a b c d e f", normalizerV2_2.normalize(" a\tb\tc\td\te\tf\r\n "));
        assertEquals("a b c d e f", normalizerV2_2.normalize(" a\tb\tc\td\te\tf\r\n"));
    }

    @Test
    public void testOe() {
        assertEquals("Château-d'Oex", normalizerV2_2.normalize("Château-d'Œx"));
        assertEquals("château-d'oex", normalizerV2_2.normalize("château-d'œx"));
    }

    @Test
    public void testAllReplacements() {
        final String before = "start \u00AD‘’°a“a”a„a‟X«a»‘’‐x‑x‒x–x—x―x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end¨";
        final String after = "start -''.a\"a\"a\"a\"X\"a\"''-x-x-x-x-x-x-AAaaOoYYyÖöxDd123AeAexaexOexoexEUREURend";
        assertEquals(after, normalizerV2_2.normalize(before));
    }

    @Test
    public void testAllReplacements_GTE_V2_3() {
        final String before = "start \u00AD‘’°a“a”a„a‟X«a»‘’‐x‑x‒x–x—x―x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end¨";
        final String after = "start \u00AD''°a\"a\"a\"a\"X«a»''-x-x-x-x-x-x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end¨";
        assertEquals(after, normalizerV2_3.normalize(before));
    }

    @Test
    public void testSameLengthReplacements() {
        final String before = "start \u00AD‘’°a“a”a„a‟X«a»‘’‐x‑x‒x–x—x―x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end";
        final String after = "start -''.a\"a\"a\"a\"X\"a\"''-x-x-x-x-x-x-AAaaOoYYyÖöxDd123ÆÆxæxŒxœx€€end";
        assertEquals(after, StringNormalizer.create().enableReplaceCharactersSameLength().normalize(before));
    }

    @Test
    public void testSameLengthReplacements_GTE_V2_3() {
        final String before = "start \u00AD‘’°a“a”a„a‟X«a»‘’‐x‑x‒x–x—x―x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end";
        final String after = "start \u00AD''°a\"a\"a\"a\"X«a»''-x-x-x-x-x-x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end";
        final StringNormalizer normalizer = StringNormalizer.create(ImplementationGuidelinesQrBillVersion.V2_3);
        assertEquals(after, normalizer.enableReplaceCharactersSameLength().normalize(before));
    }


    @Test
    public void testMultipleCharsReplacements() {
        final String before = "start \u00AD‘’°a“a”a„a‟X«a»‘’‐x‑x‒x–x—x―x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end¨";
        final String after = "start \u00AD‘’°a“a”a„a‟X«a»‘’‐x‑x‒x–x—x―x·ÃÅãåÕõÝŸÿØø×Ðð¹²³AeAexaexOexoexEUREURend¨";
        assertEquals(after, StringNormalizer.create().enableReplaceCharactersWithMultipleChars().normalize(before));
    }

    @Test
    public void testMultipleCharsReplacements_GTE_V2_3() {
        final String before = "start \u00AD‘’°a“a”a„a‟X«a»‘’‐x‑x‒x–x—x―x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end¨";
        final String after = "start \u00AD‘’°a“a”a„a‟X«a»‘’‐x‑x‒x–x—x―x·ÃÅãåÕõÝŸÿØø×Ðð¹²³ÆÆxæxŒxœx€€end¨";
        final StringNormalizer normalizer = StringNormalizer.create(ImplementationGuidelinesQrBillVersion.V2_3);
        assertEquals(after, normalizer.enableReplaceCharactersWithMultipleChars().normalize(before));
    }

    @Test
    public void testEdgeCases() {
        assertEquals("-", normalizerV2_2.normalize("·"));
        assertEquals("EUR", normalizerV2_2.normalize("€"));
    }

    @Test
    public void testNoReplacementsOfValidChars() {
        assertEquals(SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2, StringNormalizer.create().enableAll().enableTrim(false).normalize(SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2));
    }
}
