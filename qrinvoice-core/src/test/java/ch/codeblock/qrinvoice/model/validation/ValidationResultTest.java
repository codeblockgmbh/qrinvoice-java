/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import org.junit.Test;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.Assert.*;

public class ValidationResultTest {
    private static final ResourceBundle LABELS = ResourceBundle.getBundle("qrinvoice", Locale.ENGLISH);

    private static final String TEST_PATH = "testPath";
    private static final String NON_ERROR_DATA_PATH = "nonErrorDataPath";

    @Test
    public void testEmptyValidationResult() {
        final ValidationResult validationResult = new ValidationResult();
        assertTrue(validationResult.isEmpty());
        assertTrue(validationResult.getErrors().isEmpty());
        assertTrue(validationResult.isValid());
        assertTrue(validationResult.isValid(TEST_PATH));
        assertTrue(validationResult.isValid(NON_ERROR_DATA_PATH));

        assertFalse(validationResult.hasErrors());
        assertFalse(validationResult.hasErrors(TEST_PATH));
        assertFalse(validationResult.hasErrors(NON_ERROR_DATA_PATH));
        assertEquals("No validation errors", validationResult.getValidationErrorSummary());

        // mustn't throw, but if it should, test will fail
        validationResult.throwExceptionOnErrors();
    }

    @Test
    public void testOneValidationError() {
        final String messageKey = "validation.error.invalidCharacters";
        
        final ValidationResult validationResult = new ValidationResult();
        validationResult.addError(TEST_PATH, null, null, "{{"+ messageKey +"}}");
        
        final List<ValidationResult.ValidationError<Serializable>> errors = validationResult.getErrors();

        assertFalse(validationResult.isEmpty());
        assertFalse(validationResult.isValid());
        assertFalse(validationResult.isValid(TEST_PATH));
        assertTrue(validationResult.isValid(NON_ERROR_DATA_PATH));
        assertFalse(errors.isEmpty());
        assertEquals(1, errors.size());
        
        final ValidationResult.ValidationError<Serializable> validationError = errors.get(0);
        assertEquals(1, validationError.getErrorMessageKeys().size());
        assertEquals(messageKey, validationError.getErrorMessageKeys().get(0));
        assertEquals(1, validationError.getErrorMsgKeys().length);
        assertEquals(messageKey, validationError.getErrorMsgKeys()[0]);
        assertEquals(1, validationError.getErrorMessages().size());
        assertEquals(LABELS.getString(messageKey), validationError.getErrorMessages().get(0));

        assertTrue(validationResult.hasErrors());
        assertTrue(validationResult.hasErrors(TEST_PATH));
        assertFalse(validationResult.hasErrors(NON_ERROR_DATA_PATH));
        assertTrue(validationResult.getValidationErrorSummary().contains(TEST_PATH));

        try {
            // mustn't throw, but if it should, test will fail
            validationResult.throwExceptionOnErrors();
            fail("ValidationException expected but not received");
        } catch (ValidationException ex) {
            // expected
        }
    }
}
