/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.mapper;

import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.tests.resources.SpcSamplesRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class MapToModelTest {

    @Parameterized.Parameters(name = "Testfile: {0}")
    public static Collection<String> data() {
        return SpcSamplesRegistry.validDataFilePaths();
    }

    private final String testFile;

    public MapToModelTest(final String testFile) {
        this.testFile = testFile;
    }

    @Test
    public void testLoad() {
        final SwissPaymentsCode original = SwissPaymentsCodeParser.create().parse(SpcSamplesRegistry.getFileContent(testFile));
        final SwissPaymentsCodeToModelMapper mapper = SwissPaymentsCodeToModelMapper.create();
        final QrInvoice qrInvoice = mapper.map(original);
        assertEquals("SPC", qrInvoice.getHeader().getQrType());
        assertTrue(qrInvoice.getHeader().getVersion() >= 200 && qrInvoice.getHeader().getVersion() <= 299);
    }
}
