/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AlternativeSchemeParameterValidatorTest {

    @Test
    public void testValidEBill() {
        assertTrue(AlternativeSchemeParameterValidator.create().validate("eBill/B/CHE123456789").isValid());
        assertTrue(AlternativeSchemeParameterValidator.create().validate("eBill/B/peter@sample.ch").isValid());

        assertTrue(AlternativeSchemeParameterValidator.create().validate("eBill/R/peter@sample.ch").isValid());
        assertTrue(AlternativeSchemeParameterValidator.create().validate("eBill/R/CHE123456789").isValid());
    }

    @Test
    public void testValidateRawStringNull() {
        AlternativeSchemeParameterValidator validator = AlternativeSchemeParameterValidator.create();
        ValidationResult result = new ValidationResult();
        validator.validateRawString(null, result);
        assertTrue(result.isValid());
    }

    @Test
    public void testValidateRawStringWithEmptyString() {
        AlternativeSchemeParameterValidator validator = AlternativeSchemeParameterValidator.create();
        ValidationResult result = new ValidationResult();
        validator.validateRawString("", result);
        assertTrue(result.isValid());
    }

    @Test
    public void testValidGeneric() {
        assertTrue(AlternativeSchemeParameterValidator.create().validate("unknown/").isValid());
        assertTrue(AlternativeSchemeParameterValidator.create().validate("na/").isValid());
    }


    @Test
    public void testValidateRawStringWithValidInput() {
        AlternativeSchemeParameterValidator validator = AlternativeSchemeParameterValidator.create();
        ValidationResult result = new ValidationResult();
        validator.validateRawString("eBill/B/CHE123456789", result);
        assertTrue(result.isValid());
    }

    @Test
    public void testValidateRawStringWithInvalidLength() {
        AlternativeSchemeParameterValidator validator = AlternativeSchemeParameterValidator.create();
        ValidationResult result = new ValidationResult();
        String longInput = new String(new char[141]).replace("\0", "a");
        validator.validateRawString(longInput, result);
        assertFalse(result.isValid());
    }
}
