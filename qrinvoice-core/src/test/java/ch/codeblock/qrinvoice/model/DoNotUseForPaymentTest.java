/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DoNotUseForPaymentTest {

    @Test
    public void testLocalize() {
        assertEquals("NICHT ZUR ZAHLUNG VERWENDEN", DoNotUseForPayment.localize("DO NOT USE FOR PAYMENT", Locale.GERMAN));
        assertEquals("NE PAS UTILISER POUR LE PAIEMENT", DoNotUseForPayment.localize("DO NOT USE FOR PAYMENT", Locale.FRENCH));
        assertEquals("NON UTILIZZARE PER IL PAGAMENTO", DoNotUseForPayment.localize("DO NOT USE FOR PAYMENT", Locale.ITALIAN));
        assertEquals("DO NOT USE FOR PAYMENT", DoNotUseForPayment.localize("DO NOT USE FOR PAYMENT", Locale.ENGLISH));

        assertEquals("DO NOT USE FOR PAYMENT", DoNotUseForPayment.localize("NICHT ZUR ZAHLUNG VERWENDEN", Locale.ENGLISH));
        assertEquals("NICHT ZUR ZAHLUNG VERWENDEN", DoNotUseForPayment.localize("NICHT ZUR ZAHLUNG VERWENDEN", Locale.GERMAN));
        assertEquals("NE PAS UTILISER POUR LE PAIEMENT", DoNotUseForPayment.localize("NICHT ZUR ZAHLUNG VERWENDEN", Locale.FRENCH));
        assertEquals("NON UTILIZZARE PER IL PAGAMENTO", DoNotUseForPayment.localize("NICHT ZUR ZAHLUNG VERWENDEN", Locale.ITALIAN));

        assertEquals("DO NOT USE FOR PAYMENT", DoNotUseForPayment.localize("NE PAS UTILISER POUR LE PAIEMENT", Locale.ENGLISH));
        assertEquals("NICHT ZUR ZAHLUNG VERWENDEN", DoNotUseForPayment.localize("NE PAS UTILISER POUR LE PAIEMENT", Locale.GERMAN));
        assertEquals("NE PAS UTILISER POUR LE PAIEMENT", DoNotUseForPayment.localize("NE PAS UTILISER POUR LE PAIEMENT", Locale.FRENCH));
        assertEquals("NON UTILIZZARE PER IL PAGAMENTO", DoNotUseForPayment.localize("NE PAS UTILISER POUR LE PAIEMENT", Locale.ITALIAN));

        assertEquals("DO NOT USE FOR PAYMENT", DoNotUseForPayment.localize("NON UTILIZZARE PER IL PAGAMENTO", Locale.ENGLISH));
        assertEquals("NICHT ZUR ZAHLUNG VERWENDEN", DoNotUseForPayment.localize("NON UTILIZZARE PER IL PAGAMENTO", Locale.GERMAN));
        assertEquals("NE PAS UTILISER POUR LE PAIEMENT", DoNotUseForPayment.localize("NON UTILIZZARE PER IL PAGAMENTO", Locale.FRENCH));
        assertEquals("NON UTILIZZARE PER IL PAGAMENTO", DoNotUseForPayment.localize("NON UTILIZZARE PER IL PAGAMENTO", Locale.ITALIAN));

        assertEquals("Rechnung Nr. 42", DoNotUseForPayment.localize("Rechnung Nr. 42", Locale.GERMAN));

        assertEquals("", DoNotUseForPayment.localize("", Locale.ITALIAN));
        assertNull(DoNotUseForPayment.localize(null, Locale.ITALIAN));
    }
}
