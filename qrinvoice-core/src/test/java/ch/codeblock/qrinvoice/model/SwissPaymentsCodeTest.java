/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model;

import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class SwissPaymentsCodeTest {
    private final Logger logger = LoggerFactory.getLogger(SwissPaymentsCodeTest.class);

    @Test
    public void testToQrCodeString() {
        final SwissPaymentsCode swissPaymentsCode = new SwissPaymentsCode();
        swissPaymentsCode.setQrType("SPC");
        swissPaymentsCode.setAmt("1234.75");
        logger.debug(swissPaymentsCode.toString());

        swissPaymentsCode.setVersion(SwissPaymentsCode.SPC_VERSION);
        swissPaymentsCode.setCoding(String.valueOf(SwissPaymentsCode.CODING_TYPE));
        swissPaymentsCode.setTrailer(SwissPaymentsCode.END_PAYMENT_DATA_TRAILER);
        final String spc = swissPaymentsCode.toSwissPaymentsCodeString();

        final SwissPaymentsCode parsed = SwissPaymentsCodeParser.create().parse(spc);
        logger.debug(parsed.toString());

        assertEquals(swissPaymentsCode, parsed);
    }

    @Test(expected = ParseException.class)
    public void testParse() {
        final SwissPaymentsCode spc = SwissPaymentsCodeParser.create().parse(SwissPaymentsCode.QR_TYPE);
    }

    @Test
    public void testParseCrLf() {
        final SwissPaymentsCode spc = SwissPaymentsCodeParser.create().parse("SPC\r\n" +
                "0200\r\n" +
                "1\r\n" + generatedEPDafterHeader(3, "\r\n"));
        logger.debug(spc.toString());
        lineFeedTestAsserts(spc);
    }

    @Test
    public void testParseLf() {
        final SwissPaymentsCode spc = SwissPaymentsCodeParser.create().parse("SPC\n" +
                "0200\n" +
                "1\n" + generatedEPDafterHeader(3, "\n"));
        logger.debug(spc.toString());
        lineFeedTestAsserts(spc);
    }

    @Test
    public void testParseCr() {
        final SwissPaymentsCode spc = SwissPaymentsCodeParser.create().parse("SPC\r" +
                "0200\r" +
                "1\r" + generatedEPDafterHeader(3, "\r"));
        logger.debug(spc.toString());
        lineFeedTestAsserts(spc);
    }

    private void lineFeedTestAsserts(final SwissPaymentsCode spc) {
        assertNotNull(spc);
        assertEquals("SPC", spc.getQrType());
        assertEquals("0200", spc.getVersion());
        assertEquals("1", spc.getCoding());
    }

    @Test(expected = ParseException.class)
    public void testParseNull() throws Exception {
        SwissPaymentsCodeParser.create().parse(null);
    }

    @Test
    public void testEqualsHashCode() {
        final SwissPaymentsCode spc1 = SwissPaymentsCodeParser.create().parse("SPC\r" +
                "0200\r" +
                "1\r" + generatedEPDafterHeader(3, "\r"));
        final SwissPaymentsCode spc2 = SwissPaymentsCodeParser.create().parse("SPC\n" +
                "0200\n" +
                "1\n" + generatedEPDafterHeader(3, "\n"));
        assertTrue(spc1.equals(spc2));
        assertTrue(spc1.hashCode() == spc2.hashCode());

        spc2.setVersion("0100");
        assertFalse(spc1.equals(spc2));
        assertFalse(spc1.hashCode() == spc2.hashCode());
    }

    private String generatedEPDafterHeader(final int givenRowsBefore, final String separator) {
        // just generated some blank lines and then append the EPD header
        final int trailerLine = 31;
        return IntStream.range(0, trailerLine - givenRowsBefore).mapToObj(i -> "").collect(Collectors.joining(separator)) + SwissPaymentsCode.END_PAYMENT_DATA_TRAILER;
    }

    @Test
    public void testVersion() {
        assertEquals(Short.valueOf((short) 2), SwissPaymentsCode.extractMajorVersion(SwissPaymentsCode.SPC_VERSION).orElse(null));
        assertTrue(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, "0200"));
        assertTrue(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, "0210"));
        assertTrue(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, "0299"));
        assertFalse(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, "0300"));
        assertFalse(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, "0199"));
        assertFalse(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, "0100"));
        assertFalse(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, "200"));
        assertFalse(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, "2"));
        assertFalse(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, ""));
        assertFalse(SwissPaymentsCode.majorVersionEquals(SwissPaymentsCode.SPC_VERSION, null));
    }

    @Test
    public void testIsMajorVersionSupported() {
        assertTrue(SwissPaymentsCode.isVersionSupported(SwissPaymentsCode.SPC_VERSION));
        assertTrue(SwissPaymentsCode.isVersionSupported("0200"));
        assertTrue(SwissPaymentsCode.isVersionSupported("0210"));
        assertTrue(SwissPaymentsCode.isVersionSupported("0299"));
        assertFalse(SwissPaymentsCode.isVersionSupported("0100"));
        assertFalse(SwissPaymentsCode.isVersionSupported(""));
        assertFalse(SwissPaymentsCode.isVersionSupported((String) null));

        assertTrue(SwissPaymentsCode.isVersionSupported((short) 200));
        assertTrue(SwissPaymentsCode.isVersionSupported((short) 210));
        assertTrue(SwissPaymentsCode.isVersionSupported((short) 299));
        assertFalse(SwissPaymentsCode.isVersionSupported((short) 100));
        assertFalse(SwissPaymentsCode.isVersionSupported((Short) null));
    }
}
