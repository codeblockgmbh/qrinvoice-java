/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import ch.codeblock.qrinvoice.model.builder.PaymentAmountInformationBuilder;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import ch.codeblock.qrinvoice.tests.data.TestReferenceNumbers;
import org.junit.Test;

/**
 * Tests the 
 */
public class StructuredReferenceCombinationTest {

    @Test
    public void validQrIbanQrCombination() {
        create().creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012).paymentReference(
                r -> r.qrReference(TestReferenceNumbers.QRR_110001234560000000000813457)
        ).build();
    }

    @Test(expected = ValidationException.class)
    public void invalidQrIbanScorCombination() {
        create().creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012).paymentReference(
                r -> r.creditorReference(TestReferenceNumbers.SCOR_RF18539007547034)
        ).build();
    }

    @Test(expected = ValidationException.class)
    public void invalidQrIbanNonCombination() {
        create().creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012).build();
    }

    @Test
    public void validIbanScor() {
        create().creditorIBAN(TestIbans.IBAN_CH3709000000304442225).paymentReference(
                r -> r.creditorReference(TestReferenceNumbers.SCOR_RF18539007547034)
        ).build();
    }

    @Test
    public void validIbanNonCombination() {
        create().creditorIBAN(TestIbans.IBAN_CH3709000000304442225).build();
    }

    @Test(expected = ValidationException.class)
    public void invalidIbanQrrCombination() {
        create().creditorIBAN(TestIbans.IBAN_CH3709000000304442225).paymentReference(
                r -> r.creditorReference(TestReferenceNumbers.QRR_110001234560000000000813457)
        ).build();
    }


    private QrInvoiceBuilder create() {
        return QrInvoiceBuilder
                .create()
                .paymentAmountInformation(PaymentAmountInformationBuilder::eur)
                .creditor(c -> c
                        .structuredAddress()
                        .name("Salvation Army Foundation Switzerland")
                        .postalCode("3000")
                        .city("Bern")
                        .country("CH")
                );
    }
}
