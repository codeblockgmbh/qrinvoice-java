/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class ImplementationGuidelinesQrBillVersionTest {

    @Test
    public void shouldBeV2_2_past() {
        final LocalDate date = LocalDate.of(2021, 10, 7);
        final ImplementationGuidelinesQrBillVersion qrBillVersion = ImplementationGuidelinesQrBillVersion.forDate(date);
        assertEquals(ImplementationGuidelinesQrBillVersion.V2_2, qrBillVersion);
    }
    
    @Test
    public void shouldBeV2_2_lastDay() {
        final LocalDate date = LocalDate.of(2025, 11, 20);
        final ImplementationGuidelinesQrBillVersion qrBillVersion = ImplementationGuidelinesQrBillVersion.forDate(date);
        assertEquals(ImplementationGuidelinesQrBillVersion.V2_2, qrBillVersion);
    }

    @Test
    public void shouldBeV2_3_firstDay() {
        final LocalDate date = LocalDate.of(2025, 11, 21);
        final ImplementationGuidelinesQrBillVersion qrBillVersion = ImplementationGuidelinesQrBillVersion.forDate(date);
        assertEquals(ImplementationGuidelinesQrBillVersion.V2_3, qrBillVersion);
    }
    @Test
    public void shouldBeV2_3_2026() {
        final LocalDate date = LocalDate.of(2026, 2, 1);
        final ImplementationGuidelinesQrBillVersion qrBillVersion = ImplementationGuidelinesQrBillVersion.forDate(date);
        assertEquals(ImplementationGuidelinesQrBillVersion.V2_3, qrBillVersion);
    }
}
