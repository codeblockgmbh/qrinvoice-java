/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.alternativeschemes.ebill;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EBillTypeTest {
    @Test
    public void testParseEmail() {
        final EBill ebill = EBillType.getInstance().parse("eBill/B/peter@sample.ch/10201409");
        assertEquals(Type.BILL, ebill.getType());
        assertEquals("peter@sample.ch", ebill.getRecipientEmailAddress());
        assertNull(ebill.getRecipientId());
        assertNull(ebill.getRecipientEnterpriseIdentificationNumber());
        assertEquals("10201409", ebill.getReferencedBill());
    }

    @Test
    public void testParseBillRecipientId() {
        final EBill ebill = EBillType.getInstance().parse("eBill/R/41010560425610173/10201409");
        assertEquals(Type.REMINDER, ebill.getType());
        assertNull(ebill.getRecipientEmailAddress());
        assertEquals("41010560425610173", ebill.getRecipientId());
        assertNull(ebill.getRecipientEnterpriseIdentificationNumber());
        assertEquals("10201409", ebill.getReferencedBill());
    }

    @Test
    public void testParseUid() {
        final EBill ebill = EBillType.getInstance().parse("eBill/B/CHE123456789");
        assertEquals(Type.BILL, ebill.getType());
        assertNull(ebill.getRecipientEmailAddress());
        assertNull(ebill.getRecipientId());
        assertEquals("CHE123456789", ebill.getRecipientEnterpriseIdentificationNumber());
        assertNull(ebill.getReferencedBill());
    }
}
