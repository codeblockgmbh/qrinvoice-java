/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.builder;

import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.mapper.ModelToSwissPaymentsCodeMapper;
import ch.codeblock.qrinvoice.model.validation.ValidationUtilsTest;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.IntStream;

public class QrInvoiceBuilderMaxLengthsTest {
    @Before
    public void init() {
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "true");
    }

    @Test
    public void createValidateFluent() {
        final QrInvoice qrInvoice = createFluent();
        final String spc = ModelToSwissPaymentsCodeMapper.create().map(qrInvoice).toSwissPaymentsCodeString();
        System.out.println(spc);
    }


    private QrInvoice createFluent() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012)
                .paymentAmountInformation(p -> p
                        .chf(654321949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name(generate(70))
                        .streetName(generate(70))
                        .houseNumber(generate(16))
                        .postalCode(generate(16))
                        .city(generate(35))
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .structuredAddress()
                        .name(generate(70))
                        .streetName(generate(70))
                        .houseNumber(generate(16))
                        .postalCode(generate(16))
                        .city(generate(35))
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name(generate(70))
                        .streetName(generate(70))
                        .houseNumber(generate(16))
                        .postalCode(generate(16))
                        .city(generate(35))
                        .country("CH")
                )
                .paymentReference(r -> r
                        // SCOR is the longest type, however QR allows to use a longer reference number
                        .qrReference("110001234560000000000813457")
                )
                .additionalInformation(a -> a
                        // together max 140 chars
                        .unstructuredMessage(generate(70))
                        .billInformation("//CB/" + generate(70 - "//CB/".length()))
                )
                .alternativeSchemeParameters(Arrays.asList(generate(100), generate(100)))
                .build();
    }

    private String generate(int length) {
        final char[] chars = ValidationUtilsTest.getAllAllowedCharacters().toCharArray();
        System.out.println(chars);

        final StringBuilder sb = new StringBuilder(length);
        IntStream.range(0, length).forEach(value -> sb.append(chars[value % chars.length]));
        System.out.println(sb.toString() + " <-> " + sb.toString().length());
        return sb.toString();
    }

}
