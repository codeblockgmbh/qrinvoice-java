/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class AlternativeSchemesUtilsTest {

    @Test
    public void parseForOutput1() {
        final AlternativeSchemesUtils.AlternativeSchemePair av = AlternativeSchemesUtils.parseForOutput("eBill/B/john.doe@example.com");
        assertTrue(av.hasName());
        assertTrue(av.hasValue());
        assertFalse(av.isEmpty());
        assertEquals("eBill", av.getName());
        assertEquals("/B/john.doe@example.com", av.getValue());
    }

    @Test
    public void parseForOutput2() {
        final AlternativeSchemesUtils.AlternativeSchemePair av = AlternativeSchemesUtils.parseForOutput("cdbk/some/values");
        assertTrue(av.hasName());
        assertTrue(av.hasValue());
        assertFalse(av.isEmpty());
        assertEquals("cdbk", av.getName());
        assertEquals("/some/values", av.getValue());
    }

    @Test
    public void parseForOutputValueOnly() {
        final AlternativeSchemesUtils.AlternativeSchemePair av = AlternativeSchemesUtils.parseForOutput("XY;XYService;54321");
        assertFalse(av.hasName());
        assertTrue(av.hasValue());
        assertFalse(av.isEmpty());
        assertNull(av.getName());
        assertEquals("XY;XYService;54321", av.getValue());
    }

    @Test
    public void parseForOutputNull() {
        final AlternativeSchemesUtils.AlternativeSchemePair av = AlternativeSchemesUtils.parseForOutput(null);
        assertFalse(av.hasName());
        assertFalse(av.hasValue());
        assertTrue(av.isEmpty());
        assertNull(av.getName());
        assertNull(av.getValue());
    }
}
