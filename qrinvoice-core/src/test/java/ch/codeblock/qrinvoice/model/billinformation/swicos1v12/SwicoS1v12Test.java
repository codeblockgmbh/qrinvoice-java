/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.billinformation.swicos1v12;

import ch.codeblock.qrinvoice.model.PaymentAmountInformation;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.builder.PaymentConditionBuilder;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.builder.SwicoS1v12Builder;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.builder.VatDetailsBuilder;
import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class SwicoS1v12Test {

    @Test
    public void validateEmpty() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertTrue(result.isValid());
        assertTrue(result.isEmpty());
    }

    @Test
    public void validateValidUID() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setUidNumber("106017086");
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertTrue(result.isValid());
        assertTrue(result.isEmpty());
    }

    @Test
    public void validateInvalidUIDTooShort() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setUidNumber("42");
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertFalse(result.isValid());
        assertFalse(result.isEmpty());
    }

    @Test
    public void validateInvalidUIDPrefix() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setUidNumber("CHE-106017086");
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertFalse(result.isValid());
        assertFalse(result.isEmpty());
    }

    @Test
    public void validateInvalidUIDMwstDelimiters() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setUidNumber("CHE-106.017.086 MWST");
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertFalse(result.isValid());
        assertFalse(result.isEmpty());
    }

    @Test
    public void validateValidVatDetails() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDetails(new ArrayList<>());
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(7.7)));
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertTrue(result.isValid());
        assertTrue(result.isEmpty());
    }

    @Test
    public void validateValidVatDetailsList() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDetails(new ArrayList<>());
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(7.7), BigDecimal.valueOf(100)));
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(2.5), BigDecimal.valueOf(50)));
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertTrue(result.isValid());
        assertTrue(result.isEmpty());
    }

    @Test
    public void validateInvalidVatDetailsListMissingNetAmount() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDetails(new ArrayList<>());
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(7.7))); // net amount missing
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(2.5), BigDecimal.valueOf(50)));
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertFalse(result.isValid());
        assertFalse(result.isEmpty());
        assertTrue(result.getErrors().stream()
                .map(ve -> ve.getErrorMessageKeys().contains("validation.error.paymentReference.additionalInformation.billInformation.swicos1v12.vatdetails.listCondition"))
                .findAny().orElse(false));
    }

    @Test
    public void validateInvalidVatDetailsSingleVatDetails() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDetails(new ArrayList<>());
        s1.getVatDetails().add(new VatDetails(null));
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertFalse(result.isValid());
        assertFalse(result.isEmpty());
        assertTrue(result.getErrors().stream()
                .map(ve -> ve.getErrorMessageKeys().contains("validation.error.paymentReference.additionalInformation.billInformation.swicos1v12.vatdetails.singleCondition"))
                .findAny().orElse(false));
    }

    @Test
    public void validateValidVatDateStart() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDateStart(LocalDate.now());
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertTrue(result.isValid());
        assertTrue(result.isEmpty());
    }

    @Test
    public void validateValidVatDateStartEnd() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDateStart(LocalDate.now());
        s1.setVatDateEnd(LocalDate.now().plusDays(1));
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertTrue(result.isValid());
        assertTrue(result.isEmpty());
    }

    @Test
    public void validateInvalidVatDateStartEnd() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDateStart(LocalDate.now());
        s1.setVatDateEnd(LocalDate.now().minusDays(1));
        final ValidationResult result = new ValidationResult();

        // act
        s1.validate(result);

        // assert
        assertFalse(result.isValid());
        assertFalse(result.isEmpty());
        assertTrue(result.getErrors().stream()
                .map(ve -> ve.getErrorMessageKeys().contains("validation.error.paymentReference.additionalInformation.billInformation.swicos1v12.vatdate.startafterend"))
                .findAny().orElse(false));
    }


    @Test
    public void validateValidVatDetailsGrossAmount() {
        // arrange

        final QrInvoice qrInvoice = new QrInvoice();
        final PaymentAmountInformation paymentAmountInformation = new PaymentAmountInformation();
        qrInvoice.setPaymentAmountInformation(paymentAmountInformation);

        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDetails(new ArrayList<>());
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(7.7), BigDecimal.valueOf(10))); // 10.77
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(8), BigDecimal.valueOf(10))); // 10.8
        final ValidationResult result = new ValidationResult();

        // act
        paymentAmountInformation.setAmount(BigDecimal.valueOf(21.57));
        s1.validate(result, qrInvoice);

        // assert
        assertTrue(result.isValid());
        assertTrue(result.isEmpty());

        // act
        paymentAmountInformation.setAmount(BigDecimal.valueOf(21.55)); // rounded
        s1.validate(result, qrInvoice);

        // assert
        assertTrue(result.isValid());
        assertTrue(result.isEmpty());
    }

    @Test
    public void validateInvalidVatDetailsGrossAmount() {
        // arrange

        final QrInvoice qrInvoice = new QrInvoice();
        final PaymentAmountInformation paymentAmountInformation = new PaymentAmountInformation();
        qrInvoice.setPaymentAmountInformation(paymentAmountInformation);

        final SwicoS1v12 s1 = new SwicoS1v12();
        s1.setVatDetails(new ArrayList<>());
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(7.7), BigDecimal.valueOf(10))); // 10.77
        s1.getVatDetails().add(new VatDetails(BigDecimal.valueOf(8), BigDecimal.valueOf(10))); // 10.8
        final ValidationResult result = new ValidationResult();

        // act
        paymentAmountInformation.setAmount(BigDecimal.valueOf(21.30));
        s1.validate(result, qrInvoice);

        // assert
        assertFalse(result.isValid());
        assertFalse(result.isEmpty());
        assertTrue(result.getErrors().stream()
                .map(ve -> ve.getErrorMessageKeys().contains("validation.error.paymentReference.additionalInformation.billInformation.swicos1v12.vatdetails.amountmismatch"))
                .findAny().orElse(false));

    }

    @Test
    public void createFullExample() {
        // see https://www.swiss-qr-invoice.org/downloads/qr-bill-s1-syntax-de.pdf
        final SwicoS1v12 s1 =
                SwicoS1v12Builder.create()
                        .invoiceReference("10201409") // Tag 10 - any string
                        .invoiceDate(LocalDate.of(19, 5, 12)) // Tag 11 - 12.05.2019
                        .customerReference("1400.000-53") // Tag 20 - any string
                        .uidNumber("106017086") // Tag 30 - UID-Nr. without pre- and suffix or delimiters - CHE-106.017.086 MWST
                        .vatDateStart(LocalDate.of(2018,5,8)) // Tag 31 - 08.05.2018
                        .vatDetails(
                                // Tag 32 - 7.7% VAT
                                VatDetailsBuilder.create().taxPercentage(7.7).build()
                        )
                        .paymentConditions(
                                // Tag 40 - 2% cash discount when payed within 10 days (2% skonto), afterwards 0% and amount payable within 30 days
                                PaymentConditionBuilder.create().cashDiscountPercentage(2.0).eligiblePaymentPeriodDays(10).build(),
                                PaymentConditionBuilder.create().cashDiscountPercentage(0).eligiblePaymentPeriodDays(30).build()
                        )
                        .build();
        final String billInformation = s1.toBillInformationString();
        assertEquals("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30", billInformation);
    }


    @Test
    public void testMinimal() {
        // arrange
        final SwicoS1v12 s1 = new SwicoS1v12();

        // act & assert
        assertNull(s1.getPaymentConditions());
        assertNull(s1.getDefaultPaymentPeriod());
        assertNull(s1.getDefaultPaymentPeriodDays());
        assertNull(s1.getInvoiceDate());
        assertNull(s1.getCustomerReference());
        assertNull(s1.getUidNumber());
        assertNull(s1.getInvoiceDate());
        assertNull(s1.getVatDateStart());
        assertNull(s1.getVatDateEnd());
        assertNull(s1.getVatDetails());
    }

}
