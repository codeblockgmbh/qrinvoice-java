/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.parser;

import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.alternativeschemes.RawAlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill;
import org.junit.Test;

import static org.junit.Assert.*;

public class AlternativeSchemeParameterParserTest {

    @Test
    public void parseEmpty() {
        assertNull(AlternativeSchemeParameterParser.create().parseAlternativeSchemeParameter(null));
        assertNull(AlternativeSchemeParameterParser.create().parseAlternativeSchemeParameter(""));
    }

    @Test
    public void parseEBill() {
        final String input = "eBill/B/CHE123456789";

        final AlternativeSchemeParameter alternativeSchemeParameter = AlternativeSchemeParameterParser.create().parseAlternativeSchemeParameter(input);
        assertTrue(alternativeSchemeParameter instanceof EBill);
        assertEquals(input, alternativeSchemeParameter.toAlternativeSchemeParameterString());
    }

    @Test
    public void parseRawStructured() {
        final String input = "unknown/na";
        final AlternativeSchemeParameter alternativeSchemeParameter = AlternativeSchemeParameterParser.create().parseAlternativeSchemeParameter(input);
        assertTrue(alternativeSchemeParameter instanceof RawAlternativeSchemeParameter);
        assertEquals(input, alternativeSchemeParameter.toAlternativeSchemeParameterString());
    }

    @Test
    public void parseRawUnstructured() {
        final String input = "na";
        final AlternativeSchemeParameter alternativeSchemeParameter = AlternativeSchemeParameterParser.create().parseAlternativeSchemeParameter(input);
        assertTrue(alternativeSchemeParameter instanceof RawAlternativeSchemeParameter);
        assertEquals(input, alternativeSchemeParameter.toAlternativeSchemeParameterString());
    }
}
