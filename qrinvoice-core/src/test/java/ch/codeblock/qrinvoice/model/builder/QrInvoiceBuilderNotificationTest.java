/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.builder;

import ch.codeblock.qrinvoice.model.*;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class QrInvoiceBuilderNotificationTest {

    @Test
    public void createDoNotUseForPayment() {
        final QrInvoice qrInvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .doNotUseForPayment()
                .build();


        final Header header = qrInvoice.getHeader();
        assertEquals("SPC", header.getQrType());
        assertEquals(200, header.getVersion());
        assertEquals(1, header.getCodingType());

        final PaymentAmountInformation paymentAmountInformation = qrInvoice.getPaymentAmountInformation();
        assertEquals(BigDecimal.ZERO, paymentAmountInformation.getAmount());
        assertEquals("CHF", paymentAmountInformation.getCurrency().getCurrencyCode());

        final CreditorInformation creditorInformation = qrInvoice.getCreditorInformation();
        assertEquals("CH3709000000304442225", creditorInformation.getIban());

        final Creditor creditor = creditorInformation.getCreditor();
        assertEquals(AddressType.STRUCTURED, creditor.getAddressType());
        assertEquals("Robert Schneider AG", creditor.getName());
        assertEquals("Rue du Lac", creditor.getStreetName());
        assertEquals("1268", creditor.getHouseNumber());
        assertEquals("2501", creditor.getPostalCode());
        assertEquals("Biel", creditor.getCity());
        assertEquals("CH", creditor.getCountry());

        assertNull(qrInvoice.getUltimateCreditor());
        assertNull(qrInvoice.getUltimateDebtor());

        final PaymentReference paymentReference = qrInvoice.getPaymentReference();
        assertEquals("NON", paymentReference.getReferenceType().getReferenceTypeCode());
        assertNull(paymentReference.getReference());

        final AdditionalInformation additionalInformation = paymentReference.getAdditionalInformation();
        assertEquals("NICHT ZUR ZAHLUNG VERWENDEN", additionalInformation.getUnstructuredMessage());
        assertEquals("EPD", additionalInformation.getTrailer());
        assertNull(additionalInformation.getBillInformation());

        assertNull(qrInvoice.getAlternativeSchemes());
    }

    @Test
    public void createDoNotUseForPaymentEnglishPreFilledValues() {
        final QrInvoice qrInvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .paymentAmountInformation(p -> p.chf(0.0))
                .additionalInformation(a -> a.unstructuredMessage("DO NOT USE FOR PAYMENT"))
                .doNotUseForPayment(Locale.ENGLISH)
                .build();


        final PaymentAmountInformation paymentAmountInformation = qrInvoice.getPaymentAmountInformation();
        assertEquals(BigDecimal.ZERO, paymentAmountInformation.getAmount());
        assertEquals("CHF", paymentAmountInformation.getCurrency().getCurrencyCode());

        final AdditionalInformation additionalInformation = qrInvoice.getPaymentReference().getAdditionalInformation();
        assertEquals("DO NOT USE FOR PAYMENT", additionalInformation.getUnstructuredMessage());
    }

    @Test
    public void createDoNotUseForPaymentEnglishPreFilledValuesGerman() {
        final QrInvoice qrInvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .paymentAmountInformation(p -> p.chf(0.0))
                .additionalInformation(a -> a.unstructuredMessage("NICHT ZUR ZAHLUNG VERWENDEN"))
                .doNotUseForPayment(Locale.ENGLISH)
                .build();


        final PaymentAmountInformation paymentAmountInformation = qrInvoice.getPaymentAmountInformation();
        assertEquals(BigDecimal.ZERO, paymentAmountInformation.getAmount());
        assertEquals("CHF", paymentAmountInformation.getCurrency().getCurrencyCode());

        final AdditionalInformation additionalInformation = qrInvoice.getPaymentReference().getAdditionalInformation();
        assertEquals("DO NOT USE FOR PAYMENT", additionalInformation.getUnstructuredMessage());
    }

    @Test(expected = ValidationException.class)
    public void createDoNotUseForPaymentInvalidUnstructuredText() {
        final QrInvoice qrInvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .additionalInformation(a -> a.unstructuredMessage("Invoice Nr. 42"))
                .doNotUseForPayment(Locale.ENGLISH)
                .build();
    }

    @Test(expected = ValidationException.class)
    public void createDoNotUseForPaymentInvalidAmount() {
        final QrInvoice qrInvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .paymentAmountInformation(p -> p.chf(1.0))
                .doNotUseForPayment(Locale.ENGLISH)
                .build();
    }

}
