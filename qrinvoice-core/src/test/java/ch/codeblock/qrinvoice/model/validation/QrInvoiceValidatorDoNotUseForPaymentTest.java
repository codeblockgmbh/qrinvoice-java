/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import ch.codeblock.qrinvoice.model.DoNotUseForPayment;
import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class QrInvoiceValidatorDoNotUseForPaymentTest {
    private final Logger logger = LoggerFactory.getLogger(QrInvoiceValidatorDoNotUseForPaymentTest.class);

    private QrInvoiceValidator qrInvoiceValidator = QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.latestActive());

    @Test
    public void testNotificationNonZeroAmount() {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                )
                .build();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        // when setting unstructured message to "DO NOT USE FOR PAYMENT", a validation error must occur
        qrInvoice.getPaymentReference().getAdditionalInformation().setUnstructuredMessage(DoNotUseForPayment.getUnstructuredMessage(Locale.ENGLISH));
        final ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("paymentAmountInformation.amount"));
        assertEquals(1, validationResult.getErrors().size());

        // when setting amount to zero, it is expected to become valid
        qrInvoice.getPaymentAmountInformation().setAmount(BigDecimal.ZERO);
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());
    }

    @Test
    public void testNotificationZeroAmountInvalidUnstructureMessage() {
        // check valid before modifying QrInvoice
        final QrInvoice qrInvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                )
                .build();
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        // when setting amount to zero, a validation error must occur
        qrInvoice.getPaymentAmountInformation().setAmount(BigDecimal.ZERO);
        ValidationResult validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("paymentReference.additionalInformation.unstructuredMessage"));
        assertEquals(1, validationResult.getErrors().size());

        // when setting unstructured message to "DO NOT USE FOR PAYMENT", it is expected to become valid
        qrInvoice.getPaymentReference().getAdditionalInformation().setUnstructuredMessage(DoNotUseForPayment.getUnstructuredMessage(Locale.ENGLISH));
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        qrInvoice.getPaymentReference().getAdditionalInformation().setUnstructuredMessage(DoNotUseForPayment.getUnstructuredMessage(Locale.FRENCH));
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        qrInvoice.getPaymentReference().getAdditionalInformation().setUnstructuredMessage(DoNotUseForPayment.getUnstructuredMessage(Locale.GERMAN));
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        qrInvoice.getPaymentReference().getAdditionalInformation().setUnstructuredMessage(DoNotUseForPayment.getUnstructuredMessage(Locale.ITALIAN));
        assertTrue(qrInvoiceValidator.validate(qrInvoice).isValid());

        // when setting unstructured message to empty string or null, it is expected to be invalid
        qrInvoice.getPaymentReference().getAdditionalInformation().setUnstructuredMessage("");
        validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("paymentReference.additionalInformation.unstructuredMessage"));
        assertEquals(1, validationResult.getErrors().size());

        // when setting unstructured message to empty string or null, it is expected to be invalid
        qrInvoice.getPaymentReference().getAdditionalInformation().setUnstructuredMessage(null);
        validationResult = qrInvoiceValidator.validate(qrInvoice);
        logger.info(validationResult.getValidationErrorSummary());
        assertTrue(validationResult.hasErrors("paymentReference.additionalInformation.unstructuredMessage"));
        assertEquals(1, validationResult.getErrors().size());

    }

}
