/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.mapper;

import ch.codeblock.qrinvoice.MappingException;
import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.model.AdditionalInformation;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.billinformation.RawBillInformation;
import ch.codeblock.qrinvoice.model.builder.AlternativeSchemesBuilder;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import ch.codeblock.qrinvoice.tests.data.TestReferenceNumbers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class ModelToSwissPaymentsCodeMapperTest {
    private QrInvoice qrinvoice;

    @Before
    public void init() {
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "true");
        this.qrinvoice = QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.QR_IBAN_CH4431999123000889012)
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateCreditor(u -> u
                        .structuredAddress()
                        .name("Robert Schneider Services Switzerland AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268/3/1")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference(TestReferenceNumbers.QRR_110001234560000000000813457)
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(null)
                .build();
    }

    @Test
    public void map() {
        final SwissPaymentsCode spc = ModelToSwissPaymentsCodeMapper.create().map(qrinvoice);
        assertEquals("SPC", spc.getQrType());
        assertEquals("0200", spc.getVersion());
        assertEquals("1", spc.getCoding());

        assertEquals("CH4431999123000889012", spc.getIban());

        assertEquals("S", spc.getCrAdrTp());
        assertEquals("Robert Schneider AG", spc.getCrName());
        assertEquals("Rue du Lac", spc.getCrStrtNmOrAdrLine1());
        assertEquals("1268", spc.getCrBldgNbOrAdrLine2());
        assertEquals("2501", spc.getCrPstCd());
        assertEquals("Biel", spc.getCrTwnNm());
        assertEquals("CH", spc.getCrCtry());


        assertEquals("S", spc.getUcrAdrTp());
        assertEquals("Robert Schneider Services Switzerland AG", spc.getUcrName());
        assertEquals("Rue du Lac", spc.getUcrStrtNmOrAdrLine1());
        assertEquals("1268/3/1", spc.getUcrBldgNbOrAdrLine2());
        assertEquals("2501", spc.getUcrPstCd());
        assertEquals("Biel", spc.getUcrTwnNm());
        assertEquals("CH", spc.getUcrCtry());

        assertEquals("1949.75", spc.getAmt());
        assertEquals("CHF", spc.getCcy());

        assertEquals("S", spc.getUdAdrTp());
        assertEquals("Pia-Maria Rutschmann-Schnyder", spc.getUdName());
        assertEquals("Grosse Marktgasse", spc.getUdStrtNmOrAdrLine1());
        assertEquals("28", spc.getUdBldgNbOrAdrLine2());
        assertEquals("9400", spc.getUdPstCd());
        assertEquals("Rorschach", spc.getUdTwnNm());
        assertEquals("CH", spc.getUdCtry());

        assertEquals("QRR", spc.getTp());
        assertEquals("110001234560000000000813457", spc.getRef());

        assertEquals("Instruction of 03.04.2019", spc.getUstrd());
        assertEquals("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30", spc.getStrdBkgInf());

        assertNull(spc.getAltPmts());
    }

    @Test
    public void mapAdditionalInformationEmpty() {
        final AdditionalInformation additionalInformation = new AdditionalInformation();
        final SwissPaymentsCode spc = new SwissPaymentsCode();
        ModelToSwissPaymentsCodeMapper.create().mapAdditionalInformation(additionalInformation, spc);
    }

    @Test
    public void mapAdditionalInformationString() {
        final String billInformation = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30";

        final AdditionalInformation additionalInformation = new AdditionalInformation();
        additionalInformation.setBillInformation(billInformation);
        final SwissPaymentsCode spc = new SwissPaymentsCode();
        ModelToSwissPaymentsCodeMapper.create().mapAdditionalInformation(additionalInformation, spc);
        assertEquals(billInformation, spc.getStrdBkgInf());
    }

    @Test
    public void mapAdditionalInformationObject() {
        final String billInformation = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30";

        final AdditionalInformation additionalInformation = new AdditionalInformation();
        additionalInformation.setBillInformationObject(new RawBillInformation(billInformation));
        final SwissPaymentsCode spc = new SwissPaymentsCode();
        ModelToSwissPaymentsCodeMapper.create().mapAdditionalInformation(additionalInformation, spc);
        assertEquals(billInformation, spc.getStrdBkgInf());
    }

    @Test
    public void mapAdditionalInformationBoth() {
        final String billInformation = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30";

        final AdditionalInformation additionalInformation = new AdditionalInformation();
        additionalInformation.setBillInformation(billInformation);
        additionalInformation.setBillInformationObject(new RawBillInformation(billInformation));
        final SwissPaymentsCode spc = new SwissPaymentsCode();
        ModelToSwissPaymentsCodeMapper.create().mapAdditionalInformation(additionalInformation, spc);
        assertEquals(billInformation, spc.getStrdBkgInf());
    }

    @Test(expected = MappingException.class)
    public void mapAdditionalInformationBothInconsistent() {
        final String billInformation = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30";

        final AdditionalInformation additionalInformation = new AdditionalInformation();
        additionalInformation.setBillInformation(billInformation);
        additionalInformation.setBillInformationObject(new RawBillInformation(billInformation + "0"));
        final SwissPaymentsCode spc = new SwissPaymentsCode();
        ModelToSwissPaymentsCodeMapper.create().mapAdditionalInformation(additionalInformation, spc);
    }

    @Test
    public void testAlternativeSchemesNull() {
        final SwissPaymentsCode spc = ModelToSwissPaymentsCodeMapper.create().map(qrinvoice);
        assertNull(spc.getAltPmts());
    }

    @Test
    public void testAlternativeSchemesEmptyList() {
        qrinvoice.setAlternativeSchemes(AlternativeSchemesBuilder.create().alternativeSchemeParameters(Collections.emptyList()).build());
        final SwissPaymentsCode spc = ModelToSwissPaymentsCodeMapper.create().map(qrinvoice);
        assertTrue(spc.getAltPmts().isEmpty());
    }

    @Test
    public void testAlternativeSchemesListBlankNullsOnly() {
        qrinvoice.setAlternativeSchemes(AlternativeSchemesBuilder.create().alternativeSchemeParameters(Arrays.asList(null, "")).build());
        final SwissPaymentsCode spc = ModelToSwissPaymentsCodeMapper.create().map(qrinvoice);
        assertTrue(spc.getAltPmts().isEmpty());
    }

    @Test
    public void testAlternativeSchemesListEmptiesOnly() {
        qrinvoice.setAlternativeSchemes(AlternativeSchemesBuilder.create().alternativeSchemeParameters(Arrays.asList("", "")).build());
        final SwissPaymentsCode spc = ModelToSwissPaymentsCodeMapper.create().map(qrinvoice);
        assertTrue(spc.getAltPmts().isEmpty());
    }

    @Test
    public void testAlternativeSchemesListBlanksOnly() {
        qrinvoice.setAlternativeSchemes(AlternativeSchemesBuilder.create().alternativeSchemeParameters(Arrays.asList(" ", "  ")).build());
        final SwissPaymentsCode spc = ModelToSwissPaymentsCodeMapper.create().map(qrinvoice);
        assertTrue(spc.getAltPmts().isEmpty());
    }

    @Test
    public void testAlternativeSchemesListBlankAndNoBlankOnly() {
        qrinvoice.setAlternativeSchemes(AlternativeSchemesBuilder.create().alternativeSchemeParameters(Arrays.asList(" ", "  ")).build());
        final SwissPaymentsCode spc = ModelToSwissPaymentsCodeMapper.create().map(qrinvoice);
        assertTrue(spc.getAltPmts().isEmpty());
    }

    @After
    public void after() {
        System.setProperty(SystemProperties.UNLOCK_ULTIMATE_CREDITOR, "false");
    }

}
