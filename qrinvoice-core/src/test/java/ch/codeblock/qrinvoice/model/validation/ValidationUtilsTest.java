/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.validation;

import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class ValidationUtilsTest {
    private static final Pattern VALID_CHARACTERS_PATTERN = Pattern.compile(String.format("([%s]*)", Pattern.quote(SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2)));
    private final ImplementationGuidelinesQrBillVersion qrBillVersion = ImplementationGuidelinesQrBillVersion.V2_2;

    @Test
    public void testValidateLength() throws Exception {
        assertFalse(ValidationUtils.validateLength(null, 0, 10, false));
        assertTrue(ValidationUtils.validateLength("", 0, 10, false));
        assertFalse(ValidationUtils.validateLength("", 1, 10, false));
        assertTrue(ValidationUtils.validateLength("a", 1, 10, false));
        assertTrue(ValidationUtils.validateLength("abc", 1, 10, false));
        assertTrue(ValidationUtils.validateLength("abcdefghij", 1, 10, false));
        assertFalse(ValidationUtils.validateLength("abcdefghijk", 1, 10, false));
    }

    @Test
    public void testValidateLengthOptional() throws Exception {
        assertTrue(ValidationUtils.validateLength(null, 0, 10, true));
        assertTrue(ValidationUtils.validateLength("", 0, 10, true));
        assertTrue(ValidationUtils.validateLength("", 1, 10, true));
        assertTrue(ValidationUtils.validateLength("a", 1, 10, true));
        assertFalse(ValidationUtils.validateLength("a", 2, 10, true));
        assertTrue(ValidationUtils.validateLength("abc", 1, 10, true));
        assertTrue(ValidationUtils.validateLength("abcdefghij", 1, 10, true));
        assertFalse(ValidationUtils.validateLength("abcdefghijk", 1, 10, true));
    }


    @Test
    public void testValidateRange() throws Exception {
        assertTrue(ValidationUtils.validateRange(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.TEN, false));
        assertTrue(ValidationUtils.validateRange(BigDecimal.ONE, BigDecimal.ZERO, BigDecimal.TEN, false));
        assertTrue(ValidationUtils.validateRange(BigDecimal.TEN, BigDecimal.ZERO, BigDecimal.TEN, false));
        assertFalse(ValidationUtils.validateRange(null, BigDecimal.ZERO, BigDecimal.TEN, false));
        assertTrue(ValidationUtils.validateRange(null, BigDecimal.ZERO, BigDecimal.TEN, true));
        assertFalse(ValidationUtils.validateRange(BigDecimal.valueOf(-1.0), BigDecimal.ZERO, BigDecimal.TEN, false));
        assertFalse(ValidationUtils.validateRange(BigDecimal.valueOf(-1.0), BigDecimal.ZERO, BigDecimal.TEN, true));
        assertFalse(ValidationUtils.validateRange(BigDecimal.valueOf(11), BigDecimal.ZERO, BigDecimal.TEN, false));
        assertFalse(ValidationUtils.validateRange(BigDecimal.valueOf(11), BigDecimal.ZERO, BigDecimal.TEN, true));
    }


    @Test
    public void testValidLatinCharacters() {
        assertTrue("Only latin characters are valid", ValidationUtils.validateCharacters(" - abcdefghijklmnopqerstufvwxyzälüç123456780;éàèÀÉÈÜÄÖ, !", qrBillVersion).isValid());
        assertTrue("All latin characters are valid", ValidationUtils.validateCharacters(getAllAllowedCharacters(), qrBillVersion).isValid());
        assertTrue("Fix string with all allowed characters", ValidationUtils.validateCharacters(" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_`abcdefghijklmnopqrstuvwxyz{}~£´ÀÁÂÄÇÈÉÊËÌÍÎÏÑÒÓÔÖÙÚÛÜßàáâäçèéêëìíîïñòóôö÷ùúûüý", qrBillVersion).isValid());
        assertFalse("Even a single encoding stranger character is not permitted", ValidationUtils.validateCharacters("abcdefg - ж - hijkl", qrBillVersion).isValid());
        assertFalse("cyrillic characters are not valid", ValidationUtils.validateCharacters("жф", qrBillVersion).isValid());
        assertFalse("japanese characters are not valid", ValidationUtils.validateCharacters("あま", qrBillVersion).isValid());
        assertFalse("chinese characters are not valid", ValidationUtils.validateCharacters("诶比", qrBillVersion).isValid());
        assertFalse("korean characters are not valid", ValidationUtils.validateCharacters("ㅂㅁ", qrBillVersion).isValid());
        assertFalse("arabic characters are not valid", ValidationUtils.validateCharacters("ﺐﺶ", qrBillVersion).isValid());
    }

    @Test
    public void testAllowedCharacters() {
        getAllAllowedCharacters().chars().forEach(value -> {
            assertTrue("Characters '" + ((char) value) + "' is not in fix list of valid characters", SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2.indexOf(value) > -1);
        });
    }

    @Test
    public void testLineBreakValidation() {
        assertFalse("Line breaks are note permitted as characters in elements", ValidationUtils.validateCharacters("\n", qrBillVersion).isValid());
        assertFalse("Line breaks are note permitted as characters in elements", ValidationUtils.validateCharacters("\r", qrBillVersion).isValid());
        assertFalse("Line breaks are note permitted as characters in elements", ValidationUtils.validateCharacters("\r\n", qrBillVersion).isValid());
    }

    @Test
    public void testCyrillicChar() {

        final CharacterValidationResult characterValidationResult = ValidationUtils.validateCharacters("abcdefg-ж-hijkl", qrBillVersion);
        assertTrue(characterValidationResult.hasInvalidCharacters());
        assertEquals("Invalid character(s) 'ж' at index 8 with a length of 1 (context: 'efg-<ж>-hij')", characterValidationResult.getSummary());
    }

    @Test
    public void testEuroSign() {
        final CharacterValidationResult characterValidationResult = ValidationUtils.validateCharacters("Kostet 54€", qrBillVersion);
        assertTrue(characterValidationResult.hasInvalidCharacters());
        assertFalse(characterValidationResult.isValid());
        assertEquals("Invalid character(s) '€' at index 9 with a length of 1 (context: 't 54<€>')", characterValidationResult.getSummary());
    }

    @Test
    public void testNull() {
        final CharacterValidationResult characterValidationResult = ValidationUtils.validateCharacters(null, qrBillVersion);
        assertTrue(characterValidationResult.isValid());
        assertFalse(characterValidationResult.hasInvalidCharacters());
    }

    @Test
    public void testEmptyString() {
        final CharacterValidationResult characterValidationResult = ValidationUtils.validateCharacters("", qrBillVersion);
        assertTrue(characterValidationResult.isValid());
        assertFalse(characterValidationResult.hasInvalidCharacters());
    }

    @Test
    public void testBlankString() {
        final CharacterValidationResult characterValidationResult = ValidationUtils.validateCharacters("    ", qrBillVersion);
        assertTrue(characterValidationResult.isValid());
        assertFalse(characterValidationResult.hasInvalidCharacters());
    }


    public static String getAllAllowedCharacters() {
        final StringBuilder chars = new StringBuilder();
        for (int i = Character.MIN_VALUE; i < Character.MAX_VALUE; i++) {
            final String s = Character.toString((char) i);
            if (VALID_CHARACTERS_PATTERN.matcher(s).matches()) {
                chars.append(s);
            }
        }

        // move whitespace to second position due to trimable validation ' !"' -> '! "'
        final char c = chars.charAt(0);
        chars.setCharAt(0, chars.charAt((1)));
        chars.setCharAt(1, c);

        return chars.toString();
    }
}
