/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above 
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.builder;

import ch.codeblock.qrinvoice.model.*;
import ch.codeblock.qrinvoice.tests.data.TestIbans;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class QrInvoiceBuilderMinimalTest {

    @Test
    public void createValidateFluent() {
        validate(create());
    }

    private void validate(QrInvoice qrInvoice) {
        final Header header = qrInvoice.getHeader();
        assertEquals("SPC", header.getQrType());
        assertEquals(200, header.getVersion());
        assertEquals(1, header.getCodingType());

        final PaymentAmountInformation paymentAmountInformation = qrInvoice.getPaymentAmountInformation();
        assertNull(paymentAmountInformation.getAmount());
        assertEquals("EUR", paymentAmountInformation.getCurrency().getCurrencyCode());

        final CreditorInformation creditorInformation = qrInvoice.getCreditorInformation();
        assertEquals("CH3709000000304442225", creditorInformation.getIban());

        final Creditor creditor = creditorInformation.getCreditor();
        assertEquals(AddressType.STRUCTURED, creditor.getAddressType());
        assertEquals("Salvation Army Foundation Switzerland", creditor.getName());
        assertNull(creditor.getStreetName());
        assertNull(creditor.getHouseNumber());
        assertEquals("3000", creditor.getPostalCode());
        assertEquals("Bern", creditor.getCity());
        assertEquals("CH", creditor.getCountry());


        final UltimateCreditor ultimateCreditor = qrInvoice.getUltimateCreditor();
        assertNull(ultimateCreditor);


        final UltimateDebtor ultimateDebtor = qrInvoice.getUltimateDebtor();
        assertNull(ultimateDebtor);

        final PaymentReference paymentReference = qrInvoice.getPaymentReference();
        assertEquals("NON", paymentReference.getReferenceType().getReferenceTypeCode());
        assertNull(paymentReference.getReference());
        assertNull(paymentReference.getAdditionalInformation().getUnstructuredMessage());
        assertEquals("EPD", paymentReference.getAdditionalInformation().getTrailer());
        assertNull(paymentReference.getAdditionalInformation().getBillInformation());

        assertNull(qrInvoice.getAlternativeSchemes());
    }

    private QrInvoice create() {
        return QrInvoiceBuilder
                .create()
                .creditorIBAN(TestIbans.IBAN_CH3709000000304442225)
                .paymentAmountInformation(PaymentAmountInformationBuilder::eur
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Salvation Army Foundation Switzerland")
                        .postalCode("3000")
                        .city("Bern")
                        .country("CH")
                )
                .build();
    }

}
