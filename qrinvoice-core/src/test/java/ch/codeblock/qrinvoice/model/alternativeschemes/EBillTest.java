/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.model.alternativeschemes;

import ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill;
import ch.codeblock.qrinvoice.model.alternativeschemes.ebill.Type;
import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import org.junit.Test;

import static org.junit.Assert.*;

public class EBillTest {
    @Test
    public void toAlternativeSchemeParameterStringMail() {
        final EBill eBill = new EBill();
        eBill.setType(Type.BILL);
        eBill.setRecipientEmailAddress("peter@sample.ch");
        eBill.setReferencedBill("10201409");
        assertTrue(eBill.validate().isValid());
        assertEquals("eBill/B/peter@sample.ch/10201409", eBill.toAlternativeSchemeParameterString());

        eBill.setReferencedBill(null);
        assertTrue(eBill.validate().isValid());
        assertEquals("eBill/B/peter@sample.ch", eBill.toAlternativeSchemeParameterString());

        eBill.setReferencedBill(" ");
        assertTrue(eBill.validate().isValid());
        assertEquals("eBill/B/peter@sample.ch", eBill.toAlternativeSchemeParameterString());
    }

    @Test
    public void toAlternativeSchemeParameterStringRecipientId() {
        final EBill eBill = new EBill();
        eBill.setType(Type.REMINDER);
        eBill.setRecipientId("41010560425610173");
        eBill.setReferencedBill("10201409");
        assertTrue(eBill.validate().isValid());
        assertEquals("eBill/R/41010560425610173/10201409", eBill.toAlternativeSchemeParameterString());
    }

    @Test
    public void toAlternativeSchemeParameterStringEnterpriseIdentificationNumber() {
        final EBill eBill = new EBill();
        eBill.setType(Type.BILL);
        eBill.setRecipientEnterpriseIdentificationNumber("CHE123456789");
        eBill.setReferencedBill("10201409");
        assertTrue(eBill.validate().isValid());
        assertEquals("eBill/B/CHE123456789/10201409", eBill.toAlternativeSchemeParameterString());

        eBill.setReferencedBill(null);
        assertTrue(eBill.validate().isValid());
        assertEquals("eBill/B/CHE123456789", eBill.toAlternativeSchemeParameterString());
    }

    @Test
    public void testInvalidMail() {
        final EBill eBill = new EBill();
        eBill.setType(Type.BILL);
        eBill.setRecipientEmailAddress("no-mali");
        eBill.setReferencedBill("10201409");
        final ValidationResult result = eBill.validate();
        assertFalse(result.isValid());
        assertEquals(1, result.getErrors().size());
        assertTrue(result.getErrors().stream()
                .map(ve -> ve.getErrorMessageKeys().contains("validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.emailAddress"))
                .findAny().orElse(false));
    }

    @Test
    public void testInvalidUid() {
        final EBill eBill = new EBill();
        eBill.setType(Type.BILL);
        eBill.setRecipientEnterpriseIdentificationNumber("CHE42");
        final ValidationResult result = eBill.validate();
        assertFalse(result.isValid());
        assertEquals(1, result.getErrors().size());
        assertTrue(result.getErrors().stream()
                .map(ve -> ve.getErrorMessageKeys().contains("validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.enterpriseIdentificationNumber"))
                .findAny().orElse(false));
    }

    @Test
    public void testInvalidBillRecipientId() {
        final EBill eBill = new EBill();
        eBill.setType(Type.BILL);
        eBill.setRecipientId("410105604256101739");
        final ValidationResult result = eBill.validate();
        assertFalse(result.isValid());
        assertEquals(1, result.getErrors().size());
        assertTrue(result.getErrors().stream()
                .map(ve -> ve.getErrorMessageKeys().contains("validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.billRecipientId"))
                .findAny().orElse(false));
    }

    @Test
    public void testInvalidEmptyType() {
        final EBill eBill = new EBill();
        eBill.setRecipientEnterpriseIdentificationNumber("CHE123456789");
        final ValidationResult result = eBill.validate();
        assertFalse(result.isValid());
        assertEquals(1, result.getErrors().size());
        assertTrue(result.getErrors().stream()
                .map(ve -> ve.getErrorMessageKeys().contains("validation.error.alternativeSchemes.alternativeSchemeParameters.ebill.type"))
                .findAny().orElse(false));
    }
}
