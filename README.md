# QR Invoice Library for Java

A Java library for the creation and processing of Swiss QR Bills.

Please visit [www.qr-invoice.ch](https://www.qr-invoice.ch) and [docs.qr-invoice.ch](https://docs.qr-invoice.ch) for more details regarding functionality and licensing.
