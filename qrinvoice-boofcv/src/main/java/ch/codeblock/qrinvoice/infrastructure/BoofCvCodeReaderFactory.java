package ch.codeblock.qrinvoice.infrastructure;

import ch.codeblock.qrinvoice.boofcv.BoofQrCodeReader;
import ch.codeblock.qrinvoice.qrcode.IQrCodeReader;
import ch.codeblock.qrinvoice.qrcode.QrCodeLibrary;

import java.util.Collection;

public class BoofCvCodeReaderFactory implements IQrCodeReaderFactory {
    @Override
    public IQrCodeReader create() {
        return new BoofQrCodeReader();
    }

    @Override
    public boolean supports(Collection<QrCodeLibrary> libraries) {
        return QrCodeLibrary.BOOFCV.supportedBy(libraries);
    }
}
