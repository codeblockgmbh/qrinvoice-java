export function exampleOne () {
  return {
    alternativeSchemes: {
      alternativeSchemeParameters: [
        'eBill/B/john.doe@example.com',
        'cdbk/some/values'
      ]
    },
    creditorInformation: {
      creditor: {
        addressType: 'STRUCTURED',
        city: 'Biel',
        country: 'CH',
        houseNumber: '1268',
        name: 'Robert Schneider AG',
        postalCode: '2501',
        streetName: 'Rue du Lac'
      },
      iban: 'CH4431999123000889012'
    },
    paymentAmountInformation: {
      amount: 1949.75,
      currency: 'CHF'
    },
    paymentReference: {
      reference: '210000000003139471430009017',
      referenceType: 'QRR',
      additionalInformation: {
        unstructuredMessage: 'Instruction of 03.04.2019',
        billInformation: '//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30'
      }
    },
    ultimateCreditor: {
      addressType: '',
      city: '',
      country: '',
      houseNumber: '',
      name: '',
      postalCode: '',
      streetName: ''
    },
    ultimateDebtor: {
      addressType: 'STRUCTURED',
      city: 'Rorschach',
      country: 'CH',
      houseNumber: '28',
      name: 'Pia-Maria Rutschmann-Schnyder',
      postalCode: '9400',
      streetName: 'Grosse Marktgasse'
    }
  }
};

export function exampleTwo () {
  return {
    alternativeSchemes: {
      alternativeSchemeParameters: [
        '', ''
      ]
    },
    creditorInformation: {
      creditor: {
        addressType: 'STRUCTURED',
        city: 'Biel',
        country: 'CH',
        houseNumber: '1268',
        name: 'Robert Schneider AG',
        postalCode: '2501',
        streetName: 'Rue du Lac'
      },
      iban: 'CH5800791123000889012'
    },
    paymentAmountInformation: {
      amount: 199.95,
      currency: 'CHF'
    },
    paymentReference: {
      reference: 'RF18539007547034',
      referenceType: 'SCOR',
      additionalInformation: {
        unstructuredMessage: '',
        billInformation: ''
      }
    },
    ultimateCreditor: {
      addressType: '',
      city: '',
      country: '',
      houseNumber: '',
      name: '',
      postalCode: '',
      streetName: ''
    },
    ultimateDebtor: {
      addressType: 'STRUCTURED', // is COMBINED in spec
      city: 'Rorschach',
      country: 'CH',
      houseNumber: '28',
      name: 'Pia-Maria Rutschmann-Schnyder',
      postalCode: '9400',
      streetName: 'Grosse Marktgasse'
    }
  }
};

export function exampleThree () {
  return {
    alternativeSchemes: {
      alternativeSchemeParameters: [
        '', ''
      ]
    },
    creditorInformation: {
      creditor: {
        addressType: 'STRUCTURED',
        city: 'Biel',
        country: 'CH',
        houseNumber: '1268/2/22',
        name: 'Robert Schneider AG',
        postalCode: '2501',
        streetName: 'Rue du Lac'
      },
      iban: 'CH3709000000304442225'
    },
    paymentAmountInformation: {
      amount: 3949.75,
      currency: 'CHF'
    },
    paymentReference: {
      reference: '',
      referenceType: 'NON',
      additionalInformation: {
        unstructuredMessage: 'Rechnung Nr. 3139 für Gartenarbeiten und Entsorgung Schnittmaterial',
        billInformation: ''
      }
    },
    ultimateCreditor: {
      addressType: '',
      city: '',
      country: '',
      houseNumber: '',
      name: '',
      postalCode: '',
      streetName: ''
    },
    ultimateDebtor: {
      addressType: 'STRUCTURED',
      city: 'Rorschach',
      country: 'CH',
      houseNumber: '28',
      name: 'Pia-Maria Rutschmann-Schnyder',
      postalCode: '9400',
      streetName: 'Grosse Marktgasse'
    }
  }
};

export function exampleFour () {
  return {
    alternativeSchemes: {
      alternativeSchemeParameters: [
        '', ''
      ]
    },
    creditorInformation: {
      creditor: {
        addressType: 'STRUCTURED',
        city: 'Berne',
        country: 'CH',
        houseNumber: '',
        name: 'Salvation Army Foundation Switzerland',
        postalCode: '3000',
        streetName: ''
      },
      iban: 'CH3709000000304442225'
    },
    paymentAmountInformation: {
      amount: '',
      currency: 'CHF'
    },
    paymentReference: {
      reference: '',
      referenceType: 'NON',
      additionalInformation: {
        unstructuredMessage: 'Donation to the Winterfest Campaign',
        billInformation: ''
      }
    },
    ultimateCreditor: {
      addressType: '',
      city: '',
      country: '',
      houseNumber: '',
      name: '',
      postalCode: '',
      streetName: ''
    },
    ultimateDebtor: {
      addressType: '',
      city: '',
      country: '',
      houseNumber: '',
      name: '',
      postalCode: '',
      streetName: ''
    }
  }
};

export function exampleFive () {
  return {
    alternativeSchemes: {
      alternativeSchemeParameters: [
        '', ''
      ]
    },
    creditorInformation: {
      creditor: {
        addressType: 'STRUCTURED',
        city: 'Berne',
        country: 'CH',
        houseNumber: '',
        name: 'Salvation Army Foundation Switzerland',
        postalCode: '3000',
        streetName: ''
      },
      iban: 'CH4431999123000889012'
    },
    paymentAmountInformation: {
      amount: '',
      currency: 'CHF'
    },
    paymentReference: {
      reference: '210000000003139471430009017',
      referenceType: 'QRR',
      additionalInformation: {
        unstructuredMessage: 'Donation to the Winterfest Campaign',
        billInformation: ''
      }
    },
    ultimateCreditor: {
      addressType: '',
      city: '',
      country: '',
      houseNumber: '',
      name: '',
      postalCode: '',
      streetName: ''
    },
    ultimateDebtor: {
      addressType: 'STRUCTURED',
      city: 'Rorschach',
      country: 'CH',
      houseNumber: '28',
      name: 'Pia-Maria Rutschmann-Schnyder',
      postalCode: '9400',
      streetName: 'Grosse Marktgasse'
    }
  }
};

export function exampleEmpty () {
  return {
    alternativeSchemes: {
      alternativeSchemeParameters: [
        '', ''
      ]
    },
    creditorInformation: {
      creditor: {
        addressType: '',
        city: '',
        country: '',
        houseNumber: '',
        name: '',
        postalCode: '',
        streetName: ''
      },
      iban: ''
    },
    paymentAmountInformation: {
      amount: '',
      currency: ''
    },
    paymentReference: {
      reference: '',
      referenceType: '',
      additionalInformation: {
        unstructuredMessage: '',
        billInformation: ''
      }
    },
    ultimateCreditor: {
      addressType: '',
      city: '',
      country: '',
      houseNumber: '',
      name: '',
      postalCode: '',
      streetName: ''
    },
    ultimateDebtor: {
      addressType: '',
      city: '',
      country: '',
      houseNumber: '',
      name: '',
      postalCode: '',
      streetName: ''
    }
  }
};
