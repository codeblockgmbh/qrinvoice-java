import de from './de'
import fr from './fr'
import it from './it'
import en from './en'
import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

// Ready translated locale messages
const messages = {
  de: de,
  fr: fr,
  it: it,
  en: en
}

function getUserLanguage () {
  let lang = window.navigator.languages ? window.navigator.languages[0] : null
  lang = lang || window.navigator.language || window.navigator.browserLanguage || window.navigator.userLanguage
  if (lang.indexOf('-') !== -1) {
    lang = lang.split('-')[0]
  }

  if (lang.indexOf('_') !== -1) {
    lang = lang.split('_')[0]
  }
  if (['de', 'fr', 'it', 'en'].indexOf(lang)) {
    return lang
  } else {
    return 'de'
  }
}

// Create VueI18n instance with options
export const i18n = new VueI18n({
  locale: getUserLanguage(), // set local
  messages // set locale messages
})

export function setI18nLanguage (lang) {
  i18n.locale = lang
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

export default i18n
