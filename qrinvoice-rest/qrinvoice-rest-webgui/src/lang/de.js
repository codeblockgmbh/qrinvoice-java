const de = {
  nav: {
    productPage: 'Produktseite',
    swagger: 'API-Dokumentation',
    lang: {
      de: 'Deutsch',
      en: 'English',
      fr: 'Français',
      it: 'Italiano'
    },
    scan: 'Scan',
    utils: 'Rechnungstools',
    pdfUtils: 'PDF-Tools'
  },
  authentication: {
    title: 'Authentifizierung',
    apiKey: 'API Key',
    apiKey_tooltip: 'Geben Sie den von Codeblock erhaltenen API Key ein',
    apiKey_default: 'Dies ist ein API Key zu Demonstrationszwecken. Bei der Erzeugung von QR-Zahlteilen werden einzelne Werte überschrieben.'
  },
  creditor: {
    title: 'Zahlungsempfänger',
    creditorInformation: {
      iban: 'Konto',
      iban_tooltip: 'IBAN bzw. QR-IBAN des Zahlungsempfängers\n' +
        'Feste Länge: 21 alphanumerische Zeichen\n' +
        'nur IBANs mit CH- oder LI-Landescode zulässig.',
      creditor: {
        name: 'Name',
        name_tooltip: 'Name bzw. Firma des Zahlungsempfängers gemäss Kontobezeichnung.\n' +
          'Anmerkung: entspricht immer dem Kontoinhaber\n' +
          'Maximal 70 Zeichen zulässig\n' +
          'Vorname (optional, falls verfügbar) und Name oder Firmenbezeichnung',
        street: 'Strasse',
        street_tooltip: 'Strasse/Postfach des Zahlungsempfängers\n' +
          'Maximal 70 Zeichen zulässig\n' +
          'darf keine Haus- bzw. Gebäudenummer enthalten.',
        houseNumber: 'Hausnummer',
        houseNumber_tooltip: 'Hausnummer des Zahlungsempfängers\n' +
          'Maximal 16 Zeichen zulässig',
        postalCode: 'Postleitzahl',
        postalCode_tooltip: 'Postleitzahl des Zahlungsempfänges\n' +
          'Maximal 16 Zeichen zulässig\n' +
          'ist immer ohne vorangestellten Landescode anzugeben.',
        city: 'Ort',
        city_tooltip: 'Ort des Zahlungsempfängers\n' +
          'Maximal 35 Zeichen zulässig',
        country: 'Land',
        country_tooltip: 'Land des Zahlungsempfängers\n' +
          'Zweistelliger Landescode gemäss ISO 3166-1'
      }
    }
  },
  ultimateCreditor: {
    title: 'Endgültiger Zahlungsempfänger',
    sameAsCreditor: 'Identisch mit dem Zahlungsempfänger',
    enterUltimateCreditor: 'Endültigen Zahlungsempfänger eingeben',
    futureUse: 'Die gesamte Datengruppe darf vorerst nicht befüllt werden',
    name: 'Name',
    name_tooltip: 'Name bzw. Firma des endgültigen Zahlungsempfängers\n' +
      'Maximal 70 Zeichen zulässig\n' +
      'Vorname (optional, falls verfügbar) und Name oder Firmenbezeichnung',
    street: 'Strasse',
    street_tooltip: 'Strasse/Postfach des endgültigen Zahlungsempfängers\n' +
      'Maximal 70 Zeichen zulässig\n' +
      'darf keine Haus- bzw. Gebäudenummer enthalten.',
    houseNumber: 'Hausnummer',
    houseNumber_tooltip: 'Hausnummer des endgültigen Zahlungsempfängers\n' +
      'Maximal 16 Zeichen zulässig',
    postalCode: 'Postleitzahl',
    postalCode_tooltip: 'Postleitzahl des endgültigen Zahlungsempfängers\n' +
      'Maximal 16 Zeichen zulässig\n' +
      'ist immer ohne vorangestellten Landescode anzugeben.',
    city: 'Ort',
    city_tooltip: 'Ort der endgültigen Zahlungsempfängers\n' +
      'Maximal 35 Zeichen zulässig',
    country: 'Land',
    country_tooltip: 'Land des endgültigen Zahlungsempfängers\n' +
      'Zweistelliger Landescode gemäss ISO 3166-1'
  },
  ultimateDebtor: {
    title: 'Zahlungspflichtiger',
    name: 'Name',
    name_tooltip: 'Name bzw. Firma des endgültigen Zahlungspflichtigen\n' +
      'Maximal 70 Zeichen zulässig\n' +
      'Vorname (optional, falls verfügbar) und Name oder Firmenbezeichnung',
    street: 'Strasse',
    street_tooltip: 'Strasse/Postfach des endgültigen Zahlungspflichtigen\n' +
      'Maximal 70 Zeichen zulässig\n' +
      'darf keine Haus- bzw. Gebäudenummer enthalten.',
    houseNumber: 'Hausnummer',
    houseNumber_tooltip: 'Hausnummer des endgültigen Zahlungspflichtigen\n' +
      'Maximal 16 Zeichen zulässig',
    postalCode: 'Postleitzahl',
    postalCode_tooltip: 'Postleitzahl des endgültigen Zahlungspflichtigen\n' +
      'Maximal 16 Zeichen zulässig\n' +
      'ist immer ohne vorangestellten Landescode anzugeben.',
    city: 'Ort',
    city_tooltip: 'Ort des endgültigen Zahlungspflichtigen\n' +
      'Maximal 35 Zeichen zulässig',
    country: 'Land',
    country_tooltip: 'Land des endgültigen Zahlungspflichtigen\n' +
      'Zweistelliger Landescode gemäss ISO 3166-1'
  },
  paymentAmountInformation: {
    title: 'Zahlbetragsinformation',
    amount: 'Betrag',
    amount_tooltip: 'Betrag der Zahlung\n' +
      'Das Element ist ohne führende Nullen, inklusive Dezimaltrennzeichen und zwei Nachkommastellen, anzugeben.\n' +
      'Dezimal, max. 12 Stellen zulässig, inklusive Dezimaltrennzeichen. Als Dezimaltrennzeichen ist nur das Punktzeichen («.») zulässig.',
    currency: 'Währung',
    currency_tooltip: 'Währung der Zahlung, dreistelliger alphabetischer Währungscode gemäss ISO 4217\n' +
      'Nur CHF und EUR zugelassen.'
  },
  paymentReference: {
    title: 'Zahlungsreferenz',
    referenceType: 'Referenztyp',
    referenceTypes: {
      qrr: 'QR-Referenz',
      scor: 'Creditor Reference',
      non: 'ohne Referenz'
    },
    referenceType_tooltip: 'Die folgenden Codes sind zugelassen:\n' +
      'QRR – QR-Referenz\n' +
      'SCOR – Creditor Reference (ISO 11649)\n' +
      'NON – ohne Referenz\n' +
      'Maximal vier Zeichen, alphanumerisch\n' +
      'muss bei Verwendung einer QR-IBAN den Code QRR enthalten;\n' +
      'bei Verwendung der IBAN kann entweder der Code SCOR oder NON angegeben werden',
    reference: 'Referenz',
    reference_tooltip: 'Referenznummer\n' +
      'Strukturierte Zahlungsreferenz\n' +
      'Anmerkung: Die Referenz ist entweder eine QR-Referenz oder Creditor Reference (ISO 11649)\n' +
      'Maximal 27 Zeichen, alphanumerisch\n' +
      'Muss bei Verwendung einer QR-IBAN befüllt werden.\n' +
      'QR-Referenz: 27 Zeichen, numerisch, Prüfzifferberechnung nach Modulo 10 rekursiv (27. Stelle der Referenz)\n' +
      'Creditor Reference (ISO 11649): bis 25 Zeichen, alphanumerisch.\n' +
      'Für den Referenztyp NON darf das Element nicht befüllt werden.',
    additionalInformation: {
      unstructuredMessage: 'Zusätzliche Informationen',
      unstructuredMessage_tooltip: 'Unstrukturierte Informationen können zur Angabe eines Zahlungszwecks oder für ergänzende textuelle Informationen zu Zahlungen mit strukturierter Referenz verwendet werden.\n' +
        'Maximal 140 Zeichen',
      unstructuredMessage_placeholder: 'Unstrukturierte Mitteilung',
      billInformation: 'Rechnungsinformationen',
      billInformation_tooltip: 'Rechnungsinformationen enthalten codierte Informationen für die automatisierte Verbuchung der Zahlung. Die Daten werden nicht mit der Zahlung weitergeleitet.\n' +
        'Maximal 140 Zeichen zulässig',
      billInformation_placeholder: ''
    }
  },
  alternativeSchemes: {
    title: 'Alternative Verfahren',
    alternativeSchemeParameters: 'Parameter',
    alternativeSchemeParameters_tooltip: 'Kann aktuell maximal zweimal geliefert werden. Maximal je 100 Zeichen zulässig'
  },
  output: {
    title: 'Herunterladen',
    qrCode: {
      title: 'Swiss QR Code',
      format: 'Format',
      size: 'Grösse',
      resolution: 'Auflösung'
    },
    paymentPartReceipt: {
      title: 'Zahlteil & Empfangsschein',
      fontFamily: 'Schriftart',
      format: 'Format',
      language: 'Sprache',
      languages: {
        de: 'Deutsch',
        en: 'Englisch',
        fr: 'Französisch',
        it: 'Italienisch'
      },
      pageSize: 'Seitengrösse',
      pageSizes: {
        a4: 'A4 Hochformat',
        a5: 'A5 Querformat',
        din_lang: 'DIN Lang Querformat',
        din_lang_cropped: 'DIN Lang Querformat zugeschnitten (ausser PDF)'
      },
      resolution: 'Auflösung',
      boundaryLines: 'Trennlinie',
      boundaryLinesMargins: 'Sicherheitsabstand Trennlinie',
      boundaryLineScissors: 'Scherensymbol',
      boundaryLineSeparationText: 'Abtrennungshinweis',
      additionalPrintMargin: 'Zusätzlicher Druckrand (+1mm)'
    },
    swissPaymentsCode: {
      title: 'Swiss Payments Code'
    }
  },
  actions: {
    close: 'Schliessen',
    download: 'Herunterladen',
    generate: 'Generieren',
    reset: 'Zurücksetzen',
    validate: 'Validieren'
  },
  examples: {
    title: 'Beispiele',
    apply: 'Beispiel übernehmen',
    oneShort: 'QR Referenz',
    one: 'Rechnung mit QR Referenznummer und zusätzlichen Informationen',
    twoShort: 'Creditor Reference',
    two: 'Rechnung mit Creditor Referenznummer',
    threeShort: 'Ohne Referenz',
    three: 'Rechnung ohne Referenznummer, mit zusätzlichen Informationen',
    fourShort: 'Blanko',
    four: 'Rechnung ohne Referenz, Betrag und Zahlungspflichtiger',
    fiveShort: 'Ohne Betrag',
    five: 'Rechnung ohne Betrag, mit Referenz und Zahlungspflichtiger'
  },
  preview: {
    title: 'Vorschau',
    titlePreviewGenerate: 'Vorschau / Erzeugen'
  },
  validation: {
    qrReferenceNumber: 'QR Referenznummer',
    qrIban: 'QR IBAN'
  },
  pdf: {
    merge: 'Zusammenführen',
    append: 'Anhängen'
  },
  scan: {
    file: {
      label: 'QR-Rechnung',
      type: 'Dateityp',
      placeholder: 'Datei wählen oder hier hineinziehen...',
      dropPlaceholder: 'Datei hier loslassen...'
    },
    paymentPartReceiptTitle: 'Zahlteil & Empfangsschein erzeugt aus den extrahieren Daten',
    rawData: 'Rohdaten',
    scanDocument: 'Dokument scannen'
  },
  dibito: {
    ad: 'Stellen Sie Ihre QR-Rechnungen digital per eBill zu'
  }
}

export default de
