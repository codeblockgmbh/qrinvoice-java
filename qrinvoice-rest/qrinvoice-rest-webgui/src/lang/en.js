const en = {
  nav: {
    productPage: 'Product Page',
    swagger: 'API documentation',
    lang: {
      de: 'Deutsch',
      en: 'English',
      fr: 'Français',
      it: 'Italiano'
    },
    scan: 'Scan',
    utils: 'Invoice utilities',
    pdfUtils: 'PDF utilities'
  },
  authentication: {
    title: 'Authentication',
    apiKey: 'API Key',
    apiKey_tooltip: 'Enter the API Key received from Codeblock',
    apiKey_default: 'This is an API Key for demonstration purposes. Some values are overridden during create of QR Payment Part & Receipt'
  },
  creditor: {
    title: 'Creditor',
    creditorInformation: {
      iban: 'Account',
      iban_tooltip: 'IBAN or QR-IBAN of the creditor\n' +
        'Should be 21 characters in length\n' +
        'Only IBANs with CH or LI country code permitted',
      creditor: {
        name: 'Name',
        name_tooltip: 'The creditor’s name or company according to the account name\n' +
          'Comment: always matches the account holder\n' +
          'Maximum 70 characters permitted\n' +
          'First name (optional, if available) + last name or company name',
        street: 'Street',
        street_tooltip: 'Street/P.O. box of the creditor\n' +
          'Maximum 70 characters permitted\n' +
          'may not include any house or building number.',
        houseNumber: 'House Number',
        houseNumber_tooltip: 'House number of the creditor\n' +
          'Maximum 16 characters permitted',
        postalCode: 'Postal Code',
        postalCode_tooltip: 'Postal code of the creditor\n' +
          'Maximum 16 characters permitted\n' +
          'The postal code is always to be entered without a country code prefix.',
        city: 'City',
        city_tooltip: 'City of the creditor\n' +
          'Maximum 35 characters permitted',
        country: 'Country',
        country_tooltip: 'Country of the creditor\n' +
          'Two-digit country code according to ISO 3166-1'
      }
    }
  },
  ultimateCreditor: {
    title: 'Ultimate Creditor',
    sameAsCreditor: 'Same as creditor',
    enterUltimateCreditor: 'Enter ultimate creditor',
    futureUse: 'This whole data group must not be filled in for the time being',
    name: 'Name',
    name_tooltip: 'Name or company of the ultimate creditor\n' +
      'Maximum 70 characters permitted\n' +
      'First name (optional, if available) and last name or company name.',
    street: 'Street',
    street_tooltip: 'Street/P.O. box of the ultimate creditor\n' +
      'Maximum 70 characters permitted\n' +
      'may not include any house or building number.',
    houseNumber: 'House Number',
    houseNumber_tooltip: 'House number of the ultimate creditor\n' +
      'Maximum 16 characters permitted',
    postalCode: 'Postal Code',
    postalCode_tooltip: 'Postal code of the ultimate creditor\n' +
      'Maximum 16 characters permitted\n' +
      'is always to be entered without a country code prefix.',
    city: 'City',
    city_tooltip: 'City of the ultimate creditor\n' +
      'Maximum 35 characters permitted',
    country: 'Country',
    country_tooltip: 'Country of the ultimate creditor\n' +
      'Two-digit country code according to ISO 3166-1'
  },
  ultimateDebtor: {
    title: 'Debtor',
    name: 'Name',
    name_tooltip: 'Name or company of the ultimate debtor\n' +
      'Maximum 70 characters permitted\n' +
      'first name (optional, if available) and last name or company name.',
    street: 'Street',
    street_tooltip: 'Street/P.O. Box of the ultimate debtor\n' +
      'Maximum 70 characters permitted\n' +
      'may not include any house or building number.',
    houseNumber: 'House Number',
    houseNumber_tooltip: 'House number of the ultimate debtor\n' +
      'Maximum 16 characters permitted',
    postalCode: 'Postal Code',
    postalCode_tooltip: 'Postal code of the ultimate debtor\n' +
      'Maximum 16 characters permitted\n' +
      'is always to be entered without a country code prefix.',
    city: 'City',
    city_tooltip: 'City of the ultimate debtor\n' +
      'Maximum 35 characters permitted',
    country: 'Country',
    country_tooltip: 'Country of the ultimate debtor\n' +
      'Two-digit country code according to ISO 3166-1'
  },
  paymentAmountInformation: {
    title: 'Payment',
    amount: 'Amount',
    amount_tooltip: 'The payment amount\n' +
      'The amount is to be entered without leading zeroes. It should include the decimal separator (.) and two decimal places\n' +
      'Decimal, maximum 12-digits permitted, including decimal separators. Only decimal points (".") are permitted as decimal separators.',
    currency: 'Currency',
    currency_tooltip: 'The payment currency, 3-digit alphanumeric currency code according to ISO 4217\n' +
      'Only CHF and EUR are permitted'
  },
  paymentReference: {
    title: 'Payment Reference',
    referenceType: 'Reference Type',
    referenceTypes: {
      qrr: 'QR reference',
      scor: 'Creditor reference',
      non: 'Without reference'
    },
    referenceType_tooltip: 'The following codes are permitted:\n' +
      'QRR – QR reference\n' +
      'SCOR – Creditor reference (ISO 11649)\n' +
      'NON – without reference\n' +
      'Maximum four characters, alphanumeric\n' +
      'Must contain the code QRR where a QR-IBAN is used;\n' +
      'where the IBAN is used, either the SCOR or NON code can be entered',
    reference: 'Reference',
    reference_tooltip: 'Reference number\n' +
      'Structured payment reference\n' +
      'Note: The reference is either a QR reference or a Creditor Reference (ISO 11649)\n' +
      'Maximum 27 characters, alphanumeric;\n' +
      'must be filled if a QR-IBAN is used.\n' +
      'QR reference: 27 characters, numeric, check sum calculation according to Modulo 10 recursive (27th position of the reference)\n' +
      'Creditor Reference (ISO 11649): max 25 characters, alphanumeric\n' +
      'The element may not be filled for the NON reference type',
    additionalInformation: {
      unstructuredMessage: 'Additional Info',
      unstructuredMessage_tooltip: 'Unstructured information can be used to indicate the payment purpose or for additional textual information about payments with a structured reference.\n' +
          'Maximum 140 characters',
      unstructuredMessage_placeholder: 'Additional Info',
      billInformation: 'Bill information',
      billInformation_tooltip: 'Bill information contain coded information for automated booking of the payment. The data is not forwarded with the payment.\n' +
          'Maximum 140 characters permitted',
      billInformation_placeholder: ''
    }
  },
  alternativeSchemes: {
    title: 'Alternative schemes',
    alternativeSchemeParameters: 'Parameter',
    alternativeSchemeParameters_tooltip: 'A maximum of two occurrences may be provided. Maximum 100 characters per occurrence permitted'
  },
  output: {
    title: 'Download',
    qrCode: {
      title: 'Swiss QR Code',
      format: 'Format',
      size: 'Size',
      resolution: 'Resolution'
    },
    paymentPartReceipt: {
      title: 'Payment Part & Receipt',
      fontFamily: 'Font',
      format: 'Format',
      language: 'Language',
      languages: {
        de: 'German',
        en: 'English',
        fr: 'French',
        it: 'Italian'
      },
      pageSize: 'Page Size',
      pageSizes: {
        a4: 'A4 Portrait',
        a5: 'A5 Landscape',
        din_lang: 'DIN Lang Landscape',
        din_lang_cropped: 'DIN Lang Landscape cropped (except PDF)'
      },
      resolution: 'Resolution',
      boundaryLines: 'Boundary Lines',
      boundaryLinesMargins: 'Printer safety margin boundary lines',
      boundaryLineScissors: 'Scissors',
      boundaryLineSeparationText: 'Separation instruction',
      additionalPrintMargin: 'Additional print margin (+1mm)'
    },
    swissPaymentsCode: {
      title: 'Swiss Payments Code'
    }
  },
  actions: {
    close: 'Close',
    download: 'Download',
    generate: 'Generate',
    reset: 'Reset',
    validate: 'Validate'
  },
  examples: {
    title: 'Examples',
    apply: 'Apply example',
    oneShort: 'QR Reference',
    one: 'Bill with QR reference and additional information',
    twoShort: 'Creditor Reference',
    two: 'Bill with creditor reference',
    threeShort: 'Without Reference',
    three: 'Bill without reference number, with additional information',
    fourShort: 'Blank',
    four: 'Bill without reference number, amount and debtor',
    fiveShort: 'Without Amount',
    five: 'Bill without amount, with reference and debtor'
  },
  preview: {
    title: 'Preview',
    titlePreviewGenerate: 'Preview / Generate'
  },
  validation: {
    qrReferenceNumber: 'QR reference number',
    qrIban: 'QR IBAN'
  },
  pdf: {
    merge: 'Merge',
    append: 'Append'
  },
  scan: {
    file: {
      label: 'QR-Invoice',
      type: 'File-Type',
      placeholder: 'Choose a file or drop it here...',
      dropPlaceholder: 'Drop file here...'
    },
    paymentPartReceiptTitle: 'Payment Part & Receipt created from extracted data',
    rawData: 'Raw data',
    scanDocument: 'Scan document'
  },
  dibito: {
    ad: 'Deliver your QR bills digitally via eBill'
  }
}

export default en
