const fr = {
  nav: {
    productPage: 'Page produit',
    swagger: 'Documentation API',
    lang: {
      de: 'Deutsch',
      en: 'English',
      fr: 'Français',
      it: 'Italiano'
    },
    scan: 'Scan',
    utils: 'Outils factures',
    pdfUtils: 'Outils PDF'
  },
  authentication: {
    title: 'Authentification',
    apiKey: 'API Key',
    apiKey_tooltip: 'Entrez la clé API reçue de Codeblock',
    apiKey_default: 'Il s\'agit d\'une clé API à des fins de démonstration. Certaines valeurs sont remplacées lors de la création de facture QR'
  },
  creditor: {
    title: 'Bénéficiaire',
    creditorInformation: {
      iban: 'Compte',
      iban_tooltip: 'IBAN ou QR-IBAN du bénéficiare\n' +
        'Longueur fixe: 21 caractères alphanumériques\n' +
        'IBAN seulement admis avec code de pays CH ou LI.',
      creditor: {
        name: 'Nom',
        name_tooltip: 'Nom ou entreprise du bénéficiaire selon la désignation de compte.\n' +
          'Remarque: correspond toujours au titulaire du compte\n' +
          '70 caractères au maximum\n' +
          'prénom (optionnel, si disponible) et nom ou raison sociale',
        street: 'Rue',
        street_tooltip: 'Rue/Case postale du bénéficiaire\n' +
          '70 caractères au maximum admis\n' +
          'ne peut pas contenir un numéro de maison ou de bâtiment.',
        houseNumber: 'Numéro de maison',
        houseNumber_tooltip: 'Numéro de maison du bénéficiaire\n' +
          '16 caractères au maximum admis',
        postalCode: 'Code postal',
        postalCode_tooltip: 'Code postal du bénéficiaire\n' +
          '16 caractères au maximum admis\n' +
          'toujours à indiquer sans code de pays de tête',
        city: 'Lieu',
        city_tooltip: 'Lieu du bénéficiaire\n' +
          '35 caractères au maximum admis',
        country: 'Pays',
        country_tooltip: 'Pays du bénéficiaire\n' +
          'Code de pays à deux positions selon ISO 3166-1'
      }
    }
  },
  ultimateCreditor: {
    title: 'Bénéficiaire final',
    sameAsCreditor: 'Identique au bénéficiaire',
    enterUltimateCreditor: 'Saisir le bénéficiaire final',
    futureUse: 'Le groupe de données entier ne doit pour le moment pas être renseigné',
    name: 'Nom',
    name_tooltip: 'Nom ou entreprise du bénéficiaire final\n' +
      '70 caractères au maximum\n' +
      'prénom (optionnel, si disponible) et nom ou raison sociale',
    street: 'Rue',
    street_tooltip: 'Rue/Case postale du bénéficiaire final\n' +
      '70 caractères au maximum admis\n' +
      'ne peut pas contenir un numéro de maison ou de bâtiment.',
    houseNumber: 'Numéro de maison',
    houseNumber_tooltip: 'Numéro de maison du bénéficiaire final\n' +
      '16 caractères au maximum admis',
    postalCode: 'Code postal',
    postalCode_tooltip: 'Code postal du bénéficiaire final\n' +
      '16 caractères au maximum admis\n' +
      'toujours à indiquer sans code de pays de tête.',
    city: 'Lieu',
    city_tooltip: 'Lieu du bénéficiaire final\n' +
      '35 caractères au maximum admis',
    country: 'Pays',
    country_tooltip: 'Pays du bénéficiaire final\n' +
      'Code de pays à deux positions selon ISO 3166-1'
  },
  ultimateDebtor: {
    title: 'Débiteur',
    name: 'Name',
    name_tooltip: 'Nom ou entreprise du débiteur final\n' +
      '70 caractères au maximum\n' +
      'prénom (optionnel, si disponible) et nom ou raison sociale',
    street: 'Rue',
    street_tooltip: 'Rue/Case postale du débiteur final\n' +
      '70 caractères au maximum admis\n' +
      'ne peut pas contenir un numéro de maison ou de bâtiment.',
    houseNumber: 'Numéro de maison',
    houseNumber_tooltip: 'Numéro de maison du débiteur final\n' +
      '16 caractères au maximum admis',
    postalCode: 'Code postal',
    postalCode_tooltip: 'Code postaldu débiteur final\n' +
      '16 caractères au maximum admis\n' +
      'toujours à indiquer sans code de pays de tête',
    city: 'Lieu',
    city_tooltip: 'Lieu du débiteur final\n' +
      '35 caractères au maximum admis',
    country: 'Pays',
    country_tooltip: 'Pays du débiteur final\n' +
      'Code de pays à deux positions selon ISO 3166-1'
  },
  paymentAmountInformation: {
    title: 'Information sur le montant du paiement',
    amount: 'Montant',
    amount_tooltip: 'Montant du paiement\n' +
      'L\'élément est à indiquer sans zéros de tête y compris séparateur décimal et deux décimales.\n' +
      'Décimal, 12 positions au maximum admises, y compris séparateur décimal. Seul le point («.») est admis comme séparateur décimal.',
    currency: 'Monnaie',
    currency_tooltip: 'Monnaie du paiement, code monétaire alphabétique à trois positions selon ISO 4217\n' +
      'Seuls CHF et EUR sont admis.'
  },
  paymentReference: {
    title: 'Référence de paiement',
    referenceType: 'Type de référence',
    referenceTypes: {
      qrr: 'Référence QR',
      scor: 'Creditor Reference',
      non: 'sans référence'
    },
    referenceType_tooltip: 'Les codes suivants sont admis:\n' +
      'QRR – Référence QR\n' +
      'SCOR – Creditor Reference (ISO 11649)\n' +
      'NON – sans référence\n' +
      'Quatre caractères au maximum\n' +
      'En cas d\'utilisation d\'un QR-IBAN, doit contenir le code QRR;\n' +
      'en cas d\'utilisation de l\'IBAN, il est possible d\'indiquer soit le code SCOR, soit le code NON.',
    reference: 'Référence',
    reference_tooltip: 'Numéro de référence\n' +
      'Référence de paiement structurée\n' +
      'Remarque: La référence est soit une référence QR, soit une Creditor Reference (ISO 11649)\n' +
      '27 caractères alphanumériques au maximum\n' +
      'Doit être rempli en cas d\'utilisation d\'un QR-IBAN.\n' +
      'Référence QR: 27 caractères numériques, calcul du chiffre de contrôle selon modulo 10 récursif (27e position de la référence).\n' +
      'Creditor Reference (ISO 11649): jusqu\'à 25 caractères alphanumériques.\n' +
      'L\'élément ne doit pas être rempli pour le type de référence NON.',
    additionalInformation: {
      unstructuredMessage: 'Informations supplémentaires',
      unstructuredMessage_tooltip: 'Les informations instructurées peuvent être utilisées pour l\'indication d\'un motif de paiement ou pour des informations textuelles complémentaires au sujet de paiements avec référence structurée.\n' +
          '140 caractères au maximum',
      unstructuredMessage_placeholder: 'Informations supplémentaires',
      billInformation: 'Informations de facture',
      billInformation_tooltip: 'Les informations structurelles de l\'émetteur de factures contiennent des informations codées pour la comptabilisation automatisée du paiement. Les données ne sont pas transmises avec le paiement.\n' +
          '140 caractères au maximum.',
      billInformation_placeholder: ''
    }
  },
  alternativeSchemes: {
    title: 'Procédures alternatives',
    alternativeSchemeParameters: 'Paramètre',
    alternativeSchemeParameters_tooltip: 'Peuvent actuellement être livrés deux fois au maximum. A chaque fois 100 caractères au maximum admis'
  },
  output: {
    title: 'Télécharger',
    qrCode: {
      title: 'Swiss QR Code',
      format: 'Format',
      size: 'Grandeur',
      resolution: 'Résolution'
    },
    paymentPartReceipt: {
      title: 'Section paiement & récépissé',
      fontFamily: 'Police de caractères',
      format: 'Format',
      language: 'Langue',
      languages: {
        de: 'Allemand',
        en: 'Anglais',
        fr: 'Français',
        it: 'Italien'
      },
      pageSize: 'Taille de la page\n',
      pageSizes: {
        a4: 'A4 portrait',
        a5: 'A5 paysage',
        din_lang: 'DIN Lang paysage',
        din_lang_cropped: 'DIN Lang paysage tondu (sauf PDF)'
      },
      resolution: 'Résolution',
      boundaryLines: 'Ligne de partage',
      boundaryLinesMargins: 'Marge de sécurité ligne de partage',
      boundaryLineScissors: 'Ciseaux',
      boundaryLineSeparationText: 'Mention séparation',
      additionalPrintMargin: 'Marge d\'impression supplémentaire (+1mm)'
    },
    swissPaymentsCode: {
      title: 'Swiss Payments Code'
    }
  },
  actions: {
    close: 'Fermer',
    download: 'Télécharger',
    generate: 'Générer',
    reset: 'Réinitialiser',
    validate: 'Valider'
  },
  examples: {
    title: 'Exemples',
    apply: 'Utiliser exemple',
    oneShort: 'Référence QR',
    one: 'Facture avec référence QR et informations supplémentaires',
    twoShort: 'Creditor Reference',
    two: 'Facture avec creditor reference',
    threeShort: 'Sans Référence',
    three: 'Facture sans référence, avec informations supplémentaires',
    fourShort: 'Blanche',
    four: 'Facture sans référence, montant et débiteur',
    fiveShort: 'Sans Montant',
    five: 'Facture sans montant, avec référence et débiteur'
  },
  preview: {
    title: 'Prévisualisation',
    titlePreviewGenerate: 'Prévisualisation / Générer'
  },
  validation: {
    qrReferenceNumber: 'QR Numéro de référence',
    qrIban: 'QR IBAN'
  },
  pdf: {
    merge: 'Fusionner',
    append: 'Joindre'
  },
  scan: {
    file: {
      label: 'QR facture',
      type: 'Type de fichier',
      placeholder: 'Choisissez un fichier ou déposez-le ici...',
      dropPlaceholder: 'Déposez le fichier ici...'
    },
    paymentPartReceiptTitle: 'Section paiement et récépissé créés à partir des données extraites',
    rawData: 'Données brutes',
    scanDocument: 'Scan document'
  },
  dibito: {
    ad: 'Envoyez vos factures QR numériquement via eBill'
  }
}

export default fr
