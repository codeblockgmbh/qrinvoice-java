const it = {
  nav: {
    productPage: 'Pagina del prodotto',
    swagger: 'Documentazione API',
    lang: {
      de: 'Deutsch',
      en: 'English',
      fr: 'Français',
      it: 'Italiano'
    },
    scan: 'Scan',
    utils: 'Utensile fattura',
    pdfUtils: 'Utensile PDF'
  },
  authentication: {
    title: 'Autenticazione',
    apiKey: 'API Key',
    apiKey_tooltip: 'Immettere la chiave API ricevuta da Codeblock',
    apiKey_default: 'Questa è una chiave API a scopo dimostrativo. Alcuni valori vengono sovrascritti durante la creazione fattura QR'
  },
  creditor: {
    title: 'Beneficiario',
    creditorInformation: {
      iban: 'Conto',
      iban_tooltip: 'IBAN o QR-IBAN del beneficiario\n' +
        'Lunghezza fissa: 21 caratteri alfanumerici\n' +
        'ammessi solo IBAN con codice nazione CH- o LI.',
      creditor: {
        name: 'Nome',
        name_tooltip: 'Nome o azienda del beneficiario in base all’intestazione del conto\n' +
          'Nota: corrisponde sempre al titolare del conto\n' +
          'Ammessi massimo 70 caratteri\n' +
          'Nome (opzionale, se disponibile) + cognome o ragione sociale',
        street: 'Via',
        street_tooltip: 'Via/casella postale dell’indirizzo del beneficiario\n' +
          'Ammessi massimo 70 caratteri\n' +
          'non deve contenere numeri civici.',
        houseNumber: 'Numero civico',
        houseNumber_tooltip: 'Numero civico dell’indirizzo del beneficiario\n' +
          'Ammessi massimo 16 caratteri',
        postalCode: 'Codice postale',
        postalCode_tooltip: 'Codice postale dell’indirizzo del beneficiario\n' +
          'Ammessi massimo 16 caratteri\n' +
          'Indicare sempre il codice postale senza anteporre la sigla della nazione.',
        city: 'Località',
        city_tooltip: 'Località dell’indirizzo del beneficiario\n' +
          'Ammessi massimo 35 caratteri',
        country: 'Nazione',
        country_tooltip: 'Nazione dell’indirizzo del beneficiario\n' +
          'Codice nazione a 2 caratteri secondo ISO 3166-1'
      }
    }
  },
  ultimateCreditor: {
    title: 'Beneficiario finale',
    sameAsCreditor: 'Lo stesso del beneficiario',
    enterUltimateCreditor: 'Inserisci il beneficiario finale',
    futureUse: 'Questo gruppo di dati non deve essere compilato per il momento',
    name: 'Nome',
    name_tooltip: 'Nome o azienda del beneficiario finale\n' +
      'Ammessi massimo 70 caratteri\n' +
      'Nome (opzionale, se disponibile) + cognome o ragione sociale',
    street: 'Via',
    street_tooltip: 'Via/casella postale dell’indirizzo del beneficiario finale\n' +
      'Ammessi massimo 70 caratteri\n' +
      'non deve contenere numeri civici.',
    houseNumber: 'Numero civico',
    houseNumber_tooltip: 'Numero civico dell’indirizzo del beneficiario finale\n' +
      'Ammessi massimo 16 caratteri',
    postalCode: 'Codice postale di avviamento',
    postalCode_tooltip: 'Codice postale dell’indirizzo del beneficiario finale\n' +
      'Ammessi massimo 16 caratteri\n' +
      'indicare sempre il codice postale senza anteporre la sigla della nazione.',
    city: 'Località',
    city_tooltip: 'Località dell’indirizzo del beneficiario finale\n' +
      'Ammessi massimo 35 caratteri',
    country: 'Nazione',
    country_tooltip: 'Nazione dell’indirizzo del beneficiario finale\n' +
      'Codice nazione a due caratteri secondo ISO 3166-1'
  },
  ultimateDebtor: {
    title: 'Debitore finale',
    name: 'Nome',
    name_tooltip: 'Nome o azienda del debitore finale\n' +
      'Ammessi massimo 70 caratteri\n' +
      'Nome (opzionale, se disponibile) + cognome o ragione sociale',
    street: 'Via',
    street_tooltip: 'Via/casella postale dell’indirizzo del debitore finale\n' +
      'Ammessi massimo 70 caratteri\n' +
    'Non deve contenere numeri civici.',
    houseNumber: 'Numero civico',
    houseNumber_tooltip: 'Numero civico dell’indirizzo del debitore finale\n' +
      'Ammessi massimo 16 caratteri',
    postalCode: 'Codice postale',
    postalCode_tooltip: 'Codice postale dell’indirizzo del debitore finale\n' +
      'Ammessi massimo 16 caratteri\n' +
    'indicare sempre il codice postale senza anteporre la sigla della nazione.',
    city: 'Località',
    city_tooltip: 'Località dell’indirizzo del debitore finale\n' +
      'Ammessi massimo 35 caratteri',
    country: 'Nazione',
    country_tooltip: 'Nazione dell’indirizzo del debitore finale\n' +
      'Codice nazione a due caratteri secondo ISO 3166-1'
  },
  paymentAmountInformation: {
    title: 'Informazioni sull’importo da pagare',
    amount: 'Importo',
    amount_tooltip: 'Importo del pagamento\n' +
      'L’elemento deve essere indicato senza zeri antecedenti, con il segno di separazione dei decimali e due cifre dopo la virgola.\n' +
      'Per i decimali, ammessi al massimo 12 caratteri, incluso il segno di separazione. Come segno di separazione dei decimali è ammesso il punto (.).',
    currency: 'Valuta',
    currency_tooltip: 'Valuta del pagamento, codice valuta alfanumerico a 3 caratteri secondo ISO 4217\n' +
      'Supportati solo CHF e EUR.'
  },
  paymentReference: {
    title: 'Riferimento dl pagamento',
    referenceType: 'Tipo di riferimento',
    referenceTypes: {
      qrr: 'Riferimento-QR',
      scor: 'Creditor Reference',
      non: 'Senza riferimento'
    },
    referenceType_tooltip: 'Sono ammessi i seguenti codici:\n' +
      'QRR – Riferimento-QR\n' +
      'SCOR – Creditor Reference (ISO 11649)\n' +
      'NON – Senza riferimento\n' +
      'Massimo quattro caratteri, alfanumerici\n' +
      'In caso di utilizzo di un QR-IBAN, deve contenere il codice QRR.\n' +
      'In caso di utilizzo di un IBAN, deve contenere il codice ISO o NON.',
    reference: 'Riferimento',
    reference_tooltip: 'Numero di riferimento\n' +
      'Riferimento strutturato del pagamento\n' +
      'Nota: il riferimento consiste in un riferimento-QR o ISO Creditor Reference (ISO 11649)\n' +
      'Massimo 27 caratteri, alfanumerici\n' +
      'In caso di utilizzo di un QR-IBAN deve esser compilato.\n' +
      'Riferimento-QR: 27 caratteri, numerici, calcolo della check digit secondo il modulo 10 ricorsivo (27° carattere del riferimento)\n' +
      'SCOR – Creditor Reference (ISO 11649): fino a 25 caratteri, alfanumerici\n' +
      'Per il tipo di riferimento NON, l’elemento non deve essere compilato.',
    additionalInformation: {
      unstructuredMessage: 'Informazioni supplementari',
      unstructuredMessage_tooltip: 'Le informazioni supplementari possono essere usate nel processo con messaggio e nel processo con riferimento strutturato per informazioni aggiuntive del mittente della fattura.\n' +
          'Massimo 140 caratteri',
      unstructuredMessage_placeholder: 'Informazioni supplementari',
      billInformation: 'Bill informazioni\n', // not official translation
      billInformation_tooltip: 'Le informazioni di Bill contengono informazioni codificate per la prenotazione automatica del pagamento. I dati non vengono inoltrati con il pagamento.\n' + // not official translation
          'Massimo 140 caratteri.',
      billInformation_placeholder: ''
    }
  },
  alternativeSchemes: {
    title: 'Processi alternativi',
    alternativeSchemeParameters: 'Parametro',
    alternativeSchemeParameters_tooltip: 'Attualmente può essere inviato al massimo due volte. Massimo 100 caratteri'
  },
  output: {
    title: 'Scaricare',
    qrCode: {
      title: 'Swiss QR Code',
      format: 'Formato',
      size: 'Grandezza',
      resolution: 'Risoluzione'
    },
    paymentPartReceipt: {
      title: 'Sezione pagamento',
      fontFamily: 'Font',
      format: 'Formato',
      language: 'Lingua',
      languages: {
        de: 'Tedesco',
        en: 'Inglese',
        fr: 'Francese',
        it: 'Italiano'
      },
      pageSize: 'Formato pagina',
      pageSizes: {
        a4: 'A4 verticale',
        a5: 'A5 orizzontale',
        din_lang: 'DIN Lang orizzontale',
        din_lang_cropped: 'DIN Lang orizzontale ritagliata (eccetto PDF)'
      },
      resolution: 'Risoluzione',
      boundaryLines: 'Linea divisoria',
      boundaryLinesMargins: 'Margine di sicurezza linea divisoria',
      boundaryLineScissors: 'Forbici',
      boundaryLineSeparationText: 'Istruzioni di separazione',
      additionalPrintMargin: 'Margine di stampa aggiuntivo (+1mm)'
    },
    swissPaymentsCode: {
      title: 'Swiss Payments Code'
    }
  },
  actions: {
    close: 'Chiudere',
    download: 'Scaricare',
    generate: 'Generare',
    reset: 'Reset',
    validate: 'Convalida'
  },
  examples: {
    title: 'Esempi',
    apply: 'Utilizzare esempio',
    oneShort: 'Riferimento-QR',
    one: 'Fattura con riferimento-QR e informazioni supplementari',
    twoShort: 'Creditor Reference',
    two: 'Fattura con creditor reference',
    threeShort: 'Senza riferimento',
    three: 'Fattura senza riferimento, con informazioni supplementari',
    fourShort: 'Bianco',
    four: 'Fattura senza riferimento, importo e debitore',
    fiveShort: 'Senza importo',
    five: 'Fattura senza importo, con riferimento e debitore'
  },
  preview: {
    title: 'Anteprima',
    titlePreviewGenerate: 'Anteprima / Generare'
  },
  validation: {
    qrReferenceNumber: 'QR Numero di riferimento',
    qrIban: 'QR IBAN'
  },
  pdf: {
    merge: 'Fusione',
    append: 'Appendice'
  },
  scan: {
    file: {
      label: 'QR fattura',
      type: 'Tipo di file',
      placeholder: 'Scegli un file o trascinalo qui...',
      dropPlaceholder: 'Trascina il file qui...'
    },
    paymentPartReceiptTitle: 'Sezione pagamento e ricevuta creata dai dati estratti',
    rawData: 'Raw data',
    scanDocument: 'Scansione documento'
  },
  dibito: {
    ad: 'Invia le tue fatture QR digitalmente tramite eBill'
  }
}

export default it
