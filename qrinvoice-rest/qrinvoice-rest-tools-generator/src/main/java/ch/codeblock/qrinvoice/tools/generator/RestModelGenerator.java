/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.tools.generator;

import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.documents.model.application.Address;
import ch.codeblock.qrinvoice.documents.model.application.*;
import ch.codeblock.qrinvoice.model.*;
import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.alternativeschemes.RawAlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill;
import ch.codeblock.qrinvoice.model.alternativeschemes.ebill.Type;
import ch.codeblock.qrinvoice.model.annotation.Description;
import ch.codeblock.qrinvoice.model.annotation.Example;
import ch.codeblock.qrinvoice.model.annotation.Mandatory;
import ch.codeblock.qrinvoice.model.annotation.Size;
import ch.codeblock.qrinvoice.model.billinformation.BillInformation;
import ch.codeblock.qrinvoice.model.billinformation.RawBillInformation;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.VatDetails;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.*;
import ch.codeblock.qrinvoice.paymentpartreceipt.LayoutDefinitions;
import ch.codeblock.qrinvoice.qrcode.ScanningEffortLevel;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RestModelGenerator {
    private static final String INDENT = "    "; // == one tab

    private static final String REST_MODEL_PACKAGE = "ch.codeblock.qrinvoice.rest.model";
    private static final String REST_MODEL_BI_PACKAGE = "ch.codeblock.qrinvoice.rest.model.billinformation";
    private static final String REST_MODEL_S1_PACKAGE = "ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12";
    private static final String REST_MODEL_AP_PACKAGE = "ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters";
    private static final String REST_MODEL_EB_PACKAGE = "ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters.ebill";
    private static final String REST_MODEL_DOCUMENTS_PACKAGE = "ch.codeblock.qrinvoice.rest.model.documents";
    private static final Set<String> KNOWN_INTERFACES = new HashSet<>();
    private static final Map<String, String> REPLACEMENT_MAP = new HashMap<>();

    static {
        REPLACEMENT_MAP.put("ch.codeblock.qrinvoice.documents.model.application", REST_MODEL_DOCUMENTS_PACKAGE);
        REPLACEMENT_MAP.put("ch.codeblock.qrinvoice.model.billinformation.swicos1v12", REST_MODEL_S1_PACKAGE);
        REPLACEMENT_MAP.put("ch.codeblock.qrinvoice.model.alternativeschemes.ebill", REST_MODEL_EB_PACKAGE);
        REPLACEMENT_MAP.put("ch.codeblock.qrinvoice.model.alternativeschemes", REST_MODEL_AP_PACKAGE);
        REPLACEMENT_MAP.put("ch.codeblock.qrinvoice.model", REST_MODEL_PACKAGE);
        REPLACEMENT_MAP.put("java.util.Currency", REST_MODEL_PACKAGE + ".Currency");
        REPLACEMENT_MAP.put(PageSize.class.getName(), REST_MODEL_PACKAGE + "." + PageSize.class.getSimpleName());
        REPLACEMENT_MAP.put("OutputResolution", "OutputResolutionEnum");
        REPLACEMENT_MAP.put("AddressType", "AddressTypeEnum");
        REPLACEMENT_MAP.put("ReferenceType", "ReferenceTypeEnum");
        REPLACEMENT_MAP.put("Currency", "CurrencyEnum");
        REPLACEMENT_MAP.put("PageSize", "PageSizeEnum");
        REPLACEMENT_MAP.put("FontFamily", "FontFamilyEnum");
        REPLACEMENT_MAP.put("Locale", "LanguageEnum");
    }

    private static final Map<String, Set<String>> IGNORE_MAP = new HashMap<>();

    static {
        IGNORE_MAP.put("AdditionalInformation", new HashSet<>(Collections.singletonList("trailer")));
        IGNORE_MAP.put("InvoiceDocument", new HashSet<>(Arrays.asList("vatDetails", "grossAmount")));
        IGNORE_MAP.put("Position", new HashSet<>(Collections.singletonList("total")));
        IGNORE_MAP.put("Amount", new HashSet<>(Arrays.asList("vatInclusiveRoundingDifference", "vatAmount")));
    }

    private static Path packageDir;
    private static Path packageDirBillInfo;
    private static Path packageDirSwico;
    private static Path packageDirAlternativeSchemesParameters;
    private static Path packageDirEBill;
    private static Path packageDirDocuments;

    public static void main(String[] args) throws IOException {
        final Path outputModule = Paths.get(args[0]);
        if (!Files.exists(outputModule)) {
            throw new RuntimeException("Output Module does not exist");
        }

        packageDir = Files.createDirectories(outputModule.resolve("src/main/java").resolve(REST_MODEL_PACKAGE.replace('.', '/')));
        packageDirBillInfo = Files.createDirectories(packageDir.resolve("billinformation"));
        packageDirSwico = Files.createDirectories(packageDirBillInfo.resolve("swicos1v12"));
        packageDirAlternativeSchemesParameters = Files.createDirectories(packageDir.resolve("alternativeschemeparameters"));
        packageDirEBill = Files.createDirectories(packageDirAlternativeSchemesParameters.resolve("ebill"));
        packageDirDocuments = Files.createDirectories(outputModule.resolve("src/main/java").resolve(REST_MODEL_DOCUMENTS_PACKAGE.replace('.', '/')));

        generate(packageDir, Currency.class);
        generate(packageDir, Locale.class);
        generate(packageDir, PageSize.class);
        generate(packageDir, FontFamily.class);
        generate(packageDir, OutputResolution.class);
        generate(packageDir, ScanningEffortLevel.class);

        // Header is not exposed
        // generate(Header.class);

        generate(packageDir, ImplementationGuidelinesQrBillVersion.class);
        generate(packageDir, AddressType.class);
        generate(packageDir, AlternativeSchemes.class);
        generate(packageDir, Creditor.class);
        generate(packageDir, CreditorInformation.class);
        generate(packageDir, PaymentAmountInformation.class);
        generate(packageDir, PaymentReference.class);
        generate(packageDir, ReferenceType.class);
        generate(packageDir, AdditionalInformation.class);
        generate(packageDir, QrInvoice.class);
        generate(packageDir, UltimateDebtor.class);
        generate(packageDir, UltimateCreditor.class);

        // Bill Information
        generate(packageDirBillInfo, BillInformation.class);
        generate(packageDirBillInfo, RawBillInformation.class);

        // Swico S1v12
        generate(packageDirSwico, SwicoS1v12.class);
        generate(packageDirSwico, VatDetails.class);
        generate(packageDirSwico, ImportTaxPosition.class);
        generate(packageDirSwico, PaymentCondition.class);
        generate(packageDirSwico, Tag.class);
        
        // AltPMts
        generate(packageDirAlternativeSchemesParameters, AlternativeSchemeParameter.class);
        generate(packageDirAlternativeSchemesParameters, RawAlternativeSchemeParameter.class);
        
        // EBill
        generate(packageDirEBill, EBill.class);
        generate(packageDirEBill, Type.class);

        // QrInvoiceDocuments
        generate(packageDirDocuments, InvoiceDocument.class);
        generate(packageDirDocuments, Address.class);
        generate(packageDirDocuments, Amount.class);
        generate(packageDirDocuments, ContactPerson.class);
        generate(packageDirDocuments, Position.class);
        generate(packageDirDocuments, AdditionalProperty.class);
        generate(packageDirDocuments, ch.codeblock.qrinvoice.documents.model.application.VatDetails.class);
    }

    public static void generate(final Path packageDir, final Class<?> clazz) throws IOException {
        final Path javaFile = packageDir.resolve(transformType(clazz.getSimpleName(), null) + ".java");
        System.out.println(javaFile.toFile().getAbsolutePath());
        try (PrintWriter printWriter = new PrintWriter(new FileOutputStream(javaFile.toFile()))) {
            if (clazz.isEnum()) {
                enumeration(clazz, printWriter);
            } else if (clazz.equals(Currency.class)) {
                currencyEnum(clazz, printWriter);
            } else if (clazz.equals(Locale.class)) {
                localeEnum(clazz, printWriter);
            } else if (clazz.isInterface()) {
                interfaze(clazz, printWriter);
            } else {
                clazz(clazz, printWriter);
            }
        }
    }

    private static void currencyEnum(final Class<?> clazz, final PrintWriter printWriter) {
        final Stream<String> valuesStream = SwissPaymentsCode.SUPPORTED_CURRENCIES.stream().map(Currency::getCurrencyCode);
        enumeration(clazz, printWriter, valuesStream);
    }


    private static void localeEnum(final Class<?> clazz, final PrintWriter printWriter) {
        final Stream<String> valuesStream = LayoutDefinitions.SUPPORTED_LOCALES.stream().map(Locale::getLanguage);
        enumeration(clazz, printWriter, valuesStream,
                "text = text != null ? text.toLowerCase() : null;",
                "text = text != null && text.contains(\"-\") ? text.substring(0, text.indexOf('-')) : text;",
                "text = text != null && text.contains(\";\") ? text.substring(0, text.indexOf(';')) : text;"
        );
    }

    private static void enumeration(final Class<?> clazz, final PrintWriter printWriter, final Stream<String> valuesStream, String... nomalizeSteps) {
        printWriter.printf("package %s;\n", resolveTargetPackage(clazz));
        printWriter.println();
        printWriter.printf("public enum %s {", transformType(clazz.getSimpleName(), null));
        printWriter.print(valuesStream.map(v -> System.lineSeparator() + INDENT + v).collect(Collectors.joining(",")));
        printWriter.println(";");
        printWriter.println();
        printWriter.println(INDENT + "@Override");
        printWriter.println(INDENT + "@com.fasterxml.jackson.annotation.JsonValue");
        printWriter.println(INDENT + "public String toString() {");
        printWriter.println(INDENT + "    return name();");
        printWriter.println(INDENT + "}");
        printWriter.println();
        printWriter.println(INDENT + "@com.fasterxml.jackson.annotation.JsonCreator");
        printWriter.printf("    public static %s fromValue(String text) {\n", transformType(clazz.getSimpleName(), null));
        for (final String nomalizeStep : nomalizeSteps) {
            printWriter.println(INDENT + INDENT + nomalizeStep);
        }
        printWriter.printf("        for (%s v : %s.values()) {\n", transformType(clazz.getSimpleName(), null), transformType(clazz.getSimpleName(), null));
        printWriter.println(INDENT + "        if (String.valueOf(v.name()).equals(text)) {");
        printWriter.println(INDENT + "            return v;");
        printWriter.println(INDENT + "        }");
        printWriter.println(INDENT + "    }");
        printWriter.println(INDENT + "    return null;");
        printWriter.println(INDENT + "}");
        printWriter.println("}");
    }

    private static void enumeration(final Class<?> clazz, final PrintWriter printWriter) {
        printWriter.printf("package %s;\n", resolveTargetPackage(clazz));
        printWriter.println();
        printWriter.printf("// generated by ch.codeblock.qrinvoice.tools.ch.codeblock.qrinvoice.tools.generator.RestModelGenerator on date = \"%s\"\n", LocalDateTime.now().toString());

        printSwaggerSchemaDescription(clazz, printWriter);
        final String newEnumClassName = transformType(clazz.getSimpleName(), clazz.getName());
        printWriter.println("public enum " + newEnumClassName + " {");

        for (int i = 0; i < clazz.getEnumConstants().length; i++) {
            final Object o = clazz.getEnumConstants()[i];
            final String enumValuePostfix = i == clazz.getEnumConstants().length - 1 ? ";" : ",";

            final Enum e = (Enum) o;
            try {
                Field enumField = clazz.getField(e.name());
                printSwaggerSchemaDescription(enumField, printWriter);
                if (enumField.isAnnotationPresent(Deprecated.class)) {
                    printWriter.printf(INDENT + "@java.lang.Deprecated\n");
                }
                printSwaggerSchemaDescription(clazz, printWriter);
            } catch (NoSuchFieldException ex) {
                throw new RuntimeException(ex);
            }

            if (o instanceof ReferenceType) {
                final ReferenceType referenceType = (ReferenceType) o;
                printWriter.printf(INDENT + "%s(\"%s\")%s\n", referenceType.name(), referenceType.getReferenceTypeCode(), enumValuePostfix);
            } else if (o instanceof Enum) {
                printWriter.printf(INDENT + "%s(\"%s\")%s\n", e.name(), e.name(), enumValuePostfix);
            } else {
                throw new RuntimeException();
            }
        }

        printWriter.println();
        printWriter.println(INDENT + "private final String code;");
        printWriter.println();
        printWriter.printf(INDENT + "%s(final String code) {\n", newEnumClassName);
        printWriter.println(INDENT + INDENT + "this.code = code;");
        printWriter.println(INDENT + "}");
        printWriter.println();
        printWriter.println(INDENT + "public String getCode() {");
        printWriter.println(INDENT + INDENT + "return code;");
        printWriter.println(INDENT + "}");
        printWriter.println();
        printWriter.println(INDENT + "@Override");
        printWriter.println(INDENT + "@com.fasterxml.jackson.annotation.JsonValue");
        printWriter.println(INDENT + "public String toString() {");
        printWriter.println(INDENT + "    return String.valueOf(code);");
        printWriter.println(INDENT + "}");
        printWriter.println();
        printWriter.println(INDENT + "@com.fasterxml.jackson.annotation.JsonCreator");
        printWriter.printf(INDENT + "public static %s fromValue(String text) {\n", newEnumClassName);
        printWriter.printf(INDENT + "    for (%s b : %s.values()) {\n", newEnumClassName, newEnumClassName);
        printWriter.println(INDENT + "        if (String.valueOf(b.code).equals(text)) {");
        printWriter.println(INDENT + "            return b;");
        printWriter.println(INDENT + "        }");
        printWriter.println(INDENT + "    }");
        printWriter.println(INDENT + "    return null;");
        printWriter.println(INDENT + "}");

        printWriter.println("}");
    }

    private static String resolveTargetPackage(final Class<?> clazz) {
        if (isSwicos1v12(clazz)) {
            return REST_MODEL_S1_PACKAGE;
        } else if (isBillInformation(clazz)) {
            return REST_MODEL_BI_PACKAGE;
        } else if (isEbill(clazz)) {
            return REST_MODEL_EB_PACKAGE;
        } else if (isAlternativeSchemes(clazz)) {
            return REST_MODEL_AP_PACKAGE;
        } else if (clazz.getName().contains("documents.model.application")) {
            return REST_MODEL_DOCUMENTS_PACKAGE;
        } else {
            return REST_MODEL_PACKAGE;
        }
    }

    private static boolean isBillInformation(final Class<?> clazz) {
        return clazz.getName().contains("billinformation");
    }

    private static boolean isSwicos1v12(final Class<?> clazz) {
        return clazz.getName().contains("swicos1v12");
    }

    private static boolean isAlternativeSchemes(final Class<?> clazz) {
        return clazz.getName().contains("alternativeschemes");
    }

    private static boolean isEbill(final Class<?> clazz) {
        return clazz.getName().contains("ebill");
    }

    private static void interfaze(final Class<?> clazz, final PrintWriter printWriter) {
        KNOWN_INTERFACES.add(transformTypeNames(clazz.getName()));

        printWriter.printf("package %s;\n", resolveTargetPackage(clazz));
        printWriter.println();
        printWriter.printf("@javax.annotation.Generated(value = \"ch.codeblock.qrinvoice.tools.ch.codeblock.qrinvoice.tools.generator.RestModelGenerator\", date = \"%s\")\n", LocalDateTime.now().toString());

        printSwaggerSchemaDescription(clazz, printWriter);

        printWriter.printf("public interface %s {%n", transformTypeNames(clazz.getSimpleName()));
        printWriter.println();
        printWriter.println("}");
    }

    private static void printSwaggerSchemaDescription(Class<?> clazz, PrintWriter printWriter) {
        final String typeDescription = getTypeDescription(clazz);
        final boolean isDeprecated = clazz.isAnnotationPresent(Deprecated.class);
        printSwaggerSchemaDescription("", printWriter, typeDescription, isDeprecated);
    }

    private static void printSwaggerSchemaDescription(Field field, PrintWriter printWriter) {
        final String typeDescription = getFieldDescription(field);
        final boolean isDeprecated = field.isAnnotationPresent(Deprecated.class);
        printSwaggerSchemaDescription(INDENT, printWriter, typeDescription, isDeprecated);
    }

    private static void printSwaggerSchemaDescription(Object indent, PrintWriter printWriter, String typeDescription, boolean isDeprecated) {
        if (!typeDescription.trim().isEmpty() || isDeprecated) {
            printWriter.printf("%s@io.swagger.v3.oas.annotations.media.Schema(description = \"%s\", deprecated = %s)\n", indent, typeDescription, isDeprecated);
        }
    }

    private static void clazz(final Class<?> clazz, final PrintWriter printWriter) {
        printWriter.printf("package %s;\n", resolveTargetPackage(clazz));
        printWriter.println();
        printWriter.printf("@javax.annotation.Generated(value = \"ch.codeblock.qrinvoice.tools.ch.codeblock.qrinvoice.tools.generator.RestModelGenerator\", date = \"%s\")\n", LocalDateTime.now().toString());

        final String typeDescription = getTypeDescription(clazz);
        if (isSwicos1v12(clazz)) {
            String typeName = clazz.equals(SwicoS1v12.class) ? SwicoS1v12.class.getSimpleName() : String.format("SwicoS1v12%s", clazz.getSimpleName());
            printWriter.printf("@io.swagger.v3.oas.annotations.media.Schema(name = \"%s\", description = \"%s\")\n", typeName, typeDescription);
        } else if (isEbill(clazz)) {
            String typeName = clazz.equals(EBill.class) ? EBill.class.getSimpleName() : String.format("EBill%s", clazz.getSimpleName());
            printWriter.printf("@io.swagger.v3.oas.annotations.media.Schema(name = \"%s\", description = \"%s\")\n", typeName, typeDescription);
        } else {
            printSwaggerSchemaDescription(clazz, printWriter);
        }

        final String interfaces = Arrays.stream(clazz.getInterfaces()).map(intf -> transformTypeNames(intf.getName())).filter(KNOWN_INTERFACES::contains).collect(Collectors.joining(", "));
        final String impl = !interfaces.isEmpty() ? " implements " + interfaces : "";
        printWriter.printf("public class %s %s{%n", transformTypeNames(clazz.getSimpleName()), impl);
        final List<Field> filteredFieldsList = FieldUtils.getAllFieldsList(clazz).stream()
                .filter(field -> !field.getType().equals(Header.class))
                .filter(field -> !(IGNORE_MAP.containsKey(clazz.getSimpleName()) && IGNORE_MAP.get(clazz.getSimpleName()).contains(field.getName())))
                .collect(Collectors.toList());
        for (final Field field : filteredFieldsList) {
            printWriter.println();

            final Class<?> type = field.getType();
            final String name = field.getName();
            final int modifiers = field.getModifiers();

            final String transformedType = transformType(type.getName(), name);

            final Optional<Method> optionalGetter = getGetter(clazz, field);
            if (optionalGetter.isPresent()) {
                final Method getterMethod = optionalGetter.get();
                final boolean required = getterMethod.getAnnotation(Mandatory.class) != null;
                final ch.codeblock.qrinvoice.model.annotation.Optional optionalAnnotation = getterMethod.getAnnotation(ch.codeblock.qrinvoice.model.annotation.Optional.class);
                final boolean hidden = optionalAnnotation != null && optionalAnnotation.hidden();
                final boolean deprecated = getterMethod.isAnnotationPresent(Deprecated.class);

                final String example = getGetterExampleString(getterMethod);
                final String description = getGetterDescription(getterMethod);

                final Size sizeAnnotation = getterMethod.getAnnotation(Size.class);

                if (sizeAnnotation != null) {
                    printWriter.printf(INDENT + "@javax.validation.constraints.Size(min = %s, max = %s)\n", sizeAnnotation.min(), sizeAnnotation.max());
                }
                if (required) {
                    printWriter.println(INDENT + "@javax.validation.constraints.NotNull");
                }
                final boolean isArrayType = transformedType.endsWith("[]");
                if (isArrayType) {
                    printWriter.printf(INDENT + "@io.swagger.v3.oas.annotations.media.ArraySchema(schema = \n");
                }
                final String format = LocalDate.class.equals(type) ? ", format = \"date\"" : "";
                final String accessMode = hidden ? ", accessMode = io.swagger.v3.oas.annotations.media.Schema.AccessMode.READ_ONLY" : "";
                printWriter.printf(INDENT + "@io.swagger.v3.oas.annotations.media.Schema(required = %s, description = \"%s\", deprecated = %s, example = \"%s\"%s%s)\n", required, description, deprecated, example, accessMode, format);
                if (isArrayType) {
                    printWriter.printf(INDENT + ")\n");
                }
            }

            printWriter.printf(INDENT + "@com.fasterxml.jackson.annotation.JsonProperty(\"%s\")\n", field.getName());
            printWriter.printf(INDENT + "@com.fasterxml.jackson.annotation.JsonInclude(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY)\n");

            if (LocalDate.class.equals(type)) {
                printWriter.printf(INDENT + "@com.fasterxml.jackson.annotation.JsonFormat(shape = com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING, pattern = \"yyyy-MM-dd\")\n");

            }

            final String visibilityModifier = getVisibilityModifierString(modifiers);
            printWriter.printf(INDENT + "%s %s %s;\n", visibilityModifier, transformedType, name);
        }

        printWriter.println();
        for (final Field field : filteredFieldsList) {
            final Optional<Method> optionalGetter = getGetter(clazz, field);
            if (optionalGetter.isPresent()) {
                final Method getterMethod = optionalGetter.get();

                printWriter.printf(INDENT + "%s %s %s(%s) {\n", "public", transformType(getterMethod.getReturnType().getName(), field.getName()), getterMethod.getName(), "");
                printWriter.printf(INDENT + INDENT + "return this.%s;\n", field.getName());
                printWriter.println(INDENT + "}");
            }

            printWriter.println();

            String setter = "set" + StringUtils.capitalize(field.getName());
            try {
                final Method setterMethod = clazz.getDeclaredMethod(setter, field.getType());
                final StringBuilder params = new StringBuilder();
                boolean first = true;
                for (final Parameter parameter : setterMethod.getParameters()) {
                    if (!first) {
                        params.append(", ");
                    }
                    params.append(transformType(parameter.getType().getName(), field.getName()));
                    params.append(" ");
                    params.append(field.getName()); // parameter.getName()
                    first = false;
                }
                printWriter.printf(INDENT + "%s void %s(%s) {\n", "public", setterMethod.getName(), params.toString());
                printWriter.printf(INDENT + INDENT + "this.%s = %s;\n", field.getName(), field.getName());
                printWriter.println(INDENT + "}");
            } catch (NoSuchMethodException e) {
                System.err.println(e.getMessage());
            }

            printWriter.println();
        }

        printWriter.println("}");
    }

    private static String getGetterExampleString(final Method getterMethod) {
        final Example exampleAnnotation = getterMethod.getAnnotation(Example.class);
        return exampleAnnotation != null ? exampleAnnotation.value() : "";
    }

    private static String getGetterDescription(final Method getterMethod) {
        return getDescriptionValue(getterMethod.getAnnotation(Description.class));
    }

    private static String getFieldDescription(final Field field) {
        return getDescriptionValue(field.getAnnotation(Description.class));
    }

    private static String getTypeDescription(final Class<?> clazz) {
        return getDescriptionValue(clazz.getAnnotation(Description.class));
    }

    private static String getDescriptionValue(Description descriptionAnnotation) {
        final String rawDescription = (descriptionAnnotation != null ? descriptionAnnotation.value() : "");
        return rawDescription.replaceAll("\"", "\\\\\"");
    }

    private static String getVisibilityModifierString(final int modifiers) {
        final String visibilityModifier;
        if (Modifier.isPrivate(modifiers)) {
            visibilityModifier = "private";
        } else if (Modifier.isProtected(modifiers)) {
            visibilityModifier = "protected";
        } else if (Modifier.isPublic(modifiers)) {
            visibilityModifier = "public";
        } else {
            visibilityModifier = "";
        }
        return visibilityModifier;
    }

    private static Optional<Method> getGetter(final Class<?> clazz, final Field field) {
        String getter = "get" + StringUtils.capitalize(field.getName());
        return Stream.of(clazz.getDeclaredMethods()).filter(m -> m.getName().equals(getter)).findFirst();
    }

    private static String transformType(final String type, final String fieldName) {
        return transformTypeNames(replaceType(type, fieldName));
    }


    private static String transformTypeNames(String type) {
        for (final Map.Entry<String, String> entry : REPLACEMENT_MAP.entrySet()) {
            type = type.replace(entry.getKey(), entry.getValue());
        }
        return type;
    }

    private static String replaceType(final String type, final String fieldName) {
        if (("alternativeSchemeParameters".equalsIgnoreCase(fieldName) ||
                "addressLines".equalsIgnoreCase(fieldName)) &&
                List.class.getName().equals(type)) {
            return "String[]";
        } else if (Percentage.class.getName().equals(type)) {
            return "java.math.BigDecimal";
        } else if ("additionalProperties".equalsIgnoreCase(fieldName) && List.class.getName().equals(type)) {
            return "AdditionalProperty[]";
        } else if ("positions".equalsIgnoreCase(fieldName) && List.class.getName().equals(type)) {
            return "Position[]";
        } else if ("vatDetails".equalsIgnoreCase(fieldName) && List.class.getName().equals(type)) {
            return REST_MODEL_S1_PACKAGE + "." + VatDetails.class.getSimpleName() + "[]";
        } else if ("importTaxes".equalsIgnoreCase(fieldName) && List.class.getName().equals(type)) {
            return REST_MODEL_S1_PACKAGE + "." + ImportTaxPosition.class.getSimpleName() + "[]";
        } else if ("paymentConditions".equalsIgnoreCase(fieldName) && List.class.getName().equals(type)) {
            return REST_MODEL_S1_PACKAGE + "." + PaymentCondition.class.getSimpleName() + "[]";
        } else {
            return type;
        }
    }
}
