MVN_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
DOCKER_IMAGE_VERSION=${MVN_VERSION/-SNAPSHOT/}
echo "DOCKER_IMAGE_VERSION: $DOCKER_IMAGE_VERSION"

docker build --no-cache . \
  --build-arg JAR_FILE=target/*.jar \
  -t qrinvoice-rest:latest \
  -t qrinvoice-rest:${DOCKER_IMAGE_VERSION} \
  --platform linux/amd64
