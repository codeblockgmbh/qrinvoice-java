package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RestV2SwicoS1v12Test extends RestTestBase {
    private static final String VALID_BILL_OBJECT_EXAMPLE = "{\n" +
            "  \"billInformationType\": \"SwicoS1v12\",\n" +
            "  \"invoiceReference\": \"10201409\",\n" +
            "  \"invoiceDate\": \"2019-05-12\",\n" +
            "  \"customerReference\": \"1400.000-53\",\n" +
            "  \"uidNumber\": \"106017086\",\n" +
            "  \"vatDateStart\": \"2018-05-08\",\n" +
            "  \"vatDateEnd\": \"2018-05-10\",\n" +
            "  \"vatDetails\": [\n" +
            "    {\n" +
            "      \"taxPercentage\": 7.7,\n" +
            "      \"taxedNetAmount\": 185.65\n" +
            "    }\n" +
            "  ],\n" +
            "  \"importTaxes\": [\n" +
            "    {\n" +
            "      \"taxPercentage\": 7.7,\n" +
            "      \"taxAmount\": 1000\n" +
            "    }\n" +
            "  ],\n" +
            "  \"paymentConditions\": [\n" +
            "    {\n" +
            "      \"eligiblePaymentPeriodDays\": 10,\n" +
            "      \"cashDiscountPercentage\": 2\n" +
            "    }\n" +
            "  ]\n" +
            "}";
    private static final String VALID_S1_EXAMPLE = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508180510/32/7.7:185.65/33/7.7:1000/40/2:10";
    private final TestRestTemplate restTemplate = new TestRestTemplate();


    @Test
    public void swicoS1v12Creation() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> entity = new HttpEntity<>(VALID_BILL_OBJECT_EXAMPLE, headers);

        final ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/v2/bill-information/swicos1v12/create", HttpMethod.POST, entity, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo(VALID_S1_EXAMPLE));
    }

    @Test
    public void swicoS1v12Validation() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> entity = new HttpEntity<>(VALID_BILL_OBJECT_EXAMPLE, headers);

        final ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/v2/bill-information/swicos1v12/validate", HttpMethod.POST, entity, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void swicoS1v12Parse() throws JSONException {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        final HttpEntity<String> entity = new HttpEntity<>(VALID_S1_EXAMPLE, headers);

        final ResponseEntity<String> response = restTemplate.exchange(baseUrl + "/v2/bill-information/swicos1v12/parse", HttpMethod.POST, entity, String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(new JSONObject(response.getBody()).toString(), equalTo(new JSONObject(VALID_BILL_OBJECT_EXAMPLE).toString()));
    }


}