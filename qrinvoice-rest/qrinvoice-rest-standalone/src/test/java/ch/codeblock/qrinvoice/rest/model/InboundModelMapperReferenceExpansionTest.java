package ch.codeblock.qrinvoice.rest.model;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;

public class InboundModelMapperReferenceExpansionTest {
    
    @Test
    public void testCreditorReferenceExpansion() {
        assertThat(new InboundModelMapper().expandCreditorReference(null), nullValue());
        assertThat(new InboundModelMapper().expandCreditorReference(""), equalTo(""));
        assertThat("No expansion template", new InboundModelMapper().expandCreditorReference("1"), equalTo("1"));
        assertThat(new InboundModelMapper().expandCreditorReference("{1}"), equalTo("RF741"));
        assertThat(new InboundModelMapper().expandCreditorReference("{1234512345}"), equalTo("RF451234512345"));
        assertThat(new InboundModelMapper().expandCreditorReference("{AB2G5}"), equalTo("RF68AB2G5"));
        
        assertThat(new InboundModelMapper().expandCreditorReference("{}"), nullValue());
    }
    @Test
    public void testQrReferenceExpansion() {
        assertThat(new InboundModelMapper().expandQrReference(null), nullValue());
        assertThat(new InboundModelMapper().expandQrReference(""), equalTo(""));
        assertThat("No expansion template", new InboundModelMapper().expandQrReference("1"), equalTo("1"));
        assertThat(new InboundModelMapper().expandQrReference("{1}"), equalTo("000000000000000000000000011"));
        assertThat(new InboundModelMapper().expandQrReference("{127}"), equalTo("000000000000000000000001273"));

        assertThat(new InboundModelMapper().expandQrReference("{:1}"), equalTo("000000000000000000000000011"));
        assertThat(new InboundModelMapper().expandQrReference("{1:1}"), equalTo("100000000000000000000000019"));
        assertThat(new InboundModelMapper().expandQrReference("{123:127}"), equalTo("123000000000000000000001279"));
        assertThat(new InboundModelMapper().expandQrReference("{ 12 3 : 12 7 }"), equalTo("123000000000000000000001279"));
        
        assertThat(new InboundModelMapper().expandQrReference("{1:}"), equalTo("{1:}"));
        assertThat(new InboundModelMapper().expandQrReference("{:}"), nullValue());
        assertThat(new InboundModelMapper().expandQrReference("{}"), nullValue());
    }

}