/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@ActiveProfiles("apikey")
public class RestV2ApiKeyTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();
    private static Path tempDir;

    @BeforeAll
    public static void initApiKeys() throws IOException {
        tempDir = Files.createTempDirectory(RestV2ApiKeyTest.class.getName());
        final Path file = tempDir.resolve("api-keys.json");
        final String content = "[\n" +
                "\t{\n" +
                "\t\t\"apiKey\": \"0723d34b-72da-413e-83fd-39b010624bd0\",\n" +
                "\t\t\"customerId\": 42,\n" +
                "\t\t\"customerName\": \"Integration Test\",\n" +
                "\t\t\"active\": true\n" +
                "\t}\n" +
                "]";
        Files.write(file, content.getBytes(StandardCharsets.UTF_8));
        System.setProperty("APIKEY_PATH", file.toFile().getAbsolutePath());
    }

    @AfterAll
    public static void cleanUpTempDir() throws IOException {
        if (tempDir != null) {
            FileSystemUtils.deleteRecursively(tempDir);
        }
    }

    @Test
    public void testUnauthorizedNoApiKey() {
        final ResponseEntity<String> response = apiKeyQueryParamCall(null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void testUnauthorizedWrongApiKeyQueryParam() {
        final ResponseEntity<String> response = apiKeyQueryParamCall(UUID.randomUUID().toString());
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
    }


    @Test
    public void testAuthorizedApiKeyQueryParam() {
        final ResponseEntity<String> response = apiKeyQueryParamCall("0723d34b-72da-413e-83fd-39b010624bd0");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    private ResponseEntity<String> apiKeyQueryParamCall(String apiKey) {
        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/examples/iban");
        if (apiKey != null) {
            builder.queryParam("api_key", apiKey);
        }
        return restTemplate.getForEntity(builder.toUriString(), String.class);
    }

    @Test
    public void testAuthorizedApiKeyHeaderParam() {
        final ResponseEntity<String> response = apiKeyHeaderParamRequest("0723d34b-72da-413e-83fd-39b010624bd0");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void testAuthorizedWrongApiKeyHeaderParam() {
        final ResponseEntity<String> response = apiKeyHeaderParamRequest(UUID.randomUUID().toString());
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void testAuthorizedEmptyApiKeyHeaderParam() {
        final ResponseEntity<String> response = apiKeyHeaderParamRequest("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
    }

    private ResponseEntity<String> apiKeyHeaderParamRequest(String apiKey) {
        final HttpHeaders headers = new HttpHeaders();
        if (apiKey != null) {
            headers.set("X-API-Key", apiKey);
        }
        final HttpEntity<String> entity = new HttpEntity<>("", headers);
        return restTemplate.exchange(baseUrl + "/v2/examples/iban", HttpMethod.GET, entity, String.class);
    }

    @Test
    public void testUnauthorizedAsApiKeysAmbiguous() {
        final HttpHeaders headers = new HttpHeaders();
        headers.set("X-API-Key", "0723d34b-72da-413e-83fd-39b010624bd0"); // valid key
        final HttpEntity<String> entity = new HttpEntity<>("", headers);

        // invalid key
        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/examples/iban").queryParam("api_key", UUID.randomUUID().toString());
        final ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, entity, String.class);

        // as two keys are provided, unauthorized must be result
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
    }


}
