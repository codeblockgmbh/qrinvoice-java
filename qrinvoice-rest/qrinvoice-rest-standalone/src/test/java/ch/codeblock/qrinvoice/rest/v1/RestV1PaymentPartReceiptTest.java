/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v1;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RestV1PaymentPartReceiptTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void paymentPartOk() {
        final ResponseEntity<String> response = createPaymentPart("de", TestData.QR_INVOICE, true, false,
                true, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void paymentPartInvalidQrInvoice() {
        final ResponseEntity<String> response = createPaymentPart("de", TestData.QR_INVOICE_INVALID, true, false,
                true, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void paymentPartMissingParameter() {

        final ResponseEntity<String> response = createPaymentPart("", TestData.QR_INVOICE, null, null,
                null, "", "", "");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void paymentPartEmptyQrInvoice() {
        final ResponseEntity<String> response = createPaymentPart("de", "", true, false,
                true, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

    }

    @Test
    public void paymentPartEmptyQrInvoiceObject() {
        final ResponseEntity<String> response = createPaymentPart("de", "{}", true, false,
                true, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    private ResponseEntity<String> createPaymentPart(String language, String qrInvoice, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText,
                                                     Boolean boundaryLines, String fontFamily, String pageSize, String resolution) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.put("Accept-Language", Collections.singletonList(language));
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_PDF));
        final HttpEntity<String> entity = new HttpEntity<>(qrInvoice, headers);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/qr-invoice/payment-part/");
        builder.queryParam("boundaryLineScissors", String.valueOf(boundaryLineScissors)).toUriString();
        builder.queryParam("boundaryLineSeparationText", String.valueOf(boundaryLineSeparationText)).toUriString();
        builder.queryParam("boundaryLines", String.valueOf(boundaryLines)).toUriString();
        builder.queryParam("fontFamily", fontFamily);
        builder.queryParam("pageSize", pageSize);
        builder.queryParam("resolution", resolution);

        final String url = builder.toUriString();
        return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }

}
