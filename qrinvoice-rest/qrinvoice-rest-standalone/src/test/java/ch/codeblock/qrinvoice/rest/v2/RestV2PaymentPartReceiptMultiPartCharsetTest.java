/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.MediaType.APPLICATION_PDF;

/**
 * Special test case to make sure that charset in multipart form data are supported - with spring 5.2.7.RELEASE "Content-Type: application/json; Charset=ISO-8859-1" was not supported! Was fixed in 5.2.8.RELEASE
 */
public class RestV2PaymentPartReceiptMultiPartCharsetTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void paymentPartAppendUmlautUtf8Test() {
        final String qrInvoice = TestData.QR_INVOICE.replace("2501 Biel", "4052 Münchenstein");
        final ResponseEntity<String> response = createPaymentPart(StandardCharsets.UTF_8, qrInvoice);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void paymentPartAppendUmlautIso88591Test() {
        final String qrInvoice = TestData.QR_INVOICE.replace("2501 Biel", "4052 Münchenstein");
        final ResponseEntity<String> response = createPaymentPart(StandardCharsets.ISO_8859_1, qrInvoice);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void paymentPartAppendUmlautIso88592Test() {
        final String qrInvoice = TestData.QR_INVOICE.replace("2501 Biel", "4052 Münchenstein");
        final ResponseEntity<String> response = createPaymentPart(Charset.forName("iso-8859-2"), qrInvoice);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void paymentPartAppendUmlautIso88593Test() {
        final String qrInvoice = TestData.QR_INVOICE.replace("2501 Biel", "4052 Münchenstein");
        final ResponseEntity<String> response = createPaymentPart(Charset.forName("iso-8859-3"), qrInvoice);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    public ResponseEntity<String> createPaymentPart(final Charset charset, final String qrInvoice) {
        // Requests header
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.put("Accept-Language", Collections.singletonList("de"));
        headers.setAccept(Collections.singletonList(APPLICATION_PDF));

        // Request body
        final MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();

        // Add file part
        form.add("pdf", new ClassPathResource("/example.pdf"));

        // Add qrInvoice part
        final HttpHeaders headersQrInvoice = new HttpHeaders();
        headersQrInvoice.set("Content-Type", "application/json; charset=" + charset.name().replace('_', '-'));
        form.add("QrInvoice", new HttpEntity<>(qrInvoice.getBytes(charset), headersQrInvoice));

        form.add("fontFamily", "LIBERATION_SANS");
        form.add("pageSize", "DIN_LANG");
        form.add("resolution", "MEDIUM_300_DPI");

        final String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/payment-part-receipt/append").toUriString();
        final HttpEntity<?> entity = new HttpEntity<Object>(form, headers);

        return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }

}
