/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RestV2QrReferenceTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void createTest() {
        ResponseEntity<String> response = create("123", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("000000000000000000000001236", response.getBody());

        response = create("123", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("00 00000 00000 00000 00000 01236", response.getBody());

        response = create("000000000000000000000001236", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("000000000000000000000001236", response.getBody());

        response = create("000000000000000000000001236", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("00 00000 00000 00000 00000 01236", response.getBody());

        response = create("00 00000 00000 00000 00000 01236", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("000000000000000000000001236", response.getBody());

        response = create("00 00000 00000 00000 00000 01236", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("00 00000 00000 00000 00000 01236", response.getBody());
    }

    @Test
    public void createWithEmptyCustomerIdTest() {
        ResponseEntity<String> response = create("123", false, "");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("000000000000000000000001236", response.getBody());

        response = create("123", true, "");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("00 00000 00000 00000 00000 01236", response.getBody());

        response = create("000000000000000000000001236", false, "");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("000000000000000000000001236", response.getBody());

        response = create("000000000000000000000001236", true, "");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("00 00000 00000 00000 00000 01236", response.getBody());

        response = create("00 00000 00000 00000 00000 01236", false, "");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("000000000000000000000001236", response.getBody());

        response = create("00 00000 00000 00000 00000 01236", true, "");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("00 00000 00000 00000 00000 01236", response.getBody());
    }

    @Test
    public void createWithCustomerIdTest() {
        ResponseEntity<String> response = create("123", false, "127");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("127000000000000000000001239", response.getBody());

        response = create("123", true, "127");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("12 70000 00000 00000 00000 01239", response.getBody());

        response = create("00000000000000000000123", false, "127");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("127000000000000000000001239", response.getBody());

        response = create("00000000000000000000123", true, "127");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("12 70000 00000 00000 00000 01239", response.getBody());

        response = create("00000 00000 00000 0123", false, "127");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("127000000000000000000001239", response.getBody());

        response = create("00000 00000 00000 0123", true, "127");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("12 70000 00000 00000 00000 01239", response.getBody());
    }

    @Test
    public void createTestInvalid() {
        final ResponseEntity<String> response = create("xy", false);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createTestEmpty() {
        final ResponseEntity<String> response = create("", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void createTestNull() {
        final ResponseEntity<String> response = create(null, false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void createTestInvalidChar() {
        final ResponseEntity<String> response = create("0000000000000000000000x1236", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createTestInvalidChecksum() {
        final ResponseEntity<String> response = create("000000000000000000000001235", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createTestInvalidLength() {
        final ResponseEntity<String> response = create("00000000000000000000000123666", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createWithCustomerIdTestInvalid() {
        final ResponseEntity<String> response = create("123", false, "xy");

        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createWithCustomerIdTestEmpty() {
        final ResponseEntity<String> response = create("", false, "127");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void createWithCustomerIdTestNull() {
        final ResponseEntity<String> response = create("123", false, null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createWithCustomerIdTestInvalidChar() {
        final ResponseEntity<String> response = create("123", false, "12x7");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createWithCustomerIdTestInvalidLength() {
        final ResponseEntity<String> response = create("000000000000000000000012366", false, "1");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    private ResponseEntity<String> create(String qrReferenceNumber, boolean format) {
        final HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(qrReferenceNumber, headers);

        final String uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/qr-reference/create")
                .queryParam("formatQrReference", String.valueOf(format)).toUriString();
        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    private ResponseEntity<String> create(String qrReferenceNumber, boolean format, String customerId) {
        final HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(qrReferenceNumber, headers);

        final String uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/qr-reference/create")
                .queryParam("formatQrReference", String.valueOf(format))
                .queryParam("customerId", String.valueOf(customerId)).toUriString();
        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    @Test
    public void extractTestStripLeadingZeroes() {
        final ResponseEntity<String> response = extract("000000000000000000000001236", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("123", response.getBody());
    }

    @Test
    public void extractTestDoNotStripLeadingZeroes() {
        final ResponseEntity<String> response = extract("000000000000000000000001236", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("00000000000000000000000123", response.getBody());
    }

    @Test
    public void extractTestInvalidChecksum() {
        final ResponseEntity<String> response = extract("000000000000000000000001239", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void extractTestInvalidLength() {
        final ResponseEntity<String> response = extract("000000000000000000000000001236", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }
    
    private ResponseEntity<String> extract(String qrReferenceNumber, boolean stripLeadingZeroes) {
        final HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(qrReferenceNumber, headers);

        final String uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/qr-reference/extract")
                .queryParam("stripLeadingZeroes", String.valueOf(stripLeadingZeroes)).toUriString();
        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }
    
    @Test
    public void formatValid() {
        final ResponseEntity<String> response = format("000000000000000000000001236");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("00 00000 00000 00000 00000 01236", response.getBody());
    }

    @Test
    public void formatEmpty() {
        final ResponseEntity<String> response = format("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void formatNull() {
        final ResponseEntity<String> response = format(null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void formatInvalidChar() {
        final ResponseEntity<String> response = format("00000000000000000000000123x");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void formatInvalidChecksum() {
        final ResponseEntity<String> response = format("000000000000000000000001237");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void formatInvalidLength() {
        final ResponseEntity<String> response = format("0000000000000000000000012366");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    private ResponseEntity<String> format(String qrReferenceNumber) {
        final String uri = baseUrl + "/v2/qr-reference/format";

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(qrReferenceNumber, headers);

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    @Test
    public void normalize() {
        final ResponseEntity<String> response = normalize("00 00000 00000 00000 00000 01236");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("000000000000000000000001236", response.getBody());
    }

    @Test
    public void normalizeEmpty() {
        final ResponseEntity<String> response = normalize("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void normalizeNull() {
        final ResponseEntity<String> response = normalize(null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void normalizeInvalidChar() {
        final ResponseEntity<String> response = normalize("00 00000 00000 00000 00000 0123z");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void normalizeInvalidCheckum() {
        final ResponseEntity<String> response = normalize("00 00000 00000 00000 00000 01237");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void normalizeInvalidLength() {
        final ResponseEntity<String> response = normalize("00 00000 00000 00000 00000 012366");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    private ResponseEntity<String> normalize(String qrReferenceNumber) {
        final String uri = baseUrl + "/v2/qr-reference/normalize";

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(qrReferenceNumber, headers);

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    @Test
    public void validate() {
        assertEquals(HttpStatus.OK, validate("00 00000 00000 00000 00000 01236").getStatusCode());
    }

    @Test
    public void validateEmpty() {
        final ResponseEntity<String> response = validate("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void validateNull() {
        final ResponseEntity<String> response = validate(null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void validateInvalidChar() {
        final ResponseEntity<String> response = validate("00 00000 00000 00000 00000 0123z");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void validateInvalidChecksum() {
        final ResponseEntity<String> response = validate("00 00000 00000 00000 00000 01237");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void validateInvalidLength() {
        final ResponseEntity<String> response = validate("00 00000 00000 00000 00000 012366");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    private ResponseEntity<String> validate(String qrReferenceNumber) {
        final String uri = baseUrl + "/v2/qr-reference/validate";

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(qrReferenceNumber, headers);

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }
}
