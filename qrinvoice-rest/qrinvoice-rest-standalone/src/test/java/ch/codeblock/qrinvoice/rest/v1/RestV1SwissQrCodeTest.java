/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v1;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RestV1SwissQrCodeTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void swissQrCodeOk300dpi() {
        final ResponseEntity<String> response = createSwissQrCode(TestData.QR_INVOICE, "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

    }

    @Test
    public void swissQrCodeOkDefaultResolution() {
        final ResponseEntity<String> response = createSwissQrCode(TestData.QR_INVOICE, null, "");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

    }

    @Test
    public void swissQrCodeTestQrInvoiceInvalid() {
        final ResponseEntity<String> response = createSwissQrCode(TestData.QR_INVOICE_INVALID, "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

    }

    @Test
    public void swissQrCodeEmptyQrInvoice() {
        final ResponseEntity<String> response = createSwissQrCode("", "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

    }

    @Test
    public void swissQrCodeEmptyQrInvoiceObject() {
        final ResponseEntity<String> response = createSwissQrCode("{}", "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

    }

    @Test
    public void swissQrCodeQrInvoiceNull() {
        final ResponseEntity<String> response = createSwissQrCode(null, "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    private ResponseEntity<String> createSwissQrCode(String qrInvoice, String resolution, String size) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE));
        headers.setAccept(Collections.singletonList(MediaType.IMAGE_PNG));
        final HttpEntity<String> entity = new HttpEntity<>(qrInvoice, headers);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/qr-invoice/swiss-qr-code/");
        builder.queryParam("resolution", resolution);
        builder.queryParam("size", size);

        return restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity, String.class);
    }

}
