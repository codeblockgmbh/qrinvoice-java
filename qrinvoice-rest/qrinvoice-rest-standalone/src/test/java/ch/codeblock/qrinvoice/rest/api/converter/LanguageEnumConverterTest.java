package ch.codeblock.qrinvoice.rest.api.converter;

import ch.codeblock.qrinvoice.rest.model.LanguageEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LanguageEnumConverterTest {

    @Test
    void convert() {
        assertEquals(LanguageEnum.en, new LanguageEnumConverter().convert("en"));
        assertEquals(LanguageEnum.en, new LanguageEnumConverter().convert("en, de"));

        assertEquals(LanguageEnum.fr, new LanguageEnumConverter().convert("fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5"));
        assertEquals(LanguageEnum.fr, new LanguageEnumConverter().convert("fr-CH;q=0.9"));
        assertEquals(LanguageEnum.it, new LanguageEnumConverter().convert("it-CH"));

        assertEquals(LanguageEnum.de, new LanguageEnumConverter().convert("de"));
        assertEquals(LanguageEnum.de, new LanguageEnumConverter().convert(""));
        
        assertEquals(LanguageEnum.de, new LanguageEnumConverter().convert(null));
    }
}