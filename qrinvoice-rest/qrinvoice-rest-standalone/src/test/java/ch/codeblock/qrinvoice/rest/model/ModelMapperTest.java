/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model;

import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.builder.PaymentReferenceBuilder;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ModelMapperTest {
    private InboundModelMapper inboundModelMapper;
    private OutboundModelMapper outboundModelMapper;

    private QrInvoice structuredWithoutAmount;
    private QrInvoice minimalNoReference;
    private QrInvoice combinedScor;

    @BeforeEach
    public void setup() {
        this.inboundModelMapper = InboundModelMapper.create(false);
        this.outboundModelMapper = OutboundModelMapper.create();
        this.structuredWithoutAmount = QrInvoiceBuilder
                .create()
                .creditorIBAN("CH44 3199 9123 0008 8901 2")
                .paymentAmountInformation(p -> p
                        .chf(1949.75)
                )
                .creditor(c -> c
                        .structuredAddress()
                        .name("Robert Schneider AG")
                        .streetName("Rue du Lac")
                        .houseNumber("1268")
                        .postalCode("2501")
                        .city("Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .structuredAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .streetName("Grosse Marktgasse")
                        .houseNumber("28")
                        .postalCode("9400")
                        .city("Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference("210000000003139471430009017")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(Arrays.asList(
                        "eBill/B/john.doe@example.com",
                        "cdbk/some/values"
                ))
                .build();


        this.combinedScor = QrInvoiceBuilder
                .create()
                .creditorIBAN("CH5800791123000889012")
                .paymentAmountInformation(p -> p
                        .eur()
                )
                .creditor(c -> c
                        .combinedAddress()
                        .name("Robert Schneider AG")
                        .addressLine1("Rue du Lac 1268")
                        .addressLine2("2501 Biel")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .combinedAddress()
                        .name("Pia-Maria Rutschmann-Schnyder")
                        .addressLine1("Grosse Marktgasse 28")
                        .addressLine2("9400 Rorschach")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .creditorReference("RF18539007547034")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Instruction of 03.04.2019")
                        .billInformation("//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30")
                )
                .alternativeSchemeParameters(Arrays.asList(
                        "eBill/B/john.doe@example.com",
                        "cdbk/some/values"
                ))
                .build();


        this.minimalNoReference = QrInvoiceBuilder
                .create()
                .creditorIBAN("CH5800791123000889012")
                .paymentAmountInformation(p -> p
                        .chf()
                )
                .creditor(c -> c
                        .combinedAddress()
                        .name("Robert Schneider AG")
                        .addressLine1("Rue du Lac 1268")
                        .addressLine2("2501 Biel")
                        .country("CH")
                )
                .paymentReference(PaymentReferenceBuilder::withoutReference)
                .build();
    }


    @Test
    public void testStructuredWithoutAmount() throws IOException {
        mapFortAndBackWithAssertions(this.structuredWithoutAmount);
    }

    @Test
    public void testCombinedScor() throws IOException {
        mapFortAndBackWithAssertions(this.combinedScor);
    }

    @Test
    public void testMinimal() throws IOException {
        mapFortAndBackWithAssertions(this.minimalNoReference);
    }

    private void mapFortAndBackWithAssertions(final QrInvoice original) throws IOException {
        final ch.codeblock.qrinvoice.rest.model.QrInvoice qrInvoiceRest = outboundModelMapper.map(original);
        final QrInvoice qrInvoice = inboundModelMapper.map(qrInvoiceRest);
        assertEquals(original, qrInvoice);
        assertEquals(original, inboundModelMapper.map(outboundModelMapper.map(qrInvoice)));

        final ObjectMapper objectMapper = new ObjectMapper();
        final String qrInvoiceRestJson = objectMapper.writeValueAsString(qrInvoiceRest);
        assertEquals(qrInvoiceRestJson, objectMapper.writeValueAsString(objectMapper.readValue(qrInvoiceRestJson, ch.codeblock.qrinvoice.rest.model.QrInvoice.class)));
        assertEquals(original, inboundModelMapper.map(objectMapper.readValue(qrInvoiceRestJson, ch.codeblock.qrinvoice.rest.model.QrInvoice.class)));
    }
}
