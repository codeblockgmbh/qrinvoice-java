/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.MediaType.APPLICATION_PDF;

class RestV2PdfControllerTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    void merge() {
        final ResponseEntity<String> response = createMergeOrAppendRequest("merge", "/example.pdf", "/paymentpart.pdf", null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void mergeMultipage() {
        final ResponseEntity<String> response = createMergeOrAppendRequest("merge", "/example_multipage.pdf", "/paymentpart.pdf", null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void mergeMultipageOnPage() {
        final ResponseEntity<String> response = createMergeOrAppendRequest("merge", "/example_multipage.pdf", "/paymentpart.pdf", 2);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void mergeMultipageOnInvalidRequest() {
        final ResponseEntity<String> response = createMergeOrAppendRequest("merge", "/example_multipage.pdf", "/paymentpart.pdf", 112233);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    void append() {
        final ResponseEntity<String> response = createMergeOrAppendRequest("append", "/example.pdf", "/paymentpart.pdf", null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    void appendMultipage() {
        final ResponseEntity<String> response = createMergeOrAppendRequest("append", "/example_multipage.pdf", "/paymentpart.pdf", null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    private ResponseEntity<String> createMergeOrAppendRequest(final String mergeOrAppend, final String upperPartPDF, final String lowerPartPDF, final Integer onPage) {
        // Requests header
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Collections.singletonList(APPLICATION_PDF));

        // Request body
        final MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();

        // Add files part
        form.add("file", new ClassPathResource(upperPartPDF));
        form.add("file2", new ClassPathResource(lowerPartPDF));

        // onPage
        if (onPage != null) {
            form.add("onPage", onPage);
        }

        final String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/pdf/" + mergeOrAppend).toUriString();
        final HttpEntity<?> entity = new HttpEntity<Object>(form, headers);

        return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }
}