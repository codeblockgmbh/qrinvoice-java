package ch.codeblock.qrinvoice.rest.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class LanguageEnumTest {
    @Test
    public void languageTest() {
        assertEquals(LanguageEnum.de, LanguageEnum.fromValue("de"));
        assertEquals(LanguageEnum.fr, LanguageEnum.fromValue("fr"));
        assertEquals(LanguageEnum.en, LanguageEnum.fromValue("en"));
        assertEquals(LanguageEnum.it, LanguageEnum.fromValue("it"));

        assertEquals(LanguageEnum.de, LanguageEnum.fromValue("DE"));
        assertEquals(LanguageEnum.fr, LanguageEnum.fromValue("FR"));
        assertEquals(LanguageEnum.en, LanguageEnum.fromValue("EN"));
        assertEquals(LanguageEnum.it, LanguageEnum.fromValue("IT"));

        assertEquals(LanguageEnum.de, LanguageEnum.fromValue("de-CH"));
        assertEquals(LanguageEnum.fr, LanguageEnum.fromValue("fr-CH"));
        assertEquals(LanguageEnum.en, LanguageEnum.fromValue("en-CH"));
        assertEquals(LanguageEnum.it, LanguageEnum.fromValue("it-CH"));

        assertEquals(LanguageEnum.fr, LanguageEnum.fromValue("fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5"));
        assertEquals(LanguageEnum.de, LanguageEnum.fromValue("de;q=0.9, en;q=0.8, fr;q=0.7, *;q=0.5"));

        assertNull(LanguageEnum.fromValue("es"));
        assertNull(LanguageEnum.fromValue("es-CH"));

    }
}
