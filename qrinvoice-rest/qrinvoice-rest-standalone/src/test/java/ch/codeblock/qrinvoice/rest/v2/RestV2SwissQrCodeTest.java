/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.QrInvoiceCodeScanner;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import ch.codeblock.qrinvoice.tests.resources.QrCodeSample;
import ch.codeblock.qrinvoice.tests.resources.QrCodeSamplesRegistry;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.*;

public class RestV2SwissQrCodeTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void swissQrCodeOk300dpi() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, TestData.QR_INVOICE, "MEDIUM_300_DPI", "500");
        assertOkResponse(response);
    }

    @Test
    public void swissQrCodeOkDefaultResolutionPng() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, TestData.QR_INVOICE, null, "");
        assertOkResponse(response);
    }

    @Test
    public void swissQrCodeScorExpansionOkDefaultResolutionPng() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, TestData.QR_INVOICE_SCOR_EXPANSION, null, "");
        assertOkResponse(response);
    }
    @Test
    public void swissQrCodeQrrExpansionOkDefaultResolutionPng() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, TestData.QR_INVOICE_QRR_EXPANSION, null, "");
        assertOkResponse(response);
    }

    @Test
    public void swissQrCodeOkDefaultResolutionJpg() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_JPEG, TestData.QR_INVOICE, null, "");
        assertOkResponse(response);
    }

    @Test
    public void swissQrCodeOkDefaultResolutionGif() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_GIF, TestData.QR_INVOICE, null, "");
        assertOkResponse(response);
    }

    @Test
    public void swissQrCodeOkDefaultResolutionBmp() {
        final ResponseEntity<byte[]> response = createSwissQrCode(MediaType.valueOf("image/bmp"), TestData.QR_INVOICE, null, "");
        assertOkResponse(response);
    }

    @Test
    public void swissQrCodeOkDefaultResolutionTiff() {
        final ResponseEntity<byte[]> response = createSwissQrCode(MediaType.valueOf("image/tiff"), TestData.QR_INVOICE, null, "");
        assertOkResponse(response);
    }

    @Test
    public void swissQrCodeTestQrInvoiceInvalid() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, TestData.QR_INVOICE_INVALID, "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissQrCodeEmptyQrInvoice() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, "", "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissQrCodeEmptyQrInvoiceObject() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, "{}", "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissQrCodeQrInvoiceNull() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, null, "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissQrCodeValidate() {
        final String qrCodeFile = QrCodeSamplesRegistry.data().stream().filter(qrCodeSample -> qrCodeSample.getExpectedSwissQrCodeCount() == 1).map(QrCodeSample::getFilePath).findFirst().get();
        final ResponseEntity<String> response = validateSwissQrCode(qrCodeFile);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void swissQrCodeValidateNot() {
        final String qrCodeFile = QrCodeSamplesRegistry.data().stream().filter(qrCodeSample -> qrCodeSample.getExpectedSwissQrCodeCount() == 0).map(QrCodeSample::getFilePath).findFirst().get();
        final ResponseEntity<String> response = validateSwissQrCode(qrCodeFile);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    private void assertOkResponse(ResponseEntity<byte[]> response) {
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        final QrInvoice qrInvoice = QrInvoiceCodeScanner.create().scan(response.getBody());
        assertThat(qrInvoice.getPaymentAmountInformation().getAmount(), is(BigDecimal.valueOf(199.95)));
    }

    private ResponseEntity<byte[]> createSwissQrCode(final MediaType imagePng, String qrInvoice, String resolution, String size) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE));
        headers.setAccept(Collections.singletonList(imagePng));
        final HttpEntity<String> entity = new HttpEntity<>(qrInvoice, headers);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/swiss-qr-code/");
        builder.queryParam("resolution", resolution);
        builder.queryParam("size", size);

        return restTemplate.exchange(builder.toUriString(), HttpMethod.POST, entity, byte[].class);
    }

    private ResponseEntity<String> validateSwissQrCode(final String qrCodeFile) {
        // Requests header
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        // Add file
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new ClassPathResource(qrCodeFile));

        // Create request
        final String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/swiss-qr-code/validate").toUriString();
        final HttpEntity<?> entity = new HttpEntity<Object>(body, headers);

        return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }
}
