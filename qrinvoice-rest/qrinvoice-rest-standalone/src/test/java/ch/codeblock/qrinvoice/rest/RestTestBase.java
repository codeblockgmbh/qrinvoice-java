package ch.codeblock.qrinvoice.rest;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;

import javax.annotation.PostConstruct;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public abstract class RestTestBase {

    @LocalServerPort
    protected int randomServerPort;

    protected String baseUrl;

    @PostConstruct
    public void init() {
        this.baseUrl = "http://localhost:" + randomServerPort;
    }

}
