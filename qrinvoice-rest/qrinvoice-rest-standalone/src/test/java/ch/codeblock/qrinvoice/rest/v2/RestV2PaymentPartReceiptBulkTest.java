/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.bulk.csv.Csv;
import ch.codeblock.qrinvoice.bulk.excel.Xlsx;
import ch.codeblock.qrinvoice.rest.RestTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RestV2PaymentPartReceiptBulkTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void xlsxPdfTest() throws IOException {
        final ResponseEntity<String> response = bulk(Xlsx.getExample(), MimeType.XLSX, "de", true, false,
                true, false, null, "LIBERATION_SANS", "A4", "MEDIUM_300_DPI", MimeType.PDF);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getHeaders().getContentType().toString(), equalTo(MimeType.PDF.getMimeType()));
    }

    @Test
    public void xlsxZipTest() throws IOException {
        final ResponseEntity<String> response = bulk(Xlsx.getExample(), MimeType.XLSX, "de", true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI", MimeType.ZIP);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getHeaders().getContentType().toString(), equalTo(MimeType.ZIP.getMimeType()));
    }

    @Test
    public void csvPdfTest() throws IOException {
        final ResponseEntity<String> response = bulk(Csv.getExample(), MimeType.CSV, "de", true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI", MimeType.PDF);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getHeaders().getContentType().toString(), equalTo(MimeType.PDF.getMimeType()));
    }

    @Test
    public void csvZipTest() throws IOException {
        final ResponseEntity<String> response = bulk(Csv.getExample(), MimeType.CSV, "de", true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "LOW_150_DPI", MimeType.ZIP);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getHeaders().getContentType().toString(), equalTo(MimeType.ZIP.getMimeType()));
    }

    private ResponseEntity<String> bulk(final byte[] fileContents, final MimeType inputMimeType, String language, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText,
                                        Boolean boundaryLines, Boolean boundaryLinesMargins, Boolean additionalPrintMargin, String fontFamily, String pageSize, String resolution, MimeType acceptMimeType) {
        // Requests header
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.put("Accept-Language", Collections.singletonList(language));
        headers.setAccept(Collections.singletonList(MediaType.valueOf(acceptMimeType.getMimeType())));

        // Request body
        final MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();

        // Add file part
        form.add("file", new ByteArrayResource(fileContents) {
            @Override
            public String getFilename() {
                return "file." + inputMimeType.getFileExtension(); // Filename has to be returned in order to be able to post.
            }
        });

        if (boundaryLineScissors != null) {
            form.add("boundaryLineScissors", String.valueOf(boundaryLineScissors));
        }
        if (boundaryLineSeparationText != null) {
            form.add("boundaryLineSeparationText", String.valueOf(boundaryLineSeparationText));
        }
        if (boundaryLines != null) {
            form.add("boundaryLines", String.valueOf(boundaryLines));
        }
        if (boundaryLinesMargins != null) {
            form.add("boundaryLinesMargins", String.valueOf(boundaryLinesMargins));
        }
        if (additionalPrintMargin != null) {
            form.add("additionalPrintMargin", String.valueOf(additionalPrintMargin));
        }
        if (fontFamily != null) {
            form.add("fontFamily", fontFamily);
        }
        if (pageSize != null) {
            form.add("pageSize", pageSize);
        }
        if (resolution != null) {
            form.add("resolution", resolution);
        }

        final String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/payment-part-receipt/bulk/file").toUriString();
        final HttpEntity<?> entity = new HttpEntity<Object>(form, headers);

        return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }
}
