package ch.codeblock.qrinvoice.rest.management;


import ch.codeblock.qrinvoice.rest.api.v2.examples.ExampleData;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class HashCreatorTest {

    @Test
    public void createMD5Hash() throws NoSuchAlgorithmException {
        assertEquals("d41d8cd98f00b204e9800998ecf8427e", new HashCreator().createMD5Hash(""));
        assertEquals("cc34b4b6a86493f0b89db80fbb45956a", new HashCreator().createMD5Hash(ExampleData.SPC_QR_REFERENCE));
    }
}