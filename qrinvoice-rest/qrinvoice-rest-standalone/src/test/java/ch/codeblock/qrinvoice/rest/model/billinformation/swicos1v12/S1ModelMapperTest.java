/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12;

import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.builder.ImportTaxPositionBuilder;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.builder.PaymentConditionBuilder;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.builder.SwicoS1v12Builder;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.builder.VatDetailsBuilder;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class S1ModelMapperTest {
    private S1InboundModelMapper inboundModelMapper = new S1InboundModelMapper();
    private S1OutboundModelMapper outboundModelMapper = new S1OutboundModelMapper();

    @Test
    public void test1() {
        final SwicoS1v12 s1 = SwicoS1v12Builder
                .create()
                .invoiceReference("10201409") // Tag 10 - any string
                .invoiceDate(LocalDate.of(19, 5, 12)) // Tag 11 - 12.05.2019
                .customerReference("1400.000-53") // Tag 20 - any string
                .uidNumber("106017086") // Tag 30 - UID-Nr. without pre- and suffix or delimiteres - CHE-106.017.086 MWST
                .vatDateStart(LocalDate.of(2018, 5, 8)) // Tag 31 - 08.05.2018
                .vatDetails(
                        // Tag 32 - 7.7% VAT
                        VatDetailsBuilder.create().taxPercentage(7.7).build()
                )
                .paymentConditions(
                        // Tag 40 - 2% cash discount when payed within 10 days (2% skonto), afterwards 0% and amount payable within 30 days
                        PaymentConditionBuilder.create().cashDiscountPercentage(2).eligiblePaymentPeriodDays(10).build(),
                        PaymentConditionBuilder.create().cashDiscountPercentage(0).eligiblePaymentPeriodDays(30).build()
                )
                .build();
        s1.validate().throwExceptionOnErrors();

        final SwicoS1v12 result = inboundModelMapper.map(outboundModelMapper.map(s1));
        assertEquals(s1, result);
    }

    @Test
    public void test2() {
        final SwicoS1v12 s1 = SwicoS1v12Builder
                .create()
                .invoiceReference("10201409") // Tag 10 - any string
                .invoiceDate(LocalDate.of(19, 5, 12)) // Tag 11 - 12.05.2019
                .customerReference("1400.000-53") // Tag 20 - any string
                .uidNumber("106017086") // Tag 30 - UID-Nr. without pre- and suffix or delimiteres - CHE-106.017.086 MWST
                .vatDateStart(LocalDate.of(2018, 5, 8)) // Tag 31 - 08.05.2018
                .vatDetails(
                        VatDetailsBuilder.create().taxPercentage(7.7).taxedNetAmount(100).build(),
                        VatDetailsBuilder.create().taxPercentage(8).taxedNetAmount(20).build()
                )
                .paymentConditions(
                        PaymentConditionBuilder.create().cashDiscountPercentage(0).eligiblePaymentPeriodDays(30).build()
                )
                .build();
        s1.validate().throwExceptionOnErrors();

        final SwicoS1v12 result = inboundModelMapper.map(outboundModelMapper.map(s1));
        assertEquals(s1, result);
    }

    @Test
    public void testEmpty() {
        final SwicoS1v12 s1 = SwicoS1v12Builder
                .create()
                .build();
        s1.validate().throwExceptionOnErrors();

        final SwicoS1v12 result = inboundModelMapper.map(outboundModelMapper.map(s1));
        assertEquals(s1, result);
    }

    @Test
    public void testImportTaxes() {
        final SwicoS1v12 s1 = SwicoS1v12Builder
                .create()
                .importTaxes(
                        ImportTaxPositionBuilder.create().taxAmount(100).taxPercentage(7).build(),
                        ImportTaxPositionBuilder.create().taxAmount(80).taxPercentage(3).build()
                )
                .build();
        s1.validate().throwExceptionOnErrors();

        final SwicoS1v12 result = inboundModelMapper.map(outboundModelMapper.map(s1));
        assertEquals(s1, result);
    }
}
