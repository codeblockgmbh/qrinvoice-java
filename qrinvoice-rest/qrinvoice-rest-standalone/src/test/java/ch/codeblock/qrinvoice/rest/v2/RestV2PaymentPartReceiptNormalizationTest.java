/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.MediaType.APPLICATION_PDF;

public class RestV2PaymentPartReceiptNormalizationTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void paymentPartWithNormalizationOk() {
        final ResponseEntity<String> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE_NORMALIZATION_CANDIDATE, true, false,
                true, false, null,true, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void paymentPartWithoutNormalizationNok() {
        final ResponseEntity<String> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE_NORMALIZATION_CANDIDATE, true, false,
                true, false, null,false,  "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }


    private ResponseEntity<String> createPaymentPart(final MediaType mediaType, String language, String qrInvoice, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText,
                                                     Boolean boundaryLines, Boolean boundaryLinesMargins, Boolean additionalPrintMargin, Boolean normalizeInput, String fontFamily, String pageSize, String resolution) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.put("Accept-Language", Collections.singletonList(language));
        if (mediaType != null) {
            headers.setAccept(Collections.singletonList(mediaType));
        }
        final HttpEntity<String> entity = new HttpEntity<>(qrInvoice, headers);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/payment-part-receipt/");
        if (boundaryLineScissors != null) {
            builder.queryParam("boundaryLineScissors", String.valueOf(boundaryLineScissors)).toUriString();
        }
        if (boundaryLineSeparationText != null) {
            builder.queryParam("boundaryLineSeparationText", String.valueOf(boundaryLineSeparationText)).toUriString();
        }
        if (boundaryLines != null) {
            builder.queryParam("boundaryLines", String.valueOf(boundaryLines)).toUriString();
        }
        if (boundaryLinesMargins != null) {
            builder.queryParam("boundaryLinesMargins", String.valueOf(boundaryLinesMargins)).toUriString();
        }
        if (additionalPrintMargin != null) {
            builder.queryParam("additionalPrintMargin", String.valueOf(additionalPrintMargin)).toUriString();
        }
        if (normalizeInput != null) {
            builder.queryParam("normalizeInput", String.valueOf(normalizeInput)).toUriString();
        }
        if (fontFamily != null) {
            builder.queryParam("fontFamily", fontFamily);
        }
        if (pageSize != null) {
            builder.queryParam("pageSize", pageSize);
        }
        if (resolution != null) {
            builder.queryParam("resolution", resolution);
        }

        final String url = builder.toUriString();
        return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }

}
