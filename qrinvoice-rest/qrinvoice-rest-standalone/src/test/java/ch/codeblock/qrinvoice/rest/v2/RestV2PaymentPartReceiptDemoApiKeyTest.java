/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.QrInvoiceDocumentScanner;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@ActiveProfiles("apikey")
public class RestV2PaymentPartReceiptDemoApiKeyTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();
    private static Path tempDir;

    @BeforeAll
    public static void initApiKeys() throws IOException {
        tempDir = Files.createTempDirectory(RestV2PaymentPartReceiptDemoApiKeyTest.class.getName());
        final Path file = tempDir.resolve("api-keys.json");
        final String content = "[\n" + //
                "\t{\n" +//
                "\t\t\"apiKey\": \"0723d34b-72da-413e-83fd-39b010624bd0\",\n" +//
                "\t\t\"customerId\": 42,\n" +//
                "\t\t\"customerName\": \"Integration Test\",\n" +//
                "\t\t\"active\": true\n" +//
                "\t},\n" +//
                "\t{\n" +//
                "\t\t\"apiKey\": \"582c9ea9-741a-4bb6-acae-cf92f8805864\",\n" +//
                "\t\t\"customerId\": 44,\n" +//
                "\t\t\"customerName\": \"The Client C Demo\",\n" +//
                "\t\t\"active\": true,\n" +//
                "\t\t\"demo\": true\n" +//
                "\t}\n" +//
                "]";
        Files.write(file, content.getBytes(StandardCharsets.UTF_8));
        System.setProperty("APIKEY_PATH", file.toFile().getAbsolutePath());
    }

    @AfterAll
    public static void cleanUpTempDir() throws IOException {
        if (tempDir != null) {
            FileSystemUtils.deleteRecursively(tempDir);
        }
    }

    @Test
    public void paymentPartOk() {
        final ResponseEntity<byte[]> response = createPaymentPart("0723d34b-72da-413e-83fd-39b010624bd0");
        assertOkResponse(response, false);
    }

    @Test
    public void paymentPartDemoOk() {
        final ResponseEntity<byte[]> response = createPaymentPart("582c9ea9-741a-4bb6-acae-cf92f8805864");
        assertOkResponse(response, true);
    }

    private ResponseEntity<byte[]> createPaymentPart(String apiKey) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.put("Accept-Language", Collections.singletonList("de"));
        headers.setAccept(Collections.singletonList(MediaType.IMAGE_PNG));
        final HttpEntity<String> entity = new HttpEntity<>(TestData.QR_INVOICE, headers);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/payment-part-receipt/").queryParam("api_key", apiKey);
        builder.queryParam("boundaryLineScissors", String.valueOf((Boolean) true)).toUriString();
        builder.queryParam("boundaryLineSeparationText", String.valueOf((Boolean) false)).toUriString();
        builder.queryParam("boundaryLines", String.valueOf((Boolean) true)).toUriString();
        builder.queryParam("boundaryLinesMargins", String.valueOf((Boolean) false)).toUriString();
        builder.queryParam("fontFamily", "LIBERATION_SANS");
        builder.queryParam("pageSize", "DIN_LANG");
        builder.queryParam("resolution", "MEDIUM_300_DPI");

        final String url = builder.toUriString();
        return restTemplate.exchange(url, HttpMethod.POST, entity, byte[].class);
    }

    private void assertOkResponse(ResponseEntity<byte[]> response, boolean demo) {
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));


        final MimeType mt = MimeType.getByMimeType(response.getHeaders().getContentType().toString()).get();
        try {
            final Optional<QrInvoice> qrInvoiceOptional = QrInvoiceDocumentScanner.create(mt).scanDocumentUntilFirstSwissQrCode(response.getBody());
            assertThat(qrInvoiceOptional.isPresent(), is(true));

            final BigDecimal expectedAmount = demo ? BigDecimal.valueOf(1.25) : BigDecimal.valueOf(199.95);
            assertThat(qrInvoiceOptional.get().getPaymentAmountInformation().getAmount(), is(expectedAmount));

            if (demo) {
                assertThat(qrInvoiceOptional.get().getPaymentReference().getAdditionalInformation().getUnstructuredMessage().contains("DEMO"), is(true));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
