/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.QrInvoiceDocumentScanner;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.*;

public class RestV2PaymentPartReceiptTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void paymentPartPdfOk() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", null);
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartQrrExpansionPdfOk() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE_QRR_EXPANSION, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", null);
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartScorExpansionPdfOk() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE_SCOR_EXPANSION, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", null);
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartJpgOk() {
        final ResponseEntity<byte[]> response = createPaymentPart(IMAGE_JPEG, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.JPEG);
    }


    @Test
    public void paymentPartPngOk() {
        final ResponseEntity<byte[]> response = createPaymentPart(IMAGE_PNG, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PNG);
    }

    @Test
    public void paymentPartGifOk() {
        final ResponseEntity<byte[]> response = createPaymentPart(IMAGE_GIF, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.GIF);
    }

    @Test
    public void paymentPartTiffOk() {
        final ResponseEntity<byte[]> response = createPaymentPart(MediaType.valueOf("image/tiff"), "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.TIFF);
    }

    @Test
    public void paymentPartBmpOk() {
        final ResponseEntity<byte[]> response = createPaymentPart(MediaType.valueOf("image/bmp"), "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.BMP);
    }

    @Test
    public void paymentPartOkMinimal() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE, null, null,
                null, null, null, null, null, null);
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartOkWithBoundaryLineMargins() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE, false, false,
                false, true, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartOkWithAdditionalPrintMargin() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE, false, false,
                false, false, true, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartOkWithoutAdditionalPrintMargin() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE, false, false,
                false, false, false, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartInvalidQrInvoice() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE_INVALID, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void paymentPartInvalidBoundaryLines() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE, true, false,
                false, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void paymentPartMissingParameter() {
        final ResponseEntity<byte[]> response = createPaymentPart(null, null, null, null, null,
                null, null, null, null, null, null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void paymentPartEmptyQrInvoice() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", "", true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void paymentPartEmptyQrInvoiceObject() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", "{}", true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void paymentPartDinLangCroppedPng() {
        final ResponseEntity<byte[]> response = createPaymentPart(IMAGE_PNG, "de", TestData.QR_INVOICE, false, false,
                false, false, false, "LIBERATION_SANS", "DIN_LANG_CROPPED", "HIGH_600_DPI");
        assertOkResponse(response, MimeType.PNG);
    }

    @Test
    public void paymentPartDinLangCroppedPdfUnsupported() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE, false, false,
                false, false, false, "LIBERATION_SANS", "DIN_LANG_CROPPED", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    private ResponseEntity<byte[]> createPaymentPart(final MediaType mediaType, String language, String qrInvoice, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText,
                                                     Boolean boundaryLines, Boolean boundaryLinesMargins, Boolean additionalPrintMargin, String fontFamily, String pageSize, String resolution) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.put("Accept-Language", Collections.singletonList(language));
        if (mediaType != null) {
            headers.setAccept(Collections.singletonList(mediaType));
        }
        final HttpEntity<String> entity = new HttpEntity<>(qrInvoice, headers);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/payment-part-receipt/");
        if (boundaryLineScissors != null) {
            builder.queryParam("boundaryLineScissors", String.valueOf(boundaryLineScissors)).toUriString();
        }
        if (boundaryLineSeparationText != null) {
            builder.queryParam("boundaryLineSeparationText", String.valueOf(boundaryLineSeparationText)).toUriString();
        }
        if (boundaryLines != null) {
            builder.queryParam("boundaryLines", String.valueOf(boundaryLines)).toUriString();
        }
        if (boundaryLinesMargins != null) {
            builder.queryParam("boundaryLinesMargins", String.valueOf(boundaryLinesMargins)).toUriString();
        }
        if (additionalPrintMargin != null) {
            builder.queryParam("additionalPrintMargin", String.valueOf(additionalPrintMargin)).toUriString();
        }
        if (fontFamily != null) {
            builder.queryParam("fontFamily", fontFamily);
        }
        if (pageSize != null) {
            builder.queryParam("pageSize", pageSize);
        }
        if (resolution != null) {
            builder.queryParam("resolution", resolution);
        }

        final String url = builder.toUriString();
        return restTemplate.exchange(url, HttpMethod.POST, entity, byte[].class);
    }

    @Test
    public void paymentPartMergeTest() {
        final ResponseEntity<byte[]> response = createAndMergePaymentPart("merge", "/example.pdf", null, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartMergeMultiPageTest() {
        final ResponseEntity<byte[]> response = createAndMergePaymentPart("merge", "/example_multipage.pdf", null, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartMergeMultiPageOnPageTest() {
        final ResponseEntity<byte[]> response = createAndMergePaymentPart("merge", "/example_multipage.pdf", 2, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartAppendTest() {
        final ResponseEntity<byte[]> response = createAndMergePaymentPart("append", "/example.pdf", null, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartAppendMultiPageOnPageTest() {
        final ResponseEntity<byte[]> response = createAndMergePaymentPart("merge", "/example_multipage.pdf", null, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertOkResponse(response, MimeType.PDF);
    }

    private ResponseEntity<byte[]> createAndMergePaymentPart(final String mergeOrAppend, final String upperPartPDF, final Integer onPage, String language, String qrInvoice, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText,
                                                             Boolean boundaryLines, Boolean boundaryLinesMargins, Boolean additionalPrintMargin, String fontFamily, String pageSize, String resolution) {

        // Requests header
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.put("Accept-Language", Collections.singletonList(language));
        headers.setAccept(Collections.singletonList(APPLICATION_PDF));

        // Request body
        final MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();

        // Add file part
        form.add("pdf", new ClassPathResource(upperPartPDF));

        // Add qrInvoice part
        final HttpHeaders headersQrInvoice = new HttpHeaders();
        headersQrInvoice.setContentType(MediaType.APPLICATION_JSON);
        form.add("QrInvoice", new HttpEntity<>(qrInvoice, headersQrInvoice));

        // onPage
        if (onPage != null) {
            form.add("onPage", onPage);
        }

        if (boundaryLineScissors != null) {
            form.add("boundaryLineScissors", String.valueOf(boundaryLineScissors));
        }
        if (boundaryLineSeparationText != null) {
            form.add("boundaryLineSeparationText", String.valueOf(boundaryLineSeparationText));
        }
        if (boundaryLines != null) {
            form.add("boundaryLines", String.valueOf(boundaryLines));
        }
        if (boundaryLinesMargins != null) {
            form.add("boundaryLinesMargins", String.valueOf(boundaryLinesMargins));
        }
        if (additionalPrintMargin != null) {
            form.add("additionalPrintMargin", String.valueOf(additionalPrintMargin));
        }
        if (fontFamily != null) {
            form.add("fontFamily", fontFamily);
        }
        if (pageSize != null) {
            form.add("pageSize", pageSize);
        }
        if (resolution != null) {
            form.add("resolution", resolution);
        }

        final String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/payment-part-receipt/" + mergeOrAppend).toUriString();
        final HttpEntity<?> entity = new HttpEntity<Object>(form, headers);

        return restTemplate.exchange(url, HttpMethod.POST, entity, byte[].class);
    }

    private void assertOkResponse(ResponseEntity<byte[]> response, MimeType expectedMimeType) {
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final MimeType mt = MimeType.getByMimeType(response.getHeaders().getContentType().toString()).get();
        assertThat(mt, equalTo(expectedMimeType));
        try {
            final Optional<QrInvoice> qrInvoiceOptional = QrInvoiceDocumentScanner.create(mt).scanDocumentUntilFirstSwissQrCode(response.getBody());
            assertThat(qrInvoiceOptional.isPresent(), is(true));
            assertThat(qrInvoiceOptional.get().getPaymentAmountInformation().getAmount(), is(BigDecimal.valueOf(199.95)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
