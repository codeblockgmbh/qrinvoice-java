/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model.security;


import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ApiKeyReaderTest {

    @Test
    public void testApiKeyReadFromFile() throws IOException {
        File temp = File.createTempFile("tempFile", ".txt");
        try (BufferedWriter bw = new BufferedWriter((new FileWriter(temp)))) {
            bw.write(IOUtils.resourceToString("/api-keys.json", StandardCharsets.UTF_8));
        }
        String path = temp.getAbsolutePath();

        ApiKey[] apiKeys = new ApiKeyReader().readFromFile(path);
        performAsserts(apiKeys);
    }

    @Test
    public void testApiKeyReader() throws IOException {
        String jsonFileContent = IOUtils.resourceToString("/api-keys.json", StandardCharsets.UTF_8);
        ApiKeyReader apiKeyReader = new ApiKeyReader();
        ApiKey[] apiKeys = apiKeyReader.read(jsonFileContent);

        performAsserts(apiKeys);
    }

    private void performAsserts(ApiKey[] apiKeys) {
        assertThat(apiKeys.length, equalTo(3));

        assertThat(apiKeys[0].getApiKey(), equalTo("0723d34b-72da-413e-83fd-39b010624bd0"));
        assertThat(apiKeys[0].getCustomerId(), equalTo(42));
        assertThat(apiKeys[0].getCustomerName(), equalTo("The Client"));
        assertThat(apiKeys[0].isDemo(), equalTo(false));

        assertThat(apiKeys[1].getApiKey(), equalTo("c3584bfd-2ec0-4557-b0bb-904ed7e6ecbe"));
        assertThat(apiKeys[1].getCustomerId(), equalTo(43));
        assertThat(apiKeys[1].getCustomerName(), equalTo("The Client B"));
        assertThat(apiKeys[1].isDemo(), equalTo(false));

        assertThat(apiKeys[2].getApiKey(), equalTo("582c9ea9-741a-4bb6-acae-cf92f8805864"));
        assertThat(apiKeys[2].getCustomerId(), equalTo(44));
        assertThat(apiKeys[2].getCustomerName(), equalTo("The Client C Demo"));
        assertThat(apiKeys[2].isDemo(), equalTo(true));
    }

}
