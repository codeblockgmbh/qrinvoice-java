/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion;
import ch.codeblock.qrinvoice.model.validation.QrInvoiceValidator;
import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.model.InboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.QrInvoice;
import ch.codeblock.qrinvoice.tests.resources.QrInvoiceSample;
import ch.codeblock.qrinvoice.tests.resources.QrInvoiceSamplesRegistry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.MediaType.APPLICATION_JSON;

public class RestV2SwissQrCodeParseTest extends RestTestBase {

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void parseOnePdfSingle() throws JsonProcessingException {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("digital-origin-ppr-single-page.pdf")).findFirst().get();
        final ResponseEntity<String> response = parseDocument(qrInvoiceSample.getFilePath(), false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final QrInvoice qrInvoice = new ObjectMapper().readValue(response.getBody(), QrInvoice.class);
        QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(new InboundModelMapper().map(qrInvoice)).throwExceptionOnErrors();
    }

    @Test
    public void parseOnePdfAll() throws JsonProcessingException {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("digital-origin-ppr-single-page.pdf")).findFirst().get();
        final ResponseEntity<String> response = parseDocument(qrInvoiceSample.getFilePath(), true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final QrInvoice[] qrInvoices = new ObjectMapper().readValue(response.getBody(), QrInvoice[].class);
        assertEquals(qrInvoiceSample.getZxing().getExpectedSwissQrCodeCountDefault(), qrInvoices.length);
        for (final QrInvoice qrInvoice : qrInvoices) {
            QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(new InboundModelMapper().map(qrInvoice)).throwExceptionOnErrors();
        }
    }

    @Test
    public void parseTwoPdfSingle() throws JsonProcessingException {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("digital-origin-ppr-three-pages-two-spcs.pdf")).findFirst().get();
        final ResponseEntity<String> response = parseDocument(qrInvoiceSample.getFilePath(), false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final QrInvoice qrInvoice = new ObjectMapper().readValue(response.getBody(), QrInvoice.class);
        QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(new InboundModelMapper().map(qrInvoice)).throwExceptionOnErrors();
    }

    @Test
    public void parseTwoPdfAll() throws JsonProcessingException {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("digital-origin-ppr-three-pages-two-spcs.pdf")).findFirst().get();
        final ResponseEntity<String> response = parseDocument(qrInvoiceSample.getFilePath(), true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final QrInvoice[] qrInvoices = new ObjectMapper().readValue(response.getBody(), QrInvoice[].class);
        assertEquals(qrInvoiceSample.getZxing().getExpectedSwissQrCodeCountDefault(), qrInvoices.length);
        for (final QrInvoice qrInvoice : qrInvoices) {
            QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(new InboundModelMapper().map(qrInvoice)).throwExceptionOnErrors();
        }
    }

    @Test
    public void parseTwoPngSingle() throws JsonProcessingException {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("digital-origin-swissqrcode-single.png")).findFirst().get();
        final ResponseEntity<String> response = parseDocument(qrInvoiceSample.getFilePath(), false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final QrInvoice qrInvoice = new ObjectMapper().readValue(response.getBody(), QrInvoice.class);
        QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(new InboundModelMapper().map(qrInvoice)).throwExceptionOnErrors();
    }

    @Test
    public void parseTwoPngAll() throws JsonProcessingException {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("digital-origin-swissqrcode-single.png")).findFirst().get();
        final ResponseEntity<String> response = parseDocument(qrInvoiceSample.getFilePath(), true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final QrInvoice[] qrInvoices = new ObjectMapper().readValue(response.getBody(), QrInvoice[].class);
        assertEquals(qrInvoiceSample.getZxing().getExpectedSwissQrCodeCountDefault(), qrInvoices.length);
        for (final QrInvoice qrInvoice : qrInvoices) {
            QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(new InboundModelMapper().map(qrInvoice)).throwExceptionOnErrors();
        }
    }
    
    @Test
    public void parseMultiplePdfAll() throws JsonProcessingException {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("sample-0009-abacus-immobilien.pdf")).findFirst().get();
        final ResponseEntity<String> response = parseDocument(qrInvoiceSample.getFilePath(), true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final QrInvoice[] qrInvoices = new ObjectMapper().readValue(response.getBody(), QrInvoice[].class);
        assertEquals(qrInvoiceSample.getZxing().getExpectedSwissQrCodeCountDefault(), qrInvoices.length);
        for (final QrInvoice qrInvoice : qrInvoices) {
            QrInvoiceValidator.create(ImplementationGuidelinesQrBillVersion.V2_2).validate(new InboundModelMapper().map(qrInvoice)).throwExceptionOnErrors();
        }
    }

    private ResponseEntity<String> parseDocument(final String documentToParse, final boolean parseAll) {
        // Requests header
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Collections.singletonList(APPLICATION_JSON));

        // Request body
        final MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();

        // Add files part
        form.add("file", new ClassPathResource(documentToParse));

        final String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/swiss-qr-code/" + (parseAll ? "parseAll" : "parse")).toUriString();
        final HttpEntity<?> entity = new HttpEntity<Object>(form, headers);

        return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }
}
