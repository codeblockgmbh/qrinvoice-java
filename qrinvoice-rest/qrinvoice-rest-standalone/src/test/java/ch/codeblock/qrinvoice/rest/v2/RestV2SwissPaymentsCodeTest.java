/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RestV2SwissPaymentsCodeTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    private String validSwissPaymentCode = "SPC\n" +
            "0200\n" +
            "1\n" +
            "CH3908704016075473007\n" +
            "S\n" +
            "Robert Schneider AG\n" +
            "Rue du Lac\n" +
            "1268/2/22\n" +
            "2501\n" +
            "Biel\n" +
            "CH\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "199.95\n" +
            "CHF\n" +
            "S\n" +
            "Pia-Maria Rutschmann-Schnyder\n" +
            "Grosse Marktgasse\n" +
            "28\n" +
            "9400\n" +
            "Rorschach\n" +
            "CH\n" +
            "SCOR\n" +
            "RF18539007547034\n" +
            "Bill No. 3139 for garden work and disposal of cuttings\n" +
            "EPD\n" +
            "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30\n" +
            "eBill/B/john.doe@example.com\n" +
            "cdbk/some/values";

    @Test
    public void swissPaymentsCodeOk() {
        final ResponseEntity<String> response = createSwissPaymentsCode(TestData.QR_INVOICE);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        assertEquals(validSwissPaymentCode, response.getBody());
    }

    @Test
    public void swissPaymentsCodeQrrExpansionOk() {
        final ResponseEntity<String> response = createSwissPaymentsCode(TestData.QR_INVOICE_QRR_EXPANSION);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertTrue(response.getBody().contains("210000000003139471430009017"));
    }

    @Test
    public void swissPaymentsCodeScorExpansionOk() {
        final ResponseEntity<String> response = createSwissPaymentsCode(TestData.QR_INVOICE_SCOR_EXPANSION);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertTrue(response.getBody().contains("RF18539007547034"));
    }

    @Test
    public void swissPaymentsCodeInvalidQrInvoice() {
        final ResponseEntity<String> response = createSwissPaymentsCode(TestData.QR_INVOICE_INVALID);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissPaymentsCodeEmptyQrInvoice() {
        final ResponseEntity<String> response = createSwissPaymentsCode("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissPaymentsCodeEmptyQrInvoiceNull() {
        final ResponseEntity<String> response = createSwissPaymentsCode(null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissPaymentsCodeEmptyQrInvoiceObject() {
        final ResponseEntity<String> response = createSwissPaymentsCode("{}");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissPaymentCodeValidate() {
        final ResponseEntity<String> response = validateSwissPaymentCode(validSwissPaymentCode);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void swissPaymentCodeValidateWithLinebreak() {
        final ResponseEntity<String> response = validateSwissPaymentCode(validSwissPaymentCode + "\n");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void swissPaymentCodeValidateEmpty() {
        final ResponseEntity<String> response = validateSwissPaymentCode("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissPaymentCodeValidateInvalid() {
        final ResponseEntity<String> response = validateSwissPaymentCode("asdf" + validSwissPaymentCode);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void swissPaymentCodeValidateInvalidToo() {
        final ResponseEntity<String> response = validateSwissPaymentCode(validSwissPaymentCode + "\nasdf");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    private ResponseEntity<String> createSwissPaymentsCode(String qrInvoice) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE));
        headers.setAccept(Collections.singletonList(MediaType.TEXT_PLAIN));
        final HttpEntity<String> entity = new HttpEntity<>(qrInvoice, headers);

        final String uri = baseUrl + "/v2/swiss-payments-code";

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    private ResponseEntity<String> validateSwissPaymentCode(String paymentCode) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        headers.setAccept(Collections.singletonList(MediaType.TEXT_PLAIN));
        final HttpEntity<String> entity = new HttpEntity<>(paymentCode, headers);

        final String uri = baseUrl + "/v2/swiss-payments-code/validate";

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }
}
