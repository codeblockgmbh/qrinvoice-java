package ch.codeblock.qrinvoice.rest;

import ch.codeblock.qrinvoice.rest.api.ApiKeyState;
import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ApiKeyStateTest {

    @Test
    public void nullValueKey() {
        ApiKey key = null;
        ApiKeyState state = ApiKeyState.forKey(key);
        assertThat(state, equalTo(ApiKeyState.INVALID));
    }

    @Test
    public void validKey() {
        ApiKey key = new ApiKey();
        key.setActive(true);
        key.setValidTo(ZonedDateTime.now().plusDays(100));
        ApiKeyState state = ApiKeyState.forKey(key);
        assertThat(state, equalTo(ApiKeyState.VALID));
    }

    @Test
    public void inactiveKey() {
        ApiKey key = new ApiKey();
        key.setActive(false);
        key.setValidTo(ZonedDateTime.now().plusDays(100));
        ApiKeyState state = ApiKeyState.forKey(key);
        assertThat(state, equalTo(ApiKeyState.INACTIVE));
    }

    @Test
    public void expiredKey() {
        ApiKey key = new ApiKey();
        key.setActive(true);
        key.setValidTo(ZonedDateTime.now().minusDays(100));
        ApiKeyState state = ApiKeyState.forKey(key);
        assertThat(state, equalTo(ApiKeyState.EXPIRED));
    }
}
