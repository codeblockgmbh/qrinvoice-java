/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.tests.resources.QrInvoiceSample;
import ch.codeblock.qrinvoice.tests.resources.QrInvoiceSamplesRegistry;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.http.MediaType.APPLICATION_PDF;

public class RestV2PaymentPartReceiptCloneTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void paymentPartPdfCloneTest() {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("digital-origin-ppr-single-page.pdf")).findFirst().get();

        final ResponseEntity<String> response = clonePaymentPart(qrInvoiceSample.getFilePath(), "de", true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void paymentPartImageCloneTest() {
        final QrInvoiceSample qrInvoiceSample = QrInvoiceSamplesRegistry.data().stream().filter(sample -> sample.getFilePath().endsWith("digital-origin-swissqrcode-single.png")).findFirst().get();
        final ResponseEntity<String> response = clonePaymentPart(qrInvoiceSample.getFilePath(), "de", true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void paymentPartCloneNotFoundTest() {
        // example has no swiss qr code
        final ResponseEntity<String> response = clonePaymentPart("example.pdf", "de", true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }


    private ResponseEntity<String> clonePaymentPart(final String pdfToClone, String language, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText,
                                                    Boolean boundaryLines, Boolean boundaryLinesMargins, Boolean additionalPrintMargin, String fontFamily, String pageSize, String resolution) {
        // Requests header
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.put("Accept-Language", Collections.singletonList(language));
        headers.setAccept(Collections.singletonList(APPLICATION_PDF));

        // Request body
        final MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();

        // Add file part
        form.add("file", new ClassPathResource(pdfToClone));

        if (boundaryLineScissors != null) {
            form.add("boundaryLineScissors", String.valueOf(boundaryLineScissors));
        }
        if (boundaryLineSeparationText != null) {
            form.add("boundaryLineSeparationText", String.valueOf(boundaryLineSeparationText));
        }
        if (boundaryLines != null) {
            form.add("boundaryLines", String.valueOf(boundaryLines));
        }
        if (boundaryLinesMargins != null) {
            form.add("boundaryLinesMargins", String.valueOf(boundaryLinesMargins));
        }
        if (additionalPrintMargin != null) {
            form.add("additionalPrintMargin", String.valueOf(additionalPrintMargin));
        }
        if (fontFamily != null) {
            form.add("fontFamily", fontFamily);
        }
        if (pageSize != null) {
            form.add("pageSize", pageSize);
        }
        if (resolution != null) {
            form.add("resolution", resolution);
        }

        final String url = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/payment-part-receipt/clone").toUriString();
        final HttpEntity<?> entity = new HttpEntity<Object>(form, headers);

        return restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
    }
}
