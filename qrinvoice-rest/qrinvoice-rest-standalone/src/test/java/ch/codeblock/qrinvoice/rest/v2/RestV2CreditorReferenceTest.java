/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RestV2CreditorReferenceTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void createTest() {
        ResponseEntity<String> response = create("1", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF741", response.getBody());

        response = create("1", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF74 1", response.getBody());

        response = create("123", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF78123", response.getBody());

        response = create("123", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF78 123", response.getBody());

        response = create("4238791", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF374238791", response.getBody());

        response = create("4238791", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF37 4238 791", response.getBody());

        response = create("ZO3AP3O1K58Q1XXU5MFH3", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF88ZO3AP3O1K58Q1XXU5MFH3", response.getBody());

        response = create("RF88ZO3AP3O1K58Q1XXU5MFH3", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF88ZO3AP3O1K58Q1XXU5MFH3", response.getBody());

        response = create("RF88ZO3AP3O1K58Q1XXU5MFH3", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("RF88 ZO3A P3O1 K58Q 1XXU 5MFH 3", response.getBody());
    }

    @Test
    public void createTestEmpty() {
        final ResponseEntity<String> response = create("", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void createTestNull() {
        final ResponseEntity<String> response = create(null, false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void createTestInvalidChar() {
        final ResponseEntity<String> response = create("123ä", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createTestInvalidChecksum() {
        final ResponseEntity<String> response = create("RF42ZO3AP3O1K58Q1XXU5MFH3", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void createTestInvalidLength() {
        final ResponseEntity<String> response = create("RF88ZO3AP3O1K58Q1XXU5MFH31", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    private ResponseEntity<String> create(String creditorReferenceNumber, boolean format) {
        final HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(creditorReferenceNumber, headers);

        final String uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/creditor-reference/create")
                .queryParam("formatCreditorReference", String.valueOf(format)).toUriString();
        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    @Test
    public void extractTestStripLeadingZeroes() {
        final ResponseEntity<String> response = extract("RF74001", true);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("1", response.getBody());
    }

    @Test
    public void extractTestDoNotStripLeadingZeroes() {
        final ResponseEntity<String> response = extract("RF74001", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertEquals("001", response.getBody());
    }

    @Test
    public void extractTestInvalidChecksum() {
        final ResponseEntity<String> response = extract("RF99001", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void extractTestInvalidLength() {
        final ResponseEntity<String> response = extract("RF88ZO3AP3O1K58Q1XXU5MFH31", false);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    private ResponseEntity<String> extract(String creditorReferenceNumber, boolean stripLeadingZeroes) {
        final HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(creditorReferenceNumber, headers);

        final String uri = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/creditor-reference/extract")
                .queryParam("stripLeadingZeroes", String.valueOf(stripLeadingZeroes)).toUriString();
        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    @Test
    public void validateCreditorReferenceValid() {
        final ResponseEntity<String> response = getCreditorReferenceValidation("RF45 1234 5123 45");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void validateCreditorReferenceEmptyString() {
        final ResponseEntity<String> response = getCreditorReferenceValidation("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void validateCreditorReferenceNull() {
        final ResponseEntity<String> response = getCreditorReferenceValidation(null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void validateCreditorReferenceInvalidReference() {
        final ResponseEntity<String> response = getCreditorReferenceValidation("RF45 1234 5123 x0");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void validateCreditorReferenceInvalidChecksum() {
        final ResponseEntity<String> response = getCreditorReferenceValidation("RF45 1234 5123 46");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void validateCreditorReferenceInvalidLength() {
        final ResponseEntity<String> response = getCreditorReferenceValidation("RF45 1234 5123 466");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    private ResponseEntity<String> getCreditorReferenceValidation(String creditorReference) {
        final String uri = baseUrl + "/v2/creditor-reference/validate";

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(creditorReference, headers);

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

}
