package ch.codeblock.qrinvoice.rest;

public class TestData {

    public static final String QR_INVOICE =
            "{\n" +
                    "  \"alternativeSchemes\": {\n" +
                    "    \"alternativeSchemeParameters\": [\n" +
                    "      \"eBill/B/john.doe@example.com\",\n" +
                    "      \"cdbk/some/values\"\n" +
                    "    ]\n" +
                    "  },\n" +
                    "  \"creditorInformation\": {\n" +
                    "    \"creditor\": {\n" +
                    "      \"addressType\": \"STRUCTURED\",\n" +
                    "      \"city\": \"Biel\",\n" +
                    "      \"country\": \"CH\",\n" +
                    "      \"houseNumber\": \"1268/2/22\",\n" +
                    "      \"name\": \"Robert Schneider AG\",\n" +
                    "      \"postalCode\": 2501,\n" +
                    "      \"streetName\": \"Rue du Lac\"\n" +
                    "    },\n" +
                    "    \"iban\": \"CH3908704016075473007\"\n" +
                    "  },\n" +
                    "  \"paymentAmountInformation\": {\n" +
                    "    \"amount\": 199.95,\n" +
                    "    \"currency\": \"CHF\"\n" +
                    "  },\n" +
                    "  \"paymentReference\": {\n" +
                    "    \"additionalInformation\": {\n" +
                    "      \"billInformation\": \"//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30\",\n" +
                    "      \"unstructuredMessage\": \"Bill No. 3139 for garden work and disposal of cuttings\"\n" +
                    "    },\n" +
                    "    \"reference\": \"RF18539007547034\",\n" +
                    "    \"referenceType\": \"SCOR\"\n" +
                    "  },\n" +
                    "  \"ultimateDebtor\": {\n" +
                    "    \"addressType\": \"STRUCTURED\",\n" +
                    "    \"city\": \"Rorschach\",\n" +
                    "    \"country\": \"CH\",\n" +
                    "    \"houseNumber\": 28,\n" +
                    "    \"name\": \"Pia-Maria Rutschmann-Schnyder\",\n" +
                    "    \"postalCode\": 9400,\n" +
                    "    \"streetName\": \"Grosse Marktgasse\"\n" +
                    "  }\n" +
                    "}";
    public static final String QR_INVOICE_SCOR_EXPANSION =
            "{\n" +
                    "  \"creditorInformation\": {\n" +
                    "    \"creditor\": {\n" +
                    "      \"addressType\": \"STRUCTURED\",\n" +
                    "      \"city\": \"Biel\",\n" +
                    "      \"country\": \"CH\",\n" +
                    "      \"houseNumber\": \"1268/2/22\",\n" +
                    "      \"name\": \"Robert Schneider AG\",\n" +
                    "      \"postalCode\": 2501,\n" +
                    "      \"streetName\": \"Rue du Lac\"\n" +
                    "    },\n" +
                    "    \"iban\": \"CH3908704016075473007\"\n" +
                    "  },\n" +
                    "  \"paymentAmountInformation\": {\n" +
                    "    \"amount\": 199.95,\n" +
                    "    \"currency\": \"CHF\"\n" +
                    "  },\n" +
                    "  \"paymentReference\": {\n" +
                    "    \"reference\": \"{539007547034}\",\n" +
                    "    \"referenceType\": \"SCOR\"\n" +
                    "  },\n" +
                    "  \"ultimateDebtor\": {\n" +
                    "    \"addressType\": \"STRUCTURED\",\n" +
                    "    \"city\": \"Rorschach\",\n" +
                    "    \"country\": \"CH\",\n" +
                    "    \"houseNumber\": 28,\n" +
                    "    \"name\": \"Pia-Maria Rutschmann-Schnyder\",\n" +
                    "    \"postalCode\": 9400,\n" +
                    "    \"streetName\": \"Grosse Marktgasse\"\n" +
                    "  }\n" +
                    "}";
    
    public static final String QR_INVOICE_QRR_EXPANSION =
            "{\n" +
                    "  \"creditorInformation\": {\n" +
                    "    \"creditor\": {\n" +
                    "      \"addressType\": \"STRUCTURED\",\n" +
                    "      \"city\": \"Biel\",\n" +
                    "      \"country\": \"CH\",\n" +
                    "      \"houseNumber\": \"1268/2/22\",\n" +
                    "      \"name\": \"Robert Schneider AG\",\n" +
                    "      \"postalCode\": 2501,\n" +
                    "      \"streetName\": \"Rue du Lac\"\n" +
                    "    },\n" +
                    "    \"iban\": \"CH4431999123000889012\"\n" +
                    "  },\n" +
                    "  \"paymentAmountInformation\": {\n" +
                    "    \"amount\": 199.95,\n" +
                    "    \"currency\": \"CHF\"\n" +
                    "  },\n" +
                    "  \"paymentReference\": {\n" +
                    "    \"reference\": \"{21:313947143000901}\",\n" +
                    "    \"referenceType\": \"QRR\"\n" +
                    "  },\n" +
                    "  \"ultimateDebtor\": {\n" +
                    "    \"addressType\": \"STRUCTURED\",\n" +
                    "    \"city\": \"Rorschach\",\n" +
                    "    \"country\": \"CH\",\n" +
                    "    \"houseNumber\": 28,\n" +
                    "    \"name\": \"Pia-Maria Rutschmann-Schnyder\",\n" +
                    "    \"postalCode\": 9400,\n" +
                    "    \"streetName\": \"Grosse Marktgasse\"\n" +
                    "  }\n" +
                    "}";

    public static final String QR_INVOICE_NORMALIZATION_CANDIDATE =
            "{\n" +
                    "  \"creditorInformation\": {\n" +
                    "    \"creditor\": {\n" +
                    "      \"addressType\": \"STRUCTURED\",\n" +
                    "      \"city\": \"Château-d'Œx\",\n" +
                    "      \"country\": \"CH\",\n" +
                    "      \"houseNumber\": \"1268/2/22\",\n" +
                    "      \"name\": \"Robert Schneider AG\",\n" +
                    "      \"postalCode\": 1660,\n" +
                    "      \"streetName\": \"Rue du Lac\"\n" +
                    "    },\n" +
                    "    \"iban\": \"CH3908704016075473007\"\n" +
                    "  },\n" +
                    "  \"paymentAmountInformation\": {\n" +
                    "    \"amount\": 199.95,\n" +
                    "    \"currency\": \"CHF\"\n" +
                    "  },\n" +
                    "  \"paymentReference\": {\n" +
                    "    \"additionalInformation\": {\n" +
                    "      \"unstructuredMessage\": \"Bill No. «3139» for garden work and disposal of cuttings\"\n" +
                    "    },\n" +
                    "    \"reference\": \"RF18539007547034\",\n" +
                    "    \"referenceType\": \"SCOR\"\n" +
                    "  },\n" +
                    "  \"ultimateDebtor\": {\n" +
                    "    \"addressLine1\": \"Rue du Lac 1268/3/1\",\n" +
                    "    \"addressLine2\": \"2501 Biel\",\n" +
                    "    \"addressType\": \"STRUCTURED\",\n" +
                    "    \"city\": \"Rorschach\",\n" +
                    "    \"country\": \"CH\",\n" +
                    "    \"houseNumber\": 28,\n" +
                    "    \"name\": \"Pia-Maria Rutschmann-Schnyder\",\n" +
                    "    \"postalCode\": 9400,\n" +
                    "    \"streetName\": \"Grosse Marktgasse\"\n" +
                    "  }\n" +
                    "}";

    public static final String QR_INVOICE_INVALID =
            "{\n" +
                    "  \"asdfasdfasdfsadf\": {\n" +
                    "    \"alternativeSchemeParameters\": [\n" +
                    "      \"eBill/B/john.doe@example.com\",\n" +
                    "      \"cdbk/some/values\"\n" +
                    "    ]\n" +
                    "  },\n" +
                    "  \"creditorInformation\": {\n" +
                    "    \"creditor\": {\n" +
                    "      \"addressType\": \"STRUCTURED\",\n" +
                    "      \"city\": \"Biel\",\n" +
                    "      \"country\": \"CH\",\n" +
                    "      \"houseNumber\": \"1268/2/22\",\n" +
                    "      \"name\": \"Robert Schneider AG\",\n" +
                    "      \"postalCode\": 2501,\n" +
                    "      \"streetName\": \"Rue du Lac\"\n" +
                    "    },\n" +
                    "    \"iban\": \"CH390870401607547300712341234123412434123123421341234\"\n" +
                    "  },\n" +
                    "  \"paymentAmountInformation\": {\n" +
                    "    \"amount\": 199.95,\n" +
                    "    \"currency\": \"CHF\"\n" +
                    "  },\n" +
                    "  \"paymentReference\": {\n" +
                    "    \"additionalInformation\": {\n" +
                    "      \"billInformation\": \"//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30\",\n" +
                    "      \"unstructuredMessage\": \"Bill No. 3139 for garden work and disposal of cuttings\"\n" +
                    "    },\n" +
                    "    \"reference\": \"RF18539007547034\",\n" +
                    "    \"referenceType\": \"SCOR\"\n" +
                    "  },\n" +
                    "  \"ultimateDebtor\": {\n" +
                    "    \"addressType\": \"STRUCTURED\",\n" +
                    "    \"city\": \"Rorschach\",\n" +
                    "    \"country\": \"CH\",\n" +
                    "    \"houseNumber\": 2ads8,\n" +
                    "    \"name\": \"Pia-Maria Rutschmann-Schnyder\",\n" +
                    "    \"postalCode\": 9400,\n" +
                    "    \"streetName\": \"Grosse Marktgasse\"\n" +
                    "  }\n" +
                    "}";

    public static final String INVOICE_DOCUMENT = "{\n" +
            "  \"sender\": {\n" +
            "    \"addressLines\": [\n" +
            "      \"Hans Muster AG\",\n" +
            "      \"Hauptstrasse 1\",\n" +
            "      \"3232 Ins\",\n" +
            "      \"Schweiz\"\n" +
            "    ]\n" +
            "  },\n" +
            "  \"recipient\": {\n" +
            "    \"addressLines\": [\n" +
            "      \"Kunde AG\",\n" +
            "      \"Am Weg 42\",\n" +
            "      \"3000 Bern\",\n" +
            "      \"Schweiz\"\n" +
            "    ]\n" +
            "  },\n" +
            "  \"contactPerson\": {\n" +
            "    \"name\": \"Claude Gex\",\n" +
            "    \"email\": \"claude.gex@codeblock.ch\"\n" +
            "  },\n" +
            "  \"customerNr\": \"123\",\n" +
            "  \"customerReference\": \"Kundenreferenz\",\n" +
            "  \"invoiceDate\": \"2020-11-24\",\n" +
            "  \"invoiceDueDate\": \"2020-12-23\",\n" +
            "  \"termOfPaymentDays\": 30,\n" +
            "  \"invoiceNr\": \"re-00013\",\n" +
            "  \"title\": \"QR-Invoice Java Solutions\",\n" +
            "  \"prefaceText\": \"Sehr geehrte Damen und Herren\\nWir danken Ihnen für Ihren Auftrag und stellen Ihnen wie folgt in Rechnung\",\n" +
            "  \"endingText\": \"Für Fragen stehen wir Ihnen jederzeit gerne zur Verfügung.\\n\\nFreundliche Grüsse\\n\\n\\nHans Muster AG\",\n" +
            "  \"positions\": [\n" +
            "    {\n" +
            "      \"position\": \"1\",\n" +
            "      \"description\": \"This is simple but long string with lorem ipsum text\\n\\ngenerating some sort of natural text in order to test text wrapping\",\n" +
            "      \"quantity\": 12,\n" +
            "      \"unitPrice\": {\n" +
            "        \"vatExclusive\": 10,\n" +
            "        \"vatPercentage\": 7.7\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"position\": \"2\",\n" +
            "      \"description\": \"short text\",\n" +
            "      \"quantity\": 3.2,\n" +
            "      \"unitPrice\": {\n" +
            "        \"vatExclusive\": 42.1,\n" +
            "        \"vatPercentage\": 7.7\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"position\": \"3\",\n" +
            "      \"description\": \"Mineral\",\n" +
            "      \"quantity\": 44,\n" +
            "      \"unitPrice\": {\n" +
            "        \"vatExclusive\": 1,\n" +
            "        \"vatPercentage\": 2.5\n" +
            "      },\n" +
            "      \"unit\": \"Stk\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"description\": \"This is a text only position\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"currency\": \"CHF\",\n" +
            "  \"qrInvoice\": {\n" +
            "    \"creditorInformation\": {\n" +
            "      \"iban\": \"CH4431999123000889012\",\n" +
            "      \"creditor\": {\n" +
            "        \"addressType\": \"STRUCTURED\",\n" +
            "        \"name\": \"Hans Muster AG\",\n" +
            "        \"streetName\": \"Hauptstrasse\",\n" +
            "        \"houseNumber\": \"1\",\n" +
            "        \"postalCode\": \"3232\",\n" +
            "        \"city\": \"Ins\",\n" +
            "        \"country\": \"CH\"\n" +
            "      }\n" +
            "    },\n" +
            "    \"paymentAmountInformation\": {\n" +
            "      \"currency\": \"CHF\"\n" +
            "    },\n" +
            "    \"ultimateDebtor\": {\n" +
            "      \"addressType\": \"COMBINED\",\n" +
            "      \"name\": \"Kunde AG\",\n" +
            "      \"addressLine1\": \"Am Weg 42\",\n" +
            "      \"addressLine2\": \"3000 Bern\",\n" +
            "      \"country\": \"CH\"\n" +
            "    },\n" +
            "    \"paymentReference\": {\n" +
            "      \"referenceType\": \"QRR\",\n" +
            "      \"reference\": \"210000000003139471430009017\",\n" +
            "      \"additionalInformation\": {\n" +
            "        \"unstructuredMessage\": \"Invoice Document Example\"\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";
}
