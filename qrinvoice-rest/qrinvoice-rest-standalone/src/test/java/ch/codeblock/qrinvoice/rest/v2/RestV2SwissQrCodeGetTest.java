/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.QrInvoiceCodeScanner;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.http.MediaType.IMAGE_PNG;

public class RestV2SwissQrCodeGetTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void swissQrCodeOkPdf() {
        final ResponseEntity<byte[]> response = createSwissQrCode(APPLICATION_PDF, TestData.QR_INVOICE, null, null);
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void swissQrCodeOk300dpi() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, TestData.QR_INVOICE, "MEDIUM_300_DPI", "500");
        assertOkResponse(response, MimeType.PNG);
    }

    @Test
    public void swissQrCodeTestQrInvoiceInvalid() {
        final ResponseEntity<byte[]> response = createSwissQrCode(IMAGE_PNG, TestData.QR_INVOICE_INVALID, "MEDIUM_300_DPI", "500");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    private void assertOkResponse(ResponseEntity<byte[]> response, MimeType expectedMimeType) {
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final MimeType mt = MimeType.getByMimeType(response.getHeaders().getContentType().toString()).get();
        assertThat(mt, equalTo(expectedMimeType));

        if (mt == MimeType.PNG) {
            final QrInvoice qrInvoice = QrInvoiceCodeScanner.create().scan(response.getBody());
            assertThat(qrInvoice.getPaymentAmountInformation().getAmount(), is(BigDecimal.valueOf(199.95)));
        }
    }

    private ResponseEntity<byte[]> createSwissQrCode(final MediaType mediaType, String qrInvoice, String resolution, String size) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE));
        headers.setAccept(Collections.singletonList(mediaType));
        final HttpEntity<String> entity = new HttpEntity<>(headers);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/swiss-qr-code/");
        builder.queryParam("resolution", resolution);
        builder.queryParam("size", size);
        builder.queryParam("format", mediaType);
        builder.queryParam("qrInvoice", qrInvoice);

        final String url = builder.toUriString();
        return restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);
    }

}
