/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.QrInvoiceDocumentScanner;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.rest.RestTestBase;
import ch.codeblock.qrinvoice.rest.TestData;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_PDF;
import static org.springframework.http.MediaType.IMAGE_PNG;

public class RestV2PaymentPartReceiptGetTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void paymentPartPdf() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", null);
        assertOkResponse(response, MimeType.PDF);
    }

    @Test
    public void paymentPartPng() {
        final ResponseEntity<byte[]> response = createPaymentPart(IMAGE_PNG, "de", TestData.QR_INVOICE, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", null);
        assertOkResponse(response, MimeType.PNG);
    }

    @Test
    public void paymentPartInvalidQrInvoice() {
        final ResponseEntity<byte[]> response = createPaymentPart(APPLICATION_PDF, "de", TestData.QR_INVOICE_INVALID, true, false,
                true, false, null, "LIBERATION_SANS", "DIN_LANG", "MEDIUM_300_DPI");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    private ResponseEntity<byte[]> createPaymentPart(final MediaType mediaType, String language, String qrInvoice, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText,
                                                     Boolean boundaryLines, Boolean boundaryLinesMargins, Boolean additionalPrintMargin, String fontFamily, String pageSize, String resolution) {
        final HttpHeaders headers = new HttpHeaders();
        final HttpEntity<String> entity = new HttpEntity<>(headers);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/v2/payment-part-receipt/");
        if (boundaryLineScissors != null) {
            builder.queryParam("boundaryLineScissors", String.valueOf(boundaryLineScissors)).toUriString();
        }
        if (boundaryLineSeparationText != null) {
            builder.queryParam("boundaryLineSeparationText", String.valueOf(boundaryLineSeparationText)).toUriString();
        }
        if (boundaryLines != null) {
            builder.queryParam("boundaryLines", String.valueOf(boundaryLines)).toUriString();
        }
        if (boundaryLinesMargins != null) {
            builder.queryParam("boundaryLinesMargins", String.valueOf(boundaryLinesMargins)).toUriString();
        }
        if (additionalPrintMargin != null) {
            builder.queryParam("additionalPrintMargin", String.valueOf(additionalPrintMargin)).toUriString();
        }
        if (fontFamily != null) {
            builder.queryParam("fontFamily", fontFamily);
        }
        if (pageSize != null) {
            builder.queryParam("pageSize", pageSize);
        }
        if (mediaType != null) {
            builder.queryParam("format", mediaType);
        }
        if (resolution != null) {
            builder.queryParam("resolution", resolution);
        }
        if (language != null) {
            builder.queryParam("language", language);
        }
        if (qrInvoice != null) {
            builder.queryParam("qrInvoice", qrInvoice);
        }

        final String url = builder.toUriString();
        return restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);
    }

    private void assertOkResponse(ResponseEntity<byte[]> response, MimeType expectedMimeType) {
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        final MimeType mt = MimeType.getByMimeType(response.getHeaders().getContentType().toString()).get();
        assertThat(mt, equalTo(expectedMimeType));
        try {
            final Optional<QrInvoice> qrInvoiceOptional = QrInvoiceDocumentScanner.create(mt).scanDocumentUntilFirstSwissQrCode(response.getBody());
            assertThat(qrInvoiceOptional.isPresent(), is(true));
            assertThat(qrInvoiceOptional.get().getPaymentAmountInformation().getAmount(), is(BigDecimal.valueOf(199.95)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
