/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.v2;

import ch.codeblock.qrinvoice.rest.RestTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class RestV2IbanValidationTest extends RestTestBase {
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void validateIbanValid() {
        final ResponseEntity<String> response = getIbanValidation("CH3908704016075473007");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void validateIbanEmptyString() {
        final ResponseEntity<String> response = getIbanValidation("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void validateIbanNull() {
        final ResponseEntity<String> response = getIbanValidation(null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void validateIbanInvalidChars() {
        final ResponseEntity<String> response = getIbanValidation("CH3908704016075473aka08");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));

    }

    @Test
    public void validateIbanInvalidChecksum() {
        final ResponseEntity<String> response = getIbanValidation("CH3908704016075473008");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void validateIbanInvalidLength() {
        final ResponseEntity<String> response = getIbanValidation("CH39087040160754730077");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    private ResponseEntity<String> getIbanValidation(String ibanNr) {
        final String uri = baseUrl + "/v2/iban/validate";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        HttpEntity<String> entity = new HttpEntity<>(ibanNr, headers);

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

    @Test
    public void validateQrIbanValid() {
        final ResponseEntity<String> response = getQrIbanValidation("CH44 3199 9123 0008 8901 2");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void validateQrIbanEmptyString() {
        final ResponseEntity<String> response = getQrIbanValidation("");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void validateQrIbanNull() {
        final ResponseEntity<String> response = getQrIbanValidation(null);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void validateQrIbanInvalidChar() {
        final ResponseEntity<String> response = getQrIbanValidation("CH44 3199 9123 0008 8901 2222x");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void validateQrIbanInvalidChecksum() {
        final ResponseEntity<String> response = getQrIbanValidation("CH44 3199 9123 0008 8901 6");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void validateQrIbanInvalidLength() {
        final ResponseEntity<String> response = getQrIbanValidation("CH44 3199 9123 0008 8901 22");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void validateQrIbanInvalidCountryCode() {
        final ResponseEntity<String> response = getQrIbanValidation("CG44 3199 9123 0008 8901 2");
        assertThat(response.getStatusCode(), equalTo(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    private ResponseEntity<String> getQrIbanValidation(String qrIban) {
        final String uri = baseUrl + "/v2/qr-iban/validate";

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.valueOf(MediaType.TEXT_PLAIN_VALUE));
        final HttpEntity<String> entity = new HttpEntity<>(qrIban, headers);

        return restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
    }

}
