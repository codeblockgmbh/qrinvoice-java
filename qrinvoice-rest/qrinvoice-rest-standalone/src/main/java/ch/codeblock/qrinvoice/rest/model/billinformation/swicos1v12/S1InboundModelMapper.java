/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class S1InboundModelMapper {
    public ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12 map(final ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.SwicoS1v12 s1) {
        final ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12 result = new ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12();

        result.setInvoiceReference(s1.getInvoiceReference());
        result.setInvoiceDate(s1.getInvoiceDate());
        result.setCustomerReference(s1.getCustomerReference());
        result.setUidNumber(s1.getUidNumber());
        result.setVatDateStart(s1.getVatDateStart());
        result.setVatDateEnd(s1.getVatDateEnd());
        result.setVatDetails(mapVatDetails(s1.getVatDetails()));
        result.setImportTaxes(mapImportTaxes(s1.getImportTaxes()));
        result.setPaymentConditions(mapPaymentConditions(s1.getPaymentConditions()));

        return result;
    }

    private List<ch.codeblock.qrinvoice.model.billinformation.swicos1v12.PaymentCondition> mapPaymentConditions(final ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.PaymentCondition[] paymentConditions) {
        if (paymentConditions == null) {
            return null;
        }

        final List<ch.codeblock.qrinvoice.model.billinformation.swicos1v12.PaymentCondition> result = new ArrayList<>();
        for (final ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.PaymentCondition paymentCondition : paymentConditions) {
            result.add(new ch.codeblock.qrinvoice.model.billinformation.swicos1v12.PaymentCondition(paymentCondition.getCashDiscountPercentage(), paymentCondition.getEligiblePaymentPeriodDays()));
        }

        return result;
    }

    private List<ch.codeblock.qrinvoice.model.billinformation.swicos1v12.VatDetails> mapVatDetails(final ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.VatDetails[] vatDetails) {
        if (vatDetails == null) {
            return null;
        }

        final List<ch.codeblock.qrinvoice.model.billinformation.swicos1v12.VatDetails> result = new ArrayList<>();
        for (final ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.VatDetails vatDetail : vatDetails) {
            result.add(new ch.codeblock.qrinvoice.model.billinformation.swicos1v12.VatDetails(vatDetail.getTaxPercentage(), vatDetail.getTaxedNetAmount()));
        }

        return result;
    }

    private List<ch.codeblock.qrinvoice.model.billinformation.swicos1v12.ImportTaxPosition> mapImportTaxes(final ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.ImportTaxPosition[] importTaxes) {
        if (importTaxes == null) {
            return null;
        }

        final List<ch.codeblock.qrinvoice.model.billinformation.swicos1v12.ImportTaxPosition> result = new ArrayList<>();
        for (final ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.ImportTaxPosition importTaxPosition : importTaxes) {
            result.add(new ch.codeblock.qrinvoice.model.billinformation.swicos1v12.ImportTaxPosition(importTaxPosition.getTaxPercentage(), importTaxPosition.getTaxAmount()));
        }

        return result;
    }
}
