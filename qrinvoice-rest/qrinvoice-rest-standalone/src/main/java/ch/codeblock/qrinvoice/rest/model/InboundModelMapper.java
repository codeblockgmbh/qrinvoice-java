/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model;

import ch.codeblock.qrinvoice.model.ReferenceType;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import ch.codeblock.qrinvoice.model.util.StringNormalizer;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.util.CollectionUtils;
import ch.codeblock.qrinvoice.util.CreditorReferenceUtils;
import ch.codeblock.qrinvoice.util.QRReferenceUtils;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class InboundModelMapper {
    private static final Pattern CREDITOR_REFERENCE_EXPANSION_PATTERN = Pattern.compile("^\\{(?<nr>[\\sa-zA-Z0-9]*)}$");
    private static final Pattern QR_REFERENCE_EXPANSION_PATTERN = Pattern.compile("^\\{((?<customerId>[\\s0-9]*):)?(?<nr>[\\s0-9]*)}$");

    private final Logger logger = LoggerFactory.getLogger(InboundModelMapper.class);
    private final boolean normalizeString;

    public static InboundModelMapper create(boolean normalizeString) {
        return new InboundModelMapper(normalizeString);
    }

    public InboundModelMapper() {
        this(false);
    }

    public InboundModelMapper(boolean normalizeString) {
        this.normalizeString = normalizeString;
    }

    public ch.codeblock.qrinvoice.model.QrInvoice map(QrInvoice qrInvoiceRest) {
        final QrInvoiceBuilder builder = QrInvoiceBuilder.create();

        mapCreditorInformation(builder, qrInvoiceRest.getCreditorInformation());
        mapUltimateCreditor(builder, qrInvoiceRest.getUltimateCreditor());
        mapUltimateDebtor(builder, qrInvoiceRest.getUltimateDebtor());
        mapPaymentReference(builder, qrInvoiceRest.getPaymentReference());
        mapPaymentAmountInformation(builder, qrInvoiceRest.getPaymentAmountInformation());
        mapAlternativeSchemes(builder, qrInvoiceRest.getAlternativeSchemes());

        return builder.build();
    }

    private void mapCreditorInformation(final QrInvoiceBuilder builder, final CreditorInformation creditorInformation) {
        if (creditorInformation != null) {
            builder.creditorIBAN(normalize(creditorInformation.getIban()));
            final Creditor creditor = creditorInformation.getCreditor();
            if (creditor != null) {
                validateAddressTypeNotNull(creditor.getAddressType(), "Creditor Information");
                switch (creditor.getAddressType()) {
                    case STRUCTURED:
                        builder.creditor(c -> c
                                .structuredAddress()
                                .name(normalize(creditor.getName()))
                                .streetName(normalize(creditor.getStreetName()))
                                .houseNumber(normalize(creditor.getHouseNumber()))
                                .postalCode(normalize(creditor.getPostalCode()))
                                .city(normalize(creditor.getCity()))
                                .country(normalize(creditor.getCountry()))
                        );
                        break;
                    case COMBINED:
                        logCombinedAddressUsage();
                        builder.creditor(c -> c
                                .combinedAddress()
                                .name(normalize(creditor.getName()))
                                .addressLine1(normalize(creditor.getAddressLine1()))
                                .addressLine2(normalize(creditor.getAddressLine2()))
                                .country(normalize(creditor.getCountry()))
                        );
                        break;
                }
            }
        }
    }

    private void mapUltimateCreditor(final QrInvoiceBuilder builder, final UltimateCreditor ultimateCreditor) {
        if (ultimateCreditor != null) {
            validateAddressTypeNotNull(ultimateCreditor.getAddressType(), "Ultimate Creditor");
            switch (ultimateCreditor.getAddressType()) {
                case STRUCTURED:
                    builder.ultimateCreditor(u -> u
                            .structuredAddress()
                            .name(normalize(ultimateCreditor.getName()))
                            .streetName(normalize(ultimateCreditor.getStreetName()))
                            .houseNumber(normalize(ultimateCreditor.getHouseNumber()))
                            .postalCode(normalize(ultimateCreditor.getPostalCode()))
                            .city(normalize(ultimateCreditor.getCity()))
                            .country(normalize(ultimateCreditor.getCountry()))
                    );
                    break;
                case COMBINED:
                    logCombinedAddressUsage();
                    builder.ultimateCreditor(u -> u
                            .combinedAddress()
                            .name(normalize(ultimateCreditor.getName()))
                            .addressLine1(normalize(ultimateCreditor.getAddressLine1()))
                            .addressLine2(normalize(ultimateCreditor.getAddressLine2()))
                            .country(normalize(ultimateCreditor.getCountry()))
                    );
                    break;
            }
        }
    }

    private void mapUltimateDebtor(final QrInvoiceBuilder builder, final UltimateDebtor ultimateDebtor) {

        if (ultimateDebtor != null) {
            if (StringUtils.isNotEmpty(ultimateDebtor.getName()) ||
                    StringUtils.isNotEmpty(ultimateDebtor.getStreetName()) ||
                    StringUtils.isNotEmpty(ultimateDebtor.getHouseNumber()) ||
                    StringUtils.isNotEmpty(ultimateDebtor.getAddressLine1()) ||
                    StringUtils.isNotEmpty(ultimateDebtor.getAddressLine2()) ||
                    StringUtils.isNotEmpty(ultimateDebtor.getPostalCode()) ||
                    StringUtils.isNotEmpty(ultimateDebtor.getCity())
                // ignore country
            ) {
                validateAddressTypeNotNull(ultimateDebtor.getAddressType(), "Ultimate Debtor");
                switch (ultimateDebtor.getAddressType()) {
                    case STRUCTURED:
                        builder.ultimateDebtor(u -> u
                                .structuredAddress()
                                .name(normalize(ultimateDebtor.getName()))
                                .streetName(normalize(ultimateDebtor.getStreetName()))
                                .houseNumber(normalize(ultimateDebtor.getHouseNumber()))
                                .postalCode(normalize(ultimateDebtor.getPostalCode()))
                                .city(normalize(ultimateDebtor.getCity()))
                                .country(normalize(ultimateDebtor.getCountry()))
                        );
                        break;
                    case COMBINED:
                        logCombinedAddressUsage();
                        builder.ultimateDebtor(u -> u
                                .combinedAddress()
                                .name(normalize(ultimateDebtor.getName()))
                                .addressLine1(normalize(ultimateDebtor.getAddressLine1()))
                                .addressLine2(normalize(ultimateDebtor.getAddressLine2()))
                                .country(normalize(ultimateDebtor.getCountry()))
                        );
                        break;
                }
            }
        }
    }

    private void validateAddressTypeNotNull(final AddressTypeEnum addressType, String sectionInfo) {
        if (addressType == null) {
            throw new ValidationException("Address type must be set for " + sectionInfo);
        }
    }

    private void mapPaymentReference(final QrInvoiceBuilder builder, final PaymentReference paymentReference) {
        if (paymentReference == null) {
            return;
        }

        if (paymentReference.getReferenceType() != null) {
            builder.paymentReference().referenceType(ReferenceType.valueOf(normalize(paymentReference.getReferenceType().name())));
        }

        final String normalizedReferenceInput = StringUtils.emptyStringAsNull(normalize(paymentReference.getReference()));
        if (normalizedReferenceInput != null) {
            final String reference = expandReferenceNumber(paymentReference, normalizedReferenceInput);
            builder.paymentReference().reference(reference);
        }

        mapAdditionalInformation(builder, paymentReference.getAdditionalInformation());
    }

    String expandReferenceNumber(PaymentReference paymentReference, String reference) {
        if (paymentReference.getReferenceType() == ReferenceTypeEnum.QR_REFERENCE) {
            return expandQrReference(reference);
        } else if (paymentReference.getReferenceType() == ReferenceTypeEnum.CREDITOR_REFERENCE) {
            return expandCreditorReference(reference);
        } else {
            return reference;
        }
    }

    String expandQrReference(String reference) {
        if (reference == null) {
            return null;
        }

        final Matcher matcher = QR_REFERENCE_EXPANSION_PATTERN.matcher(reference);
        if (matcher.matches()) {
            final String customerId = StringUtils.emptyStringAsNull(normalize(matcher.group("customerId")));
            final String nr = StringUtils.emptyStringAsNull(normalize(matcher.group("nr")));
            if (customerId != null && nr != null) {
                // {1:2} case
                reference = QRReferenceUtils.createQrReference(customerId, nr);
            } else if (nr != null) {
                // {1} or {:1} case
                reference = QRReferenceUtils.createQrReference(nr);
            } else if (customerId == null && nr == null) {
                // {} or {:} case
                return null;
            }
        }
        return reference;
    }

    String expandCreditorReference(String reference) {
        if (reference == null) {
            return null;
        }

        final Matcher matcher = CREDITOR_REFERENCE_EXPANSION_PATTERN.matcher(reference);
        if (matcher.matches()) {
            final String nr = StringUtils.emptyStringAsNull(normalize(matcher.group("nr")));
            if (nr != null) {
                reference = CreditorReferenceUtils.createCreditorReference(nr);
            } else {
                // {} case
                return null;
            }
        }
        return reference;
    }

    private void mapAdditionalInformation(final QrInvoiceBuilder builder, final AdditionalInformation additionalInformation) {
        if (additionalInformation == null) {
            return;
        }
        if (additionalInformation.getBillInformationObject() != null) {
            throw new ValidationException("Not yet supported to pass bill information as object");
        }
        builder.additionalInformation().unstructuredMessage(StringUtils.emptyStringAsNull(normalize(additionalInformation.getUnstructuredMessage())));
        builder.additionalInformation().billInformation(StringUtils.emptyStringAsNull(normalize(additionalInformation.getBillInformation())));
    }

    private void mapPaymentAmountInformation(final QrInvoiceBuilder builder, final PaymentAmountInformation paymentAmountInformation) {
        if (paymentAmountInformation == null) {
            return;
        }
        if (paymentAmountInformation.getAmount() != null) {
            builder.paymentAmountInformation().amount(paymentAmountInformation.getAmount());
        }
        if (paymentAmountInformation.getCurrency() != null) {
            builder.paymentAmountInformation().currency(Currency.getInstance(paymentAmountInformation.getCurrency().name()));
        }
    }

    private void mapAlternativeSchemes(final QrInvoiceBuilder builder, final AlternativeSchemes alternativeSchemes) {
        if (alternativeSchemes != null) {

            if (CollectionUtils.isNotEmpty(alternativeSchemes.getAlternativeSchemeParameterObjects())) {
                throw new ValidationException("Not yet supported to pass alternative schemes parameter as objects");
            }

            final List<String> alternativeSchemeParameters = Arrays.stream(alternativeSchemes.getAlternativeSchemeParameters())
                    .map(this::normalize)
                    .collect(Collectors.toList());
            builder.alternativeSchemeParameters(alternativeSchemeParameters);
        }
    }

    private String normalize(String str) {
        if (!normalizeString || str == null) {
            return str;
        }

        return StringNormalizer.create().enableAll().normalize(str);
    }

    private void logCombinedAddressUsage() {
        logger.warn("Combined address is used (phase out november 2025");
    }

}
