/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2.examples;

public class ExampleData {
    public static final String IBAN = "CH3709000000304442225";
    public static final String QR_IBAN = "CH4431999123000889012";
    public static final String QR_REFERENCE = "210000000003139471430009017";
    public static final String CREDITOR_REFERENCE = "RF18539007547034";
    
    public static final String SWICO_S1_V12 = "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30";
    
    public static final String SPC_QR_REFERENCE = "SPC\n" +
            "0200\n" +
            "1\n" +
            "CH4431999123000889012\n" +
            "S\n" +
            "Robert Schneider AG\n" +
            "Rue du Lac\n" +
            "1268\n" +
            "2501\n" +
            "Biel\n" +
            "CH\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "1949.75\n" +
            "CHF\n" +
            "S\n" +
            "Pia-Maria Rutschmann-Schnyder\n" +
            "Grosse Marktgasse\n" +
            "28\n" +
            "9400\n" +
            "Rorschach\n" +
            "CH\n" +
            "QRR\n" +
            "210000000003139471430009017\n" +
            "Instruction of 03.04.2019\n" +
            "EPD\n" +
            "//S1/10/10201409/11/190512/20/1400.000-53/30/106017086/31/180508/32/7.7/40/2:10;0:30\n" +
            "eBill/B/john.doe@example.com\n" +
            "cdbk/some/values";
    
    @Deprecated
    public static final String SPC_COMBINED_ADDRESS = "SPC\n" +
            "0200\n" +
            "1\n" +
            "CH4431999123000889012\n" +
            "K\n" +
            "Robert Schneider AG\n" +
            "Rue du Lac 1268\n" +
            "2501 Biel\n" +
            "\n" +
            "\n" +
            "CH\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "1949.75\n" +
            "CHF\n" +
            "K\n" +
            "Pia-Maria Rutschmann-Schnyder\n" +
            "Grosse Marktgasse 28\n" +
            "9400 Rorschach\n" +
            "\n" +
            "\n" +
            "CH\n" +
            "QRR\n" +
            "210000000003139471430009017\n" +
            "Instruction of 03.04.2019\n" +
            "EPD\n" +
            "\n" +
            "\n" +
            "";

    public static final String SPC_CREDITOR_REFERENCE = "SPC\n" +
            "0200\n" +
            "1\n" +
            "CH5800791123000889012\n" +
            "S\n" +
            "Robert Schneider AG\n" +
            "Rue du Lac\n" +
            "1268\n" +
            "2501\n" +
            "Biel\n" +
            "CH\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "199.95\n" +
            "CHF\n" +
            "S\n" +
            "Pia-Maria Rutschmann-Schnyder\n" +
            "Grosse Marktgasse\n" +
            "28\n" +
            "9400\n" +
            "Rorschach\n" +
            "CH\n" +
            "SCOR\n" +
            "RF18539007547034\n" +
            "\n" +
            "EPD\n" +
            "\n" +
            "\n";


    public static final String SPC_NON_REFERENCE = "SPC\n" +
            "0200\n" +
            "1\n" +
            "CH3709000000304442225\n" +
            "S\n" +
            "Robert Schneider AG\n" +
            "Rue du Lac\n" +
            "1268/2/22\n" +
            "2501\n" +
            "Biel\n" +
            "CH\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "3949.75\n" +
            "CHF\n" +
            "S\n" +
            "Pia-Maria Rutschmann-Schnyder\n" +
            "Grosse Marktgasse\n" +
            "28\n" +
            "9400\n" +
            "Rorschach\n" +
            "CH\n" +
            "NON\n" +
            "\n" +
            "Rechnung Nr. 3139 für Gartenarbeiten und Entsorgung Schnittmaterial\n" +
            "EPD\n" +
            "\n" +
            "\n";

    public static final String SPC_BLANK = "SPC\n" +
            "0200\n" +
            "1\n" +
            "CH3709000000304442225\n" +
            "S\n" +
            "Salvation Army Foundation Switzerland\n" +
            "\n" +
            "\n" +
            "3000\n" +
            "Berne\n" +
            "CH\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "CHF\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "NON\n" +
            "\n" +
            "Donation to the Winterfest Campaign\n" +
            "EPD\n" +
            "\n" +
            "\n";


    public static final String SPC_WITHOUT_AMOUNT = "SPC\n" +
            "0200\n" +
            "1\n" +
            "CH4431999123000889012\n" +
            "S\n" +
            "Salvation Army Foundation Switzerland\n" +
            "\n" +
            "\n" +
            "3000\n" +
            "Berne\n" +
            "CH\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "\n" +
            "CHF\n" +
            "S\n" +
            "Pia-Maria Rutschmann-Schnyder\n" +
            "Grosse Marktgasse\n" +
            "28\n" +
            "9400\n" +
            "Rorschach\n" +
            "CH\n" +
            "QRR\n" +
            "210000000003139471430009017\n" +
            "Donation to the Winterfest Campaign\n" +
            "EPD\n" +
            "\n" +
            "\n";


}
