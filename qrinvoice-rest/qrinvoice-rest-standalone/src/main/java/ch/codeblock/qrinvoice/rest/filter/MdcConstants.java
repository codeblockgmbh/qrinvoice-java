package ch.codeblock.qrinvoice.rest.filter;

public class MdcConstants {
    public static final String TRACE_ID = "traceId";
    public static final String CUSTOMER_ID = "customerId";
    public static final String API_KEY_ID = "apiKeyId";
    public static final String DEMO_MODE = "demoMode";
}
