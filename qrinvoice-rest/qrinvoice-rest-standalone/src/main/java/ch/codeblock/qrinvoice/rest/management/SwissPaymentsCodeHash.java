package ch.codeblock.qrinvoice.rest.management;


import java.util.UUID;

public class SwissPaymentsCodeHash {

    private UUID requestId;
    private String hash;

    public SwissPaymentsCodeHash(final UUID requestId, final String hash) {
        this.requestId = requestId;
        this.hash = hash;
    }

    public UUID getRequestId() {
        return requestId;
    }

    public void setRequestId(final UUID requestId) {
        this.requestId = requestId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(final String hash) {
        this.hash = hash;
    }
}
