/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model;

import ch.codeblock.qrinvoice.documents.model.application.Percentage;
import ch.codeblock.qrinvoice.rest.model.documents.*;
import ch.codeblock.qrinvoice.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

public class InvoiceDocumentOutboundModelMapper {

    public static InvoiceDocumentOutboundModelMapper create() {
        return new InvoiceDocumentOutboundModelMapper();
    }

    public InvoiceDocument map(ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument invoiceDocument) {
        final InvoiceDocument result = new InvoiceDocument();
        result.setSender(map(invoiceDocument.getSender()));
        result.setRecipient(map(invoiceDocument.getRecipient()));
        result.setContactPerson(map(invoiceDocument.getContactPerson()));
        result.setCustomerNr(invoiceDocument.getCustomerNr());
        result.setCustomerReference(invoiceDocument.getCustomerReference());
        result.setInvoiceDate(invoiceDocument.getInvoiceDate());
        result.setInvoiceDueDate(invoiceDocument.getInvoiceDueDate());
        result.setTermOfPaymentDays(invoiceDocument.getTermOfPaymentDays());
        result.setInvoiceNr(invoiceDocument.getInvoiceNr());
        result.setTitle(invoiceDocument.getTitle());
        result.setPrefaceText(invoiceDocument.getPrefaceText());
        result.setEndingText(invoiceDocument.getEndingText());
        result.setPositions(mapPositions(invoiceDocument.getPositions()));
        result.setCurrency(map(invoiceDocument.getCurrency()));
        result.setAdditionalProperties(mapAdditionalProperties(invoiceDocument.getAdditionalProperties()));
        result.setQrInvoice(OutboundModelMapper.create().map(invoiceDocument.getQrInvoice()));

        // remove amount from QrInvoice - gets calculated from positions
        result.getQrInvoice().getPaymentAmountInformation().setAmount(null);

        return result;
    }

    private Position[] mapPositions(List<ch.codeblock.qrinvoice.documents.model.application.Position> positions) {
        if (CollectionUtils.isEmpty(positions)) {
            return new Position[0];
        }


        return positions.stream().map(position -> {
            final Position p = new Position();
            p.setPosition(position.getPosition());
            p.setDescription(position.getDescription());
            p.setQuantity(position.getQuantity());
            p.setUnitPrice(mapAmount(position.getUnitPrice()));
            p.setUnit(position.getUnit());
            p.setDiscountAbsolute(mapAmount(position.getDiscountAbsolute()));
            p.setDiscountPercentage(map(position.getDiscountPercentage()));
            p.setAdditionalProperties(mapAdditionalProperties(position.getAdditionalProperties()));
            return p;
        }).toArray(Position[]::new);
    }

    private AdditionalProperty[] mapAdditionalProperties(List<ch.codeblock.qrinvoice.documents.model.application.AdditionalProperty> additionalProperties) {
        if (CollectionUtils.isEmpty(additionalProperties)) {
            return new AdditionalProperty[0];
        }

        return additionalProperties.stream().map(additionalProperty -> {
            final AdditionalProperty ap = new AdditionalProperty();
            ap.setKey(additionalProperty.getKey());
            ap.setValue(additionalProperty.getValue());
            return ap;
        }).toArray(AdditionalProperty[]::new);
    }

    private Amount mapAmount(ch.codeblock.qrinvoice.documents.model.application.Amount amount) {
        if (amount == null) {
            return null;
        }

        final Amount result = new Amount();
        // only return one value
        if (amount.preferVatExclusive()) {
            result.setVatExclusive(amount.getVatExclusive());
        } else {
            result.setVatInclusive(amount.getVatInclusive());
        }
        result.setVatPercentage(map(amount.getVatPercentage()));
        return result;
    }

    private BigDecimal map(Percentage percentage) {
        if (percentage == null) {
            return null;
        }
        return percentage.getNumericalPercentageValue();
    }

    private CurrencyEnum map(Currency currency) {
        if (currency == null) {
            return null;
        }
        return CurrencyEnum.fromValue(currency.getCurrencyCode());
    }

    private ContactPerson map(ch.codeblock.qrinvoice.documents.model.application.ContactPerson contactPerson) {
        if (contactPerson == null) {
            return null;
        }
        final ContactPerson result = new ContactPerson();
        result.setName(contactPerson.getName());
        result.setEmail(contactPerson.getEmail());
        result.setPhoneNr(contactPerson.getPhoneNr());
        return result;
    }

    private Address map(ch.codeblock.qrinvoice.documents.model.application.Address address) {
        if (address == null) {
            return null;
        }
        final Address result = new Address();
        if (address.getAddressLines() != null) {
            result.setAddressLines(address.getAddressLines().toArray(new String[0]));
        }
        return result;
    }


}
