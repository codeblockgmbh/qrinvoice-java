/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import ch.codeblock.qrinvoice.rest.model.security.ApiKeyRegistry;
import ch.codeblock.qrinvoice.rest.resources.CustomerResourcesRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.TreeSet;

@ExposedApi
@RestController
@RequestMapping("/v2/invoice-document/resources")
@Tag(name = "10 QR Invoice Documents including Payment Part & Receipt (QR Bill)")
public class InvoiceDocumentResourcesController {
    private final ResponseHelper responseHelper;
    private final CustomerResourcesRepository customerResourcesRepository;
    private final ApiKeyRegistry apiKeyRegistry;

    @Autowired
    public InvoiceDocumentResourcesController(final ResponseHelper responseHelper, CustomerResourcesRepository customerResourcesRepository, ApiKeyRegistry apiKeyRegistry) {
        this.responseHelper = responseHelper;
        this.customerResourcesRepository = customerResourcesRepository;
        this.apiKeyRegistry = apiKeyRegistry;
    }

    @Operation(summary = "Add new customer resource")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Resource created or updated successfully")
    })
    @PostMapping(value = "/{filename}",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_PDF_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> addCustomerResource(
            @RequestParam(value = ApiKey.HTTP_REQUEST_PARAM_NAME, required = false) final String apiKey,
            @PathVariable("filename") String filename,
            @Parameter(description = "Base PDF file") @RequestParam("file") final MultipartFile file
    ) {
        try {
            final Integer customerId = apiKeyRegistry.getApiKey(apiKey).map(ApiKey::getCustomerId).orElse(null);
            final Path resourceFile = customerResourcesRepository.writeFile(customerId, filename, file.getInputStream());
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "List customer resources")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "TODO")
    })
    @GetMapping(value = "",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> listCustomerResources(
            @RequestParam(value = ApiKey.HTTP_REQUEST_PARAM_NAME, required = false) final String apiKey) {
        try {
            final Integer customerId = apiKeyRegistry.getApiKey(apiKey).map(ApiKey::getCustomerId).orElse(null);
            final Path customerDir = customerResourcesRepository.getCustomerDir(customerId);
            Set<String> files = new TreeSet<>();
            Files.list(customerDir).map(f -> f.toFile().getName()).forEach(files::add);
            return ResponseEntity.ok(files);
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }
}
