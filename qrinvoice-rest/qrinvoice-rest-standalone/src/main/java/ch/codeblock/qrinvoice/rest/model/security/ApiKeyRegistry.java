/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model.security;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@Component
public class ApiKeyRegistry {
    private final Logger logger = LoggerFactory.getLogger(ApiKeyRegistry.class);

    @Value("${qrinvoice.apikey.path}")
    private String apiKeyPath;

    @Value("${qrinvoice.apikey.enabled}")
    private String apiKeyEnabled;

    private ApiKey[] apiKeys;

    @PostConstruct
    public void init() {
        init(Boolean.parseBoolean(apiKeyEnabled), apiKeyPath);
    }

    void init(boolean apiKeyEnabled, String apiKeyPath) {
        if (apiKeyEnabled) {
            try {
                apiKeys = new ApiKeyReader().readFromFile(apiKeyPath);
            } catch (Exception e) {
                logger.error("Error during read of API Key file from apiKeyPath={}", apiKeyPath, e);
                apiKeys = new ApiKey[0];
            }
        }
    }

    public ApiKey[] getApiKeys() {
        return apiKeys;
    }

    public synchronized void reloadApiKeyFile() throws IOException {
        logger.info("Reloading API Keys from {}", apiKeyPath);
        final ApiKey[] newLoadedApiKeys = new ApiKeyReader().readFromFile(apiKeyPath);
        switchKeys(newLoadedApiKeys);
    }

    /**
     * @param newLoadedApiKeys
     * @return true, if keys were changed
     */
    private synchronized boolean switchKeys(ApiKey[] newLoadedApiKeys) {
        if (newLoadedApiKeys == null || newLoadedApiKeys.length == 0) {
            logger.warn("Reloading API Keys ignored as it was empty");
        } else if (newLoadedApiKeys.length < apiKeys.length - 20) {
            logger.warn("Reloading API Keys ignored as more than 20 API Keys would have been deleted");
        } else {
            if (CollectionUtils.isEqualCollection(Arrays.asList(apiKeys), Arrays.asList(newLoadedApiKeys))) {
                logger.info("Reloading API Keys ignored as keys were unchanged");
            } else {
                logger.info("Reloading API Keys - count={}", newLoadedApiKeys.length);
                this.apiKeys = newLoadedApiKeys;
                return true;
            }
        }
        return false;
    }

    public Optional<ApiKey> getApiKey(String apiKey) {
        if (apiKeys != null) {
            for (ApiKey key : apiKeys) {
                if (key.getApiKey().equals(apiKey)) {
                    return Optional.of(key);
                }
            }
        }
        return Optional.empty();
    }

    public synchronized void updateKeys(ApiKey[] newLoadedApiKeys) throws IOException {
        if (switchKeys(newLoadedApiKeys)) {
            new ApiKeyWriter().writeToFile(apiKeyPath, newLoadedApiKeys);
        }
    }
}
