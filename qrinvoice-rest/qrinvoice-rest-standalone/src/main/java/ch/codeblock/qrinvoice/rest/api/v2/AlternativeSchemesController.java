/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.alternativeschemes.RawAlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.alternativeschemes.RawAlternativeSchemeParameterType;
import ch.codeblock.qrinvoice.model.parser.AlternativeSchemeParameterParser;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters.RawAlternativeSchemeParameterOutboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters.ebill.EBillOutboundModelMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@ExposedApi
@RestController
@RequestMapping("/v2/alternative-schemas/")
@Tag(name = "31 Alternative Schemes")
public class AlternativeSchemesController {

    private final EBillOutboundModelMapper eBillOutboundModelMapper;
    private final RawAlternativeSchemeParameterOutboundModelMapper rawOutboundModelMapper;

    public AlternativeSchemesController(final EBillOutboundModelMapper eBillOutboundModelMapper, final RawAlternativeSchemeParameterOutboundModelMapper rawOutboundModelMapper) {
        this.eBillOutboundModelMapper = eBillOutboundModelMapper;
        this.rawOutboundModelMapper = rawOutboundModelMapper;
    }

    @Operation(summary = "Parse Alternative Schema including")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The parsed Alternative Scheme", content = @Content(schema = @Schema(implementation = ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters.AlternativeSchemeParameter.class))),
            @ApiResponse(responseCode = "422", description = "Unsupported alternative schema structure", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "parse",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> parseAlternativeScheme(@Parameter(description = "Alternative Scheme") @RequestBody final String alternativeSchemeStr) {
        final AlternativeSchemeParameter altPmt = AlternativeSchemeParameterParser.create().parseAlternativeSchemeParameter(alternativeSchemeStr);
        if (altPmt instanceof ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill) {
            final ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill ebill = (ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill) altPmt;
            return ResponseEntity.ok(eBillOutboundModelMapper.map(ebill));
        } else if (altPmt instanceof RawAlternativeSchemeParameter) {
            final RawAlternativeSchemeParameter rawAltPmt = (RawAlternativeSchemeParameter) altPmt;
            return ResponseEntity.ok(rawOutboundModelMapper.map(rawAltPmt));
        } else {
            return ResponseEntity.unprocessableEntity().body("Unsupported alternative schema type");
        }
    }

    @Operation(summary = "Parse Raw Alternative Schema")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The parsed Alternative Schema", content = @Content(schema = @Schema(implementation = ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters.AlternativeSchemeParameter.class))),
            @ApiResponse(responseCode = "422", description = "Unsupported alternative schema structure", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "parse/raw",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> parseRawAltPmt(@Parameter(description = "Alternative Schema") @RequestBody final String alternativeSchemeStr) {
        final RawAlternativeSchemeParameter rawAltPmt = RawAlternativeSchemeParameterType.getInstance().parse(alternativeSchemeStr);
        if (rawAltPmt != null) {
            return ResponseEntity.ok(rawOutboundModelMapper.map(rawAltPmt));
        } else {
            return ResponseEntity.unprocessableEntity().body("Unsupported alternative schema type - only raw supported here");
        }
    }
}
