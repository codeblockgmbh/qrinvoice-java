/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RequestResponseLogger {
    private final Logger logger = LoggerFactory.getLogger(RequestResponseLogger.class);

    public void logRequestData(ch.codeblock.qrinvoice.model.QrInvoice qrInvoice) {
        logger.debug("Request {}", qrInvoice);
    }
    public void logResponseData(ch.codeblock.qrinvoice.model.QrInvoice qrInvoice) {
        logger.debug("Response {}", qrInvoice);
    }

    public void logRequestData(ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument invoiceDocument) {
        logger.debug("Request {}", invoiceDocument);
        
    }
}
