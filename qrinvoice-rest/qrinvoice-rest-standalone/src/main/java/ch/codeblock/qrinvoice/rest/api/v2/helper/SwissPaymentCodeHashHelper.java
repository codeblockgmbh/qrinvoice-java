package ch.codeblock.qrinvoice.rest.api.v2.helper;

import ch.codeblock.qrinvoice.QrInvoiceCodeCreator;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.rest.management.HashCreator;
import ch.codeblock.qrinvoice.rest.management.RequestReporter;
import ch.codeblock.qrinvoice.rest.management.SwissPaymentsCodeHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static ch.codeblock.qrinvoice.rest.filter.MdcConstants.TRACE_ID;

@Component
public class SwissPaymentCodeHashHelper {
    private final Logger logger = LoggerFactory.getLogger(SwissPaymentCodeHashHelper.class);

    private final HashCreator hashCreator;
    private final RequestReporter requestReporter;

    public SwissPaymentCodeHashHelper(final HashCreator hashCreator, @Autowired(required = false) final RequestReporter requestReporter) {
        this.hashCreator = hashCreator;
        this.requestReporter = requestReporter;
    }

    public void report(String swissPaymentsCode) {
        if (requestReporter != null && swissPaymentsCode != null) {
            try {
                final String hash = hashCreator.createMD5Hash(swissPaymentsCode);
                final String requestId = MDC.get(TRACE_ID);
                requestReporter.report(new SwissPaymentsCodeHash(UUID.fromString(requestId), hash));
            } catch (Exception e) {
                logger.warn("Unexpected exception occurred while reporting SPC", e);
            }
        }
    }

    public void report(QrInvoice qrInvoice) {
        if (requestReporter != null) {
            try {
                final String spc = QrInvoiceCodeCreator
                        .create()
                        .qrInvoice(qrInvoice)
                        .createSwissPaymentsCode();
                report(spc);
            } catch (Exception e) {
                logger.warn("Unexpected exception occurred while creating SPC from QrInvoice during SPC reporting", e);
            }
        }
    }
}
