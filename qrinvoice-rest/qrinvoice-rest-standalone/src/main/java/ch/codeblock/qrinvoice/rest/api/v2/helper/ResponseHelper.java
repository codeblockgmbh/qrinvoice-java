/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2.helper;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.output.Output;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Component
public class ResponseHelper {
    private final Logger logger = LoggerFactory.getLogger(ResponseHelper.class);

    public ResponseEntity<?> buildResponse(final Output output, final String fileName) {
        final String mimeType = Optional.ofNullable(output.getOutputFormat()).map(OutputFormat::getMimeType).orElse(null);
        return buildResponse(output.getData(), mimeType, fileName);
    }

    public ResponseEntity<?> buildResponse(final byte[] output, final String fileName) {
        return buildResponse(output, null, fileName);
    }

    public ResponseEntity<?> buildResponse(final byte[] output, final String contentType, final String fileName) {
        HttpHeaders headers = new HttpHeaders();
        if (contentType != null) {
            headers.add("Content-Type", contentType);
        }
        headers.add("Content-Description", "File Transfer");
        headers.add("Content-Disposition", "attachment; filename=" + fileName);
        headers.add("content-length", String.valueOf(output.length));

        return ResponseEntity.ok().headers(headers).body(output);
    }

    public ResponseEntity<String> buildExceptionResponse(final Exception e) {
        logger.info(e.getMessage(), e);
        return utf8(ResponseEntity.badRequest()).body(e.getMessage());
    }

    public ResponseEntity.BodyBuilder okUtf8() {
        return utf8(ResponseEntity.ok());
    }

    public ResponseEntity.BodyBuilder utf8(ResponseEntity.BodyBuilder responseEntity) {
        return responseEntity.header("content-type", "text/plain;charset=UTF-8");
    }


    public ResponseEntity<?> requestedMimeTypeNotSupported(final String accept) {
        return ResponseEntity.badRequest().body(String.format("Requested MimeType (Accept Header) '%s' is not supported", accept));
    }

    public ResponseEntity<?> inputMimeTypeNotSupported(MultipartFile file) {
        return inputMimeTypeNotSupported(StringUtils.isNotBlank(file.getContentType()) ? file.getContentType() : FilenameUtils.getBaseName(file.getOriginalFilename()));
    }

    public ResponseEntity<?> inputMimeTypeNotSupported(final MimeType mimeType) {
        return inputMimeTypeNotSupported(mimeType.getMimeType());
    }

    public ResponseEntity<?> inputMimeTypeNotSupported(final String mimeType) {
        return ResponseEntity
                .unprocessableEntity()
                .contentType(MediaType.TEXT_PLAIN)
                .body(String.format("%s is not yet supported", mimeType));
    }

}
