/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.bulk.csv.Csv;
import ch.codeblock.qrinvoice.bulk.excel.Xlsx;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.model.mapper.SwissPaymentsCodeToModelMapper;
import ch.codeblock.qrinvoice.model.parser.SwissPaymentsCodeParser;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.examples.ExampleData;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import ch.codeblock.qrinvoice.rest.model.OutboundModelMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@ExposedApi
@RestController
@RequestMapping("/v2/examples/qr-invoice")
@Tag(name = "00 Example Data")
public class ExampleQrInvoiceController {

    @Operation(summary = "QrInvoice - Example with QR Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoice")
    })
    @GetMapping(value = "with-qr-reference", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<ch.codeblock.qrinvoice.rest.model.QrInvoice> qrReference() {
        return ResponseEntity.ok(toResponseModel(ExampleData.SPC_QR_REFERENCE));
    }

    @Operation(summary = "QrInvoice - Example with Creditor Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoice")
    })
    @GetMapping(value = "with-creditor-reference", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<ch.codeblock.qrinvoice.rest.model.QrInvoice> creditorReference() {
        return ResponseEntity.ok(toResponseModel(ExampleData.SPC_CREDITOR_REFERENCE));
    }

    @Operation(summary = "QrInvoice - Example without Reference Number")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoice")
    })
    @GetMapping(value = "without-reference", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<ch.codeblock.qrinvoice.rest.model.QrInvoice> nonReference() {
        return ResponseEntity.ok(toResponseModel(ExampleData.SPC_NON_REFERENCE));
    }

    @Operation(summary = "QrInvoice - Example without Amount")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoice")
    })
    @GetMapping(value = "without-amount", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<ch.codeblock.qrinvoice.rest.model.QrInvoice> withoutAmount() {
        return ResponseEntity.ok(toResponseModel(ExampleData.SPC_WITHOUT_AMOUNT));
    }

    @Operation(summary = "QrInvoice - Example without Amount and Debtor (blank)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoice")
    })
    @GetMapping(value = "blank", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<ch.codeblock.qrinvoice.rest.model.QrInvoice> blank() {
        return ResponseEntity.ok(toResponseModel(ExampleData.SPC_BLANK));
    }

    @Operation(summary = "QrInvoice - Example with combined address")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoice")
    })
    @GetMapping(value = "combined-address", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    @Deprecated
    public ResponseEntity<ch.codeblock.qrinvoice.rest.model.QrInvoice> qrReferenceCombinedAddress() {
        return ResponseEntity.ok(toResponseModel(ExampleData.SPC_COMBINED_ADDRESS));
    }

    @Operation(summary = "QrInvoice - Example with a list of QR invoices")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoices")
    })
    @GetMapping(value = "bulk/list", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<List<ch.codeblock.qrinvoice.rest.model.QrInvoice>> list() {
        return ResponseEntity.ok(Arrays.asList(
                toResponseModel(ExampleData.SPC_BLANK),
                toResponseModel(ExampleData.SPC_WITHOUT_AMOUNT),
                toResponseModel(ExampleData.SPC_NON_REFERENCE),
                toResponseModel(ExampleData.SPC_CREDITOR_REFERENCE),
                toResponseModel(ExampleData.SPC_QR_REFERENCE)
        ));
    }

    @Operation(summary = "QrInvoice - Example CSV file that is used as input for bulk operation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoice")
    })
    @GetMapping(value = "bulk/csv", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<?> csv() throws IOException {
        return new ResponseHelper().buildResponse(Csv.getExample(), "example.csv");
    }

    @Operation(summary = "QrInvoice - Example Excel (XLSX) file that is used as input for bulk operation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "QrInvoice")
    })
    @GetMapping(value = "bulk/xlsx", consumes = {MediaType.ALL_VALUE}, produces = {"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
    @ResponseBody
    public ResponseEntity<?> xlsx() throws IOException {
        return new ResponseHelper().buildResponse(Xlsx.getExample(), "example.xlsx");
    }

    private ch.codeblock.qrinvoice.rest.model.QrInvoice toResponseModel(String spcString) {
        final SwissPaymentsCode spc = SwissPaymentsCodeParser.create().parse(spcString);
        final QrInvoice qrInvoice = SwissPaymentsCodeToModelMapper.create().map(spc);
        return OutboundModelMapper.create().map(qrInvoice);
    }
}
