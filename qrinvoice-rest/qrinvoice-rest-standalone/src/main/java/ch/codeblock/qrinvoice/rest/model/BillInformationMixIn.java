package ch.codeblock.qrinvoice.rest.model;

import ch.codeblock.qrinvoice.model.billinformation.RawBillInformation;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

@JsonTypeInfo(use = NAME, include = PROPERTY, property = "billInformationType", defaultImpl = RawBillInformation.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = RawBillInformation.class, name = "RawBillInformation"),
        @JsonSubTypes.Type(value = SwicoS1v12.class, name = "SwicoS1v12")
})
public interface BillInformationMixIn {

}
