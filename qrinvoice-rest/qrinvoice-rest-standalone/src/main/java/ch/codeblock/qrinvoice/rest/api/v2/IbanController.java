/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static ch.codeblock.qrinvoice.util.IbanUtils.formatIban;
import static ch.codeblock.qrinvoice.util.IbanUtils.isValidIBAN;
import static ch.codeblock.qrinvoice.util.IbanUtils.normalizeIBAN;

@ExposedApi
@RestController
@RequestMapping(value = "/v2/iban")
@Tag(name = "20 IBAN")
public class IbanController {

    @Operation(summary = "Validate an IBAN")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "IBAN is valid"),
            @ApiResponse(responseCode = "422", description = "Invalid IBAN")
    })
    @PostMapping(value = "validate",
            consumes = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> validateIban(
            @Parameter(description = "IBAN", content = @Content(schema = @Schema(example = "CH3908704016075473007", hidden = true))) @RequestBody final String iban
    ) {
        if (isValidIBAN(iban, true)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.unprocessableEntity().body("Invalid IBAN: " + iban);
    }

    @Operation(summary = "Format an IBAN. Works for QR-IBAN too.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid IBAN"),
            @ApiResponse(responseCode = "422", description = "Invalid IBAN")
    })
    @PostMapping(value = "format",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> format(
            @Parameter(description = "IBAN", content = @Content(schema = @Schema(example = "CH3908704016075473007"))) @RequestBody final String iban
    ) {
        if (isValidIBAN(iban, false)) {
            return ResponseEntity.ok(formatIban(iban));
        }
        return ResponseEntity.unprocessableEntity().body("Invalid IBAN: " + iban);
    }

    @Operation(summary = "Normalize an IBAN. Works for QR-IBAN too.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Normalized IBAN"),
            @ApiResponse(responseCode = "422", description = "Invalid IBAN")
    })
    @PostMapping(value = "normalize",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> normalize(
            @Parameter(description = "IBAN", content = @Content(schema = @Schema(example = "CH3908704016075473007"))) @RequestBody final String iban
    ) {
        if (isValidIBAN(iban, false)) {
            return ResponseEntity.ok(normalizeIBAN(iban));
        }
        return ResponseEntity.unprocessableEntity().body("Invalid IBAN: " + iban);
    }

}
