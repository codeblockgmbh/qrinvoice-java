/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.examples.ExampleData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@ExposedApi
@RestController
@RequestMapping("/v2/examples/swiss-payments-code")
@Tag(name = "00 Example Data")
public class ExampleSpcController {
    @Operation(summary = "Swiss Payments Code - Example with QR Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Swiss Payments Code (SPC)")
    })
    @GetMapping(value = "with-qr-reference", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> qrReference() {
        return ResponseEntity.ok().header("content-type", "text/plain;charset=UTF-8").body(ExampleData.SPC_QR_REFERENCE);
    }

    @Operation(summary = "Swiss Payments Code - Example with Creditor Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Swiss Payments Code (SPC)")
    })
    @GetMapping(value = "with-creditor-reference", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> creditorReference() {
        return ResponseEntity.ok().header("content-type", "text/plain;charset=UTF-8").body(ExampleData.SPC_CREDITOR_REFERENCE);
    }

    @Operation(summary = "Swiss Payments Code - Example without Reference Number")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Swiss Payments Code (SPC)")
    })
    @GetMapping(value = "without-reference", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> nonReference() {
        return ResponseEntity.ok().header("content-type", "text/plain;charset=UTF-8").body(ExampleData.SPC_NON_REFERENCE);
    }

    @Operation(summary = "Swiss Payments Code - Example without Amount")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Swiss Payments Code (SPC)")
    })
    @GetMapping(value = "without-amount", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> withoutAmount() {
        return ResponseEntity.ok().header("content-type", "text/plain;charset=UTF-8").body(ExampleData.SPC_WITHOUT_AMOUNT);
    }

    @Operation(summary = "Swiss Payments Code - Example without Amount and Debtor (blank)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Swiss Payments Code (SPC)")
    })
    @GetMapping(value = "blank", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> blank() {
        return ResponseEntity.ok().header("content-type", "text/plain;charset=UTF-8").body(ExampleData.SPC_BLANK);
    }
}
