package ch.codeblock.qrinvoice.rest.management;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

public class RequestReporter {
    private final Logger logger = LoggerFactory.getLogger(RequestReporter.class);

    private final ConcurrentLinkedQueue<RequestInfo> infosToReport = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<SwissPaymentsCodeHash> hashesToReport = new ConcurrentLinkedQueue<>();

    private final DataSource dataSource;

    public RequestReporter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void report(RequestInfo toReport) {
        this.infosToReport.offer(toReport);
    }

    public void report(SwissPaymentsCodeHash toReport) {
        this.hashesToReport.offer(toReport);
    }


    @Scheduled(fixedDelay = 20_000) // every 20 sec
    public void reportToDb() {
        if (infosToReport.isEmpty() && hashesToReport.isEmpty()) {
            logger.info("No requests to be reported");
            return;
        }

        try (Connection connection = dataSource.getConnection()) {

            reportRequestInfos(connection);

            reportSwissPaymentCodeHashes(connection);

        } catch (Exception e) {
            logger.warn("Error occurred during reporting of requests", e);
        }

    }

    private void reportSwissPaymentCodeHashes(Connection connection) throws SQLException {
        try(final PreparedStatement stmt = connection.prepareStatement("insert into request_hash_log(request_id, hash) values (?, ?)")) {
            int batchSize = 50;

            SwissPaymentsCodeHash hash = hashesToReport.poll();
            for(int count = 1; hash != null; hash = hashesToReport.poll()) {
                stmt.setObject(1, hash.getRequestId());
                stmt.setString(2, hash.getHash());
                stmt.addBatch();

                if(count % batchSize == 0) {
                    executeBatch(stmt, "request_hash_log");
                }

                count ++;
            }

            // for any remaining items after last batch size hit
            executeBatch(stmt, "request_hash_log");
        }
    }

    private void reportRequestInfos(Connection connection) throws SQLException {
        try (final PreparedStatement stmt = connection.prepareStatement("insert into request_log " +
                "(request_id, \"timestamp\", path, method, status, accept, ms, api_key)\n" +
                "values (?, ?, ?, ?, ?, ?, ?, ?)")) {

            int batchSize = 50;

            RequestInfo info = infosToReport.poll();
            for (int count = 1; info != null; info = infosToReport.poll()) {
                stmt.setObject(1, info.getRequestId());
                stmt.setTimestamp(2, Timestamp.valueOf(info.getTime()));
                stmt.setString(3, info.getUrl());
                stmt.setString(4, info.getMethod());
                stmt.setInt(5, info.getStatus());
                stmt.setString(6, info.getAccept());
                stmt.setLong(7, info.getMillis());
                stmt.setObject(8, UUID.fromString(info.getApiKey()));
                stmt.addBatch();

                if (count % batchSize == 0) {
                    executeBatch(stmt, "request_log");
                }

                count++;
            }

            // for any remaining items after last batch size hit
            executeBatch(stmt, "request_log");
        }
    }

    private void executeBatch(PreparedStatement statement, String kind) throws SQLException {
        final int[] counts = statement.executeBatch();
        logger.info("Reported {} {} records", Arrays.stream(counts).sum(), kind);
    }
}
