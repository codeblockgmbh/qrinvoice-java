/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v1;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.PaymentPartReceiptController;
import ch.codeblock.qrinvoice.rest.api.v2.SwissPaymentsCodeController;
import ch.codeblock.qrinvoice.rest.api.v2.SwissQrCodeController;
import ch.codeblock.qrinvoice.rest.model.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@ExposedApi
@RestController
@RequestMapping("/qr-invoice")
@Tag(name = "XX Deprecated QR Invoice Operations v1")
public class QrInvoiceApiController {

    private final PaymentPartReceiptController paymentPartReceiptController;
    private final SwissQrCodeController swissQrCodeController;
    private final SwissPaymentsCodeController swissPaymentsCodeController;

    public QrInvoiceApiController(final PaymentPartReceiptController paymentPartReceiptController, final SwissQrCodeController swissQrCodeController, final SwissPaymentsCodeController swissPaymentsCodeController) {
        this.paymentPartReceiptController = paymentPartReceiptController;
        this.swissQrCodeController = swissQrCodeController;
        this.swissPaymentsCodeController = swissPaymentsCodeController;
    }

    @Deprecated
    @Operation(summary = "Create the Payment Part & Receipt from the given parameters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The payment part in the requested output format.", content = {
                    @Content(mediaType = "application/pdf"),
                    @Content(mediaType = "image/png"),
                    @Content(mediaType = "image/gif"),
                    @Content(mediaType = "image/jpeg"),
                    @Content(mediaType = "image/bmp"),
                    @Content(mediaType = "image/tiff")
            })
    })
    @PostMapping(value = "/payment-part",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {"application/pdf", "image/png", "image/gif", "image/jpeg", "image/bmp", "image/tiff"}
    )
    @ResponseBody
    public ResponseEntity<? extends Object> paymentPartReceipt(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Locale") @RequestHeader(value = "Accept-Language", required = false, defaultValue = "de") final LanguageEnum language, // TODO check why enum is missing
            @Parameter(description = "Font Family") @RequestParam(value = "fontFamily", required = false, defaultValue = "LIBERATION_SANS") final FontFamilyEnum fontFamily,
            @Parameter(description = "Page Size") @RequestParam(value = "pageSize", required = false, defaultValue = "DIN_LANG") final PageSizeEnum pageSize,
            @Parameter(description = "Output Resolution") @RequestParam(value = "resolution", required = false, defaultValue = "MEDIUM_300_DPI") final OutputResolutionEnum resolution,
            @Parameter(description = "If a line should printed to mark the payment parts and receipts boundary") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If scissors should be printed on the boundary lines") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "true") final Boolean boundaryLineSeparationText,
            @Parameter(description = "QrInvoice") @RequestBody final QrInvoice qrInvoiceRest
    ) {
        return paymentPartReceiptController.createPaymentPartReceipt(accept, language, fontFamily, true, pageSize, resolution, boundaryLines, false, boundaryLineScissors, boundaryLineSeparationText, false, false, qrInvoiceRest);
    }

    @Deprecated
    @Operation(summary = "Create the Swiss QR Code from the given parameters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The QR code in the requested output format.", content = {
                    @Content(mediaType = "application/pdf"),
                    @Content(mediaType = "image/png"),
                    @Content(mediaType = "image/gif"),
                    @Content(mediaType = "image/jpeg"),
                    @Content(mediaType = "image/bmp"),
                    @Content(mediaType = "image/tiff")
            })
    })
    @PostMapping(value = "/swiss-qr-code",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {"application/pdf", "image/png", "image/gif", "image/jpeg", "image/bmp", "image/tiff"}
    )
    @ResponseBody
    public ResponseEntity<? extends Object> swissQrCode(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Output Resolution") @RequestParam(value = "resolution", required = false, defaultValue = "MEDIUM_300_DPI") final OutputResolutionEnum resolution,
            @Parameter(description = "Desired QR Code size (in pixels)", example = "500") @RequestParam(value = "size", required = false) final Integer qrCodeSize,
            @Parameter(description = "QrInvoice", required = true, name = "QrInvoice") @RequestBody final QrInvoice qrInvoiceRest
    ) {
        return swissQrCodeController.create(accept, resolution, qrCodeSize, false, qrInvoiceRest);
    }

    @Deprecated
    @Operation(summary = "Create the Swiss Payments Code from the given parameters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The raw Swiss Payments Code (SPC)")
    })
    @PostMapping(value = "/swiss-payments-code",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> swissPaymentsCode(@RequestBody final QrInvoice qrInvoiceRest) {
        return swissPaymentsCodeController.create(false, qrInvoiceRest);
    }
}
