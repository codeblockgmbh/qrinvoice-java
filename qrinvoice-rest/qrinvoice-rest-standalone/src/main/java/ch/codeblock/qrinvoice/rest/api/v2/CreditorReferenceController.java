/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.util.CreditorReferenceUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static ch.codeblock.qrinvoice.util.CreditorReferenceUtils.*;

@ExposedApi
@RestController
@RequestMapping(value = "/v2/creditor-reference")
@Tag(name = "22 Creditor Reference (SCOR)")
public class CreditorReferenceController {
    @Operation(summary = "Validate a creditor reference number")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Creditor reference is valid", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "422", description = "Invalid creditor reference", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "validate",
            consumes = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> validate(
            @Parameter(description = "creditor reference", content = @Content(schema = @Schema(example = "RF45 1234 5123 45"))) @RequestBody final String creditorReference
    ) {
        if (isValidCreditorReference(creditorReference)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.unprocessableEntity().body("Invalid creditor reference for: " + creditorReference);
    }

    @Operation(summary = "Create a creditor reference. If the passed creditor reference is missing prefix and checksum, it is calculated and appended.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid creditor reference including checksum"),
            @ApiResponse(responseCode = "422", description = "Invalid creditor reference - checksum could not be added")
    })
    @PostMapping(value = "create",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> create(
            @Parameter(description = "Creditor reference", content = @Content(schema = @Schema(example = "42"))) @RequestBody final String creditorReference,
            @Parameter(description = "If true, creditor reference is formatted in response") @RequestParam(required = false, defaultValue = "false") final boolean formatCreditorReference
    ) {
        try {
            return ResponseEntity.ok(formatCreditorReferenceNumber(CreditorReferenceUtils.createCreditorReference(creditorReference), formatCreditorReference));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.unprocessableEntity().body("Invalid creditor reference" + creditorReference + ", could not be created.");
        }
    }

    @Operation(summary = "Extract the actual reference number from a Creditor Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Actual reference number without checksum"),
            @ApiResponse(responseCode = "400", description = "Invalid Creditor Reference")
    })
    @PostMapping(value = "extract",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> extract(
            @Parameter(description = "Creditor reference", content = @Content(schema = @Schema(example = "RF451234512345"))) @RequestBody final String creditorReference,
            @Parameter(description = "If true, leading zeroes (0) are stripped away") @RequestParam(required = false, defaultValue = "true") final boolean stripLeadingZeroes
    ) {
        try {
            return ResponseEntity.ok(CreditorReferenceUtils.extractReferenceNumberFromCreditorReference(creditorReference, stripLeadingZeroes));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body("Invalid Creditor Reference " + creditorReference + ". Reference number could not be extracted.");
        }
    }

    @Operation(summary = "Format a Creditor Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid Creditor Reference"),
            @ApiResponse(responseCode = "422", description = "Invalid Creditor Reference")
    })
    @PostMapping(value = "format",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> format(
            @Parameter(description = "Creditor Reference", content = @Content(schema = @Schema(example = "RF451234512345"))) @RequestBody final String creditorReference
    ) {
        if (isValidCreditorReference(creditorReference)) {
            return ResponseEntity.ok(formatCreditorReference(creditorReference));
        }
        return ResponseEntity.unprocessableEntity().body("Invalid Creditor Reference " + creditorReference + ", could not be formatted.");
    }

    @Operation(summary = "Normalize a Creditor Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Normalized Creditor Reference"),
            @ApiResponse(responseCode = "422", description = "Invalid Creditor Reference")
    })
    @PostMapping(value = "normalize",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> normalize(
            @Parameter(description = "Creditor Reference", content = @Content(schema = @Schema(example = "RF451234512345"))) @RequestBody final String creditorReference
    ) {
        if (isValidCreditorReference(creditorReference)) {
            return ResponseEntity.ok(normalizeCreditorReference(creditorReference));
        }
        return ResponseEntity.unprocessableEntity().body("Invalid Creditor Reference  " + creditorReference + ", could not be normalized.");
    }

    @Operation(summary = "Calculate Check Digits using Modulo 97 if the given input would be added to creditor reference RF[checkdigits][input]")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Check Digits for Input"),
            @ApiResponse(responseCode = "422", description = "Invalid Creditor Reference")
    })
    @PostMapping(value = "modulo97",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> modulo97(
            @Parameter(description = "number", content = @Content(schema = @Schema(example = "42"))) @RequestBody final String number
    ) {
        try {
            return ResponseEntity.ok(calculateCheckDigitsForReferenceNumber(number));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.unprocessableEntity().body("Unprocessable number " + number);
        }
    }

    private String formatCreditorReferenceNumber(String creditorReferenceNumber, boolean creditorReferenceNumberFormat) {
        if (creditorReferenceNumberFormat) {
            return formatCreditorReference(creditorReferenceNumber);
        }
        return creditorReferenceNumber;
    }
}
