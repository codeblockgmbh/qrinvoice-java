/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model.security;

import java.time.ZonedDateTime;
import java.util.Objects;

public class ApiKey {
    public static final String HTTP_REQUEST_HEADER_NAME = "X-API-Key";
    public static final String HTTP_REQUEST_PARAM_NAME = "api_key";
    private String apiKey;
    private int apiKeyId;
    private int customerId;
    private String customerName;
    private boolean demo;
    private boolean trial;
    private boolean production;
    private boolean active;
    private ZonedDateTime validTo;

    public ApiKey() {
    }

    public ApiKey(String apiKey, int apiKeyId, int customerId, String customerName, boolean demo, boolean trial, boolean production, boolean active, ZonedDateTime validTo) {
        this.apiKey = apiKey;
        this.apiKeyId = apiKeyId;
        this.customerId = customerId;
        this.customerName = customerName;
        this.demo = demo;
        this.trial = trial;
        this.production = production;
        this.active = active;
        this.validTo = validTo;
    }

    public int getApiKeyId() {
        return apiKeyId;
    }

    public void setApiKeyId(int apiKeyId) {
        this.apiKeyId = apiKeyId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public boolean isDemo() {
        return demo;
    }

    public void setDemo(boolean demo) {
        this.demo = demo;
    }

    public boolean isTrial() {
        return trial;
    }

    public void setTrial(boolean trial) {
        this.trial = trial;
    }

    public boolean isProduction() {
        return production;
    }

    public void setProduction(final boolean production) {
        this.production = production;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public ZonedDateTime getValidTo() {
        return validTo;
    }

    public void setValidTo(final ZonedDateTime validTo) {
        this.validTo = validTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiKey apiKey1 = (ApiKey) o;
        return customerId == apiKey1.customerId
                && demo == apiKey1.demo
                && trial == apiKey1.trial
                && production == apiKey1.production
                && apiKeyId == apiKey1.apiKeyId
                && apiKey.equals(apiKey1.apiKey)
                && customerName.equals(apiKey1.customerName)
                && active == apiKey1.active
                && Objects.equals(validTo, apiKey1.validTo);
    }



    @Override
    public int hashCode() {
        return Objects.hash(apiKey, apiKeyId, customerId, customerName, demo, trial, production, active, validTo);
    }
}
