/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.filter;

import ch.codeblock.qrinvoice.rest.api.RequestContext;
import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class ApiKeyFilter implements Filter {

    private ServletContext context;

    private final RequestContext requestContext;

    @Value("${qrinvoice.apikey.enabled}")
    private String apiKeyEnabled;

    public ApiKeyFilter(RequestContext requestContext) {
        this.requestContext = requestContext;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.context = filterConfig.getServletContext();
        this.context.log("ApiKeyFilter initialized");
    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;

        try {
            if (Boolean.parseBoolean(apiKeyEnabled)) {
                switch (requestContext.getApiKeyState()) {
                    case VALID:
                        // has to be present (otherwise not VALID) if not, NPE is fine
                        final ApiKey apiKey = requestContext.getApiKey().get();
                        MDC.put(MdcConstants.DEMO_MODE, String.valueOf(apiKey.isDemo()));
                        MDC.put(MdcConstants.CUSTOMER_ID, String.valueOf(apiKey.getCustomerId()));
                        MDC.put(MdcConstants.API_KEY_ID, String.valueOf(apiKey.getApiKeyId()));
                        chain.doFilter(request, response);
                        break;
                    case INVALID:
                        respondUnauthorized(res, "provided API key '" + requestContext.getApiKeyFromRequest() + "' is invalid");
                        break;
                    case AMBIGUOUS:
                        respondUnauthorized(res, "multiple different API keys were provided");
                        break;
                    case MISSING:
                        respondUnauthorized(res, "API key parameter is missing");
                        break;
                    case INACTIVE:
                        respondUnauthorized(res, "provided API key '" + requestContext.getApiKeyFromRequest() + "' is inactive");
                        break;
                    case EXPIRED:
                        respondUnauthorized(res, "provided API key '" + requestContext.getApiKeyFromRequest() + "' is expired");
                        break;
                    default:
                        // just in case new enum value introduced but unhandled
                        respondUnauthorized(res, "n/a");
                        break;
                }
            } else {
                final String serverName = req.getServerName();
                final boolean demoInstance = StringUtils.isNotBlank(serverName) && serverName.contains("demo.qr-invoice");
                MDC.put(MdcConstants.DEMO_MODE, String.valueOf(demoInstance));
                chain.doFilter(request, response);
            }
        } finally {
            MDC.remove(MdcConstants.DEMO_MODE);
            MDC.remove(MdcConstants.CUSTOMER_ID);
            MDC.remove(MdcConstants.API_KEY_ID);
        }
    }

    private void respondUnauthorized(HttpServletResponse res, String msg) throws IOException {
        this.context.log("HTTP Status 401 Unauthorized - " + msg);
        res.setStatus(401);
        res.getWriter().print(msg);
    }

    @Override
    public void destroy() {

    }

}






