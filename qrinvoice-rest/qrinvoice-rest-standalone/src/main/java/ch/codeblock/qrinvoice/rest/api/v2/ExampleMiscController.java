/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.model.SwissPaymentsCode;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.examples.ExampleData;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@ExposedApi
@RestController
@RequestMapping("/v2/examples")
@Tag(name = "00 Example Data")
public class ExampleMiscController {
    @Operation(summary = "IBAN")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "IBAN"))
    @GetMapping(value = "iban", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> iban() {
        return ResponseEntity.ok(ExampleData.IBAN);
    }

    @Operation(summary = "QR-IBAN")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "QR-IBAN"))
    @GetMapping(value = "qr-iban", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> qrIban() {
        return ResponseEntity.ok(ExampleData.QR_IBAN);
    }

    @Operation(summary = "QR Reference")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "QR Reference"))
    @GetMapping(value = "qr-reference", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> qrReference() {
        return ResponseEntity.ok(ExampleData.QR_REFERENCE);
    }

    @Operation(summary = "Creditor Reference")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "Creditor Reference"))
    @GetMapping(value = "creditor-reference", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> creditorReference() {
        return ResponseEntity.ok(ExampleData.CREDITOR_REFERENCE);
    }

    @Operation(summary = "Swico S1 v1.2")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "Swico S1 v1.2"))
    @GetMapping(value = "swicos1v12", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> swicos1v12() {
        return ResponseEntity.ok(ExampleData.SWICO_S1_V12);
    }

    @Operation(summary = "Valid Characters IG 2.0 - 2.2", description = "Valid characters according to specification 2.0 - 2.2 (Implementation Guidelines for the QR-bill)")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "Supported Charset v2.2"))
    @GetMapping(value = "charset/lteV2_2", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> charsetLteV2_2() {
        return ResponseEntity.ok(SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2);
    }

    @Operation(summary = "Valid Characters IG 2.3", description = "Valid characters according to specification 2.3 (Implementation Guidelines for the QR-bill). Newly added characters be used after 21.11.2025")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "Supported Charset v2.3"))
    @GetMapping(value = "charset/gteV2_3", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> charsetGteV2_3() {
        return ResponseEntity.ok(SwissPaymentsCode.VALID_CHARACTERS_GTE_2_3);
    }
    
    @Operation(summary = "Added Characters in IG 2.3", description = "Added characters to allowed set of characters in specification 2.3 (Implementation Guidelines for the QR-bill). Newly added characters be used after 21.11.2025")
    @ApiResponses(value = @ApiResponse(responseCode = "200", description = "Additionally Supported Charset v2.3"))
    @GetMapping(value = "charset/addedInV2_3", consumes = {MediaType.ALL_VALUE}, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public ResponseEntity<String> addedCharsV2_3() {
        final StringBuilder result = new StringBuilder();

        for (char ch : SwissPaymentsCode.VALID_CHARACTERS_GTE_2_3.toCharArray()) {
            if (SwissPaymentsCode.VALID_CHARACTERS_LTE_2_2.indexOf(ch) == -1) {
                result.append(ch);
            }
        }

        return ResponseEntity.ok(result.toString());
    }
}
