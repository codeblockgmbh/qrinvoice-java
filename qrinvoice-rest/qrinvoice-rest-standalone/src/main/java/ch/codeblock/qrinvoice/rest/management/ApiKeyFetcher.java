package ch.codeblock.qrinvoice.rest.management;

import ch.codeblock.qrinvoice.rest.model.security.ApiKeyReader;
import ch.codeblock.qrinvoice.rest.model.security.ApiKeyRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class ApiKeyFetcher {
    private final Logger logger = LoggerFactory.getLogger(ApiKeyFetcher.class);

    private final ApiKeyRegistry apiKeyRegistry;
    private final DataSource dataSource;

    public ApiKeyFetcher(ApiKeyRegistry apiKeyRegistry, DataSource dataSource) {
        this.apiKeyRegistry = apiKeyRegistry;
        this.dataSource = dataSource;
    }

    @Scheduled(initialDelay = 20_000, fixedDelay = 300_000) // every 5 min
    public void fetchApiKeys() {
        logger.info("Going to read latest API Keys from central registry");
        try (Connection connection = dataSource.getConnection()) {
            try (Statement statement = connection.createStatement()) {
                final ResultSet rs = statement.executeQuery("select json from api_keys_json");
                if (rs.next()) {
                    final String json = rs.getString("json");
                    apiKeyRegistry.updateKeys(new ApiKeyReader().read(json));
                }
            }
        } catch (Exception e) {
            logger.warn("Error occurred during fetching of api keys", e);
        }
    }
}
