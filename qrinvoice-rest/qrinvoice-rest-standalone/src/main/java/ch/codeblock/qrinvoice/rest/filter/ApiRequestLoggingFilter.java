/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.filter;

import ch.codeblock.qrinvoice.rest.api.RequestContext;
import ch.codeblock.qrinvoice.rest.api.v2.helper.RequestResponseLogger;
import ch.codeblock.qrinvoice.rest.management.RequestInfo;
import ch.codeblock.qrinvoice.rest.management.RequestReporter;
import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

import static ch.codeblock.qrinvoice.rest.filter.MdcConstants.TRACE_ID;

@Component
public class ApiRequestLoggingFilter implements Filter {

    private static final String ITEM_COUNT = "itemCount";
    private static final Set<String> EXCLUDED_PARAMETERS = new HashSet<>(Arrays.asList(ApiKey.HTTP_REQUEST_PARAM_NAME, "apikey", "qrInvoice", "QrInvoice", "qrInvoices", "QrInvoices"));
    private static final Set<String> INCLUDED_HEADERS = new HashSet<>(Arrays.asList("accept", "accept-language", "content-type", "user-agent"));

    private final Logger logger = LoggerFactory.getLogger(RequestResponseLogger.class);

    private final RequestReporter requestReporter;
    private final RequestContext requestContext;

    public ApiRequestLoggingFilter(@Autowired(required = false) RequestReporter requestReporter, RequestContext requestContext) {
        this.requestReporter = requestReporter;
        this.requestContext = requestContext;
    }

    @Override
    public void init(FilterConfig filterConfig) {
        final ServletContext context = filterConfig.getServletContext();
        context.log("ApiRequestLoggingFilter initialized");
    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        final LocalDateTime time = LocalDateTime.now();
        final long start = System.currentTimeMillis();

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;

        final Map<String, Object> headersToLog = collectHeaders(req);
        final Map<String, Object> paramsToLog = collectParameters(req);

        final StringBuilder sb = new StringBuilder();
        if (logger.isInfoEnabled()) {
            sb.append("url=").append(req.getRequestURL());
        }

        final UUID requestId = UUID.randomUUID();
        try {
            MDC.put(TRACE_ID, requestId.toString());
            chain.doFilter(request, response);
        } finally {
            final String itemCount = MDC.get(ITEM_COUNT) == null ? "1" : MDC.get(ITEM_COUNT);
            MDC.remove(ITEM_COUNT);

            final long end = System.currentTimeMillis();
            if (requestReporter != null) {
                final RequestInfo requestInfo = new RequestInfo();
                requestInfo.setRequestId(requestId);
                requestInfo.setTime(time);
                requestInfo.setUrl(req.getRequestURL().toString());
                requestInfo.setMethod(req.getMethod());
                requestInfo.setStatus(res.getStatus());
                requestInfo.setMillis(end - start);
                requestInfo.setCount(Integer.parseInt(itemCount));
                requestInfo.setHeaders(headersToLog);
                requestInfo.setParams(paramsToLog);
                requestInfo.setApiKey(requestContext.getApiKey().orElse(new ApiKey()).getApiKey());
                requestInfo.setAccept(req.getHeader("accept"));
                requestReporter.report(requestInfo);
            }

            if (logger.isInfoEnabled()) {
                sb.append(" ");
                sb.append("method=").append(req.getMethod());
                sb.append(" ");
                sb.append("status=").append(res.getStatus());
                sb.append(" time=").append((end - start)).append(" ms");
                sb.append(" count=").append(itemCount);
                sb.append(" - ");
                logMap(sb, "headers", headersToLog);
                sb.append(" - ");
                logMap(sb, "params", paramsToLog);
                logger.info(sb.toString());
            }

            MDC.remove(TRACE_ID);
        }
    }

    private Map<String, Object> collectHeaders(final HttpServletRequest req) {
        final Map<String, Object> headersToLog = new LinkedHashMap<>();
        final Enumeration<String> headerNames = req.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            final String headerName = headerNames.nextElement();
            if (INCLUDED_HEADERS.contains(headerName.toLowerCase())) {
                headersToLog.put(headerName, req.getHeader(headerName));
            }
        }
        return headersToLog;
    }

    private Map<String, Object> collectParameters(final HttpServletRequest req) {
        final Map<String, Object> paramsToLog = new LinkedHashMap<>();
        final Map<String, String[]> parameterMap = req.getParameterMap();
        for (final Map.Entry<String, String[]> stringEntry : parameterMap.entrySet()) {
            if (EXCLUDED_PARAMETERS.contains(stringEntry.getKey())) {
                continue;
            }
            if (stringEntry.getValue() == null || stringEntry.getValue().length == 0) {
                paramsToLog.put(stringEntry.getKey(), "");
            } else if (stringEntry.getValue().length == 1) {
                paramsToLog.put(stringEntry.getKey(), stringEntry.getValue()[0]);
            } else {
                paramsToLog.put(stringEntry.getKey(), String.join(", ", stringEntry.getValue()));
            }
        }
        return paramsToLog;
    }

    private void logMap(final StringBuilder sb, final String groupName, final Map<String, Object> map) {
        sb.append(groupName).append(" {");
        boolean first = true;
        for (final Map.Entry<String, Object> entry : map.entrySet()) {
            if (!first) {
                sb.append(", ");
            }
            sb.append(entry.getKey()).append("=").append(entry.getValue());
            first = false;
        }
        sb.append("}");
    }


    public static void setItemCount(int itemCount) {
        MDC.put(ITEM_COUNT, String.valueOf(itemCount));
    }

    @Override
    public void destroy() {

    }

}






