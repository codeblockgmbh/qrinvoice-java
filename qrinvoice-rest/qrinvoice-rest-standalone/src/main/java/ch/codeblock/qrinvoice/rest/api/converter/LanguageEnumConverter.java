package ch.codeblock.qrinvoice.rest.api.converter;

import ch.codeblock.qrinvoice.rest.model.LanguageEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import java.util.Optional;

public class LanguageEnumConverter implements Converter<String, LanguageEnum> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LanguageEnumConverter.class);

    @Override
    public LanguageEnum convert(String source) {
        // always use first value in list e.g. "de, fr-CH" -> "de" -> "GERMAN"
        return Optional.ofNullable(source)
                .map(String::trim)
                .map(s -> s.split(","))
                .map(arr -> arr[0])
                .map(String::trim)
                .map(LanguageEnum::fromValue)
                .orElseGet(() -> {
                    LOGGER.debug("No language detected, falling back to german");
                    return LanguageEnum.de;
                });
    }
}
