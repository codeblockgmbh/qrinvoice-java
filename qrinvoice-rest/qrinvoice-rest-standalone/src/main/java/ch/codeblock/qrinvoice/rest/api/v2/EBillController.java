/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.BaseException;
import ch.codeblock.qrinvoice.model.alternativeschemes.AlternativeSchemeParameter;
import ch.codeblock.qrinvoice.model.parser.AlternativeSchemeParameterParser;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters.ebill.EBill;
import ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters.ebill.EBillInboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.alternativeschemeparameters.ebill.EBillOutboundModelMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@ExposedApi
@RestController
@RequestMapping("/v2/alternative-schemas/ebill")
@Tag(name = "31 Alternative Schemes")
public class EBillController {
    private final ResponseHelper responseHelper;

    private final EBillInboundModelMapper eBillInboundModelMapper;
    private final EBillOutboundModelMapper eBillOutboundModelMapper;

    public EBillController(ResponseHelper responseHelper, final EBillInboundModelMapper eBillInboundModelMapper, final EBillOutboundModelMapper eBillOutboundModelMapper) {
        this.responseHelper = responseHelper;
        this.eBillInboundModelMapper = eBillInboundModelMapper;
        this.eBillOutboundModelMapper = eBillOutboundModelMapper;
    }

    @Operation(summary = "Parse Alternative Scheme as EBill")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The parsed Alternative Scheme", content = @Content(schema = @Schema(implementation = EBill.class))),
            @ApiResponse(responseCode = "422", description = "Unsupported Alternative Scheme - not EBill")
    })
    @PostMapping(value = "parse",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> parseEBill(@Parameter(description = "Alternative Scheme", name = "alternativeScheme", example = "eBill/B/john@example.com/2018-123456-22") @RequestBody final String alternativeSchemeParameterString) {
        try {
            final AlternativeSchemeParameter alternativeSchemeParameter = AlternativeSchemeParameterParser.create().parseAlternativeSchemeParameter(alternativeSchemeParameterString);
            if (alternativeSchemeParameter instanceof ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill) {
                final ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill eBill = (ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill) alternativeSchemeParameter;
                return ResponseEntity.ok(eBillOutboundModelMapper.map(eBill));
            } else {
                return ResponseEntity.unprocessableEntity().body("Unsupported Alternative Scheme type");
            }
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Serialize a EBill into Alternative Scheme string")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The Alternative Scheme string", content = @Content(schema = @Schema(implementation = String.class)))
    })
    @PostMapping(value = "create",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> createAlternativeSchemeParameter(@Parameter(description = "EBill") @RequestBody final EBill eBill) {
        try {
            final ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill eBillMapped = eBillInboundModelMapper.map(eBill);
            eBillMapped.validate().throwExceptionOnErrors();
            return ResponseEntity.ok(eBillMapped.toAlternativeSchemeParameterString());
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }


    @Operation(summary = "Validate a EBill object")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid EBill data", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "422", description = "Invalid EBill data", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "validate",
            consumes = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> validate(@Parameter(description = "EBill") @RequestBody final EBill eBill) {
        try {
            final ch.codeblock.qrinvoice.model.alternativeschemes.ebill.EBill eBillMapped = eBillInboundModelMapper.map(eBill);
            eBillMapped.validate().throwExceptionOnErrors();
            return ResponseEntity.ok().build();
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }
}
