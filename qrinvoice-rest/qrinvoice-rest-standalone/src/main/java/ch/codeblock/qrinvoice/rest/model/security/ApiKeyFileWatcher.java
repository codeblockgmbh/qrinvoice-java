/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ApiKeyFileWatcher {
    private final Logger logger = LoggerFactory.getLogger(ApiKeyFileWatcher.class);

    @Value("${qrinvoice.apikey.path}")
    private String apiKeyPath;

    private final ApiKeyRegistry apiKeyRegistry;

    private ExecutorService executorService;

    @Value("${qrinvoice.apikey.enabled}")
    private String apiKeyEnabled;

    @Autowired
    public ApiKeyFileWatcher(ApiKeyRegistry apiKeyRegistry) {
        this.apiKeyRegistry = apiKeyRegistry;
    }

    @PostConstruct
    public void initFileWatchService() throws IOException {
        if (Boolean.parseBoolean(apiKeyEnabled)) {
            this.executorService = Executors.newSingleThreadExecutor();
            final WatchService watchService = FileSystems.getDefault().newWatchService();
            final Path path = Paths.get(apiKeyPath).getParent();

            if (Files.exists(path)) {
                path.register(
                        watchService,
                        StandardWatchEventKinds.ENTRY_MODIFY);

                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            WatchKey key;
                            while ((key = watchService.take()) != null) {
                                for (WatchEvent<?> event : key.pollEvents()) {
                                    try {
                                        apiKeyRegistry.reloadApiKeyFile();
                                    } catch (IOException e) {
                                        logger.warn("Exception while reloading API Keys from {}", apiKeyPath, e);
                                    }
                                }
                                key.reset();
                            }
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                };
                executorService.submit(runnable);
            } else {
                logger.warn("Path does not exist: {}", path);
            }
        }
    }
}
