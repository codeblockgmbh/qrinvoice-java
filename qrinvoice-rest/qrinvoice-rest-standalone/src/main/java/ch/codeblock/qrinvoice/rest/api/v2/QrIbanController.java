/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static ch.codeblock.qrinvoice.util.IbanUtils.isQrIBAN;

@ExposedApi
@RestController
@RequestMapping(value = "/v2/qr-iban")
@Tag(name = "20 IBAN")
public class QrIbanController {

    @Operation(summary = "Validate a QR IBAN. QR IBANs are regular IBANs. The only special thing is that the IID (institutional identification) is in the QR-IID range. " +
            "QR-IIDs consist exclusively of numbers from 30000 to 31999.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "QR-IBAN  is valid"),
            @ApiResponse(responseCode = "422", description = "Invalid QR-IBAN")
    })
    @PostMapping(value = "/validate",
            consumes = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> validateQrIban(
            @Parameter(description = "QR-IBAN", content = @Content(schema = @Schema(example = "CH44 3199 9123 0008 8901 2"))) @RequestBody final String qrIban
    ) {
        if (isQrIBAN(qrIban)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.unprocessableEntity().body("Invalid QR-IBAN for: " + qrIban);
    }
}
