/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.OutputFormat;
import ch.codeblock.qrinvoice.documents.DocumentTemplateRepository;
import ch.codeblock.qrinvoice.documents.QrInvoiceDocumentCreator;
import ch.codeblock.qrinvoice.documents.model.DocumentTemplate;
import ch.codeblock.qrinvoice.documents.model.application.builder.DocumentLayoutBuilder;
import ch.codeblock.qrinvoice.documents.model.application.layout.DocumentLayout;
import ch.codeblock.qrinvoice.documents.model.application.layout.Resource;
import ch.codeblock.qrinvoice.documents.output.QrInvoiceDocument;
import ch.codeblock.qrinvoice.documents.xsl.XslFoProcessor;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.helper.RequestParamHelper;
import ch.codeblock.qrinvoice.rest.api.v2.helper.RequestResponseLogger;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import ch.codeblock.qrinvoice.rest.api.v2.helper.SwissPaymentCodeHashHelper;
import ch.codeblock.qrinvoice.rest.model.InvoiceDocumentInboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.LanguageEnum;
import ch.codeblock.qrinvoice.rest.model.documents.InvoiceDocument;
import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import ch.codeblock.qrinvoice.rest.model.security.ApiKeyRegistry;
import ch.codeblock.qrinvoice.rest.resources.CustomerResourcesRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

import static org.apache.commons.lang3.BooleanUtils.toBooleanDefaultIfNull;

@ExposedApi
@RestController
@RequestMapping("/v2/invoice-document")
@Tag(name = "10 QR Invoice Documents including Payment Part & Receipt (QR Bill)")
public class InvoiceDocumentController {
    private final RequestResponseLogger requestResponseLogger;
    private final ResponseHelper responseHelper;
    private final RequestParamHelper requestParamHelper;
    private final ApiKeyRegistry apiKeyRegistry;
    private final CustomerResourcesRepository customerResourcesRepository;
    private final SwissPaymentCodeHashHelper hashReportHelper;

    @Autowired
    public InvoiceDocumentController(final RequestResponseLogger requestResponseLogger, final ResponseHelper responseHelper, final RequestParamHelper requestParamHelper, final ApiKeyRegistry apiKeyRegistry, final CustomerResourcesRepository customerResourcesRepository, final SwissPaymentCodeHashHelper hashReportHelper) {
        this.requestResponseLogger = requestResponseLogger;
        this.hashReportHelper = hashReportHelper;
        this.responseHelper = responseHelper;
        this.requestParamHelper = requestParamHelper;
        this.apiKeyRegistry = apiKeyRegistry;
        this.customerResourcesRepository = customerResourcesRepository;
    }

    @Operation(summary = "Create a QR Invoice Document (QR Bill)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The QR Invoice Document as PDF")
    })
    @PostMapping(
            value = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {"application/pdf"}
    )
    @ResponseBody
    public ResponseEntity<?> createInvoiceDocument(
            @Parameter(hidden = true) @RequestParam(value = ApiKey.HTTP_REQUEST_PARAM_NAME, required = false) final String apiKey,
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Locale") @RequestHeader(value = "Accept-Language", required = false, defaultValue = "de") final LanguageEnum language,
            @Parameter(description = "If a line should be printed to mark the payment parts and receipts boundary. This should be true in all cases except the invoice is printed to perforated paper.") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If the boundary lines should include a margin and not extend to the border of the page. Printers usually are unable to print the lines completely otherwise.") @RequestParam(value = "boundaryLinesMargins", required = false, defaultValue = "false") final Boolean boundaryLinesMargins,
            @Parameter(description = "If scissors should be printed on the boundary lines. Only to be set to true if parameter boundaryLines is set to true.") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part. This can be used as an alternative or in addition to boundaryLineScissors.") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "false") final Boolean boundaryLineSeparationText,
            @Parameter(description = "If an additional print margin (1mm) should be added to the left, right and bottom of the payment part & receipt. If set, 6mm instead of 5mm print margin is used. This may be needed some printers and print services") @RequestParam(value = "additionalPrintMargin", required = false, defaultValue = "false") final Boolean additionalPrintMargin,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "The Logo to be used - must be previously uploaded as customer resource") @RequestParam(value = "logoResource", required = false) final String logoResource,
            @Parameter(description = "Invoice Document Template") @RequestParam(value = "templateId", required = false, defaultValue = "STANDARD_RECIPIENT_RIGHT_V1") final String templateId,
            @Parameter(description = "True if the total amount in the QR Payment Part & Receipt should be calculated") @RequestParam(value = "calculateAmount", required = false, defaultValue = "true") final Boolean calculateAmount,
            @Parameter(description = "The InvoiceDocument") @RequestBody final InvoiceDocument invoiceDocument
    ) {
        final Optional<OutputFormat> optionalOutputFormat = OutputFormat.getByMimeType(accept);
        if (!optionalOutputFormat.isPresent()) {
            return responseHelper.requestedMimeTypeNotSupported(accept);
        }

        try {

            final DocumentLayout invoiceLayout = createDocumentLayout(apiKey, templateId, logoResource);
            final ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument mappedInvoiceDocument = mapInvoiceDocument(invoiceDocument, calculateAmount, normalizeInput);

            requestResponseLogger.logRequestData(mappedInvoiceDocument);

            final QrInvoiceDocument document = QrInvoiceDocumentCreator.create()
                    .invoice(mappedInvoiceDocument)
                    .invoiceLayout(invoiceLayout)
                    .locale(requestParamHelper.toLocale(language))
                    .boundaryLines(requestParamHelper.toBoundaryLines(boundaryLines, boundaryLinesMargins))
                    .boundaryLineScissors(toBooleanDefaultIfNull(boundaryLineScissors, true))
                    .boundaryLineSeparationText(toBooleanDefaultIfNull(boundaryLineSeparationText, false))
                    .additionalPrintMargin(toBooleanDefaultIfNull(additionalPrintMargin, false))
                    .createQrInvoiceDocument();

            hashReportHelper.report(mappedInvoiceDocument.getQrInvoice());

            return responseHelper.buildResponse(document, "QR-Invoice.pdf");
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Create an InvoiceDocument xml template model")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The InvoiceDocument xml template model")
    })
    @PostMapping(
            path = "/xml-template-model",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.TEXT_XML_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> createInvoiceDocumentTemplateModel(
            @Parameter(hidden = true) @RequestParam(value = ApiKey.HTTP_REQUEST_PARAM_NAME, required = false) final String apiKey,
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Locale") @RequestHeader(value = "Accept-Language", required = false, defaultValue = "de") final LanguageEnum language,
            @Parameter(description = "If a line should be printed to mark the payment parts and receipts boundary. This should be true in all cases except the invoice is printed to perforated paper.") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If the boundary lines should include a margin and not extend to the border of the page. Printers usually are unable to print the lines completely otherwise.") @RequestParam(value = "boundaryLinesMargins", required = false, defaultValue = "false") final Boolean boundaryLinesMargins,
            @Parameter(description = "If scissors should be printed on the boundary lines. Only to be set to true if parameter boundaryLines is set to true.") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part. This can be used as an alternative or in addition to boundaryLineScissors.") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "false") final Boolean boundaryLineSeparationText,
            @Parameter(description = "If an additional print margin (1mm) should be added to the left, right and bottom of the payment part & receipt. If set, 6mm instead of 5mm print margin is used. This may be needed some printers and print services") @RequestParam(value = "additionalPrintMargin", required = false, defaultValue = "false") final Boolean additionalPrintMargin,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "The Logo to be used - must be previously uploaded as customer resource") @RequestParam(value = "logoResource", required = false) final String logoResource,
            @Parameter(description = "Invoice Document Template") @RequestParam(value = "templateId", required = false, defaultValue = "STANDARD_RECIPIENT_RIGHT_V1") final String templateId,
            @Parameter(description = "True if the total amount in the QR Payment Part & Receipt should be calculated") @RequestParam(value = "calculateAmount", required = false, defaultValue = "true") final Boolean calculateAmount,
            @Parameter(description = "The InvoiceDocument") @RequestBody final InvoiceDocument invoiceDocument
    ) {
        try {
            final DocumentLayout invoiceLayout = createDocumentLayout(apiKey, templateId, logoResource);
            final ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument mappedInvoiceDocument = mapInvoiceDocument(invoiceDocument, calculateAmount, normalizeInput);

            requestResponseLogger.logRequestData(mappedInvoiceDocument);

            final ch.codeblock.qrinvoice.documents.model.template.InvoiceDocument document = QrInvoiceDocumentCreator.create()
                    .invoice(mappedInvoiceDocument)
                    .invoiceLayout(invoiceLayout)
                    .locale(requestParamHelper.toLocale(language))
                    .boundaryLines(requestParamHelper.toBoundaryLines(boundaryLines, boundaryLinesMargins))
                    .boundaryLineScissors(toBooleanDefaultIfNull(boundaryLineScissors, true))
                    .boundaryLineSeparationText(toBooleanDefaultIfNull(boundaryLineSeparationText, false))
                    .additionalPrintMargin(toBooleanDefaultIfNull(additionalPrintMargin, false))
                    .createInvoiceDocument();

            hashReportHelper.report(mappedInvoiceDocument.getQrInvoice());

            return ResponseEntity.ok(XslFoProcessor.writeValueAsString(document));
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    public Resource mapLogoResource(Optional<Path> logoFileOptional) {
        if (logoFileOptional.isPresent()) {
            final Path logoFile = logoFileOptional.get();

            final MimeType mimeType = MimeType.getByFilename(logoFile.toFile().getName()).orElseThrow(() -> new ValidationException("Unsupported mimeType"));
            return new Resource(mimeType, logoFile.toFile());
        }
        return null;
    }

    private ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument mapInvoiceDocument(InvoiceDocument invoiceDocument, Boolean calculateAmount, Boolean normalizeInput) {
        final ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument mappedInvoiceDocument = InvoiceDocumentInboundModelMapper.create(normalizeInput).map(invoiceDocument)
                .calculate();
        if (calculateAmount) {
            final QrInvoice qrInvoice = mappedInvoiceDocument.getQrInvoice();
            if (qrInvoice.getPaymentAmountInformation().getAmount() == null) {
                mappedInvoiceDocument.applyCurrencyAmount(qrInvoice);
            } else {
                throw new ValidationException("Requested amount calculation for the QR Payment Part & Receipt but value was already set in the QrInvoice object");
            }
        }
        return mappedInvoiceDocument;
    }

    private DocumentLayout createDocumentLayout(String apiKey, String templateId, String logoResource) throws IOException {
        final Integer customerId = apiKeyRegistry.getApiKey(apiKey).map(ApiKey::getCustomerId).orElse(null);
        final Optional<Path> logoFile = customerResourcesRepository.getFile(customerId, logoResource);


        final DocumentTemplate documentTemplate = DocumentTemplateRepository.getByExternalIdentifier(templateId)
                .orElseThrow(() -> new ValidationException("Unable to find template with identifier: " + templateId));

        return DocumentLayoutBuilder
                .create()
                .documentTemplate(documentTemplate)
                .logo(mapLogoResource(logoFile))
                .build();
    }
}
