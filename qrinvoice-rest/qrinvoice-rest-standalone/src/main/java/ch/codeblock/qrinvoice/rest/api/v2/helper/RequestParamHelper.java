package ch.codeblock.qrinvoice.rest.api.v2.helper;


import ch.codeblock.qrinvoice.FontFamily;
import ch.codeblock.qrinvoice.OutputResolution;
import ch.codeblock.qrinvoice.PageSize;
import ch.codeblock.qrinvoice.TechnicalException;
import ch.codeblock.qrinvoice.paymentpartreceipt.BoundaryLines;
import ch.codeblock.qrinvoice.rest.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@Component
public class RequestParamHelper {
    private final Logger logger = LoggerFactory.getLogger(RequestParamHelper.class);
    private final ObjectMapper objectMapper;

    public RequestParamHelper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public OutputResolution toOutputResolution(final OutputResolutionEnum resolution) {
        return resolution != null ? OutputResolution.valueOf(resolution.getCode()) : null;
    }

    public PageSize toPageSize(final PageSizeEnum pageSizeEnum) {
        return PageSize.valueOf(pageSizeEnum.getCode());
    }

    public FontFamily toFontFamily(final FontFamilyEnum fontFamilyEnum) {
        return fontFamilyEnum != null ? FontFamily.valueOf(fontFamilyEnum.name()) : FontFamily.LIBERATION_SANS;
    }

    public BoundaryLines toBoundaryLines(final Boolean boundaryLines, final Boolean boundaryLinesMargins) {
        BoundaryLines boundaryLinesChosen = BoundaryLines.ENABLED; // Default to enabled

        if (boundaryLines != null) {
            boundaryLinesChosen = boundaryLines ? BoundaryLines.ENABLED : BoundaryLines.NONE;

            if (boundaryLines && boundaryLinesMargins != null) {
                boundaryLinesChosen = boundaryLinesMargins ? BoundaryLines.ENABLED_WITH_MARGINS : BoundaryLines.ENABLED;
            }
        }
        return boundaryLinesChosen;
    }

    public Locale toLocale(final LanguageEnum languageEnum) {
        return new Locale(languageEnum.name());
    }

    public QrInvoice convertQrInvoiceQueryParamToObject(String qrInvoiceStr) {
        try {
            final String decodedQrInvoiceStr = URLDecoder.decode(qrInvoiceStr, StandardCharsets.UTF_8.name());
            return objectMapper.readValue(decodedQrInvoiceStr, QrInvoice.class);
        } catch (Exception e) {
            logger.warn("Error trying to decode and map a qrInvoice query param string to QrInvoice object", e);
            throw new TechnicalException("Unable to decode or map qrInvoice query param. Make sure it is properly URL encoded using UTF8");
        }
    }

}
