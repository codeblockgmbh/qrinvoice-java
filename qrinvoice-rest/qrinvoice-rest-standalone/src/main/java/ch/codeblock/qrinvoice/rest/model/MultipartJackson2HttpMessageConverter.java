package ch.codeblock.qrinvoice.rest.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

@Component
public class MultipartJackson2HttpMessageConverter extends AbstractJackson2HttpMessageConverter {
    /**
     * Converter for support http request with header Content-Type: multipart/form-data
     * <p>
     * Note: This is for multipart form bodies which contain application/json data but do not have the
     * appropriate content type set.
     * <p>
     * Concrete utilisation: The OpenAPI 3 / SwaggerUI does not set the content type of the body
     * in the /v2/payment-part-receipt/merge and /v2/payment-part-receipt/append endpoints.
     * <p>
     * This converter allows these methods to work from the UI and also makes the lives of API consumers easier.
     */
    public MultipartJackson2HttpMessageConverter(ObjectMapper objectMapper) {
        super(objectMapper, MediaType.APPLICATION_OCTET_STREAM);
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return false;
    }

    @Override
    public boolean canWrite(Type type, Class<?> clazz, MediaType mediaType) {
        return false;
    }

    @Override
    protected boolean canWrite(MediaType mediaType) {
        return false;
    }
}