/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api;

import ch.codeblock.qrinvoice.rest.filter.MdcConstants;
import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import ch.codeblock.qrinvoice.rest.model.security.ApiKeyRegistry;
import ch.codeblock.qrinvoice.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import static ch.codeblock.qrinvoice.util.StringUtils.isNotBlank;
import static ch.codeblock.qrinvoice.util.StringUtils.trimToNull;

@Component
@RequestScope
public class RequestContext {
    private final Logger logger = LoggerFactory.getLogger(RequestContext.class);

    private final ApiKeyState apiKeyState;
    private String apiKeyFromRequest;
    private final Optional<ApiKey> apiKey;
    private boolean demoInstanceRequest;

    public RequestContext(HttpServletRequest httpServletRequest, ApiKeyRegistry apiKeyRegistry) {
        final String serverName = httpServletRequest.getServerName();
        if (isNotBlank(serverName) && serverName.contains("demo.qr-invoice")) {
            this.demoInstanceRequest = true;
        }

        final String apiKeyFromHeader = trimToNull(httpServletRequest.getHeader(ApiKey.HTTP_REQUEST_HEADER_NAME));
        final String apiKeyFromQueryParam = trimToNull(httpServletRequest.getParameter(ApiKey.HTTP_REQUEST_PARAM_NAME));

        if (isNotBlank(apiKeyFromHeader) && isNotBlank(apiKeyFromQueryParam) &&
                !apiKeyFromHeader.equals(apiKeyFromQueryParam)) {
            apiKeyState = ApiKeyState.AMBIGUOUS;
            this.apiKey = Optional.empty();
        } else {
            // Check for API key in request header (preferred way)
            String apiKey = apiKeyFromHeader;
            if (apiKey == null || apiKey.isEmpty()) {
                // Alternatively, check request parameters for api key
                apiKey = apiKeyFromQueryParam;
            }

            this.apiKeyFromRequest = apiKey;

            if (StringUtils.isNotBlank(apiKey)) {
                this.apiKey = apiKeyRegistry.getApiKey(apiKey);
                this.apiKeyState = ApiKeyState.forKey(this.apiKey.orElse(null));
            } else {
                this.apiKeyState = ApiKeyState.MISSING;
                this.apiKey = Optional.empty();
            }
        }
    }

    public ApiKeyState getApiKeyState() {
        return apiKeyState;
    }

    public String getApiKeyFromRequest() {
        return apiKeyFromRequest;
    }

    public Optional<ApiKey> getApiKey() {
        return apiKey;
    }

    public boolean isDemoInstanceRequest() {
        return demoInstanceRequest;
    }

    public boolean isDemo() {
        try {
            if (apiKey.isPresent()) {
                final ApiKey apiKey = this.apiKey.get();
                if (apiKey.isDemo()) {
                    // paranoid double check. if API key from request context is a demo one, double check customer id against MDC
                    if (String.valueOf(apiKey.getCustomerId()).equals(MDC.get(MdcConstants.CUSTOMER_ID))) {
                        logger.debug("Demo API Key");
                        return true;
                    } else {
                        logger.warn("API Key resolved in RequestContext does not match the one given in MDC - API Key customerId={} vs MDC={}",
                                apiKey.getCustomerId(),
                                MDC.get(MdcConstants.CUSTOMER_ID));
                        return false;
                    }
                }
            }

            return demoInstanceRequest;
        } catch (Throwable t) {
            logger.warn("Error while check if demo mode should be enabled", t);
            return false;
        }
    }
}
