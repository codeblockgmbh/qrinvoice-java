package ch.codeblock.qrinvoice.rest.management;

import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.*;
import java.time.ZonedDateTime;
import java.util.UUID;

public class ApiKeyIssuer {
    private final Logger logger = LoggerFactory.getLogger(ApiKeyIssuer.class);
    private final DataSource dataSource;

    public ApiKeyIssuer(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ApiKey newApiKey(int customerId, String customerName, boolean demo, boolean trial, boolean production, ZonedDateTime expirationDate) throws SQLException {
        if(expirationDate == null && trial) {
            expirationDate = ZonedDateTime.now().plusDays(30).withHour(23).withMinute(59).withSecond(59).withNano(999999999);
        }
        UUID uuid = UUID.randomUUID();
        final ApiKey apiKey = new ApiKey(uuid.toString(), 0, customerId, customerName, demo, trial, production, true, expirationDate);

        try (Connection connection = dataSource.getConnection()) {
            insertCustomer(connection, customerId, customerName);

            final int apiKeyId = insertApiKey(connection, apiKey);
            apiKey.setApiKeyId(apiKeyId);
            return apiKey;
        }
    }

    private void insertCustomer(Connection connection, int customerId, String customerName) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement("insert into customer(id, name) values (?,?) on conflict do nothing")) {
            statement.setInt(1, customerId);
            statement.setString(2, customerName);
            statement.execute();
        }
    }

    private int insertApiKey(Connection connection, ApiKey apiKey) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement("insert into api_keys(customer_id, api_key, demo, trial, production, active, valid_to) values (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, apiKey.getCustomerId());
            statement.setObject(2, UUID.fromString(apiKey.getApiKey()));
            statement.setBoolean(3, apiKey.isDemo());
            statement.setBoolean(4, apiKey.isTrial());
            statement.setBoolean(5, apiKey.isProduction());
            statement.setBoolean(6, true);
            Timestamp timestamp = apiKey.getValidTo() != null ? java.sql.Timestamp.from(apiKey.getValidTo().toInstant()) : null;
            statement.setTimestamp(7, timestamp);
            statement.execute();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            return resultSet.getInt(1);
        }
    }
}

