/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.*;
import ch.codeblock.qrinvoice.model.ParseException;
import ch.codeblock.qrinvoice.model.util.DemoValues;
import ch.codeblock.qrinvoice.model.validation.QrInvoiceValidator;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import ch.codeblock.qrinvoice.output.QrCode;
import ch.codeblock.qrinvoice.qrcode.*;
import ch.codeblock.qrinvoice.rest.api.RequestContext;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.helper.RequestParamHelper;
import ch.codeblock.qrinvoice.rest.api.v2.helper.RequestResponseLogger;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import ch.codeblock.qrinvoice.rest.api.v2.helper.SwissPaymentCodeHashHelper;
import ch.codeblock.qrinvoice.rest.model.InboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.OutboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.OutputResolutionEnum;
import ch.codeblock.qrinvoice.rest.model.QrInvoice;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@ExposedApi
@RestController
@RequestMapping("/v2/swiss-qr-code")
@Tag(name = "12 Swiss QR Code")
public class SwissQrCodeController {
    private final RequestContext requestContext;
    private final ResponseHelper responseHelper;
    private final RequestResponseLogger requestResponseLogger;
    private final SwissPaymentCodeHashHelper hashReportHelper;
    private final RequestParamHelper requestParamHelper;

    public SwissQrCodeController(RequestContext requestContext, final RequestResponseLogger requestResponseLogger, final ResponseHelper responseHelper, final SwissPaymentCodeHashHelper hashReportHelper, RequestParamHelper requestParamHelper) {
        this.requestContext = requestContext;
        this.requestResponseLogger = requestResponseLogger;
        this.responseHelper = responseHelper;
        this.hashReportHelper = hashReportHelper;
        this.requestParamHelper = requestParamHelper;
    }

    @Operation(summary = "Create Swiss QR Code")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The QR code in the requested output format.")
    })
    @PostMapping(
            value = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {"application/pdf", "image/png", "image/gif", "image/jpeg", "image/bmp", "image/tiff", "image/svg+xml"}
    )
    @ResponseBody
    public ResponseEntity<?> create(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Output Resolution.") @RequestParam(value = "resolution", required = false, defaultValue = "MEDIUM_300_DPI") final OutputResolutionEnum resolution,
            @Parameter(description = "Desired QR Code size (in pixels). This is an alternative to Output Resolution. This is only relevant for raster graphics (e.g. PNG). Please note that it is only a hint.", content = @Content(schema = @Schema(example = "500"))) @RequestParam(value = "size", required = false) final Integer qrCodeSize,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "QrInvoice", name = "QrInvoice") @RequestBody final QrInvoice qrInvoice
    ) {
        try {
            return createInternal(accept, resolution, qrCodeSize, normalizeInput, qrInvoice);
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Create Swiss QR Code - Prefer POST endpoint if possible")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The QR code in the requested output format.")
    })
    @GetMapping(
            value = "",
            produces = {"application/pdf", "image/png", "image/gif", "image/jpeg", "image/bmp", "image/tiff", "image/svg+xml"}
    )
    @ResponseBody
    public ResponseEntity<?> create(
            @Parameter(description = "Output Format") @RequestParam(value = "format", required = false, defaultValue = "application/pdf") String outputFormat,
            @Parameter(description = "Output Resolution.") @RequestParam(value = "resolution", required = false, defaultValue = "MEDIUM_300_DPI") final OutputResolutionEnum resolution,
            @Parameter(description = "Desired QR Code size (in pixels). This is an alternative to Output Resolution. This is only relevant for raster graphics (e.g. PNG). Please note that it is only a hint.", content = @Content(schema = @Schema(example = "500"))) @RequestParam(value = "size", required = false) final Integer qrCodeSize,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "QrInvoice json document, URL encoded using UTF8") @RequestParam(value = "qrInvoice") final String qrInvoiceStr
    ) throws JsonProcessingException {
        try {
            final QrInvoice qrInvoice = requestParamHelper.convertQrInvoiceQueryParamToObject(qrInvoiceStr);
            return createInternal(outputFormat, resolution, qrCodeSize, normalizeInput, qrInvoice);
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    private ResponseEntity<?> createInternal(String accept, OutputResolutionEnum resolution, Integer qrCodeSize, Boolean normalizeInput, QrInvoice qrInvoice) {
        final Map<String, Object> logParams = new HashMap<>();
        logParams.put("accept", accept);
        logParams.put("resolution", resolution);
        logParams.put("size", qrCodeSize);

        final Optional<OutputFormat> outputFormat = OutputFormat.getByMimeType(accept);
        if (!outputFormat.isPresent()) {
            return responseHelper.requestedMimeTypeNotSupported(accept);
        }

        final ch.codeblock.qrinvoice.model.QrInvoice qrInvoiceMapped = InboundModelMapper.create(normalizeInput).map(qrInvoice);
        requestResponseLogger.logRequestData(qrInvoiceMapped);
        if (requestContext.isDemo()) {
            DemoValues.apply(qrInvoiceMapped);
        }

        final QrInvoiceCodeCreator qrInvoiceCodeCreator = QrInvoiceCodeCreator
                .create()
                .qrInvoice(qrInvoiceMapped)
                .outputFormat(outputFormat.get());

        if (qrCodeSize != null && qrCodeSize > QrCodeWriterOptions.DESIRED_QR_CODE_SIZE_UNSET) {
            qrInvoiceCodeCreator.desiredQrCodeSize(qrCodeSize);
        } else {
            qrInvoiceCodeCreator.outputResolution(OutputResolution.valueOf(resolution.getCode()));
        }

        final QrCode qrCode = qrInvoiceCodeCreator.createQrCode();

        hashReportHelper.report(qrInvoiceMapped);

        return buildResponse(qrCode);
    }

    @Operation(summary = "Validate file containing a single Swiss QR Code")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The validation result")
    })
    @PostMapping(value = "validate",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> validate(@Parameter(description = "QrInvoice", name = "QR-Code") @RequestParam("file") final MultipartFile file) {
        try {
            final ch.codeblock.qrinvoice.model.QrInvoice qrInvoice = QrInvoiceCodeScanner.create().scan(file.getBytes());
            final ValidationResult validationResult = QrInvoiceValidator.create(ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion.latestActive()).validate(qrInvoice);
            if (validationResult.isValid()) {
                return ResponseEntity.ok().build();
            } else {
                final String validationErrorSummary = validationResult.getValidationErrorSummary();
                return responseHelper.okUtf8().body(validationErrorSummary);
            }
        } catch (BaseException | IOException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Parse / extract Swiss QR Code from document (Returns first code if multiple present)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The parsed QR Invoice structure", content = @Content(schema = @Schema(implementation = QrInvoice.class))),
            @ApiResponse(responseCode = "404", description = "No Swiss QR Code could be parsed / extracted from the given document")
    })
    @PostMapping(value = "/parse",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> parse(@Parameter(description = "If true, bill information will be expanded as object if present") @RequestParam(value = "expandBillInformation", defaultValue = "false") final boolean expandBillInformation,
                                   @Parameter(description = "If HARDER, service tries harder to detect QR codes. Operation is slower when efforts are strengthened, but detection rate is higher.") @RequestParam(value = "effortLevel", defaultValue = "DEFAULT") final ScanningEffortLevel effortLevel,
                                   @Parameter(description = "QR bill to parse. Supported are PDFs and raster graphics such as JPEG and PNG.") @RequestParam("file") final MultipartFile file) {
        final Optional<QrInvoiceDocumentScanner> documentScannerOptional = getDocumentScanner(file.getContentType(), effortLevel);
        if (!documentScannerOptional.isPresent()) {
            return missingDocumentScanner(file.getContentType());
        }
        final QrInvoiceDocumentScanner documentScanner = documentScannerOptional.get();

        try (final InputStream imageInputStream = file.getInputStream()) {
            final Optional<ch.codeblock.qrinvoice.model.QrInvoice> qrInvoiceOptional = documentScanner.scanDocumentUntilFirstSwissQrCode(imageInputStream);
            if (qrInvoiceOptional.isPresent()) {
                return ResponseEntity.ok(validateAndMap(qrInvoiceOptional.get(), expandBillInformation));
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (DecodeException | ValidationException | ParseException | IOException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Parse / extract all Swiss QR Code from document in an array")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The parsed QR Invoices structure", content = @Content(schema = @Schema(implementation = QrInvoice.class))),
            @ApiResponse(responseCode = "404", description = "No Swiss QR Code could be parsed / extracted from the given document")
    })
    @PostMapping(value = "/parseAll",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> parseAll(@Parameter(description = "If true, bill information will be expanded as object if present") @RequestParam(value = "expandBillInformation", defaultValue = "false") final boolean expandBillInformation,
                                      @Parameter(description = "If HARDER, service tries harder to detect QR codes. Operation is slower when efforts are strengthened, but detection rate is higher.") @RequestParam(value = "effortLevel", defaultValue = "DEFAULT") final ScanningEffortLevel effortLevel,
                                      @Parameter(description = "QR bill to parse. Supported are PDFs and raster graphics such as JPEG and PNG.") @RequestParam("file") final MultipartFile file) {
        final Optional<QrInvoiceDocumentScanner> documentScannerOptional = getDocumentScanner(file.getContentType(), effortLevel);
        if (!documentScannerOptional.isPresent()) {
            return missingDocumentScanner(file.getContentType());
        }
        final QrInvoiceDocumentScanner documentScanner = documentScannerOptional.get();

        try (final InputStream imageInputStream = file.getInputStream()) {
            final List<QrInvoice> qrInvoices = documentScanner.scanDocumentForAllSwissQrCodes(imageInputStream)
                    .stream()
                    .map(qrInvoice -> validateAndMap(qrInvoice, expandBillInformation))
                    .collect(Collectors.toList());

            if (qrInvoices.isEmpty()) {
                return ResponseEntity.notFound().build();
            } else {
                return ResponseEntity.ok(qrInvoices);
            }
        } catch (DecodeException | ValidationException | ParseException | IOException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    private QrInvoice validateAndMap(final ch.codeblock.qrinvoice.model.QrInvoice qrInvoice, final boolean expandBillInformation) {
        QrInvoiceValidator.create(ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion.latestActive()).validate(qrInvoice).throwExceptionOnErrors();

        if (expandBillInformation) {
            QrInvoiceCodeParser.create().applyBillInformationObject(qrInvoice);
        }

        requestResponseLogger.logResponseData(qrInvoice);

        return OutboundModelMapper.create().map(qrInvoice, expandBillInformation);
    }

    private Optional<QrInvoiceDocumentScanner> getDocumentScanner(final String contentType, ScanningEffortLevel effortLevel) {
        try {
            final QrCodeReaderOptions options;
            if (effortLevel == ScanningEffortLevel.HARDER) {
                options = new QrCodeReaderOptions(effortLevel, QrCodeLibrary.ALL);
            } else {
                options = new QrCodeReaderOptions(QrCodeLibrary.DEFAULT, QrCodeLibrary.DEFAULT);
            }
            return Optional.of(QrInvoiceDocumentScanner.create(contentType, options));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private ResponseEntity<?> missingDocumentScanner(final String contentType) {
        return ResponseEntity
                .unprocessableEntity()
                .contentType(MediaType.TEXT_PLAIN)
                .body(String.format("%s is not yet supported", contentType));
    }

    private ResponseEntity<?> buildResponse(final QrCode output) {
        return responseHelper.buildResponse(output, String.format("%s.%s", output.getClass().getSimpleName(), output.getOutputFormat().getFileExtension()));
    }

}
