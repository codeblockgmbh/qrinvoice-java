/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model;

import ch.codeblock.qrinvoice.model.CombinedAddress;
import ch.codeblock.qrinvoice.model.StructuredAddress;
import ch.codeblock.qrinvoice.model.billinformation.RawBillInformation;
import ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12;
import ch.codeblock.qrinvoice.rest.model.billinformation.RawBillInformationOutboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.S1OutboundModelMapper;

public class OutboundModelMapper {
    public static OutboundModelMapper create() {
        return new OutboundModelMapper();
    }

    public QrInvoice map(ch.codeblock.qrinvoice.model.QrInvoice qrInvoice) {
        return map(qrInvoice, false);
    }

    public QrInvoice map(ch.codeblock.qrinvoice.model.QrInvoice qrInvoice, boolean expandBillInformation) {
        if (qrInvoice == null) {
            return null;
        }

        final QrInvoice result = new QrInvoice();

        result.setCreditorInformation(mapCreditorInformation(qrInvoice.getCreditorInformation()));
        result.setUltimateCreditor(mapUltimateCreditor(qrInvoice.getUltimateCreditor()));
        result.setUltimateDebtor(mapUltimateDebtor(qrInvoice.getUltimateDebtor()));
        result.setPaymentReference(mapPaymentReference(qrInvoice.getPaymentReference(), expandBillInformation));
        result.setPaymentAmountInformation(mapPaymentAmountInformation(qrInvoice.getPaymentAmountInformation()));
        result.setAlternativeSchemes(mapAlternativeSchemes(qrInvoice.getAlternativeSchemes()));

        return result;
    }


    private CreditorInformation mapCreditorInformation(final ch.codeblock.qrinvoice.model.CreditorInformation creditorInformation) {
        if (creditorInformation == null) {
            return null;
        }

        final CreditorInformation result = new CreditorInformation();
        result.setIban(creditorInformation.getIban());
        result.setCreditor(mapCreditor(creditorInformation.getCreditor()));
        return result;
    }

    private Creditor mapCreditor(final ch.codeblock.qrinvoice.model.Creditor creditor) {
        if (creditor == null) {
            return null;
        }

        final Creditor result = new Creditor();
        result.setName(creditor.getName());

        switch (creditor.getAddressType()) {
            case STRUCTURED:
                result.setAddressType(AddressTypeEnum.STRUCTURED);
                result.setStreetName(((StructuredAddress) creditor).getStreetName());
                result.setHouseNumber(((StructuredAddress) creditor).getHouseNumber());
                result.setPostalCode(((StructuredAddress) creditor).getPostalCode());
                result.setCity(((StructuredAddress) creditor).getCity());
                break;
            case COMBINED:
                result.setAddressType(AddressTypeEnum.COMBINED);
                result.setAddressLine1(((CombinedAddress) creditor).getAddressLine1());
                result.setAddressLine2(((CombinedAddress) creditor).getAddressLine2());
                break;
        }

        result.setCountry(creditor.getCountry());
        return result;
    }

    private UltimateCreditor mapUltimateCreditor(final ch.codeblock.qrinvoice.model.UltimateCreditor ultimateCreditor) {
        if (ultimateCreditor == null) {
            return null;
        }

        final UltimateCreditor result = new UltimateCreditor();
        result.setName(ultimateCreditor.getName());

        switch (ultimateCreditor.getAddressType()) {
            case STRUCTURED:
                result.setAddressType(AddressTypeEnum.STRUCTURED);
                result.setStreetName(((StructuredAddress) ultimateCreditor).getStreetName());
                result.setHouseNumber(((StructuredAddress) ultimateCreditor).getHouseNumber());
                result.setPostalCode(((StructuredAddress) ultimateCreditor).getPostalCode());
                result.setCity(((StructuredAddress) ultimateCreditor).getCity());
                break;
            case COMBINED:
                result.setAddressType(AddressTypeEnum.COMBINED);
                result.setAddressLine1(((CombinedAddress) ultimateCreditor).getAddressLine1());
                result.setAddressLine2(((CombinedAddress) ultimateCreditor).getAddressLine2());
                break;
        }

        result.setCountry(ultimateCreditor.getCountry());
        return result;
    }


    private UltimateDebtor mapUltimateDebtor(final ch.codeblock.qrinvoice.model.UltimateDebtor ultimateDebtor) {
        if (ultimateDebtor == null) {
            return null;
        }

        final UltimateDebtor result = new UltimateDebtor();

        result.setName(ultimateDebtor.getName());

        switch (ultimateDebtor.getAddressType()) {
            case STRUCTURED:
                result.setAddressType(AddressTypeEnum.STRUCTURED);
                result.setStreetName(((StructuredAddress) ultimateDebtor).getStreetName());
                result.setHouseNumber(((StructuredAddress) ultimateDebtor).getHouseNumber());
                result.setPostalCode(((StructuredAddress) ultimateDebtor).getPostalCode());
                result.setCity(((StructuredAddress) ultimateDebtor).getCity());
                break;
            case COMBINED:
                result.setAddressType(AddressTypeEnum.COMBINED);
                result.setAddressLine1(((CombinedAddress) ultimateDebtor).getAddressLine1());
                result.setAddressLine2(((CombinedAddress) ultimateDebtor).getAddressLine2());
                break;
        }

        result.setCountry(ultimateDebtor.getCountry());
        return result;
    }

    private PaymentReference mapPaymentReference(final ch.codeblock.qrinvoice.model.PaymentReference paymentReference, final boolean expandBillInformation) {
        if (paymentReference == null) {
            return null;
        }

        final PaymentReference result = new PaymentReference();
        result.setReferenceType(ReferenceTypeEnum.valueOf(paymentReference.getReferenceType().name()));
        result.setReference(paymentReference.getReference());
        result.setAdditionalInformation(mapAdditionalInformation(paymentReference.getAdditionalInformation(), expandBillInformation));
        return result;
    }

    private AdditionalInformation mapAdditionalInformation(final ch.codeblock.qrinvoice.model.AdditionalInformation additionalInformation, final boolean expandBillInformation) {
        if (additionalInformation == null) {
            return null;
        }

        final AdditionalInformation result = new AdditionalInformation();
        result.setBillInformation(additionalInformation.getBillInformation());
        if (expandBillInformation) {
            if (additionalInformation.getBillInformationObject() instanceof SwicoS1v12) {
                result.setBillInformationObject(new S1OutboundModelMapper().map((SwicoS1v12) additionalInformation.getBillInformationObject()));
            } else if (additionalInformation.getBillInformationObject() instanceof RawBillInformation) {
                result.setBillInformationObject(new RawBillInformationOutboundModelMapper().map((RawBillInformation) additionalInformation.getBillInformationObject()));
            }
        }
        result.setUnstructuredMessage(additionalInformation.getUnstructuredMessage());
        return result;
    }

    private PaymentAmountInformation mapPaymentAmountInformation(final ch.codeblock.qrinvoice.model.PaymentAmountInformation paymentAmountInformation) {
        if (paymentAmountInformation == null) {
            return null;
        }

        final PaymentAmountInformation result = new PaymentAmountInformation();
        result.setAmount(paymentAmountInformation.getAmount());
        result.setCurrency(CurrencyEnum.fromValue(paymentAmountInformation.getCurrency().getCurrencyCode()));
        return result;
    }


    private AlternativeSchemes mapAlternativeSchemes(final ch.codeblock.qrinvoice.model.AlternativeSchemes alternativeSchemes) {
        if (alternativeSchemes == null) {
            return null;
        }

        if (alternativeSchemes.getAlternativeSchemeParameters() != null) {
            final AlternativeSchemes result = new AlternativeSchemes();
            result.setAlternativeSchemeParameters(alternativeSchemes.getAlternativeSchemeParameters().toArray(new String[0]));
            return result;
        } else {
            return null;
        }
    }

}
