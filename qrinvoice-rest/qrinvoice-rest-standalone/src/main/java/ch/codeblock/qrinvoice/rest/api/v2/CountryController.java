/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.util.CountryUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@ExposedApi
@RestController
@RequestMapping(value = "/v2/country")
@Tag(name = "23 Country Codes")
public class CountryController {
    private List<Country> countries;
    private List<String> countryCodes;

    @PostConstruct
    public void init() {
        final ResourceBundle countriesBundle = CountryUtils.getCountries();
        final ResourceBundle en = CountryUtils.getCountries(Locale.ENGLISH);
        final ResourceBundle de = CountryUtils.getCountries(Locale.GERMAN);
        final ResourceBundle fr = CountryUtils.getCountries(Locale.FRENCH);
        final ResourceBundle it = CountryUtils.getCountries(Locale.ITALIAN);
        final List<Country> countries = countriesBundle
                .keySet()
                .stream()
                .map(key -> new Country(key, en.getString(key), de.getString(key), fr.getString(key), it.getString(key)))
                .sorted()
                .collect(Collectors.toList());

        countryCodes = Collections.unmodifiableList(countries.stream().map(Country::getCode).collect(Collectors.toList()));

        moveElement(countries, "CH", 0);
        moveElement(countries, "LI", 1);
        moveElement(countries, "DE", 2);
        moveElement(countries, "AT", 3);
        moveElement(countries, "FR", 4);
        moveElement(countries, "IT", 5);
        this.countries = Collections.unmodifiableList(countries);
    }

    private void moveElement(final List<Country> countries, String code, int targetIndex) {
        for (int i = 0; i < countries.size(); i++) {
            if (code.equals(countries.get(i).getCode())) {
                countries.add(targetIndex, countries.remove(i));
                return;
            }
        }
    }

    @Operation(summary = "List Countries")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "List of Countries according to ISO 3166-1 alpha-2")
    })
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<List<Country>> list() {
        return ResponseEntity.ok(countries);
    }

    @Operation(summary = "List Country Codes")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "List of Country Codes according to ISO 3166-1 alpha-2")
    })
    @GetMapping(value = "codes", produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<List<String>> listCodes() {
        return ResponseEntity.ok(countryCodes);
    }

    @Operation(summary = "Validate a Country Code against list of ISO 3166-1 alpha-2")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid Country Code", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "422", description = "Invalid Country Code", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "validate",
            consumes = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> validate(
            @Parameter(description = "Country Code", schema = @Schema(example = "CH")) @RequestBody final String countryCode
    ) {
        if (CountryUtils.isValidIsoCode(countryCode)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.unprocessableEntity().body("Invalid Country Code. " + countryCode);
    }

    public static class Country implements Comparable<Country> {

        @Schema(required = true, description = "The alpha-2 value", example = "CH")
        private String code;
        @Schema(description = "English name", example = "Switzerland")
        private String english;
        @Schema(description = "German name", example = "Schweiz")
        private String german;
        @Schema(description = "French name", example = "Suisse")
        private String french;
        @Schema(description = "Italian name", example = "Svizzera")
        private String italian;

        public Country(final String code, final String english, final String german, final String french, final String italian) {
            this.code = code;
            this.english = english;
            this.german = german;
            this.french = french;
            this.italian = italian;
        }

        public String getCode() {
            return code;
        }

        public void setCode(final String code) {
            this.code = code;
        }

        public String getEnglish() {
            return english;
        }

        public void setEnglish(final String english) {
            this.english = english;
        }

        public String getGerman() {
            return german;
        }

        public void setGerman(final String german) {
            this.german = german;
        }

        public String getFrench() {
            return french;
        }

        public void setFrench(final String french) {
            this.french = french;
        }

        public String getItalian() {
            return italian;
        }

        public void setItalian(final String italian) {
            this.italian = italian;
        }

        @Override
        public int compareTo(final Country o) {
            if (o != null) {
                return code.compareTo(o.code);
            } else {
                return 0;
            }
        }
    }

}
