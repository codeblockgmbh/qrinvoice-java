package ch.codeblock.qrinvoice.rest.management;

import ch.codeblock.qrinvoice.rest.model.security.ApiKeyRegistry;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;

@Configuration
@AutoConfigureAfter(DataSourceAutoConfiguration.class)
@ConditionalOnBean(DataSource.class)
@EnableScheduling
public class CloudManagementConfiguration {

    @Bean
    public ApiKeyFetcher apiKeyFetcher(ApiKeyRegistry apiKeyRegistry, DataSource dataSource) {
        return new ApiKeyFetcher(apiKeyRegistry, dataSource);
    }

    @Bean
    public RequestReporter requestReporter(DataSource dataSource) {
        return new RequestReporter(dataSource);
    }

    @Bean
    public ApiKeyIssuer apiKeyIssuer(DataSource dataSource) {
        return new ApiKeyIssuer(dataSource);
    }
}
