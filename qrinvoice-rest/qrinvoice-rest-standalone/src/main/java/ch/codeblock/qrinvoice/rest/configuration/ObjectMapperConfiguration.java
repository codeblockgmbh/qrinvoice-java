package ch.codeblock.qrinvoice.rest.configuration;

import ch.codeblock.qrinvoice.rest.model.BillInformationMixIn;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class ObjectMapperConfiguration {

    private final ObjectMapper mapper;

    public ObjectMapperConfiguration(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @PostConstruct
    private void configureMixIn() {
        mapper.addMixIn(ch.codeblock.qrinvoice.rest.model.billinformation.BillInformation.class, BillInformationMixIn.class);
        mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
    }
}
