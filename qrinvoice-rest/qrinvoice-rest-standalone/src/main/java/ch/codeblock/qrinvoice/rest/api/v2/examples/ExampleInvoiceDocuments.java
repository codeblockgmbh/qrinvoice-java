/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2.examples;

import ch.codeblock.qrinvoice.documents.model.application.AdditionalProperty;
import ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument;
import ch.codeblock.qrinvoice.documents.model.application.builder.AmountBuilder;
import ch.codeblock.qrinvoice.documents.model.application.builder.InvoiceDocumentBuilder;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Currency;

public class ExampleInvoiceDocuments {
    public static final InvoiceDocument INVOICE_DOCUMENT_MIXED_VAT = createMixedVatDocument();
    public static final InvoiceDocument INVOICE_DOCUMENT_DISCOUNTS = createDiscountsDocument();
    public static final InvoiceDocument INVOICE_DOCUMENT_GTIN = createGtinDocument();

    private static InvoiceDocument createDiscountsDocument() {
        final InvoiceDocumentBuilder invoiceBuilder = InvoiceDocumentBuilder.create()
                .invoiceNr("re-00013")
                .title("QR-Invoice Java Solutions")
                .customerNr("123")
                .customerReference("Kundenreferenz")
                .invoiceDate(LocalDate.now())
                .invoiceDueDate(LocalDate.now().plusDays(30))
                .termOfPaymentDays(30)
                .contactPerson(c -> c
                        .name("Claude Gex")
                        .email("claude.gex@codeblock.ch")
                )
                .currency(Currency.getInstance("CHF"))
                .prefaceText(t -> t
                        .append("Sehr geehrte Damen und Herren")
                        .lineBreak()
                        .append("Wir danken Ihnen für Ihren Auftrag und stellen Ihnen wie folgt in Rechnung")
                )
                .endingText(t -> t
                        .append("Für Fragen stehen wir Ihnen jederzeit gerne zur Verfügung.")
                        .lineBreaks(2)
                        .append("Freundliche Grüsse")
                        .lineBreaks(3)
                        .append("Hans Muster AG")
                )
                .sender(s -> s
                        .addAddressLine("Hans Muster AG")
                        .addAddressLine("Hauptstrasse 1")
                        .addAddressLine("3232 Ins")
                        .addAddressLine("Schweiz")
                )
                .recipient(r -> r
                        .addAddressLine("Kunde AG")
                        .addAddressLine("Am Weg 42")
                        .addAddressLine("3000 Bern")
                        .addAddressLine("Schweiz")
                )
                .addPosition(p -> p
                        .position(1)
                        .description(t -> t
                                .append("This is simple but long string with lorem ipsum text")
                                .lineBreaks(2)
                                .append("generating some sort of natural text in order to test text wrapping")
                        )
                        .quantity(12)
                        .unitPriceVatExclusive(10.0)
                        .vatPercentage(7.7)
                        .discountPercentage(10)
                )
                .addPosition(p -> p
                        .position(2)
                        .description("short text")
                        .quantity(3.2)
                        .unitPriceVatExclusive(42.1)
                        .vatPercentage(7.7)
                        .discountAbsolute(AmountBuilder.create().vatPercentage(7.7).vatExclusive(25).build())
                )
                .addPosition(p -> p
                        .position(3)
                        .description("Mineral")
                        .unit("Stk")
                        .quantity(44)
                        .unitPriceVatExclusive(1.0)
                        .vatPercentage(2.5)
                )
                .addPosition(p -> p
                        .position(4)
                        .description("Customer discount")
                        .quantity(1)
                        .unitPriceVatExclusive(-100.0)
                        .vatPercentage(7.7)
                );

        final InvoiceDocument invoiceDocument = invoiceBuilder.build().calculate();

        final QrInvoice qrInvoice = invoiceDocument.applyCurrencyAmount(QrInvoiceBuilder.create())
                .creditorIBAN("CH44 3199 9123 0008 8901 2")
                .creditor(c -> c
                        .structuredAddress()
                        .name("Hans Muster AG")
                        .streetName("Hauptstrasse")
                        .houseNumber("1")
                        .postalCode("3232")
                        .city("Ins")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .combinedAddress()
                        .name("Kunde AG")
                        .addressLine1("Am Weg 42")
                        .addressLine2("3000 Bern")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference("210000000003139471430009017")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Invoice Document Example")
                )
                .build();
        invoiceDocument.setQrInvoice(qrInvoice);

        return invoiceDocument;
    }

    private static InvoiceDocument createGtinDocument() {
        final InvoiceDocumentBuilder invoiceBuilder = InvoiceDocumentBuilder.create()
                .invoiceNr("re-00013")
                .title("QR-Invoice Java Solutions")
                .customerNr("123")
                .customerReference("Kundenreferenz")
                .invoiceDate(LocalDate.now())
                .invoiceDueDate(LocalDate.now().plusDays(30))
                .termOfPaymentDays(30)
                .contactPerson(c -> c
                        .name("Claude Gex")
                        .email("claude.gex@codeblock.ch")
                )
                .currency(Currency.getInstance("CHF"))
                .prefaceText(t -> t
                        .append("Sehr geehrte Damen und Herren")
                        .lineBreak()
                        .append("Wir danken Ihnen für Ihren Auftrag und stellen Ihnen wie folgt in Rechnung")
                )
                .endingText(t -> t
                        .append("Für Fragen stehen wir Ihnen jederzeit gerne zur Verfügung.")
                        .lineBreaks(2)
                        .append("Freundliche Grüsse")
                        .lineBreaks(3)
                        .append("Hans Muster AG")
                )
                .sender(s -> s
                        .addAddressLine("Hans Muster AG")
                        .addAddressLine("Hauptstrasse 1")
                        .addAddressLine("3232 Ins")
                        .addAddressLine("Schweiz")
                )
                .recipient(r -> r
                        .addAddressLine("Kunde AG")
                        .addAddressLine("Am Weg 42")
                        .addAddressLine("3000 Bern")
                        .addAddressLine("Schweiz")
                )
                .addPosition(p -> p
                        .position(1)
                        .description(t -> t
                                .append("This is simple but long string with lorem ipsum text")
                                .lineBreaks(2)
                                .append("generating some sort of natural text in order to test text wrapping")
                        )
                        .quantity(12)
                        .unitPriceVatExclusive(10.0)
                        .vatPercentage(7.7)
                        .additionalProperties(Collections.singletonList(new AdditionalProperty("GTIN", "01827304879014")))
                )
                .addPosition(p -> p
                        .position(2)
                        .description("short text")
                        .quantity(3.2)
                        .unitPriceVatExclusive(42.1)
                        .vatPercentage(7.7)
                        .additionalProperties(Collections.singletonList(new AdditionalProperty("GTIN", "01827304879021")))
                )
                .addPosition(p -> p
                        .position(3)
                        .description("Mineral")
                        .unit("Stk")
                        .quantity(44)
                        .unitPriceVatExclusive(1.0)
                        .vatPercentage(2.5)
                        .additionalProperties(Collections.singletonList(new AdditionalProperty("GTIN", "01827304879038")))
                )
                .addPosition(p -> p
                        .description("This is a text only position")
                );

        final InvoiceDocument invoiceDocument = invoiceBuilder.build().calculate();

        final QrInvoice qrInvoice = invoiceDocument.applyCurrencyAmount(QrInvoiceBuilder.create())
                .creditorIBAN("CH44 3199 9123 0008 8901 2")
                .creditor(c -> c
                        .structuredAddress()
                        .name("Hans Muster AG")
                        .streetName("Hauptstrasse")
                        .houseNumber("1")
                        .postalCode("3232")
                        .city("Ins")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .combinedAddress()
                        .name("Kunde AG")
                        .addressLine1("Am Weg 42")
                        .addressLine2("3000 Bern")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference("210000000003139471430009017")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Invoice Document Example")
                )
                .build();
        invoiceDocument.setQrInvoice(qrInvoice);

        return invoiceDocument;
    }
    private static InvoiceDocument createMixedVatDocument() {
        final InvoiceDocumentBuilder invoiceBuilder = InvoiceDocumentBuilder.create()
                .invoiceNr("re-00013")
                .title("QR-Invoice Java Solutions")
                .customerNr("123")
                .customerReference("Kundenreferenz")
                .invoiceDate(LocalDate.now())
                .invoiceDueDate(LocalDate.now().plusDays(30))
                .termOfPaymentDays(30)
                .contactPerson(c -> c
                        .name("Claude Gex")
                        .email("claude.gex@codeblock.ch")
                )
                .currency(Currency.getInstance("CHF"))
                .prefaceText(t -> t
                        .append("Sehr geehrte Damen und Herren")
                        .lineBreak()
                        .append("Wir danken Ihnen für Ihren Auftrag und stellen Ihnen wie folgt in Rechnung")
                )
                .endingText(t -> t
                        .append("Für Fragen stehen wir Ihnen jederzeit gerne zur Verfügung.")
                        .lineBreaks(2)
                        .append("Freundliche Grüsse")
                        .lineBreaks(3)
                        .append("Hans Muster AG")
                )
                .sender(s -> s
                        .addAddressLine("Hans Muster AG")
                        .addAddressLine("Hauptstrasse 1")
                        .addAddressLine("3232 Ins")
                        .addAddressLine("Schweiz")
                )
                .recipient(r -> r
                        .addAddressLine("Kunde AG")
                        .addAddressLine("Am Weg 42")
                        .addAddressLine("3000 Bern")
                        .addAddressLine("Schweiz")
                )
                .addPosition(p -> p
                        .position(1)
                        .description(t -> t
                                .append("This is simple but long string with lorem ipsum text")
                                .lineBreaks(2)
                                .append("generating some sort of natural text in order to test text wrapping")
                        )
                        .quantity(12)
                        .unitPriceVatExclusive(10.0)
                        .vatPercentage(7.7)
                )
                .addPosition(p -> p
                        .position(2)
                        .description("short text")
                        .quantity(3.2)
                        .unitPriceVatExclusive(42.1)
                        .vatPercentage(7.7)
                )
                .addPosition(p -> p
                        .position(3)
                        .description("Mineral")
                        .unit("Stk")
                        .quantity(44)
                        .unitPriceVatExclusive(1.0)
                        .vatPercentage(2.5)
                )
                .addPosition(p -> p
                        .description("This is a text only position")
                );

        final InvoiceDocument invoiceDocument = invoiceBuilder.build().calculate();

        final QrInvoice qrInvoice = invoiceDocument.applyCurrencyAmount(QrInvoiceBuilder.create())
                .creditorIBAN("CH44 3199 9123 0008 8901 2")
                .creditor(c -> c
                        .structuredAddress()
                        .name("Hans Muster AG")
                        .streetName("Hauptstrasse")
                        .houseNumber("1")
                        .postalCode("3232")
                        .city("Ins")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .combinedAddress()
                        .name("Kunde AG")
                        .addressLine1("Am Weg 42")
                        .addressLine2("3000 Bern")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference("210000000003139471430009017")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Invoice Document Example")
                )
                .build();
        invoiceDocument.setQrInvoice(qrInvoice);

        return invoiceDocument;
    }
}
