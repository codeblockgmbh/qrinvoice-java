package ch.codeblock.qrinvoice.rest.resources;

import ch.codeblock.qrinvoice.util.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

@Component
public class CustomerResourcesRepository {

    private Path customerResources;

    public CustomerResourcesRepository(@Value("${qrinvoice.customer.resources.path}")
                                               String customerResourcesPath) {
        this.customerResources = Paths.get(customerResourcesPath);
    }


    public Path getCustomerDir(Integer customerId) throws IOException {
        String customerIdString = customerId != null ? customerId.toString() : "default";
        final Path customerDir = customerResources.resolve(customerIdString);
        Files.createDirectories(customerDir);
        return customerDir;
    }

    public Optional<Path> getFile(Integer customerId, String filename) throws IOException {
        if(StringUtils.isBlank(filename)) {
            return Optional.empty();
        }
        final Path customerDir = getCustomerDir(customerId);
        final Path resourceFile = customerDir.resolve(filename);

        if (Files.exists(resourceFile)) {
            return Optional.of(resourceFile);
        } else {
            return Optional.empty();
        }
    }

    public Path writeFile(Integer customerId, String filename, InputStream inputStream) throws IOException {
        final Path customerDir = getCustomerDir(customerId);
        final Path resourceFile = customerDir.resolve(filename);
        Files.copy(inputStream, resourceFile);
        return resourceFile;
    }
}
