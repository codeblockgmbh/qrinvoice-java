/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.*;
import ch.codeblock.qrinvoice.bulk.QrInvoicePaymentPartReceiptBulkCreator;
import ch.codeblock.qrinvoice.model.ParseException;
import ch.codeblock.qrinvoice.model.util.DemoValues;
import ch.codeblock.qrinvoice.model.validation.QrInvoiceValidator;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.output.BulkOutput;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.pdf.PdfMerger;
import ch.codeblock.qrinvoice.pdf.QrPdfMerger;
import ch.codeblock.qrinvoice.qrcode.DecodeException;
import ch.codeblock.qrinvoice.qrcode.QrCodeReaderOptions;
import ch.codeblock.qrinvoice.rest.api.RequestContext;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.helper.RequestParamHelper;
import ch.codeblock.qrinvoice.rest.api.v2.helper.RequestResponseLogger;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import ch.codeblock.qrinvoice.rest.api.v2.helper.SwissPaymentCodeHashHelper;
import ch.codeblock.qrinvoice.rest.filter.ApiRequestLoggingFilter;
import ch.codeblock.qrinvoice.rest.model.*;
import ch.codeblock.qrinvoice.util.StringUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.io.FilenameUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.BooleanUtils.toBooleanDefaultIfNull;

@ExposedApi
@RestController
@RequestMapping("/v2/payment-part-receipt")
@Tag(name = "11 Payment Part & Receipt (QR Bill)")
public class PaymentPartReceiptController {
    private final RequestContext requestContext;
    private final RequestResponseLogger requestResponseLogger;
    private final RequestParamHelper requestParamHelper;
    private final ResponseHelper responseHelper;
    private final SwissPaymentCodeHashHelper hashReportHelper;

    public PaymentPartReceiptController(RequestContext requestContext, final RequestResponseLogger requestResponseLogger, RequestParamHelper requestParamHelper, final ResponseHelper responseHelper, final SwissPaymentCodeHashHelper hashReportHelper) {
        this.requestContext = requestContext;
        this.requestResponseLogger = requestResponseLogger;
        this.requestParamHelper = requestParamHelper;
        this.responseHelper = responseHelper;
        this.hashReportHelper = hashReportHelper;
    }

    @Operation(summary = "Create Payment Part & Receipt")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The payment part & receipt in the requested output format.")
    })
    @PostMapping(
            value = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {"application/pdf", "image/png", "image/gif", "image/jpeg", "image/bmp", "image/tiff"}
    )
    @ResponseBody
    public ResponseEntity<?> createPaymentPartReceipt(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Locale") @RequestHeader(value = "Accept-Language", required = false, defaultValue = "de") final LanguageEnum language,
            @Parameter(description = "Font Family") @RequestParam(value = "fontFamily", required = false, defaultValue = "LIBERATION_SANS") final FontFamilyEnum fontFamily,
            @Parameter(description = "Embed Fonts in PDF") @RequestParam(value = "fontsEmbedded", required = false, defaultValue = "true") final Boolean fontsEmbedded,
            @Parameter(description = "Page Size") @RequestParam(value = "pageSize", required = false, defaultValue = "DIN_LANG") final PageSizeEnum pageSize,
            @Parameter(description = "Output Resolution") @RequestParam(value = "resolution", required = false, defaultValue = "MEDIUM_300_DPI") final OutputResolutionEnum resolution,
            @Parameter(description = "If a line should be printed to mark the payment parts and receipts boundary. This should be true in all cases except the invoice is printed to perforated paper.") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If the boundary lines should include a margin and not extend to the border of the page. Printers usually are unable to print the lines completely otherwise.") @RequestParam(value = "boundaryLinesMargins", required = false, defaultValue = "false") final Boolean boundaryLinesMargins,
            @Parameter(description = "If scissors should be printed on the boundary lines. Only to be set to true if parameter boundaryLines is set to true.") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part. This can be used as an alternative or in addition to boundaryLineScissors.") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "false") final Boolean boundaryLineSeparationText,
            @Parameter(description = "If an additional print margin (1mm) should be added to the left, right and bottom of the payment part & receipt. If set, 6mm instead of 5mm print margin is used. This may be needed some printers and print services") @RequestParam(value = "additionalPrintMargin", required = false, defaultValue = "false") final Boolean additionalPrintMargin,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "QrInvoice") @RequestBody final QrInvoice qrInvoice
    ) {
        try {
            return createPaymentPartReceiptInternal(accept, language, fontFamily, fontsEmbedded, pageSize, resolution, boundaryLines, boundaryLinesMargins, boundaryLineScissors, boundaryLineSeparationText, additionalPrintMargin, normalizeInput, qrInvoice);
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Create Payment Part & Receipt - Prefer POST endpoint if possible")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The payment part & receipt in the requested output format.")
    })
    @GetMapping(
            value = "",
            produces = {"application/pdf", "image/png", "image/gif", "image/jpeg", "image/bmp", "image/tiff"}
    )
    @ResponseBody
    public ResponseEntity<?> createPaymentPartReceipt(
            @Parameter(description = "Locale") @RequestParam(value = "language", required = false, defaultValue = "de") final LanguageEnum language,
            @Parameter(description = "Font Family") @RequestParam(value = "fontFamily", required = false, defaultValue = "LIBERATION_SANS") final FontFamilyEnum fontFamily,
            @Parameter(description = "Embed Fonts in PDF") @RequestParam(value = "fontsEmbedded", required = false, defaultValue = "true") final Boolean fontsEmbedded,
            @Parameter(description = "Page Size") @RequestParam(value = "pageSize", required = false, defaultValue = "DIN_LANG") final PageSizeEnum pageSize,
            @Parameter(description = "Output Format") @RequestParam(value = "format", required = false, defaultValue = "application/pdf") String outputFormat,
            @Parameter(description = "Output Resolution") @RequestParam(value = "resolution", required = false, defaultValue = "MEDIUM_300_DPI") final OutputResolutionEnum resolution,
            @Parameter(description = "If a line should be printed to mark the payment parts and receipts boundary. This should be true in all cases except the invoice is printed to perforated paper.") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If the boundary lines should include a margin and not extend to the border of the page. Printers usually are unable to print the lines completely otherwise.") @RequestParam(value = "boundaryLinesMargins", required = false, defaultValue = "false") final Boolean boundaryLinesMargins,
            @Parameter(description = "If scissors should be printed on the boundary lines. Only to be set to true if parameter boundaryLines is set to true.") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part. This can be used as an alternative or in addition to boundaryLineScissors.") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "false") final Boolean boundaryLineSeparationText,
            @Parameter(description = "If an additional print margin (1mm) should be added to the left, right and bottom of the payment part & receipt. If set, 6mm instead of 5mm print margin is used. This may be needed some printers and print services") @RequestParam(value = "additionalPrintMargin", required = false, defaultValue = "false") final Boolean additionalPrintMargin,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "QrInvoice json document, URL encoded using UTF8") @RequestParam(value = "qrInvoice") final String qrInvoiceStr
    ) {
        try {
            final QrInvoice qrInvoice = requestParamHelper.convertQrInvoiceQueryParamToObject(qrInvoiceStr);
            return createPaymentPartReceiptInternal(outputFormat, language, fontFamily, fontsEmbedded, pageSize, resolution, boundaryLines, boundaryLinesMargins, boundaryLineScissors, boundaryLineSeparationText, additionalPrintMargin, normalizeInput, qrInvoice);
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    private ResponseEntity<?> createPaymentPartReceiptInternal(String outputFormat, LanguageEnum language, FontFamilyEnum fontFamily, Boolean fontsEmbedded, PageSizeEnum pageSize, OutputResolutionEnum resolution, Boolean boundaryLines, Boolean boundaryLinesMargins, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText, Boolean additionalPrintMargin, Boolean normalizeInput, QrInvoice qrInvoice) {
        final Optional<OutputFormat> optionalOutputFormat = OutputFormat.getByMimeType(outputFormat);
        if (!optionalOutputFormat.isPresent()) {
            return responseHelper.requestedMimeTypeNotSupported(outputFormat);
        }

        final ch.codeblock.qrinvoice.model.QrInvoice invoice = InboundModelMapper.create(normalizeInput).map(qrInvoice);

        final PaymentPartReceipt paymentPartReceipt = getPaymentPartReceipt(language, fontFamily, fontsEmbedded, pageSize, resolution, boundaryLines, boundaryLinesMargins, boundaryLineScissors, boundaryLineSeparationText, additionalPrintMargin, invoice, optionalOutputFormat.get());
        hashReportHelper.report(invoice);

        return buildResponse(paymentPartReceipt);
    }

    @Operation(summary = "Create Payment Part & Receipt and merge it with a PDF")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The merged payment part as PDF.")
    })
    @PostMapping(
            value = "merge",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {"application/pdf"}
    )
    @ResponseBody
    public ResponseEntity<?> createAndMergePaymentPartReceipt(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Locale") @RequestHeader(value = "Accept-Language", required = false, defaultValue = "de") final LanguageEnum language,
            @Parameter(description = "Font Family") @RequestParam(value = "fontFamily", required = false, defaultValue = "LIBERATION_SANS") final FontFamilyEnum fontFamily,
            @Parameter(description = "Embed Fonts in PDF") @RequestParam(value = "fontsEmbedded", required = false, defaultValue = "true") final Boolean fontsEmbedded,
            @Parameter(description = "If a line should be printed to mark the payment parts and receipts boundary. This should be true in all cases except the invoice is printed to perforated paper.") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If the boundary lines should include a margin and not extend to the border of the page. Printers usually are unable to print the lines completely otherwise.") @RequestParam(value = "boundaryLinesMargins", required = false, defaultValue = "false") final Boolean boundaryLinesMargins,
            @Parameter(description = "If scissors should be printed on the boundary lines. Only to be set to true if parameter boundaryLines is set to true.") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part. This can be used as an alternative or in addition to boundaryLineScissors.") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "false") final Boolean boundaryLineSeparationText,
            @Parameter(description = "If an additional print margin (1mm) should be added to the left, right and bottom of the payment part & receipt. If set, 6mm instead of 5mm print margin is used. This may be needed some printers and print services") @RequestParam(value = "additionalPrintMargin", required = false, defaultValue = "false") final Boolean additionalPrintMargin,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "QrInvoice in JSON format") @RequestPart("QrInvoice") final QrInvoice qrInvoice,
            @Parameter(description = "PDF file to be merged") @RequestPart("pdf") final MultipartFile pdf,
            @Parameter(description = "Page number on which to place the payment part receipt") @RequestParam(value = "onPage", required = false, defaultValue = "1") final int onPage
    ) {
        try {
            final ch.codeblock.qrinvoice.model.QrInvoice invoice = InboundModelMapper.create(normalizeInput).map(qrInvoice);

            final PaymentPartReceipt paymentPartReceipt = getPaymentPartReceipt(language, fontFamily, fontsEmbedded, PageSizeEnum.A4, null, boundaryLines, boundaryLinesMargins, boundaryLineScissors, boundaryLineSeparationText, additionalPrintMargin, invoice, OutputFormat.PDF);

            final byte[] mergedParts = QrPdfMerger.create().mergePdfs(pdf.getBytes(), paymentPartReceipt.getData(), onPage);

            final PaymentPartReceipt mergedReceipt = new PaymentPartReceipt(
                    paymentPartReceipt.getPageSize(),
                    paymentPartReceipt.getOutputFormat(),
                    mergedParts,
                    paymentPartReceipt.getWidth(),
                    paymentPartReceipt.getHeight());

            hashReportHelper.report(invoice);

            return buildResponse(mergedReceipt);
        } catch (BaseException | IOException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Create Payment Part & Receipt and append it to a PDF on a new page")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The PDF with the payment part & receipt appended.")
    })
    @PostMapping(
            value = "append",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {"application/pdf"}
    )
    @ResponseBody
    public ResponseEntity<?> createAndAppendPaymentPartReceipt(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Locale") @RequestHeader(value = "Accept-Language", required = false, defaultValue = "de") final LanguageEnum language,
            @Parameter(description = "Font Family") @RequestParam(value = "fontFamily", required = false, defaultValue = "LIBERATION_SANS") final FontFamilyEnum fontFamily,
            @Parameter(description = "Embed Fonts in PDF") @RequestParam(value = "fontsEmbedded", required = false, defaultValue = "true") final Boolean fontsEmbedded,
            @Parameter(description = "If a line should be printed to mark the payment parts and receipts boundary. This should be true in all cases except the invoice is printed to perforated paper.") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If the boundary lines should include a margin and not extend to the border of the page. Printers usually are unable to print the lines completely otherwise.") @RequestParam(value = "boundaryLinesMargins", required = false, defaultValue = "false") final Boolean boundaryLinesMargins,
            @Parameter(description = "If scissors should be printed on the boundary lines. Only to be set to true if parameter boundaryLines is set to true.") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part. This can be used as an alternative or in addition to boundaryLineScissors.") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "false") final Boolean boundaryLineSeparationText,
            @Parameter(description = "If an additional print margin (1mm) should be added to the left, right and bottom of the payment part & receipt. If set, 6mm instead of 5mm print margin is used. This may be needed some printers and print services") @RequestParam(value = "additionalPrintMargin", required = false, defaultValue = "false") final Boolean additionalPrintMargin,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "QrInvoice in JSON format") @RequestPart(value = "QrInvoice") final QrInvoice qrInvoice,
            @Parameter(description = "PDF file to be appended to") @RequestPart(value = "pdf") final MultipartFile pdf
    ) {
        try {
            final ch.codeblock.qrinvoice.model.QrInvoice invoice = InboundModelMapper.create(normalizeInput).map(qrInvoice);

            final PaymentPartReceipt paymentPartReceipt = getPaymentPartReceipt(language, fontFamily, fontsEmbedded, PageSizeEnum.A4, null, boundaryLines, boundaryLinesMargins, boundaryLineScissors, boundaryLineSeparationText, additionalPrintMargin, invoice, OutputFormat.PDF);

            final byte[] mergedParts = QrPdfMerger.create().appendPdfs(Arrays.asList(pdf.getBytes(), paymentPartReceipt.getData()));

            final PaymentPartReceipt mergedReceipt = new PaymentPartReceipt(
                    paymentPartReceipt.getPageSize(),
                    paymentPartReceipt.getOutputFormat(),
                    mergedParts,
                    paymentPartReceipt.getWidth(),
                    paymentPartReceipt.getHeight());

            hashReportHelper.report(invoice);

            return buildResponse(mergedReceipt);
        } catch (BaseException | IOException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }


    @Operation(summary = "Clones / recreates a given Payment Part & Receipt based on an existing Swiss QR Code of a given document")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The payment part & receipt in the requested output format.")
    })
    @PostMapping(
            value = "clone",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {"application/pdf", "image/png", "image/gif", "image/jpeg", "image/bmp", "image/tiff"}
    )
    @ResponseBody
    public ResponseEntity<?> createAndAppendPaymentPartReceipt(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Locale") @RequestHeader(value = "Accept-Language", required = false, defaultValue = "de") final LanguageEnum language,
            @Parameter(description = "Font Family") @RequestParam(value = "fontFamily", required = false, defaultValue = "LIBERATION_SANS") final FontFamilyEnum fontFamily,
            @Parameter(description = "Embed Fonts in PDF") @RequestParam(value = "fontsEmbedded", required = false, defaultValue = "true") final Boolean fontsEmbedded,
            @Parameter(description = "If a line should be printed to mark the payment parts and receipts boundary. This should be true in all cases except the invoice is printed to perforated paper.") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If the boundary lines should include a margin and not extend to the border of the page. Printers usually are unable to print the lines completely otherwise.") @RequestParam(value = "boundaryLinesMargins", required = false, defaultValue = "false") final Boolean boundaryLinesMargins,
            @Parameter(description = "If scissors should be printed on the boundary lines. Only to be set to true if parameter boundaryLines is set to true.") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part. This can be used as an alternative or in addition to boundaryLineScissors.") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "false") final Boolean boundaryLineSeparationText,
            @Parameter(description = "If an additional print margin (1mm) should be added to the left, right and bottom of the payment part & receipt. If set, 6mm instead of 5mm print margin is used. This may be needed some printers and print services") @RequestParam(value = "additionalPrintMargin", required = false, defaultValue = "false") final Boolean additionalPrintMargin,
            @Parameter(description = "If true, last page is expected to contain only the Swiss QR Code and should be replaced with the newly generated payment part & receipt. Only works for PDF") @RequestParam(value = "replaceLastPage", required = false, defaultValue = "false") final Boolean replaceLastPage,
            @Parameter(description = "Document to clone the payment part & receipt from") @RequestPart(value = "file") final MultipartFile file
    ) {

        final Optional<OutputFormat> optionalOutputFormat = OutputFormat.getByMimeType(accept);
        if (!optionalOutputFormat.isPresent()) {
            return responseHelper.requestedMimeTypeNotSupported(accept);
        }

        final Optional<QrInvoiceDocumentScanner> documentScannerOptional = determineMimeType(file).flatMap(this::getDocumentScanner);
        if (!documentScannerOptional.isPresent()) {
            return responseHelper.inputMimeTypeNotSupported(file);
        }
        final QrInvoiceDocumentScanner documentScanner = documentScannerOptional.get();

        try (final InputStream documentInputStream = file.getInputStream()) {
            final Optional<ch.codeblock.qrinvoice.model.QrInvoice> qrInvoiceOptional = documentScanner.scanDocumentUntilFirstSwissQrCode(documentInputStream);
            if (qrInvoiceOptional.isPresent()) {
                final ch.codeblock.qrinvoice.model.QrInvoice qrInvoice = qrInvoiceOptional.get();
                QrInvoiceValidator.create(ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion.latestActive()).validate(qrInvoice).throwExceptionOnErrors();

                final PaymentPartReceipt paymentPartReceipt = getPaymentPartReceipt(language, fontFamily, fontsEmbedded, PageSizeEnum.A4, null, boundaryLines, boundaryLinesMargins, boundaryLineScissors, boundaryLineSeparationText, additionalPrintMargin, qrInvoice, optionalOutputFormat.get());

                final String baseFilename = getOriginalBaseFilename(file);
                final String filename = StringUtils.isNotBlank(baseFilename) ? baseFilename : null;
                final String newFileName;
                if (filename == null) {
                    newFileName = "clone." + optionalOutputFormat.get().getFileExtension();
                } else {
                    final int lastDotIndex = filename.lastIndexOf(".");
                    if (lastDotIndex > -1) {
                        newFileName = filename.substring(0, lastDotIndex) + "_clone." + filename.substring(lastDotIndex + 1);
                    } else {
                        newFileName = filename + "_clone." + optionalOutputFormat.get().getFileExtension();
                    }
                }

                if (replaceLastPage && determineMimeType(file).orElse(MimeType.PNG) == MimeType.PDF) {
                    final byte[] mergedResult = PdfMerger.create()
                            .addPdf(file.getBytes(), true)
                            .addPdf(paymentPartReceipt.getData())
                            .getPdf();
                    return responseHelper.buildResponse(mergedResult, newFileName);
                } else {
                    return responseHelper.buildResponse(paymentPartReceipt, newFileName);
                }
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (DecodeException | ValidationException | ParseException | IOException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Create Payment Part & Receipts for a list of QrInvoices")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The payment part & receipts in the requested output format. Result is either a multi page PDF file or a ZIP file containing files for each list entry.")
    })
    @PostMapping(
            value = "/bulk/list",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {"application/pdf", "application/zip"}
    )
    @ResponseBody
    public ResponseEntity<?> createPaymentPartReceipts(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "Locale") @RequestHeader(value = "Accept-Language", required = false, defaultValue = "de") final LanguageEnum language,
            @Parameter(description = "Font Family") @RequestParam(value = "fontFamily", required = false, defaultValue = "LIBERATION_SANS") final FontFamilyEnum fontFamily,
            @Parameter(description = "Embed Fonts in PDF") @RequestParam(value = "fontsEmbedded", required = false, defaultValue = "true") final Boolean fontsEmbedded,
            @Parameter(description = "Page Size") @RequestParam(value = "pageSize", required = false, defaultValue = "DIN_LANG") final PageSizeEnum pageSize,
            @Parameter(description = "Output Resolution") @RequestParam(value = "resolution", required = false, defaultValue = "MEDIUM_300_DPI") final OutputResolutionEnum resolution,
            @Parameter(description = "If a line should be printed to mark the payment parts and receipts boundary. This should be true in all cases except the invoice is printed to perforated paper.") @RequestParam(value = "boundaryLines", required = false, defaultValue = "true") final Boolean boundaryLines,
            @Parameter(description = "If the boundary lines should include a margin and not extend to the border of the page. Printers usually are unable to print the lines completely otherwise.") @RequestParam(value = "boundaryLinesMargins", required = false, defaultValue = "false") final Boolean boundaryLinesMargins,
            @Parameter(description = "If scissors should be printed on the boundary lines. Only to be set to true if parameter boundaryLines is set to true.") @RequestParam(value = "boundaryLineScissors", required = false, defaultValue = "true") final Boolean boundaryLineScissors,
            @Parameter(description = "If a separation label should be printed above the payment part. This can be used as an alternative or in addition to boundaryLineScissors.") @RequestParam(value = "boundaryLineSeparationText", required = false, defaultValue = "false") final Boolean boundaryLineSeparationText,
            @Parameter(description = "If an additional print margin (1mm) should be added to the left, right and bottom of the payment part & receipt. If set, 6mm instead of 5mm print margin is used. This may be needed some printers and print services") @RequestParam(value = "additionalPrintMargin", required = false, defaultValue = "false") final Boolean additionalPrintMargin,
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "QrInvoices") @RequestBody final List<QrInvoice> qrInvoices
    ) {
        final Optional<OutputFormat> optionalOutputFormat = OutputFormat.getByMimeType(accept);
        if (!optionalOutputFormat.isPresent()) {
            return responseHelper.requestedMimeTypeNotSupported(accept);
        }

        final InboundModelMapper inboundModelMapper = InboundModelMapper.create(normalizeInput);

        int nr = 1;
        final List<ch.codeblock.qrinvoice.model.QrInvoice> mappedQrInvoices = new ArrayList<>();
        for (final QrInvoice qrInvoice : qrInvoices) {
            try {
                mappedQrInvoices.add(inboundModelMapper.map(qrInvoice));
            } catch (ValidationException e) {
                e.getValidationResult().addError("List", "Item", null, String.format("Validation errors occurred on element nr. %s", nr));
                return responseHelper.buildExceptionResponse(new ValidationException(e.getValidationResult()));
            }
            nr++;
        }

        try {
            final BulkOutput bulkOutput = QrInvoicePaymentPartReceiptBulkCreator.create().qrInvoices(mappedQrInvoices, qrInvoicePaymentPartReceiptCreator ->
                            qrInvoicePaymentPartReceiptCreator
                                    .locale(requestParamHelper.toLocale(language))
                                    .fontFamily(requestParamHelper.toFontFamily(fontFamily))
                                    .pageSize(requestParamHelper.toPageSize(pageSize))
                                    .fontsEmbedded(toBooleanDefaultIfNull(fontsEmbedded, true))
                                    .outputFormat(optionalOutputFormat.get())
                                    .outputResolution(requestParamHelper.toOutputResolution(resolution))
                                    .boundaryLines(requestParamHelper.toBoundaryLines(boundaryLines, boundaryLinesMargins))
                                    .boundaryLineScissors(toBooleanDefaultIfNull(boundaryLineScissors, true))
                                    .boundaryLineSeparationText(toBooleanDefaultIfNull(boundaryLineSeparationText, false))
                                    .additionalPrintMargin(toBooleanDefaultIfNull(additionalPrintMargin, false)))
                    .limit(1000)
                    .createAsMimeType(MimeType.getByMimeType(accept).get());

            mappedQrInvoices.forEach(hashReportHelper::report);

            return buildBulkResponse(bulkOutput);
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Create Payment Part & Receipts as a bulk operation based on a given input file (CSV or XLSX). Result is either a multi page PDF file or a ZIP file containing files for each list entry.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The payment part & receipts in the requested output format.")
    })
    @PostMapping(
            value = "/bulk/file",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {"application/pdf", "application/zip"}
    )
    @ResponseBody
    public ResponseEntity<?> bulk(
            @Parameter(hidden = true) @RequestHeader("Accept") String accept,
            @Parameter(description = "CSV separator - only relevant for CSV files") @RequestPart(value = "csvSeparator", required = false) final String csvSeparator,
            @Parameter(description = "File containing the list of data to generate from (CSV or XLSX)") @RequestPart(value = "file") final MultipartFile file
    ) {
        final Optional<MimeType> optionalMimeType = MimeType.getByMimeType(accept);
        if (!optionalMimeType.isPresent() || !QrInvoicePaymentPartReceiptBulkCreator.create().isSupported(optionalMimeType.get())) {
            return responseHelper.requestedMimeTypeNotSupported(accept);
        }

        try (final InputStream fileInputStream = file.getInputStream()) {
            final Optional<MimeType> inputMimeType = determineMimeType(file);
            if (inputMimeType.isPresent()) {
                final BulkOutput bulkOutput = QrInvoicePaymentPartReceiptBulkCreator.create()
                        .inputMimeType(inputMimeType.get())
                        .csvSeparator(csvSeparator) // may be null
                        .inputStream(fileInputStream)
                        .qrInvoiceConsumer(hashReportHelper::report)
                        .limit(1000)
                        .createAsMimeType(optionalMimeType.get());

                return buildBulkResponse(bulkOutput);
            } else {
                return responseHelper.inputMimeTypeNotSupported(file);
            }
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    private Optional<MimeType> determineMimeType(MultipartFile file) {
        final Optional<MimeType> byMimeType = MimeType.getByMimeType(file.getContentType());
        if (byMimeType.isPresent()) {
            return byMimeType;
        }

        return MimeType.getByFilename(getOriginalBaseFilename(file));
    }

    private String getOriginalBaseFilename(MultipartFile file) {
        return FilenameUtils.getBaseName(file.getOriginalFilename());
    }

    private Optional<QrInvoiceDocumentScanner> getDocumentScanner(final MimeType mimeType) {
        try {
            // TODO make use of param
            return Optional.of(QrInvoiceDocumentScanner.create(mimeType, QrCodeReaderOptions.DEFAULT));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private PaymentPartReceipt getPaymentPartReceipt(LanguageEnum languageEnum, FontFamilyEnum fontFamilyEnum, final Boolean fontsEmbedded, PageSizeEnum pageSizeEnum, OutputResolutionEnum resolution, Boolean boundaryLines, Boolean boundaryLinesMargins, Boolean boundaryLineScissors, Boolean boundaryLineSeparationText, Boolean additionalPrintMargin, final ch.codeblock.qrinvoice.model.QrInvoice qrInvoice, OutputFormat outputFormat) {
        requestResponseLogger.logRequestData(qrInvoice);
        if (requestContext.isDemo()) {
            DemoValues.apply(qrInvoice);
        }

        return QrInvoicePaymentPartReceiptCreator
                .create()
                .qrInvoice(qrInvoice)
                .locale(requestParamHelper.toLocale(languageEnum))
                .fontFamily(requestParamHelper.toFontFamily(fontFamilyEnum))
                .pageSize(requestParamHelper.toPageSize(pageSizeEnum))
                .fontsEmbedded(toBooleanDefaultIfNull(fontsEmbedded, true))
                .outputFormat(outputFormat)
                .outputResolution(requestParamHelper.toOutputResolution(resolution))
                .boundaryLines(requestParamHelper.toBoundaryLines(boundaryLines, boundaryLinesMargins))
                .boundaryLineScissors(toBooleanDefaultIfNull(boundaryLineScissors, true))
                .boundaryLineSeparationText(toBooleanDefaultIfNull(boundaryLineSeparationText, false))
                .additionalPrintMargin(toBooleanDefaultIfNull(additionalPrintMargin, false))
                .createPaymentPartReceipt();
    }

    public ResponseEntity<?> buildBulkResponse(final BulkOutput bulkOutput) {
        ApiRequestLoggingFilter.setItemCount(bulkOutput.getCount());
        return responseHelper.buildResponse(bulkOutput.getData(), String.format("%s.%s", PaymentPartReceipt.class.getSimpleName(), bulkOutput.getOutputFormat().getFileExtension()));
    }

    public ResponseEntity<?> buildResponse(final PaymentPartReceipt output) {
        return responseHelper.buildResponse(output, String.format("%s_%s.%s", output.getClass().getSimpleName(), output.getPageSize(), output.getOutputFormat().getFileExtension()));
    }

}
