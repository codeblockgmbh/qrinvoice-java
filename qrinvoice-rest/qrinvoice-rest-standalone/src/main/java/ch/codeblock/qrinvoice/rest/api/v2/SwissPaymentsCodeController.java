/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.BaseException;
import ch.codeblock.qrinvoice.QrInvoiceCodeCreator;
import ch.codeblock.qrinvoice.QrInvoiceCodeParser;
import ch.codeblock.qrinvoice.model.util.DemoValues;
import ch.codeblock.qrinvoice.model.validation.QrInvoiceValidator;
import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import ch.codeblock.qrinvoice.rest.api.RequestContext;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.helper.RequestResponseLogger;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import ch.codeblock.qrinvoice.rest.api.v2.helper.SwissPaymentCodeHashHelper;
import ch.codeblock.qrinvoice.rest.model.InboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.OutboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.QrInvoice;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@ExposedApi
@RestController
@RequestMapping("/v2/swiss-payments-code")
@Tag(name = "13 Swiss Payments Code")
public class SwissPaymentsCodeController {
    private final RequestContext requestContext;
    private final ResponseHelper responseHelper;
    private final RequestResponseLogger requestResponseLogger;
    private final SwissPaymentCodeHashHelper hashReportHelper;

    @Autowired
    public SwissPaymentsCodeController(RequestContext requestContext, final RequestResponseLogger requestResponseLogger, final ResponseHelper responseHelper, final SwissPaymentCodeHashHelper hashReportHelper) {
        this.requestContext = requestContext;
        this.requestResponseLogger = requestResponseLogger;
        this.responseHelper = responseHelper;
        this.hashReportHelper = hashReportHelper;
    }

    @Operation(summary = "Create Swiss Payments Code")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The raw Swiss Payments Code (SPC)")
    })
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> create(
            @Parameter(description = "If true, input gets normalized. This means strings are trimmed, special quotations replaced with ASCII ones and special characters replaced with semantically equal ones (e.g. œ -> oe)") @RequestParam(value = "normalizeInput", required = false, defaultValue = "false") final Boolean normalizeInput,
            @Parameter(description = "QrInvoice", name = "QrInvoice")
            @RequestBody final QrInvoice qrInvoice) {

        try {
            final ch.codeblock.qrinvoice.model.QrInvoice qrInvoiceMapped = InboundModelMapper.create(normalizeInput).map(qrInvoice);
            requestResponseLogger.logRequestData(qrInvoiceMapped);
            if (requestContext.isDemo()) {
                DemoValues.apply(qrInvoiceMapped);
            }
            final String spc = QrInvoiceCodeCreator
                    .create()
                    .qrInvoice(qrInvoiceMapped)
                    .createSwissPaymentsCode();

            hashReportHelper.report(spc);

            return responseHelper.utf8(ResponseEntity.ok()).body(spc);
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Parse Swiss Payments Code")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The QrInvoice Object", content = @Content(schema = @Schema(implementation = QrInvoice.class)))
    })
    @PostMapping(
            value = "parse",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> parse(
            @Parameter(description = "Swiss Payments Code", name = "SwissPaymentsCode")
            @RequestBody final String swissPaymentsCode) {
        try {
            final ch.codeblock.qrinvoice.model.QrInvoice qrInvoice = QrInvoiceCodeParser.create().parse(swissPaymentsCode);
            QrInvoiceValidator.create(ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion.latestActive()).validate(qrInvoice).throwExceptionOnErrors();
            requestResponseLogger.logResponseData(qrInvoice);
            return ResponseEntity.ok(OutboundModelMapper.create().map(qrInvoice));
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Validates the given Swiss Payments Code")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The validation result", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "validate",
            consumes = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> validate(
            @Parameter(description = "Swiss Payments Code", name = "SwissPaymentsCode")
            @RequestBody final String swissPaymentsCode) {
        try {
            final ch.codeblock.qrinvoice.model.QrInvoice qrInvoice = QrInvoiceCodeParser.create().parse(swissPaymentsCode);
            final ValidationResult validationResult = QrInvoiceValidator.create(ch.codeblock.qrinvoice.model.ImplementationGuidelinesQrBillVersion.latestActive()).validate(qrInvoice);
            if (validationResult.isValid()) {
                return ResponseEntity.ok().build();
            } else {
                final String validationErrorSummary = validationResult.getValidationErrorSummary();
                return responseHelper.okUtf8().body(validationErrorSummary);
            }
        } catch (BaseException e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }
}
