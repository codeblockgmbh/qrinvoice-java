/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.examples.ExampleInvoiceDocuments;
import ch.codeblock.qrinvoice.rest.model.InvoiceDocumentOutboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.documents.InvoiceDocument;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@ExposedApi
@RestController
@RequestMapping("/v2/examples/invoice-document")
@Tag(name = "00 Example Data")
public class ExampleInvoiceDocumentController {

    @Operation(summary = "Invoice Document - Example with QR Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Invoice Document")
    })
    @GetMapping(value = "mixed-vat", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<InvoiceDocument> mixedVat() {
        return ResponseEntity.ok(InvoiceDocumentOutboundModelMapper.create().map(ExampleInvoiceDocuments.INVOICE_DOCUMENT_MIXED_VAT));
    }

    @Operation(summary = "Invoice Document - Example with QR Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Invoice Document")
    })
    @GetMapping(value = "discounts", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<InvoiceDocument> discounts() {
        return ResponseEntity.ok(InvoiceDocumentOutboundModelMapper.create().map(ExampleInvoiceDocuments.INVOICE_DOCUMENT_DISCOUNTS));
    }

    @Operation(summary = "Invoice Document - Example with QR Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Invoice Document")
    })
    @GetMapping(value = "gtin", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity<InvoiceDocument> gtin() {
        return ResponseEntity.ok(InvoiceDocumentOutboundModelMapper.create().map(ExampleInvoiceDocuments.INVOICE_DOCUMENT_GTIN));
    }

}
