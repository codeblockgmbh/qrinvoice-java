/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.model.billinformation.BillInformation;
import ch.codeblock.qrinvoice.model.parser.BillInformationParser;
import ch.codeblock.qrinvoice.model.validation.ValidationResult;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.S1InboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.S1OutboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.SwicoS1v12;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@ExposedApi
@RestController
@RequestMapping("/v2/bill-information/swicos1v12")
@Tag(name = "30 Bill Information")
public class SwicoS1v12Controller {

    private final S1InboundModelMapper s1InboundModelMapper;
    private final S1OutboundModelMapper s1OutboundModelMapper;

    public SwicoS1v12Controller(final S1InboundModelMapper s1InboundModelMapper, final S1OutboundModelMapper s1OutboundModelMapper) {
        this.s1InboundModelMapper = s1InboundModelMapper;
        this.s1OutboundModelMapper = s1OutboundModelMapper;
    }

    @Operation(summary = "Parse Bill Information as SwicoS1v12")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The parsed Bill Information", content = @Content(schema = @Schema(implementation = SwicoS1v12.class))),
            @ApiResponse(responseCode = "422", description = "Unsupported bill information - not Swico S1 v1.2")
    })
    @PostMapping(value = "parse",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody

    public ResponseEntity<?> parseSwicoS1v12(@Parameter(description = "Bill Information", name = "billInformation") @RequestBody final String billInformationStr) {
        final BillInformation billInformation = BillInformationParser.create().parseBillInformation(billInformationStr);
        if (billInformation instanceof ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12) {
            final ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12 s1 = (ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12) billInformation;
            return ResponseEntity.ok(s1OutboundModelMapper.map(s1));
        } else {
            return ResponseEntity.unprocessableEntity().body("Unsupported bill information type");
        }
    }


    @Operation(summary = "Serialize a SwicoS1v12 into Bill Information string")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The Bill Information string", content = @Content(schema = @Schema(implementation = String.class)))
    })
    @PostMapping(value = "create",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> createBillInformation(@Parameter(description = "Swico S1 v1.2") @RequestBody final SwicoS1v12 s1) {
        final ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12 s1Mapped = s1InboundModelMapper.map(s1);
        s1Mapped.validate().throwExceptionOnErrors();
        return ResponseEntity.ok(s1Mapped.toBillInformationString());
    }


    @Operation(summary = "Validate a SwicoS1v12 object")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid Swico S1 v1.2 data", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "422", description = "Invalid Swico S1 v1.2 data", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "validate",
            consumes = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> validate(@Parameter(description = "Swico S1 v1.2") @RequestBody final SwicoS1v12 s1) {
        final ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12 s1Mapped = s1InboundModelMapper.map(s1);
        final ValidationResult result = s1Mapped.validate();
        if (result.isValid()) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.unprocessableEntity().body("Invalid Bill information: " + s1);
        }
    }
}
