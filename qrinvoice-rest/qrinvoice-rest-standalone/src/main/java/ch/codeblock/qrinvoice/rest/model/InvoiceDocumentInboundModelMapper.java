/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.model;

import ch.codeblock.qrinvoice.documents.model.application.AdditionalProperty;
import ch.codeblock.qrinvoice.documents.model.application.Amount;
import ch.codeblock.qrinvoice.documents.model.application.Percentage;
import ch.codeblock.qrinvoice.documents.model.application.Position;
import ch.codeblock.qrinvoice.documents.model.application.builder.AmountBuilder;
import ch.codeblock.qrinvoice.documents.model.application.builder.InvoiceDocumentBuilder;
import ch.codeblock.qrinvoice.documents.model.application.builder.PositionBuilder;
import ch.codeblock.qrinvoice.rest.model.documents.Address;
import ch.codeblock.qrinvoice.rest.model.documents.InvoiceDocument;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InvoiceDocumentInboundModelMapper {

    private final boolean normalizeString;

    public InvoiceDocumentInboundModelMapper(boolean normalizeString) {
        this.normalizeString = normalizeString;
    }

    public static InvoiceDocumentInboundModelMapper create(boolean normalizeString) {
        return new InvoiceDocumentInboundModelMapper(normalizeString);
    }

    public ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument map(InvoiceDocument invoiceDocument) {
        return InvoiceDocumentBuilder.create()
                .customerNr(invoiceDocument.getCustomerNr())
                .customerReference(invoiceDocument.getCustomerReference())
                .invoiceDate(invoiceDocument.getInvoiceDate())
                .invoiceDueDate(invoiceDocument.getInvoiceDueDate())
                .termOfPaymentDays(invoiceDocument.getTermOfPaymentDays())
                .invoiceNr(invoiceDocument.getInvoiceNr())
                .title(invoiceDocument.getTitle())
                .prefaceText(invoiceDocument.getPrefaceText())
                .endingText(invoiceDocument.getEndingText())
                .positions(map(invoiceDocument.getPositions()))
                .currency(Currency.getInstance(invoiceDocument.getCurrency().name()))
                .sender(s -> s.addressLines(map(invoiceDocument.getSender())).build())
                .recipient(r -> r.addressLines(map(invoiceDocument.getRecipient())).build())
                .additionalProperties(mapAdditionalProperties(invoiceDocument.getAdditionalProperties()))
                .contactPerson(cp -> {
                    if (invoiceDocument.getContactPerson() != null) {
                        cp.name(invoiceDocument.getContactPerson().getName());
                        cp.email(invoiceDocument.getContactPerson().getEmail());
                        cp.phoneNr(invoiceDocument.getContactPerson().getPhoneNr());
                    }
                    cp.build();
                })
                .qrInvoice(InboundModelMapper.create(normalizeString).map(invoiceDocument.getQrInvoice()))
                .build();
    }

    private String[] map(Address address) {
        if (address == null || address.getAddressLines() == null) {
            return null;
        }
        return address.getAddressLines();
    }

    private List<Position> map(ch.codeblock.qrinvoice.rest.model.documents.Position[] positions) {
        if (positions == null) {
            return Collections.emptyList();
        }
        return Arrays.stream(positions)
                .map(position -> PositionBuilder.create()
                        .position(position.getPosition())
                        .description(position.getDescription())
                        .quantity(position.getQuantity())
                        .unitPrice(mapUnitPrice(position))
                        .unit(position.getUnit())
                        .discountAbsolute(mapDiscountAbsolute(position))
                        .discountPercentage(Percentage.ofNumericalPercentage(position.getDiscountPercentage()))
                        .additionalProperties(mapAdditionalProperties(position.getAdditionalProperties()))
                        .build())
                .collect(Collectors.toList());
    }

    private Amount mapUnitPrice(ch.codeblock.qrinvoice.rest.model.documents.Position position) {
        if (position.getUnitPrice() == null) {
            return null;
        }
        return AmountBuilder.create()
                .vatExclusive(position.getUnitPrice().getVatExclusive())
                .vatInclusive(position.getUnitPrice().getVatInclusive())
                .vatPercentage(mapVatPercentage(position.getUnitPrice().getVatPercentage()))
                .build();
    }

    private Amount mapDiscountAbsolute(ch.codeblock.qrinvoice.rest.model.documents.Position position) {
        if (position.getDiscountAbsolute() == null) {
            return null;
        }
        return AmountBuilder.create()
                .vatExclusive(position.getDiscountAbsolute().getVatExclusive())
                .vatInclusive(position.getDiscountAbsolute().getVatInclusive())
                .vatPercentage(mapVatPercentage(position.getDiscountAbsolute().getVatPercentage()))
                .build();
    }

    private Percentage mapVatPercentage(BigDecimal vatPercentage) {
        return vatPercentage != null ? Percentage.ofNumericalPercentage(vatPercentage) : null;
    }

    private List<AdditionalProperty> mapAdditionalProperties(ch.codeblock.qrinvoice.rest.model.documents.AdditionalProperty[] additionalProperties) {
        if(additionalProperties == null){
            return Collections.emptyList();
        }
        return Stream.of(additionalProperties)
                .map(additionalProperty -> new AdditionalProperty(additionalProperty.getKey(), additionalProperty.getValue()))
                .collect(Collectors.toList());
    }
}
