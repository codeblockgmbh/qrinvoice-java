/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.configuration;

import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.tags.Tag;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
@OpenAPIDefinition
public class SwaggerDocumentationConfig {
    @Value("${qrinvoice.artifact.version}")
    private String artifactVersion;

    @Bean
    public OpenAPI apiInfo() {
        return new OpenAPI()
                .info(new Info()
                        .title("QR Invoice REST API")
                        .description(
                                "The QR Invoice REST API provides various services for creation and processing of Swiss QR Invoices. " +
                                        "This API is available as a Cloud Service but is also available as a self-hosted solution." +
                                        "<br />" +
                                        "<h3>Product Website</h3>" +
                                        "<a href=\"https://www.qr-invoice.ch\">www.qr-invoice.ch</a>" +
                                        "<h3>API Key</h3>" +
                                        "This API requires an API Key for authorization on the Cloud. " +
                                        "You may use the following Demo API Key or request a trial API on our website: <a href=\"https://www.qr-invoice.ch/bestellung-trial/\">QrInvoice - Order Trial Key</a>" +
                                        "<pre>582c9ea9-741a-4bb6-acae-cf92f8805864</pre>" +
                                        "<br /><strong>Important:</strong> Use of Demo API Key comes with a few restrictions!" +
                                        "<br />" +
                                        "<h3>Hints</h3>" +
                                        "<ul>" +
                                        "<li>Please note, that you can retrieve JSON example documents in the «00 Example Data» section using the respective Endpoints. E.g.: https://rest.qr-invoice.cloud/v2/examples/qr-invoice/with-qr-reference</li>" +
                                        "<li>Handle error responses correctly by checking HTTP status and read detailed error/validation message from response body</li>" +
                                        "<li>Check all parameters</li>" +
                                        "<li>Check out length limitation</li>" +
                                        "<li>Consult official standards / specification if you need more detailed information regarding QR-Bill specification</li>" +
                                        "</ul>" +
                                        "<br />" +
                                        "<h3>Standards / Specifications</h3>" +
                                        "Overview of official specs <a href=\"https://www.qr-invoice.ch/dokumentation/standards-merkblaetter/\">www.qr-invoice.ch/dokumentation/standards-merkblaetter/</a>" +
                                        "<br />" +
                                        "<h3>QR Invoice Layers</h3>" +
                                        "<img src=\"https://rest.qr-invoice.cloud/qrinvoice-layers.png\" width=\"550\" />" +
                                        "<br />" +
                                        "<h3>QR Invoice Model</h3>" +
                                        "<img src=\"https://rest.qr-invoice.cloud/qrinvoice-model.png\" width=\"700\" />" +
                                        "<br />" +
                                        "<br />Product Version: " + artifactVersion
                        )
                        .license(new License().name("Commercial license").url("https://www.qr-invoice.ch/licenses/"))
                        .version("2.0")
                        .contact(new Contact()
                                .name("Codeblock GmbH")
                                .url("https://www.qr-invoice.ch")
                                .email("contact@codeblock.ch")
                        ))
                .components(new Components()
                        .addSecuritySchemes(ApiKey.HTTP_REQUEST_HEADER_NAME, new SecurityScheme()
                                .type(SecurityScheme.Type.APIKEY)
                                .description("API Key via Header")
                                .name(ApiKey.HTTP_REQUEST_HEADER_NAME)
                                .in(SecurityScheme.In.HEADER)
                        )
                        .addSecuritySchemes(ApiKey.HTTP_REQUEST_PARAM_NAME, new SecurityScheme()
                                .type(SecurityScheme.Type.APIKEY)
                                .description("API Key via Parameter")
                                .name(ApiKey.HTTP_REQUEST_PARAM_NAME)
                                .in(SecurityScheme.In.QUERY)
                        ))
                .addSecurityItem(new SecurityRequirement().addList(ApiKey.HTTP_REQUEST_HEADER_NAME))
                .addSecurityItem(new SecurityRequirement().addList(ApiKey.HTTP_REQUEST_PARAM_NAME))
                .tags(Arrays.asList(
                        new Tag().name("00 Example Data").description("Various example data that can be used as example input to other services"),
                        new Tag().name("10 QR Invoice Documents including Payment Part & Receipt (QR Bill)"),
                        new Tag().name("11 Payment Part & Receipt (QR Bill)"),
                        new Tag().name("12 Swiss QR Code"),
                        new Tag().name("13 Swiss Payments Code"),
                        new Tag().name("20 IBAN"),
                        new Tag().name("21 QR Reference (QRR)"),
                        new Tag().name("22 Creditor Reference (SCOR)"),
                        new Tag().name("23 Country Codes").description("According to ISO 3166-1 alpha-2"),
                        new Tag().name("30 Bill Information"),
                        new Tag().name("31 Alternative Schemes"),
                        new Tag().name("90 PDF"),
                        new Tag().name("99 Authentication"),
                        new Tag().name("XX Deprecated QR Invoice Operations v1").description("Version 1 of QR Invoice REST API")
                ));
    }

    @Bean
    public OpenApiCustomiser addDefaultResponsesPost() {
        return openApi -> {
            openApi.getPaths().values().forEach(path -> {
                        Operation operation = path.getPost();
                        if (operation == null) return;

                        ApiResponses apiResponses = operation.getResponses();

                        apiResponses.addApiResponse("400", new ApiResponse().description("Bad request"));
                        apiResponses.addApiResponse("401", new ApiResponse().description("Unauthorized"));
                        apiResponses.addApiResponse("403", new ApiResponse().description("Forbidden"));
                        apiResponses.addApiResponse("406", new ApiResponse().description("Not acceptable - please check content negotiation. Either 'Content-Type'- or 'Accept'-Header might not match this endpoint."));
                        apiResponses.addApiResponse("422", new ApiResponse().description("Unprocessable entity - invalid or unsupported data."));
                    }
            );
        };
    }

    @Bean
    public OpenApiCustomiser addDefaultResponsesGet() {
        return openApi -> {
            openApi.getPaths().values().forEach(path -> {
                        Operation operation = path.getGet();
                        if (operation == null) return;

                        ApiResponses apiResponses = operation.getResponses();

                        apiResponses.addApiResponse("400", new ApiResponse().description("Bad request"));
                        apiResponses.addApiResponse("401", new ApiResponse().description("Unauthorized"));
                        apiResponses.addApiResponse("403", new ApiResponse().description("Forbidden"));
                    }
            );
        };
    }

}
