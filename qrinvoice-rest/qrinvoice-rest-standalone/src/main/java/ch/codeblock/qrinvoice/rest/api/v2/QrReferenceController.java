/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.util.QRReferenceUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static ch.codeblock.qrinvoice.util.QRReferenceUtils.*;

@ExposedApi
@RestController
@RequestMapping(value = "/v2/qr-reference", method = RequestMethod.POST)
@Tag(name = "21 QR Reference (QRR)")
public class QrReferenceController {
    private final Logger logger = LoggerFactory.getLogger(QrReferenceController.class);

    @Operation(summary = "Validate a QR reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid QR reference"),
            @ApiResponse(responseCode = "422", description = "Invalid QR reference")
    })
    @PostMapping(value = "validate",
            consumes = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> validate(
            @Parameter(description = "QR reference", content = @Content(schema = @Schema(example = "00 00000 00000 00000 12345 67894"))) @RequestBody final String qrReference
    ) {
        if (isValidQrReference(qrReference)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.unprocessableEntity().body("Invalid QR reference. " + qrReference);
    }

    @Operation(summary = "Create a QR reference with an optional customer ID. If the passed QR reference is missing checksum, it is calculated and appended.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid QR reference including checksum"),
            @ApiResponse(responseCode = "422", description = "Invalid QR reference - checksum could not be added")
    })
    @PostMapping(value = "create",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> create(
            @Parameter(description = "QR reference", content = @Content(schema = @Schema(example = "123"))) @RequestBody final String qrReference,
            @Parameter(description = "If true, QR reference is formatted in response") @RequestParam(required = false, defaultValue = "false") final boolean formatQrReference,
            @Parameter(description = "Optional customer ID as prefix to the reference") @RequestParam(required = false, defaultValue = "") final String customerId
    ) {
        try {
            return ResponseEntity.ok(formatQrReferenceNumber(QRReferenceUtils.createQrReference(customerId, qrReference), formatQrReference));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.unprocessableEntity().body("Invalid QR reference " + qrReference + ", (Customer ID: " + customerId + ") could not be created.");
        }
    }

    @Operation(summary = "Extract the actual reference number from a QR Reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Actual reference number without checksum"),
            @ApiResponse(responseCode = "400", description = "Invalid QR reference")
    })
    @PostMapping(value = "extract",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> extract(
            @Parameter(description = "QR reference", content = @Content(schema = @Schema(example = "00 00000 00000 00000 12345 67894"))) @RequestBody final String qrReference,
            @Parameter(description = "If true, leading zeroes (0) are stripped away") @RequestParam(required = false, defaultValue = "true") final boolean stripLeadingZeroes
    ) {
        try {
            return ResponseEntity.ok(QRReferenceUtils.extractReferenceNumberFromQrReference(qrReference, stripLeadingZeroes));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body("Invalid QR reference " + qrReference + ". Reference number could not be extracted.");
        }
    }

    @Operation(summary = "Format a QR reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Valid QR reference"),
            @ApiResponse(responseCode = "422", description = "Invalid QR reference")
    })
    @PostMapping(value = "format",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> format(
            @Parameter(description = "QR reference", content = @Content(schema = @Schema(example = "000000000000000001234567894"))) @RequestBody final String qrReference
    ) {
        if (isValidQrReference(qrReference)) {
            return ResponseEntity.ok(QRReferenceUtils.formatQrReference(qrReference));
        }
        return ResponseEntity.unprocessableEntity().body("Invalid QR reference " + qrReference + ", could not be formatted.");
    }

    @Operation(summary = "Normalize a QR reference")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Normalized QR reference"),
            @ApiResponse(responseCode = "422", description = "Invalid QR reference")
    })
    @PostMapping(value = "normalize",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<String> normalize(
            @Parameter(description = "QR reference", content = @Content(schema = @Schema(example = "00 00000 00000 00000 12345 67894"))) @RequestBody final String qrReference
    ) {
        if (isValidQrReference(qrReference)) {
            return ResponseEntity.ok(normalizeQrReference(qrReference));
        }
        return ResponseEntity.unprocessableEntity().body("Invalid QR reference  " + qrReference + ", could not be normalized.");
    }


    @Operation(summary = "Calculate Check Digit by Modulo 10 recursive")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Check Digit for Input"),
            @ApiResponse(responseCode = "422", description = "Invalid QR reference")
    })
    @PostMapping(value = "modulo10recursive",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.TEXT_PLAIN_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> modulo10recursive(
            @Parameter(description = "number", content = @Content(schema = @Schema(example = "123"))) @RequestBody final String number
    ) {
        try {
            return ResponseEntity.ok(String.valueOf(modulo10Recursive(number)));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.unprocessableEntity().body("Unprocessable number " + number);
        }

    }

    private String formatQrReferenceNumber(String qrReferenceNumber, boolean qrReferenceNumberFormat) {
        if (qrReferenceNumberFormat) {
            return formatQrReference(qrReferenceNumber);
        }
        return qrReferenceNumber;
    }
}
