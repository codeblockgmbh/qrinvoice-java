/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api;

import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.management.ApiKeyIssuer;
import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.Optional;

@ExposedApi
@RestController
@RequestMapping("/apikey")
@Tag(name = "99 Authentication")
public class ApiKeyController {
    private static final int CODEBLOCK_CUSTOMER_ID = 0;
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiKeyController.class);
    private final RequestContext requestContext;
    private final ApiKeyIssuer apiKeyIssuer;

    public ApiKeyController(RequestContext requestContext, @Autowired(required = false) ApiKeyIssuer apiKeyIssuer) {
        this.requestContext = requestContext;
        this.apiKeyIssuer = apiKeyIssuer;
    }

    @Operation(summary = "Check if API key is valid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The API Key exists and is valid", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "401", description = "The API Key does not exist or is invalid", content = @Content(schema = @Schema(hidden = true)))
    })
    @RequestMapping(method = RequestMethod.HEAD, value = "")
    // this endpoint is used by zapier
    public ResponseEntity<?> checkApiKey() {
        if (requestContext.getApiKeyState() == ApiKeyState.VALID) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @Hidden
    @RequestMapping(method = RequestMethod.POST, value = "new")
    public ResponseEntity<?> createApiKey(
            @Parameter(description = "Customer ID") @RequestParam(value = "customerId") final int customerId,
            @Parameter(description = "Customer Name") @RequestParam(value = "customerName", required = false) final String customerName,
            @Parameter(description = "Key Type") @RequestParam(value = "keyType", required = true, defaultValue = "TRIAL") final ApiKeyType keyType,
            @Parameter(description = "Expiration Date") @RequestParam(value = "expirationDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final ZonedDateTime expirationDate
    ) {
        Optional<Integer> authCustomerId = requestContext.getApiKey().map(ApiKey::getCustomerId);
        if(authCustomerId.isPresent() && authCustomerId.get().equals(CODEBLOCK_CUSTOMER_ID)) {
            final boolean demo = keyType.equals(ApiKeyType.DEMO);
            final boolean trial = keyType.equals(ApiKeyType.TRIAL);
            final boolean production = keyType.equals(ApiKeyType.PRODUCTION);
            try {
                final ApiKey apiKey = apiKeyIssuer.newApiKey(customerId, customerName, demo, trial, production, expirationDate);
                return ResponseEntity.ok(apiKey);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    public enum ApiKeyType {
        DEMO, TRIAL, PRODUCTION
    }
}
