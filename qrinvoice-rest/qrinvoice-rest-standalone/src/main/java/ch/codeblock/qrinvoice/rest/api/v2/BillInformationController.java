/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.model.billinformation.BillInformation;
import ch.codeblock.qrinvoice.model.billinformation.RawBillInformation;
import ch.codeblock.qrinvoice.model.billinformation.RawBillInformationType;
import ch.codeblock.qrinvoice.model.parser.BillInformationParser;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.model.billinformation.RawBillInformationOutboundModelMapper;
import ch.codeblock.qrinvoice.rest.model.billinformation.swicos1v12.S1OutboundModelMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@ExposedApi
@RestController
@RequestMapping("/v2/bill-information/")
@Tag(name = "30 Bill Information")
public class BillInformationController {

    private final S1OutboundModelMapper s1OutboundModelMapper;
    private final RawBillInformationOutboundModelMapper rawOutboundModelMapper;

    public BillInformationController(final S1OutboundModelMapper s1OutboundModelMapper, final RawBillInformationOutboundModelMapper rawOutboundModelMapper) {
        this.s1OutboundModelMapper = s1OutboundModelMapper;
        this.rawOutboundModelMapper = rawOutboundModelMapper;
    }

    @Operation(summary = "Parse Bill Information including optional Swico information")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The parsed Bill Information", content = @Content(schema = @Schema(implementation = ch.codeblock.qrinvoice.rest.model.billinformation.BillInformation.class))),
            @ApiResponse(responseCode = "422", description = "Unsupported bill information structure", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "parse",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> parseBillInformation(@Parameter(description = "Bill Information") @RequestBody final String billInformationStr) {
        final BillInformation billInformation = BillInformationParser.create().parseBillInformation(billInformationStr);
        if (billInformation instanceof ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12) {
            final ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12 s1 = (ch.codeblock.qrinvoice.model.billinformation.swicos1v12.SwicoS1v12) billInformation;
            return ResponseEntity.ok(s1OutboundModelMapper.map(s1));
        } else if (billInformation instanceof ch.codeblock.qrinvoice.model.billinformation.RawBillInformation) {
            final ch.codeblock.qrinvoice.model.billinformation.RawBillInformation rawBillInformation = (ch.codeblock.qrinvoice.model.billinformation.RawBillInformation) billInformation;
            return ResponseEntity.ok(rawOutboundModelMapper.map(rawBillInformation));
        } else {
            return ResponseEntity.unprocessableEntity().body("Unsupported bill information type");
        }
    }

    @Operation(summary = "Parse Raw Bill Information")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The parsed Bill Information", content = @Content(schema = @Schema(implementation = ch.codeblock.qrinvoice.rest.model.billinformation.BillInformation.class))),
            @ApiResponse(responseCode = "422", description = "Unsupported bill information structure", content = @Content(schema = @Schema(hidden = true)))
    })
    @PostMapping(value = "parse/raw",
            consumes = {MediaType.TEXT_PLAIN_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> parseRawBillInformation(@Parameter(description = "Bill Information") @RequestBody final String billInformationStr) {
        final RawBillInformation billInformation = RawBillInformationType.getInstance().parse(billInformationStr);
        if (billInformation != null) {
            return ResponseEntity.ok(rawOutboundModelMapper.map(billInformation));
        } else {
            return ResponseEntity.unprocessableEntity().body("Unsupported bill information type - only raw supported here");
        }
    }
}
