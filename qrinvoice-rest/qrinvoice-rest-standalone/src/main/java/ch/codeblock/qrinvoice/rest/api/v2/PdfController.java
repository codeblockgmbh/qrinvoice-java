/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.pdf.QrPdfMerger;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@ExposedApi
@RestController
@RequestMapping("/v2/pdf")
@Tag(name = "90 PDF")
public class PdfController {
    private final ResponseHelper responseHelper;

    public PdfController(final ResponseHelper responseHelper) {
        this.responseHelper = responseHelper;
    }

    @Operation(summary = "Merge")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The merged PDF file")
    })
    @PostMapping(value = "/merge",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_PDF_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> merge(@Parameter(description = "Base PDF file") @RequestParam("file") final MultipartFile file,
                                   @Parameter(description = "PDF file (single page) which should get merged onto the first document") @RequestParam("file2") final MultipartFile file2,
                                   @Parameter(description = "Page number on which to place the payment part receipt") @RequestParam(value = "onPage", required = false, defaultValue = "1") final int onPage) {
        try {
            final byte[] data = QrPdfMerger.create().mergePdfs(file.getBytes(), file2.getBytes(), onPage);
            return responseHelper.buildResponse(data, "merged.pdf");
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }

    @Operation(summary = "Append")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The appended PDF file")
    })
    @PostMapping(value = "/append",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_PDF_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> append(@Parameter(description = "Base PDF file") @RequestParam("file") final MultipartFile file,
                                    @Parameter(description = "PDF file which should be appended to the base file") @RequestParam("file2") final MultipartFile file2) {
        try {
            List<byte[]> fileList = new ArrayList<>();
            fileList.add(file.getBytes());
            fileList.add(file2.getBytes());

            final byte[] data = QrPdfMerger.create().appendPdfs(fileList);
            return responseHelper.buildResponse(data, "appended.pdf");
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }
}
