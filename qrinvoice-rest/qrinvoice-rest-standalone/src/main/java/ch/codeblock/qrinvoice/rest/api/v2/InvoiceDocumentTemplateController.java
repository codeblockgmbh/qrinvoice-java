/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2020 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.rest.api.v2;

import ch.codeblock.qrinvoice.documents.DocumentTemplateRepository;
import ch.codeblock.qrinvoice.rest.api.annotation.ExposedApi;
import ch.codeblock.qrinvoice.rest.api.v2.helper.ResponseHelper;
import ch.codeblock.qrinvoice.rest.model.security.ApiKey;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@ExposedApi
@RestController
@RequestMapping("/v2/invoice-document")
@Tag(name = "10 QR Invoice Documents including Payment Part & Receipt (QR Bill)")
public class InvoiceDocumentTemplateController {
    private final ResponseHelper responseHelper;

    @Autowired
    public InvoiceDocumentTemplateController(final ResponseHelper responseHelper) {
        this.responseHelper = responseHelper;
    }

    @Operation(summary = "List Available Invoice Document Templates")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of templates successfully returned")
    })
    @GetMapping(value = "templates",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> listTemplates(@Parameter(hidden = true) @RequestParam(value = ApiKey.HTTP_REQUEST_PARAM_NAME, required = false) final String apiKey) {
        try {
            return ResponseEntity.ok(DocumentTemplateRepository.ALL.stream().map(tpl -> new InvoiceDocumentTemplate(tpl.getExternalIdentifier(), tpl.getDescription())).collect(Collectors.toList()));
        } catch (Exception e) {
            return responseHelper.buildExceptionResponse(e);
        }
    }
    
    public static class InvoiceDocumentTemplate {
        private final String templateId;
        private final String description;

        public InvoiceDocumentTemplate(String templateId, String description) {
            this.templateId = templateId;
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public String getTemplateId() {
            return templateId;
        }
    }

}
