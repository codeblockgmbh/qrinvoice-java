# QR Invoice REST

* `qrinvoice-rest-model` is generated with use of `qrinvoice-rest-tools-generator`
* `qrinvoice-rest-standalone` is a spring boot app containing REST endpoints and including the webgui
* `qrinvoice-rest-webgui` is a demo webgui to the REST services
* `qrinvoice-rest-tools-generator` generates a separate REST data model based on the `qrinvoice-core` model 