/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

public class PercentageTest {

    @Test
    public void testPercentageValueString() {
        final Percentage percentage = new Percentage("4.2%");

        assertEquals("4.2%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(0.042d), percentage.getNumericalValue());
    }

    @Test
    public void testPercentageValueStringTrim() {
        final Percentage percentage = new Percentage(" 4.2% ");

        assertEquals("4.2%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(0.042d), percentage.getNumericalValue());
    }

    @Test
    public void testPercentageValueStringHarmonized() {
        // leading and following zeroes have to be removed
        final Percentage percentage = new Percentage("04.20%");

        assertEquals("4.2%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(0.042d), percentage.getNumericalValue());
    }

    @Test
    public void testPercentageValueStringBig() {
        // leading and following zeroes have to be removed
        final Percentage percentage = new Percentage("12345678.0123%");

        assertEquals("12'345'678.0123%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(123456.780123d), percentage.getNumericalValue());
    }

    @Test
    public void testPercentageValueStringFloatingPointIssue() {
        final Percentage percentage = new Percentage("7%");

        assertEquals("7%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(0.07d), percentage.getNumericalValue());
        assertEquals("0.07", String.valueOf(percentage.getNumericalValue()));
    }

    @Test
    public void testMultiplier() {
        assertEquals(BigDecimal.valueOf(1.07d), new Percentage("7%").addMultiplier());
        assertEquals(BigDecimal.valueOf(1.001d), new Percentage("0.1%").addMultiplier());
        assertEquals(BigDecimal.valueOf(1.999d), new Percentage("99.9%").addMultiplier());
    }

    @Test
    public void testAddPercentageToValue() {
        assertEquals(BigDecimal.valueOf(10), new Percentage("0%").addPercentageToValue(BigDecimal.valueOf(10)));
        assertEquals(BigDecimal.valueOf(10.77d), new Percentage("7.7%").addPercentageToValue(BigDecimal.valueOf(10)));
        assertEquals(BigDecimal.valueOf(19.99d), new Percentage("99.9%").addPercentageToValue(BigDecimal.valueOf(10)));
    }

    @Test
    public void testSubtractPercentageToValue() {
        assertEquals(BigDecimal.valueOf(10), new Percentage("0%").subtractPercentageFromValue(BigDecimal.valueOf(10)));
        assertEquals(BigDecimal.valueOf(9.23d), new Percentage("7.7%").subtractPercentageFromValue(BigDecimal.valueOf(10)));
        assertEquals(BigDecimal.valueOf(0.01), new Percentage("99.9%").subtractPercentageFromValue(BigDecimal.valueOf(10)));

        assertEquals(BigDecimal.valueOf(9.75d), new Percentage("2.5%").subtractPercentageFromValue(BigDecimal.valueOf(10)));
    }

    @Test
    public void testSubtractAddedPercentageFromValue() {
        assertEquals(BigDecimal.valueOf(10), new Percentage("0.0%").subtractAddedPercentageFromValue(BigDecimal.valueOf(10d)));
        assertEquals(BigDecimal.valueOf(10), new Percentage("7.7%").subtractAddedPercentageFromValue(BigDecimal.valueOf(10.77d)));
        assertEquals(BigDecimal.valueOf(10.005), new Percentage("99.9%").subtractAddedPercentageFromValue(BigDecimal.valueOf(20d)).setScale(3, RoundingMode.HALF_UP));

        assertEquals(BigDecimal.valueOf(9.2850510678), new Percentage("7.7%").subtractAddedPercentageFromValue(BigDecimal.valueOf(10)));
    }

    @Test
    public void testPercentageValueStringNegative() {
        final Percentage percentage = new Percentage("-7%");

        assertEquals("-7%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(-0.07d), percentage.getNumericalValue());
        assertEquals("-0.07", String.valueOf(percentage.getNumericalValue()));
    }

    @Test
    public void testPercentageValueNumeric() {
        final Percentage percentage = new Percentage(BigDecimal.valueOf(0.042d));

        assertEquals("4.2%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(0.042d), percentage.getNumericalValue());
        assertEquals(4.2d, percentage.getNumericalPercentageValue().doubleValue(), 0);
    }

    @Test
    public void testPercentageValueNumericNegative() {
        final Percentage percentage = new Percentage(BigDecimal.valueOf(-0.042d));

        assertEquals("-4.2%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(-0.042d), percentage.getNumericalValue());
    }

    @Test
    public void testPercentageValueNumericAdditionalZeros() {
        final Percentage percentage = new Percentage(BigDecimal.valueOf(00.04200d));

        assertEquals("4.2%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(0.042d), percentage.getNumericalValue());
    }

    @Test
    public void testPercentageValueStringPrecise() {
        final Percentage percentage = new Percentage("1.23456%");

        assertEquals("1.23456%", percentage.getPercentageValue());
        assertEquals(BigDecimal.valueOf(0.0123456d), percentage.getNumericalValue());
        assertEquals("0.0123456", String.valueOf(percentage.getNumericalValue()));
    }


    @Test
    public void testPercentageNegative() {
        assertFalse(new Percentage("0.1%").isNegative());
        assertFalse(new Percentage("0.0%").isNegative());
        assertFalse(new Percentage("-0.0%").isNegative());
        assertTrue(new Percentage("-0.1%").isNegative());
        assertTrue(new Percentage("-1.0%").isNegative());
    }

    @Test
    public void testPercentageGreaterThan100Percent() {
        assertFalse(new Percentage("0.1%").isGreaterThan100Percent());
        assertFalse(new Percentage("0.0%").isGreaterThan100Percent());
        assertFalse(new Percentage("-0.0%").isGreaterThan100Percent());
        assertFalse(new Percentage("-0.1%").isGreaterThan100Percent());
        assertFalse(new Percentage("-1.0%").isGreaterThan100Percent());
        assertFalse(new Percentage("99%").isGreaterThan100Percent());
        assertFalse(new Percentage("100%").isGreaterThan100Percent());
        assertTrue(new Percentage("101%").isGreaterThan100Percent());
        assertTrue(new Percentage("999%").isGreaterThan100Percent());
    }

    @Test
    public void testOfNumericalPercentage() {
        assertEquals(new Percentage("0.1%"), Percentage.ofNumericalPercentage(0.1));
        assertEquals(new Percentage("1.1%"), Percentage.ofNumericalPercentage(1.1));
        assertEquals(new Percentage("50%"), Percentage.ofNumericalPercentage(50));
    }

    @Test
    public void testOf() {
        assertEquals(new Percentage("0.1%"), Percentage.ofNumericalPercentage(BigDecimal.valueOf(0.1)));
        assertEquals(new Percentage("1.1%"), Percentage.ofNumericalPercentage(BigDecimal.valueOf(1.1)));
        assertEquals(new Percentage("1.1%"), Percentage.of("1.1%"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailPercentageValueStringWithoutPercentageSign() {
        new Percentage("4.2");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailPercentageValueStringUnparseable() {
        new Percentage("4.2null2%");
    }


    @Test(expected = IllegalArgumentException.class)
    public void testPercentageValueStringPercentageSign() {
        new Percentage("%");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPercentageValueStringEmpty() {
        new Percentage("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPercentageValueStringNull() {
        new Percentage((String) null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPercentageValueNumericNull() {
        new Percentage((BigDecimal) null);
    }

}
