/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.documents.model.DocumentTemplate;
import ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument;
import ch.codeblock.qrinvoice.documents.model.application.builder.DocumentLayoutBuilder;
import ch.codeblock.qrinvoice.documents.model.application.builder.InvoiceDocumentBuilder;
import ch.codeblock.qrinvoice.documents.model.application.layout.DocumentLayout;
import ch.codeblock.qrinvoice.documents.model.application.layout.Resource;
import ch.codeblock.qrinvoice.documents.output.QrInvoiceDocument;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class QrInvoiceDocumentCreatorTest {
    private static final Resource LOGO = new Resource(MimeType.PNG, "iVBORw0KGgoAAAANSUhEUgAAAiQAAACKCAYAAACaelHHAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAACAASURBVHic7J15fFxV9cC/575J0ybdWMoiW2WRpQUBURARioJsttI2L21ZBAUKAlZoM2kBl0EF2syk5VetAooiyNJMStkUoawiP1yRtew/WZR96ZKkTTLvnt8fk5a0zXuZmbyZSer7fj78fqb3zjlnlvveeeeee47w38a4RIytqvdxRPdUwx6o2UPR7QW2BUYBVUBl1/8HWAW0AC0C76ryKoY3ROVVD+8JqHia9Mw1ZXo3ERERERERmwVSbgOKjjt/Swf7JRUOF9WDFPYHhoSoIQPyAthHBR7wMvIgS+Pvhig/IiIiIiJis2fzdEimNH7aeHayCMcqHAg4JdSuwBPAEmu1iSX1L5VQd0RERERExIBk83FI3Hl7GjGnobjA7uU2pxtPgFxnyVxHes7KchsTERERERHRHxnYDsn4RJUzpGoqKt9U+EK5zemFFlR/a5X5UdQkIiIiIiJiQwamQzJpwfYmljkb5Txg63KbkyedoL+2jl7KLbPfLLcxERERERER/YGB5ZBMmbe38czFCFOBWLnN6SNtKD+277cmeSiRKbcxERERERER5WRgOCST544xTuwHqE4GTLnNCZnHrTHfZPGsJ8ttSERERERERLno3w7JtCu3NZmOH4KcQWlPypSaDkRn2qb6ReU2JCIiIiIiohz0T4fEnT/E4M0EZgPDgHcF/qUir4N9HeR1UfOqJ7oCpXX964xdQcZUIHaUY9hGYTuQUSg7kj3+OxaoKMt7ygXheqvOOVGhtYiIiIiI/zb6l0OSSBhnedVEVZmE8qYY/bOX0b9w6+x/hyL/uIWVDF3zaaPOQRjGoXoCH1dk7RcI+meP2AmkZ35YblsiIiIiIiJKRf9ySMYnqlhNR8mSPMcnqpzKqhPUGLd/OSf6tIWvkK5/u9yWRERERERElIL+5ZCUE3fuCKPOuQgXANuU2xzgJRur+CI3X/BOuQ2JiIiIiIgoNpFDsjHjE1Vm8NCzQOuAHctpisA/vHZ7JHfMXl1OOyIiIiIiIopN5JD4MT5RZYZUX4bybcp4wkfRZfpe2/FRrZKIiIiIiM2Zzfkobd948aFOfXbZPbrvsfeK6qHAqHKYIchuUj1oiC5ftqwc+iMiIiIiIkpBFCHJheMWVpph7fNRzi2TBSrKRK85fnuZ9EdERERERBSVgeGQuIlB2OoDjLA/hl2w7CTC6GydEe2q3CrDgE6B/1j0LVHzBujLVu2f+GDNE2FseZia5AUIKcoTWfrIOnZs1P8mIiIiImJzpH86JNOu3NbJdH5ZlYNFOFhhf6CyDxJbFH1M1NxmM85N3HbhikIFxdyGYy2yGBjeB3sKQpG7NF03vtR6IyIiIiIiik3/cUgmzdvRxMwkUSYpHEbxohBrUJZY9Bqa6x8pSILbcIRB/gAMDte03hHRGq+pfkmp9UZERERERBST8jok7vwhjninoPpNRQ4utT0Kf1Dr1bFkzrP5vtapaXBV5BZK3+zvDVvd+imuS6wtsd6IiIiIiIiiUR6HZNKC7U2s81xUzqZMp1e64YFea2PmEm6uez+fFxq34VsgPyuWYQHMsun4/DLojYiIiIiIKAqldUgmpXYxRi9FmAYMKqnu3vm3RaaRrvtTPi8ytQ2/QOXMYhnlw/t2UOVu3DhjVYn1RkQE487fV/B6dJYFvd+m6+eW2qT+gLgNS0GGbjqibZqu/1rpLeobxk39WtEeC0dqS+VXuXtGe6ltisiPmJs62kPrexozIou8prrbSm5TSbScmqw27cRRrQeGlERn/uxo0AepTV5m92n9IYmEzeVFtlIuMGs5EtityPZ1Z2vT3nGWhcYS6oyI6B2xW4hyVI9jKv+1vZkEGQeM7GFklZbamFDQzwvs2eNI7MOovtUAQIVP+K1VRe8stT1Q7PyHRMI4tQ3fNGt5CeUH9F9nZB0xlB/Is1VLOG5hbqd6boi3GrFnAaW9roh+G7cpWvgREREREZsFxYuQuHN3dp51rlc4omg6ioQgJzK0/XZ1508kPXNNb/MzTbMfNDWpnyF6Xins62IXR14f70HJw2qhk60zs5cjsq8a3RGrwzFmGMowhGrUfoRIG7AGWClW/u2p9wKDh7wYbVtFREREbB4UxSFx3ORJCj9TGFEM+X1gLfAvRV4RYQ2W1YjNoFKpoiNARgrsAIwWOAa8u/TU5ARuiLf2Jth6HT8wsYqvA8OK/i7W6VT7DQaiQ+ImBsW0apw1MgnVQ0H2wlChaDbOJALaFXBSANkg/qSiGDHQ0Q5u8i2Ff4rIfVZ0GYvjz5ThHUVERERE9JFwHRJ37giD8zOFk0KVWzjPITwolkc8K49x66zXQRT891cUYFwixqjho2Nkds90mH2Av/WqaenFH+CmFoHOCc36XhDkWD3pii246aKPSqWzT0xpPMR49jzgq1YYud7Z6BvbC2yP6vFGATf5FsJS63E1S+JP9VV4RERERERpCM8hmXrFaOM5vwf2Dk1mYbyIcLMVu5jFs5/bcKguNwnZMvMvZ+DlfBTbTEfKxCrOo3RRkkFOpmKCB78pkb7CcBuOEOS7Yu1RJTjXtT3KucZwrrjJPyH8xGuKNxVda0REREREnwjHIZmS+qzx7J3AtqHIKwCBRxHmeU11d62LgpScpRd/QG3q512niUqCVZ1Af3VIpjbuJJ69RuDYcqhXOAzV5UDkkERERET0c/p8ysZxGycYqw+ClMUZEXjYGvmcl44f5jXF7yybM9KFVe9XpdQncASJRKmrxfaKU9PgGs8+US5npItOi/xX1r2IiIiIGGj0KULi1CZPUbXXUZ7ut68IZqaXnnVHGXT7k579Am7yCbINAUvBVjxdPRboH/kSbmKQkerfqDK1QAnvCfqKRVaJyEoAVTvSIMM1W8dhV6AiJ0kqN9Bc968C7YiIiIiIKCEFOyROTfJrqvya0jsjCvoLS/Us0ue1lFh3bqjejEipHBKMo1+0/cEhcRcNFVpvRTk6j1e9iLBErDziZZzHundi7h7q8tbrSAzCDNvb8XSsin4BOB7YpQe5GevYy/N+DxERERERZaEghyTmpo626OJCX98HXrcip9IU/2OJ9eaFtWaxcXQupSrNbxlbEj1BnHTFFk5n291dTRJ7wyosVWNSLJ7157z0pBMdwJMePAncCIA7f1+DNwE4A/gkAMJNLK5/JS/ZERERERFlI//cg5rGQy26FMitkmlIKNxpM50H0lTXr50RAG6tew14qVTqRGSfUunqEbfJkc7YLQq9OiMC/2tx9td0vCZvZ8SP9MynbTp+mR3TurvAVxW5y4q5LBTZERERERElIb8Ix6R5OxqxS4Hq4pjTIxlU5mjzrPnlTljNk78AnyqFIoWyOiRGX7sM4Su9TMsgepG3T9v8XPsE5U0iYT34Hdn/IiIiIiIGELlHSKZfXeE45mZgm+KZswkfGuR421zXOMCcERD9Swm1bZ1z752QcWobJiP0dsx5pVE9wTbVp4rmjEREREREDGhydkjMh6tSCocV05iNeM7iHJxJ1y0roc7QsGrC2Y7IlSFrRpVUH8CEecNU5ScE58q0WZEJmeb6e0tlVkRERETEwCMnh8RxG6YgzCi2MesQeMRWZL5AemZelVL7FVsMe4puh0OKjuOUvA6MGWS+D2wfMMUzqhMHRN5PRER5iCKGERFd9O6QTLtyW0V+XgJbAFD0Ng/nmAHTn8WPa87uBN4rmT4paV4PTG7YA+HbgXOES6LISEREIJ3lNiAior/Qa1Kr8TqvBLYogS2AXKfscibp2tJFForL28B2JdGktjRHjLswDj9A/U9aCTzsNdU1QLyUZpWe4xZWMnTN6Jg4o9Xa4YjJrhXRNpAWz5PXGFzxCjfOWFVmS4Nx546A2O6OsguGYSiVCO1YVnvCa7R7L3LH7NXlNjMv3PlDgN0d9XYFGZp12tVDdZVneBNn0MvcfME7ZbYyk/PM4xZWMnzNrjEv9gnFG4KYwSK6IqO0QufLpC8u3QNQf+L0xGBWDx3tCJ/E6DBURgKgtIK2eI55lTWZ/xswv1+3YTuMM9pRbwfUVAFDwHYgrBZkZUblTbYY9lLXQ+9mRaBDEnMbjrWFV9zMl2vtmJbpJGo3mxCmwtul8hJioib3K1sfmZjcBqUmYEaHZ+y3Blwick6oUDP/88Z4k0TlMKX9QDAVVhVEWF/OrauntDEKHe3gJp8DfcTA0gyjl/ULp3vyvIOMY05GOQoYAyra7S2ggHSFUSuN4jY8A7LMIjeRrvtHjzKFNb6ttEuBO39fo97JInxJ8Q4EHBXYoMyeCEaBTCe4yTdQHhYxaY/Vf+iqc1NKApatSqw29WWLTED1y9C+J9Y4VizZtC1FdV2YuwLc5AcKfxL0HktsMemZH5bkHZSaRMLwTNUXjJFJohymreyP0djHbdy7vuuui6+xtuv3m1oO+kcj3JrRXR7sF2sQsqdXHWeyol8WOAQYhbUo3RejZEuCAgaFj1Z14iZfQXhCVB70jL2/P9ddMm7qO2B9T4MakXSmKX6fv0NyarLarpWfU4qri+gvbVN8OunN6wYmwlulujhnVErmyJkKPROVoFM9127aaXmA484fYvDOgOSFILuiku9XuzfI3hamG157G7fhf+ygwT8rR+TEqUkdh+gPFQ7K400IyL7AvgadKW7qrwLf3STpPMPqvnfIyp/se7LfV7xDuq7dubITwimKPcVQ/RZuMmWrW3/GdYm1xbO2O7rpU67b5Djy2jdVU/VW2T2Pd7OVwNdAvmbwFuCmbrboD0nHN4/2Ce6ioUbWnMWzegHCzutu0DkioGOAMVb5luG1/1CTWmCH6FXcEG8tms0BxGqTR3mq9YJ8GdTk+fBaAeyFspeiU40VcJPLQa6xnc5vule8LjemtuFiVC/zO/ug0JzRXR6EgBwSs1ZmgY4ujokbGmN19Dmb5dO0pXQhQiulyblJJAwqZwfM6LROpqEktpQIpyY1yeC9BPwEZNcQRG4HcoXpaH/ZqU2eEoK83JiY3EbchttV9PcKB/VFlKKfs+i94iZvxZ2/5foBY0obFp/auJO4Dfdm35Mc0kdp2wONprX6SdxUiU4UmjUb/Dk5uZ/h1X+qcg2wex8EV4KebuB5U9twMeMSpa6qHSpObWqaoe0lVOcDO4cgcgdEU2YtLzs1DW4I8nJncmovcVMPWGWZIEcTQpPbLvYBvTI2KNOntR0mxk2ej4pvkUqFu5XWk9dFq3r+IE5cMBL0wiLZ2M0YXdbdmM0OU8LS+hX2/ZLoea5qfwIuCAp3cstFr5bElmIzPlFlaht+q6JLgB2KoGGUKjeIm1yCu2hoEeR/zOTGA0yMJwSZEKZYgYkG7wmmJNe1LyjZNoFTkzrOePbprot6mHzKoA+a2mTRTxYKuj5C5tQmTzGGP3dFosJiECqXyaih92av6wOMCfOGiZtsVtWbKE4+3nYq0mTc5I1dOUdFxdQkzzZGnxD0yCKpeHNdtKHcODUNXwcW+o0LPKw4k7tvk/bokJhBmQvIdlYtJs8r1i3Dnm0pKVXjQcWrKEmExNjgiqxG5IZS2FF0pqW2NoOrH0Hl5GKrEphkaP1f3IbiJEDXNB5qjH2Y4CPafWEnY3mIyY0HkJ65BoofGXTc5BkqegcwokgqYij/Y2qSPyiSfAAsrAJwapNnqfIboCg3RUGPNBWZR5h4+VbFkF8UJi3Y3lQ6jwlMLoG2kxy8PzIttXVxxKuYmtRPEa6imG1XlJv6wwO+4yYnqsi1+OzTCPJXr92O77perGfTJ3h37gi06DVHPrQ440nHVxZZT3nR0kRIBP668RdbLBTzlYDsiRZPW35fCjuKysTLtzIZvQ/4dA6zM8BTIC8hvAvaddJBRmF1WxF206ycXn4Lsq+B++3E5JEsjb/btzfQDXfengZ7BzAsh9nvIjwA+rSoeR/FqrEjUdkGYVuUA8m2KejpQWYrY+xd1p3/OfDeyVFfQThuwxSFa3zs6E67wiMi+iTwtiArVaUKdJSiY7J7973YKSRMTcOHtrn+JyGZv6F4kVVO9vDAVfi/n05B/qlqX0HkA4QPQDtARoGOEmU3RT5DNq8giLFOrOJOz53/5VJdLwpm2pXbmkznA8BeOczOCDyhwstY3sXwPioCOgqRbVHdHdiPXh4QFQ4yGb3XnnTFl8MuO2FqUz9FOTePl7SCvCfoOxZWGhis6BYguwFVfi+yjvlt363tG7Gahq9YuBn/a95THuY47qjb5MFlkxcYcWagRT3m6xlw7UAuepYz4pQgKXiVB0XfXgNgfKJK0C/4DSs8POAjXuMSMYlVNNG7M7JchCs99ZpIzwl2rN25Ixwxk1XlO2QvjH7s48RY6rmJI0P5HE9PDDatpgkIfCoW+BuqP/TGtv2+19L+k+btaBw5BbgAZONifJ8weA/0pq9PTJ53kCK/IdAZkVdF7Q+8IbKEG+KtfitQ3cQgh+oTFK4A9vQXJ43UNDxBc/0jfTG9ZyN0b4vcSA/vR+BPKIu8IdzJDXXBiZcnLxzutHfUqmicgB5aCp83ZK60EJQHVl6OW1jpZNqXau/OyJOCXum165Jej/SedMUWTmeFq+h3CO79dYB0xpp1XOIYHkqEcnDR1KYuRDUXZ+QZ4NcW+QPpWc91z6tcH/Jwmxz41+4O5jOqjEP4ErBbdlCfZvGsJ8OwuWBqG75gVW7FPwr0oo1VfIWbL+hxa3dDhySRMDzLmSGbuCHKjzLN8QeKqqOfYAfr+XiZmaEKXd3ezp2JtlBl5kpV1V5YBvkNi8j9Az0z2Wwz9DJUvxQwpRWVeju25aqc+/Kk56z04FckEtc5z1Z/Q2E+MLynqQqHGh0638L5BZi/Aaal+kdIoAOUAS72xrQ2kkhYmnMQeuvsf1uYy4R5i8wgGhA5Z6MZxWso6c4dYZDF+F/sFOXHtnXQZdw9o71XeelEhwdLmX71XWbFqktRLvKZWWFEfm3HJ/Yrwtobu+k/6Tsi5hyvqe62nKXcOGOVB79kXOI6s031RSjfx/cJVaY7tcm7vKb4nYWZXFxMdUejwucDpqwSmOml636V82GImy76yINrSCR+aZ4Zeg6iDfg0iRX4kmwz9DILswswf0Pc5OdR7S3J/w0RmeE1zbr94/dT1/PM7HbMCx68ANwEwOS5Y4xjpgqU9zTV5MYDjNq78G+++5p1zFFBtX822N+JuQ3HWuTuMG3cSNnDHrt8uT/scUXkj1ObmtaVXNYjVjmS5vhDJTQpXNzUZwz6F3xDu/qONRzP4vrH+6hnH4PejX9ysFr0SNL1DxesY0rjp4y1T4OvA5kRZJqXrsvFDfHF1DacR+/9jLIov7XN8VML1lWT/J+AFhYZQc/y0vXXFS6/oR6Reb4TlB/Z5vj3C5LtJj8it7y8l62TObqvieGOm5yosBj/bZyXbUvl2Jwctx4wbvJ5fKJKdm1rdcGOm5s6zKAP4x8B+7c1HMfi+DMFyV+vp2F/g/we/7wqz2IPJT37rwXrmH51hflo5T+CkpSzBzus22uUtQg4tanTVPW6HgeF79imuG9C6iZMTu1ljD6Mf/PdtyzO4b21g9ngS7divpmzAfmz0sPZfE/U/BegqH9YG0Ccp0pkSlFw0J/gv8/cEoozApCuW26NORr4wGeGGOSqbHi2MIzaS/F3RkCkvq/OCIBtql8EXN5XOb0yee4YJHAPfnZfnBEA21zfQDYC0zPCd4qcFLrKGnNCGKfUvHR8KSoXBEzZ3VR3nNFXPeGiYtCf4u+MrLDIMX12RgDS9U9YnGMAP0fAMZirQAuubWlWrJwe7Ixwn24x4oRyOCOhMvWK0cboMvydkQ+s4Su59Kb7+IufePlWqIZ6JLA7ArNIz/xPseRHlIQgh+SDgVwZMlabOiYoTCxwQSjOyDoWz3pRVE8PmLGXw+snFSR7auNOQZV0BR61TbOuLEh2D9j3WhNAeJ9NDxjjXIzPFoQid9l03YIw9Fg6vg34HaEfbpxYEW/iOofFs14MS5ptrvuZov7d0kUvIJEoQxm7nnFqGicSkLslyrdI1y0PTWF65tOCBuXSHNBlU/64iUGo1AfMeF07Y+6AL/8+acH2xostA3b0mbHKWntsrk7k+h+jqRjkUqTjSIren93vixjIqAbVAdA3S2dJ+HhqZ/mNCfzJS8evDV1nc/1dQU/kivrlNARirD2DgFM9nujsUAsRPpTIGJGLQ5O3MZNSuwC1PqMZtc53Qns/6YvfA/ml77hIsaLIr9j32n4RtlA1Thz/zPo9Ys8MK1Y9jPwR9V2DCvd4zfFbwlbppesXK3JXgE0F/a4dGTqZgHpNIvqt/lRNtSDc+VsaJ3Mv/gX82qzqV1ky+++5ilzvkKjqsX21z4dORc/bLCux/pdhwLd4lyID1yGZNG/HriOgPSLID4ul2lr7PfxvGHtT2+B7qskXFd/oiMKdNNU/mrfMXsg01d0DPB+2XAATsyfj52AJN7Hkwv8LU5/FXIv/d7InbirolEaByPVhnerYgMWznlTU9xCBFc/P0Sstkxv2CIpQqpqirUG1eonvGHwGt2H/fGVaVd9cKYFHvKb6gV0eYcK8YQ72bnpMygag3ahOzPdkWtYhGZeICYzrm4U+qP6U9OwXiiI7oqRogEMCtJTMkJAxjqnFf9/6zcyYlvuLpnxJ/UsCf/QbNkh+SaDu/N27enb0LE/lurzk5YPKjcWRa3y3rqxnw68Pkp75MqhviNkIx4St0mKWhi1zHSImIFco3Mq9hWIcmYp/YvQrNM98rGjKl8SfEvB9ii9gDW4p4F89OFswbODizh8ileYORT/nMyMjMC3TXH9vvqKzF+Fthh5KcSoevm8zFUXzbCNKjq9DIqIlakYWPkpg9dlbcj7eW7AB4l/MSMkrculI5vCA4TavveUP+cjLB+vY8J/6pjbuFOBgvcmS+p67DvcVkYAws++FuFBeIT3z6ZBlrsdmnNsBv9/wdkxpLN5R7RwRDViDyk3FjrArAWsQjstHlqN2HP5bpmu8ikFFcz6LzvSrKwTbFBDAsCJ8w0vHC3qPBsBYG7rHn0UbB/w+WUR3fCsEYk1BxwfLzvSrKwR8b+JWzZJim+BJJk232kcbsQvuvODTTd1QxLcpnMI9Ra1hs3fbE0CoFUAdz/PdSkP090W7UWlAkq6Sdwi/F/43ZHkbcuuFbwG+BbMcz+a/LRgmpyarFXwbI1qn+GvQYv1PV8HeTJrnl7S5CQq+dYwUfl+ODt95Y3XTdeU2OWbFqusF/arPqxTlXK8pXnC1WAMgIl8sVEAAH9h2XVQEuRFlo4dW6esQ7a1sdf/kg1V7498/xDLEFr/yYfbYn+/pCkednLt3iuJ7zFCEv+RpWX5kI0kvhSlSlYP9xkTN38LUtaHswJyoT/blOGgPyoqSe7MByrO+Q8Z/i68krGU//CMK7byz2tf20EjXvw287jfsxPJYg6IHBIz9OU/LyoIYNqkMbHi1EWWq74tU59jm+NV90WsAFP+LWB9Y0Gs534gBhvhvy2gRG0YVEcf4JmUBvMwN8eCS3WGh+oTvkGguPXWyMwOOZovV4l/YkedClSbi+/14qqEdkd1ELxJUG6IS94rQmrCJNcV3SMD3uKyq7F0C/b44ErgGlxcl2bcHlIA1aPNag/45XJaBUatJN3RIjJs8H+Q7AS/4YbaOT9+IMbVxJzwbdmffdpsh9CNsEWXH1yFREf/tnH6MquzqF/VXSnjxMPIkyjQfO3Lb45/WuBUZ/2ZxnlaEV8PBD9Hnw2zfpOB7s3SEas9NfSY8bd30qrdDcPHZwSOA98LQ5TnFc6zWIUae0x6i8AACO5fzCKSq7Op7qElLtwZF5UmEnpN8Jcc16F6xNVT45mNmZIA4JMj6QwqOmzxBwb9ukeoC21wfSlfsmGO9fTWHqs95IdwUasfSiH6BwCq/C5eBbQZkCV7RT/gOweululAL/Cvgs90hp8+2I7Mtxre4a4Z9V75K0Xfj9b1cqsjnxOmJwbSypa8muMsUqXmlSq/vwW+bL3/EFr2goCfytvFxSICNmySWFtFPBHyNvtsooZsRsAYFdshJiK3YPqDtY0vX1lC/x0OzDsmUxk+rtTfjV8FauN6m47MgqAZc7hgNKG1bKFbk52HLjCg/Fn3Lb0xRv54Q/RoNuhgLJdtyFFXfRDf177exATFH/Eo3A6wu+mkhQDDhHf9eW7kdoXk3YWP9y/Lni6cl2Ba0Qd/LVqHmxOSJav9Yg5hQ1mDQvP6fzLoe04I7d4Sxdgn4R11FZS/cS0PLHzQoe4QlrIvnWFxXtGSziPIhIr4OCbAdbiK8i3TpCDg5JCW7GGbUCbpY+XXP3ABFgp7aS1QnJvDGlx86qBilCPof760tvkMimaDvxXD6pWXLATNBa1BKtwZFTQhr0AbVahpAOZVei2CuA3YLmqXo5wxVfc4dWUdM0VES5kOIcHN4wgYYbvKTRvXbiPkK6JbA4B5mrQLt2+6G8IBtqj+rTzIKI6gXUQV28B7gn83fP5FKv/1rKeXTmWNX+VaKyHV7QCXoplKS9yKYFg1rGyVjBweEvzcX2kuStCkVLf7FZ4EVIwYTkCNWXALWoC3dGsxYXWX8f299XoMygBwSo+YihBNzmy0zHDf5cKG1R7oTM8iWYe7CWjFB57k3W2K1yaOskkZkZODChy36GoUWxLfnQzERdHlQvpFjzFhvwDkkQaHqPjqO+ZCRDMb3d5Nb11/VGP65DyW52WRgTWg+hOigfrtjEx6l+Y1ljA1UVaFldP36yRo0kgm4dufYeTvwNztwikcKp+UzW+FXTF7wZF/bOMQUQju+BjwXZrfKAcOk1C5W9RYg7NNKPfGG926LfzOoIuKpPmMCbhAq5gvAQHNIfcPlqv57p6ETY1hAhCS3C5k47f4FOXMLOfcrVDoCfm4tCD8uoTUbojaszuVDSCRM0fN7PDss8JY62CtbYUMLrb5fsynhGlQd5v97y7USMF6B1gAAIABJREFUtXT4ShiIa7AbitwVUBRtpGMyi73jFh7G3TMK/i3FgK0KffEmqBatLHW/ZVwi5ji6WMP8HINQuaZU5/I35ZMvw2tr8AtfqvafzqE5Ioh/KLuEF8OYJ8Otf9HRnKqfCl5HQAQraG87NGIwJLQ7a8ysxfpKG2Sb6ho2g6adwuNDqil2OF8yQwMf8tfuUsand/81WMqHAkfMcPV16E1uFYhV1gb8JEvnXIVLRpDzbLruGqlNXoVydk+TFA4yQ9tTFr5dqCIDDC/YzI2FGXNPWLIGCmZU1fcU/2qSIdNpY96vSqRrU9K1nqJBZa7HdLWKHziI9b8RlPBiqGKD1uH7ucjIBBfzKsl7URv4PvLD2qAkw0GceOXmkfRaKcV/co5VBH3/q0jXlu3Uvmj/WIMErEFBc1qDEnBSB3QgOiSrDTreS9ddA2CrWi8goA0BcL5T0+AWqswQ3h5mJrOmxb/V8OTUXrgNR4Skq38wOXkwyMUl1JjmltlBJa2LjsB9QcPGsT0W9+rH/Mt3REpXn0FFfHUpmtt3bjSo9k81068ufnn/MKNKLZVvEpSQVWmDjjkPHIwTnhPng2MJ+qzKXRujf6xB678GbY5rMCNBn6WMZFzCr0R+f+Tf1pgvZtL1H+98XJdYa7FTCDi1pyK/zHYdzx8D+PcnyY+nghp3GaPHO8i8cp53DxV3/pbGcCP+PRjCJmOxZe+cbK0GOSSAnDqQvmOxAb1XAvrChI7qfn5DIpJbcahBEnRjcVi5oqCLRD6olfBurtm9aN9qqDHr7RqarjLieBp4tDIMVKxvxVuF8j7kiP8aFPBdF6FjAtYgOa5B6wR9lpWMGlr2zso58qTFOYTFszaNhqRnvyAq5wS8drjBa+L0RE+nTAMxgG8STl6o/jVwGD1e4WDHbZwcir5yMv3qCiHTRC9ntENF9DrSs18omT4/ltT/AzQok3ofpyZ5Qsns6SOe4wQlYe9TkqgCIBDQK0Nya02f7bvjm2zpWKcETdQkVKdHEd9TWzags/FAQg17lUDNPn4DIpT1uuKp/xrM9lkr0QOO+q9ByXUNpmd+CPhGKp2AB49+hfAr0jN9ryVec92NwLUBEg4wLVUL8lUbmkMiIn/3HRyXiAlyKICil5XqIl8szIpVPxHEvy16+Ky1xil7dCSLKBDcXlq4ZMBESbKdRP32fSv5qLX4N4tEwgQ1uDSqOXccVvw7x6oWv6uriIYaVRL0n/5jFKNLeemxWvTmdqL+DgkBjfdKgsg/8T9JNpzJV36y6DZkizr6fg+ezWsN+jovCvvnaVm/xa5tnQE84ztB5BynJunfHbgHDGgox728oBbaWw3ek49PZnzKfLTyzDB0lgNT01Dvl2VcNFR/zi2z3iipzgCs4XoCzpcqcohT2/j1EppUOA8lMgoP+w0bMsWP9jxTfTj+SaftmcH8JVdRgjzuN6aiRb+Bh905XFQeDdD1OdxFJTk9VExEinyTci8fpXCA37AVExjdLjrpmWsUfJPljXQWfQ06VB8N+FWabsG0+jrGGyMY/0rlwoCJHvfKnYk2i0whqHSCcA1TGnPepjJC4L5z7jjGN+znSGzjBfcD3LkDLkPe1DTUIzKvxGo/spK5osQ6g1lc/wpKc9AUVW3AbdiuVCb1BUHuDxg+qdj6jdGT/cYUHuvaiskJUf2j7xhyBO7lo/K1L2emJMcCW4Qp0qscdB/+UdzBDq01YeorBwoH4c7PrXlbAThUTMD/zG8rI4b+o1i6c0VEfdegSPHXoGrgGnyYdCLnnQSjEpRnN5bJyYGxbZML6brlgp4fMGOYsbYJd35OlW6NwqshmPUhN9f5HotSsRt5SLKtwVwagt6SYdyGOWVwRhCIk744lDbnYWKVywguSbuNIDfjNuVY4bB8WMdLAz61XWTfol5ATk8MRsX3pipCXuWYPbGP4H8DjzlSkWM56PwxnoZ/wurGGasUHvAbVuTCAbM96I8YvInFEm7BV7bCA1xzdlgHGwrGCovxiboqcnChpzZyYsK8YYh8zW/YkN8azAxd/SgBjfRMCRysUuKl669DuD5gyqeNev+TiyyD6qsh2NSbjB6elOU83Ib+v5/mNjmmtiEFUvIohcIDXrqufHVHglgSfwrlxqApAuMMr13d728Yt8x+U+FOv2FjqCuWatNafSb+FX47rCM35SUwPWelBh3NVorUA0kFMUW50BrRXwYM7+fUpnyfbgcKAnnttefM5LljBI71GzZIuih682Vx/SsBv1sxmilauwwz2JyLf4O/Vm9QZX6f0XWJtQhLfMeFsznpilAjieXGVnIu8JzvBOEspzZ5Sm9yjIh5ra/GaEBWcXa8x5bMMYOk+/XWzckLhwuvLUXL0jumTXHO7s+VKK3HLODDXqadYdzkVf39/L2D/Dxg+GSmNB4SutJpqa2B7/uOK01BkUc/jPo7MQqfdWpToUdJnNqUCzo6bLkA3sgRdwC+xy5VWcDUeZ8ohu5SofCFWO280Csdi3F+hP92zSqv3bstbJ2FYiRgDYqcwZTGgJNoBTJ13idQ5vjr5bfcOCOoQF+PWKu/DhgeaTpjl+Urs19zQ7zV4kwhoKq0Kj9ncirwkIARoc8OiUiwQ4J/wt7ugnNtv3yCdhv2Nx3tfxcYXxb9oj8gPfPlsujOlaXxd0X1wt4nynQZVX03Ey8vTXn9Asik65YpPOQzbIy1v+HEBeH1KkokjGTsLwC/nA5rHXt5IaI9aUkTUFtCVX+CO3/LQmT3yIkLRqqSDE3exlxzdqdA0CmzrR3P3DrQE1ytmkWF1G7wI1Y770ghoGOr6rXcMbvfdKD1mmbdLohfgm2Fsd4NoX7HbpMjnvkV/hHKTmsycwuS3Vz/iIBvQjZwjuM2TihIdn8lPfNpEb4TMGOoMZpmfMIvGoXJaMY/zJIrmltp654QmGxqU9f2m1yD4xZWmtqGH5nswtijHCYI/NHq6LzPcJcDr7n+eoSre5sncJSJxZ51alP9tpKrGp2F/+mhT0lFZgmnJkMo861inqleIEjAzYJrWTy7sLWZTnSgMj9gxo5C5haOW+jbKj1n3MQgqcjcCOzcZ1kBeO+1/gZ4wm9c4WCHtnuKmrTrzh0RqlO6KXublurfhHItdJOftGpuwr/1bJu1GvQbKQOintiZ+Oamyb5C2+JQnLZEwhh59SqBY/zNYRG3XPRqoSokKPoJotibqEmOK1R+f8Rriv8C1ZsDpow1Q6p/6jdoSM95nb5W6hN6OTrcy7aD8g2jr91U9vokbuowM7T9cVS+C5TLlnc9x04rZ2+JfLHaOkPgT73PlG1V9SbHTf3FqU2dSCIRbsvz4xZWUtPwReOmvu/UpPI/Wr64/nHQn/gNC3zJWcv9ferXM2HeMOOmfoswI2DW23ZQZnbBOgDbOuinQQXsBDlahrbf0ae97ImXbyVU3SVwfMEycuWhRMYaczoBdZMUDjVUPO7UNoRnj9vkxNyGY01Nw00G5y1imeLmvQm1wqu39snxcefva+BBeszdW68nya2z/12wjmLRVP8oor4FtwSOd1qr7+nTFt2JC0bKs9VpVIKuEa9ZrfpewTqATDr+AKJBeXbVRrjbuA3fCmWXYHLyYFPTUHBju7CwHXo24F9wUvmGU5s6raehWHacvwn4Zhn3jvZ28+z92KJQKx+t2kanNn695DU33NRnBP2hoMW/sAZjjXCyLXO/mrxJJzq8kxee4HS0361waG/TFf0cylLzbPVb1CTTRvTuDPYx0nOCmsNtyLhEjK0G7+k4zgGqHKBwoNB+MMgQUBQWFvJWLG31DlUHK9JjzojCwcbRJ6lNXmG1ahHp83x7OmyA2+Q48to0VX4MBDk0GQMn25su+qgA8z/m7hntpjZ5tlXuIVsAcRMEviKdsSekpqHOa44355yvND5RZQZXf5PsE+DGEYk2ss58+A794llP4jZcAPKzgFk7qsrvxE09aEQXeNp6Tz5HNgGYesVoJ1NxlApHwWtfssgo/ybK4SPIBKnILJea1Pc9abk+Z/vd+UOMeheA9138kzQBfdpWtRW2FVECrMZmGLzP4lO9WOFw45mnqU392K5puTqoZckGTL+6wvlw1SkqmR8BQcesOyxMy3ltB2Adc4HJ6BfxjyAOBvmZ46ZORRp+7Onoe3J+GHXnD4mpHmpFjwKdBHxKkH8Avg9VJeGO2aut2zDFII8BPUazVHURk+f+nSVzNqjELACmJjUL0VTBBig/ss1x3/CUqW34RS/eaHdWCJznpeP5nS7IGxVqUkeI6HcE+Rr+oc3SIVxqm+KJcptRMBPmDZNKc5vAlwp4tQVeUfg/EX0DKx9gWIuKRbUKw0gs1SKM1uwN/RME9VNXFtrmeNB+pj/u3J0NziP0vg3RgujtglnmqX2STOYNll78AQAnLxxOR8eOjtoxKnI4MBnoKbl7I7t1hm2uD+2CYtzUFaD+SXsf8wpwi1H9Y8bRN2gf9Ba3XbiCExeMpEK3Bm+UETlEVY8UOIKeu4R7RjhWlR8qfH6TUeW3tjl+ah/fEsZNNgIzc5y+QuERER5H9R1R846HfhgzxBQdgTJMxWyB2r0E2VOzJdZ9t32sciTN8YcKsPkjes5V+LbCCQEnYT5A5XbBPuyJPo9j38Ub/j7p81oYn6hiUNUOjsN+ihyNUkvvdWBarOHzLI77V9jM7f08D+zZ05hd21qds5Pghzt/d4P3R3pfMytRbhOR+z0jT2Hlja7y7dktNuPs5Fgdo8gRZNdgbw0ZVeAsLx0PKoueH5PnHWSMeZhAJ3E9b6HcD/q0GPMO2PcEXaOYoVgZrkZ3BPYUZR/NOmwb3PAF/uGl4wflYpZTmzpNVa/rcVD4jm2KF/RQt45s5Cfw4WG5HcznutdZigFYMbcbvMIdEuNftbOLfIqvjVS40XGT0wW5LJOuW1awXT3hzt/BSOY0NPUNYPf+4IcAKNyrusuPym1Hn7hj9modlzhGtqmai8pM8vtwDbCHwB6oZF+pZP/Puv8twYVPQiM953U79YojjOfcDxLUwG0oKicrerJBIFYBblduZ0d2F1Mlj49ANG7T4TkjAHZMyyVmefVotNdjpbsBl1iRS4wVqMh0vZdu5VlUg79QpS6Tjt9napKnID04JCFh03V1pibViZDLttZIgfEo40FQUQxgFdb/PDX74yrHcTaLsxQ6bhBij0KPpf23QvSbinzTIOAZoO3j3xld5udGhxGpsYvr+uSMlIT0zJftlMZxxtr7gJ0CZo5AOE3R04zt+iC6fTZY6OVX2x1F5XyvuS48ZwRgyey/Gzc53mZLC/TmlGyPcAoIuv532WW/6PoLYL89etkNm67/ualNHYmq6zNlH9POzyys377JhnKzpzmCmoz1ornnsMw6BMm7eZPCERa913FTfzE1qXOZ2hj0o/TnuIWVsZrGL5ua5FzjJh83eG+gchlQ9M6nebBcO2NTBlLeiC8PJTK2qb4uezpJXi23OQVzy0WvWmKHS0BJ6xBpE+FU21Rf+EOBH4mEtdp6Wi+Fi/qOMMc2x6/s+qvIzdpEbXN8jgjTCThmOAB4ifTM/5Ces9LScSTgW/Y/BFpFdGKmqe6eIuoIl8WzXrRO5nAB/z5p4bFakFrbXBf0RF8wmXT8AQtHUebOyqXGauYsstHXnlG+7tQ2fHPdn91rQ6SBSwrUG1j22FN53BTo0yn6OYTPGU8X4SafQPTvwAuCvOB5vIuRDMZrw0plTNlGMaMUuwPIvghjoH0fC30/TVA09B3reCeQjq8otyVh4qXjv2N84kFTWT2H7FGw8NrSl4r0zP944xJHmK2rL0H4Lhuul1AQ5K8e5kyaZubWTbQQ0okOi55uapLPIvJDwl0Pq0T1TC9dv754lBh9UUtwkt9riv8Cd/6fHbyrcsldCgHtPYE/D0Q/7qGUvvg9O2HeOFNpFgF93tbaiOet0ZOzSdsDjFsuetWbfvWh5qPVPwSNE7RNWyACf/KsnMWSOv9+bGGQjj9m3YbPCHJV33I2BxDpOSvt5HlTjTF/wue6oyo/xZ3/N9Izn16f7GazrYR723rpERV6yXje+QVySWztnf1ROROVpCp3GMOfDfp3Y81yg/zTityjor9FZF427MUB9GtnhDaLTujL0bJ+zZ2JNtsc/77F2xnV2SWKmHgCjwgazlbfQ4mMbY5faq3u05UxX9Aa6YFXBM70xrR8nnQRnZH1iNrm+gZrOEghnKdkpcni7es1129QydLTWOHR1nxJz3zaS9cdJnAy8FSRtLyO8iNrdA/S8cfCEirIhk0d75i92qbjXxeRiUAYN8cWRH9scQ4ckM7IOq45u9Om6y6yyH4oTYS3Bl8Q5Oteuu7wojsj60jXv63p+IkCXxXwb8JXGCtU9NaQZfadJbP/jlAfMGOIwUvjLhq6wWOMuMn7C0xIfMGm44EV2MRN3lG2ImP9Eysqrtdc1/9+QMVkSuqzxqor8KWupKy+Rh06gWcQ/Ydg/uQ5/K6Q6qY5M6XxU8Z6J5PtctljUl8Aq1G9S5DFnuxyV1m36KY0HmI8PQvRyUA+1ZI/QDVtHecqFs/quSW7O3+IwWth49M9ISW1+pNNVDfolK7eJL0nEfdMRuBvqtxn0WWMbXuURKJPN8GeklqtY3b2PVE4LhFzRg090aKnCwR1ou2J51Gutx7XsjTeW9HKgih6UmsQU+btbaw5mWy5/d3yfPVKlDsFXeyNbft9X7/XPlPT8EUDJyFMBNm2AAktCg8a4U6vkpvyacRZ7KTWDVERN7VEAvoqATdt4JA4buMExd5egLZWyy4jgi6wpiZ5NsJVBcjeHFFBzvHSddeU25CyMj5RRWXVZxzDHmplNMJoRUeAVJlsdV9jYSXgicgqVNeC/Ae1bwryuufoG6wa/Cx3zwgvjJ4PUxp2czwOVCMHoOyiMNJkb+7GQquIfITV1zDyivXkL3yw+mkeSvg08SsT4xIxth12kLF6MMoeKjo6+/nr8K7PvlVUXgf7gnXMn7E7Pz5gcp3c+bs74h2iqnsgMlqV7YBBBoZZZBVou4isRmkF+7qoedET7yXaea4/VTBlfKIqVll1mBUOBNldYQcD1cDgru9ohSiviJHnPNP58GYbce2JKY2fcqx3gKocALqziqxbg1hoE+VDjLwqyiuekb9gd3qm3/5+3Xl7OuJkf6/KLipsZ7IR/iEWVoC2CdIC/EtEXvI8fZ6thj/eH5ojhsVGG70qxk39HTgwX0HWmP19n5gAps77hPHM6xRhD3CAoaicX6zkqYiIiIiIiIHIRgWTREWYV5Ag6wUnld0y+01Ff1eI7M0K4aLIGYmIiIiIiNiQTSo4ek116dzKgG+ESq9Z7g4EdVTd/BG9xDbFC3L4IiIiIiIiNmd6KCkt6mFnkW/tFeGLvdXjz4xpuxfw39bZfPFQOc821RfUvTUiIiIiImJzp+d8juX3/Uf2+coOCJ/JQ9ZIHfvoMp5d5t+H5qGH1Iz9yn+AftvxtQi0i3CKTcevK7chERERERER/RXfbqt2CBcCL+cnTHo90uc1xe8U+GM+cgcwKyx6jNcUbyq3IREREREREf0Z//bvN8RbrcgZQO5HpJQpnJ4ILCMP4BlzFgO75HMuvGwNXyRd/3DvUyMiIiIiIv678XdIAJrq/gj63TzkjXRah/o10vmYxbNeROV7ecgdUCjcbvEO6mtHzYiIiIiIiP8Weq0JosvvfVTGPLYXMDZHmfvrmC/8nOUPBUZWdPk9f5Z9/nfvbL+ZzQYP4Uc6pvVcfvbdteU2JiIiIiIiYqCQQ9luUTs4eYazlp2VnFqKf9JQdYbt9YivqJVFZxha9wbZNydr+zf/tsqppOMPlduQiIiIzYBTk9V4mWzJ+NXt7UUtxx4R0Q8I3rJZxw3xVg/vOOCfuYmV73FqsrrXaenzWqynxwMv5Sa3X2JRWWQHVY6hOXJGIiIiwsG080vTGfvQdMY+NJVDG8ptT0REscnNIYFsG2H0eCCXvIjtTbtempPcW2f/26KHA8tztqX/8JIRe5RtrjufG2esKrcxEREREUVnSsNuxk09Y9xkm6lJ5ZNjGBERSO4OCUC6/m1bkTlc4NFe56rMdNzkCTnLpXOcwEA5kbIKle9ZWsdmmmY/WG5jIiIiIkqF8WQG6BhgCKKXctIVW5TbpojNg/wcEoCbLvrIG8wxCkt7mSkKv2Ricpuc5KYvfs/bYvjRiPyEfKvElo5WlHk207mrba77MelER7kNioiIiCgpRqu6/0VnZZXv3IiIPMjfIQG4Id6q6brJKBcTXKdkO4nxG8YlckieBa45u9M21c2wIuPoX3klHaDXWC+2h22Oz2HpxR+U26CIiIiInHAvH2XcZMa4ydYwxBk1TUAGQOEB0he+GYbciIjCHBIARG1z/AoDXwFe850Fx5pR1b8ikchdV1PdH+1gDgAuA1oKt7HPvIBo3NK5o03Xn82tF75VRlsiIiIi8sbR2HHkUOIhVzLpumXWxva0IkfoFsOPzQbDIyL6Tm6RiwAy6fgDnLxwP9PenkI4E+ipwd6pZnn1agvn5Sz4hnirhe8yLXWlyWg9cBYwsq/25sBalGZr5Bc0zXokWmwREREDGSsyPrDraSEsufD/gP8LW2zEfzd9iJB048YZq2xzfLo15lBB/9zjHOVcU5ua11tH4E24ue59m47X2+rW7UXlFIX76AoXhoe8iupVopxoqRplm+OnZqvURs5IRETEAGb61RUCR5fbjIiIXAjdcQYVx03WKnIxsN+mGvVGW9V2JtclCq9keuKCkU6s82gVOVzgYIVPA4NyfHUb8Dyiz6HmbxbvD6Rnv1CwLRERueAmBiFVn3UwO6u1W2NMp6j9yLPyHB+0LuehRDhO9vSrK/iw5bOO0V3U2q3F0Arynqf6N9L1b4eiA8BdNJSKVRW0DVZuu3DFBmPHLaxk6NpDHJGd1OoWIqwWdd7IdGT+yh2zV4dmAwAqTJm/h+PpWIStVbVKRNpQ+6YnsedJX/hKwQ8Wxy2sZIvWbMJmp1jSc1bm/NoJ84Yx1GYj0IPa1xRyvTO1yZtRpgKgssg2152fl4DpV1eYFavPR3V+17+ssRWZHXqc27nbKtK1ufUt6/65SLVXUMmD6VdX0PLhUACGbtnCNWd3bjA+Zd7ejjp7quqOKJ2CvOeJ/pN0/F9568oFd+7OjsQORNlGYYhgV3rIC2Q6X2SI2B5f0zm8k/R55Uwp2Ozo85bNpoh6aRaDNjk1jcda0QsEjmJdNEblZKe1endv0oKJBedk3HbhCg/SZP8Dt8mB13aOwSdV2UaRLTE6LDtZW0TlIxH5IKP6Ium6V6PIR0TJmDJvb2OdH4B+FaVaURABVRTBGGBU9Upqk3dbY2dxy+zCEgQnL9jVOJkf8NGqExGGqwIiaNcv3SAqbupvqF7uNcdv7+vbEtrS0hk7lopMxp6eGMZ1ibVMS21tPL0UbZ8GssV6GwAVi6k0GWqSt1rHfI/Fs17skwGnJwablqoZSOocLJ9cH3cVWPcZGzxwk++gyWVG9MbMmLZ7SSR6vrn0gDO04zTtjF3d9edrFkbn+lqpNLdJp/kSAB2xhIXc6jKFwYkLRpqKznP4aNX5QHcHZIjpjH3Y00us/b+xwLO5iHeGtp+inbFfZv9qf9HCnvma6Hy4erxKbAkAH608N1vZW8Vxk6cpciGW/XTdYcuu79QAuKlnRbnCa667MV+dPdrhpk5VbBxkX9WPbwuKZPXFKqCz59cqrXcofC0MOyKyFMEhWYeo18zdwN06ad6OxpFTQFzgAIWDjZP5q3VTJ5Ku+0efVWU9+39lIAfvOd5ndRERuWDc5CVYLgXtLaFwBMp4vKFnFaSnNnUOmvkfNDBKKIp+DuE2cRvu0LVt00IqRR5jTdU+1KaGmoymgaBj/jGEWmPtcaamcWKmedb9BWmcNG9H0yq/R3JpOSHbIpxikSN4iF2BnB2SgYoTyxyryBXltiNnlP0Yn6iSwalbFTmml8ljVPitqW0YZ5vi0wt+uByXiJlRQ3+r6JSNNgrayLogIwqSG9EniuiQdOPW2f+2MBeYy7Qrt3U6O45RwxcdZQG1qfleU91tJbEjIqJEmNrUPFTru/2TJ/CoKo9h9H2QLVHZFfQQYBfg9kLCv8ZNxlHtXla8BdHbseZ/BfseQrWK2RvVycBuAIJMMIOrl3nu/KNIz1zTpzcKGDVTUD0T2BJoQ7gDeEws72KwCp8U5WvdemENs2KbmJjcm6Xxd/NS5jY5Dq81K6xzRjoU/b0I9wm8DaxBzVYqureofFnRz2bfMotC2xbr53iOfdKomQOAlU8h+s2uoU6EnrusD4q9UyLzNkFEPiODq28QOAZA4C8q/EGQf2FtG5gRKnwBdAowBACVM42bfNyme+uZ1jNmm6HzUZ3S9WcGIWmVX6zfEjp54XCnvf14FbkCdHTXvFWCfkdEX8uIvI5te6Mv7ztiU0rjkHTn5gve8eB6sv+Rc42SiIgBglOTOk5V14fiBP7mWe8bLJnTc0h8cnI/Yib/nCo3dRjo+idhRe9XR7/e47bPuMQlZlRVHcjlZB8rDzV4DRa+nbfejelyvBRdpvB1mnrMVZlnahq+jcjCrr+3NA7nW/h+PqpivHq0RQ7u+nOlxfki6ZlP+z4mu3N3NupMsjjX56NnQLN49nMWngOI1aaOscp6h8Q2xeeV0bIeUfiswGeBFQadlknX/6GHab9kcsPlxsj9wE7Zf5KLmX71LzfJP+mNKY2fwtpz1/0pcJrXFL9pgzk3zljlwS1Mu/JBk+l8HPgEMBykI9NUH1XnLhLhnLLpC/8lTy0R/yUkEkZFf8L6OLA86+Ed7euMACyJP1VIToVBF/FxfYnHldh43xyUhxIZm66fi3BRt389jykNB+ar14enlLavBiXO2ub6nyjcuf4fhAn5KrHdO44LN5Ke+XTgC9JzXrfN8StJz+wxdyKi32BpKfD5AAALb0lEQVQteqKPM5JlSf1LIhuUjtiRj1YckK8iY61L17oR+JuX3sgZ6c7NF7yDSKrbv3wrX30RuVN+hyQiYjPCWV51LF1bIwBW7bfyOp2RKzXJcXQ7xWbh/Fy2X6zukgJddxMX48mFYZgjKnNyaaVgkO6RirGcnhicp6YheZoWMQBQuJ10fa+9zLx9Wn8HvL/ub6POQQXoOrjb/763t/lWzX3d5h+S/282IlcihyQiIkRUZVK3v56muf6RYugxwuSP/5JnSccfy+mF6VoP5JqPX8okpl9d0UdzPvDeb1mWy0QPr3tEw2HtkC3zU6UfJ64rtUxt3Cm/10f0R4xwS04TEwkrG5wG0lF564KtP3655HCqbW33qF+MVUO29p0a0ScihyQiIlwO/vh/in/4uY9I960L7H3+MzfF4nS3q6qQsHd3FH0i563XWOWGNUs6neH56LL/397dx1ZZ3QEc//7Oc1ugJXOOMXyZLk5xDDX4shendUHFCNuEIX2KiEZd5tzCZobS4oxxN8Mx6L2gIS4xos5NR1ZaXhaYb2QbUWdEnRgVNJk4ZG5T5guILZbe5/z2x9Pbe1ts6W3vbSv9fZIm92nPec650Oae57z8fhlZQ3wSAuCzLvJbgpr0HNuL9skWRYnnelvWQ+53qCO8Q+95NLd5XPTQsx1ReefkgSN9UXICmYPZgMSYYglXB8DE7KUIL5aqKYVTOtpBet5H0dUpH7xOXo6oQIODAxgWQGBHrwtnWjofu01EhQVnXFe7W4TryB3fPVpVV7mxlTtddfoW5twxrqD7maHAc9renQWVz1IpOLiniORC3osecg9V4PT0vMvdrLppT7eFTb/YgMSYonnzCPL+pkSlNEcpw+WjgLwnO/e/gurHwcE66qjTMf3qj1Dk6Ks9i1bXPiiqM4D86fZjEV3kMm27XHXqAcL0WQPZJ9MvzYUErOsv53Vtx4XKLGbXn9h9aRUvXN9xKayzwJqlYwMSY4olaO20/JDJnxoupoTr1I4ofZhCzuubSv+SVnpa+1W/D6Kmuo3+o+bxiNaC7Mz7UTnCFQ59LghTTyTC+qkD3TdTsL6nEemDTFPtJuD59suRzvMY1fXnHVRw5uIxLlx2n8AF7d/Z5zP+toHq53Bk667GFI3rfMpEpL+bRT+e7D+Q/6er4vuw619ydfo0oMm/1SA9MW5ItnhIk0wuD7ZXTPUq8wSm0v6gpVClyMMuTN/vPyz/IQ9fP2ADJwcJe4weqkR98KtZLgqeBhkH8kUnPE6Yek1hm4gcENVjFM6Cjj0mLU641K9d+Oagdv0wZzMkxhRL8/5Oa8tBf2ceuvOf1n3kZ9joWztHdrxy/v3+d2oQJZM+Wl33kDbWftt7nQDcCeQNDvVqN7p15UB2SaGgzbpmgP3hZzt9EJ3dKS4OnCQwA9VQ4VziZVFVeNTjz8ysri1o87gpnA1IjCmWODdMx4e7ajShJO3EJ1o6jr8qfmIPpQ82M/U58o4+inev91D6k2VN3T98Y+1PvPOnCzyb95MrmbWsX6eJCqNHD1xbpk8ibQays5jvILoM9G6Q+1G9XeD73ul4baydahnhB4Yt2RhTRArPZHNyxPk3SkWeAj0ZQEQKaicok3PyMptqJG5LsXs36BoWvhLNXDzNJcpeJA77jRM/1cPWAu6SH2iuottSXc25YxyZNjvtM5RdkqxwlG0CJgFv+MhXYcsxg85mSIwpIkE3517LxYSLCw7c1Kt2hD9mXyucR7jk+N7W9apXdtwHnjtsw6qvu/ldNC8WjGhhgwTR/CW4MUxf2quYF0Fb5pKC2ikl1fw8LyOZtmLEoPVlCHGjKq4hHowgKrfZYGRosAGJMUXk4X5y+xdGOC37ZSnaiXZ/uBHY1X7pnLolvaoYpr4hMLPjWvXO4vduKNHy3GtX0MArEt2ed+mCEcHFh6wUJstV+Gkh7ZRSxrv8/UGOigMnDFpnhhLvcvFHnA5apmPTmQ1IjCmmOLncvR3XwrWuJrUQtOcATnPShYWj3pzMCHmJ8kTmuOr0LT3WmZWe4KCJjsR/vBxJS+9Cdg8V1anJhEu/1Kuys+tPRJiRvXTokwW11VC3A+hIMa/oIuau6H6z6uRkwknlXaCndFtmoDl5lbwN0E78lT2UHkY0F4dHWZSoSU0hrD+KMFneUy1TWraHxJgi81TUOVqmAOMBUJYEYfo7SPqeSKJncCPew7eNDSI5TmGiiE7XjJ7jcxvseiVqrF3lqlPTEK4AQHSRhPWTHcGKqM09zvr5ewiT5fjKCc5RAzqf3F6I/d4zlzWHTog3lDhhCribJUz9TVXWe9GnGMmLPFAbH12+OjmS/aNOcD4I8fpjILvM8nyGLxwyedtBVO9EZGn71QR3oHWL1KR+Hh1IPMb6+XtAhXDpcQHuAoUbUE4j3nuyAzi1/++4nxpv2K9hapPAtwAQWehq6t/3bZnfsO7mdwG4NP0FVPezrnb3YHZ1IPlAVzovPyI+DTXJK5scAlRCmOpavBl4DZHHvfr7aKx7YaD7O1zYgMSYYmuc96EPl0xxJB7KPi0rVKFa5dSBjx9YtT18R/v20j6FrfCf+dT33J69gspcAEEuVPyFrsxDmGoFRnzMPOh7HpnBmgUlC21fYqJQhWiVgzisVphqATI0Z4/bdvrn/LfHXx4nFiyMl5Y7Aiqntx8DBZigSoMry0CY+gjSIyDID8TiRfUqFTkZGBJBtNRJUrxeRDzgDVBJuURZijC1FxgNGohodQRrBrmrA6ehbofU1M9RlSbgUBmkK4FJqE5yyDxq6m/3E1vqBjK67HBhSzbGlELjTbs8mXMRqafzaY3uvNandu6+rs2vrrtCVK8it6ckq+sGRo/yoCc6g8YFhS1fDBFO5AnQjzumXMHBsT8ilAd94L/W52ObjckD0UguRlkJdE0gOJLc8hcg2zxURU11jQLbGSoaFjwrKpcBH3T5yRFAMAg9GlzhkuMlTDWqygZyg5E24I34dyv7xVtwUBRih8qNbnvlrQPZ5eHCZkiMKZXGm/Z6WEi4fGlA5iJFqhSOJf4g8AK7UXnFI3/h1H1P09j3pqKmut8xObkqMbZiihd3vqp+GbQCpEVE3xbvtkQ+eqRYpwmcyoo4rwdEroCjtJUH9klz+XUd163l/y2k3czqBY+STI7npVFnOue+iXCyKscQf7A4YJ/Av0R0a9QmG4uyDPFAbbOHH3DZskXOR9NR9xVFx4GWC+4dVHd6pxtYveCpbJ6TiLYnA+L3GQl/70uzzrt7VPSvAJHz2/rzFqKmBWsJl292RJcjerYqR4G0ispbImyJ2to29/pe+CcDgvb/Q7+3T/0R3Rog8T3UFxQ6PlC9S8U9EvdFC0ssGaYnuvgk3Fggg0jaq1sJn/9ntzNo4a9HJ/Sjr6v4XyicE/eZhcxdsZzfX991kGf6oeBMicYYY8wnTjLp3LaKF0BOAxCVa6OmBff0uv53b/+0K8vson1fkqheEjXVbSxNZ4cnW7Ixxhhz2Etsq5ycHYyAvh413XhvjxW6Wj9/j5I32+XEovEWmQ1IjDHGHPY8nJ59rcjW7PJaIVz+STjfKXCeKQIbkBhjjDn8KfmnYgqPNxIuHhtnAI5FgbxSjG6ZHBuQGGOMOew50VezrwXOJ6w/qteVw+WjhLLfEp+sAvQlGmpfLnYfhzsbkBhjjDnsZY484s/kjsaPdsijzK4/s6c6TFsxIqhOX+qInhGY1v7dyCvzStnX4cpO2RhjjBkWEjVLz/fq/kTnYGgvo/oSTt7GswcYhdMxqnKSwFeJA6Nl7Rf0mqixrmFAOz5M2IDEGGPM8DF72dnO+7toz/bbS15hrTp/Kw0Lbe9IidiAxBhjzPCSTLrE9soLvDJD4AyN806NJp452QM0C+xSlVfF6dOR6ob2xJmmhP4P/7vBh2oYZo8AAAAASUVORK5CYII=");

    @Parameterized.Parameters(name = "InvoiceDocumentTemplate: {0}")
    public static List<String> data() {
        return DocumentTemplateRepository.ALL.stream().map(DocumentTemplate::getExternalIdentifier).collect(Collectors.toList());
    }

    private final DocumentTemplate sample;

    public QrInvoiceDocumentCreatorTest(final String externalIdentifier) {
        this.sample = DocumentTemplateRepository.getByExternalIdentifier(externalIdentifier)
                .orElseThrow(() -> new RuntimeException("DocumentTemplate externalIdentifier=" + externalIdentifier));
    }

    @Test
    public void verifyData() {
        final InvoiceDocument data = createData();
        assertEquals("QR-Invoice Java Solutions", data.getTitle());
    }

    @Test
    public void createOutputTest() throws Exception {
        final InvoiceDocument invoice = createData();
        final DocumentLayout invoiceLayout = DocumentLayoutBuilder
                .create()
                .documentTemplate(sample)
                .logo(LOGO)
                .build();

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            final QrInvoiceDocument document = QrInvoiceDocumentCreator.create()
                    .invoice(invoice)
                    .invoiceLayout(invoiceLayout)
                    .inGerman()
                    .withoutBoundaryLines()
                    .createQrInvoiceDocument(out);

            assertTrue(out.size() > 20_000);
            assertEquals(2, document.getPageCount());
        }
    }

    @Test
    public void createQrInvoiceOutputTest() throws Exception {
        final InvoiceDocument invoice = createData();
        final DocumentLayout invoiceLayout = DocumentLayoutBuilder
                .create()
                .documentTemplate(sample)
                .logo(LOGO)
                .build();

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            final QrInvoiceDocument document = QrInvoiceDocumentCreator.create()
                    .invoice(invoice)
                    .invoiceLayout(invoiceLayout)
                    .inGerman()
                    .withoutBoundaryLines()
                    .createQrInvoiceDocument(out);

            assertTrue(out.size() > 20_000);
            assertEquals(2, document.getPageCount());
        }
    }

    private InvoiceDocument createData() {
        final InvoiceDocumentBuilder invoiceBuilder = InvoiceDocumentBuilder.create()
                .invoiceNr("re-00013")
                .title("QR-Invoice Java Solutions")
                .customerNr("123")
                .customerReference("Kundenreferenz")
                .invoiceDate(LocalDate.now())
                .invoiceDueDate(LocalDate.now().plusDays(30))
                .termOfPaymentDays(30)
                .contactPerson(c -> c
                        .name("Claude Gex")
                        .email("claude.gex@codeblock.ch")
                )
                .currency(Currency.getInstance("CHF"))
                .prefaceText(t -> t
                        .append("Sehr geehrte Damen und Herren")
                        .lineBreak()
                        .append("Wir danken Ihnen für Ihren Auftrag und stellen Ihnen wie folgt in Rechnung")
                )
                .endingText(t -> t
                        .append("Für Fragen stehen wir Ihnen jederzeit gerne zur Verfügung.")
                        .lineBreaks(2)
                        .append("Freundliche Grüsse")
                        .lineBreaks(3)
                        .append("Hans Muster AG")
                )
                .sender(s -> s
                        .addAddressLine("Hans Muster AG")
                        .addAddressLine("Hauptstrasse 1")
                        .addAddressLine("3232 Ins")
                        .addAddressLine("Schweiz")
                )
                .recipient(r -> r
                        .addAddressLine("Kunde AG")
                        .addAddressLine("Am Weg 42")
                        .addAddressLine("3000 Bern")
                        .addAddressLine("Schweiz")
                )
                .addPosition(p -> p
                        .position(1)
                        .description(t -> t
                                .append("This is simple but long string with lorem ipsum text")
                                .lineBreaks(2)
                                .append("generating some sort of natural text in order to test text wrapping")
                        )
                        .quantity(12)
                        .unitPriceVatExclusive(10.0)
                        .vatPercentage(7.7)
                )
                .addPosition(p -> p
                        .position(2)
                        .description("short text")
                        .quantity(3.2)
                        .unitPriceVatExclusive(42.1)
                        .vatPercentage(7.7)
                )
                .addPosition(p -> p
                        .position(2)
                        .description("Mineral")
                        .unit("Stk")
                        .quantity(44)
                        .unitPriceVatExclusive(1.0)
                        .vatPercentage(2.5)
                )
                .addPosition(p -> p
                        .position(3)
                        .description(t -> t
                                .append("This is simple but long string with lorem ipsum text")
                                .lineBreaks(2)
                                .append("generating some sort of natural text in order to test text wrapping")
                        )
                        .quantity(10)
                        .unitPriceVatInclusive(10.0)
                        .vatPercentage(7.7)
                )
                .addPosition(p -> p
                        .description("This is a text only position")
                )
                .addPosition(p -> p
                        .position(4)
                        .description(t -> t
                                .append("This is simple but long string with lorem ipsum text")
                                .lineBreaks(2)
                                .append("generating some sort of natural text in order to test text wrapping")
                        )
                        .quantity(42)
                        .unitPriceVatInclusive(9.0)
                        .vatPercentage(7.7)
                        .discountPercentage(10.0)
                );

        final InvoiceDocument invoiceDocument = invoiceBuilder.build().calculate();

        final QrInvoice qrInvoice = invoiceDocument.applyCurrencyAmount(QrInvoiceBuilder.create())
                .creditorIBAN("CH44 3199 9123 0008 8901 2")
                .creditor(c -> c
                        .structuredAddress()
                        .name("Hans Muster AG")
                        .streetName("Hauptstrasse")
                        .houseNumber("1")
                        .postalCode("3232")
                        .city("Ins")
                        .country("CH")
                )
                .ultimateDebtor(d -> d
                        .combinedAddress()
                        .name("Kunde AG")
                        .addressLine1("Am Weg 42")
                        .addressLine2("3000 Bern")
                        .country("CH")
                )
                .paymentReference(r -> r
                        .qrReference("210000000003139471430009017")
                )
                .additionalInformation(a -> a
                        .unstructuredMessage("Invoice Document Example")
                )
                .build();
        invoiceDocument.setQrInvoice(qrInvoice);

        return invoiceDocument;
    }
}
