/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import ch.codeblock.qrinvoice.documents.model.application.builder.AmountBuilder;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

public class PositionTest {

    @Test
    public void testPositionCalculationInclusiveBased() {
        final Position position = new Position();

        position.setQuantity(23);
        position.setUnitPrice(AmountBuilder.create().vatInclusive(10).vatPercentage(Percentage.of("7.7%")).build());

        position.calculate();
        assertEquals(0, BigDecimal.valueOf(230.00).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(213.56).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(16.44).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));

        // verify re-calculate behaves idempotent
        position.calculate();
        assertEquals(0, BigDecimal.valueOf(230.00).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(213.56).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(16.44).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));

    }

    @Test
    public void testPositionCalculationExclusiveBased() {
        final Position position = new Position();

        position.setQuantity(23);
        position.setUnitPrice(AmountBuilder.create().vatExclusive(10).vatPercentage(Percentage.of("7.7%")).build());

        position.calculate();
        assertEquals(0, BigDecimal.valueOf(247.71).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(230.00).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(17.71).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));

        // verify re-calculate behaves idempotent
        position.calculate();
        assertEquals(0, BigDecimal.valueOf(247.71).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(230.00).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(17.71).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    public void testPositionCalculationInclusiveBasedWithDiscount() {
        final Position position = new Position();

        position.setQuantity(23);
        position.setUnitPrice(AmountBuilder.create().vatInclusive(10).vatPercentage(Percentage.of("7.7%")).build());
        position.setDiscountPercentage(Percentage.of("10%"));

        position.calculate();
        assertEquals(0, BigDecimal.valueOf(207.00).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(192.20).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(14.80).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));

        // verify re-calculate behaves idempotent
        position.calculate();
        assertEquals(0, BigDecimal.valueOf(207.00).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(192.20).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(14.80).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    public void testPositionCalculationExclusiveBasedWithDiscount() {
        final Position position = new Position();

        position.setQuantity(23);
        position.setUnitPrice(AmountBuilder.create().vatExclusive(10).vatPercentage(Percentage.of("7.7%")).build());
        position.setDiscountPercentage(Percentage.of("10%"));

        position.calculate();
        assertEquals(0, BigDecimal.valueOf(222.94).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(207.00).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(15.94).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));

        // verify re-calculate behaves idempotent
        position.calculate();
        assertEquals(0, BigDecimal.valueOf(222.94).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(207.00).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(15.94).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    public void testDiscountAbsoluteVatExclusive() {
        final Position position = new Position();
        position.setUnitPrice(AmountBuilder.create().vatPercentage(Percentage.of("7.7%")).vatExclusive(50.0).build());
        position.setDiscountAbsolute(AmountBuilder.create().vatPercentage(Percentage.of("7.7%")).vatExclusive(10.0).build());
        position.setQuantity(1);
        position.calculate();

        assertEquals(0, position.getTotal().getVatExclusive().compareTo(BigDecimal.valueOf(40)));
    }

    @Test
    public void testDiscountAbsoluteVatInclusive() {
        final Position position = new Position();
        position.setUnitPrice(AmountBuilder.create().vatPercentage(Percentage.of("7.7%")).vatInclusive(50.0).build());
        position.setDiscountAbsolute(AmountBuilder.create().vatPercentage(Percentage.of("7.7%")).vatInclusive(10.0).build());
        position.setQuantity(1);
        position.calculate();

        assertEquals(0, position.getTotal().getVatInclusive().compareTo(BigDecimal.valueOf(40)));
    }

    @Test
    public void testDiscountPercentageVatExclusive() {
        final Position position = new Position();
        position.setUnitPrice(AmountBuilder.create().vatPercentage(Percentage.of("7.7%")).vatExclusive(50.0).build());
        position.setDiscountPercentage(Percentage.of("10%"));
        position.setQuantity(1);
        position.calculate();

        assertEquals(0, position.getTotal().getVatExclusive().compareTo(BigDecimal.valueOf(45)));
    }

    @Test
    public void testDiscountPercentageVatInclusive() {
        final Position position = new Position();
        position.setUnitPrice(AmountBuilder.create().vatPercentage(Percentage.of("7.7%")).vatInclusive(50.0).build());
        position.setDiscountPercentage(Percentage.of("10%"));
        position.setQuantity(1);
        position.calculate();

        assertEquals(0, position.getTotal().getVatInclusive().compareTo(BigDecimal.valueOf(45)));
    }

    @Test
    public void testHasDiscount() {
        final Position positionNullDiscount = new Position();

        final Position positionZeroPercentDiscount = new Position();
        positionZeroPercentDiscount.setDiscountPercentage(Percentage.ofNumericalPercentage(0));
        final Position positionPercentDiscount = new Position();
        positionPercentDiscount.setDiscountPercentage(Percentage.ofNumericalPercentage(10));

        final Position positionZeroAbsoluteDiscount = new Position();
        positionZeroAbsoluteDiscount.setDiscountAbsolute(AmountBuilder.create().vatInclusive(0).vatPercentage(Percentage.ofNumericalPercentage(7.7)).build());
        final Position positionAbsoluteDiscount = new Position();
        positionAbsoluteDiscount.setDiscountAbsolute(AmountBuilder.create().vatInclusive(100).vatPercentage(Percentage.ofNumericalPercentage(7.7)).build());

        assertFalse(positionNullDiscount.hasDiscount());

        assertFalse(positionZeroPercentDiscount.hasDiscount());
        assertTrue(positionPercentDiscount.hasDiscount());

        assertFalse(positionZeroAbsoluteDiscount.hasDiscount());
        assertTrue(positionAbsoluteDiscount.hasDiscount());
    }
}
