/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import ch.codeblock.qrinvoice.model.validation.ValidationException;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class AmountTest {
    @Test
    public void testIsEmpty() {
        final Amount amount = new Amount();
        assertTrue(amount.isEmpty());
        amount.setVatExclusive(10.00);
        assertFalse(amount.isEmpty());
    }

    @Test(expected = ValidationException.class)
    public void testMissingVat() {
        final Amount amount = new Amount();
        amount.setVatExclusive(10.00);
        amount.calculate();
    }

    @Test
    public void testVatExclusiveBased() {
        final Amount amount = new Amount();
        amount.setVatExclusive(10.00);
        amount.setVatPercentage(Percentage.of("7.7%"));
        amount.calculate();

        validateAmountDefaultValues(amount);

        amount.calculate();
        validateAmountDefaultValues(amount);
    }

    @Test
    public void testVatInclusiveBased() {
        final Amount amount = new Amount();
        amount.setVatInclusive(10.77);
        amount.setVatPercentage(Percentage.of("7.7%"));
        amount.calculate();

        validateAmountDefaultValues(amount);

        amount.calculate();
        validateAmountDefaultValues(amount);
    }

    @Test
    public void testPreferVatExclusive1() {
        final Amount amount = new Amount();
        amount.setVatInclusive(10.77);
        amount.setVatPercentage(Percentage.of("7.7%"));
        amount.calculate();

        assertTrue(amount.preferVatExclusive());
    }

    @Test
    public void testPreferVatExclusive2() {
        final Amount amount = new Amount();
        amount.setVatInclusive(10.0);
        amount.setVatPercentage(Percentage.of("7.7%"));
        amount.calculate();

        assertFalse(amount.preferVatExclusive());
    }

    @Test
    public void testPreferVatExclusive3() {
        final Amount amount = new Amount();
        amount.setVatExclusive(10.0);
        amount.setVatPercentage(Percentage.of("7.7%"));
        amount.calculate();

        assertTrue(amount.preferVatExclusive());
    }

    public void validateAmountDefaultValues(Amount amount) {
        assertEquals(0, BigDecimal.valueOf(10).compareTo(amount.getVatExclusive()));
        assertEquals(0, BigDecimal.valueOf(10.77).compareTo(amount.getVatInclusive()));
        assertEquals(0, BigDecimal.valueOf(.77).compareTo(amount.getVatAmount()));
        assertEquals(Percentage.of("7.7%"), amount.getVatPercentage());
    }

    @Test
    public void testIsZero() {
        final Amount emptyAmount = new Amount();
        final Amount amountZeroIncludingVat = new Amount();
        amountZeroIncludingVat.setVatInclusive(BigDecimal.ZERO);
        final Amount amountZeroExclusingVat = new Amount();
        amountZeroExclusingVat.setVatExclusive(BigDecimal.ZERO);
        final Amount amountAllZero = new Amount();
        amountAllZero.setVatInclusive(BigDecimal.ZERO);
        amountAllZero.setVatExclusive(BigDecimal.ZERO);

        assertFalse(emptyAmount.isZero());
        assertTrue(amountZeroIncludingVat.isZero());
        assertTrue(amountZeroExclusingVat.isZero());
        assertTrue(amountAllZero.isZero());
    }

}
