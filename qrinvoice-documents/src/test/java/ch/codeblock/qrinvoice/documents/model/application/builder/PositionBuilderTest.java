/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application.builder;

import ch.codeblock.qrinvoice.documents.model.application.Position;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;

public class PositionBuilderTest {
    @Test
    public void testVatInclusive() {
        final Position position = PositionBuilder.create().position(1).quantity(23).unitPriceVatInclusive(10.0).vatPercentage("7.7%").build();
        position.calculate();

        assertEquals(0, BigDecimal.valueOf(230.00).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(213.56).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(16.44).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));
    }

    @Test
    public void testVatExclusive() {
        final Position position = PositionBuilder.create().position(1).quantity(23).unitPriceVatExclusive(10.0).vatPercentage("7.7%").build();
        position.calculate();

        assertEquals(0, BigDecimal.valueOf(247.71).compareTo(position.getTotal().getVatInclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(230.00).compareTo(position.getTotal().getVatExclusive().setScale(2, RoundingMode.HALF_UP)));
        assertEquals(0, BigDecimal.valueOf(17.71).compareTo(position.getTotal().getVatAmount().setScale(2, RoundingMode.HALF_UP)));
    }

    @Test(expected = ValidationException.class)
    public void testVatIclusiveExclusiveValidationError() {
        PositionBuilder.create().position(1).quantity(23).unitPriceVatInclusive(10.0).unitPriceVatExclusive(10.0).vatPercentage("7.7%").build();
    }

    @Test(expected = ValidationException.class)
    public void testMissingVatValidationError() {
        PositionBuilder.create().position(1).quantity(23).unitPriceVatInclusive(10.0).build();
    }

}
