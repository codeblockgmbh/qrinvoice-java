/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import ch.codeblock.qrinvoice.model.annotation.Description;
import ch.codeblock.qrinvoice.model.annotation.Example;
import ch.codeblock.qrinvoice.model.validation.ValidationException;

import java.math.BigDecimal;

import static ch.codeblock.qrinvoice.documents.utils.BigDecimalUtils.stripTrailingZeros;

public class Amount {
    private BigDecimal vatExclusive;
    private BigDecimal vatInclusive;
    private BigDecimal vatAmount;
    private Percentage vatPercentage;

    /**
     * Currently only used for gross total
     */
    private BigDecimal vatInclusiveRoundingDifference;

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Amount that does not include VAT (net). Either provide vatExclusive or vatInclusive")
    @Example("10")
    public BigDecimal getVatExclusive() {
        return vatExclusive;
    }

    public void setVatExclusive(Double vatExclusive) {
        setVatExclusive(BigDecimal.valueOf(vatExclusive));
    }

    public void setVatExclusive(BigDecimal vatExclusive) {
        this.vatExclusive = vatExclusive;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Amount that does include VAT (gross). Either provide vatExclusive or vatInclusive")
    @Example("10.77")
    public BigDecimal getVatInclusive() {
        return vatInclusive;
    }

    public void setVatInclusive(Double vatInclusive) {
        setVatInclusive(BigDecimal.valueOf(vatInclusive));
    }

    public void setVatInclusive(BigDecimal vatInclusive) {
        this.vatInclusive = vatInclusive;
    }

    public BigDecimal getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("VAT percentage")
    @Example("7.7")
    public Percentage getVatPercentage() {
        return vatPercentage;
    }

    public void setVatPercentage(Percentage vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    public BigDecimal getVatInclusiveRoundingDifference() {
        return vatInclusiveRoundingDifference;
    }

    public void setVatInclusiveRoundingDifference(BigDecimal vatInclusiveRoundingDifference) {
        this.vatInclusiveRoundingDifference = vatInclusiveRoundingDifference;
    }

    public boolean preferVatExclusive() {
        if (vatExclusive == null) {
            return false;
        } else if (vatInclusive == null) {
            return true;
        } else {
            return stripTrailingZeros(vatExclusive).scale() < stripTrailingZeros(vatInclusive).scale();
        }
    }

    void calculate() {
        if (isEmpty()) {
            return;
        }

        if (vatPercentage == null) {
            throw new ValidationException("VAT percentage has to be set, even if value is 0%");
        }

        if (vatExclusive == null) {
            vatExclusive = vatPercentage.subtractAddedPercentageFromValue(vatInclusive);
        }

        if (vatInclusive == null) {
            vatInclusive = vatPercentage.addPercentageToValue(vatExclusive);
        }

        vatAmount = stripTrailingZeros(vatInclusive.subtract(vatExclusive));
    }

    public boolean isEmpty() {
        return vatExclusive == null
                && vatInclusive == null
                && vatAmount == null
                && vatPercentage == null;
    }

    public boolean isZero() {
        return (vatExclusive != null && vatExclusive.compareTo(BigDecimal.ZERO) == 0) ||
                (vatInclusive != null && vatInclusive.compareTo(BigDecimal.ZERO) == 0);
    }

    public static boolean isEmpty(Amount amount) {
        return amount == null || amount.isEmpty();
    }

    public static Amount createIfMissing(Amount amt) {
        if (amt != null) {
            return amt;
        }
        return new Amount();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Amount{");
        sb.append("vatExclusive=").append(vatExclusive);
        sb.append(", vatInclusive=").append(vatInclusive);
        sb.append(", vatAmount=").append(vatAmount);
        sb.append(", vatPercentage=").append(vatPercentage);
        sb.append('}');
        return sb.toString();
    }
}
