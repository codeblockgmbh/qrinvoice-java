/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents;

import ch.codeblock.qrinvoice.config.SystemProperties;
import ch.codeblock.qrinvoice.documents.model.DocumentTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class DocumentTemplateRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentTemplateRepository.class);
    private static final String TEMPLATE_FILE_EXTENSION = ".xsl";
    public static final Set<DocumentTemplate> ALL = new HashSet<>();
    private static final String DEFAULT_TEMPLATE_DIRECTORY = "/xsl/invoice/shared";

    public static DocumentTemplate STANDARD_RECIPIENT_RIGHT_V1;
    public static DocumentTemplate STANDARD_RECIPIENT_LEFT_V1;

    static {
        loadClasspathTemplates();
        loadExternalTemplates();
    }

    private static void loadClasspathTemplates() {
        try {
            STANDARD_RECIPIENT_RIGHT_V1 = new DocumentTemplate(DEFAULT_TEMPLATE_DIRECTORY + "/standard_recipient_right_v1.xsl");
            STANDARD_RECIPIENT_LEFT_V1 = new DocumentTemplate(DEFAULT_TEMPLATE_DIRECTORY + "/standard_recipient_left_v1.xsl");
            registerTemplate(STANDARD_RECIPIENT_RIGHT_V1);
            registerTemplate(STANDARD_RECIPIENT_LEFT_V1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void registerTemplate(DocumentTemplate template) {
        ALL.add(template);
        LOGGER.info("DocumentTemplate {} was registered", template.getExternalIdentifier());
    }

    public static void registerTemplates(Collection<DocumentTemplate> templates) {
        templates.forEach(DocumentTemplateRepository::registerTemplate);
    }

    private static void loadExternalTemplates() {
        String documentTemplatesPath = System.getProperty(SystemProperties.DOCUMENT_TEMPLATES);
        if (documentTemplatesPath != null) {
            List<DocumentTemplate> externalTemplates = importTemplates(documentTemplatesPath);
            registerTemplates(externalTemplates);
        }
    }

    private static List<DocumentTemplate> importTemplates(String path) {
        File templatesDirectory = new File(path);
        File[] files = templatesDirectory
                .listFiles((dir, name) -> name.toLowerCase().endsWith(TEMPLATE_FILE_EXTENSION));

        if (files == null) {
            return Collections.emptyList();
        }

        return asTemplates(Arrays.asList(files));
    }

    private static List<DocumentTemplate> asTemplates(List<File> files) {
        return files.stream()
                .map(DocumentTemplate::new)
                .collect(Collectors.toList());
    }

    public static Optional<DocumentTemplate> getByExternalIdentifier(String externalIdentifier) {
        for (DocumentTemplate documentTemplate : ALL) {
            if (documentTemplate.getExternalIdentifier().equals(externalIdentifier)) {
                return Optional.of(documentTemplate);
            }
        }
        return Optional.empty();
    }
}
