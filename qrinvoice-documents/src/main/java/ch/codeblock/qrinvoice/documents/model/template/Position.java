/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.template;

import javax.xml.bind.annotation.XmlElement;
import java.util.Map;

public class Position {
    @XmlElement(name="position")
    private String position;
    @XmlElement(name="description")
    private String description;

    @XmlElement(name="quantity")
    private Double quantity;

    @XmlElement(name="unit-price-vat-exclusive")
    private Double unitPriceVatExclusive;

    @XmlElement(name="unit-price-vat-inclusive")
    private Double unitPriceVatInclusive;

    @XmlElement(name="unit")
    private String unit;
    
    @XmlElement(name="vat-amount")
    private Double vatAmount;

    @XmlElement(name="vat-percentage")
    private Double vatPercentage;

    @XmlElement(name="discount-absolute-vat-exclusive")
    private Double discountAbsoluteVatExclusive;

    @XmlElement(name="discount-absolute-vat-inclusive")
    private Double discountAbsoluteVatInclusive;

    @XmlElement(name="discount-percentage")
    private Double discountPercentage;

    @XmlElement(name="amount-vat-exclusive")
    private Double amountVatExclusive;

    @XmlElement(name="amount-vat-inclusive")
    private Double amountVatInclusive;

    @XmlElement(name = "additional-properties")
    private Map<String, String> additionalProperties;

    public String getPosition() {
        return position;
    }

    public void setPosition(final String position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(final Double quantity) {
        this.quantity = quantity;
    }

    public Double getUnitPriceVatExclusive() {
        return unitPriceVatExclusive;
    }

    public void setUnitPriceVatExclusive(final Double unitPriceVatExclusive) {
        this.unitPriceVatExclusive = unitPriceVatExclusive;
    }

    public Double getUnitPriceVatInclusive() {
        return unitPriceVatInclusive;
    }

    public void setUnitPriceVatInclusive(final Double unitPriceVatInclusive) {
        this.unitPriceVatInclusive = unitPriceVatInclusive;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }
    public Double getVatAmount() {
        return vatAmount;
    }

    public void setVatAmount(final Double vatAmount) {
        this.vatAmount = vatAmount;
    }

    public Double getVatPercentage() {
        return vatPercentage;
    }

    public void setVatPercentage(final Double vatPercentage) {
        this.vatPercentage = vatPercentage;
    }

    public Double getDiscountAbsoluteVatExclusive() {
        return discountAbsoluteVatExclusive;
    }

    public void setDiscountAbsoluteVatExclusive(Double discountAbsoluteVatExclusive) {
        this.discountAbsoluteVatExclusive = discountAbsoluteVatExclusive;
    }

    public Double getDiscountAbsoluteVatInclusive() {
        return discountAbsoluteVatInclusive;
    }

    public void setDiscountAbsoluteVatInclusive(Double discountAbsoluteVatInclusive) {
        this.discountAbsoluteVatInclusive = discountAbsoluteVatInclusive;
    }

    public Double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public Double getAmountVatExclusive() {
        return amountVatExclusive;
    }

    public void setAmountVatExclusive(final Double amountVatExclusive) {
        this.amountVatExclusive = amountVatExclusive;
    }

    public Double getAmountVatInclusive() {
        return amountVatInclusive;
    }

    public void setAmountVatInclusive(final Double amountVatInclusive) {
        this.amountVatInclusive = amountVatInclusive;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
