/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import ch.codeblock.qrinvoice.model.annotation.Description;
import ch.codeblock.qrinvoice.model.annotation.Example;
import ch.codeblock.qrinvoice.model.validation.ValidationException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class Position {
    private String position;
    private String description;
    private Double quantity;
    private Amount unitPrice;
    private String unit;
    private Amount discountAbsolute;
    private Percentage discountPercentage;
    private Amount total;

    private List<AdditionalProperty> additionalProperties;

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Position Nr.")
    @Example("1")
    public String getPosition() {
        return position;
    }

    public void setPosition(final String position) {
        this.position = position;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("The positions description")
    @Example("QR Invoice Cloud REST Services - Basic 100 / Monthly fee")
    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("How many units (or fractions) of the current position")
    @Example("1")
    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(final double quantity) {
        this.setQuantity((Double) quantity);
    }

    public void setQuantity(final Double quantity) {
        this.quantity = quantity;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("How much a single unit costs")
    public Amount getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Amount unitPrice) {
        this.unitPrice = unitPrice;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("An optional unit specification, e.g. pcs / hrs")
    @Example("pcs")
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Total amount of a position, basicall quantity * unitPrice")
    public Amount getTotal() {
        return total;
    }

    public void setTotal(Amount total) {
        this.total = total;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("An absolute discount value")
    public Amount getDiscountAbsolute() {
        return discountAbsolute;
    }

    public void setDiscountAbsolute(Amount discountAbsolute) {
        this.discountAbsolute = discountAbsolute;
    }

    public void setDiscountPercentage(Percentage discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("A discount as percentage")
    @Example("10")
    public Percentage getDiscountPercentage() {
        return discountPercentage;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Additional properties that may be used in custom templates")
    public List<AdditionalProperty> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(List<AdditionalProperty> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public void calculate() {
        validate();
        calculateUnitPrices();
        calculateTotal();
    }

    private void calculateTotal() {
        if (Amount.isEmpty(unitPrice)) {
            return;
        }

        this.total = Amount.createIfMissing(this.total);

        if (!Amount.isEmpty(discountAbsolute)) {
            discountAbsolute.calculate();
        }

        if (unitPrice.getVatExclusive().scale() < unitPrice.getVatInclusive().scale()) {
            BigDecimal positionTotalVatExclusive = unitPrice.getVatExclusive().multiply(BigDecimal.valueOf(quantity)).setScale(5, RoundingMode.HALF_UP);
            if (discountPercentage != null) {
                positionTotalVatExclusive = discountPercentage.subtractPercentageFromValue(positionTotalVatExclusive);
            }
            if (!Amount.isEmpty(discountAbsolute)) {
                positionTotalVatExclusive = positionTotalVatExclusive.subtract(discountAbsolute.getVatExclusive());
            }
            this.total.setVatExclusive(positionTotalVatExclusive);
        } else {
            BigDecimal positionTotalVatInclusive = unitPrice.getVatInclusive().multiply(BigDecimal.valueOf(quantity)).setScale(5, RoundingMode.HALF_UP);
            if (discountPercentage != null) {
                positionTotalVatInclusive = discountPercentage.subtractPercentageFromValue(positionTotalVatInclusive);
            }
            if (!Amount.isEmpty(discountAbsolute)) {
                positionTotalVatInclusive = positionTotalVatInclusive.subtract(discountAbsolute.getVatInclusive());
            }
            this.total.setVatInclusive(positionTotalVatInclusive);
        }
        this.total.setVatPercentage(unitPrice.getVatPercentage());
        this.total.calculate();
    }

    private void calculateUnitPrices() {
        if (unitPrice == null) {
            return;
        }
        unitPrice.calculate();
    }

    private void validate() {
        if (quantity == null
                && Amount.isEmpty(unitPrice)
                && unitPrice == null
                && total == null
                && discountPercentage == null
                && discountAbsolute == null
        ) {
            // all empty is okay for text only position
            return;
        }

        if (quantity == null) {
            throw new ValidationException("Quantity must not be null");
        }
        if (quantity < 0) {
            throw new ValidationException("Quantity must not be negative");
        }

        if (Amount.isEmpty(unitPrice)) {
            throw new ValidationException("Unit price must be set");
        }
        if (unitPrice.getVatPercentage().isNegative() || unitPrice.getVatPercentage().isGreaterThan100Percent()) {
            throw new ValidationException("Unit price VAT percentage has to be in range of 0-100%");
        }

        if ((discountPercentage != null && !discountPercentage.equals(Percentage.ofNumericalPercentage(0)))
                && (!Amount.isEmpty(discountAbsolute) && !discountAbsolute.isZero())) {
            throw new ValidationException("Cannot have percentage and absolute discount");
        }

        if (discountPercentage != null && (discountPercentage.isNegative() || discountPercentage.isGreaterThan100Percent())) {
            throw new ValidationException("Discount percentage must be in range of 0-100% if set");
        }

        if (discountAbsolute != null) {
            if ((discountAbsolute.getVatExclusive() != null && discountAbsolute.getVatExclusive().signum() < 0)
                    || (discountAbsolute.getVatInclusive() != null && discountAbsolute.getVatInclusive().signum() < 0)) {
                throw new ValidationException("Absolute discount must not be negative");
            }
        }
    }

    public boolean hasAmount() {
        return !Amount.isEmpty(total);
    }

    public boolean hasDiscount() {
        return (discountAbsolute != null && !discountAbsolute.isZero()) ||
                (discountPercentage != null && discountPercentage.getNumericalValue().compareTo(BigDecimal.ZERO) != 0);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Position{");
        sb.append("position='").append(position).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", quantity=").append(quantity);
        sb.append(", unitPrice=").append(unitPrice);
        sb.append(", unit='").append(unit).append('\'');
        sb.append(", discountAbsolute=").append(discountAbsolute);
        sb.append(", discountPercentage=").append(discountPercentage);
        sb.append(", total=").append(total);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }
}
