/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application.builder;

import ch.codeblock.qrinvoice.documents.model.application.AdditionalProperty;
import ch.codeblock.qrinvoice.documents.model.application.Amount;
import ch.codeblock.qrinvoice.documents.model.application.Percentage;
import ch.codeblock.qrinvoice.documents.model.application.Position;
import ch.codeblock.qrinvoice.model.validation.ValidationException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public final class PositionBuilder {
    private String position;
    private String description;
    private Double quantity;
    private String unit;
    private Amount unitPrice;
    private Amount discountAbsolute;
    private Percentage discountPercentage;
    private Amount total;

    private List<AdditionalProperty> additionalProperties;

    private PositionBuilder() {
    }

    public static PositionBuilder create() {
        return new PositionBuilder();
    }

    public PositionBuilder position(String position) {
        this.position = position;
        return this;
    }

    public PositionBuilder position(Integer position) {
        if (position == null) {
            this.position = null;
        } else {
            this.position = position.toString();
        }
        return this;
    }

    public PositionBuilder description(String description) {
        this.description = description;
        return this;
    }

    public PositionBuilder description(final Consumer<TextBuilder> func) {
        final TextBuilder textBuilder = TextBuilder.create();
        func.accept(textBuilder);
        return description(textBuilder.build());
    }

    public PositionBuilder quantity(final Integer quantity) {
        return quantity(quantity.doubleValue());
    }

    public PositionBuilder quantity(Double quantity) {
        this.quantity = quantity;
        return this;
    }

    public PositionBuilder unit(String unit) {
        this.unit = unit;
        return this;
    }

    public PositionBuilder unitPrice(Amount unitPrice) {
        this.unitPrice = unitPrice;
        return this;
    }

    public PositionBuilder unitPriceVatExclusive(final Double unitPriceVatExclusive) {
        (this.unitPrice = Amount.createIfMissing(this.unitPrice)).setVatExclusive(unitPriceVatExclusive);
        return this;
    }

    public PositionBuilder unitPriceVatInclusive(final Double unitPriceVatInclusive) {
        (this.unitPrice = Amount.createIfMissing(this.unitPrice)).setVatInclusive(unitPriceVatInclusive);
        return this;
    }

    public PositionBuilder vatPercentage(final double vatPercentage) {
        (this.unitPrice = Amount.createIfMissing(this.unitPrice)).setVatPercentage(Percentage.ofNumericalPercentage(vatPercentage));
        return this;
    }

    public PositionBuilder vatPercentage(final String vatPercentage) {
        (this.unitPrice = Amount.createIfMissing(this.unitPrice)).setVatPercentage(new Percentage(vatPercentage));
        return this;
    }

    public PositionBuilder discountAbsolute(Amount discountAbsolute) {
        this.discountAbsolute = discountAbsolute;
        return this;
    }

    public PositionBuilder discountAbsoluteVatExclusive(final Double discountAbsoluteVatExclusive) {
        (this.discountAbsolute = Amount.createIfMissing(this.discountAbsolute)).setVatExclusive(discountAbsoluteVatExclusive);
        return this;
    }

    public PositionBuilder discountAbsoluteVatInclusive(final Double discountAbsoluteVatInclusive) {
        (this.discountAbsolute = Amount.createIfMissing(this.discountAbsolute)).setVatInclusive(discountAbsoluteVatInclusive);
        return this;
    }

    public PositionBuilder discountPercentage(Percentage discountPercentage) {
        this.discountPercentage = discountPercentage;
        return this;
    }

    public PositionBuilder discountPercentage(final double discountPercentage) {
        return discountPercentage(Percentage.ofNumericalPercentage(discountPercentage));
    }

    public PositionBuilder total(Amount total) {
        this.total = total;
        return this;
    }

    public PositionBuilder addProperty(String key, String value) {
        if (this.additionalProperties == null) {
            this.additionalProperties = new ArrayList<>();
        }
        this.additionalProperties.add(new AdditionalProperty(key, value));
        return this;
    }

    public PositionBuilder additionalProperties(List<AdditionalProperty> additionalProperties) {
        this.additionalProperties = additionalProperties;
        return this;
    }

    public Position build() {
        if (unitPrice != null) {
            if (unitPrice.getVatExclusive() != null && unitPrice.getVatInclusive() != null) {
                throw new ValidationException("Either provide unit price including or excluding VAT");
            }

            if (unitPrice.getVatPercentage() == null) {
                throw new ValidationException("VAT percentage has to be set, even if it is 0");
            }
        }

        final Position position = new Position();
        position.setPosition(this.position);
        position.setDescription(description);
        position.setQuantity(quantity);
        position.setUnit(unit);
        position.setUnitPrice(unitPrice);
        position.setDiscountAbsolute(discountAbsolute);
        position.setDiscountPercentage(discountPercentage);
        position.setTotal(total);
        position.setAdditionalProperties(additionalProperties);
        return position;
    }

}
