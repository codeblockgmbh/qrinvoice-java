/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application.builder;

import ch.codeblock.qrinvoice.documents.model.application.*;
import ch.codeblock.qrinvoice.model.QrInvoice;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.function.Consumer;

public final class InvoiceDocumentBuilder {
    private String customerNr;
    private String customerReference;
    private LocalDate invoiceDate;
    private LocalDate invoiceDueDate;
    private Integer termOfPaymentDays;
    private String invoiceNr;
    private String title;
    private String prefaceText;
    private String endingText;
    private List<Position> positions;
    private Currency currency;

    private List<AdditionalProperty> additionalProperties;

    private AddressBuilder senderBuilder;
    private AddressBuilder recipientBuilder;
    private ContactPersonBuilder contactPersonBuilder;

    private QrInvoice qrInvoice;

    private InvoiceDocumentBuilder() {
    }

    public static InvoiceDocumentBuilder create() {
        return new InvoiceDocumentBuilder();
    }

    public InvoiceDocumentBuilder customerNr(String customerNr) {
        this.customerNr = customerNr;
        return this;
    }

    public InvoiceDocumentBuilder customerReference(String customerReference) {
        this.customerReference = customerReference;
        return this;
    }

    public InvoiceDocumentBuilder invoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
        return this;
    }

    public InvoiceDocumentBuilder invoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
        return this;
    }

    public InvoiceDocumentBuilder termOfPaymentDays(Integer termOfPaymentDays) {
        this.termOfPaymentDays = termOfPaymentDays;
        return this;
    }

    public InvoiceDocumentBuilder invoiceNr(String invoiceNr) {
        this.invoiceNr = invoiceNr;
        return this;
    }

    public InvoiceDocumentBuilder title(String title) {
        this.title = title;
        return this;
    }

    public InvoiceDocumentBuilder prefaceText(String prefaceText) {
        this.prefaceText = prefaceText;
        return this;
    }

    public InvoiceDocumentBuilder endingText(String endingText) {
        this.endingText = endingText;
        return this;
    }

    public InvoiceDocumentBuilder addPosition(final Position position) {
        return addPositions(position);
    }

    public InvoiceDocumentBuilder addPositions(final Position... positions) {
        return addPositions(Arrays.asList(positions));
    }

    public InvoiceDocumentBuilder addPositions(final List<Position> additionalPositions) {
        if (this.positions == null) {
            positions(additionalPositions);
        } else {
            this.positions = new ArrayList<>(this.positions);
            for (Position additionalPosition : additionalPositions) {
                if (!this.positions.contains(additionalPosition)) {
                    this.positions.add(additionalPosition);
                }
            }
        }
        return this;
    }

    public InvoiceDocumentBuilder positions(List<Position> positions) {
        this.positions = positions;
        return this;
    }

    public InvoiceDocumentBuilder currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public InvoiceDocumentBuilder qrInvoice(QrInvoice qrInvoice) {
        this.qrInvoice = qrInvoice;
        return this;
    }

    /**
     * @return The AddressBuilder instance of the sender
     * @see Address
     */
    public AddressBuilder sender() {
        if (senderBuilder == null) {
            senderBuilder = AddressBuilder.create();
        }
        return senderBuilder;
    }

    /**
     * @param func A lambda function which uses the supplied {@link AddressBuilder} to build the sender {@link Address}
     * @return The {@link InvoiceDocumentBuilder} instance
     * @see Address
     */
    public InvoiceDocumentBuilder sender(final Consumer<AddressBuilder> func) {
        func.accept(sender());
        return this;
    }

    /**
     * @return The AddressBuilder instance of the sender
     * @see Address
     */
    public AddressBuilder recipient() {
        if (recipientBuilder == null) {
            recipientBuilder = AddressBuilder.create();
        }
        return recipientBuilder;
    }

    /**
     * @param func A lambda function which uses the supplied {@link AddressBuilder} to build the recipient {@link Address}
     * @return The {@link InvoiceDocumentBuilder} instance
     * @see Address
     */
    public InvoiceDocumentBuilder recipient(final Consumer<AddressBuilder> func) {
        func.accept(recipient());
        return this;
    }


    /**
     * @return The ContactPersonBuilder instance
     * @see ContactPerson
     */
    public ContactPersonBuilder contactPerson() {
        if (contactPersonBuilder == null) {
            contactPersonBuilder = ContactPersonBuilder.create();
        }
        return contactPersonBuilder;
    }

    /**
     * @param func A lambda function which uses the supplied {@link ContactPersonBuilder} to build the recipient {@link ContactPerson}
     * @return The {@link ContactPersonBuilder} instance
     * @see ContactPerson
     */
    public InvoiceDocumentBuilder contactPerson(final Consumer<ContactPersonBuilder> func) {
        func.accept(contactPerson());
        return this;
    }

    public InvoiceDocumentBuilder prefaceText(final Consumer<TextBuilder> func) {
        final TextBuilder textBuilder = TextBuilder.create();
        func.accept(textBuilder);
        return prefaceText(textBuilder.build());
    }

    public InvoiceDocumentBuilder endingText(final Consumer<TextBuilder> func) {
        final TextBuilder textBuilder = TextBuilder.create();
        func.accept(textBuilder);
        return endingText(textBuilder.build());
    }

    public InvoiceDocumentBuilder addPosition(final Consumer<PositionBuilder> func) {
        final PositionBuilder positionBuilder = PositionBuilder.create();
        func.accept(positionBuilder);
        addPositions(positionBuilder.build());
        return this;
    }

    public InvoiceDocumentBuilder addProperty(String key, String value) {
        if (this.additionalProperties == null) {
            this.additionalProperties = new ArrayList<>();
        }
        this.additionalProperties.add(new AdditionalProperty(key, value));
        return this;
    }

    public InvoiceDocumentBuilder additionalProperties(List<AdditionalProperty> additionalProperties) {
        this.additionalProperties = additionalProperties;
        return this;
    }

    public InvoiceDocument build() {
        InvoiceDocument invoice = new InvoiceDocument();
        invoice.setSender(senderBuilder != null ? senderBuilder.build() : null);
        invoice.setRecipient(recipientBuilder != null ? recipientBuilder.build() : null);
        invoice.setContactPerson(contactPersonBuilder != null ? contactPersonBuilder.build() : null);
        invoice.setCustomerNr(customerNr);
        invoice.setCustomerReference(customerReference);
        invoice.setInvoiceDate(invoiceDate);
        invoice.setInvoiceDueDate(invoiceDueDate);
        invoice.setTermOfPaymentDays(termOfPaymentDays);
        invoice.setInvoiceNr(invoiceNr);
        invoice.setTitle(title);
        invoice.setPrefaceText(prefaceText);
        invoice.setEndingText(endingText);
        invoice.setPositions(positions);
        invoice.setCurrency(currency);
        invoice.setAdditionalProperties(additionalProperties);
        invoice.setQrInvoice(qrInvoice);
        return invoice;
    }

}
