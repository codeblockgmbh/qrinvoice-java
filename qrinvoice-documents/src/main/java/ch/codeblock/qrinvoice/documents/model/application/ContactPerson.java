/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import ch.codeblock.qrinvoice.model.annotation.Example;

public class ContactPerson {
    private String name;
    private String email;
    private String phoneNr;

    @Example("John Doe")
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Example("john.doe@example.com")
    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    @Example("+41 00 000 00 00")
    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(final String phoneNr) {
        this.phoneNr = phoneNr;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ContactPerson{");
        sb.append("name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", phoneNr='").append(phoneNr).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
