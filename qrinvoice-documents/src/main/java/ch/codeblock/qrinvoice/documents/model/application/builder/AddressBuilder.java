/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application.builder;

import ch.codeblock.qrinvoice.documents.model.application.Address;

import java.util.ArrayList;
import java.util.List;

public final class AddressBuilder {
    private List<String> addressLines;

    private AddressBuilder() {
    }

    public static AddressBuilder create() {
        return new AddressBuilder();
    }

    public AddressBuilder addressLines(String[] addressLines) {
        if (addressLines != null) {
            for (String addressLine : addressLines) {
                addAddressLine(addressLine);
            }
        }
        return this;
    }

    public AddressBuilder addressLines(List<String> addressLines) {
        this.addressLines = addressLines;
        return this;
    }

    public AddressBuilder addAddressLine(String addressLine) {
        if (addressLines == null) {
            addressLines = new ArrayList<>();
        }
        addressLines.add(addressLine);
        return this;
    }

    public Address build() {
        Address address = new Address();
        address.setAddressLines(addressLines);
        return address;
    }
}
