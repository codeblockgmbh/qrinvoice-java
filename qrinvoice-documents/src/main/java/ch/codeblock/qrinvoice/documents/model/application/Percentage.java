/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import ch.codeblock.qrinvoice.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;
import java.util.Objects;

import static ch.codeblock.qrinvoice.documents.utils.BigDecimalUtils.stripTrailingZeros;

public class Percentage {
    public static final BigDecimal HUNDRED = BigDecimal.valueOf(100);
    private static final String DECIMAL_FORMAT_PATTERN = "###,###,##0.##########";
    private static final DecimalFormatSymbols DECIMAL_FORMAT_SYMBOLS = new DecimalFormatSymbols();

    static {
        DECIMAL_FORMAT_SYMBOLS.setGroupingSeparator('\'');
        DECIMAL_FORMAT_SYMBOLS.setDecimalSeparator('.');
    }

    private final BigDecimal numericalValue;
    private final String percentageValue;

    public Percentage(final String percentageValue) {
        this(convertToDecimal(removePercentageSign(StringUtils.trimToNull(percentageValue))));
    }

    public Percentage(final BigDecimal numericalValue) {
        this(numericalValue, convertToPercentageValue(numericalValue));
    }

    private Percentage(final BigDecimal numericalValue, final String percentageValue) {
        this.numericalValue = numericalValue;
        this.percentageValue = percentageValue;
    }

    public BigDecimal getNumericalValue() {
        return numericalValue;
    }

    BigDecimal addMultiplier() {
        if (isNegative()) {
            throw new RuntimeException("Multiplier not supported for negative percentages");
        }
        return BigDecimal.ONE.add(numericalValue);
    }

    BigDecimal subtractMultiplier() {
        if (isNegative()) {
            throw new RuntimeException("Multiplier not supported for negative percentages");
        }
        return BigDecimal.ONE.subtract(numericalValue);
    }

    public BigDecimal addPercentageToValue(BigDecimal value) {
        return stripTrailingZeros(value.multiply(addMultiplier()));
    }

    public BigDecimal subtractPercentageFromValue(BigDecimal value) {
        return stripTrailingZeros(value.multiply(subtractMultiplier()));
    }

    public BigDecimal subtractAddedPercentageFromValue(BigDecimal value) {
        return stripTrailingZeros(value
                .setScale(10, RoundingMode.HALF_UP)
                .divide(addMultiplier(), RoundingMode.HALF_UP));
    }

    public BigDecimal getNumericalPercentageValue() {
        return getNumericalValue().multiply(HUNDRED);
    }

    public String getPercentageValue() {
        return percentageValue;
    }

    public boolean isNegative() {
        return numericalValue.signum() == -1;
    }

    public boolean isGreaterThan100Percent() {
        return numericalValue.compareTo(BigDecimal.ONE) > 0;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Percentage)) return false;
        final Percentage that = (Percentage) o;
        return numericalValue.compareTo(that.numericalValue) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numericalValue);
    }

    /**
     * @param percentage, e.g. 7.7%
     * @return Percentage, or null, if null or blank string was passed
     */
    public static Percentage of(final String percentage) {
        if (percentage == null || percentage.trim().isEmpty()) {
            return null;
        }
        return new Percentage(percentage);
    }

    /**
     * @param percentage, e.g. 7.7
     * @return Percentage, or null, if null was passed
     */
    public static Percentage ofNumericalPercentage(final BigDecimal percentage) {
        if (percentage == null) {
            return null;
        }
        return new Percentage(percentageValueToDecimal(percentage));
    }

    /**
     * @param number, e.g. 7.7
     * @return Percante instance for 7.7%
     */
    public static Percentage ofNumericalPercentage(final double number) {
        return new Percentage(percentageValueToDecimal(BigDecimal.valueOf(number)));
    }

    private static String convertToPercentageValue(final BigDecimal numericalValue) {
        if (numericalValue == null) {
            throw new IllegalArgumentException("Numerical value must not be null");
        }
        final BigDecimal percentageValue = numericalValue.multiply(HUNDRED);
        return createDecimalFormat().format(percentageValue) + "%";
    }

    private static String removePercentageSign(final String percentageValueString) {
        if (percentageValueString == null) {
            throw new IllegalArgumentException("Percentage value must not be null");
        } else if ("".equals(percentageValueString.trim())) {
            throw new IllegalArgumentException("Percentage value must not be an empty string");
        } else if (percentageValueString.endsWith("%")) {
            final String percentageValueStringWithoutPctSign = percentageValueString.substring(0, percentageValueString.length() - 1);
            if ("".equals(percentageValueStringWithoutPctSign.trim())) {
                throw new IllegalArgumentException("An explicit percentage value is expected. '%' is not a valid input");
            } else {
                return percentageValueStringWithoutPctSign;
            }
        } else {
            throw new IllegalArgumentException("Percentag sign expected at the end. Parameter was: " + percentageValueString);
        }
    }

    private static BigDecimal convertToDecimal(final String percentageValueStringWithoutPctSign) {
        // first parse string
        final BigDecimal percentageValueBigDecimal = parsePercentageValueString(percentageValueStringWithoutPctSign);

        return percentageValueToDecimal(percentageValueBigDecimal);
    }

    private static BigDecimal percentageValueToDecimal(BigDecimal percentageValueBigDecimal) {
        // divide by 100 means increasing "scale" by 2 in order to stay accurate without rounding
        final int newScale = percentageValueBigDecimal.scale() + 2;
        return percentageValueBigDecimal.divide(HUNDRED, newScale, RoundingMode.UNNECESSARY);
    }

    private static BigDecimal parsePercentageValueString(final String percentageValueStringWithoutPctSign) {
        final DecimalFormat df = createDecimalFormat();

        // parse the string
        final ParsePosition parsePosition = new ParsePosition(0);
        final BigDecimal percentageValueBigDecimal = (BigDecimal) df.parse(percentageValueStringWithoutPctSign, parsePosition);
        if (parsePosition.getIndex() < percentageValueStringWithoutPctSign.length()) {
            // only when ParsePosition matches the strings length everything has been parsed
            // DecimalFormat just stops earlier when other characters are encountered
            throw new IllegalArgumentException("Input could not be parsed as number: " +
                    percentageValueStringWithoutPctSign + "%");
        }
        return percentageValueBigDecimal.stripTrailingZeros();
    }

    private static DecimalFormat createDecimalFormat() {
        final DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT_PATTERN, DECIMAL_FORMAT_SYMBOLS);
        df.setParseBigDecimal(true);
        return df;
    }

    @Override
    public String toString() {
        return percentageValue;
    }
}
