/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.template.mapper;


import ch.codeblock.qrinvoice.documents.model.application.AdditionalProperty;
import ch.codeblock.qrinvoice.documents.model.application.Amount;
import ch.codeblock.qrinvoice.documents.model.application.VatDetails;
import ch.codeblock.qrinvoice.documents.model.application.layout.DocumentLayout;
import ch.codeblock.qrinvoice.documents.model.template.*;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InvoiceMapper {
    public static InvoiceDocument map(ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument input, DocumentLayout invoiceDocumentLayout, PaymentPartReceipt ppr) {
        final InvoiceDocument invoice = new InvoiceDocument();
        invoice.setLabels(invoiceDocumentLayout.getLabels());

        invoice.setContactPerson(map(input.getContactPerson()));
        invoice.setCurrency(input.getCurrency());
        invoice.setCustomerNr(input.getCustomerNr());
        invoice.setCustomerReference(input.getCustomerReference());

        invoice.setPrefaceText(StringUtils.emptyStringAsNull(input.getPrefaceText()));
        invoice.setEndingText(StringUtils.emptyStringAsNull(input.getEndingText()));

        invoice.setGrossAmountVatExclusive(input.getGrossAmount().getVatExclusive().doubleValue());
        invoice.setGrossAmountVatInclusive(input.getGrossAmount().getVatInclusive().doubleValue());
        invoice.setRoundingDifference(input.getGrossAmount().getVatInclusiveRoundingDifference().doubleValue());

        invoice.setInvoiceDate(input.getInvoiceDate());
        invoice.setInvoiceDueDate(input.getInvoiceDueDate());
        invoice.setTermOfPaymentDays(input.getTermOfPaymentDays());
        invoice.setInvoiceNr(input.getInvoiceNr());
        invoice.setPositions(mapPositions(input.getPositions()));

        invoice.setHasDiscounts(input.getPositions().stream().anyMatch(ch.codeblock.qrinvoice.documents.model.application.Position::hasDiscount));

        invoice.setRecipient(map(input.getRecipient()));
        invoice.setSender(map(input.getSender()));
        
        invoice.setAdditionalProperties(map(input.getAdditionalProperties()));

        invoice.setPpr(mapResource(new ch.codeblock.qrinvoice.documents.model.application.layout.Resource(ppr)));
        invoice.setLogo(mapResource(invoiceDocumentLayout.getLogo()));

        invoice.setTitle(input.getTitle());
        invoice.setVatDetails(mapVatDetails(input.getVatDetails()));
        return invoice;
    }

    private static Map<String, String> map(List<AdditionalProperty> additionalProperties) {
        if(additionalProperties == null) {
            return null;
        }

        final HashMap<String, String> map = new HashMap<>();
        for (AdditionalProperty additionalProperty : additionalProperties) {
            map.put(additionalProperty.getKey(), additionalProperty.getValue());
        }
        return map;
    }

    public static Resource mapResource(ch.codeblock.qrinvoice.documents.model.application.layout.Resource resource) {
        if (resource == null) {
            return null;
        }
        if (resource.getFile() != null) {
            return new Resource(resource.getFile());
        } else {
            return new Resource(resource.getMimeType().getMimeType(), resource.getBase64Data());
        }
    }

    private static ContactPerson map(final ch.codeblock.qrinvoice.documents.model.application.ContactPerson input) {
        final ContactPerson contactPerson = new ContactPerson();
        if (input != null) {
            contactPerson.setName(input.getName());
            contactPerson.setEmail(input.getEmail());
            contactPerson.setPhoneNr(input.getPhoneNr());
        }
        return contactPerson;
    }

    private static Address map(final ch.codeblock.qrinvoice.documents.model.application.Address input) {
        final Address address = new Address();
        if (input != null) {
            address.setAddressLines(input.getAddressLines());
        }
        return address;
    }

    private static List<Position> mapPositions(final List<ch.codeblock.qrinvoice.documents.model.application.Position> input) {
        return input.stream()
                .map(InvoiceMapper::map)
                .collect(Collectors.toList());
    }

    private static Position map(final ch.codeblock.qrinvoice.documents.model.application.Position input) {
        final Position position = new Position();
        position.setDescription(input.getDescription());
        position.setPosition(input.getPosition());
        position.setQuantity(input.getQuantity());

        if(input.getDiscountAbsolute() != null) {
            if(input.getDiscountAbsolute().getVatExclusive() != null) {
                position.setDiscountAbsoluteVatExclusive(input.getDiscountAbsolute().getVatExclusive().doubleValue());
            }
            if(input.getDiscountAbsolute().getVatInclusive() != null) {
                position.setDiscountAbsoluteVatInclusive(input.getDiscountAbsolute().getVatInclusive().doubleValue());
            }
        }

        if (input.getDiscountPercentage() != null) {
            position.setDiscountPercentage(input.getDiscountPercentage().getNumericalPercentageValue().doubleValue());
        }

        if (input.getUnitPrice() != null) {
            position.setUnitPriceVatExclusive(input.getUnitPrice().getVatExclusive().doubleValue());
            position.setUnitPriceVatInclusive(input.getUnitPrice().getVatInclusive().doubleValue());

            if (input.getUnitPrice().getVatPercentage() != null) {
                position.setVatPercentage(input.getUnitPrice().getVatPercentage().getNumericalPercentageValue().doubleValue());
            }
        }
        position.setUnit(input.getUnit());
        if (!Amount.isEmpty(input.getTotal())) {
            position.setAmountVatExclusive(input.getTotal().getVatExclusive().doubleValue());
            position.setAmountVatInclusive(input.getTotal().getVatInclusive().doubleValue());
            position.setVatAmount(input.getTotal().getVatAmount().doubleValue());
        }

        position.setAdditionalProperties(map(input.getAdditionalProperties()));
        
        return position;
    }

    private static List<ch.codeblock.qrinvoice.documents.model.template.VatDetails> mapVatDetails(final List<VatDetails> input) {
        return input.stream()
                .map(InvoiceMapper::map)
                .collect(Collectors.toList());
    }

    private static ch.codeblock.qrinvoice.documents.model.template.VatDetails map(final VatDetails input) {
        final ch.codeblock.qrinvoice.documents.model.template.VatDetails vatDetails = new ch.codeblock.qrinvoice.documents.model.template.VatDetails();
        vatDetails.setTaxBase(input.getTaxBase().doubleValue());
        vatDetails.setTaxAmount(input.getTaxAmount().doubleValue());
        vatDetails.setTaxPercentage(input.getTaxPercentage().getNumericalPercentageValue().doubleValue());
        return vatDetails;
    }
}
