/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import ch.codeblock.qrinvoice.model.annotation.Description;

import java.util.List;

/**
 * Using the correct address format and providing complete address information ensures the entire mailing process runs smoothly and that consignments are delivered without problems to the correct recipient
 *
 * <ul>
 * <li>At least three and no more than six lines</li>
 * <li>Addressing lengthwise on the consignment</li>
 * <li>Official street name in full and correct house number</li>
 * <li>P.O. Box details on the penultimate line</li>
 * <li>The correct postcode and the full name of the town</li>
 * <li>No underlined postcode or town</li>
 * <li>Private address</li>
 * </ul>
 * <p>
 * Source: https://www.post.ch/en/sending-letters/addressing-and-designing/addressing-consignments-correctly
 */
public class Address {
    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("All address lines, ideally contains the country as well")
    private List<String> addressLines;

    public List<String> getAddressLines() {
        return addressLines;
    }

    public void setAddressLines(final List<String> addressLines) {
        this.addressLines = addressLines;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Address{");
        sb.append("addressLines=").append(addressLines);
        sb.append('}');
        return sb.toString();
    }
}
