/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.template;

import ch.codeblock.qrinvoice.documents.jaxb.LocalDateAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;
import java.util.Map;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoiceDocument")
@XmlRootElement(name = "invoice")
public class InvoiceDocument {
    @XmlElement(name = "labels")
    private Labels labels;

    @XmlElement(name = "sender")
    private Address sender;
    @XmlElement(name = "recipient")
    private Address recipient;

    @XmlElement(name = "contact-person")
    private ContactPerson contactPerson;

    @XmlElement(name = "customer-nr")
    private String customerNr;
    @XmlElement(name = "customer-reference")
    private String customerReference;

    @XmlElement(name = "invoice-date")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate invoiceDate;

    @XmlElement(name = "invoice-due-date")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate invoiceDueDate;

    @XmlElement(name = "term-of-payment-days")
    private Integer termOfPaymentDays;

    @XmlElement(name = "invoice-nr")
    private String invoiceNr;
    @XmlElement(name = "title")
    private String title;
    @XmlElement(name = "preface-text")
    private String prefaceText;
    @XmlElement(name = "ending-text")
    private String endingText;

    @XmlElement(name = "has-discounts")
    private Boolean hasDiscounts;

    @XmlElement(name = "position")
    @XmlElementWrapper(name = "positions")
    private List<Position> positions;

    @XmlElementWrapper(name = "vat-details")
    @XmlElement(name = "vat-details")
    private List<VatDetails> vatDetails;

    @XmlElement(name = "gross-amount-vat-exclusive")
    private Double grossAmountVatExclusive;
    @XmlElement(name = "gross-amount-vat-inclusive")
    private Double grossAmountVatInclusive;

    @XmlElement(name = "rounding-difference")
    private Double roundingDifference;

    @XmlElement(name = "currency")
    private Currency currency;

    @XmlElement(name = "logo")
    private Resource logo;

    @XmlElement(name = "ppr")
    private Resource ppr;

    // TODO add test for data structure (especially for lists and maps)
    @XmlElement(name = "additional-properties")
    private Map<String, String> additionalProperties;

    public Labels getLabels() {
        return labels;
    }

    public void setLabels(Labels labels) {
        this.labels = labels;
    }

    public Address getSender() {
        return sender;
    }

    public void setSender(final Address sender) {
        this.sender = sender;
    }

    public Address getRecipient() {
        return recipient;
    }

    public void setRecipient(final Address recipient) {
        this.recipient = recipient;
    }

    public ContactPerson getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(final ContactPerson contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getCustomerNr() {
        return customerNr;
    }

    public void setCustomerNr(final String customerNr) {
        this.customerNr = customerNr;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(final String customerReference) {
        this.customerReference = customerReference;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(final LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(final LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public void setTermOfPaymentDays(Integer termOfPaymentDays) {
        this.termOfPaymentDays = termOfPaymentDays;
    }

    public Integer getTermOfPaymentDays() {
        return termOfPaymentDays;
    }

    public String getInvoiceNr() {
        return invoiceNr;
    }

    public void setInvoiceNr(final String invoiceNr) {
        this.invoiceNr = invoiceNr;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getPrefaceText() {
        return prefaceText;
    }

    public void setPrefaceText(final String prefaceText) {
        this.prefaceText = prefaceText;
    }

    public String getEndingText() {
        return endingText;
    }

    public void setEndingText(final String endingText) {
        this.endingText = endingText;
    }

    public Boolean getHasDiscounts() {
        return hasDiscounts;
    }

    public void setHasDiscounts(Boolean hasDiscounts) {
        this.hasDiscounts = hasDiscounts;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(final List<Position> positions) {
        this.positions = positions;
    }

    public List<VatDetails> getVatDetails() {
        return vatDetails;
    }

    public void setVatDetails(final List<VatDetails> vatDetails) {
        this.vatDetails = vatDetails;
    }

    public Double getGrossAmountVatExclusive() {
        return grossAmountVatExclusive;
    }

    public void setGrossAmountVatExclusive(final Double grossAmountVatExclusive) {
        this.grossAmountVatExclusive = grossAmountVatExclusive;
    }

    public Double getGrossAmountVatInclusive() {
        return grossAmountVatInclusive;
    }

    public void setGrossAmountVatInclusive(final Double grossAmountVatInclusive) {
        this.grossAmountVatInclusive = grossAmountVatInclusive;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    public Double getRoundingDifference() {
        return roundingDifference;
    }

    public void setRoundingDifference(final Double roundingDifference) {
        this.roundingDifference = roundingDifference;
    }

    public Resource getLogo() {
        return logo;
    }

    public void setLogo(Resource logo) {
        this.logo = logo;
    }

    public Resource getPpr() {
        return ppr;
    }

    public void setPpr(Resource ppr) {
        this.ppr = ppr;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }
}
