/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.xsl;

import org.apache.xml.res.XMLMessages;
import org.apache.xml.utils.SAXSourceLocator;
import org.apache.xml.utils.WrappedRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.TransformerException;
import java.io.PrintStream;

/**
 * An SLF4J based alternative to {@link org.apache.xml.utils.DefaultErrorHandler}
 */
public class Slf4jErrorHandler implements ErrorHandler, ErrorListener {
    private static final Logger logger = LoggerFactory.getLogger(Slf4jErrorHandler.class);
    boolean throwExceptionOnError;

    public Slf4jErrorHandler(boolean throwExceptionOnError) {
        this.throwExceptionOnError = true;
        this.throwExceptionOnError = throwExceptionOnError;
    }

    public void warning(SAXParseException exception) throws SAXException {
        printLocation(exception);
        logger.warn("Parser warning: {}", exception.getMessage());
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        throw exception;
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        throw exception;
    }

    @Override
    public void warning(TransformerException exception) throws TransformerException {
        printLocation(exception);
        logger.warn(exception.getMessage());
    }

    @Override
    public void error(TransformerException exception) throws TransformerException {
        if (this.throwExceptionOnError) {
            throw exception;
        } else {
            printLocation(exception);
            logger.error(exception.getMessage());
        }
    }

    @Override
    public void fatalError(TransformerException exception) throws TransformerException {
        if (this.throwExceptionOnError) {
            throw exception;
        } else {
            printLocation(exception);
            logger.error(exception.getMessage());
        }
    }

    public static void ensureLocationSet(TransformerException exception) {
        SourceLocator locator = null;
        Object cause = exception;

        do {
            if (cause instanceof SAXParseException) {
                locator = new SAXSourceLocator((SAXParseException) cause);
            } else if (cause instanceof TransformerException) {
                SourceLocator causeLocator = ((TransformerException) cause).getLocator();
                if (null != causeLocator) {
                    locator = causeLocator;
                }
            }

            if (cause instanceof TransformerException) {
                cause = ((TransformerException) cause).getCause();
            } else if (cause instanceof SAXException) {
                cause = ((SAXException) cause).getException();
            } else {
                cause = null;
            }
        } while (null != cause);

        exception.setLocator((SourceLocator) locator);
    }

    public static void printLocation(PrintStream pw, TransformerException exception) {
        printLocation((Throwable) exception);
    }

    public static void printLocation(PrintStream pw, SAXParseException exception) {
        printLocation((Throwable) exception);
    }

    public static void printLocation(Throwable exception) {
        SourceLocator locator = null;
        Object cause = exception;

        do {
            if (cause instanceof SAXParseException) {
                locator = new SAXSourceLocator((SAXParseException) cause);
            } else if (cause instanceof TransformerException) {
                SourceLocator causeLocator = ((TransformerException) cause).getLocator();
                if (null != causeLocator) {
                    locator = causeLocator;
                }
            }

            if (cause instanceof TransformerException) {
                cause = ((TransformerException) cause).getCause();
            } else if (cause instanceof WrappedRuntimeException) {
                cause = ((WrappedRuntimeException) cause).getException();
            } else if (cause instanceof SAXException) {
                cause = ((SAXException) cause).getException();
            } else {
                cause = null;
            }
        } while (null != cause);

        if (null != locator) {
            String id = null != ((SourceLocator) locator).getPublicId() ? ((SourceLocator) locator).getPublicId() : (null != ((SourceLocator) locator).getSystemId() ? ((SourceLocator) locator).getSystemId() : XMLMessages.createXMLMessage("ER_SYSTEMID_UNKNOWN", (Object[]) null));
            logger.info(id + "; " + XMLMessages.createXMLMessage("line", (Object[]) null) + ((SourceLocator) locator).getLineNumber() + "; " + XMLMessages.createXMLMessage("column", (Object[]) null) + ((SourceLocator) locator).getColumnNumber() + "; ");
        } else {
            logger.info("(" + XMLMessages.createXMLMessage("ER_LOCATION_UNKNOWN", (Object[]) null) + ")");
        }

    }
}
