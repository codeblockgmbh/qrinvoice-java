/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application.builder;

import ch.codeblock.qrinvoice.documents.model.application.Amount;
import ch.codeblock.qrinvoice.documents.model.application.Percentage;

import java.math.BigDecimal;

public final class AmountBuilder {
    private BigDecimal vatExclusive;
    private BigDecimal vatInclusive;
    private BigDecimal vatAmount;
    private Percentage vatPercentage;
    private BigDecimal vatInclusiveRoundingDifference;

    private AmountBuilder() {
    }

    public static AmountBuilder create() {
        return new AmountBuilder();
    }

    public AmountBuilder vatExclusive(double vatExclusive) {
        return vatExclusive(BigDecimal.valueOf(vatExclusive));
    }

    public AmountBuilder vatExclusive(BigDecimal vatExclusive) {
        this.vatExclusive = vatExclusive;
        return this;
    }

    public AmountBuilder vatInclusive(double vatInclusive) {
        return this.vatInclusive(BigDecimal.valueOf(vatInclusive));
    }

    public AmountBuilder vatInclusive(BigDecimal vatInclusive) {
        this.vatInclusive = vatInclusive;
        return this;
    }

    public AmountBuilder vatAmount(double vatAmount) {
        return this.vatAmount(BigDecimal.valueOf(vatAmount));
    }

    public AmountBuilder vatAmount(BigDecimal vatAmount) {
        this.vatAmount = vatAmount;
        return this;
    }

    public AmountBuilder vatPercentage(Percentage vatPercentage) {
        this.vatPercentage = vatPercentage;
        return this;
    }

    public AmountBuilder vatPercentage(double vatPercentage) {
        this.vatPercentage = Percentage.ofNumericalPercentage(vatPercentage);
        return this;
    }

    public AmountBuilder vatInclusiveRoundingDifference(BigDecimal vatInclusiveRoundingDifference) {
        this.vatInclusiveRoundingDifference = vatInclusiveRoundingDifference;
        return this;
    }

    public Amount build() {
        final Amount amount = new Amount();
        amount.setVatExclusive(vatExclusive);
        amount.setVatInclusive(vatInclusive);
        amount.setVatAmount(vatAmount);
        amount.setVatPercentage(vatPercentage);
        amount.setVatInclusiveRoundingDifference(vatInclusiveRoundingDifference);
        return amount;
    }
}
