/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents;

import ch.codeblock.qrinvoice.*;
import ch.codeblock.qrinvoice.documents.model.application.InvoiceDocument;
import ch.codeblock.qrinvoice.documents.model.application.layout.DocumentLayout;
import ch.codeblock.qrinvoice.documents.model.template.Labels;
import ch.codeblock.qrinvoice.documents.model.template.mapper.InvoiceMapper;
import ch.codeblock.qrinvoice.documents.output.QrInvoiceDocument;
import ch.codeblock.qrinvoice.documents.utils.ResourceBundleUtils;
import ch.codeblock.qrinvoice.model.validation.ValidationException;
import ch.codeblock.qrinvoice.output.PaymentPartReceipt;
import ch.codeblock.qrinvoice.paymentpartreceipt.BoundaryLines;
import ch.codeblock.qrinvoice.paymentpartreceipt.LayoutDefinitions;
import org.apache.fop.apps.Fop;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.Locale;
import java.util.ResourceBundle;

import static ch.codeblock.qrinvoice.documents.xsl.XslFoProcessor.transform;

public class QrInvoiceDocumentCreator {

    public static QrInvoiceDocumentCreator create() {
        return new QrInvoiceDocumentCreator();
    }

    private InvoiceDocument invoice;
    private DocumentLayout invoiceLayout;
    private FontFamily fontFamily;
    private Locale locale;
    private BoundaryLines boundaryLines;
    private Boolean boundaryLineScissors;
    private Boolean boundaryLineSeparationText;
    private Boolean additionalPrintMargin;

    private QrInvoiceDocumentCreator() {
    }

    public QrInvoiceDocumentCreator invoice(final InvoiceDocument invoice) {
        this.invoice = invoice;
        return this;
    }

    public QrInvoiceDocumentCreator invoiceLayout(final DocumentLayout invoiceLayout) {
        this.invoiceLayout = invoiceLayout;
        return this;
    }

    public QrInvoiceDocumentCreator fontFamily(final FontFamily fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    public QrInvoiceDocumentCreator locale(final Locale locale) {
        if (!LayoutDefinitions.SUPPORTED_LOCALES.contains(locale)) {
            throw new ValidationException("Unsupported locale '" + locale + "'");
        }
        this.locale = locale;
        return this;
    }

    public QrInvoiceDocumentCreator boundaryLines(final boolean boundaryLines) {
        this.boundaryLines = boundaryLines ? BoundaryLines.ENABLED : BoundaryLines.NONE;
        return this;
    }

    public QrInvoiceDocumentCreator boundaryLines(final BoundaryLines boundaryLines) {
        this.boundaryLines = boundaryLines;
        return this;
    }

    public QrInvoiceDocumentCreator withBoundaryLines() {
        return boundaryLines(true);
    }

    public QrInvoiceDocumentCreator withBoundaryLinesWithMargins() {
        return boundaryLines(BoundaryLines.ENABLED_WITH_MARGINS);
    }

    public QrInvoiceDocumentCreator withoutBoundaryLines() {
        return boundaryLines(false);
    }

    public QrInvoiceDocumentCreator withScissors() {
        return boundaryLineScissors(true);
    }

    public QrInvoiceDocumentCreator withoutScissors() {
        return boundaryLineScissors(false);
    }

    public QrInvoiceDocumentCreator boundaryLineScissors(final boolean boundaryLineScissors) {
        this.boundaryLineScissors = boundaryLineScissors;
        return this;
    }

    public QrInvoiceDocumentCreator withSeparationText() {
        return boundaryLineSeparationText(true);
    }

    public QrInvoiceDocumentCreator withoutSeparationText() {
        return boundaryLineSeparationText(false);
    }

    public QrInvoiceDocumentCreator boundaryLineSeparationText(final boolean boundaryLineSeparationText) {
        this.boundaryLineSeparationText = boundaryLineSeparationText;
        return this;
    }

    public QrInvoiceDocumentCreator additionalPrintMargin(final boolean additionalPrintMargin) {
        this.additionalPrintMargin = additionalPrintMargin;
        return this;
    }

    public QrInvoiceDocumentCreator withAdditionalPrintMargin() {
        return additionalPrintMargin(true);
    }

    public QrInvoiceDocumentCreator withoutAdditionalPrintMargin() {
        return additionalPrintMargin(false);
    }

    public QrInvoiceDocumentCreator inGerman() {
        this.locale(Locale.GERMAN);
        return this;
    }

    public QrInvoiceDocumentCreator inFrench() {
        this.locale(Locale.FRENCH);
        return this;
    }

    public QrInvoiceDocumentCreator inEnglish() {
        this.locale(Locale.ENGLISH);
        return this;
    }

    public QrInvoiceDocumentCreator inItalian() {
        this.locale(Locale.ITALIAN);
        return this;
    }

    /**
     * Create the {@link QrInvoiceDocument}
     *
     * @return QrInvoiceDocument containing the output as data
     * @throws BaseException
     */
    public QrInvoiceDocument createQrInvoiceDocument() throws BaseException {
        try {
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            final QrInvoiceDocument document = createQrInvoiceDocument(byteArrayOutputStream);
            return new QrInvoiceDocument(document.getPageSize(), document.getOutputFormat(), byteArrayOutputStream.toByteArray(), document.getPageCount());
        } catch (Exception e) {
            if (e instanceof BaseException) {
                throw (BaseException) e;
            } else {
                throw new TechnicalException(e);
            }
        }

    }

    /**
     * Create the {@link QrInvoiceDocument} but write PDF output directly to passed {@link OutputStream}.
     *
     * @param out The output stream to write into
     * @return QrInvoiceDocument without data property set
     */
    public QrInvoiceDocument createQrInvoiceDocument(OutputStream out) {
        try {
            final ch.codeblock.qrinvoice.documents.model.template.InvoiceDocument obj = createInvoiceDocument();
            final Fop fop = transform(invoiceLayout.getDocumentTemplate(), out, obj);
            return new QrInvoiceDocument(PageSize.A4, OutputFormat.PDF, null, fop.getResults().getPageCount());
        } catch (Exception e) {
            if (e instanceof BaseException) {
                throw (BaseException) e;
            } else {
                throw new TechnicalException(e);
            }
        }
    }

    public ch.codeblock.qrinvoice.documents.model.template.InvoiceDocument createInvoiceDocument() {
        invoiceLayout.setLocale(locale);

        final Labels labels = new Labels();
        labels.setDefaultLabels(ResourceBundleUtils.convertResourceBundleToMap(ResourceBundle.getBundle("qrinvoicedocuments", locale)));
        invoiceLayout.setLabels(labels);

        return InvoiceMapper.map(invoice, invoiceLayout, createPpr());
    }

    private PaymentPartReceipt createPpr() {
        return QrInvoicePaymentPartReceiptCreator
                .create()
                .qrInvoice(invoice.getQrInvoice())
                .outputFormat(OutputFormat.PDF) // always PDF here
                .pageSize(PageSize.A5) // always A5
                .fontFamily(fontFamily)
                .boundaryLines(boundaryLines)
                .boundaryLineSeparationText(boundaryLineSeparationText)
                .boundaryLineScissors(boundaryLineScissors)
                .additionalPrintMargin(additionalPrintMargin)
                .locale(locale)
                .createPaymentPartReceipt();
    }
}
