/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application.layout;

import ch.codeblock.qrinvoice.MimeType;
import ch.codeblock.qrinvoice.output.Output;

import java.io.File;
import java.util.Base64;

public class Resource {
    private final MimeType mimeType;
    private final File file;
    private final byte[] data;
    private String base64Data;

    public Resource(MimeType mimeType, String base64Data) {
        this.mimeType = mimeType;
        this.base64Data = base64Data;
        data = null;
        file = null;
    }

    public Resource(Output output) {
        this(output.getOutputFormat().toMimeType(), output.getData());
    }

    public Resource(MimeType mimeType, byte[] data) {
        this.mimeType = mimeType;
        this.data = data;
        this.file = null;
    }
    public Resource(MimeType mimeType, File file) {
        this.mimeType = mimeType;
        this.data = null;
        this.file = file;
    }

    public MimeType getMimeType() {
        return mimeType;
    }

    public String getBase64Data() {
        if (base64Data == null && data != null) {
            base64Data = Base64.getEncoder().encodeToString(data);
        }
        return base64Data;
    }

    public File getFile() {
        return file;
    }
}
