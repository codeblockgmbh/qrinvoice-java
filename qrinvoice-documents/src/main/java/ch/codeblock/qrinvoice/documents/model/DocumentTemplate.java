/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DocumentTemplate {
    private static final Pattern PATTERN = Pattern.compile("^.*?(?<filebasename>[^\\\\/]+)\\.[^\\\\/]*$");
    private final File file;
    private final String classpathResource;
    private final String externalIdentifier;
    private String description;

    public DocumentTemplate(String classpathResource) {
        this(null, classpathResource);
    }

    public DocumentTemplate(File file) {
        this(file, null);
    }

    public DocumentTemplate(File file, String classpathResource) {
        this.file = file;
        this.classpathResource = classpathResource;
        if (file != null) {
            this.externalIdentifier = externalIdentifier(this.file);
        } else {
            this.externalIdentifier = externalIdentifier(this.classpathResource);
        }
        System.out.println("file: " + file);
        System.out.println("externalIdentifier: " + externalIdentifier);
    }

    private String externalIdentifier(File file) {
        return externalIdentifier(file.getName());
    }

    private String externalIdentifier(String path) {
        final Matcher matcher = PATTERN.matcher(path);
        if (matcher.matches()) {
            return matcher.group("filebasename").toUpperCase();
        } else {
            throw new RuntimeException("External Identifier could not be determined");
        }
    }

    public String getExternalIdentifier() {
        return externalIdentifier;
    }

    public InputStream getInputStream() throws IOException {
        if (file != null) {
            return new BufferedInputStream(Files.newInputStream(file.toPath()));
        } else {
            return DocumentTemplate.class.getResourceAsStream(this.classpathResource);
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
