/*-
 * #%L
 * QR Invoice Solutions
 * %%
 * Copyright (C) 2017 - 2024 Codeblock GmbH
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * -
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * -
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * -
 * Other licenses:
 * -----------------------------------------------------------------------------
 * Commercial licenses are available for this software. These replace the above
 * AGPLv3 terms and offer support, maintenance and allow the use in commercial /
 * proprietary products.
 * -
 * More information on commercial licenses are available at the following page:
 * https://www.qr-invoice.ch/licenses/
 * #L%
 */
package ch.codeblock.qrinvoice.documents.model.application;

import ch.codeblock.qrinvoice.documents.model.application.builder.VatDetailsBuilder;
import ch.codeblock.qrinvoice.model.QrInvoice;
import ch.codeblock.qrinvoice.model.annotation.Description;
import ch.codeblock.qrinvoice.model.annotation.Example;
import ch.codeblock.qrinvoice.model.annotation.Mandatory;
import ch.codeblock.qrinvoice.model.builder.QrInvoiceBuilder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;

public class InvoiceDocument {
    private Address sender;
    private Address recipient;

    private ContactPerson contactPerson;

    private String customerNr;
    private String customerReference;

    private LocalDate invoiceDate;
    private LocalDate invoiceDueDate;
    private Integer termOfPaymentDays;

    private String invoiceNr;
    private String title;
    private String prefaceText;
    private String endingText;

    private List<Position> positions;

    private List<VatDetails> vatDetails;

    private Amount grossAmount;

    private Currency currency;

    private List<AdditionalProperty> additionalProperties;

    private QrInvoice qrInvoice;

    @Mandatory
    @Description("Bill sender")
    public Address getSender() {
        return sender;
    }

    public void setSender(final Address sender) {
        this.sender = sender;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Bill recipient")
    public Address getRecipient() {
        return recipient;
    }

    public void setRecipient(final Address recipient) {
        this.recipient = recipient;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("The billers contact person")
    public ContactPerson getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(final ContactPerson contactPerson) {
        this.contactPerson = contactPerson;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Customer number")
    @Example("0001839")
    public String getCustomerNr() {
        return customerNr;
    }

    public void setCustomerNr(final String customerNr) {
        this.customerNr = customerNr;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("If given a customers reference")
    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(final String customerReference) {
        this.customerReference = customerReference;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("The invoice date")
    @Example("2020-11-15")
    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(final LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("The date this invoice is due / is to be paid. Should be invoice date + termOfPaymentDays")
    @Example("2020-12-14")
    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(final LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public void setTermOfPaymentDays(Integer termOfPaymentDays) {
        this.termOfPaymentDays = termOfPaymentDays;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("The number of days the invoice is to be paid")
    @Example("30")
    public Integer getTermOfPaymentDays() {
        return termOfPaymentDays;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("The invoice number")
    @Example("inv-001532")
    public String getInvoiceNr() {
        return invoiceNr;
    }

    public void setInvoiceNr(final String invoiceNr) {
        this.invoiceNr = invoiceNr;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("The invoice title")
    @Example("Online Order 11.11.2020")
    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Text which is added after the table of invoice positions")
    @Example("Dear Sir or Madam\\n\\nWe thank you for your order.")
    public String getPrefaceText() {
        return prefaceText;
    }

    public void setPrefaceText(final String prefaceText) {
        this.prefaceText = prefaceText;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Closing text which is added after the table of invoice positions")
    @Example("Please do not hesitate to contact us if you have any questions.\\n\\nKind regards\\n\\nJohn Doe")
    public String getEndingText() {
        return endingText;
    }

    public void setEndingText(final String endingText) {
        this.endingText = endingText;
    }

    @Mandatory
    @Description("List of positions")
    public List<Position> getPositions() {
        if (positions == null) {
            positions = new ArrayList<>();
        }
        return positions;
    }

    public void setPositions(final List<Position> positions) {
        this.positions = positions;
    }

    public List<VatDetails> getVatDetails() {
        if (vatDetails == null) {
            vatDetails = new ArrayList<>();
        }
        return vatDetails;
    }

    public void setVatDetails(final List<VatDetails> vatDetails) {
        this.vatDetails = vatDetails;
    }

    public Amount getGrossAmount() {
        return grossAmount;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Currency of the invoice which has to match the one specified in QrInvoice")
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    @Mandatory
    @Description("The QrInvoice object containing the Payment Part & Receipt related information")
    public QrInvoice getQrInvoice() {
        return qrInvoice;
    }

    @ch.codeblock.qrinvoice.model.annotation.Optional
    @Description("Additional properties that may be used in custom templates")
    public List<AdditionalProperty> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(List<AdditionalProperty> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public void setQrInvoice(QrInvoice qrInvoice) {
        this.qrInvoice = qrInvoice;
    }

    public Position addPosition() {
        final Position newPosition = new Position();
        addPositions(newPosition);
        return newPosition;
    }

    public void addPositions(final Position... positions) {
        addPositions(Arrays.asList(positions));
    }

    public void addPositions(final List<Position> additionalPositions) {
        if (this.positions == null) {
            setPositions(additionalPositions);
        } else {
            this.positions = new ArrayList<>(this.positions);
            for (Position additionalPosition : additionalPositions) {
                if (!this.positions.contains(additionalPosition)) {
                    this.positions.add(additionalPosition);
                }
            }
        }
    }

    public InvoiceDocument calculate() {
        getPositions().forEach(Position::calculate);

        (this.grossAmount = Amount.createIfMissing(this.grossAmount)).setVatExclusive(
                getPositions().stream().map(Position::getTotal)
                        .filter(Objects::nonNull).map(Amount::getVatExclusive)
                        .filter(Objects::nonNull)
                        .reduce(BigDecimal.ZERO, BigDecimal::add));

        // e.g. 42.04
        BigDecimal grossAmountVatInclusive = getPositions().stream().map(Position::getTotal)
                .filter(Objects::nonNull).map(Amount::getVatInclusive)
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);


        // To round to the nearest .05, multiply by 20, round to the nearest integer, then divide by 20
        // e.g. 42.05
        BigDecimal grossAmountVatInclusiveRounded = BigDecimal.valueOf(Math.round(grossAmountVatInclusive.doubleValue() * 20.0) / 20.0)
                .setScale(2, RoundingMode.HALF_UP);
        (this.grossAmount = Amount.createIfMissing(this.grossAmount)).setVatInclusive(grossAmountVatInclusiveRounded);

        // 42.05 - 42.04 = 0.01
        this.grossAmount.setVatInclusiveRoundingDifference(grossAmountVatInclusiveRounded.subtract(grossAmountVatInclusive));

        calculateVatDetails();


        return this;
    }

    private void calculateVatDetails() {
        this.vatDetails = new ArrayList<>();

        for (final Position p : getPositions()) {
            if (!p.hasAmount()) {
                continue;
            }
            final Optional<VatDetails> vatDetailsOptional = this.vatDetails.stream()
                    .filter(d -> d.getTaxPercentage() != null)
                    .filter(d -> d.getTaxPercentage().equals(p.getUnitPrice().getVatPercentage()))
                    .findFirst();
            if (vatDetailsOptional.isPresent()) {
                final VatDetails vatDetails = vatDetailsOptional.get();
                vatDetails.setTaxBase(vatDetails.getTaxBase().add(p.getTotal().getVatExclusive()));
                vatDetails.setTaxAmount(vatDetails.getTaxAmount().add(p.getTotal().getVatAmount()));
            } else {
                this.vatDetails.add(VatDetailsBuilder.create()
                        .taxBase(p.getTotal().getVatExclusive())
                        .taxAmount(p.getTotal().getVatAmount())
                        .taxPercentage(p.getUnitPrice().getVatPercentage())
                        .build());
            }
        }
    }

    public QrInvoiceBuilder applyCurrencyAmount(QrInvoiceBuilder qrInvoiceBuilder) {
        return qrInvoiceBuilder
                .paymentAmountInformation(p -> p.currency(currency).amount(grossAmount.getVatInclusive()));
    }


    public QrInvoice applyCurrencyAmount(QrInvoice qrInvoice) {
        qrInvoice.getPaymentAmountInformation().setAmount(grossAmount.getVatInclusive());
        qrInvoice.getPaymentAmountInformation().setCurrency(currency);
        return qrInvoice;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InvoiceDocument{");
        sb.append("sender=").append(sender);
        sb.append(", recipient=").append(recipient);
        sb.append(", contactPerson=").append(contactPerson);
        sb.append(", customerNr='").append(customerNr).append('\'');
        sb.append(", customerReference='").append(customerReference).append('\'');
        sb.append(", invoiceDate=").append(invoiceDate);
        sb.append(", invoiceDueDate=").append(invoiceDueDate);
        sb.append(", termOfPaymentDays=").append(termOfPaymentDays);
        sb.append(", invoiceNr='").append(invoiceNr).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", prefaceText='").append(prefaceText).append('\'');
        sb.append(", endingText='").append(endingText).append('\'');
        sb.append(", positions=").append(positions);
        sb.append(", vatDetails=").append(vatDetails);
        sb.append(", grossAmount=").append(grossAmount);
        sb.append(", currency=").append(currency);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append(", qrInvoice=").append(qrInvoice);
        sb.append('}');
        return sb.toString();
    }
}
