<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:date="http://exslt.org/dates-and-times"
                extension-element-prefixes="date">
    <xsl:output method="xml" indent="yes"/>
    <xsl:decimal-format name="amount" decimal-separator="." grouping-separator="'"/>
    <xsl:template match="/invoice">
        <!--<xsl:variable name="debug-border" select="'0.1mm solid red'"/>-->
        <xsl:variable name="debug-border" select="'0mm solid transparent'"/>
        <xsl:variable name="bottom-margin" select="'2cm'"/>
        <fo:root font-family="Helvetica">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="standard-page" page-height="29.7cm" page-width="21cm"
                                       margin-top="2cm"
                                       margin-bottom="{$bottom-margin}" margin-left="23mm" margin-right="20mm"
                >
                    <fo:region-body/>
                    <fo:region-after region-name="standard-invoice-footer" extent="5mm"/>
                </fo:simple-page-master>
                <fo:simple-page-master master-name="last-page" page-height="29.7cm" page-width="21cm" margin-top="2cm"
                                       margin-bottom="{$bottom-margin}" margin-left="22mm" margin-right="22mm"
                >
                    <fo:region-body/>
                </fo:simple-page-master>

                <fo:page-sequence-master master-name="pages">
                    <fo:repeatable-page-master-alternatives>
                        <fo:conditional-page-master-reference page-position="last" master-reference="last-page"/>
                        <fo:conditional-page-master-reference master-reference="standard-page"/>
                    </fo:repeatable-page-master-alternatives>
                </fo:page-sequence-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="pages">
                <fo:static-content flow-name="standard-invoice-footer" font-size="7pt">
                    <fo:table table-layout="fixed" width="100%">
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell text-align="center">
                                    <fo:block></fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body" font-size="8pt">

                    <!--
                    pingen: Adressbereich (X/Y/W/H): 
                    - left:   22/60/85.5/25.5mm
                    - right: 118/60/85.5/25.5mm
                    - now we add 1mm padding to each side
                    -->

                    <fo:block-container absolute-position="fixed" border="{$debug-border}" top="61mm" left="23mm"
                                        height="23.5mm" width="83.5mm" text-align="left">
                        <fo:block-container>
                            <xsl:for-each select="recipient/address-lines/address-line">
                                <fo:block>
                                    <xsl:value-of select="current()"/>
                                </fo:block>
                            </xsl:for-each>
                            <xsl:if test="not(recipient/address-lines/address-line)">
                                <fo:block></fo:block>
                            </xsl:if>
                        </fo:block-container>
                    </fo:block-container>

                    <fo:block-container absolute-position="fixed" border="{$debug-border}" top="61mm" left="119mm"
                                        height="23.5mm" width="83.5mm" text-align="left">
                        <fo:block-container>
                            <xsl:for-each select="sender/address-lines/address-line">
                                <fo:block>
                                    <xsl:value-of select="current()"/>
                                </fo:block>
                            </xsl:for-each>
                            <xsl:if test="not(sender/address-lines/address-line)">
                                <fo:block></fo:block>
                            </xsl:if>
                        </fo:block-container>
                    </fo:block-container>


                    <fo:block-container absolute-position="fixed" top="20mm" left="22mm" text-align="left">
                        <fo:block>
                            <xsl:if test="/invoice/logo and /invoice/logo/src != ''">
                                <fo:external-graphic content-width="5cm" src="{/invoice/logo/src}"/>
                            </xsl:if>
                        </fo:block>
                    </fo:block-container>

                    <fo:block-container>
                        <fo:block space-before="8cm" space-after="0cm" text-align="left" font-size="16pt"
                                  font-weight="bold">
                            <xsl:value-of select="/invoice/labels/default/invoice"/>
                        </fo:block>
                        <fo:block space-before="0cm" space-after="1cm" text-align="left" font-size="16pt"
                                  font-weight="bold">
                            <xsl:value-of select="title"/>
                        </fo:block>
                    </fo:block-container>

                    <fo:table table-layout="fixed" width="100%" end-indent="0mm">
                        <fo:table-column column-width="8.3cm" end-indent="0mm"/>
                        <fo:table-column column-width="8.3cm" end-indent="0mm"/>

                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell text-align="left">
                                    <fo:block><xsl:value-of select="/invoice/labels/default/invoice-nr"/>:
                                        <xsl:value-of select="invoice-nr"/>
                                    </fo:block>
                                    <fo:block><xsl:value-of select="/invoice/labels/default/customer-nr"/>:
                                        <xsl:value-of select="customer-nr"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right">
                                    <fo:block><xsl:value-of select="/invoice/labels/default/invoice-date"/>:
                                        <xsl:value-of select="date:format-date(invoice-date, 'dd.MM.yyyy')"/>
                                    </fo:block>
                                    <fo:block><xsl:value-of select="/invoice/labels/default/invoice-due-date"/>:
                                        <xsl:value-of select="date:format-date(invoice-due-date, 'dd.MM.yyyy')"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <xsl:if test="preface-text != ''">
                        <fo:block space-before="1cm" space-after="1cm" linefeed-treatment="preserve">
                            <xsl:value-of select="preface-text"/>
                        </fo:block>
                    </xsl:if>
                    <xsl:if test="count(positions/position) > 0">
                        <fo:table table-layout="fixed" width="100%">
                            <fo:table-column column-width="8mm"/>
                            <xsl:if test="/invoice/has-discounts = 'true'">
                                <fo:table-column column-width="8.8cm"/>
                            </xsl:if>
                            <xsl:if test="not(/invoice/has-discounts = 'true')">
                                <fo:table-column column-width="10.8cm"/>
                            </xsl:if>
                            <fo:table-column column-width="1cm"/> <!-- 1.2 -->
                            <fo:table-column column-width="2cm"/>
                            <xsl:if test="/invoice/has-discounts = 'true'">
                                <fo:table-column column-width="2cm"/>
                            </xsl:if>
                            <fo:table-column column-width="2cm"/>
                            <fo:table-header font-weight="bold">
                                <fo:table-row border-bottom="1px solid #eee">
                                    <fo:table-cell text-align="left" padding-bottom="2px">
                                        <fo:block>
                                            <xsl:value-of select="/invoice/labels/default/position-header-abbr"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="left" padding-bottom="2px">
                                        <fo:block>
                                            <xsl:value-of select="/invoice/labels/default/position-header-description"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" padding-bottom="2px">
                                        <fo:block>
                                            <xsl:value-of select="/invoice/labels/default/position-header-quantity"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" padding-bottom="2px">
                                        <fo:block>
                                            <xsl:value-of select="/invoice/labels/default/position-header-unit-price"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <xsl:if test="/invoice/has-discounts = 'true'">
                                        <fo:table-cell text-align="right" padding-bottom="2px">
                                            <fo:block>
                                                <xsl:value-of select="/invoice/labels/default/position-header-discount"/>
                                            </fo:block>
                                        </fo:table-cell>
                                    </xsl:if>
                                    <fo:table-cell text-align="right" padding-bottom="2px">
                                        <fo:block><xsl:value-of
                                                select="/invoice/labels/default/position-header-currency-prefix"/>&#160;<xsl:value-of
                                                select="/invoice/currency"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-header>
                            <fo:table-body>
                                <xsl:for-each select="positions/position">
                                    <fo:table-row keep-together.within-page="always" border-bottom="1px solid #eee">
                                        <fo:table-cell text-align="left" padding-bottom="2px" padding-top="2px">
                                            <fo:block>
                                                <xsl:value-of select="position"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell text-align="left" padding-bottom="2px" padding-top="2px">
                                            <fo:block linefeed-treatment="preserve">
                                                <xsl:value-of select="description"/>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px">
                                            <fo:block>
                                                <xsl:if test="quantity != ''">
                                                    <xsl:value-of
                                                            select="format-number(quantity, '######.##', 'amount')"/>
                                                    <xsl:if test="unit != ''">
                                                        &#160;<xsl:value-of select="unit"/>
                                                    </xsl:if>
                                                </xsl:if>
                                            </fo:block>
                                        </fo:table-cell>
                                        <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px">
                                            <fo:block>
                                                <xsl:if test="unit-price-vat-exclusive != ''">
                                                    <xsl:value-of
                                                            select="format-number(unit-price-vat-exclusive, '######.00', 'amount')"/>
                                                </xsl:if>
                                            </fo:block>
                                        </fo:table-cell>
                                        <xsl:if test="/invoice/has-discounts = 'true'">
                                            <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px">
                                                <fo:block>
                                                    <xsl:if test="discount-percentage != ''">
                                                        <xsl:value-of
                                                                select="concat(format-number(-discount-percentage, '##.##', 'amount'),'%')"/>
                                                    </xsl:if>
                                                    <xsl:if test="discount-absolute-vat-exclusive != '' ">
                                                        <xsl:value-of
                                                                select="format-number(-discount-absolute-vat-exclusive, '######.00', 'amount')"/>
                                                    </xsl:if>
                                                </fo:block>
                                            </fo:table-cell>
                                        </xsl:if>
                                        <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px">
                                            <fo:block>
                                                <xsl:if test="amount-vat-exclusive != ''">
                                                    <xsl:value-of
                                                            select="format-number(amount-vat-exclusive, '######.00', 'amount')"/>
                                                </xsl:if>
                                            </fo:block>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </xsl:for-each>
                            </fo:table-body>
                        </fo:table>
                    </xsl:if>
                    <fo:block space-after="1cm"></fo:block>
                    <fo:table table-layout="fixed" width="100%">
                        <fo:table-column column-width="8.6cm"/>
                        <fo:table-column column-width="4cm"/>
                        <fo:table-column column-width="4cm"/>
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block></fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px"
                                               border-bottom="1px solid #eee">
                                    <fo:block font-weight="bold">
                                        <xsl:value-of select="/invoice/labels/default/total-excl-vat"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px"
                                               border-bottom="1px solid #eee">
                                    <fo:block>
                                        <xsl:value-of
                                                select="format-number(gross-amount-vat-exclusive, '#####0.00', 'amount')"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <xsl:for-each select="vat-details/vat-details">
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block></fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px"
                                                   border-bottom="1px solid #eee">
                                        <fo:block>
                                            <xsl:value-of
                                                    select="/invoice/labels/default/additional-vat"/>&#160;<xsl:value-of
                                                select="format-number(tax-percentage, '#0.00', 'amount')"/>%&#160;<xsl:value-of
                                                select="/invoice/labels/default/additional-vat-on"/>&#160;<xsl:value-of
                                                select="format-number(tax-base, '#0.00', 'amount')"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px"
                                                   border-bottom="1px solid #eee">
                                        <fo:block>
                                            <xsl:value-of select="format-number(tax-amount, '#####0.00', 'amount')"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:for-each>

                            <xsl:if test="rounding-difference != 0">
                                <fo:table-row>
                                    <fo:table-cell>
                                        <fo:block></fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px"
                                                   border-bottom="1px solid #eee">
                                        <fo:block>
                                            <xsl:value-of select="/invoice/labels/default/rounding-difference"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px"
                                                   border-bottom="1px solid #eee">
                                        <fo:block>
                                            <xsl:value-of
                                                    select="format-number(rounding-difference, '#####0.00', 'amount')"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:if>

                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block></fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px"
                                               border-bottom="1px solid #eee">
                                    <fo:block font-weight="bold">
                                        <xsl:value-of select="/invoice/labels/default/total-incl-vat"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell text-align="right" padding-bottom="2px" padding-top="2px"
                                               border-bottom="1px solid #eee">
                                    <fo:block>
                                        <xsl:value-of
                                                select="format-number(gross-amount-vat-inclusive, '#####0.00', 'amount')"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>

                    <xsl:if test="ending-text != ''">
                        <fo:block-container>
                            <fo:block space-before="1cm" space-after="5mm" linefeed-treatment="preserve" page-break-inside="avoid"
                                      border="{$debug-border}">
                                <xsl:value-of select="ending-text"/>
                            </fo:block>
                        </fo:block-container>
                    </xsl:if>

                    <!-- block container make sure there is empty space for the DIN-LANG overlay -->
                    <fo:block-container height="105mm - {$bottom-margin}">
                        <fo:block>&#160;</fo:block>
                    </fo:block-container>

                    <fo:block-container position="fixed" left="0" top="149mm" height="148mm">
                        <!-- line-height=0 fixes warning of "WARNING: Content overflows the viewport of an fo:block-container in block-progression direction by ..."  -->
                        <fo:block line-height="0pt">
                            <fo:external-graphic src="{/invoice/ppr/src}"/>
                        </fo:block>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>